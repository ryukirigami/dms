
<%@ page import="com.kombos.administrasi.MasterDurasiPerjalananDriver" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="masterDurasiPerjalananDriver_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>
		
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="masterDurasiPerjalananDriver.companyDealer1.label" default="Lokasi 1" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="masterDurasiPerjalananDriver.companyDealer2.label" default="Lokasi 2" /></div>
			</th>
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="masterDurasiPerjalananDriver.m009TglBerlaku.label" default="Tanggal Berlaku" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="masterDurasiPerjalananDriver.m009Durasi.label" default="Durasi" /></div>
			</th>
		</tr>
		<tr>
		

			
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer2" class="search_init" />
				</div>
			</th>
			
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m009TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m009TglBerlaku" value="date.struct">
					<input type="hidden" name="search_m009TglBerlaku_day" id="search_m009TglBerlaku_day" value="">
					<input type="hidden" name="search_m009TglBerlaku_month" id="search_m009TglBerlaku_month" value="">
					<input type="hidden" name="search_m009TglBerlaku_year" id="search_m009TglBerlaku_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m009TglBerlaku_dp" value="" id="search_m009TglBerlaku" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m009Durasi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m009Durasi" class="search_init durasi" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>

function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	return false;
	
	return true;
}

var masterDurasiPerjalananDriverTable;
var reloadMasterDurasiPerjalananDriverTable;
$(function(){
	
	reloadMasterDurasiPerjalananDriverTable = function() {
		masterDurasiPerjalananDriverTable.fnDraw();
	}

	var recordsmasterDurasiPerjalananDriverperpage = [];//new Array();
    var anMasterDurasiPerjalananDriverSelected;
    var jmlRecMasterDurasiPerjalananDriverPerPage=0;
    var id;

	$('#search_m009TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m009TglBerlaku_day').val(newDate.getDate());
			$('#search_m009TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m009TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			masterDurasiPerjalananDriverTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	masterDurasiPerjalananDriverTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	masterDurasiPerjalananDriverTable = $('#masterDurasiPerjalananDriver_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsMasterDurasiPerjalananDriver = $("#masterDurasiPerjalananDriver_datatables tbody .row-select");
            var jmlMasterDurasiPerjalananDriverCek = 0;
            var nRow;
            var idRec;
            rsMasterDurasiPerjalananDriver.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsmasterDurasiPerjalananDriverperpage[idRec]=="1"){
                    jmlMasterDurasiPerjalananDriverCek = jmlMasterDurasiPerjalananDriverCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsmasterDurasiPerjalananDriverperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecMasterDurasiPerjalananDriverPerPage = rsMasterDurasiPerjalananDriver.length;
            if(jmlMasterDurasiPerjalananDriverCek==jmlRecMasterDurasiPerjalananDriverPerPage && jmlRecMasterDurasiPerjalananDriverPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [



{
	"sName": "companyDealer1",
	"mDataProp": "companyDealer1",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "companyDealer2",
	"mDataProp": "companyDealer2",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "m009TglBerlaku",
	"mDataProp": "m009TglBerlaku",
	"aTargets": [2],
	
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,
{
	"sName": "m009Durasi",
	"mDataProp": "m009Durasi",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m009TglBerlaku = $('#search_m009TglBerlaku').val();
						var m009TglBerlakuDay = $('#search_m009TglBerlaku_day').val();
						var m009TglBerlakuMonth = $('#search_m009TglBerlaku_month').val();
						var m009TglBerlakuYear = $('#search_m009TglBerlaku_year').val();
						
						if(m009TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_m009TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m009TglBerlaku_dp', "value": m009TglBerlaku},
									{"name": 'sCriteria_m009TglBerlaku_day', "value": m009TglBerlakuDay},
									{"name": 'sCriteria_m009TglBerlaku_month', "value": m009TglBerlakuMonth},
									{"name": 'sCriteria_m009TglBerlaku_year', "value": m009TglBerlakuYear}
							);
						}
	
						var companyDealer1 = $('#filter_companyDealer1 input').val();
						if(companyDealer1){
							aoData.push(
									{"name": 'sCriteria_companyDealer1', "value": companyDealer1}
							);
						}
	
						var companyDealer2 = $('#filter_companyDealer2 input').val();
						if(companyDealer2){
							aoData.push(
									{"name": 'sCriteria_companyDealer2', "value": companyDealer2}
							);
						}
	
						var m009Durasi = $('#filter_m009Durasi input').val();
						if(m009Durasi){
							aoData.push(
									{"name": 'sCriteria_m009Durasi', "value": m009Durasi}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#masterDurasiPerjalananDriver_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsmasterDurasiPerjalananDriverperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsmasterDurasiPerjalananDriverperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#masterDurasiPerjalananDriver_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsmasterDurasiPerjalananDriverperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anMasterDurasiPerjalananDriverSelected = masterDurasiPerjalananDriverTable.$('tr.row_selected');
            if(jmlRecMasterDurasiPerjalananDriverPerPage == anMasterDurasiPerjalananDriverSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsmasterDurasiPerjalananDriverperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>
<script type="text/javascript">
    jQuery(function($) {
        $('.durasi').autoNumeric('init',{
            vMin:'0',
            vMax:'1000',
            mDec: '2',
            aSep:''
        });
    });
</script>

			
