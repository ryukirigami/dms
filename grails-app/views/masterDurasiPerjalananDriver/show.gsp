

<%@ page import="com.kombos.administrasi.MasterDurasiPerjalananDriver" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'masterDurasiPerjalananDriver.label', default: 'Master Durasi Perjalanan Driver')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteMasterDurasiPerjalananDriver;

$(function(){ 
	deleteMasterDurasiPerjalananDriver=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/masterDurasiPerjalananDriver/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadMasterDurasiPerjalananDriverTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-masterDurasiPerjalananDriver" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="masterDurasiPerjalananDriver"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${masterDurasiPerjalananDriverInstance?.m009TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m009TglBerlaku-label" class="property-label"><g:message
					code="masterDurasiPerjalananDriver.m009TglBerlaku.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m009TglBerlaku-label">
						%{--<ba:editableValue
								bean="${masterDurasiPerjalananDriverInstance}" field="m009TglBerlaku"
								url="${request.contextPath}/MasterDurasiPerjalananDriver/updatefield" type="text"
								title="Enter m009TglBerlaku" onsuccess="reloadMasterDurasiPerjalananDriverTable();" />--}%
							
								<g:formatDate date="${masterDurasiPerjalananDriverInstance?.m009TglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterDurasiPerjalananDriverInstance?.companyDealer1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer1-label" class="property-label"><g:message
					code="masterDurasiPerjalananDriver.companyDealer1.label" default="Lokasi 1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer1-label">
						%{--<ba:editableValue
								bean="${masterDurasiPerjalananDriverInstance}" field="companyDealer1"
								url="${request.contextPath}/MasterDurasiPerjalananDriver/updatefield" type="text"
								title="Enter companyDealer1" onsuccess="reloadMasterDurasiPerjalananDriverTable();" />--}%
							
                                <g:fieldValue bean="${masterDurasiPerjalananDriverInstance?.companyDealer1}" field="m011Alamat"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterDurasiPerjalananDriverInstance?.companyDealer2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer2-label" class="property-label"><g:message
					code="masterDurasiPerjalananDriver.companyDealer2.label" default="Lokasi 2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer2-label">
						%{--<ba:editableValue
								bean="${masterDurasiPerjalananDriverInstance}" field="companyDealer2"
								url="${request.contextPath}/MasterDurasiPerjalananDriver/updatefield" type="text"
								title="Enter companyDealer2" onsuccess="reloadMasterDurasiPerjalananDriverTable();" />--}%
							
                        <g:fieldValue bean="${masterDurasiPerjalananDriverInstance?.companyDealer2}" field="m011Alamat"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterDurasiPerjalananDriverInstance?.m009Durasi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m009Durasi-label" class="property-label"><g:message
					code="masterDurasiPerjalananDriver.m009Durasi.label" default="Durasi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m009Durasi-label">
						%{--<ba:editableValue
								bean="${masterDurasiPerjalananDriverInstance}" field="m009Durasi"
								url="${request.contextPath}/MasterDurasiPerjalananDriver/updatefield" type="text"
								title="Enter m009Durasi" onsuccess="reloadMasterDurasiPerjalananDriverTable();" />--}%
							
								<g:fieldValue bean="${masterDurasiPerjalananDriverInstance}" field="m009Durasi"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${masterDurasiPerjalananDriverInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${masterDurasiPerjalananDriverInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${masterDurasiPerjalananDriverInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>


			<g:if test="${masterDurasiPerjalananDriverInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${masterDurasiPerjalananDriverInstance}" field="dateCreated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${masterDurasiPerjalananDriverInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterDurasiPerjalananDriverInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="jenisStall.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${masterDurasiPerjalananDriverInstance}" field="createdBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${masterDurasiPerjalananDriverInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterDurasiPerjalananDriverInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jenisStall.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${masterDurasiPerjalananDriverInstance}" field="lastUpdated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${masterDurasiPerjalananDriverInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterDurasiPerjalananDriverInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="jenisStall.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${masterDurasiPerjalananDriverInstance}" field="updatedBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${masterDurasiPerjalananDriverInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${masterDurasiPerjalananDriverInstance?.id}"
					update="[success:'masterDurasiPerjalananDriver-form',failure:'masterDurasiPerjalananDriver-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteMasterDurasiPerjalananDriver('${masterDurasiPerjalananDriverInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
