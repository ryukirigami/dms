<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var problemFindingSubSubSubTable;
$(function(){
problemFindingSubSubSubTable = $('#problemFinding_datatables_sub_sub_sub_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubSubSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
	"sName": null,
	"mDataProp": "roleRespon",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return ' <input type="hidden" value="'+data+'">&nbsp;&nbsp;' + data;
        },
        "bSortable": false,
        "sWidth":"241px",
        "bVisible": true
    },
    {
        "sName": null,
        "mDataProp": "responder",
        "aTargets": [1],
        "bSortable": false,
        "sWidth":"153px",
        "bVisible": true
    },
    {
        "sName": null,
	    "mDataProp": "tanggalRespon",
        "aTargets": [2],
        "bSortable": false,
        "sWidth":"143px",
        "bVisible": true
    },
    {
        "sName": null,
        "mDataProp": "deskripsiRespon",
        "aTargets": [3],
        "bSortable": false,
        "sWidth":"275px",
        "bVisible": true
    },
    {
        "sName": null ,
        "mDataProp": "solusi",
        "aTargets": [4],
        "mRender": function ( data, type, row ) {
            if(data=="Ya"){
                return '<span style="color:green">' + data + '</span>';
            }else{
                return '<span><a style="color:red" href="javascript:void(0);" onclick="if(confirm(\' Are You Sure?\' )){link2(\''+row['noWo']+'\',this)}">' + data + '</a></span>';
            }

        },
        "bSortable": false,
        "sWidth":"169px",
        "bVisible": true
    }

    ],
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                            aoData.push(
                                        {"name": 'noWo', "value": "${noWo}"},
                                        {"name": 'idMasalah', "value": "${idMasalah}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
    </g:javascript>
</head>
<body>
<div class="innerinnerDetails">
    <table id="problemFinding_datatables_sub_sub_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Role Respon</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Responder</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Tanggal Respon</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Deskripsi Respon</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Solusi</div>
            </th>


        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
