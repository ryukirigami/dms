<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'returnsAdd.label', default: 'Respond Problem')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	$(function(){

        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

        $('.box-action').click(function(){
            return false;
        });

    Respond = function(){
        var id = $('#noWo').val();
        var idMasalah = $('#idMasalah').val();
        var responProblem = $('#responProblem').val();

        if(responProblem=="" || responProblem==null)
           responProblem="-"
        var forwardKe = $('#forwardKe').val();
        $.ajax({
            url:'${request.contextPath}/problemFindingGr/respon',
            type: "POST", // Always use POST when deleting data
            data : {noWo:id,responProblem :responProblem ,forwardKe :forwardKe,idMasalah:idMasalah },
            success : function(data){
                toastr.success('<div>Respon Success</div>');
                expandTableLayout();
            },
        error: function(xhr, textStatus, errorThrown) {
        alert('Internal Server Error');
        }
        });
    }
   });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="row-fluid">
        <div id="kiri" class="span5">
            <table style="width: 100%;">
                <tr>
                    <td style="padding: 5px">
                        <g:message code="PF.t401NoWo.label" default="Nomor WO" />
                    </td>
                    <td>
                     <g:textField style="width:100%" name="noWo" id="noWo" readonly="" value="${noWo}" />
                     <g:hiddenField name="idMasalah" id="idMasalah" value="${idMasalah}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="PF.noPolisi.label" default="Nomor Polisi" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="nopol" readonly="" value="${nopol}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                         <g:message code="PF.model.label" default="Model Kendaraan" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="model" readonly="" value="${model}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="PF.stall.label" default="Nama Stall" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="stall" readonly="" value="${stall}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="PF.teknisi.label" default="Teknisi" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="teknisi" readonly="" value="${teknisi}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="PF.foreman.label" default="Foreman" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="foreman" readonly="" value="${foreman}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="PF.job.label" default="Job" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="job" readonly="" value="${job}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="PF.deskripsi.label" default="Deskripsi Problem" />
                    </td>
                    <td>
                        <g:textArea style="width:100%" name="deskripsi" readonly="" value="${deskripsi}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="PF.tanggal.label" default="Tanggal Problem" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="tanggalProblem" readonly="" value="${tanggalProblem}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="PF.jenis.label" default="Jenis Problem" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="jenisProblem" readonly="" value="${jenisProblem}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="PF.status.label" default="Status Kendaraan" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="statusKendaraan" readonly="" value="${statusKendaraan}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="PF.catatan.label" default="Catatan Tambahan"  />
                    </td>
                    <td>
                        <g:textArea style="width:100%" name="catatanTambahan" readonly="" value="${catatanTambahan}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="PF.jenis1.label" default="Butuh RSA ?" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="butuhRSA" readonly="" value="${butuhRSA}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="PF.status1.label" default="Bisa Dikerjakan Sendiri" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="bisaDikerjaanSendiri" readonly="" value="${bisaDikerjakanSendiri}" />
                    </td>
                </tr>
             </table>
        </div>

    <div id="kanan" class="span7">
        <table style="width: 100%;">

            <tr>
                <td style="padding: 5px">
                    <g:message code="PF.catatan1.label" default="Perlu Tambahan Waktu" />
                </td>
                <td>
                    <g:textField style="width:100%" name="perluTambahan" readonly="" value="${perluTambahanWaktu}" />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="PF.statu1s.label" default="Butuh Tambahan Job" />
                </td>
                <td>
                    <g:textField style="width:100%" name="butuhTambahanJob" readonly="" value="${butuhTambahanJob}" />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="PF.catatan1.label" default="Perlu Tambahan Material" />
                </td>
                <td>
                    <g:textField style="width:100%" name="Perlu" readonly="" value="${perluTambahanMaterial}" />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px" colspan="2">
                    <fieldset>
                        <legend>Parts yg Dibutuhkan</legend>
                        <div class="control-group">
                            <g:render template="dataTablesParts" />
                        </div>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px" >
                    <g:message code="PF.catatan4.label" default="Hasil Pencarian parts yang dibutuhkan" />
                </td>
                <td>
                    <g:textArea style="width:100%" name="hasil" readonly="" value="${partDibutuhkan}" />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px"  >
                    <g:message code="PF.catata3n.label" default="Respon Terhadap Problem" />
                </td>
                <td>
                    <g:textArea style="width:100%" name="respon" id="responProblem" value="${respon}" />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px" >
                    <g:message code="PF.statu1s.label" default="Tanggal Update" />
                </td>
                <td>
                    <g:textField style="width:100%" name="tglUpdate" readonly="" value="${tanggalUpdate}" />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px" >
                    <g:message code="PF.catatan1.label" default="Forward Ke" />
                </td>
                <td>
                    <select name="forwardKe" id="forwardKe" size="3">
                        <option value="TL">TL</option>
                        <option value="ATL">ATL</option>
                        <option value="KB">Kepala Bengkel</option>
                    </select>
                </td>
            </tr>
          </table>
        <a class="btn btn-primary" href="javascript:void(0);" onclick="Respond();">Respond</a>
        <a class="btn cancel" onclick="window.location.href ='#/home';"><g:message code="default.button.cancel.label" default="Close" /></a>
    </div>
 </div>
</div>
</body>
</html>

