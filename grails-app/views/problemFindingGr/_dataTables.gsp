
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay" />

<table id="problemFinding_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
    <thead>
    <tr>
        <th></th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Nomor WO</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Nomor Polisi</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Tanggal WO</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Start</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Stop</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Final Inspection</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>SA</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Janji Penyerahan</div>
        </th>

    </tr>

    </thead>
</table>

<g:javascript>
var problemFindingTable;
var reloadProblemFindingTable;
$(function(){

    reloadProblemFindingTable = function() {
		problemFindingTable.fnDraw();
	}
 $('#clear').click(function(e){
        $('#search_t017Tanggal').val("");
        $('#search_t017Tanggal_day').val("");
        $('#search_t017Tanggal_month').val("");
        $('#search_t017Tanggal_year').val("");
        $('#search_t017Tanggal2').val("");
        $('#search_t017Tanggal2_day').val("");
        $('#search_t017Tanggal2_month').val("");
        $('#search_t017Tanggal2_year').val("");
        e.stopPropagation();
		problemFindingTable.fnDraw();
	});
    $('#view').click(function(e){
        e.stopPropagation();
		problemFindingTable.fnDraw();
	});
	var anOpen = [];
	$('#problemFinding_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = problemFindingTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/problemFindingGr/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = problemFindingTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			problemFindingTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});
	
    $('#problemFinding_datatables_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( problemFindingTable, nEditing );
            editRow( problemFindingTable, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( problemFindingTable, nEditing );
            nEditing = null;
        }
        else {
            editRow( problemFindingTable, nRow );
            nEditing = nRow;
        }
    } );
    
	reloadProblemFindingTable = function() {
		problemFindingTable.fnDraw();
	}

 	problemFindingTable = $('#problemFinding_datatables_${idTable}').dataTable({
		"sScrollX": "1000px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "jpb",
	"mDataProp": "noWo",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" value="'+data+'">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"sWidth":"170px",
	"bVisible": true
},
{
	"sName": "jpb",
	"mDataProp": "nopol",
	"aTargets": [1] ,
	"bSortable": false,
	"sWidth":"120px",
	"bVisible": true
},
{
	"sName": "jpb",
	"mDataProp": "tglWo",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"120px",
	"bVisible": true
},
{
	"sName": "actual",
	"mDataProp": "start",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"120px",
	"bVisible": true
},
{
	"sName": "actual",
	"mDataProp": "stop",
	"aTargets": [4],
	"bSortable": false,
	"sWidth":"160px",
	"bVisible": true
},
{
	"sName": null,
	"mDataProp": "finalInspection",
	"aTargets": [5],
	"bSortable": false,
	"sWidth":"120px",
	"bVisible": true
},
{
	"sName": "jpb",
	"mDataProp": "SA",
	"aTargets": [6],
	"bSortable": false,
	"sWidth":"220px",
	"bVisible": true
},
{
	"sName": "jpb",
	"mDataProp": "janjiPenyerahan",
	"aTargets": [7],
	"bSortable": false,
	"sWidth":"120px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var t017Tanggal = $('#search_t017Tanggal').val();
						var t017TanggalDay = $('#search_t017Tanggal_day').val();
						var t017TanggalMonth = $('#search_t017Tanggal_month').val();
						var t017TanggalYear = $('#search_t017Tanggal_year').val();

						var t017Tanggal2 = $('#search_t017Tanggal2').val();
						var t017TanggalDay2 = $('#search_t017Tanggal2_day').val();
						var t017TanggalMonth2 = $('#search_t017Tanggal2_month').val();
						var t017TanggalYear2 = $('#search_t017Tanggal2_year').val();

						if(t017Tanggal){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal_dp', "value": t017Tanggal},
									{"name": 'sCriteria_t017Tanggal_day', "value": t017TanggalDay},
									{"name": 'sCriteria_t017Tanggal_month', "value": t017TanggalMonth},
									{"name": 'sCriteria_t017Tanggal_year', "value": t017TanggalYear}
							);
						}


						if(t017Tanggal2){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal2', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal2_dp', "value": t017Tanggal2},
									{"name": 'sCriteria_t017Tanggal2_day', "value": t017TanggalDay2},
									{"name": 'sCriteria_t017Tanggal2_month', "value": t017TanggalMonth2},
									{"name": 'sCriteria_t017Tanggal2_year', "value": t017TanggalYear2}
							);
						}
	                    var status = $('#status').val();
                        if(status){
                          aoData.push(
									{"name": 'status', "value": status}
							);
                        }
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});



</g:javascript>



