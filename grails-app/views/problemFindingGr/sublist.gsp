<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var problemFindingSubTable_${idTable};
$(function(){ 
var anOpen = [];
	$('#problemFinding_datatables_sub_${idTable} td.subcontrol').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = problemFindingSubTable_${idTable}.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/problemFindingGr/subsublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = problemFindingSubTable_${idTable}.fnOpen(nTr,data,'details');
    				$('div.innerinnerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerinnerDetails', $(nTr).next()[0]).slideUp( function () {
      			problemFindingSubTable_${idTable}.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    $('#problemFinding_datatables_sub_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( problemFindingSubTable_${idTable}, nEditing );
            editRow( problemFindingSubTable_${idTable}, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( problemFindingSubTable_${idTable}, nEditing );
            nEditing = null;
        }
        else {
            editRow( problemFindingSubTable_${idTable}, nRow );
            nEditing = nRow;
        }
    } );
    if(problemFindingSubTable_${idTable})
    	problemFindingSubTable_${idTable}.dataTable().fnDestroy();
problemFindingSubTable_${idTable} = $('#problemFinding_datatables_sub_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "jpb",
	"mDataProp": "operation",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return ' <input type="hidden" value="'+data+'">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"sWidth":"165px",
	"bVisible": true
},
{
	"sName": "t501TglUpdate",
	"mDataProp": "t501TglUpdate",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": null,
	"mDataProp": "lastRespon",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
} ,
{
	"sName": null,
	"mDataProp": "jumlah",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
} ,
{
	"sName": "jpb",
	"mDataProp": "foreman",
	"aTargets": [4],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
} ,
{
	"sName": "jpb",
	"mDataProp": "teknisi",
	"aTargets": [5],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
} ,
{
	"sName": "t501StaSolved",
	"mDataProp": "staSolved",
	"aTargets": [6],
	"mRender": function ( data, type, row ) {
	    if(data=="Solved"){
            return '<span style="color:green">' + data + '</span>';
	    }else{
	        return '<span><a style="color:red" href="javascript:void(0);" onclick="if(confirm(\' Are You Sure?\' )){link(\''+row['noWo']+'\',this)}">' + data + '</a></span>';
	    }

	},
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'noWo', "value": "${noWo}"}

						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
    </g:javascript>
</head>
<body>
<div class="innerDetails">
    <table id="problemFinding_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Nama Job</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Last Problem</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Last Respon</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Jumlah Problem</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Foreman</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Teknisi</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Status Problem</div>
            </th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>

