<%@ page import="com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="title" value="Problem Finding List" />
    <title>${title}</title>
    <r:require modules="baseapplayout" />
    <r:require module="export"/>
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var printReturns;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();" />
        break;
    case '_DELETE_' :
        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});

         		break;
       }
       return false;
	});

    link = function(id,itemLink){
        var nRow = id;
        $('#spinner').fadeIn(1);
        $.ajax({type:'POST', url:'${request.contextPath}/problemFindingGr/ubah',
            data: { noWo: nRow},
            success:function(data,textStatus){
                 $(itemLink).parent().css("color","green");
                 $(itemLink).parent().html(data);
            },
            error:function(XMLHttpRequest,textStatus,errorThrown){},
            complete:function(XMLHttpRequest,textStatus){
                $('#spinner').fadeOut();
            }
        });

	}
	link2 = function(id,itemLink){
        var nRow = id;
        $('#spinner').fadeIn(1);
        $.ajax({type:'POST', url:'${request.contextPath}/problemFindingGr/ubah2',
            data: { noWo: nRow},
            success:function(data,textStatus){
                 $(itemLink).parent().css("color","green");
                 $(itemLink).parent().html(data);
            },
            error:function(XMLHttpRequest,textStatus,errorThrown){},
            complete:function(XMLHttpRequest,textStatus){
                $('#spinner').fadeOut();
            }
        });

	}
    respon = function() {

       	var nRow = ""
       	var checkGoods =[];
       	$("#problemFinding-table tbody .row-select2").each(function() {
       	        if(this.checked){
       	            nRow = $(this).next("input:hidden").val();
       	            checkGoods.push(nRow);
       	        }
       	});
       	if(checkGoods.length==1){
       	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
         $.ajax({type:'POST', url:'${request.contextPath}/problemFindingGr/responProblem',
   		     data: { id: nRow},
   			success:function(data,textStatus){
                isiData(nRow);
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		 });
       	}else{
       	    alert("Pilih Salah Satu Problem");
       	}
    };


            loadForm = function(data, textStatus){
                $('#respon-form').empty();
                $('#respon-form').append(data);
            }

            shrinkTableLayout = function(){
                $("#problemFinding-table").hide()
                $("#respon-form").css("display","block");
            }

            expandTableLayout = function(){
                $("#problemFinding-table").show()
                $("#respon-form").css("display","none");
            }

    woDetail = function(){
           checkProblemFinding =[];
            var idCheckJobForTeknisi="";

            $("#problemFinding-table tbody .row-select").each(function() {
                 if(this.checked){
                    var nRow = $(this).next("input:hidden").val();
					checkProblemFinding.push(nRow);
                    idCheckProblemFinding =  JSON.stringify(checkProblemFinding);
//                     window.location.replace('#/woDetailGr');
					    $.ajax({
                            url:'${request.contextPath}/woDetailGr/list',
                            type: "POST",
                            data: { id: nRow, aksi : 'inspect' },
                            dataType: "html",
                            complete : function (req, err) {
                                $('#main-content').html(req.responseText);
                                $('#spinner').fadeOut();
                                loading = false;
                            }
                        });
                }
            });
            if(checkProblemFinding.length<1 || checkProblemFinding.length>1 ){
                alert('Silahkan Pilih Salah satu WO');
                return;
            }
    }

    exports = function(){
           window.location = "${request.contextPath}/problemFindingGr/report";
    }

});
        var checkin = $('#search_t017Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
            checkin.hide();
            $('#search_t017Tanggal2')[0].focus();
        }).data('datepicker');

        var checkout = $('#search_t017Tanggal2').datepicker({
            onRender: function(date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');

        function isiData(idMasalah){
            var id=idMasalah;
            $.ajax({
                url:'${request.contextPath}/problemFindingGr/getTableKebutuhanPart',
                type: "POST",
                data: { idMasalah: id },
                success : function(data){
                    if(data.length>0){
                        $.each(data,function(i,item){
                            partsTable.fnAddData({
                                'kodeParts': item.kodeParts,
                                'namaParts': item.namaParts,
                                'qty': item.qty
                            });
                        });
                    }
                }
            });
        }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left">${title}</span>
    <ul class="nav pull-right">

    </ul>
</div>
<div class="box">
    <div class="span12" id="problemFinding-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <fieldset>
            <table style="padding-right: 10px">
                <tr>
                    <td style="width: 130px">
                        <label class="control-label" for="t951Tanggal">
                            <g:message code="auditTrail.tanggal.label" default="Tanggal WO" />&nbsp;
                        </label>&nbsp;&nbsp;
                    </td>
                    <td>
                        <div id="filter_m777Tgl" class="controls">
                            <ba:datePicker id="search_t017Tanggal" name="search_t017Tanggal" precision="day" format="dd/MM/yyyy"  value="" />
                            &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                            <ba:datePicker id="search_t017Tanggal2" name="search_t017Tanggal2" precision="day" format="dd/MM/yyyy"  value=""  />
                        </div>
                    </td>
                    <td style="width: 200px">
                        <label class="control-label" for="t951Tanggal" style="text-align: right">
                            <g:message code="auditTrail.tanggal.label" default="Status Problem Finding" />&nbsp;
                        </label>&nbsp;&nbsp;
                    </td>
                    <td>
                        <div id="filter_status" class="controls">
                            <select name="status" id="status">
                                <option value="">Semua</option>
                                <option value="1">Solved</option>
                                <option value="0">Not Solved</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" >
                        <div class="controls" style="right: 0">
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="view" id="view" >Filter</button>
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel" name="clear" id="clear" >Clear</button>

                        </div>
                    </td>
                </tr>
            </table>

        </fieldset>
        <g:render template="dataTables" />
        <fieldset class="buttons controls" style="padding-top: 10px;">
            <button id='responss' onclick="respon();" type="button" class="btn btn-primary" style="width: 150px;">Respond Problem</button>
            <button id='wodetail' onclick="woDetail();" type="button" class="btn btn-cancel" style="width: 150px;">WO Details</button>
        </fieldset>
        <fieldset class="buttons controls" style="padding-top: 10px;">
            <button id='jpb' onclick="window.location.replace('#/JPBGRView')" type="button" class="btn btn-cancel" style="width: 100px;">Run JPB</button>
            <button id='export' onclick="exports()" type="button" class="btn btn-cancel" style="width: 100px;">Export Xls</button>
        </fieldset>

    </div>
    <div class="span7" id="respon-form" style="display: none;width:95%"></div>
</div>
</body>
</html>


