<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var problemFindingSubSubTable_${idTable};
$(function(){

var anOpen = [];
	$('#problemFinding_datatables_sub_sub_${idTable} td.subcontrol').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = problemFindingSubSubTable_${idTable}.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/problemFindingGr/subsubsublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = problemFindingSubSubTable_${idTable}.fnOpen(nTr,data,'details');
    				$('div.innerinnerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerinnerDetails', $(nTr).next()[0]).slideUp( function () {
      			problemFindingSubSubTable_${idTable}.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    $('#problemFinding_datatables_sub_sub_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( problemFindingSubSubTable_${idTable}, nEditing );
            editRow( problemFindingSubSubTable_${idTable}, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( problemFindingSubSubTable_${idTable}, nEditing );
            nEditing = null;
        }
        else {
            editRow( problemFindingSubSubTable_${idTable}, nRow );
            nEditing = nRow;
        }
    } );
    if(problemFindingSubSubTable_${idTable})
    	problemFindingSubSubTable_${idTable}.dataTable().fnDestroy();
problemFindingSubSubTable_${idTable} = $('#problemFinding_datatables_sub_sub_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": null,
	"mDataProp": "rolePengirim",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select2" aria-label="Row '+data+'" title="Select this"> <input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;' + data;
        },
        "bSortable": false,
        "sWidth":"241px",
        "bVisible": true
    },
    {
        "sName": "jpb",
        "mDataProp": "pengirim",
        "aTargets": [1],
        "bSortable": false,
        "sWidth":"153px",
        "bVisible": true
    },
    {
        "sName": "t501TglUpdate",
	    "mDataProp": "tanggalKirim",
        "aTargets": [2],
        "bSortable": false,
        "sWidth":"183px",
        "bVisible": true
    },
    {
        "sName": null,
        "mDataProp": "deskripsiMasalah",
        "aTargets": [3],
        "bSortable": false,
        "sWidth":"285px",
        "bVisible": true
    },
    {
        "sName": null ,
        "mDataProp": "tanggalSolved",
        "aTargets": [4],
        "bSortable": false,
        "sWidth":"189px",
        "bVisible": true
    },
    {
        "sName": null,
        "mDataProp": "staSolved",
        "aTargets": [5],
	    "mRender": function ( data, type, row ) {
	    if(data=="Solved"){
            return '<span style="color:green">' + data + '</span>';
	    }else{
	        return '<span><a style="color:red" href="javascript:void(0);" onclick="if(confirm(\' Are You Sure?\' )){link(\''+row['noWo']+'\',this)}">' + data + '</a></span>';
	    }
	},
        "bSortable": false,
        "sWidth":"150px",
        "bVisible": true
    }

    ],
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                            aoData.push(
                                        {"name": 'noWo', "value": "${noWo}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
    </g:javascript>
</head>
<body>
<div class="innerinnerDetails">
    <table id="problemFinding_datatables_sub_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th></th>

            <th style="border-bottom: none; padding: 5px;">
                <div>Role Pengirim</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Pengirim</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Tanggal Kirim</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Deskripsi Masalah</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Tanggal Solved</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Status Problem</div>
            </th>

        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
