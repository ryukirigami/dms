<%@ page import="com.kombos.customercomplaint.Complaint" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="parts_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 130px"><g:message code="complaint.t921TglComplain.label" default="Kode Parts" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 125px"><g:message code="complaint.t921NoReff.label" default="Nama Parts" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 115px"><g:message code="complaint.t921StaMediaKeluhan.label" default="Qty" /></div>
        </th>

		</tr>
	</thead>
</table>

<g:javascript>
var gatepassT800Table;
var reloadGatepassT800Table;
$(function(){

	reloadGatepassT800Table = function() {
		gatepassT800Table.fnDraw();
	}


	gatepassT800Table = $('#parts_datatables').dataTable({
		"sScrollX": "",
		"bScrollCollapse": false,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsGatepassT800 = $("#parts_datatables tbody .row-select");
            var jmlGatepassT800Cek = 0;
            var nRow;
            var idRec;
            rsGatepassT800.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsGatepassT800PerPage[idRec]=="1"){
                    jmlGatepassT800Cek = jmlGatepassT800Cek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsGatepassT800PerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecGatepassT800PerPage = rsGatepassT800.length;
            if(jmlGatepassT800Cek==jmlRecGatepassT800PerPage && jmlRecGatepassT800PerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "databaleparts")}",
		"aoColumns": [

{
	"sName": "kodeParts",
	"mDataProp": "kodeParts",
	"aTargets": [0],

	"bSearchable": true,
	"bSortable": true,
	"sWidth":"20px",
	"bVisible": true
}

,

{
	"sName": "namaParts",
	"mDataProp": "namaParts",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"50px",
	"bVisible": true
}

,

{
	"sName": "qty",
	"mDataProp": "qty",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"20px",
	"bVisible": true
}



],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                           aoData.push(
                                {"name":'idMasalah', "value": $('#idMasalah').val()}
                            );


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});


});
</g:javascript>


			
