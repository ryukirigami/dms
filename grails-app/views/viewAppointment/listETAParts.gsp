<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title></title>
		<r:require modules="baseapplayout" />
		<g:render template="../menu/maxLineDisplay"/>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">${label}</span>
	</div>
	<div class="box">
		<div class="span12" id="tungguETAParts_table">
			<table id="tungguETAParts_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			 <g:each in="${columns}" status="i" var="col">
				<th style="border-bottom: none;padding: 5px;">
					<div>${col.label}</div>
				</th>
			</g:each>			
		</tr>
		<tr>
			 <g:each in="${columns}" status="i" var="col">
				<th style="border-top: none;padding: 5px;">
				<g:if test="${col.type == 'date'}">
					<div id="filter_${col.field}" style="padding-top: 0px;position:relative; margin-top: 0px;width: 275px;" >
					<input type="hidden" name="searchFrom_${col.field}" value="date.struct">
					<input type="hidden" name="searchFrom_${col.field}_day" id="searchFrom_${col.field}_day" value="">
					<input type="hidden" name="searchFrom_${col.field}_month" id="searchFrom_${col.field}_month" value="">
					<input type="hidden" name="searchFrom_${col.field}_year" id="searchFrom_${col.field}_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="searchFrom_${col.field}_dp" value="" id="searchFrom_${col.field}" class="search_init" style="width: 35%;">
					-
					<input type="hidden" name="searchTo_${col.field}" value="date.struct">
					<input type="hidden" name="searchTo_${col.field}_day" id="searchTo_${col.field}_day" value="">
					<input type="hidden" name="searchTo_${col.field}_month" id="searchTo_${col.field}_month" value="">
					<input type="hidden" name="searchTo_${col.field}_year" id="searchTo_${col.field}_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="searchTo_${col.field}_dp" value="" id="searchTo_${col.field}" class="search_init" style="width: 35%;">
					</div>
				</g:if>
				<g:elseif test="${col.type != 'seq' && col.type != 'nested' && col.type != 'sex'}">
					<div id="filter_${col.field}" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
						<input type="text" name="search_${col.field}" class="search_init" />
					</div>
				</g:elseif>
				</th>
			</g:each>			
		</tr>
	</thead>
</table>
<g:javascript>
var tungguETAPartsTable;
<g:each in="${columns}" status="i" var="col">
	<g:if test="${col.type == 'date'}">
var searchTo_${col.field}_dp;
var searchFrom_${col.field}_dp;
	</g:if>
</g:each>
$(function(){
var anOpen = [];
	$('#tungguETAParts_datatables td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = tungguETAPartsTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${g.createLink(action: "tungguETAPartsSub")}',
	   			success:function(data,textStatus){
	   				var nDetailsRow = tungguETAPartsTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			tungguETAPartsTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});
    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	tungguETAPartsTable.fnDraw();
		}
	});

	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

<g:each in="${columns}" status="i" var="col">
	<g:if test="${col.type == 'date'}">
	
	searchFrom_${col.field}_dp = $('#searchFrom_${col.field}').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#searchFrom_${col.field}_day').val(newDate.getDate());
			$('#searchFrom_${col.field}_month').val(newDate.getMonth()+1);
			$('#searchFrom_${col.field}_year').val(newDate.getFullYear());
			if(!searchTo_${col.field}_dp.date.valueOf()){
				var newDate = new Date(ev.date)
				newDate.setDate(newDate.getDate() + 1);
				searchTo_${col.field}_dp.setValue(newDate);
			} else if (ev.date.valueOf() > searchTo_${col.field}_dp.date.valueOf()) {
				var newDate = new Date(ev.date)
				newDate.setDate(newDate.getDate() + 1);
				searchTo_${col.field}_dp.setValue(newDate);
			} 
			$(this).datepicker('hide');
			tungguETAPartsTable.fnDraw();

				
	}).data('datepicker');
	
	searchTo_${col.field}_dp = $('#searchTo_${col.field}').datepicker({
							onRender: function(date) {
								return date.valueOf() < searchFrom_${col.field}_dp.date.valueOf() ? 'disabled' : '';
							}
			}).on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#searchTo_${col.field}_day').val(newDate.getDate());
			$('#searchTo_${col.field}_month').val(newDate.getMonth()+1);
			$('#searchTo_${col.field}_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			tungguETAPartsTable.fnDraw();
	}).data('datepicker');
	</g:if>
</g:each>
	
tungguETAPartsTable = $('#tungguETAParts_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "tungguETAPartsDatatablesList")}",
		"aoColumns": [
<g:each in="${columns}" status="i" var="col">
{
	"sName": "${col.field}",
	"mDataProp": "${col.field}",
	"aTargets": [${i}],
	"bSearchable": false,
	<g:if test="${col.type != 'seq'}">
	"bSortable": false,
	</g:if>	
	<g:else>
	"bSortable": false,
	"sClass": "control",
    "mRender": function ( data, type, row ) {
	    return '<i class="icon-plus"></i>&nbsp;' + data;
	},
	</g:else>
	
	"sWidth":"${col.width}",
	"bVisible": true
}
<g:if test="${i < columns.size() - 1}">
,
</g:if>
</g:each>
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
<g:each in="${columns}" status="i" var="col">
<g:if test="${col.type == 'date'}">
			var ${col.field}_from = $('#searchFrom_${col.field}').val();
			var ${col.field}Day_from = $('#searchFrom_${col.field}_day').val();
			var ${col.field}Month_from = $('#searchFrom_${col.field}_month').val();
			var ${col.field}Year_from = $('#searchFrom_${col.field}_year').val();
						
			if(${col.field}_from){
				aoData.push(
					{"name": 'sCriteria_from_${col.field}', "value": "date.struct"},
					{"name": 'sCriteria_from_${col.field}_dp', "value": ${col.field}_from},
					{"name": 'sCriteria_from_${col.field}_day', "value": ${col.field}Day_from},
					{"name": 'sCriteria_from_${col.field}_month', "value": ${col.field}Month_from},
					{"name": 'sCriteria_from_${col.field}_year', "value": ${col.field}Year_from}
				);
			}
			
			var ${col.field}_to = $('#searchTo_${col.field}').val();
			var ${col.field}Day_to = $('#searchTo_${col.field}_day').val();
			var ${col.field}Month_to = $('#searchTo_${col.field}_month').val();
			var ${col.field}Year_to = $('#searchTo_${col.field}_year').val();
						
			if(${col.field}_to){
				aoData.push(
					{"name": 'sCriteria_to_${col.field}', "value": "date.struct"},
					{"name": 'sCriteria_to_${col.field}_dp', "value": ${col.field}_to},
					{"name": 'sCriteria_to_${col.field}_day', "value": ${col.field}Day_to},
					{"name": 'sCriteria_to_${col.field}_month', "value": ${col.field}Month_to},
					{"name": 'sCriteria_to_${col.field}_year', "value": ${col.field}Year_to}
				);
			}
	</g:if>
	<g:elseif test="${col.type == 'string'}">
		var ${col.field} = $('#filter_${col.field} input').val();
		if(${col.field}){
			aoData.push(
				{"name": 'sCriteria_${col.field}', "value": ${col.field}}
			);
		}
	</g:elseif>
	<g:elseif test="${col.type == 'numeric'}">
		var ${col.field} = $('#filter_${col.field} input').val();
		if(${col.field}){
			aoData.push(
				{"name": 'sCriteria_${col.field}', "value": ${col.field}}
			);
		}
	</g:elseif>
</g:each>		
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>

		</div>
	</div>
</body>
</html>
