<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title></title>
		<r:require modules="baseapplayout" />
		<g:render template="../menu/maxLineDisplay"/>
	</head>
	<body>
    <div class="innerDetails">
			<table id="${noWO}_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			 <g:each in="${columns}" status="i" var="col">
				<th style="border-bottom: none;padding: 5px;">
					<div>${col.label}</div>
				</th>
			</g:each>
		</tr>

	</thead>
</table>

        <g:javascript>

$(function(){
$('#${noWO}_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		%{--"fnInitComplete": function () {--}%
			%{--this.fnAdjustColumnSizing(true);--}%
		   %{--},--}%
		   %{--"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {--}%
			%{--return nRow;--}%
		   %{--},--}%
//		"bSort": true,
//		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 1000, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "${id}DatatablesList")}",
		"aoColumns": [
            <g:each in="${columns}" status="i" var="col">
                {
                    "sName": "${col.field}",
	                "mDataProp": "${col.field}",
	                "aTargets": [${i}],
	                "bSearchable": false,
                    "bSortable": false,
                    "sWidth":"${col.width}",
	                "bVisible": true
                }
                <g:if test="${i < columns.size() - 1}">
                    ,
                </g:if>
            </g:each>
            ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                                aoData.push(
                                            {"name": 'noWO', "value": "${noWO}"}
                            );
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
        </g:javascript>

	</div>
</body>
</html>
