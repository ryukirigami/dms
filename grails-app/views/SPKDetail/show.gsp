

<%@ page import="com.kombos.customerprofile.SPKDetail" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'SPKDetail.label', default: 'SPK Detail')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteSPKDetail;

$(function(){ 
	deleteSPKDetail=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/SPKDetail/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadSPKDetailTable();
   				expandTableLayoutSPKDetail();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-SPKDetail" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="SPKDetail"
			class="table table-bordered table-hover">
			<tbody>



            <g:if test="${SPKDetailInstance?.company}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="company-label" class="property-label"><g:message
                                code="SPKDetail.company.label" default="Company" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="company-label">
                        %{--<ba:editableValue
                                bean="${SPKDetailInstance}" field="company"
                                url="${request.contextPath}/SPKDetail/updatefield" type="text"
                                title="Enter company" onsuccess="reloadSPKDetailTable();" />--}%

                        ${SPKDetailInstance?.company?.encodeAsHTML()}
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${SPKDetailInstance?.spk}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="spk-label" class="property-label"><g:message
					code="SPKDetail.spk.label" default="Nomor SPK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="spk-label">
						%{--<ba:editableValue
								bean="${SPKDetailInstance}" field="spk"
								url="${request.contextPath}/SPKDetail/updatefield" type="text"
								title="Enter spk" onsuccess="reloadSPKDetailTable();" />--}%
							
							${SPKDetailInstance?.spk?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SPKDetailInstance?.customerVehicle}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="customerVehicle-label" class="property-label"><g:message
					code="SPKDetail.customerVehicle.label" default="VIN Code" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="customerVehicle-label">
						%{--<ba:editableValue
								bean="${SPKDetailInstance}" field="customerVehicle"
								url="${request.contextPath}/SPKDetail/updatefield" type="text"
								title="Enter customerVehicle" onsuccess="reloadSPKDetailTable();" />--}%
							
								${SPKDetailInstance?.customerVehicle?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${SPKDetailInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="SPKDetail.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${SPKDetailInstance}" field="createdBy"
								url="${request.contextPath}/SPKDetail/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadSPKDetailTable();" />--}%
							
								<g:fieldValue bean="${SPKDetailInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SPKDetailInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="SPKDetail.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${SPKDetailInstance}" field="updatedBy"
								url="${request.contextPath}/SPKDetail/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadSPKDetailTable();" />--}%
							
								<g:fieldValue bean="${SPKDetailInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SPKDetailInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="SPKDetail.lastUpdProcess.label" default="Last Update Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${SPKDetailInstance}" field="lastUpdProcess"
								url="${request.contextPath}/SPKDetail/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadSPKDetailTable();" />--}%
							
								<g:fieldValue bean="${SPKDetailInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>

				<g:if test="${SPKDetailInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="SPKDetail.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${SPKDetailInstance}" field="dateCreated"
								url="${request.contextPath}/SPKDetail/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadSPKDetailTable();" />--}%
							
								<g:formatDate date="${SPKDetailInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SPKDetailInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="SPKDetail.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${SPKDetailInstance}" field="lastUpdated"
								url="${request.contextPath}/SPKDetail/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadSPKDetailTable();" />--}%
							
								<g:formatDate date="${SPKDetailInstance?.lastUpdated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayoutSPKDetail();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${SPKDetailInstance?.id}"
					update="[success:'SPKDetail-form',failure:'SPKDetail-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>

			</fieldset>
		</g:form>
	</div>
</body>
</html>
