
<%@ page import="com.kombos.customerprofile.SPKDetail" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="SPKDetail_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover fixed table-selectable"
	width="100%" style="table-layout: fixed">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <input type="checkbox" class="pull-left select-all" aria-label="Select all" title="Select all"/>
                <div><g:message code="SPKDetail.company.label" default="Nama Perusahaan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SPKDetail.spk.label" default="Nomor SPK" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SPKDetail.customerVehicle.label" default="VIN Code" /></div>
			</th>


		
		</tr>
		<tr>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_company" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_company" class="search_init" />
                </div>
            </th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_spk" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_spk" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_customerVehicle" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_customerVehicle" class="search_init" />
				</div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var SPKDetailTable;
var reloadSPKDetailTable;
$(function(){

	var recordsSPKDetailperpage = [];//new Array();
    var anSPKDetailSelected;
    var jmlRecSPKDetailPerPage=0;
    var id;

	reloadSPKDetailTable = function() {
		SPKDetailTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	SPKDetailTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	SPKDetailTable = $('#SPKDetail_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsSPKDetail = $("#SPKDetail_datatables tbody .row-select");
            var jmlSPKDetailCek = 0;
            var nRow;
            var idRec;
            rsSPKDetail.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsSPKDetailperpage[idRec]=="1"){
                    jmlSPKDetailCek = jmlSPKDetailCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsSPKDetailperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecSPKDetailPerPage = rsSPKDetail.length;
            if(jmlSPKDetailCek==jmlRecSPKDetailPerPage && jmlRecSPKDetailPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "company",
	"mDataProp": "company",
	"aTargets": [6],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="showSPKDetail('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="editSPKDetail('+row['id']+');"><i class="icon-pencil"></i></a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "spk",
	"mDataProp": "spk",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "customerVehicle",
	"mDataProp": "customerVehicle",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var spk = $('#filter_spk input').val();
						if(spk){
							aoData.push(
									{"name": 'sCriteria_spk', "value": spk}
							);
						}
	
						var customerVehicle = $('#filter_customerVehicle input').val();
						if(customerVehicle){
							aoData.push(
									{"name": 'sCriteria_customerVehicle', "value": customerVehicle}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var company = $('#filter_company input').val();
						if(company){
							aoData.push(
									{"name": 'sCriteria_company', "value": company}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#SPKDetail_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsSPKDetailperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsSPKDetailperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#SPKDetail_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsSPKDetailperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anSPKDetailSelected = SPKDetailTable.$('tr.row_selected');
            if(jmlRecSPKDetailPerPage == anSPKDetailSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsSPKDetailperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
