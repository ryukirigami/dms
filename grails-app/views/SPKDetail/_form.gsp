<%@ page import="com.kombos.customerprofile.SPKDetail" %>

<g:javascript>

var cekKolektifCompany;
var cekKolektifSPK;
		$(function(){
			$('#customerVehicle').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/SPKDetail/getCustomerVehicle', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});

			selectNamaAlamatSTNK();

			cekKolektifCompany = function(){
			    var checked = $("#kolektifCompany").is(":checked");

			    if(checked){
			        $("#company").attr("readonly",true);
			    }else{
			        $("#company").attr("readonly",false);
			    }

			}

			cekKolektifSPK = function(){
                var checked = $("#kolektifSPK").is(":checked");

			    if(checked){
			        $("#spk").attr("readonly",true);
			    }else{
			        $("#spk").attr("readonly",false);
			    }
			}

		});
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: SPKDetailInstance, field: 'company', 'error')} required">
    <label class="control-label" for="company">
        <g:message code="SPKDetail.company.label" default="Nama Perusahaan" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:hiddenField name="spkDetail.id" id="spkDetail.id" value="${SPKDetailInstance?.id}"/>
        <g:select id="company" name="company.id"  from="${com.kombos.maintable.Company.list()}" optionKey="id" required="" optionValue="namaPerusahaan" value="${SPKDetailInstance?.company?.id}" class="many-to-one"/> <g:checkBox style="position:relative;margin-bottom: 6px"  name="kolektifCompany" id="kolektifCompany" onchange="cekKolektifCompany()"/> Kolektif
    </div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: SPKDetailInstance, field: 'spk', 'error')} required">
	<label class="control-label" for="spk">
		<g:message code="SPKDetail.spk.label" default="Spk" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="spk" name="spk.id" from="${com.kombos.customerprofile.SPK.createCriteria().list{eq("staDel","0");eq("companyDealer",session?.userCompanyDealer)}}" optionKey="id" required="" value="${SPKDetailInstance?.spk?.id}" class="many-to-one"/> <g:checkBox name="kolektifSPK" style="position:relative;margin-bottom: 6px"  id="kolektifSPK" onblur="cekKolektifSPK();"/>  Kolektif
	</div>
</div>

<div>
	<label class="control-label"  >
		<g:message code="SPKDetail.customerVehicle.label" default="Customer Vehicle" />
	<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="customerVehicle" id="customerVehicle"  value="${SPKDetailInstance?.customerVehicle?.t103VinCode}" class="typeahead"  autocomplete="off" maxlength="220" onblur="selectNamaAlamatSTNK();" />
    </div>
</div>

<div>
    <label class="control-label" >
        <g:message code="SPKDetail.customerVehicle.label" default="Nama STNK" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField readonly="true" id="namaSTNK" name="namaSTNK"  value="" class="many-to-one"/>
    </div>
</div>

<div>
    <label class="control-label" >
        <g:message code="SPKDetail.customerVehicle.label" default="Alamat STNK" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField id="alamatSTNK" name="alamatSTNK" readonly="true" value="" class="many-to-one"/>
    </div>
</div>




