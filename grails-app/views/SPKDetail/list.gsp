
<%@ page import="com.kombos.customerprofile.SPKDetail" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'SPKDetail.label', default: 'SPK Detail')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>


		<g:javascript disposition="head">
	var showSPKDetail;
	var loadFormSPKDetail;
	var shrinkTableLayoutSPKDetail;
	var expandTableLayoutSPKDetail;
	var editSPKDetail;
	var showProgress;
	var hideProgerss;
	$(function(){

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();


	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE2_' :
         		shrinkTableLayoutSPKDetail();
         		<g:remoteFunction action="create"
		onLoading="showProgress();"
		onSuccess="loadFormSPKDetail(data, textStatus);"
		onComplete="hideProgress();" />
         		break;
         	case '_DELETE2_' :
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			}).css(
        {
            'margin-top': function () {
                return -($(this).height() / 2);
            },
            'margin-left': function () {
                return -($(this).width() / 2);
            }
        })


         		break;
       }
       return false;
	});

    showSPKDetail = function(id) {
    	shrinkTableLayoutSPKDetail();
           showProgress();
   		$.ajax({type:'POST', url:'${request.contextPath}/SPKDetail/show/'+id,
   			success:function(data,textStatus){
   				loadFormSPKDetail(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				hideProgress();
   			}
   		});
    };

    editSPKDetail = function(id) {
    	shrinkTableLayoutSPKDetail();
        showProgress();
   		$.ajax({type:'POST', url:'${request.contextPath}/SPKDetail/edit/'+id,
   			success:function(data,textStatus){
   				loadFormSPKDetail(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				hideProgress();
   			}
   		});
    };

    loadFormSPKDetail = function(data, textStatus){
		$('#SPKDetail-form').empty();
    	$('#SPKDetail-form').append(data);
   	}
    
    shrinkTableLayoutSPKDetail = function(){
    	if($("#SPKDetail-table").hasClass("span12")){
   			$("#SPKDetail-table").toggleClass("span12 span5");
        }
        $("#SPKDetail-form").css("display","block"); 
   	}
   	
   	expandTableLayoutSPKDetail = function(){
   		if($("#SPKDetail-table").hasClass("span5")){
   			$("#SPKDetail-table").toggleClass("span5 span12");
   		}
        $("#SPKDetail-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#SPKDetail-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/SPKDetail/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadSPKDetailTable();
    		}
		});
		
   	}

    showProgress = function(){
        $('#spinner2').modal('show').css(
        {
            'margin-top': function () {
                return -($(this).height() / 2);
            },
            'margin-left': function () {
                return -($(this).width() / 2);
            },
             'width' : '500px'

        });
    }

   hideProgress = function(){
        $('#spinner2').modal('hide').css(
        {
            'margin-top': function () {
                return -($(this).height() / 2);
            },
            'margin-left': function () {
                return -($(this).width() / 2);
            },
             'width' : '500px'

        });
    }


});

 function selectNamaAlamatSTNK() {
            var customerVehicle = $("#customerVehicle").val();
         //   alert("Hallo");
            if (customerVehicle != null) {
                jQuery.getJSON('${request.contextPath}/SPKDetail/getNamaAlamatSTNK?customerVehicle=' + customerVehicle, function (data) {
                    $('#namaSTNK').empty();
                    $('#alamatSTNK').empty();

                    if (data) {
                        jQuery.each(data, function (index, value) {
                            $('#namaSTNK').val(data.namaSTNK );
                            $('#alamatSTNK').val(data.alamatSTNK);

                        });

                    }
                });
            }
        }


</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE2_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
            <li><a class="pull-right box-action" href="#"
                   style="display: block;" target="_DELETE2_">&nbsp;&nbsp;<i
                        class="icon-remove"></i>&nbsp;&nbsp;
            </a></li>
			%{--<li class="separator"></li>--}%
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="SPKDetail-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="SPKDetail-form" style="display: none;"></div>
	</div>
    <div class="modal hide" id="spinner2" data-backdrop="static" data-keyboard="false">
        <div class="modal-header">
            <h1>Processing...</h1>
        </div>
        <div class="modal-body">
            <div class="progress progress-striped active">
                <div class="bar" style="width: 100%;"></div>
            </div>
        </div>
    </div>
</body>

</html>
