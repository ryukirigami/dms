
<%@ page import="com.kombos.customerprofile.Agama" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'agama.label', default: 'Agama')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteAgama;

$(function(){
	deleteAgama=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/agama/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadAgamaTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-agama" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="agama"
			class="table table-bordered table-hover">
			<tbody>


				<g:if test="${agamaInstance?.m061ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m061ID-label" class="property-label"><g:message
					code="agama.m061ID.label" default="Kode Agama" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m061ID-label">
						%{--<ba:editableValue
								bean="${agamaInstance}" field="m061ID"
								url="${request.contextPath}/Agama/updatefield" type="text"
								title="Enter m061ID" onsuccess="reloadAgamaTable();" />--}%

								<g:fieldValue bean="${agamaInstance}" field="m061ID"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${agamaInstance?.m061NamaAgama}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m061NamaAgama-label" class="property-label"><g:message
					code="agama.m061NamaAgama.label" default="Nama Agama" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m061NamaAgama-label">
						%{--<ba:editableValue
								bean="${agamaInstance}" field="m061NamaAgama"
								url="${request.contextPath}/Agama/updatefield" type="text"
								title="Enter m061NamaAgama" onsuccess="reloadAgamaTable();" />--}%

								<g:fieldValue bean="${agamaInstance}" field="m061NamaAgama"/>

						</span></td>

				</tr>
				</g:if>

				%{--<g:if test="${agamaInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="agama.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${agamaInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/Agama/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadAgamaTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${agamaInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

            <g:if test="${agamaInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="agama.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">

                        <g:fieldValue bean="${agamaInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${agamaInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="agama.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">

                        <g:formatDate date="${agamaInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${agamaInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="agama.createdBy.label" default="Created By" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">

								<g:fieldValue bean="${agamaInstance}" field="createdBy"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${agamaInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="agama.lastUpdated.label" default="Last Updated" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">

								<g:formatDate date="${agamaInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

						</span></td>

				</tr>
				</g:if>

            <g:if test="${agamaInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="agama.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">

                        <g:fieldValue bean="${agamaInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            </tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${agamaInstance?.id}"
					update="[success:'agama-form',failure:'agama-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteAgama('${agamaInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
