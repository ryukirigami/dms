<%@ page import="com.kombos.customerprofile.Agama" %>


<g:if test="${agamaInstance?.m061ID}">
<div class="control-group fieldcontain ${hasErrors(bean: agamaInstance, field: 'm061ID', 'error')} required">
	<label class="control-label" for="m061ID">
		<g:message code="agama.m061ID.label" default="Kode Agama" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	${agamaInstance?.m061ID}
	</div>
</div>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: agamaInstance, field: 'm061NamaAgama', 'error')} required">
	<label class="control-label" for="m061NamaAgama">
		<g:message code="agama.m061NamaAgama.label" default="Nama Agama" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m061NamaAgama" maxlength="20" required="" value="${agamaInstance?.m061NamaAgama}"/>
	</div>
</div>

<g:javascript>
    document.getElementById("m061NamaAgama").focus();
</g:javascript>