<%--
  Created by IntelliJ IDEA.
  User: Mohammad Fauzi
  Date: 3/23/14
  Time: 8:19 PM
--%>

<%@ page import="com.kombos.maintable.JenisInvitation; com.kombos.maintable.JenisReminder; com.kombos.administrasi.ModelName; com.kombos.maintable.Company; com.kombos.maintable.Nikah; com.kombos.customerprofile.Agama; com.kombos.customerprofile.Hobby; com.kombos.maintable.Retention" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Send SMS Retention</title>
    <r:require modules="baseapplayout"/>
    <style>
    #spinner {
        z-index: 1000;
    }

    legend {
        display: block;
        width: 100%;
        padding: 0;
        /* margin-bottom: 20px; */
        font-size: 21px;
        line-height: 20px;
        color: #333333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    td {
        padding-bottom: 0px;
        padding-left: 5px;
        font-size: x-small;
    }
    </style>


    <g:javascript disposition="head">
        $(function () {


            completeProcess = function () {

            }

            jQuery("#buttonUpdate").click(function (event) {
                var invitationID = document.getElementById("invitationID").value;
                var t202StaLangsungAppointment = document.getElementById("t202StaLangsungAppointment").value;

                if(invitationID == '-1' && t202StaLangsungAppointment == "0"){
                    toastr.error('Pilih salah satu feedback');
                    return
                }

                if(invitationID == '-1' && t202StaLangsungAppointment == "1"){
                    toastr.error('Pilih salah satu feedback');
                    return
                }

                if(t202StaLangsungAppointment == '-1'){
                    toastr.warning('Pilih salah satu status Appointment');
                    return
                }
                $.ajax({
                    url:'${request.contextPath}/customerFeedback/updateInvitation',
                    type: "POST", // Always use POST when deleting data
                    data : { invitationID: invitationID, t202StaLangsungAppointment: t202StaLangsungAppointment },
                    success : function(data){
                        reloadHistoryCustomerTable();
                        alert('Berhasil diupdate');
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                    }
                });
                event.preventDefault(); //to stop the default loading
            });

            jQuery("#buttonSearch").click(function (event) {
                reloadHistoryCustomerTable();
                event.preventDefault(); //to stop the default loading
            });

            jQuery("#buttonClearSearch").click(function (event) {

                var inputs, index;

                inputs = document.getElementsByTagName('input');
                for (index = 0; index < inputs.length; ++index) {
                    inputs[index].value = '';
                    inputs[index].checked = false;
                }
                reloadHistoryCustomerTable();
                event.preventDefault(); //to stop the default loading
            });

            prepareUpdateFeedback = function(id){
                $.ajax({
                    url:'${request.contextPath}/customerFeedback/loadInvitation',
                    type: "POST", // Always use POST when deleting data
                    data : { id: id },
                    success : function(data){
                        document.getElementById("NamaCustomer").innerHTML = data.NamaCustomer;
                        document.getElementById("TanggalInvite").innerHTML = data.TanggalInvite;
                        document.getElementById("TanggalFeedback").innerHTML = data.TanggalFeedback;
                        document.getElementById("MetodeInvitation").innerHTML = data.MetodeInvitation;
                        document.getElementById("t202StaLangsungAppointment").value = data.t202StaLangsungAppointment;
                        document.getElementById("balasanCustomer").value = data.balasanCustomer;
                        document.getElementById("invitationID").value = data.invitationID;
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                    }
                });
            }


            doResponse = function (data) {
                jQuery('#spinner').fadeOut();
                if (data.status == 'OK') {
                    alert("success");
                } else {
                    alert('Fail\n' + data.error);
                }

            }

        });

    </g:javascript>
</head>

<body>

<g:formRemote name="form" id="form" on404="alert('not found!')" update="updateMe"
              method="post"
              onComplete="completeProcess()"
              onSuccess="doResponse(data)"
              url="[controller: 'customerFeedback', action: 'doSave']">

    <g:hiddenField name="invitationID" id="invitationID" value="-1"/>

    <div class="box">
        <legend style="font-size: small">Search Criteria:</legend>
        <table style="width: 100%;">
            <tr>
                <td style="vertical-align: top">
                    <div class="box">
                        <table style="width: 100%;">
                            <tr>
                                <td>Tanggal Invitation</td>
                                <td>
                                    <g:checkBox name="checkTanggalInvitation"/>
                                    <ba:datePicker name="tanggalInvitationStart" format="dd-MM-yyyy"/>
                                    s.d
                                    <ba:datePicker name="tanggalInvitationEnd" format="dd-MM-yyyy"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Tanggal Feedback</td>
                                <td>
                                    <g:checkBox name="checkTanggalFeedback"/>
                                    <ba:datePicker name="tanggalFeedbackStart" format="dd-MM-yyyy"/>
                                    s.d
                                    <ba:datePicker name="tanggalFeedbackEnd" format="dd-MM-yyyy"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Metode Invitation</td>
                                <td>
                                    <g:each in="${JenisReminder.listOrderByM201Id()}">
                                        <input name="searchReminder" id="searchReminder_${it?.id}" type="checkbox"
                                               value="${it?.id}" checked> ${it?.m201NamaJenisReminder}
                                    </g:each>
                                </td>
                            </tr>
                            <tr>
                                <td>Tipe Invitation</td>
                                <td>
                                    <g:each in="${JenisInvitation.listOrderByM204JenisInvitation()}">
                                        <g:if test="${it?.m204JenisInvitation?.equalsIgnoreCase("Email") || it?.m204JenisInvitation?.equalsIgnoreCase("SMS")}">
                                            <input name="searchMethod" id="searchMethod_${it?.id}" type="checkbox"
                                                   value="${it?.id}" checked> ${it?.m204JenisInvitation}
                                        </g:if>
                                    </g:each>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td style="vertical-align: top;padding-left: 30px;">
                    <div class="box">
                        <table style="width: 100%;">
                            <tr>
                                <td style="vertical-align: top">Status Feedback</td>
                                <td style="vertical-align: top">
                                    <table>

                                        <g:each in="${statusPencarianFeedback}">
                                            <tr>
                                                <td>
                                                    %{--<g:checkBox name="status${it.id}" value="${it.id}"/>--}%
                                                    <input checked type="checkbox" id="status${it.id}" name="status"
                                                           value="${it.id}">
                                                </td>
                                                <td>
                                                    <label for="status${it.id}">${it.nama}</label>
                                                </td>
                                            </tr>
                                        </g:each>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">

                    <fieldset class="buttons controls" style="text-align: right;">

                        <button class="btn btn-primary" id="buttonSearch">Search</button>

                        <button class="btn btn-primary" id="buttonClearSearch">Clear Search</button>

                    </fieldset>

                </td>
            </tr>
        </table>
    </div>

    <div class="box">
        <legend style="font-size: small">Search Result:</legend>
        <g:render template="dataTables"/>
    </div>

    <div class="box">
        <legend style="font-size: small">Customer Feedback:</legend>
        <table style="width: 100%;border: 0px;">
            <tr>
                <td style="vertical-align: top">Balasan Customer</td>
                <td style="vertical-align: top">:</td>
                <td style="width: 50%;vertical-align: top">
                    %{--<g:textArea style="height:100px; width: 100%;" readonly="readonly" id="balasanCustomer"--}%
                                %{--name="balasanCustomer"/>--}%
                    <g:textArea style="height:100px; width: 100%;" id="balasanCustomer"
                                name="balasanCustomer"/>
                </td>
                <td style="width: 40%;vertical-align: top;padding-left: 40px;">
                    <div class="box">
                        <table>
                            <tr>
                                <td>
                                    Nama Customer
                                </td>
                                <td>:</td>
                                <td id="NamaCustomer">

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Tanggal Invite
                                </td>
                                <td>:</td>
                                <td id="TanggalInvite">

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Tanggal Feedback
                                </td>
                                <td>:</td>
                                <td id="TanggalFeedback">

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Metode Invitation
                                </td>
                                <td>:</td>
                                <td id="MetodeInvitation">

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Setuju Appointment
                                </td>
                                <td>:</td>
                                <td>
                                    <g:select id="t202StaLangsungAppointment" name="t202StaLangsungAppointment"
                                              from="${[[id: "0", nama: "Tidak"], [id: "1", nama: "Ya"]]}"
                                              optionValue="nama" optionKey="id"
                                              noSelection="${['-1': "-"]}"/>
                                    <button class="btn btn-primary" id="buttonUpdate">Update</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>

        </table>
    </div>

    <fieldset class="buttons controls" style="text-align: right;">

        <button class="btn btn-primary" id="buttonClose" onclick="window.location.replace('#/home');">Close</button>

    </fieldset>

</g:formRemote>
</body>
</html>