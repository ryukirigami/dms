
<%@ page import="com.kombos.customerprofile.HistoryCustomer" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="historyCustomer_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>


        <th style="border-bottom: none;padding: 5px;">
            <div>Tanggal Invite</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Customer</div>
        </th>


        <th style="border-bottom: none;padding: 5px;width: 40%;">
            <div>Nomor Polisi</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Telepon</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Telepon Seluler</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Tanggal Feedback</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Tipe</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Metode</div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div>Status Feedback</div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div>Appointment</div>
        </th>

    </tr>

    </thead>
</table>

<g:javascript>
var historyCustomerTable;
var reloadHistoryCustomerTable;
$(function(){




	reloadHistoryCustomerTable = function() {
		historyCustomerTable.fnDraw();
	}




	historyCustomerTable = $('#historyCustomer_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [




{
	"sName": "t202TglJamKirim",
	"mDataProp": "t202TglJamKirim",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input onclick="prepareUpdateFeedback(this.value);" name="customer" value="'+row['id']+'" type="radio" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"/>&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "customer",
	"mDataProp": "customer",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "nopol",
	"mDataProp": "nopol",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "telp",
	"mDataProp": "telp",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "hp",
	"mDataProp": "hp",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tglFeedback",
	"mDataProp": "tglFeedback",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tipe",
	"mDataProp": "tipe",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "metode",
	"mDataProp": "metode",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "status",
	"mDataProp": "status",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}


,


{
	"sName": "app",
	"mDataProp": "app",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        var searchReminder = document.getElementsByName('searchReminder');
                        var checkRetreiveValues = new Array()
                        for(var i = 0; i < searchReminder.length; i++){
                            if(searchReminder[i].checked){
                                checkRetreiveValues.push(searchReminder[i].value)
                            }
                        }
                        aoData.push(
                            {"name": 'searchReminder', "value": checkRetreiveValues}
                        );


                        var status = document.getElementsByName('status');
                        checkRetreiveValues = new Array()
                        for(var i = 0; i < status.length; i++){
                            if(status[i].checked){
                                checkRetreiveValues.push(status[i].value)
                            }
                        }
                        aoData.push(
                            {"name": 'status', "value": checkRetreiveValues}
                        );


                        var searchMethod = document.getElementsByName('searchMethod');
                        checkRetreiveValues = new Array()
                        for(var i = 0; i < searchMethod.length; i++){
                            if(searchMethod[i].checked){
                                checkRetreiveValues.push(searchMethod[i].value)
                            }
                        }
                        aoData.push(
                            {"name": 'searchMethod', "value": checkRetreiveValues}
                        );




                        if(document.getElementById('checkTanggalInvitation').checked == true){
                            var tanggalInvitationStart = $('#tanggalInvitationStart').val();
                            var tanggalInvitationStartDay = $('#tanggalInvitationStart_day').val();
                            var tanggalInvitationStartMonth = $('#tanggalInvitationStart_month').val();
                            var tanggalInvitationStartYear = $('#tanggalInvitationStart_year').val();

                            if(tanggalInvitationStart){
                                aoData.push(
                                        {"name": 'tanggalInvitationStart', "value": "date.struct"},
                                        {"name": 'tanggalInvitationStart_dp', "value": tanggalInvitationStart},
                                        {"name": 'tanggalInvitationStart_day', "value": tanggalInvitationStartDay},
                                        {"name": 'tanggalInvitationStart_month', "value": tanggalInvitationStartMonth},
                                        {"name": 'tanggalInvitationStart_year', "value": tanggalInvitationStartYear}
                                );
                            }

                            var tanggalInvitationEnd = $('#tanggalInvitationEnd').val();
                            var tanggalInvitationEndDay = $('#tanggalInvitationEnd_day').val();
                            var tanggalInvitationEndMonth = $('#tanggalInvitationEnd_month').val();
                            var tanggalInvitationEndYear = $('#tanggalInvitationEnd_year').val();

                            if(tanggalInvitationEnd){
                                aoData.push(
                                        {"name": 'tanggalInvitationEnd', "value": "date.struct"},
                                        {"name": 'tanggalInvitationEnd_dp', "value": tanggalInvitationEnd},
                                        {"name": 'tanggalInvitationEnd_day', "value": tanggalInvitationEndDay},
                                        {"name": 'tanggalInvitationEnd_month', "value": tanggalInvitationEndMonth},
                                        {"name": 'tanggalInvitationEnd_year', "value": tanggalInvitationEndYear}
                                );
                            }
						}


						if(document.getElementById('checkTanggalFeedback').checked == true){
                            var tanggalFeedbackStart = $('#tanggalFeedbackStart').val();
                            var tanggalFeedbackStartDay = $('#tanggalFeedbackStart_day').val();
                            var tanggalFeedbackStartMonth = $('#tanggalFeedbackStart_month').val();
                            var tanggalFeedbackStartYear = $('#tanggalFeedbackStart_year').val();

                            if(tanggalFeedbackStart){
                                aoData.push(
                                        {"name": 'tanggalFeedbackStart', "value": "date.struct"},
                                        {"name": 'tanggalFeedbackStart_dp', "value": tanggalFeedbackStart},
                                        {"name": 'tanggalFeedbackStart_day', "value": tanggalFeedbackStartDay},
                                        {"name": 'tanggalFeedbackStart_month', "value": tanggalFeedbackStartMonth},
                                        {"name": 'tanggalFeedbackStart_year', "value": tanggalFeedbackStartYear}
                                );
                            }

                            var tanggalFeedbackEnd = $('#tanggalFeedbackEnd').val();
                            var tanggalFeedbackEndDay = $('#tanggalFeedbackEnd_day').val();
                            var tanggalFeedbackEndMonth = $('#tanggalFeedbackEnd_month').val();
                            var tanggalFeedbackEndYear = $('#tanggalFeedbackEnd_year').val();

                            if(tanggalFeedbackEnd){
                                aoData.push(
                                        {"name": 'tanggalFeedbackEnd', "value": "date.struct"},
                                        {"name": 'tanggalFeedbackEnd_dp', "value": tanggalFeedbackEnd},
                                        {"name": 'tanggalFeedbackEnd_day', "value": tanggalFeedbackEndDay},
                                        {"name": 'tanggalFeedbackEnd_month', "value": tanggalFeedbackEndMonth},
                                        {"name": 'tanggalFeedbackEnd_year', "value": tanggalFeedbackEndYear}
                                );
                            }
						}




$.ajax({ "dataType": 'json',
    "type": "POST",
    "url": sSource,
    "data": aoData ,
    "success": function (json) {
        fnCallback(json);
       },
    "complete": function () {
       }
});
}
});
});
</g:javascript>



