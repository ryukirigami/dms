
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="partsAging_datatables" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th></th>
        <th style="border-bottom: none;padding: 5px;">
            Tanggal Receive
        </th>

        <th style="border-bottom: none;padding: 5px;">
            Qty
        </th>

        <th style="border-bottom: none;padding: 5px;">
            Qty Used
        </th>

        <th style="border-bottom: none;padding: 5px;">
            Qty Sisa
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var partsAgingTable;
var reloadPartsAgingTable;
$(function(){
    var anOpen = [];
    $('#partsAging_datatables td.control').live('click',function () {
        var nTr = this.parentNode;
        var i = $.inArray( nTr, anOpen );
        if ( i === -1 ) {
            $('i', this).attr( 'class', "icon-minus" );
            var oData = partsAgingTable.fnGetData(nTr);
            $('#spinner').fadeIn(1);
            $.ajax({type:'POST',
                data : oData,
                url:'${request.contextPath}/partsAging/subList',
                success:function(data,textStatus){
                    var nDetailsRow = partsAgingTable.fnOpen(nTr,data,'details');
                    $('div.innerDetails', nDetailsRow).slideDown();
                    anOpen.push( nTr );
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });

        } else {
            $('i', this).attr( 'class', 'icon-plus' );
            $('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
                partsAgingTable.fnClose( nTr );
                anOpen.splice( i, 1 );
            });
        }
    });

	reloadPartsAgingTable = function() {
		partsAgingTable.fnDraw();
	}


    partsAgingTable = $('#partsAging_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"bDestroy": true,
		"aoColumns": [
{
   "mDataProp": null,
   "sClass": "control center",
   "bSortable": false,
   "sWidth":"10px",
   "sDefaultContent": '<i class="icon-plus"></i>'
}
,
{
	"sName": "tanggalReceive",
	"mDataProp": "tanggalReceive",
	"aTargets": [0],
	"bSortable": false,
	"sWidth":"15%",
	"bVisible": true
}
,
{
	"sName": "qty",
	"mDataProp": "qty",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"20%",
	"bVisible": true
}
,
{
	"sName": "qtyUsed",
	"mDataProp": "qtyUsed",
	"bSortable": false,
	"sWidth":"35%",
	"bVisible": true
}
,
{
	"sName": "qtySisa",
	"mDataProp": "qtySisa",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"15%",
	"bVisible": true
}
],
    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
        var goods = $('#goods').val();
        if(goods){
            aoData.push(
                    {"name": 'sCriteria_goods', "value": goods}
            );
        }

        var tanggalAwal = $('#tanggalAwal').val();
        if(tanggalAwal){
            aoData.push(
                    {"name": 'sCriteria_tanggalAwal', "value": tanggalAwal}
            );
        }

        var tanggalAkhir = $('#tanggalAkhir').val();
        if(tanggalAkhir){
            aoData.push(
                    {"name": 'sCriteria_tanggalAkhir', "value": tanggalAkhir}
            );
        }


    $.ajax({ "dataType": 'json',
        "type": "POST",
        "url": sSource,
        "data": aoData ,
        "success": function (json) {
            fnCallback(json);
           },
        "complete": function () {
           }
    });
}
});
});

</g:javascript>
