<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 31/01/15
  Time: 13:53
--%>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
    var partsFifoSubTable;
$(function(){

	partsFifoSubTable = $('#partsFifo_datatables_sub_${idTable}').dataTable({
		"sScrollX": "1246px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"10px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "sClass": "subcontrol center",
   "bSortable": false,
   "sWidth":"10px",
   "sDefaultContent": ''
},
{
	"sName": "noWO",
	"mDataProp": "noWO",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"367px",
	"bVisible": true
}
,

{
	"sName": "tanggal",
	"mDataProp": "tanggal",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"394px",
	"bVisible": true
}
,
{
	"sName": "qty",
	"mDataProp": "qty",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"393px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						aoData.push(
									{"name": 'idDetail', "value": "${idDetail}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
    </g:javascript>

</head>
<body>
<div class="innerDetails">
    <table id="partsFifo_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>

            <th></th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="wo.t401NoWo.label" default="Nomor WO" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="wo.tanggalWO.label" default="Tanggal Dikeluarkan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="qty" default="Qty" /></div>
            </th>

        </tr>
        </thead>
    </table>

</div>
</body>
</html>