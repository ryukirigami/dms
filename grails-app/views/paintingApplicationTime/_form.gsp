<%@ page import="com.kombos.administrasi.MasterPanel; com.kombos.administrasi.PaintingApplicationTime" %>
<r:require modules="autoNumeric" />
<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init',{
            vMin:'0',
            aSep:'',
            mDec:''
        });
    });
</script>


<div class="control-group fieldcontain ${hasErrors(bean: paintingApplicationTimeInstance, field: 'm044TglBerlaku', 'error')} required">
	<label class="control-label" for="m044TglBerlaku">
		<g:message code="paintingApplicationTime.m044TglBerlaku.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="m044TglBerlaku" id="m044TglBerlaku" precision="day"  value="${paintingApplicationTimeInstance?.m044TglBerlaku}" format="dd/MM/yyyy" required="true" />
	</div>
</div>
<g:javascript>
    document.getElementById('m044TglBerlaku').focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: paintingApplicationTimeInstance, field: 'masterPanel', 'error')} required">
	<label class="control-label" for="masterPanel">
		<g:message code="paintingApplicationTime.masterPanel.label" default="Panel" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="masterPanel" name="masterPanel.id" from="${MasterPanel.list()}" optionKey="id" required="" value="${paintingApplicationTimeInstance?.masterPanel?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: paintingApplicationTimeInstance, field: 'm044NewMultiple', 'error')} required">
	<label class="control-label" for="m044NewMultiple">
		<g:message code="paintingApplicationTime.m044NewMultiple.label" default="New Part Multiple Panel" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m044NewMultiple" class="auto" value="${fieldValue(bean: paintingApplicationTimeInstance, field: 'm044NewMultiple')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: paintingApplicationTimeInstance, field: 'm044NewUnit', 'error')} required">
	<label class="control-label" for="m044NewUnit">
		<g:message code="paintingApplicationTime.m044NewUnit.label" default="New Part Unit Panel" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m044NewUnit" class="auto" value="${fieldValue(bean: paintingApplicationTimeInstance, field: 'm044NewUnit')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: paintingApplicationTimeInstance, field: 'm044RepMultiple', 'error')} required">
	<label class="control-label" for="m044RepMultiple">
		<g:message code="paintingApplicationTime.m044RepMultiple.label" default="Repaired Multiple Panel" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m044RepMultiple" class="auto" value="${fieldValue(bean: paintingApplicationTimeInstance, field: 'm044RepMultiple')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: paintingApplicationTimeInstance, field: 'm044RepUnit', 'error')} required">
	<label class="control-label" for="m044RepUnit">
		<g:message code="paintingApplicationTime.m044RepUnit.label" default="Repaired Unit Panel" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m044RepUnit" class="auto" value="${fieldValue(bean: paintingApplicationTimeInstance, field: 'm044RepUnit')}" required=""/>
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: paintingApplicationTimeInstance, field: 'companyDealer', 'error')} ">
	<label class="control-label" for="companyDealer">
		<g:message code="paintingApplicationTime.companyDealer.label" default="Company Dealer" />
		
	</label>
	<div class="controls">
	<g:select id="companyDealer" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" value="${paintingApplicationTimeInstance?.companyDealer?.id}" class="many-to-one" noSelection="['null': '']"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: paintingApplicationTimeInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="paintingApplicationTime.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${paintingApplicationTimeInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: paintingApplicationTimeInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="paintingApplicationTime.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${paintingApplicationTimeInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: paintingApplicationTimeInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="paintingApplicationTime.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${paintingApplicationTimeInstance?.lastUpdProcess}"/>
	</div>
</div>
--}%
