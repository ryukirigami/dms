
<%@ page import="com.kombos.administrasi.PaintingApplicationTime" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
    jQuery(function($) {
        $('.auto').autoNumeric('init',{
            vMin:'0',
            aSep:'',
            mDec:''
        });
    });
</g:javascript>

<table id="paintingApplicationTime_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="paintingApplicationTime.m044TglBerlaku.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="paintingApplicationTime.masterPanel.label" default="Panel" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="paintingApplicationTime.m044NewMultiple.label" default="NPMP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="paintingApplicationTime.m044NewUnit.label" default="NPUP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="paintingApplicationTime.m044RepMultiple.label" default="RMP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="paintingApplicationTime.m044RepUnit.label" default="RUP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="paintingApplicationTime.companyDealer.label" default="Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="paintingApplicationTime.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="paintingApplicationTime.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="paintingApplicationTime.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
		<tr>
		

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m044TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m044TglBerlaku" value="date.struct">
					<input type="hidden" name="search_m044TglBerlaku_day" id="search_m044TglBerlaku_day" value="">
					<input type="hidden" name="search_m044TglBerlaku_month" id="search_m044TglBerlaku_month" value="">
					<input type="hidden" name="search_m044TglBerlaku_year" id="search_m044TglBerlaku_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m044TglBerlaku_dp" value="" id="search_m044TglBerlaku" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_masterPanel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_masterPanel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m044NewMultiple" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text"  name="search_m044NewMultiple" class="search_init auto" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m044NewUnit" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text"  name="search_m044NewUnit" class="search_init auto" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m044RepMultiple" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text"  name="search_m044RepMultiple" class="search_init auto" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m044RepUnit" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text"  name="search_m044RepUnit" class="search_init auto" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var paintingApplicationTimeTable;
var reloadPaintingApplicationTimeTable;
$(function(){
	
	reloadPaintingApplicationTimeTable = function() {
		paintingApplicationTimeTable.fnDraw();
	}

    var recordspaintingApplicationTimeperpage = [];
    var anPaintingApplicationTimeSelected;
    var jmlRecPaintingApplicationTimePerPage=0;
    var id;

	$('#search_m044TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m044TglBerlaku_day').val(newDate.getDate());
			$('#search_m044TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m044TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			paintingApplicationTimeTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	paintingApplicationTimeTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	paintingApplicationTimeTable = $('#paintingApplicationTime_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsPaintingApplicationTime = $("#paintingApplicationTime_datatables tbody .row-select");
            var jmlPaintingApplicationTimeCek = 0;
            var nRow;
            var idRec;
            rsPaintingApplicationTime.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordspaintingApplicationTimeperpage[idRec]=="1"){
                    jmlPaintingApplicationTimeCek = jmlPaintingApplicationTimeCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspaintingApplicationTimeperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPaintingApplicationTimePerPage = rsPaintingApplicationTime.length;
            if(jmlPaintingApplicationTimeCek==jmlRecPaintingApplicationTimePerPage && jmlRecPaintingApplicationTimePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m044TglBerlaku",
	"mDataProp": "m044TglBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "masterPanel",
	"mDataProp": "masterPanel",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m044NewMultiple",
	"mDataProp": "m044NewMultiple",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m044NewUnit",
	"mDataProp": "m044NewUnit",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m044RepMultiple",
	"mDataProp": "m044RepMultiple",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m044RepUnit",
	"mDataProp": "m044RepUnit",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m044TglBerlaku = $('#search_m044TglBerlaku').val();
						var m044TglBerlakuDay = $('#search_m044TglBerlaku_day').val();
						var m044TglBerlakuMonth = $('#search_m044TglBerlaku_month').val();
						var m044TglBerlakuYear = $('#search_m044TglBerlaku_year').val();
						
						if(m044TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_m044TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m044TglBerlaku_dp', "value": m044TglBerlaku},
									{"name": 'sCriteria_m044TglBerlaku_day', "value": m044TglBerlakuDay},
									{"name": 'sCriteria_m044TglBerlaku_month', "value": m044TglBerlakuMonth},
									{"name": 'sCriteria_m044TglBerlaku_year', "value": m044TglBerlakuYear}
							);
						}
	
						var masterPanel = $('#filter_masterPanel input').val();
						if(masterPanel){
							aoData.push(
									{"name": 'sCriteria_masterPanel', "value": masterPanel}
							);
						}
	
						var m044NewMultiple = $('#filter_m044NewMultiple input').val();
						if(m044NewMultiple){
							aoData.push(
									{"name": 'sCriteria_m044NewMultiple', "value": m044NewMultiple}
							);
						}
	
						var m044NewUnit = $('#filter_m044NewUnit input').val();
						if(m044NewUnit){
							aoData.push(
									{"name": 'sCriteria_m044NewUnit', "value": m044NewUnit}
							);
						}
	
						var m044RepMultiple = $('#filter_m044RepMultiple input').val();
						if(m044RepMultiple){
							aoData.push(
									{"name": 'sCriteria_m044RepMultiple', "value": m044RepMultiple}
							);
						}
	
						var m044RepUnit = $('#filter_m044RepUnit input').val();
						if(m044RepUnit){
							aoData.push(
									{"name": 'sCriteria_m044RepUnit', "value": m044RepUnit}
							);
						}
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
    $('.select-all').click(function(e) {
        $("#paintingApplicationTime_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordspaintingApplicationTimeperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordspaintingApplicationTimeperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

	$('#paintingApplicationTime_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordspaintingApplicationTimeperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPaintingApplicationTimeSelected = paintingApplicationTimeTable.$('tr.row_selected');
            if(jmlRecPaintingApplicationTimePerPage == anPaintingApplicationTimeSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordspaintingApplicationTimeperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });


});
</g:javascript>


			
