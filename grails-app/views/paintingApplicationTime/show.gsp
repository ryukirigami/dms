

<%@ page import="com.kombos.administrasi.PaintingApplicationTime" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'paintingApplicationTime.label', default: 'Painting Application Time')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePaintingApplicationTime;

$(function(){ 
	deletePaintingApplicationTime=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/paintingApplicationTime/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPaintingApplicationTimeTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-paintingApplicationTime" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="paintingApplicationTime"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${paintingApplicationTimeInstance?.m044TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m044TglBerlaku-label" class="property-label"><g:message
					code="paintingApplicationTime.m044TglBerlaku.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m044TglBerlaku-label">
						%{--<ba:editableValue
								bean="${paintingApplicationTimeInstance}" field="m044TglBerlaku"
								url="${request.contextPath}/PaintingApplicationTime/updatefield" type="text"
								title="Enter m044TglBerlaku" onsuccess="reloadPaintingApplicationTimeTable();" />--}%
							
								<g:formatDate date="${paintingApplicationTimeInstance?.m044TglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${paintingApplicationTimeInstance?.masterPanel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="masterPanel-label" class="property-label"><g:message
					code="paintingApplicationTime.masterPanel.label" default="Panel" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="masterPanel-label">
						%{--<ba:editableValue
								bean="${paintingApplicationTimeInstance}" field="masterPanel"
								url="${request.contextPath}/PaintingApplicationTime/updatefield" type="text"
								title="Enter masterPanel" onsuccess="reloadPaintingApplicationTimeTable();" />--}%
							
								${paintingApplicationTimeInstance?.masterPanel?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${paintingApplicationTimeInstance?.m044NewMultiple}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m044NewMultiple-label" class="property-label"><g:message
					code="paintingApplicationTime.m044NewMultiple.label" default="New Part Multiple Panel" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m044NewMultiple-label">
						%{--<ba:editableValue
								bean="${paintingApplicationTimeInstance}" field="m044NewMultiple"
								url="${request.contextPath}/PaintingApplicationTime/updatefield" type="text"
								title="Enter m044NewMultiple" onsuccess="reloadPaintingApplicationTimeTable();" />--}%
							
								<g:fieldValue bean="${paintingApplicationTimeInstance}" field="m044NewMultiple"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${paintingApplicationTimeInstance?.m044NewUnit}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m044NewUnit-label" class="property-label"><g:message
					code="paintingApplicationTime.m044NewUnit.label" default="New Part Unit Panel" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m044NewUnit-label">
						%{--<ba:editableValue
								bean="${paintingApplicationTimeInstance}" field="m044NewUnit"
								url="${request.contextPath}/PaintingApplicationTime/updatefield" type="text"
								title="Enter m044NewUnit" onsuccess="reloadPaintingApplicationTimeTable();" />--}%
							
								<g:fieldValue bean="${paintingApplicationTimeInstance}" field="m044NewUnit"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${paintingApplicationTimeInstance?.m044RepMultiple}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m044RepMultiple-label" class="property-label"><g:message
					code="paintingApplicationTime.m044RepMultiple.label" default="Repaired Multiple Panel" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m044RepMultiple-label">
						%{--<ba:editableValue
								bean="${paintingApplicationTimeInstance}" field="m044RepMultiple"
								url="${request.contextPath}/PaintingApplicationTime/updatefield" type="text"
								title="Enter m044RepMultiple" onsuccess="reloadPaintingApplicationTimeTable();" />--}%
							
								<g:fieldValue bean="${paintingApplicationTimeInstance}" field="m044RepMultiple"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${paintingApplicationTimeInstance?.m044RepUnit}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m044RepUnit-label" class="property-label"><g:message
					code="paintingApplicationTime.m044RepUnit.label" default="Repaired Unit Panel" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m044RepUnit-label">
						%{--<ba:editableValue
								bean="${paintingApplicationTimeInstance}" field="m044RepUnit"
								url="${request.contextPath}/PaintingApplicationTime/updatefield" type="text"
								title="Enter m044RepUnit" onsuccess="reloadPaintingApplicationTimeTable();" />--}%
							
								<g:fieldValue bean="${paintingApplicationTimeInstance}" field="m044RepUnit"/>
							
						</span></td>

                    </tr>
                    </g:if>
            %{--
                                <g:if test="${paintingApplicationTimeInstance?.companyDealer}">
                                <tr>
                                <td class="span2" style="text-align: right;"><span
                                    id="companyDealer-label" class="property-label"><g:message
                                    code="paintingApplicationTime.companyDealer.label" default="Company Dealer" />:</span></td>

                                        <td class="span3"><span class="property-value"
                                        aria-labelledby="companyDealer-label">
                                        <ba:editableValue
                                                bean="${paintingApplicationTimeInstance}" field="companyDealer"
                                                url="${request.contextPath}/PaintingApplicationTime/updatefield" type="text"
                                                title="Enter companyDealer" onsuccess="reloadPaintingApplicationTimeTable();" />

                                            <g:link controller="companyDealer" action="show" id="${paintingApplicationTimeInstance?.companyDealer?.id}">${paintingApplicationTimeInstance?.companyDealer?.encodeAsHTML()}</g:link>

                                    </span></td>

                            </tr>
                            </g:if>
                        --}%

            <g:if test="${paintingApplicationTimeInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="paintingApplicationTime.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${paintingApplicationTimeInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/PaintingApplicationTime/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadPaintingApplicationTimeTable();" />--}%

                        <g:fieldValue bean="${paintingApplicationTimeInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${paintingApplicationTimeInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="paintingApplicationTime.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${paintingApplicationTimeInstance}" field="dateCreated"
                                url="${request.contextPath}/PaintingApplicationTime/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadPaintingApplicationTimeTable();" />--}%

                        <g:formatDate date="${paintingApplicationTimeInstance?.dateCreated}" type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${paintingApplicationTimeInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="paintingApplicationTime.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${paintingApplicationTimeInstance}" field="createdBy"
                                url="${request.contextPath}/PaintingApplicationTime/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadPaintingApplicationTimeTable();" />--}%

                        <g:fieldValue bean="${paintingApplicationTimeInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${paintingApplicationTimeInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="paintingApplicationTime.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${paintingApplicationTimeInstance}" field="lastUpdated"
                                url="${request.contextPath}/PaintingApplicationTime/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadPaintingApplicationTimeTable();" />--}%

                        <g:formatDate date="${paintingApplicationTimeInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${paintingApplicationTimeInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="paintingApplicationTime.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${paintingApplicationTimeInstance}" field="updatedBy"
                                url="${request.contextPath}/PaintingApplicationTime/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadPaintingApplicationTimeTable();" />--}%

                        <g:fieldValue bean="${paintingApplicationTimeInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.close.label" default="Close" /></a>
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${paintingApplicationTimeInstance?.id}"
					update="[success:'paintingApplicationTime-form',failure:'paintingApplicationTime-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePaintingApplicationTime('${paintingApplicationTimeInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
