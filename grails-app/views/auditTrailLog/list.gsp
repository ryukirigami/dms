<%@ page import="com.kombos.baseapp.AppSettingParam; com.kombos.baseapp.AppSettingParamService" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<html>
<!--<![endif]-->

<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'auditTrailLog.label', default: 'Security Log')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="export"></r:require>
</head>
<body>

<h3 class="navbar box-header no-border">
    <g:message code="auditTrailLog.label" default="Security Log"/> List
    <ul class="nav pull-right">
        <!--  <li class="dropdown"><a data-toggle="dropdown"
                                class="dropdown-toggle usermenu" href="#"><i class="icon-filter"></i></a>
            <div class="dropdown-menu" style="width: 400px;">
                <table cellspacing="0" cellpadding="0" border="0"
                       class="display dropdown-form" id="instanceDatatables-filters"
                       width="100%">
                    <thead>
                    <tr>
                        <th>Column</th>
                        <th>Filter</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div></li>
        <li class="separator"></li>
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
                    class="icon-plus"></i>&nbsp;&nbsp;
        </a></li>
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>-->
    </ul>
</h3>
<div class="box">
    <div class="span12" id="instance-table">

        <g:render template="../menu/maxLineDisplay"/>
        <script type="text/javascript">
            var shrinkInstanceTable;
            var reloadInstanceTable;
            var expandInstanceTable;
            jQuery(function () {

                var oTable = $('#instanceDatatables_datatable').dataTable({
                	"sScrollX": "100%",            				
    				"bScrollCollapse": true,
					"bAutoWidth" : false,
                    "bPaginate" : true,
                    "sInfo" : "",
                    "sInfoEmpty" : "",

                    "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
                    bFilter: false,
                    "bStateSave": false,
                    'sPaginationType': 'bootstrap',
                    "fnInitComplete": function () {
                    	var dataTables_wrapper_height = $('.dataTables_wrapper').height();
           			 dataTables_wrapper_height += 50;
           			 $('.dataTables_wrapper').height(dataTables_wrapper_height);
                    },
                    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    },
                    "fnServerParams": function ( aoData ) {
                		aoData.push( {"name": "sFilter_1", "value": $('#operator_menuCode li.active').attr('name')} );
                    	aoData.push( {"name": "sFilter_2", "value": $('#operator_userName li.active').attr('name')} );
                    	aoData.push( {"name": "sFilter_3", "value": $('#operator_actionType li.active').attr('name')} );
                    	aoData.push( {"name": "sFilter_4", "value": $('#operator_pkId li.active').attr('name')} );
                    	aoData.push( {"name": "sFilter_5", "value": $('#operator_oldValue li.active').attr('name')} );
                    	aoData.push( {"name": "sFilter_6", "value": $('#operator_newValue li.active').attr('name')} );
                    	aoData.push( {"name": "sFilter_7", "value": $('#operator_createdDate li.active').attr('name')} );
                    	aoData.push( {"name": "sFilter_8", "value": $('#operator_createdHost li.active').attr('name')} );
                    	aoData.push( {"name": "sFilter_9", "value": $('#operator_createdHostName li.active').attr('name')} );
                	},
                    "bSort": true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
                    "sAjaxSource": "${request.contextPath}/auditTrailLog/getList",
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var username = jQuery('#username').val();
                        var menuCode = jQuery('#menuCode').val();
                        var from_date = jQuery('#from_date').val();
                        var to_date = jQuery('#to_date').val();

                        aoData.push(
                                {"name": "username", "value": username},
                                {"name": "menuCode", "value": menuCode},
                                {"name": "from_date", "value": from_date},
                                {"name": "to_date", "value": to_date}
                        );
                        
                        $('.search_init').each(function(idx){
                        		aoData.push( {"name": "sSearch_"+(idx+1), "value": this.value} );
                        });

                        $.ajax({ "dataType": 'json',
                            "type": "POST",
                            "url": sSource,
                            "data": aoData ,
                            "success": function (json) {
                                fnCallback(json);                              
                            }
                        });
                    },
                    "aoColumnDefs": [
                        {   "sName": "logId",
                            "aTargets": [0],
                            "bSearchable": true,
                            "bSortable": true,
                            "bVisible": false
                        }  ,
                        {   "sName": "menuCode",
                            "aTargets": [1],
                            "bSearchable": true,
                            "bSortable": true,
                            "bVisible": true
                        }  ,
                        {   "sName": "userName",
                            "aTargets": [2],
                            "bSearchable": true,
                            "bSortable": true,
                            "bVisible": true
                        }  ,
                        {   "sName": "actionType",
                            "aTargets": [3],
                            "bSearchable": true,
                            "bSortable": true,
                            "bVisible": true
                        }  ,
                        {   "sName": "pkId",
                            "aTargets": [4],
                            "bSearchable": true,
                            "bSortable": true,
                            "bVisible": true
                        }   ,
                        {   "sName": "oldValue",
                            "aTargets": [5],
                            "bSearchable": true,
                            "bSortable": true,
                            "bVisible": true
                        }   ,
                        {   "sName": "newValue",
                            "aTargets": [6],
                            "bSearchable": true,
                            "bSortable": true,
                            "bVisible": true
                        }   ,
                        {   "sName": "createdDate",
                            "aTargets": [7],
                            "bSearchable": true,
                            "bSortable": true,
                            "bVisible": true
                        }  ,
                        {   "sName": "createdHost",
                            "aTargets": [8],
                            "bSearchable": true,
                            "bSortable": true,
                            "bVisible": true
                        }  ,
                        {   "sName": "createdHostName",
                            "aTargets": [9],
                            "bSearchable": true,
                            "bSortable": true,
                            "bVisible": true
                        }
                    ]
                });

                $("#instanceDatatables-filters tbody")
                        .append("<tr><td align=\"center\">role.id.label&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_id\"> <input type=\"text\" name=\"search_id\" class=\"search_init\" size=\"10\"/></td></tr>")
                        .append("<tr><td align=\"center\">Name&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_name\"> <input type=\"text\" name=\"search_name\" class=\"search_init\" size=\"10\"/></td></tr>")
                        .append("<tr><td align=\"center\">Label&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_permissions\"> <input type=\"text\" name=\"search_permissions\" class=\"search_init\" size=\"10\"/></td></tr>")
                        .append("<tr><td align=\"center\">Users&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_users\"> <input type=\"text\" name=\"search_users\" class=\"search_init\" size=\"10\"/></td></tr>")
                ;

                $("#filter_menuCode input").keypress(function (e) {
                    /* Filter on the column (the index) of this element */
                     var code = (e.keyCode ? e.keyCode : e.which);
		            if (code == 13) { //Enter keycode
		               oTable.fnDraw();   
		                e.stopPropagation();
		            }

                });

                $("#filter_userName input").keypress(function (e) {
                    /* Filter on the column (the index) of this element */
                     var code = (e.keyCode ? e.keyCode : e.which);
		            if (code == 13) { //Enter keycode
		                oTable.fnDraw();   
		                e.stopPropagation();
		            }

                });

                $("#filter_actionType input").keypress(function (e) {
                    /* Filter on the column (the index) of this element */
                     var code = (e.keyCode ? e.keyCode : e.which);
		            if (code == 13) { //Enter keycode
		               oTable.fnDraw();   
		                e.stopPropagation();
		            }
                });

                $("#filter_pkId input").keypress(function (e) {
                    /* Filter on the column (the index) of this element */
                     var code = (e.keyCode ? e.keyCode : e.which);
		            if (code == 13) { //Enter keycode
		               oTable.fnDraw();   
		                e.stopPropagation();
		            }

                });

                $("#filter_oldValue input").keypress(function (e) {
                    /* Filter on the column (the index) of this element */
                     var code = (e.keyCode ? e.keyCode : e.which);
		            if (code == 13) { //Enter keycode
		              oTable.fnDraw();   
		                e.stopPropagation();
		            }

                });

                $("#filter_newValue input").keypress(function (e) {
                    /* Filter on the column (the index) of this element */
                    var code = (e.keyCode ? e.keyCode : e.which);
		            if (code == 13) { //Enter keycode
		               oTable.fnDraw();   
		                e.stopPropagation();
		            }

                });

                $("#filter_createdDate input").keypress(function (e) {
                    /* Filter on the column (the index) of this element */
                     var code = (e.keyCode ? e.keyCode : e.which);
		            if (code == 13) { //Enter keycode
		               oTable.fnDraw();   
		                e.stopPropagation();
		            }

                });

                $("#filter_createdHost input").keypress(function (e) {
                    /* Filter on the column (the index) of this element */
                     var code = (e.keyCode ? e.keyCode : e.which);
		            if (code == 13) { //Enter keycode
		              oTable.fnDraw();   
		                e.stopPropagation();
		            }

                });

                $("#filter_createdHostName input").keypress(function (e) {
                    /* Filter on the column (the index) of this element */
                     var code = (e.keyCode ? e.keyCode : e.which);
		            if (code == 13) { //Enter keycode
		               oTable.fnDraw();   
		                e.stopPropagation();
		            }

                });


                $("#filter_menuCode input").click(function (e) {				    	
				    	 e.stopPropagation();  
				    }); 

                $("#filter_userName input").click(function (e) {				    	
				    	 e.stopPropagation();  
				    }); 

                $("#filter_actionType input").click(function (e) {				    	
				    	 e.stopPropagation();  
				    }); 

                $("#filter_pkId input").click(function (e) {				    	
				    	 e.stopPropagation();  
				    }); 

                $("#filter_oldValue input").click(function (e) {				    	
				    	 e.stopPropagation();  
				    }); 

                $("#filter_newValue input").click(function (e) {				    	
				    	 e.stopPropagation();  
				    }); 

                $("#filter_createdDate input").click(function (e) {				    	
				    	 e.stopPropagation();  
				    }); 
				    
                $("#filter_createdHost input").click(function (e) {				    	
				    	 e.stopPropagation();  
				    }); 

                $("#filter_createdHostName input").click(function (e) {				    	
				    	 e.stopPropagation();  
				    }); 


                shrinkInstanceTable = function(){
                    var oTable = $('#instanceDatatables_datatable').dataTable();

                }

                expandInstanceTable = function(){
                    var oTable = $('#instanceDatatables_datatable').dataTable();

                }

                reloadInstanceTable = function(){
                    var oTable = $('#instanceDatatables_datatable').dataTable();
                    oTable.fnReloadAjax();
                }

                $('#instanceDatatables_datatable th .select-all').click(function(e) {
                    var tc = $('#instanceDatatables_datatable tbody .row-select').attr('checked', this.checked);

                    if(this.checked)
                        tc.parent().parent().addClass('row_selected');
                    else
                        tc.parent().parent().removeClass('row_selected');
                    e.stopPropagation();
                });
                $('#instanceDatatables_datatable tbody tr .row-select').live('click', function (e) {
                    if(this.checked)
                        $(this).parent().parent().addClass('row_selected');
                    else
                        $(this).parent().parent().removeClass('row_selected');
                    e.stopPropagation();

                } );

                $('#instanceDatatables_datatable tbody tr a').live('click', function (e) {
                    e.stopPropagation();
                } );

                $('#instanceDatatables_datatable tbody td').live('click', function(e) {
                    if (e.target.tagName.toUpperCase() != "INPUT") {
                        var $tc = $(this).parent().find('input:checkbox'),
                                tv = $tc.attr('checked');
                        $tc.attr('checked', !tv);
                        $(this).parent().toggleClass('row_selected');
                    }
                });

                $("#search_button").bind('click', function (e) {

//                    $('#export').display('block');

                    var from_date = jQuery('#from_date').val();
                    var to_date = jQuery('#to_date').val();

                    if(!from_date){
                        return alert('Please Enter From Date');
                    }

                    if(!to_date){
                        return alert('Please Enter To Date');
                    }

                    var oTable = $('#instanceDatatables_datatable').dataTable();
                    oTable.fnDraw();
                });

                $(".operator li").click(function (e) {
                    $(this).addClass('active').siblings().removeClass('active');
                    $(this).closest('span').toggleClass('open');
                });

            });
        </script>

    <div class="form-horizontal">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label>User</label>
                    <g:select class="span12" name="username" from="${com.kombos.baseapp.sec.shiro.User.list()}" optionKey="username" size="1" default="none" noSelection="['': '']"/>
                </div>
                <div class="span4">
                    <label>Menu</label>
                    <g:select class="span12" name="menuCode" from="${com.kombos.baseapp.menu.Menu.listOrderByLabel()}" optionKey="menuCode" optionValue="label" size="1" default="none" noSelection="['': '']"/>
                </div>
            </div>
            <div class="row-fluid">
                &nbsp;
            </div>
            <div class="row-fluid">
                <div class="span4">
                    <label>From Date</label>
                    <input type="text" class="span12" id="from_date" name="from_date" data-date-format="dd/mm/yyyy">
                    <g:javascript>
                        $('#from_date').datepicker();
                    </g:javascript>
                </div>
                <div class="span4">
                    <label>To Date</label>
                    <input type="text" class="span12" id="to_date" name="to_date" data-date-format="dd/mm/yyyy">
                    <g:javascript>
                        $('#to_date').datepicker();
                    </g:javascript>
                </div>
            </div>
            <div class="row-fluid">
                &nbsp;
            </div>
            <div class="row-fluid">
                <div class="span4">
                    <a href="#search" id="search_button" role="button" class="btn" data-toggle="modal">Search</a>
                </div>
            </div>
            <div class="row-fluid">
                &nbsp;
            </div>
        </div>
    </div>
        %{--<export:formats formats="['excel','csv']" style="display: none;" id="export"></export:formats>--}%
        <export:formats formats="['excel','csv']" action="export"></export:formats>
        <table id="instanceDatatables_datatable" cellpadding="0" cellspacing="0"
               border="0"
               class="display table table-striped table-bordered table-hover" width="100%">
            <thead>
            <tr>
            <tr>
                <th>
                    <g:message code="app.auditTrailLog.logId.label" default="logId"/>
                </th>
                <th>
                    <g:message code="app.auditTrailLog.logId.label" default="Menu&nbsp;Name"/>
                    <div id="filter_menuCode" style="margin-top: 38px;">
                        <input type="text" name="search_menuCode" class="search_init" />
                    </div>
                </th>
                <th>
                    <g:message code="app.auditTrailLog.userName.label" default="User&nbsp;Name"/>
                    <div id="filter_userName" style="margin-top: 38px;">
                        <input type="text" name="search_userName" class="search_init" />                        
                    </div>
                </th>
                <th>
                    <g:message code="app.auditTrailLog.actionType.label" default="Action&nbsp;Type"/>
                    <div id="filter_actionType" style="margin-top: 38px;">
                        <input type="text" name="search_actionType" class="search_init" />
                    </div>
                </th>
                <th>
                    <g:message code="app.auditTrailLog.pkId.label" default="Ref&nbsp;PkId"/>
                    <div id="filter_pkId" style="margin-top: 38px;">
                        <input type="text" name="search_pkId" class="search_init" />
                    </div>
                </th>
                <th>
                    <g:message code="app.auditTrailLog.oldValue.label" default="Old&nbsp;Value"/>
                    <div id="filter_oldValue" style="margin-top: 38px;">
                        <input type="text" name="search_oldValue" class="search_init" />
                    </div>
                </th>
                <th>
                    <g:message code="app.auditTrailLog.newValue.label" default="New&nbsp;Value"/>
                    <div id="filter_newValue" style="margin-top: 38px;">
                        <input type="text" name="search_newValue" class="search_init" />
                    </div>
                </th>
                <th>
                    <g:message code="app.auditTrailLog.createdDate.label" default="Created&nbsp;Date"/>
                    <div id="filter_createdDate" style="margin-top: 38px;">
                        <input type="text" name="search_createdDate" class="search_init" />
                    </div>
                </th>
                <th>
                    <g:message code="app.auditTrailLog.createdHost.label" default="Created&nbsp;Host"/>
                    <div id="filter_createdHost" style="margin-top: 38px;">
                        <input type="text" name="search_createdHost" class="search_init" />
                    </div>
                </th>
                <th>
                    <g:message code="app.auditTrailLog.createdHostName.label" default="Created&nbsp;Host&nbsp;Name"/>
                    <div id="filter_createdHostName" style="margin-top: 38px;">
                        <input type="text" name="search_createdHostName" class="search_init" />
                    </div>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="4" class="dataTables_empty">Loading data from server</td>
            </tr>
            </tbody>
        </table>



    </div>
    <div class="span7" id="role-form" style="display: none;"></div>
</div>

<script type="text/javascript">
    var show;
    var loadForm;
    var shrinkTableLayout;
    var expandTableLayout;
    $(function(){

        $('.box-action').click(function(){
            switch($(this).attr('target')){
                case '_CREATE_' :
                    shrinkTableLayout();
                    jQuery('#spinner').fadeIn(1);jQuery.ajax({type:'POST', url:'${request.contextPath}/role/create',success:function(data,textStatus){loadForm(data, textStatus);;},error:function(XMLHttpRequest,textStatus,errorThrown){},complete:function(XMLHttpRequest,textStatus){jQuery('#spinner').fadeOut();}});
                    break;
                case '_DELETE_' :
                    bootbox.confirm('Are you sure?',
                            function(result){
                                if(result){
                                    massDelete();
                                }
                            });

                    break;
            }
            return false;
        });

        show = function(id) {
            shrinkTableLayout();
            $('#spinner').fadeIn(1);
            $.ajax({type:'POST', url:'${request.contextPath}/role/show/'+id,
                success:function(data,textStatus){
                    loadForm(data, textStatus);
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        };

        edit = function(id) {
            shrinkTableLayout();
            $('#spinner').fadeIn(1);
            $.ajax({type:'POST', url:'${request.contextPath}/role/edit/'+id,
                success:function(data,textStatus){
                    loadForm(data, textStatus);
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        };

        loadForm = function(data, textStatus){
            $('#role-form').empty();
            $('#role-form').append(data);
        }

        shrinkTableLayout = function(){
            if($("#instance-table").hasClass('span12')){
                $("#instance-table").toggleClass('span12 span5');
            }
            $("#role-form").css("display","block");
            shrinkInstanceTable();
        }

        expandTableLayout = function(){
            if($("#instance-table").hasClass('span5')){
                $("#instance-table").toggleClass('span5 span12');
            }
            $("#role-form").css("display","none");
            expandInstanceTable();
        }

        massDelete = function() {
            var recordsToDelete = [];
            $("#instance-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    recordsToDelete.push(id);
                }
            });

            var json = JSON.stringify(recordsToDelete);

            $.ajax({
                url:'${request.contextPath}/role/massdelete',
                type: "POST", // Always use POST when deleting data
                data: { ids: json },
                complete: function(xhr, status) {
                    reloadInstanceTable();
                }
            });

        }

    });
</script>
</body>
</html>