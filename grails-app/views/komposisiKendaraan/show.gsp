

<%@ page import="com.kombos.customerprofile.KomposisiKendaraan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'komposisiKendaraan.label', default: 'Komposisi Kendaraan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteKomposisiKendaraan;

$(function(){ 
	deleteKomposisiKendaraan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/komposisiKendaraan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadKomposisiKendaraanTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-komposisiKendaraan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="komposisiKendaraan"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${komposisiKendaraanInstance?.company}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="company-label" class="property-label"><g:message
					code="komposisiKendaraan.company.label" default="Perusahaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="company-label">
						%{--<ba:editableValue
								bean="${komposisiKendaraanInstance}" field="company"
								url="${request.contextPath}/KomposisiKendaraan/updatefield" type="text"
								title="Enter company" onsuccess="reloadKomposisiKendaraanTable();" />--}%
							
								${komposisiKendaraanInstance?.company?.encodeAsHTML()}

						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${komposisiKendaraanInstance?.merk}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="merk-label" class="property-label"><g:message
					code="komposisiKendaraan.merk.label" default="Merk" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="merk-label">
						%{--<ba:editableValue
								bean="${komposisiKendaraanInstance}" field="merk"
								url="${request.contextPath}/KomposisiKendaraan/updatefield" type="text"
								title="Enter merk" onsuccess="reloadKomposisiKendaraanTable();" />--}%
							
								${komposisiKendaraanInstance?.merk?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${komposisiKendaraanInstance?.model}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="model-label" class="property-label"><g:message
					code="komposisiKendaraan.model.label" default="Model" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="model-label">
						%{--<ba:editableValue
								bean="${komposisiKendaraanInstance}" field="model"
								url="${request.contextPath}/KomposisiKendaraan/updatefield" type="text"
								title="Enter model" onsuccess="reloadKomposisiKendaraanTable();" />--}%
							
								${komposisiKendaraanInstance?.model?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>

                <g:if test="${komposisiKendaraanInstance?.t105Jumlah}">
                <tr>
                <td class="span2" style="text-align: right;"><span
                    id="t105Jumlah-label" class="property-label"><g:message
                    code="komposisiKendaraan.t105Jumlah.label" default="Jumlah" />:</span></td>

                        <td class="span3"><span class="property-value"
                        aria-labelledby="t105Jumlah-label">
                        %{--<ba:editableValue
                                bean="${komposisiKendaraanInstance}" field="t105Jumlah"
                                url="${request.contextPath}/KomposisiKendaraan/updatefield" type="text"
                                title="Enter t105Jumlah" onsuccess="reloadKomposisiKendaraanTable();" />--}%

                                <g:fieldValue bean="${komposisiKendaraanInstance}" field="t105Jumlah"/>

                    </span></td>

                </tr>
                </g:if>

                <g:if test="${komposisiKendaraanInstance?.t105Tahun}">
                <tr>
                <td class="span2" style="text-align: right;"><span
                    id="t105Tahun-label" class="property-label"><g:message
                    code="komposisiKendaraan.t105Tahun.label" default="Tahun" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t105Tahun-label">
                        %{--<ba:editableValue
                                bean="${komposisiKendaraanInstance}" field="t105Tahun"
                                url="${request.contextPath}/KomposisiKendaraan/updatefield" type="text"
                                title="Enter t105Tahun" onsuccess="reloadKomposisiKendaraanTable();" />--}%

                        <g:fieldValue bean="${komposisiKendaraanInstance}" field="t105Tahun"/>

                    </span></td>

                </tr>
                </g:if>
			
				<g:if test="${komposisiKendaraanInstance?.t105Id}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t105Id-label" class="property-label"><g:message
					code="komposisiKendaraan.t105Id.label" default="T105 Id" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t105Id-label">
						%{--<ba:editableValue
								bean="${komposisiKendaraanInstance}" field="t105Id"
								url="${request.contextPath}/KomposisiKendaraan/updatefield" type="text"
								title="Enter t105Id" onsuccess="reloadKomposisiKendaraanTable();" />--}%
							
								<g:fieldValue bean="${komposisiKendaraanInstance}" field="t105Id"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${komposisiKendaraanInstance?.t105TipeKepemilikan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t105TipeKepemilikan-label" class="property-label"><g:message
					code="komposisiKendaraan.t105TipeKepemilikan.label" default="T105 Tipe Kepemilikan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t105TipeKepemilikan-label">
						%{--<ba:editableValue
								bean="${komposisiKendaraanInstance}" field="t105TipeKepemilikan"
								url="${request.contextPath}/KomposisiKendaraan/updatefield" type="text"
								title="Enter t105TipeKepemilikan" onsuccess="reloadKomposisiKendaraanTable();" />--}%
							
								<g:fieldValue bean="${komposisiKendaraanInstance}" field="t105TipeKepemilikan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${komposisiKendaraanInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="komposisiKendaraan.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${komposisiKendaraanInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/KomposisiKendaraan/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadKomposisiKendaraanTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${komposisiKendaraanInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${komposisiKendaraanInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="komposisiKendaraan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${komposisiKendaraanInstance}" field="lastUpdProcess"
								url="${request.contextPath}/KomposisiKendaraan/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadKomposisiKendaraanTable();" />--}%
							
								<g:fieldValue bean="${komposisiKendaraanInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${komposisiKendaraanInstance?.customer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="customer-label" class="property-label"><g:message
					code="komposisiKendaraan.customer.label" default="Customer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="customer-label">
						%{--<ba:editableValue
								bean="${komposisiKendaraanInstance}" field="customer"
								url="${request.contextPath}/KomposisiKendaraan/updatefield" type="text"
								title="Enter customer" onsuccess="reloadKomposisiKendaraanTable();" />--}%
							
								<g:link controller="customer" action="show" id="${komposisiKendaraanInstance?.customer?.id}">${komposisiKendaraanInstance?.customer?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${komposisiKendaraanInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="komposisiKendaraan.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${komposisiKendaraanInstance}" field="dateCreated"
								url="${request.contextPath}/KomposisiKendaraan/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadKomposisiKendaraanTable();" />--}%
							
								<g:formatDate date="${komposisiKendaraanInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${komposisiKendaraanInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="komposisiKendaraan.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${komposisiKendaraanInstance}" field="createdBy"
                                url="${request.contextPath}/KomposisiKendaraan/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadKomposisiKendaraanTable();" />--}%

                        <g:fieldValue bean="${komposisiKendaraanInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${komposisiKendaraanInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="komposisiKendaraan.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${komposisiKendaraanInstance}" field="lastUpdated"
								url="${request.contextPath}/KomposisiKendaraan/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadKomposisiKendaraanTable();" />--}%
							
								<g:formatDate date="${komposisiKendaraanInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${komposisiKendaraanInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="komposisiKendaraan.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${komposisiKendaraanInstance}" field="updatedBy"
                                url="${request.contextPath}/KomposisiKendaraan/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadKomposisiKendaraanTable();" />--}%

                        <g:fieldValue bean="${komposisiKendaraanInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${komposisiKendaraanInstance?.id}"
					update="[success:'komposisiKendaraan-form',failure:'komposisiKendaraan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteKomposisiKendaraan('${komposisiKendaraanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
