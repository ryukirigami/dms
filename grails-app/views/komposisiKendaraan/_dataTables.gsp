
<%@ page import="com.kombos.customerprofile.KomposisiKendaraan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="komposisiKendaraan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="komposisiKendaraan.company.label" default="Perusahaan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="komposisiKendaraan.merk.label" default="Merk" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="komposisiKendaraan.model.label" default="Model" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="komposisiKendaraan.t105Jumlah.label" default="Jumlah" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="komposisiKendaraan.t105Tahun.label" default="Tahun" /></div>
            </th>


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="komposisiKendaraan.t105Id.label" default="T105 Id" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="komposisiKendaraan.t105TipeKepemilikan.label" default="T105 Tipe Kepemilikan" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="komposisiKendaraan.staDel.label" default="Sta Del" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="komposisiKendaraan.createdBy.label" default="Created By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="komposisiKendaraan.updatedBy.label" default="Updated By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="komposisiKendaraan.lastUpdProcess.label" default="Last Upd Process" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="komposisiKendaraan.customer.label" default="Customer" /></div>--}%
			%{--</th>--}%

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_company" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_company" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_merk" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_merk" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_model" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_model" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t105Jumlah" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t105Jumlah" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t105Tahun" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t105Tahun" class="search_init" />
                </div>
            </th>
	
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_t105Id" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_t105Id" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_t105TipeKepemilikan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_t105TipeKepemilikan" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%

			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_staDel" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_createdBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_updatedBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_lastUpdProcess" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_customer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_customer" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%


		</tr>
	</thead>
</table>

<g:javascript>
var komposisiKendaraanTable;
var reloadKomposisiKendaraanTable;
$(function(){

	reloadKomposisiKendaraanTable = function() {
		komposisiKendaraanTable.fnDraw();
	}

	var recordsKomposisiKendaraanPerPage = [];//new Array();
    var anKomposisiKendaraanSelected;
    var jmlRecKomposisiKendaraanPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	komposisiKendaraanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	komposisiKendaraanTable = $('#komposisiKendaraan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsKomposisiKendaraan = $("#komposisiKendaraan_datatables tbody .row-select");
            var jmlKomposisiKendaraanCek = 0;
            var nRow;
            var idRec;
            rsKomposisiKendaraan.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsKomposisiKendaraanPerPage[idRec]=="1"){
                    jmlKomposisiKendaraanCek = jmlKomposisiKendaraanCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsKomposisiKendaraanPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecKomposisiKendaraanPerPage = rsKomposisiKendaraan.length;
            if(jmlKomposisiKendaraanCek==jmlRecKomposisiKendaraanPerPage && jmlRecKomposisiKendaraanPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "company",
	"mDataProp": "company",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "merk",
	"mDataProp": "merk",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "model",
	"mDataProp": "model",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t105Jumlah",
	"mDataProp": "t105Jumlah",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t105Tahun",
	"mDataProp": "t105Tahun",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
//
//,
//
//{
//	"sName": "t105Id",
//	"mDataProp": "t105Id",
//	"aTargets": [5],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "t105TipeKepemilikan",
//	"mDataProp": "t105TipeKepemilikan",
//	"aTargets": [6],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "staDel",
//	"mDataProp": "staDel",
//	"aTargets": [7],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "createdBy",
//	"mDataProp": "createdBy",
//	"aTargets": [8],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "updatedBy",
//	"mDataProp": "updatedBy",
//	"aTargets": [9],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "lastUpdProcess",
//	"mDataProp": "lastUpdProcess",
//	"aTargets": [10],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "customer",
//	"mDataProp": "customer",
//	"aTargets": [11],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var company = $('#filter_company input').val();
						if(company){
							aoData.push(
									{"name": 'sCriteria_company', "value": company}
							);
						}
	
						var merk = $('#filter_merk input').val();
						if(merk){
							aoData.push(
									{"name": 'sCriteria_merk', "value": merk}
							);
						}
	
						var model = $('#filter_model input').val();
						if(model){
							aoData.push(
									{"name": 'sCriteria_model', "value": model}
							);
						}

						var t105Jumlah = $('#filter_t105Jumlah input').val();
						if(t105Jumlah){
							aoData.push(
									{"name": 'sCriteria_t105Jumlah', "value": t105Jumlah}
							);
						}

						var t105Tahun = $('#filter_t105Tahun input').val();
						if(t105Tahun){
							aoData.push(
									{"name": 'sCriteria_t105Tahun', "value": t105Tahun}
							);
						}
	
						%{--var t105Id = $('#filter_t105Id input').val();--}%
						%{--if(t105Id){--}%
							%{--aoData.push(--}%
									%{--{"name": 'sCriteria_t105Id', "value": t105Id}--}%
							%{--);--}%
						%{--}--}%
	%{----}%
						%{--var t105TipeKepemilikan = $('#filter_t105TipeKepemilikan input').val();--}%
						%{--if(t105TipeKepemilikan){--}%
							%{--aoData.push(--}%
									%{--{"name": 'sCriteria_t105TipeKepemilikan', "value": t105TipeKepemilikan}--}%
							%{--);--}%
						%{--}--}%
	%{----}%
						%{--var staDel = $('#filter_staDel input').val();--}%
						%{--if(staDel){--}%
							%{--aoData.push(--}%
									%{--{"name": 'sCriteria_staDel', "value": staDel}--}%
							%{--);--}%
						%{--}--}%
	%{----}%
						%{--var createdBy = $('#filter_createdBy input').val();--}%
						%{--if(createdBy){--}%
							%{--aoData.push(--}%
									%{--{"name": 'sCriteria_createdBy', "value": createdBy}--}%
							%{--);--}%
						%{--}--}%
	%{----}%
						%{--var updatedBy = $('#filter_updatedBy input').val();--}%
						%{--if(updatedBy){--}%
							%{--aoData.push(--}%
									%{--{"name": 'sCriteria_updatedBy', "value": updatedBy}--}%
							%{--);--}%
						%{--}--}%
	%{----}%
						%{--var lastUpdProcess = $('#filter_lastUpdProcess input').val();--}%
						%{--if(lastUpdProcess){--}%
							%{--aoData.push(--}%
									%{--{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}--}%
							%{--);--}%
						%{--}--}%
	%{----}%
						%{--var customer = $('#filter_customer input').val();--}%
						%{--if(customer){--}%
							%{--aoData.push(--}%
									%{--{"name": 'sCriteria_customer', "value": customer}--}%
							%{--);--}%
						%{--}--}%

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#komposisiKendaraan_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsKomposisiKendaraanPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsKomposisiKendaraanPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#komposisiKendaraan_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsKomposisiKendaraanPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anKomposisiKendaraanSelected = komposisiKendaraanTable.$('tr.row_selected');
            if(jmlRecKomposisiKendaraanPerPage == anKomposisiKendaraanSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsKomposisiKendaraanPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
