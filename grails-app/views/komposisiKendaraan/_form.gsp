<%@ page import="com.kombos.customerprofile.Merk; com.kombos.customerprofile.Model; com.kombos.maintable.Company; com.kombos.customerprofile.KomposisiKendaraan" %>
<g:javascript>
		$(function(){
			$('#namaPerusahaan').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/komposisiKendaraan/dataPerusahaan', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
</g:javascript>
<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init',{
            aSep:'',
            mDec:''
        });
    });



    %{--function selectModel(){--}%
        %{--var root="${resource()}";--}%
        %{--var id;--}%
        %{--id=$('#merk').val();--}%
        %{--var url = root+'/komposisiKendaraan/findModel?merk.id='+id;--}%
        %{--jQuery('#model').load(url);--}%
    %{--}--}%

    var createMerk;
    var createModel;
    var saveMerk;
    var saveModel;
    var cancelMerk;
    var cancelModel;
    var disableEdit;
    var updateDeleteMerk;
    var updateDeleteModel;

    $(function(){
        createMerk = function(){
            $("#inputMerk").show();
            $("#saveMerkBtn").show();
            $("#createMerkBtn").hide();
            $("#cancelMerkBtn").show();
            $("#inputModel").hide();
            $("#merk").hide();
            $("#model").show();
            $("#model").attr("disabled","true");
            $("#createModelBtn").show();
            $("#saveModelBtn").hide();
            $("#cancelModelBtn").hide();
            $("#saveMerkBtn").show();

        }

        createModel = function(){
            $("#inputModel").show();
            $("#model").hide();
            $("#createModelBtn").hide();
            $("#inputMerk").hide();
            $("#cancelModelBtn").show();
            $("#merk").show();
            $("#merk").attr("disabled","true");
            $("#saveMerkBtn").hide();
            $("#createMerkBtn").show();
            $("#cancelMerkBtn").hide();
            $("#saveModelBtn").show();
        }

        cancelMerk = function(){
            $("#inputMerk").hide();
            disableEdit();
            $("#createMerkBtn").show();
            $("#cancelMerkBtn").hide();
            $("#saveMerkBtn").hide();

        }

        cancelModel = function(){
            $("#inputModel").hide();
            disableEdit();
            $("#createModelBtn").show();
            $("#cancelModelBtn").hide();
            $("#saveModelBtn").hide();

        }

        disableEdit = function(){
            $("#merk").show();
            $("#model").show();

            $("#merk").prop("disabled", null);
            $("#model").prop("disabled", null);

        }

        saveMerk = function(){

            var isiMerk = $('#inputMerk').val();
            if(isiMerk == ""){
                alert("Nama Merk Tidak boleh kosong");
                return;
            }



            $.ajax({type:'POST', url:'${request.contextPath}/komposisiKendaraan/insertMerk',
                data : {namaMerk : isiMerk},
                success:function(data,textStatus){
                    if(data){

                        if(data.error === "duplicate"){
                            alert("Data Duplicate, Silakan input yang belum ada");
                            $('#inputMerk').val("");
                            $("#inputMerk").focus();

                        }else{
                            $('#inputMerk').val("");
                            $('#merk').append("<option value='" + data.id + "'>" + data.namaMerk + "</option>");
                            toastr.success("Merk Berhasil disimpan");
                            cancelMerk();

                        }

                    }

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        };

        saveModel = function(){
            var idMerk = $('#merk').val();
            var isiModel = $('#inputModel').val();

            if(isiModel == ""){
                alert("Nama Kab/Kota tidak boleh kosong");
                return;
            }


            $.ajax({type:'POST', url:'${request.contextPath}/komposisiKendaraan/insertModel',
                data : {namaModel : $("#inputModel").val(), idMerk : idMerk},
                success:function(data,textStatus){
                    if(data){
                        if(data.error === "duplicate"){
                            alert("data dupliclate, Silakan input data yang belum ada");
                            $("#inputModel").val("");
                            $("#inputModel").focus();

                        }else{
                            $("#inputModel").val("");
                            $('#model').append("<option value='" + data.id + "'>" + data.namaModel + "</option>");
                            cancelModel();
                            selectModel();
                            toastr.success("Model Berhasil disimpan");

                        }
                    }

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        };

        selectModel = function() {
            var idMerk = $('#merk option:selected').val();
            if(idMerk!= undefined){
                jQuery.getJSON('${request.contextPath}/komposisiKendaraan/getModel?idMerk='+idMerk, function (data) {
                    if (data) {
                        $('#model').empty();
                        jQuery.each(data, function (index, value) {
                            $('#model').append("<option value='" + value.id + "'>" + value.namaModel + "</option>");
                        });
                    }
                });

            }else{
                $('#model').empty();
            }

        };
    });
</script>


<div class="control-group fieldcontain ${hasErrors(bean: komposisiKendaraanInstance, field: 'company', 'error')} required">
	<label class="control-label" for="company">
		<g:message code="komposisiKendaraan.company.label" default="Perusahaan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:select id="company" name="company.id" from="${Company.createCriteria().list{eq("staDel", "0");order("namaPerusahaan","asc")}}" optionKey="id" required="" value="${komposisiKendaraanInstance?.company?.id}" class="many-to-one"/>--}%
    <g:textField name="namaPerusahaan" id="namaPerusahaan"  class="typeahead" maxlength="10" value="${komposisiKendaraanInstance?.company?.namaPerusahaan}" autocomplete="off" required="true"/>
	</div>
</div>

<g:javascript>
    document.getElementById("namaPerusahaan").focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: komposisiKendaraanInstance, field: 'merk', 'error')} required">
	<label class="control-label" for="merk">
		<g:message code="komposisiKendaraan.merk.label" default="Merk Kendaraan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select style="width: 180px" id="merk" name="merk.id" from="${Merk.createCriteria().list(){eq("staDel", "0");order("m064NamaMerk", "asc")}}" optionKey="id" required="" onchange="selectModel();" value="${komposisiKendaraanInstance?.merk?.id}" class="many-to-one"/>
    <g:textField name="inputMerk" maxlength="20" />
    <g:field type="button" onclick="createMerk();" class="btn btn-primary create" name="createMerkBtn" id="createMerkBtn" value="${message(code: 'default.button.add.label', default: '+')}"/>
    <g:field type="button" onclick="saveMerk();" class="btn btn-primary create" name="saveMerkBtn" id="saveMerkBtn" value="${message(code: 'default.button.save.label', default: 'Simpan')}"/>
    <g:field type="button" onclick="cancelMerk();" class="btn btn-primary create" name="cancelMerkBtn" id="cancelMerkBtn" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: komposisiKendaraanInstance, field: 'model', 'error')} required">
	<label class="control-label" for="model">
		<g:message code="komposisiKendaraan.model.label" default="Model Kendaraan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select style="width: 180px" id="model" name="model.id" from="${Model.createCriteria().list(){eq("staDel", "0");order("m065NamaModel", "asc")}}" optionKey="id" required="" value="${komposisiKendaraanInstance?.model?.id}" class="many-to-one"/>
    <g:textField name="inputModel" maxlength="25" />
    <g:field type="button" onclick="createModel();" class="btn btn-primary create" name="createModelBtn" id="createModelBtn" value="${message(code: 'default.button.add.label', default: '+')}"/>
    <g:field type="button" onclick="saveModel();" class="btn btn-primary create" name="saveModelBtn" id="saveModelBtn" value="${message(code: 'default.button.save.label', default: 'Simpan')}"/>
    <g:field type="button" hidden="true" onclick="cancelModel();" class="btn btn-primary create" name="cancelModelBtn" id="cancelModelBtn" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: komposisiKendaraanInstance, field: 't105Jumlah', 'error')} required">
    <label class="control-label" for="t105Jumlah">
        <g:message code="komposisiKendaraan.t105Jumlah.label" default="Jumlah" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t105Jumlah" class="auto" value="${komposisiKendaraanInstance.t105Jumlah}" required="true"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: komposisiKendaraanInstance, field: 't105Tahun', 'error')} required">
    <label class="control-label" for="t105Tahun">
        <g:message code="komposisiKendaraan.t105Tahun.label" default="Tahun" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t105Tahun" data-v-min="0" data-v-max="2999" class="auto" maxlength="4" required="true" value="${komposisiKendaraanInstance?.t105Tahun}"/>
    </div>
</div>
<g:javascript>
    $(function(){
        $("#inputMerk").hide();
        $("#inputModel").hide();
        $("#saveMerkBtn").hide();
        $("#saveModelBtn").hide();
        $("#cancelMerkBtn").hide();
        $("#cancelModelBtn").hide();
        $("#updateMerkBtn").hide();
        $("#updateModelBtn").hide();
        $("#editMerkBtn").hide();
        $("#deleteMerkBtn").hide();
        var dataID = "${komposisiKendaraanInstance?.id}";
        if(!dataID){
            selectModel();
        }


    });

</g:javascript>
