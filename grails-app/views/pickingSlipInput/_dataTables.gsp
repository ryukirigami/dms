
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="pickingSlip_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="pickingSlip.goods.label" default="Kode Parts" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="pickingSlip.goods.label" default="Nama Parts" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="pickingSlip.goods.label" default="Qty Yg Hrs Di Ambil" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="pickingSlip.goods.label" default="Qty yg Sdh Diambil" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="pickingSlip.goods.label" default="Qty yg Blm Diambil" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="pickingSlip.t162Qty1.label" default="Satuan" /></div>
            </th>
		</tr>
	</thead>
</table>

<g:javascript>
var pickingSlipTable;
var reloadPickingSlipTable;
$(function(){

	reloadPickingSlipTable = function() {
		pickingSlipTable.fnDraw();
	}

	var recordsPickingSlipPerPage = [];
    var anPickingSlipSelected;
    var jmlRecPickingSlipPerPage=0;
    var id;

	$('#search_t162TglRequest').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t162TglRequest_day').val(newDate.getDate());
			$('#search_t162TglRequest_month').val(newDate.getMonth()+1);
			$('#search_t162TglRequest_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			pickingSlipTable.fnDraw();
	});




$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	pickingSlipTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	pickingSlipTable = $('#pickingSlip_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsPickingSlip = $("#pickingSlip_datatables tbody .row-select");
            var jmlPickingSlipCek = 0;
            var nRow;
            var idRec;
            rsPickingSlip.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsPickingSlipPerPage[idRec]=="1"){
                    jmlPickingSlipCek = jmlPickingSlipCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsPickingSlipPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPickingSlipPerPage = rsPickingSlip.length;
            if(jmlPickingSlipCek==jmlRecPickingSlipPerPage){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
        "bSort": true,
		//"bProcessing": true,
		//"bServerSide": true,
		//"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		//"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	     if(row['qtyHrsDiambil']==row['qtySdhDiAmbil']){
	        return data
	     }else{
		    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'" id="idPopUp">&nbsp;'+data;
		}
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "goods2",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "t142Qty1",
	"mDataProp": "qtyHrsDiambil",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "t142Qty1",
	"mDataProp": "qtySdhDiAmbil",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "t142Qty1",
	"mDataProp": "t142Qty1",
	"aTargets": [2],
	"bSearchable": true,
    "mRender": function ( data, type, row ) {
        return '<input id="qty-'+row['id']+'" class="inline-edit" type="text" maxlength="18" style="width:218px;" value="'+data+'">';
    },
    "bSortable": true,
    "sWidth":"200px",
    "bVisible": true
}
,

{
	"sName": "goods",
	"mDataProp": "satuan",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var t162ID = $('#filter_t162ID input').val();
						if(t162ID){
							aoData.push(
									{"name": 'sCriteria_t162ID', "value": t162ID}
							);
						}

						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}

						var t162Qty1 = $('#filter_t162Qty1 input').val();
						if(t162Qty1){
							aoData.push(
									{"name": 'sCriteria_t162Qty1', "value": t162Qty1}
							);
						}

						var t162Qty2 = $('#filter_t162Qty2 input').val();
						if(t162Qty2){
							aoData.push(
									{"name": 'sCriteria_t162Qty2', "value": t162Qty2}
							);
						}

						var t162TglRequest = $('#search_t162TglRequest').val();
						var t162TglRequestDay = $('#search_t162TglRequest_day').val();
						var t162TglRequestMonth = $('#search_t162TglRequest_month').val();
						var t162TglRequestYear = $('#search_t162TglRequest_year').val();

						if(t162TglRequest){
							aoData.push(
									{"name": 'sCriteria_t162TglRequest', "value": "date.struct"},
									{"name": 'sCriteria_t162TglRequest_dp', "value": t162TglRequest},
									{"name": 'sCriteria_t162TglRequest_day', "value": t162TglRequestDay},
									{"name": 'sCriteria_t162TglRequest_month', "value": t162TglRequestMonth},
									{"name": 'sCriteria_t162TglRequest_year', "value": t162TglRequestYear}
							);
						}
	
						var t162StaFA = $('#filter_t162StaFA input').val();
						if(t162StaFA){
							aoData.push(
									{"name": 'sCriteria_t162StaFA', "value": t162StaFA}
							);
						}
	
						var t162NoReff = $('#filter_t162NoReff input').val();
						if(t162NoReff){
							aoData.push(
									{"name": 'sCriteria_t162NoReff', "value": t162NoReff}
							);
						}
	
						var t162Qty1Available = $('#filter_t162Qty1Available input').val();
						if(t162Qty1Available){
							aoData.push(
									{"name": 'sCriteria_t162Qty1Available', "value": t162Qty1Available}
							);
						}
	
						var t162Qty2Available = $('#filter_t162Qty2Available input').val();
						if(t162Qty2Available){
							aoData.push(
									{"name": 'sCriteria_t162Qty2Available', "value": t162Qty2Available}
							);
						}
	
						var t162NamaPemohon = $('#filter_t162NamaPemohon input').val();
						if(t162NamaPemohon){
							aoData.push(
									{"name": 'sCriteria_t162NamaPemohon', "value": t162NamaPemohon}
							);
						}
	
						var t162xNamaUser = $('#filter_t162xNamaUser input').val();
						if(t162xNamaUser){
							aoData.push(
									{"name": 'sCriteria_t162xNamaUser', "value": t162xNamaUser}
							);
						}
	
						var t162xNamaDivisi = $('#filter_t162xNamaDivisi input').val();
						if(t162xNamaDivisi){
							aoData.push(
									{"name": 'sCriteria_t162xNamaDivisi', "value": t162xNamaDivisi}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#pickingSlip_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsPickingSlipPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsPickingSlipPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#pickingSlip_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsPickingSlipPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPickingSlipSelected = pickingSlipTable.$('tr.row_selected');

            if(jmlRecPickingSlipPerPage == anPickingSlipSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsPickingSlipPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
