<%@ page import="com.kombos.parts.PickingSlipDetail" %>
<g:javascript>
		$(function(){
			$('#t141NamaTeknisi').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/pickingSlipInput/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
</g:javascript>

<g:javascript>
    function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
    }
    $(document).ready(function()
    {
        $('input:radio[name=t162StaFA]').click(function(){
            if($('input:radio[name=t162StaFA]:nth(0)').is(':checked')){
                $("#t162NoReff").prop('disabled', false);

            }else{
                $("#t162NoReff").val("")
                $("#t162NoReff").prop('disabled', true);

            }
        });


    });
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: pickingSlipInstance, field: 't172ID', 'error')} ">
	<label class="control-label" for="t172ID">
		<g:message code="pickingSlip.t172ID.label" default="ID" />
		
	</label>
	<div class="controls">
	<g:textField name="t172ID" maxlength="20" value="${pickingSlipInstance?.goods?.m111ID}" readonly=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: pickingSlipInstance, field: 'goods', 'error')} ">
	<label class="control-label" for="goods">
		<g:message code="pickingSlip.goods.label" default="Nama Parts" />
		
	</label>
	<div class="controls">
	%{--<g:select id="goods" name="goods.id" from="${com.kombos.parts.Goods.list()}" optionKey="id" value="${pickingSlipInstance?.goods?.id}" class="many-to-one" noSelection="['null': '']" rereadonly=""/>--}%
    <g:textField name="goods" maxlength="20" value="${pickingSlipInstance?.goods?.m111Nama}" readonly=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: pickingSlipInstance, field: 't142Qty1', 'error')} ">
	<label class="control-label" for="t172Qty1Return">
		<g:message code="pickingSlip.t172Qty1Return.label" default="Qty" />
		
	</label>
	<div class="controls">
	    <g:textField name="t142Qty1" value="${fieldValue(bean: pickingSlipInstance, field: 't142Qty1')}" onkeypress="return isNumberKey(event)" />
	</div>
</div>
