<%@ page import="com.kombos.hrd.WarningKaryawan" %>



%{--
<div class="control-group fieldcontain ${hasErrors(bean: warningKaryawanInstance, field: 'historyWarningKaryawan', 'error')} ">
	<label class="control-label" for="historyWarningKaryawan">
		<g:message code="warningKaryawan.historyWarningKaryawan.label" default="History Warning Karyawan" />
		
	</label>
	<div class="controls">
	
<ul class="one-to-many">
<g:each in="${warningKaryawanInstance?.historyWarningKaryawan?}" var="h">
    <li><g:link controller="historyWarningKaryawan" action="show" id="${h.id}">${h?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="historyWarningKaryawan" action="create" params="['warningKaryawan.id': warningKaryawanInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'historyWarningKaryawan.label', default: 'HistoryWarningKaryawan')])}</g:link>
</li>
</ul>

	</div>
</div>

--}%


<div class="control-group fieldcontain ${hasErrors(bean: warningKaryawanInstance, field: 'warningKaryawan', 'error')} ">
	<label class="control-label" for="warningKaryawan">
		<g:message code="warningKaryawan.warningKaryawan.label" default="Warning Karyawan" />
		
	</label>
	<div class="controls">
	<g:textField name="warningKaryawan" value="${warningKaryawanInstance?.warningKaryawan}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: warningKaryawanInstance, field: 'keterangan', 'error')} ">
    <label class="control-label" for="keterangan">
        <g:message code="warningKaryawan.keterangan.label" default="Keterangan" />

    </label>
    <div class="controls">
        <g:textField name="keterangan" value="${warningKaryawanInstance?.keterangan}" />
    </div>
</div>

<g:javascript>
    $(function() {
        oFormUtils.fnInit();
    })
</g:javascript>