

<%@ page import="com.kombos.hrd.WarningKaryawan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'warningKaryawan.label', default: 'WarningKaryawan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteWarningKaryawan;

$(function(){ 
	deleteWarningKaryawan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/warningKaryawan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadWarningKaryawanTable();
   				expandTableLayout('warningKaryawan');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-warningKaryawan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="warningKaryawan"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${warningKaryawanInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="warningKaryawan.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${warningKaryawanInstance}" field="createdBy"
								url="${request.contextPath}/WarningKaryawan/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadWarningKaryawanTable();" />--}%
							
								<g:fieldValue bean="${warningKaryawanInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warningKaryawanInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="warningKaryawan.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${warningKaryawanInstance}" field="dateCreated"
								url="${request.contextPath}/WarningKaryawan/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadWarningKaryawanTable();" />--}%
							
								<g:formatDate date="${warningKaryawanInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warningKaryawanInstance?.historyWarningKaryawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="historyWarningKaryawan-label" class="property-label"><g:message
					code="warningKaryawan.historyWarningKaryawan.label" default="History Warning Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="historyWarningKaryawan-label">
						%{--<ba:editableValue
								bean="${warningKaryawanInstance}" field="historyWarningKaryawan"
								url="${request.contextPath}/WarningKaryawan/updatefield" type="text"
								title="Enter historyWarningKaryawan" onsuccess="reloadWarningKaryawanTable();" />--}%
							
								<g:each in="${warningKaryawanInstance.historyWarningKaryawan}" var="h">
								<g:link controller="historyWarningKaryawan" action="show" id="${h.id}">${h?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warningKaryawanInstance?.keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="keterangan-label" class="property-label"><g:message
					code="warningKaryawan.keterangan.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="keterangan-label">
						%{--<ba:editableValue
								bean="${warningKaryawanInstance}" field="keterangan"
								url="${request.contextPath}/WarningKaryawan/updatefield" type="text"
								title="Enter keterangan" onsuccess="reloadWarningKaryawanTable();" />--}%
							
								<g:fieldValue bean="${warningKaryawanInstance}" field="keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warningKaryawanInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="warningKaryawan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${warningKaryawanInstance}" field="lastUpdProcess"
								url="${request.contextPath}/WarningKaryawan/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadWarningKaryawanTable();" />--}%
							
								<g:fieldValue bean="${warningKaryawanInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warningKaryawanInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="warningKaryawan.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${warningKaryawanInstance}" field="lastUpdated"
								url="${request.contextPath}/WarningKaryawan/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadWarningKaryawanTable();" />--}%
							
								<g:formatDate date="${warningKaryawanInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warningKaryawanInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="warningKaryawan.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${warningKaryawanInstance}" field="staDel"
								url="${request.contextPath}/WarningKaryawan/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadWarningKaryawanTable();" />--}%
							
								<g:fieldValue bean="${warningKaryawanInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warningKaryawanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="warningKaryawan.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${warningKaryawanInstance}" field="updatedBy"
								url="${request.contextPath}/WarningKaryawan/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadWarningKaryawanTable();" />--}%
							
								<g:fieldValue bean="${warningKaryawanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warningKaryawanInstance?.warningKaryawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="warningKaryawan-label" class="property-label"><g:message
					code="warningKaryawan.warningKaryawan.label" default="Warning Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="warningKaryawan-label">
						%{--<ba:editableValue
								bean="${warningKaryawanInstance}" field="warningKaryawan"
								url="${request.contextPath}/WarningKaryawan/updatefield" type="text"
								title="Enter warningKaryawan" onsuccess="reloadWarningKaryawanTable();" />--}%
							
								<g:fieldValue bean="${warningKaryawanInstance}" field="warningKaryawan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('warningKaryawan');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${warningKaryawanInstance?.id}"
					update="[success:'warningKaryawan-form',failure:'warningKaryawan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteWarningKaryawan('${warningKaryawanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
