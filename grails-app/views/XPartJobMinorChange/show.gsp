

<%@ page import="com.kombos.administrasi.XPartJobMinorChange" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'XPartJobMinorChange.label', default: 'Mapping PartJob MinorChange')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteXPartJobMinorChange;

$(function(){ 
	deleteXPartJobMinorChange=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/XPartJobMinorChange/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadXPartJobMinorChangeTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-XPartJobMinorChange" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="XPartJobMinorChange"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${XPartJobMinorChangeInstance?.operation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="operation-label" class="property-label"><g:message
					code="XPartJobMinorChange.operation.label" default="Jobs" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="operation-label">
						%{--<ba:editableValue
								bean="${XPartJobMinorChangeInstance}" field="operation"
								url="${request.contextPath}/XPartJobMinorChange/updatefield" type="text"
								title="Enter operation" onsuccess="reloadXPartJobMinorChangeTable();" />--}%
							
								<g:link controller="operation" action="show" id="${XPartJobMinorChangeInstance?.operation?.id}">${XPartJobMinorChangeInstance?.operation?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${XPartJobMinorChangeInstance?.fullModelCode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="fullModelCode-label" class="property-label"><g:message
					code="XPartJobMinorChange.fullModelCode.label" default="Full Model" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="fullModelCode-label">
						%{--<ba:editableValue
								bean="${XPartJobMinorChangeInstance}" field="fullModelCode"
								url="${request.contextPath}/XPartJobMinorChange/updatefield" type="text"
								title="Enter fullModelCode" onsuccess="reloadXPartJobMinorChangeTable();" />--}%
							
								<g:link controller="fullModelCode" action="show" id="${XPartJobMinorChangeInstance?.fullModelCode?.id}">${XPartJobMinorChangeInstance?.fullModelCode?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${XPartJobMinorChangeInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="XPartJobMinorChange.goods.label" default="Goods" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${XPartJobMinorChangeInstance}" field="Kode Parts"
								url="${request.contextPath}/XPartJobMinorChange/updatefield" type="text"
								title="Enter goods" onsuccess="reloadXPartJobMinorChangeTable();" />--}%
							
								<g:link controller="goods" action="show" id="${XPartJobMinorChangeInstance?.goods?.id}">${XPartJobMinorChangeInstance?.goods?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${XPartJobMinorChangeInstance?.t112Jumlah}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t112Jumlah-label" class="property-label"><g:message
					code="XPartJobMinorChange.t112Jumlah.label" default="Jumlah" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t112Jumlah-label">
						%{--<ba:editableValue
								bean="${XPartJobMinorChangeInstance}" field="t112Jumlah"
								url="${request.contextPath}/XPartJobMinorChange/updatefield" type="text"
								title="Enter t112Jumlah" onsuccess="reloadXPartJobMinorChangeTable();" />--}%
							
								<g:fieldValue bean="${XPartJobMinorChangeInstance}" field="t112Jumlah"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${XPartJobMinorChangeInstance?.satuan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="satuan-label" class="property-label"><g:message
					code="XPartJobMinorChange.satuan.label" default="Satuan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="satuan-label">
						%{--<ba:editableValue
								bean="${XPartJobMinorChangeInstance}" field="satuan"
								url="${request.contextPath}/XPartJobMinorChange/updatefield" type="text"
								title="Enter satuan" onsuccess="reloadXPartJobMinorChangeTable();" />--}%
							
								<g:link controller="satuan" action="show" id="${XPartJobMinorChangeInstance?.satuan?.id}">${XPartJobMinorChangeInstance?.satuan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${XPartJobMinorChangeInstance?.namaProsesBP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaProsesBP-label" class="property-label"><g:message
					code="XPartJobMinorChange.namaProsesBP.label" default="Proses" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaProsesBP-label">
						%{--<ba:editableValue
								bean="${XPartJobMinorChangeInstance}" field="namaProsesBP"
								url="${request.contextPath}/XPartJobMinorChange/updatefield" type="text"
								title="Enter namaProsesBP" onsuccess="reloadXPartJobMinorChangeTable();" />--}%
							
								<g:link controller="namaProsesBP" action="show" id="${XPartJobMinorChangeInstance?.namaProsesBP?.id}">${XPartJobMinorChangeInstance?.namaProsesBP?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${XPartJobMinorChangeInstance?.m102Foto}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m102Foto-label" class="property-label"><g:message
					code="XPartJobMinorChange.m102Foto.label" default="Foto" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m102Foto-label">
						%{--<ba:editableValue
								bean="${XPartJobMinorChangeInstance}" field="m102Foto"
								url="${request.contextPath}/XPartJobMinorChange/updatefield" type="text"
								title="Enter m102Foto" onsuccess="reloadXPartJobMinorChangeTable();" />--}%
							<img class="" src="${createLink(controller:'XPartJobMinorChange', action:'showFoto', id : XPartJobMinorChangeInstance.id, random : logostamp)}" />
						</span></td>
					
				</tr>
				</g:if>
			 
			 
				<g:if test="${XPartJobMinorChangeInstance?.t112xThn}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t112xThn-label" class="property-label"><g:message
					code="XPartJobMinorChange.t112xThn.label" default="Thn" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t112xThn-label">
						%{--<ba:editableValue
								bean="${XPartJobMinorChangeInstance}" field="t112xThn"
								url="${request.contextPath}/XPartJobMinorChange/updatefield" type="text"
								title="Enter t112xThn" onsuccess="reloadXPartJobMinorChangeTable();" />--}%
							
								<g:fieldValue bean="${XPartJobMinorChangeInstance}" field="t112xThn"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${XPartJobMinorChangeInstance?.t112xBln}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t112xBln-label" class="property-label"><g:message
					code="XPartJobMinorChange.t112xBln.label" default="Bulan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t112xBln-label">
						%{--<ba:editableValue
								bean="${XPartJobMinorChangeInstance}" field="t112xBln"
								url="${request.contextPath}/XPartJobMinorChange/updatefield" type="text"
								title="Enter t112xBln" onsuccess="reloadXPartJobMinorChangeTable();" />--}%
							
								<g:fieldValue bean="${XPartJobMinorChangeInstance}" field="t112xBln"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${XPartJobMinorChangeInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="XPartJobMinorChange.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${XPartJobMinorChangeInstance}" field="createdBy"
								url="${request.contextPath}/XPartJobMinorChange/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadXPartJobMinorChangeTable();" />--}%
							
								<g:fieldValue bean="${XPartJobMinorChangeInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${XPartJobMinorChangeInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="XPartJobMinorChange.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${XPartJobMinorChangeInstance}" field="updatedBy"
								url="${request.contextPath}/XPartJobMinorChange/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadXPartJobMinorChangeTable();" />--}%
							
								<g:fieldValue bean="${XPartJobMinorChangeInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${XPartJobMinorChangeInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="XPartJobMinorChange.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${XPartJobMinorChangeInstance}" field="lastUpdProcess"
								url="${request.contextPath}/XPartJobMinorChange/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadXPartJobMinorChangeTable();" />--}%
							
								<g:fieldValue bean="${XPartJobMinorChangeInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${XPartJobMinorChangeInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="XPartJobMinorChange.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${XPartJobMinorChangeInstance}" field="dateCreated"
								url="${request.contextPath}/XPartJobMinorChange/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadXPartJobMinorChangeTable();" />--}%
							
								<g:formatDate date="${XPartJobMinorChangeInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${XPartJobMinorChangeInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="XPartJobMinorChange.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${XPartJobMinorChangeInstance}" field="lastUpdated"
								url="${request.contextPath}/XPartJobMinorChange/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadXPartJobMinorChangeTable();" />--}%
							
								<g:formatDate date="${XPartJobMinorChangeInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${XPartJobMinorChangeInstance?.id}"
					update="[success:'XPartJobMinorChange-form',failure:'XPartJobMinorChange-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteXPartJobMinorChange('${XPartJobMinorChangeInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
