<%@ page import="com.kombos.administrasi.PartsMapping; com.kombos.administrasi.NamaProsesBP; com.kombos.parts.Satuan; com.kombos.parts.Goods; com.kombos.administrasi.FullModelCode; com.kombos.administrasi.Operation; com.kombos.administrasi.XPartJobMinorChange" %>


<g:javascript>
function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	return false;
	
	return true;
}

</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: XPartJobMinorChangeInstance, field: 'operation', 'error')} required">
	<label class="control-label" for="operation">
		<g:message code="XPartJobMinorChange.operation.label" default="Job" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="operation" name="operation.id" from="${Operation.list()}" optionKey="id" required="" value="${XPartJobMinorChangeInstance?.operation?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: XPartJobMinorChangeInstance, field: 'fullModelCode', 'error')} required">
	<label class="control-label" for="fullModelCode">
		<g:message code="XPartJobMinorChange.fullModelCode.label" default="Full Model" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="fullModelCode" name="fullModelCode.id" from="${FullModelCode.list()}" optionKey="id" required=""  value="${XPartJobMinorChangeInstance?.fullModelCode?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: XPartJobMinorChangeInstance, field: 'goods', 'error')} required">
	<label class="control-label" for="goods">
		<g:message code="XPartJobMinorChange.goods.label" default="Kode Parts" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="partsMapping" name="partsMapping.id" from="${PartsMapping.list()}" optionKey="id" required="" value="${XPartJobMinorChangeInstance?.partsMapping?.id}" optionValue="goods" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: XPartJobMinorChangeInstance, field: 't112Jumlah', 'error')} required">
	<label class="control-label" for="t112Jumlah">
		<g:message code="XPartJobMinorChange.t112Jumlah.label" default="Jumlah" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t112xJumlah1" value="${fieldValue(bean: XPartJobMinorChangeInstance, field: 't112xJumlah1')}" required="" onkeypress="return isNumberKey(event)"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: XPartJobMinorChangeInstance, field: 'satuan', 'error')} required">
	<label class="control-label" for="satuan">
		<g:message code="XPartJobMinorChange.satuan.label" default="Satuan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="satuan" name="satuan.id" from="${Satuan.list()}" optionKey="id" required="" value="${XPartJobMinorChangeInstance?.partsMapping?.satuan?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: XPartJobMinorChangeInstance, field: 'namaProsesBP', 'error')} required">
	<label class="control-label" for="namaProsesBP">
		<g:message code="XPartJobMinorChange.namaProsesBP.label" default="Proses" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="namaProsesBP" name="namaProsesBP.id" from="${NamaProsesBP.list()}" optionKey="id" required="" value="${XPartJobMinorChangeInstance?.namaProsesBP?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: XPartJobMinorChangeInstance, field: 'm102Foto', 'error')} ">
	<label class="control-label" for="m102Foto">
		<g:message code="XPartJobMinorChange.m102Foto.label" default="Foto" />
		
	</label>
	<div class="controls">
	<input type="file" id="m102Foto" name="m102Foto" />
	</div>
</div>
 

<div class="control-group fieldcontain ${hasErrors(bean: XPartJobMinorChangeInstance, field: 't112xThn', 'error')} required">
	<label class="control-label" for="t112xThn">
		<g:message code="XPartJobMinorChange.t112xThn.label" default="Tahun" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
			<select id="t112xThnBln" name="t112xThnBln" style="width: 120px" required="">
                %{ 
                    for (int i=1990;i<2114;i++){
                            if(i==XPartJobMinorChangeInstance?.t112xThnBln){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>')
                            } else if(i==2014){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>')
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>')
                            }
                    }
                }%
            </select> 
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: XPartJobMinorChangeInstance, field: 't112xBln', 'error')} required">--}%
	%{--<label class="control-label" for="t112xBln">--}%
		%{--<g:message code="XPartJobMinorChange.t112xBln.label" default="Bulan" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{----}%
	%{--<select id="t112xBln" name="t112xBln" style="width: 130px" required="">--}%
		%{--%{--}%
			 %{--if(XPartJobMinorChangeInstance?.t112xBln != null){--}%
        %{--}%--}%
                         %{--<option value="${XPartJobMinorChangeInstance?.t112xBln}" selected="">${XPartJobMinorChangeInstance?.t112xBln}</option>--}%
        %{--%{--}%
              %{--} --}%
		%{--}%--}%
		%{--<option value="Januari">Januari</option>--}%
		%{--<option value="Februari">Februari</option>--}%
		%{--<option value="Maret">Maret</option>--}%
		%{--<option value="April">April</option>--}%
		%{--<option value="Mei">Mei</option>--}%
		%{--<option value="Juni">Juni</option>--}%
		%{--<option value="Juli">Juli</option>--}%
		%{--<option value="Agustus">Agustus</option>--}%
		%{--<option value="September">September</option>--}%
		%{--<option value="Oktober">Oktober</option>--}%
		%{--<option value="Nopember">Nopember</option>--}%
		%{--<option value="Desember">Desember</option>--}%
	%{--</select>--}%
	%{--</div>--}%
</div>
 

