
<%@ page import="com.kombos.administrasi.XPartJobMinorChange" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	return false;
	
	return true;
}

</g:javascript>
<table id="XPartJobMinorChange_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="XPartJobMinorChange.operation.label" default="Jobs" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="XPartJobMinorChange.fullModelCode.label" default="Full Model" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="XPartJobMinorChange.goods.label" default="Parts" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="XPartJobMinorChange.t112Jumlah.label" default="Jumlah"  /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="XPartJobMinorChange.satuan.label" default="Satuan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="XPartJobMinorChange.namaProsesBP.label" default="Proses" /></div>
			</th>

 
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="XPartJobMinorChange.t112xThn.label" default="Tahun" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="XPartJobMinorChange.t112xBln.label" default="Bulan" /></div>
			</th>

 
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_operation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_operation" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_fullModelCode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_fullModelCode" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_goods" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t112Jumlah" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t112Jumlah" class="search_init" onkeypress="return isNumberKey(event)"/>
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_satuan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_satuan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_namaProsesBP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaProsesBP" class="search_init" />
				</div>
			</th>
	
		 
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t112xThn" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t112xThn" class="search_init" onkeypress="return isNumberKey(event)"/>
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t112xBln" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t112xBln" class="search_init" />
				</div>
			</th>
	 

		</tr>
	</thead>
</table>

<g:javascript>
var XPartJobMinorChangeTable;
var reloadXPartJobMinorChangeTable;
$(function(){
	
	reloadXPartJobMinorChangeTable = function() {
		XPartJobMinorChangeTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	XPartJobMinorChangeTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	XPartJobMinorChangeTable = $('#XPartJobMinorChange_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "operation",
	"mDataProp": "operation",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "fullModelCode",
	"mDataProp": "fullModelCode",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t112Jumlah",
	"mDataProp": "t112Jumlah",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "namaProsesBP",
	"mDataProp": "namaProsesBP",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t112xThn",
	"mDataProp": "t112xThn",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t112xBln",
	"mDataProp": "t112xBln",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var operation = $('#filter_operation input').val();
						if(operation){
							aoData.push(
									{"name": 'sCriteria_operation', "value": operation}
							);
						}
	
						var fullModelCode = $('#filter_fullModelCode input').val();
						if(fullModelCode){
							aoData.push(
									{"name": 'sCriteria_fullModelCode', "value": fullModelCode}
							);
						}
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
	
						var t112Jumlah = $('#filter_t112Jumlah input').val();
						if(t112Jumlah){
							aoData.push(
									{"name": 'sCriteria_t112Jumlah', "value": t112Jumlah}
							);
						}
	
						var satuan = $('#filter_satuan input').val();
						if(satuan){
							aoData.push(
									{"name": 'sCriteria_satuan', "value": satuan}
							);
						}
	
						var namaProsesBP = $('#filter_namaProsesBP input').val();
						if(namaProsesBP){
							aoData.push(
									{"name": 'sCriteria_namaProsesBP', "value": namaProsesBP}
							);
						}
	
						var m102Foto = $('#filter_m102Foto input').val();
						if(m102Foto){
							aoData.push(
									{"name": 'sCriteria_m102Foto', "value": m102Foto}
							);
						}
	
						var m102FotoImageMime = $('#filter_m102FotoImageMime input').val();
						if(m102FotoImageMime){
							aoData.push(
									{"name": 'sCriteria_m102FotoImageMime', "value": m102FotoImageMime}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var t112xThn = $('#filter_t112xThn input').val();
						if(t112xThn){
							aoData.push(
									{"name": 'sCriteria_t112xThn', "value": t112xThn}
							);
						}
	
						var t112xBln = $('#filter_t112xBln input').val();
						if(t112xBln){
							aoData.push(
									{"name": 'sCriteria_t112xBln', "value": t112xBln}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
