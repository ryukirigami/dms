
<%@ page import="com.kombos.parts.Vendor; com.kombos.parts.Invoice" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'invoice.label', default: 'PO')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massAction();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/poMaintenance/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/poMaintenance/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#invoice-form').empty();
    	$('#invoice-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#invoice-table").hasClass("span12")){
   			$("#invoice-table").toggleClass("span12 span5");
        }
        $("#invoice-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#invoice-table").hasClass("span5")){
   			$("#invoice-table").toggleClass("span5 span12");
   		}
        $("#invoice-form").css("display","none");
   	}
   	
   	massAction = function(sta) {
   		var recordsToDelete = [];
		$("#invoice-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/poMaintenance/massAction?sta='+sta,
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		success:function(xhr, status) {
    		    toastr.success("Sukses")
    		},
    		complete: function(xhr, status) {
        		reloadInvoiceTable();
    		}
		});

		$('input[name=checkSelect]').attr("checked",false);
   	}

});

    function selectAll(){
        if($('input[name=checkSelect]').is(':checked')){
            $(".row-select").attr("checked",true);
        }else {
            $(".row-select").attr("checked",false);
        }
    }

    function clearInput(){
        $("#search_t156Tanggal_start").val("");
        $("#search_t156Tanggal_start_day").val("");
        $("#search_t156Tanggal_start_month").val("");
        $("#search_t156Tanggal_start_year").val("");

        $("#search_t156Tanggal_end").val("");
        $("#search_t156Tanggal_end_day").val("");
        $("#search_t156Tanggal_end_month").val("");
        $("#search_t156Tanggal_end_year").val("");
    }

</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="invoice-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="tanggal_invoice">
                                <g:message code="invoice.tanggal.label" default="No Po" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="noPO" class="controls">
                                <g:textField name="noPo" id="search_noPo" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="tanggal_invoice">
                                <g:message code="invoice.tanggal.label" default="Tanggal Invoice" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="tanggal_invoice" class="controls">
                                <ba:datePicker id="search_t156Tanggal_start" name="search_t156Tanggal_start" precision="day" format="dd/MM/yyyy"  value="" />
                                &nbsp;-&nbsp;
                                <ba:datePicker id="search_t156Tanggal_end" name="search_t156Tanggal_end" precision="day" format="dd/MM/yyyy"  value="" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" for="sta">
                                <g:message code="invoice.statusPO.label" default="Status PO" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="sta" class="controls">
                                <input type="radio" id="radio1" name="sta" value="X" checked>PO Outstanding&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" id="radio2" name="sta" value="2">Closed PO&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" id="radio3" name="sta" value="1">Cancel PO&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="view" id="view" onclick="searchInvoice();" >Search</button>
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel clear" name="clear" id="clear" onclick="clearInput();">Clear</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
			<g:render template="dataTables" />
            <br/>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td>
                            <button style="width: 100px;height: 30px; border-radius: 5px" class="btn btn-primary" onclick="massAction(1)">Cancel PO</button>
                            <button style="width: 100px;height: 30px; border-radius: 5px" class="btn btn-primary" onclick="massAction(0)">Open PO</button>
                            <button style="width: 100px;height: 30px; border-radius: 5px" class="btn btn-primary" onclick="massAction(2)">Close PO</button>
                        </td>
                    </tr>
                </table>
            </fieldset>
		</div>
		<div class="span7" id="invoice-form" style="display: none;"></div>
	</div>
</body>
</html>
