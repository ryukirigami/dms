
<%@ page import="com.kombos.parts.Invoice" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="invoice_datatables_${idTable}" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <th style="border-bottom: none; padding: 5px; width: 200px;">
            <div>&nbsp;<input type="checkbox" name="checkSelect" onclick="selectAll();"/>&nbsp;Vendor</div>
        </th>
        <th style="border-bottom: none; padding: 5px; width: 200px;">
            <div>No PO</div>
        </th>
        <th style="border-bottom: none; padding: 5px; width: 200px;">
            <div>Tanggal PO</div>
        </th>
        <th style="border-bottom: none; padding: 5px; width: 200px;">
            <div>Tipe Order</div>
        </th>
        <th style="border-bottom: none; padding: 5px; width: 200px;">
            <div>Status</div>
        </th>
        <th style="border-bottom: none; padding: 5px; width: 100px;">
            <div>Last Proses</div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var invoiceTable;
var reloadInvoiceTable;

function searchInvoice(){
    reloadInvoiceTable();
}
function clearInput(){
    $('#search_t156Tanggal_start').val("");
    $('#search_t156Tanggal_end').val("");
    $('#search_noPo').val("");
    $('#radio1').prop('checked',false);
    $('#radio2').prop('checked',false);
    $('#radio3').prop('checked',false);
    reloadInvoiceTable();
}

$(function(){

	reloadInvoiceTable = function() {
		invoiceTable.fnDraw();
	}


	$('#search_t166TglInv').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t166TglInv_day').val(newDate.getDate());
			$('#search_t166TglInv_month').val(newDate.getMonth()+1);
			$('#search_t166TglInv_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			invoiceTable.fnDraw();
	});




$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	invoiceTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});


    var anOpen = [];
    $('#invoice_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = invoiceTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/poMaintenance/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = invoiceTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			invoiceTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

	invoiceTable = $('#invoice_datatables_${idTable}').dataTable({
		"sScrollX": "1203px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "id",
	"mDataProp": "id",
	"bSortable": true,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": false
},
{
	"sName": "vendor",
	"mDataProp": "vendor",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"250px",
	"bVisible": true
},
{
	"sName": "noPo",
	"mDataProp": "noPo",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		return data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "tglPo",
	"mDataProp": "tglPo",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
		return data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "tipeOrder",
	"mDataProp": "tipeOrder",
	"aTargets": [3],
	"mRender": function ( data, type, row ) {
		return data;
	},
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "t164StaOpenCancelClose",
	"mDataProp": "t164StaOpenCancelClose",
	"aTargets": [4],
	"mRender": function ( data, type, row ) {
		return data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "lastProses",
	"mDataProp": "lastProses",
	"aTargets": [5],
	"mRender": function ( data, type, row ) {
		return data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"120px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						var tanggalStart = $("#search_t156Tanggal_start").val();
						var tanggalStartDay = $('#search_t156Tanggal_start_day').val();
						var tanggalStartMonth = $('#search_t156Tanggal_start_month').val();
						var tanggalStartYear = $('#search_t156Tanggal_start_year').val();
                        if(tanggalStart){
							aoData.push(
									{"name": 'tanggalStart', "value": "date.struct"},
									{"name": 'tanggalStart_dp', "value": tanggalStart},
									{"name": 'tanggalStart_day', "value": tanggalStartDay},
									{"name": 'tanggalStart_month', "value": tanggalStartMonth},
									{"name": 'tanggalStart_year', "value": tanggalStartYear}
							);
						}

                        var tanggalEnd = $("#search_t156Tanggal_start").val();
                        var tanggalEndDay = $('#search_t156Tanggal_end_day').val();
						var tanggalEndMonth = $('#search_t156Tanggal_end_month').val();
						var tanggalEndYear = $('#search_t156Tanggal_end_year').val();
						if(tanggalEnd){
							aoData.push(
									{"name": 'tanggalEnd', "value": "date.struct"},
									{"name": 'tanggalEnd_dp', "value": tanggalEnd},
									{"name": 'tanggalEnd_day', "value": tanggalEndDay},
									{"name": 'tanggalEnd_month', "value": tanggalEndMonth},
									{"name": 'tanggalEnd_year', "value": tanggalEndYear}
							);
						}

                        var sta = $('input[name=sta]:checked').val();
                        if(sta){
							aoData.push(
									{"name": 'sta', "value": sta}
							);
						}

						var search_noPo = $('#search_noPo').val();
                        if(search_noPo){
							aoData.push(
									{"name": 'search_noPo', "value": search_noPo}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>



