
<%@ page import="com.kombos.maintable.PartsTransaction" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="partsTransaction_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="98%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partsTransaction.dealerCode.label" default="Company Dealer" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partsTransaction.namaFile.label" default="No Dokumen" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partsTransaction.noPol.label" default="Tanggal" /></div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var partsTransactionTable;
var reloadPartsTransactionTable;
$(function(){
	
	reloadPartsTransactionTable = function() {
		partsTransactionTable.fnDraw();
	}
	$('#clear2').click(function(e){
         $('#search_Tanggal').val("");
        $('#search_Tanggal_day').val("");
        $('#search_Tanggal_month').val("");
        $('#search_Tanggal_year').val("");
        partsTransactionTable.fnDraw();
	});
    $('#clear').click(function(e){
         var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        }

        if(mm<10) {
            mm='0'+mm
        }

        var tgl = dd+"/"+mm+"/"+yyyy;
        $('#search_Tanggal').val(tgl);
        $('#search_Tanggal_day').val(dd);
        $('#search_Tanggal_month').val(mm);
        $('#search_Tanggal_year').val(yyyy);
        partsTransactionTable.fnDraw();
	});
    $('#view').click(function(e){
        e.stopPropagation();
		partsTransactionTable.fnDraw();
	});
	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	partsTransactionTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	partsTransactionTable = $('#partsTransaction_datatables').dataTable({
		"sScrollX": "98%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    if(row['staDel']=="1"){
	        return data;
	    }else{
		    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['docNumberId']+'">&nbsp;&nbsp;'+data;
		}
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "docNumber",
	"mDataProp": "docNumber",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "dateCreated",
	"mDataProp": "dateCreated",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}



],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#companyDealer').val();
						var Tanggal = $('#search_Tanggal').val();
						var TanggalDay = $('#search_Tanggal_day').val();
						var TanggalMonth = $('#search_Tanggal_month').val();
						var TanggalYear = $('#search_Tanggal_year').val();


						var noWo = $('#noWo').val();

                        if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
						    );
						}
						if(Tanggal){
							aoData.push(
									{"name": 'sCriteria_Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_Tanggal_dp', "value": Tanggal},
									{"name": 'sCriteria_Tanggal_day', "value": TanggalDay},
									{"name": 'sCriteria_Tanggal_month', "value": TanggalMonth},
									{"name": 'sCriteria_Tanggal_year', "value": TanggalYear}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
                                $("#keterangan").html(json.cd);
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
