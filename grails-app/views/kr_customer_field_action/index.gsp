<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 27/02/15
  Time: 10:37
--%>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript disappointmentGrition="head">
    var previewData;
    var changeReport;
	$(function(){
        previewData = function(){
                var namaReport = $("#namaReport").val();
                var tanggal = $('#search_tanggal').val();
                var tanggalAkhir = $('#search_tanggalAkhir').val();
                var companyDealer = $('#companyDealer').val();
                var format = $('input[name="formatReport"]:checked').val();
                var staBooking = $('input[name="staBooking"]:checked').val();
                var faStatus = $('input[name="staFA"]:checked').val();
                var bulan = $('#bulanCari').val();
                var bulan2 = $('#bulanCari2').val();
                var tahun = $('#tahunCari').val();
                var jenisFA = "";
                $("input[name='jenisFA']").each(function (){
                    if ($(this).attr("checked")) {
                        jenisFA += $(this).val()+ ',';
                    }
                });
                jenisFA = jenisFA.substr(0, jenisFA.length - 1);
                var mainDealer = "";
                $("input[name='mainDealer']").each(function (){
                    if ($(this).attr("checked")) {
                        mainDealer += '\''+$(this).val()+'\''+',';
                    }
                });
                mainDealer = mainDealer.substr(0, mainDealer.length - 1);

                window.location = "${request.contextPath}/kr_customer_field_action/previewData?namaReport="+namaReport+"&tanggal="+tanggal+"&tanggalAkhir="+tanggalAkhir+"&companyDealer="+companyDealer+"&format="+format+"&bulan="+bulan+"&bulan2="+bulan2+"&tahun="+tahun+"&staBooking="+staBooking+"&faStatus="+faStatus+"&mainDealer="+mainDealer+"&jenisFA="+jenisFA;
        }

        changeReport = function(){
            var namaReport = $("#namaReport").val();
            if(namaReport=="FA_DAILY"){
                $('#search_tanggal').show();
                $('#search_tanggalAkhir').show();
                $('#bulanCari').hide();
                $('#bulanCari2').hide();
                $('#tahunCari').hide();
                $('#filter_statusbooking').show();
                $('#filter_statusFA').show();
            }else {
                $('#bulanCari').show();
                $('#bulanCari2').show();
                $('#tahunCari').show();
                $('#search_tanggalAkhir').hide();
                $('#search_tanggal').hide();
                $('#filter_statusbooking').hide();
                $('#filter_statusFA').hide();
            }

        }
        
        var checkin = $('#search_tanggal').datepicker({
            onRender: function(date) {
                return '';
            }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
            checkin.hide();
            $('#search_tanggalAkhir')[0].focus();
        }).data('datepicker');
        
        var checkout = $('#search_tanggalAkhir').datepicker({
            onRender: function(date) {
                return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');
	});
    </g:javascript>

</head>
<body>
<div class="navbar box-header no-border">
    Kriteria Report -> Customer Field Action
</div><br>
<div class="box">
    <div class="span12" id="appointmentGr-table">
        <div id="form-appointmentGr" class="form-horizontal">
            <table style="width: 100%;padding-left: 5px;padding-right: 5px;">
                <tr style="vertical-align: top;">
                    <td style="width: 50%;padding:5px;">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label" for="filter_periode" style="text-align: left;">
                                    Format Report
                                </label>
                                <div id="filter_format" class="controls">
                                    <g:radioGroup name="formatReport" id="formatReport" values="['x','p']" labels="['Excel','PDF']" value="x" required="" >
                                        ${it.radio} ${it.label}
                                    </g:radioGroup>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="filter_periode" style="text-align: left;">
                                    Company Dealer
                                </label>
                                <div id="filter_company" class="controls">
                                    <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                        <g:select name="companyDealer" id="companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                    </g:if>
                                    <g:else>
                                        <g:select name="companyDealer" id="companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                    </g:else>
                                </div>
                            </div>
                            <div class="control-group" id="filter_statusbooking">
                                <label class="control-label" for="filter_periode" style="text-align: left;">
                                    Status Booking
                                </label>
                                <div id="filter_booking" class="controls">
                                    <g:radioGroup name="staBooking" values="['2','0','1']" value="2" labels="['All','Yes','No']">
                                        ${it.radio} <g:message code="${it.label}" />
                                    </g:radioGroup>
                                </div>
                            </div>
                            <div class="control-group" id="filter_statusFA">
                                <label class="control-label" for="filter_periode" style="text-align: left;">
                                    FA Status
                                </label>
                                <div class="controls">
                                    <g:radioGroup name="staFA" values="['2','0','1']" value="2" labels="['All','Finish','Not Yet']">
                                        ${it.radio} <g:message code="${it.label}" />
                                    </g:radioGroup>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="filter_periode" style="text-align: left;">
                                    Tanggal
                                </label>
                                <div id="filter_periode" class="controls">
                                    <ba:datePicker id="search_tanggal" name="search_tanggal" precision="day" format="dd-MM-yyyy"  value="${new Date()}" style="width:100px" />
                                    <ba:datePicker id="search_tanggalAkhir" name="search_tanggalAkhir" precision="day" format="dd-MM-yyyy" value="${new Date()+1}" />
                                    <select name="bulanCari" id="bulanCari" style="font-size: 12px; display: none;width: 120px">
                                        <option value="1" selected>Januari</option>
                                        <option value="2" >Februari</option>
                                        <option value="3" >Maret</option>
                                        <option value="4" >April</option>
                                        <option value="5" >Mei</option>
                                        <option value="6" >Juni</option>
                                        <option value="7" >Juli</option>
                                        <option value="8" >Agustus</option>
                                        <option value="9" >September</option>
                                        <option value="10" >Oktober</option>
                                        <option value="11" >November</option>
                                        <option value="12" >Desember</option>
                                    </select>
                                    <select name="bulanCari2" id="bulanCari2" style="font-size: 12px; display: none;width: 120px">
                                        <option value="1" ${bulanCari==1 ? "selected" : ""}>Januari</option>
                                        <option value="2" ${bulanCari==2 ? "selected" : ""}>Februari</option>
                                        <option value="3" ${bulanCari==3 ? "selected" : ""}>Maret</option>
                                        <option value="4" ${bulanCari==4 ? "selected" : ""}>April</option>
                                        <option value="5" ${bulanCari==5 ? "selected" : ""}>Mei</option>
                                        <option value="6" ${bulanCari==6 ? "selected" : ""}>Juni</option>
                                        <option value="7" ${bulanCari==7 ? "selected" : ""}>Juli</option>
                                        <option value="8" ${bulanCari==8 ? "selected" : ""}>Agustus</option>
                                        <option value="9" ${bulanCari==9 ? "selected" : ""}>September</option>
                                        <option value="10" ${bulanCari==10 ? "selected" : ""}>Oktober</option>
                                        <option value="11" ${bulanCari==11 ? "selected" : ""}>November</option>
                                        <option value="12" ${bulanCari==12 ? "selected" : ""}>Desember</option>
                                    </select>
                                    <select name="tahunCari" id="tahunCari" style="font-size: 12px; display: none;width: 120px">
                                        <%
                                            int thn = 2014;
                                            int thnSekarang = new Date()?.format("yyyy")?.toInteger();
                                            for(int a=thn;a<=thnSekarang;a++){
                                                String isSelected = "";
                                                if(a==thnSekarang){
                                                    isSelected = "selected";
                                                }
                                                print("<option value='"+a+"' "+isSelected+" >"+a+"</option>");
                                            }
                                        %>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="filter_nama" style="text-align: left;">
                                    Nama Report
                                </label>
                                <div id="filter_nama" class="controls">
                                    <select name="namaReport" id="namaReport" size="5" style="width: 80%; font-size: 12px;" onchange="changeReport();">
                                        <option value="FA_DAILY" selected>CUSTOMER FIELD ACTION (DAILY)</option>
                                        <option value="FA_MONTHLY">CUSTOMER FIELD ACTION (MONTHLY)</option>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                    </td>
                    <td style="width: 50%;padding:5px;">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label" for="filter_periode" style="text-align: left;">
                                    Main Dealer
                                </label>
                                <div class="controls">
                                    <g:checkBox name="mainDealer" value="Auto 2000" checked="false" />&nbsp;Auto 2000<br/>
                                    <g:checkBox name="mainDealer" value="Agung Auto Mall" checked="false"  />&nbsp;Agung Auto Mall<br/>
                                    <g:checkBox name="mainDealer" value="Hadji Kalla" checked="false"  />&nbsp;Hadji Kalla<br/>
                                    <g:checkBox name="mainDealer" value="New Ratna Motor" checked="false"  />&nbsp;New Ratna Motor<br/>
                                    <g:checkBox name="mainDealer" value="Hasjrat Abadi" checked="false"  />&nbsp;Hasjrat Abadi
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="filter_periode" style="text-align: left;">
                                    Jenis FA
                                </label>
                                <div class="controls">
                                    <g:render template="dataTablesJenisFA" />
                                </div>
                            </div>
                        </fieldset>
                    </td>
                </tr>
            </table>

        </div>
        <g:field type="button" onclick="previewData()" class="btn btn-primary create" name="preview"  value="${message(code: 'default.button.upload.label', default: 'Preview')}" />
        <g:field type="button" onclick="window.location.replace('#/home')" class="btn btn-cancel cancel" name="cancel"  value="${message(code: 'default.button.upload.label', default: 'Close')}" />
    </div>
</div>
</body>
</html>
