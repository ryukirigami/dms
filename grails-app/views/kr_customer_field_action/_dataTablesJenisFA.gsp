
<%@ page import="com.kombos.maintable.JenisFA" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="jenisFA_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            &nbsp;<input type="checkbox" class="select-all" />&nbsp;Jenis FA
        </th>
    </tr>
    <tr>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_m184NamaJenisFA" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_m184NamaJenisFA" class="search_init" />
            </div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var jenisFATable;
var reloadJenisFATable;
var reloadJenisFATableSave;
$(function(){

	reloadJenisFATable = function() {
		jenisFATable.fnDraw();
	}

    var recordsJenisFAPerPage = [];//new Array();
    var anJenisFASelected;
    var jmlRecJenisFAPerPage=0;
    var id;


    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	jenisFATable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	jenisFATable = $('#jenisFA_datatables').dataTable({
		"sScrollX": "100%",
		"sScrollY": "250px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsJenisFA = $("#jenisFA_datatables tbody .row-select");
            var jmlJenisFACek = 0;
            var nRow;
            var idRec;
            rsJenisFA.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsJenisFAPerPage[idRec]=="1"){
                    jmlJenisFACek = jmlJenisFACek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsJenisFAPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecJenisFAPerPage = rsJenisFA.length;
            if(jmlJenisFACek==jmlRecJenisFAPerPage && jmlRecJenisFAPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m184NamaJenisFA",
	"mDataProp": "m184NamaJenisFA",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" name="jenisFA" value="'+row['id']+'" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m184KodeJenisFA = $('#filter_m184KodeJenisFA input').val();
						if(m184KodeJenisFA){
							aoData.push(
									{"name": 'sCriteria_m184KodeJenisFA', "value": m184KodeJenisFA}
							);
						}

						var m184NamaJenisFA = $('#filter_m184NamaJenisFA input').val();
						if(m184NamaJenisFA){
							aoData.push(
									{"name": 'sCriteria_m184NamaJenisFA', "value": m184NamaJenisFA}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
	$('.select-all').click(function(e) {

        $("#jenisFA_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsJenisFAPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsJenisFAPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#jenisFA_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsJenisFAPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anJenisFASelected = jenisFATable.$('tr.row_selected');
            if(jmlRecJenisFAPerPage == anJenisFASelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsJenisFAPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>