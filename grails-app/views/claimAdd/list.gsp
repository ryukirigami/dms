
<%@ page import="com.kombos.parts.Claim" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'ClaimAdd.label', default: 'Input Parts Claim')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
        var show;
        var loadForm;
        var shrinkTableLayout;
        var expandTableLayout;
        $(function(){

            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

            $('.box-action').click(function(){
                return false;
            });




            loadForm = function(data, textStatus){
                $('#ClaimAdd-form').empty();
                $('#ClaimAdd-form').append(data);
            }

            shrinkTableLayout = function(){
                if($("#ClaimAdd-table").hasClass("span12")){
                    $("#ClaimAdd-table").toggleClass("span12 span5");
                }
                $("#ClaimAdd-form").css("display","block");
            }

            expandTableLayout = function(){
                if($("#ClaimAdd-table").hasClass("span5")){
                    $("#ClaimAdd-table").toggleClass("span5 span12");
                }
                $("#ClaimAdd-form").css("display","none");
            }


        });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        %{--<li class="separator"></li>--}%
    </ul>
</div>
<div class="box">
    <div class="span12" id="ClaimAdd-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <g:render template="dataTables" />
        <g:field type="button" onclick="tambahReq();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.upload.label', default: 'Add Selected')}" />
        <span style="margin-left: 830px"><button id='closeSpkDetail' type="button" class="btn btn-primary">Close</button></span>
        %{--<div class="modal-footer"><button id='closeSpkDetail' type="button" class="btn btn-primary">Close</button></div>--}%
    </div>
    <div class="span7" id="ClaimAdd-form" style="display: none;"></div>
</div>
</body>
</html>
