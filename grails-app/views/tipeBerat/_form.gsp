<%@ page import="com.kombos.administrasi.TipeBerat" %>
<r:require modules="autoNumeric" />
<g:javascript>
    jQuery(function($) {
        $('.auto').autoNumeric('init',{
            aSep:''
        });
    });
    $('#m026Berat1').blur(function(){
            $("#m026Berat2").val(parseInt($('#m026Berat1').val())+1);
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: tipeBeratInstance, field: 'm026NamaTipeBerat', 'error')} required">
	<label class="control-label" for="m026NamaTipeBerat">
		<g:message code="tipeBerat.m026NamaTipeBerat.label" default="Nama Tipe Berat" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m026NamaTipeBerat" id="m026NamaTipeBerat" required="" value="${tipeBeratInstance?.m026NamaTipeBerat}"/>
	</div>
</div>

<g:javascript>
    document.getElementById('m026NamaTipeBerat').focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: tipeBeratInstance, field: 'm026Berat1', 'error')} required">
	<label class="control-label" for="m026Berat1">
		<g:message code="tipeBerat.m026Berat1.label" default="Berat 1" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m026Berat1" data-l-zero="deny" class="auto" value="${fieldValue(bean: tipeBeratInstance, field: 'm026Berat1')}" required=""/> Kg
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tipeBeratInstance, field: 'm026Berat2', 'error')} required">
	<label class="control-label" for="m026Berat2">
		<g:message code="tipeBerat.m026Berat2.label" default="Berat 2" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m026Berat2" data-l-zero="deny" class="auto"  value="${fieldValue(bean: tipeBeratInstance, field: 'm026Berat2')}" required="" number=""/> Kg
	</div>
</div>

%{--
<div class="control-group fieldcontain ${hasErrors(bean: tipeBeratInstance, field: 'm026Id', 'error')} ">
	<label class="control-label" for="m026Id">
		<g:message code="tipeBerat.m026Id.label" default="Id" />
		
	</label>
	<div class="controls">
	<g:textField name="m026Id" value="${tipeBeratInstance?.m026Id}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tipeBeratInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="tipeBerat.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${tipeBeratInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tipeBeratInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="tipeBerat.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${tipeBeratInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tipeBeratInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="tipeBerat.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${tipeBeratInstance?.lastUpdProcess}"/>
	</div>
</div>

--}%