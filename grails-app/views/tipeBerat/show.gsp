

<%@ page import="com.kombos.administrasi.TipeBerat" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'tipeBerat.label', default: 'Tipe Berat')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteTipeBerat;

$(function(){ 
	deleteTipeBerat=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/tipeBerat/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadTipeBeratTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-tipeBerat" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="tipeBerat"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${tipeBeratInstance?.m026NamaTipeBerat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m026NamaTipeBerat-label" class="property-label"><g:message
					code="tipeBerat.m026NamaTipeBerat.label" default="Nama Tipe Berat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m026NamaTipeBerat-label">
						%{--<ba:editableValue
								bean="${tipeBeratInstance}" field="m026NamaTipeBerat"
								url="${request.contextPath}/TipeBerat/updatefield" type="text"
								title="Enter m026NamaTipeBerat" onsuccess="reloadTipeBeratTable();" />--}%
							
								<g:fieldValue bean="${tipeBeratInstance}" field="m026NamaTipeBerat"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tipeBeratInstance?.m026Berat1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m026Berat1-label" class="property-label"><g:message
					code="tipeBerat.m026Berat1.label" default="Berat 1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m026Berat1-label">
						%{--<ba:editableValue
								bean="${tipeBeratInstance}" field="m026Berat1"
								url="${request.contextPath}/TipeBerat/updatefield" type="text"
								title="Enter m026Berat1" onsuccess="reloadTipeBeratTable();" />--}%
							
								<g:fieldValue bean="${tipeBeratInstance}" field="m026Berat1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tipeBeratInstance?.m026Berat2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m026Berat2-label" class="property-label"><g:message
					code="tipeBerat.m026Berat2.label" default="Berat 2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m026Berat2-label">
						%{--<ba:editableValue
								bean="${tipeBeratInstance}" field="m026Berat2"
								url="${request.contextPath}/TipeBerat/updatefield" type="text"
								title="Enter m026Berat2" onsuccess="reloadTipeBeratTable();" />--}%
							
								<g:fieldValue bean="${tipeBeratInstance}" field="m026Berat2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			%{--
				<g:if test="${tipeBeratInstance?.m026Id}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m026Id-label" class="property-label"><g:message
					code="tipeBerat.m026Id.label" default="M026 Id" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m026Id-label">
						<ba:editableValue
								bean="${tipeBeratInstance}" field="m026Id"
								url="${request.contextPath}/TipeBerat/updatefield" type="text"
								title="Enter m026Id" onsuccess="reloadTipeBeratTable();" />

								<g:fieldValue bean="${tipeBeratInstance}" field="m026Id"/>

						</span></td>

				</tr>
				</g:if>
            --}%
				<g:if test="${tipeBeratInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="tipeBerat.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${tipeBeratInstance}" field="createdBy"
								url="${request.contextPath}/TipeBerat/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadTipeBeratTable();" />--}%
							
								<g:fieldValue bean="${tipeBeratInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tipeBeratInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="tipeBerat.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${tipeBeratInstance}" field="updatedBy"
								url="${request.contextPath}/TipeBerat/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadTipeBeratTable();" />--}%
							
								<g:fieldValue bean="${tipeBeratInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tipeBeratInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="tipeBerat.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${tipeBeratInstance}" field="lastUpdProcess"
								url="${request.contextPath}/TipeBerat/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadTipeBeratTable();" />--}%
							
								<g:fieldValue bean="${tipeBeratInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tipeBeratInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="tipeBerat.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${tipeBeratInstance}" field="dateCreated"
								url="${request.contextPath}/TipeBerat/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadTipeBeratTable();" />--}%
							
								<g:formatDate date="${tipeBeratInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tipeBeratInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="tipeBerat.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${tipeBeratInstance}" field="lastUpdated"
								url="${request.contextPath}/TipeBerat/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadTipeBeratTable();" />--}%
							
								<g:formatDate date="${tipeBeratInstance?.lastUpdated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${tipeBeratInstance?.id}"
					update="[success:'tipeBerat-form',failure:'tipeBerat-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteTipeBerat('${tipeBeratInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
