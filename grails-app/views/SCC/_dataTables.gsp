
<%@ page import="com.kombos.parts.SCC" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="SCC_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SCC.m113TglBerlaku.label" default="Tanggal Penetapan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SCC.m113KodeSCC.label" default="Kode SCC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SCC.m113NamaSCC.label" default="Nama SCC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SCC.m113StaRPP.label" default="Termasuk RPP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SCC.m113Ket.label" default="Keterangan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SCC.staDel.label" default="Sta Del" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SCC.m113ID.label" default="M113 ID" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SCC.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SCC.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SCC.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
		<tr>
		

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m113TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m113TglBerlaku" value="date.struct">
					<input type="hidden" name="search_m113TglBerlaku_day" id="search_m113TglBerlaku_day" value="">
					<input type="hidden" name="search_m113TglBerlaku_month" id="search_m113TglBerlaku_month" value="">
					<input type="hidden" name="search_m113TglBerlaku_year" id="search_m113TglBerlaku_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m113TglBerlaku_dp" value="" id="search_m113TglBerlaku" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m113KodeSCC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m113KodeSCC" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m113NamaSCC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m113NamaSCC" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m113StaRPP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <select name="search_m113StaRPP" id="search_m113StaRPP" style="width:100%" onchange="reloadSCCTable();">
                        <option value="">ALL</option>
                        <option value="0">TERMASUK</option>
                        <option value="1">TIDAK TERMASUK</option>

                    </select>
                </div>
            </th>
	
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m113StaRPP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m113StaRPP" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m113Ket" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m113Ket" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m113ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m113ID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var SCCTable;
var reloadSCCTable;
$(function(){
	
	reloadSCCTable = function() {
		SCCTable.fnDraw();
	}

	var recordsSCCPerPage = [];
    var anSCCSelected;
    var jmlRecSCCPerPage=0;
    var id;

	$('#search_m113TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m113TglBerlaku_day').val(newDate.getDate());
			$('#search_m113TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m113TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			SCCTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	SCCTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	SCCTable = $('#SCC_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsSCC = $("#SCC_datatables tbody .row-select");
            var jmlSCCCek = 0;
            var nRow;
            var idRec;
            rsSCC.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsSCCPerPage[idRec]=="1"){
                    jmlSCCCek = jmlSCCCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsSCCPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecSCCPerPage = rsSCC.length;
            if(jmlSCCCek==jmlRecSCCPerPage && jmlRecSCCPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m113TglBerlaku",
	"mDataProp": "m113TglBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m113KodeSCC",
	"mDataProp": "m113KodeSCC",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m113NamaSCC",
	"mDataProp": "m113NamaSCC",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m113StaRPP",
	"mDataProp": "m113StaRPP",
	"aTargets": [3],
	"mRender": function ( data, type, row ) {
            if(data=="0"){
                  return "Termasuk";
             }else
             {
                  return "Tidak Termasuk";
             }

        },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m113Ket",
	"mDataProp": "m113Ket",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m113ID",
	"mDataProp": "m113ID",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m113TglBerlaku = $('#search_m113TglBerlaku').val();
						var m113TglBerlakuDay = $('#search_m113TglBerlaku_day').val();
						var m113TglBerlakuMonth = $('#search_m113TglBerlaku_month').val();
						var m113TglBerlakuYear = $('#search_m113TglBerlaku_year').val();
						
						if(m113TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_m113TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m113TglBerlaku_dp', "value": m113TglBerlaku},
									{"name": 'sCriteria_m113TglBerlaku_day', "value": m113TglBerlakuDay},
									{"name": 'sCriteria_m113TglBerlaku_month', "value": m113TglBerlakuMonth},
									{"name": 'sCriteria_m113TglBerlaku_year', "value": m113TglBerlakuYear}
							);
						}
	
						var m113KodeSCC = $('#filter_m113KodeSCC input').val();
						if(m113KodeSCC){
							aoData.push(
									{"name": 'sCriteria_m113KodeSCC', "value": m113KodeSCC}
							);
						}
	
						var m113NamaSCC = $('#filter_m113NamaSCC input').val();
						if(m113NamaSCC){
							aoData.push(
									{"name": 'sCriteria_m113NamaSCC', "value": m113NamaSCC}
							);
						}
	
						var m113StaRPP = $('#search_m113StaRPP').val();
						if(m113StaRPP){
							aoData.push(
									{"name": 'sCriteria_m113StaRPP', "value": m113StaRPP}
							);
						}
	
						var m113Ket = $('#filter_m113Ket input').val();
						if(m113Ket){
							aoData.push(
									{"name": 'sCriteria_m113Ket', "value": m113Ket}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var m113ID = $('#filter_m113ID input').val();
						if(m113ID){
							aoData.push(
									{"name": 'sCriteria_m113ID', "value": m113ID}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#SCC_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsSCCPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsSCCPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#SCC_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsSCCPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anSCCSelected = SCCTable.$('tr.row_selected');
            if(jmlRecSCCPerPage == anSCCSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsSCCPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
