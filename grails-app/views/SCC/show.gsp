

<%@ page import="com.kombos.parts.SCC" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'SCC.label', default: 'SCC')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteSCC;

$(function(){ 
	deleteSCC=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/SCC/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadSCCTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-SCC" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="SCC"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${SCCInstance?.m113TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m113TglBerlaku-label" class="property-label"><g:message
					code="SCC.m113TglBerlaku.label" default="Tanggal Penetapan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m113TglBerlaku-label">
						%{--<ba:editableValue
								bean="${SCCInstance}" field="m113TglBerlaku"
								url="${request.contextPath}/SCC/updatefield" type="text"
								title="Enter m113TglBerlaku" onsuccess="reloadSCCTable();" />--}%
							
								<g:formatDate date="${SCCInstance?.m113TglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SCCInstance?.m113KodeSCC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m113KodeSCC-label" class="property-label"><g:message
					code="SCC.m113KodeSCC.label" default="Kode SCC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m113KodeSCC-label">
						%{--<ba:editableValue
								bean="${SCCInstance}" field="m113KodeSCC"
								url="${request.contextPath}/SCC/updatefield" type="text"
								title="Enter m113KodeSCC" onsuccess="reloadSCCTable();" />--}%
							
								<g:fieldValue bean="${SCCInstance}" field="m113KodeSCC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SCCInstance?.m113NamaSCC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m113NamaSCC-label" class="property-label"><g:message
					code="SCC.m113NamaSCC.label" default="Nama SCC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m113NamaSCC-label">
						%{--<ba:editableValue
								bean="${SCCInstance}" field="m113NamaSCC"
								url="${request.contextPath}/SCC/updatefield" type="text"
								title="Enter m113NamaSCC" onsuccess="reloadSCCTable();" />--}%
							
								<g:fieldValue bean="${SCCInstance}" field="m113NamaSCC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SCCInstance?.m113StaRPP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m113StaRPP-label" class="property-label"><g:message
					code="SCC.m113StaRPP.label" default="Termasuk RPP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m113StaRPP-label">
						%{--<ba:editableValue
								bean="${SCCInstance}" field="m113StaRPP"
								url="${request.contextPath}/SCC/updatefield" type="text"
								title="Enter m113StaRPP" onsuccess="reloadSCCTable();" />--}%

								%{--<g:fieldValue bean="${SCCInstance}" field="m113StaRPP"/>--}%
                        <g:if test="${SCCInstance?.m113StaRPP == '0'}">
                            <g:message code="SCC.m113StaRPP.label" default="TERMASUK" />
                        </g:if>
                        <g:else>
                            <g:message code="SCC.m113StaRPP.label" default="TIDAK TERMASUK" />
                        </g:else>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SCCInstance?.m113Ket}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m113Ket-label" class="property-label"><g:message
					code="SCC.m113Ket.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m113Ket-label">
						%{--<ba:editableValue
								bean="${SCCInstance}" field="m113Ket"
								url="${request.contextPath}/SCC/updatefield" type="text"
								title="Enter m113Ket" onsuccess="reloadSCCTable();" />--}%
							
								<g:fieldValue bean="${SCCInstance}" field="m113Ket"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${SCCInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="SCC.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${SCCInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/SCC/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadSCCTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${SCCInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${SCCInstance?.m113ID}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m113ID-label" class="property-label"><g:message--}%
					%{--code="SCC.m113ID.label" default="M113 ID" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m113ID-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${SCCInstance}" field="m113ID"--}%
								%{--url="${request.contextPath}/SCC/updatefield" type="text"--}%
								%{--title="Enter m113ID" onsuccess="reloadSCCTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${SCCInstance}" field="m113ID"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

            <g:if test="${SCCInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="SCC.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${SCCInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/SCC/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadSCCTable();" />--}%

                        <g:fieldValue bean="${SCCInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${SCCInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="SCC.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${SCCInstance}" field="dateCreated"
								url="${request.contextPath}/SCC/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadSCCTable();" />--}%
							
								<g:formatDate date="${SCCInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if><g:if test="${SCCInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="SCC.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${SCCInstance}" field="createdBy"
                                url="${request.contextPath}/SCC/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadSCCTable();" />--}%

                        <g:fieldValue bean="${SCCInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${SCCInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="SCC.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${SCCInstance}" field="lastUpdated"
								url="${request.contextPath}/SCC/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadSCCTable();" />--}%
							
								<g:formatDate date="${SCCInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${SCCInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="SCC.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${SCCInstance}" field="updatedBy"
                                url="${request.contextPath}/SCC/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadSCCTable();" />--}%

                        <g:fieldValue bean="${SCCInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${SCCInstance?.id}"
					update="[success:'SCC-form',failure:'SCC-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteSCC('${SCCInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
