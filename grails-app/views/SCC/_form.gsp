<%@ page import="com.kombos.parts.SCC" %>

<g:javascript>
$(document).ready(function() {
        if($('#m113Ket').val().length>0){
            $('#hitung').text(50 - $('#m113Ket').val().length);
        }

        $('#m113Ket').keyup(function() {
            var len = this.value.length;
            if (len >= 50) {
                this.value = this.value.substring(0, 50);
                len = 50;
            }
            $('#hitung').text(50 - len);
        });
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: SCCInstance, field: 'm113TglBerlaku', 'error')} required">
	<label class="control-label" for="m113TglBerlaku">
		<g:message code="SCC.m113TglBerlaku.label" default="Tanggal Penetapan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="m113TglBerlaku" precision="day" required="true" value="${SCCInstance?.m113TglBerlaku}" format="dd/MM/yyyy" />
	</div>
</div>

<g:if test="${SCCInstance?.m113KodeSCC}">
<div class="control-group fieldcontain ${hasErrors(bean: SCCInstance, field: 'm113KodeSCC', 'error')} required">
	<label class="control-label" for="m113KodeSCC">
		<g:message code="SCC.m113KodeSCC.label" default="Kode SCC" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	${SCCInstance?.m113KodeSCC}
	</div>
</div>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: SCCInstance, field: 'm113NamaSCC', 'error')} required">
	<label class="control-label" for="m113NamaSCC">
		<g:message code="SCC.m113NamaSCC.label" default="Nama SCC" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m113NamaSCC" maxlength="50" required="" value="${SCCInstance?.m113NamaSCC}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: SCCInstance, field: 'm113StaRPP', 'error')} required">
	<label class="control-label" for="m113StaRPP">
		<g:message code="SCC.m113StaRPP.label" default="Termasuk RPP" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:radioGroup name="m113StaRPP"
                      values="['0','1']"
                      labels="['Ya','Tidak']"
                      value="${SCCInstance?.m113StaRPP}" required="">
            ${it.radio} ${it.label}
        </g:radioGroup>
	%{--<g:textField name="m113StaRPP" maxlength="1" required="" value="${SCCInstance?.m113StaRPP}"/>--}%
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: SCCInstance, field: 'm113Ket', 'error')} ">
	<label class="control-label" for="m113Ket">
		<g:message code="SCC.m113Ket.label" default="Keterangan" />
		
	</label>
	<div class="controls">
	<g:textArea name="m113Ket" value="${SCCInstance?.m113Ket}"/>
        <br/>
        <span id="hitung">50</span> Karakter Tersisa.
    </div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: SCCInstance, field: 'staDel', 'error')} required">--}%
	%{--<label class="control-label" for="staDel">--}%
		%{--<g:message code="SCC.staDel.label" default="Sta Del" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="staDel" maxlength="1" required="" value="${SCCInstance?.staDel}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: SCCInstance, field: 'm113ID', 'error')} required">--}%
	%{--<label class="control-label" for="m113ID">--}%
		%{--<g:message code="SCC.m113ID.label" default="M113 ID" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:field name="m113ID" type="number" value="${SCCInstance.m113ID}" required=""/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: SCCInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="SCC.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${SCCInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: SCCInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="SCC.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${SCCInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: SCCInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="SCC.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${SCCInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

<g:javascript>
    document.getElementById("m113TglBerlaku").focus();
</g:javascript>