

<%@ page import="com.kombos.parts.HargaJualNonSPLDPerPart" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'hargaJualNonSPLDPerPart.label', default: 'Per Parts')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteHargaJualNonSPLDPerPart;

$(function(){ 
	deleteHargaJualNonSPLDPerPart=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/hargaJualNonSPLDPerPart/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadHargaJualNonSPLDPerPartTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-hargaJualNonSPLDPerPart" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="hargaJualNonSPLDPerPart"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${hargaJualNonSPLDPerPartInstance?.t160TMT}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t160TMT-label" class="property-label"><g:message
					code="hargaJualNonSPLDPerPart.t160TMT.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t160TMT-label">
						%{--<ba:editableValue
								bean="${hargaJualNonSPLDPerPartInstance}" field="t160TMT"
								url="${request.contextPath}/HargaJualNonSPLDPerPart/updatefield" type="text"
								title="Enter t160TMT" onsuccess="reloadHargaJualNonSPLDPerPartTable();" />--}%
							
								<g:formatDate date="${hargaJualNonSPLDPerPartInstance?.t160TMT}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${hargaJualNonSPLDPerPartInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="hargaJualNonSPLDPerPart.goods.label" default="Goods" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${hargaJualNonSPLDPerPartInstance}" field="goods"
								url="${request.contextPath}/HargaJualNonSPLDPerPart/updatefield" type="text"
								title="Enter goods" onsuccess="reloadHargaJualNonSPLDPerPartTable();" />--}%
							
								${hargaJualNonSPLDPerPartInstance?.goods?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${hargaJualNonSPLDPerPartInstance?.t160PersenKenaikanHarga}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t160PersenKenaikanHarga-label" class="property-label"><g:message
					code="hargaJualNonSPLDPerPart.t160PersenKenaikanHarga.label" default="Persen Kenaikan Harga" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t160PersenKenaikanHarga-label">
						%{--<ba:editableValue
								bean="${hargaJualNonSPLDPerPartInstance}" field="t160PersenKenaikanHarga"
								url="${request.contextPath}/HargaJualNonSPLDPerPart/updatefield" type="text"
								title="Enter t160PersenKenaikanHarga" onsuccess="reloadHargaJualNonSPLDPerPartTable();" />--}%
							
								<g:fieldValue bean="${hargaJualNonSPLDPerPartInstance}" field="t160PersenKenaikanHarga"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${hargaJualNonSPLDPerPartInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="hargaJualNonSPLDPerPart.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${hargaJualNonSPLDPerPartInstance}" field="createdBy"
								url="${request.contextPath}/HargaJualNonSPLDPerPart/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadHargaJualNonSPLDPerPartTable();" />--}%
							
								<g:fieldValue bean="${hargaJualNonSPLDPerPartInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${hargaJualNonSPLDPerPartInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="hargaJualNonSPLDPerPart.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${hargaJualNonSPLDPerPartInstance}" field="updatedBy"
								url="${request.contextPath}/HargaJualNonSPLDPerPart/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadHargaJualNonSPLDPerPartTable();" />--}%
							
								<g:fieldValue bean="${hargaJualNonSPLDPerPartInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${hargaJualNonSPLDPerPartInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="hargaJualNonSPLDPerPart.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${hargaJualNonSPLDPerPartInstance}" field="lastUpdProcess"
								url="${request.contextPath}/HargaJualNonSPLDPerPart/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadHargaJualNonSPLDPerPartTable();" />--}%
							
								<g:fieldValue bean="${hargaJualNonSPLDPerPartInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${hargaJualNonSPLDPerPartInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="hargaJualNonSPLDPerPart.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${hargaJualNonSPLDPerPartInstance}" field="dateCreated"
								url="${request.contextPath}/HargaJualNonSPLDPerPart/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadHargaJualNonSPLDPerPartTable();" />--}%
							
								<g:formatDate date="${hargaJualNonSPLDPerPartInstance?.dateCreated}"  type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${hargaJualNonSPLDPerPartInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="hargaJualNonSPLDPerPart.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${hargaJualNonSPLDPerPartInstance}" field="lastUpdated"
								url="${request.contextPath}/HargaJualNonSPLDPerPart/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadHargaJualNonSPLDPerPartTable();" />--}%
							
								<g:formatDate date="${hargaJualNonSPLDPerPartInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${hargaJualNonSPLDPerPartInstance?.id}"
					update="[success:'hargaJualNonSPLDPerPart-form',failure:'hargaJualNonSPLDPerPart-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteHargaJualNonSPLDPerPart('${hargaJualNonSPLDPerPartInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
