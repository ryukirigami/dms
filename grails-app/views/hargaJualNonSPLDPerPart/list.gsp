
<%@ page import="com.kombos.parts.HargaJualNonSPLDPerPart" %>
<%@ page import="com.kombos.parts.Goods" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'hargaJualNonSPLDPerPart.label', default: 'Per Parts')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var loadPilihPart;
	var editStatus = 0;
	var idHargaJualNonSPLD = 0;
	$(function(){



	$('input[type=submit]').click(function() {
        $(this).attr('disabled', 'disabled');
        $(this).parents('form').submit();
    })
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/hargaJualNonSPLDPerPart/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/hargaJualNonSPLDPerPart/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#hargaJualNonSPLDPerPart-form').empty();
    	$('#hargaJualNonSPLDPerPart-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#hargaJualNonSPLDPerPart-table").hasClass("span12")){
   			$("#hargaJualNonSPLDPerPart-table").toggleClass("span12 span5");
        }
        $("#hargaJualNonSPLDPerPart-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#hargaJualNonSPLDPerPart-table").hasClass("span5")){
   			$("#hargaJualNonSPLDPerPart-table").toggleClass("span5 span12");
   		}
        $("#hargaJualNonSPLDPerPart-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#hargaJualNonSPLDPerPart-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/hargaJualNonSPLDPerPart/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadHargaJualNonSPLDPerPartTable();
    		}
		});
		
   	}

    $("#pilihPartModal").on("show", function() { 
		$("#pilihPartModal .btn").on("click", function(e) {
			$("#pilihPartModal").modal('hide'); 
		});
	}); 
	$("#pilihPartModal").on("hide", function() { 
		$("#pilihPartModal a.btn").off("click");
	});
	
   	loadPilihPartModal = function(idHargaJual){

		if(typeof idHargaJual != 'undefined'){
		        idHargaJualNonSPLD = idHargaJual;
		     //   alert("ada isi "+idHargaJual);

    	}else{
            idHargaJualNonSPLD = 0;
         //   alert("ini "+idHargaJual)
    	}


		$("#pilihPartContent").empty();
	//	expandTableLayout();
		$.ajax({type:'POST', url:'${request.contextPath}/pilihParts',
   			success:function(data,textStatus){
					$("#pilihPartContent").html(data);
				    $("#pilihPartModal").modal({
						"backdrop" : "static",
						"keyboard" : true,
						"show" : true
					}).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});



    }

     pilihPart = function(good) {

     //   alert("id harga jual : "+idHargaJualNonSPLD );
		$.ajax({
    		url:'${request.contextPath}/HargaJualNonSPLDPerPart/getGoods/',
    		type: "GET", // Always use POST when deleting data
    	    data:{idGood : good, idHargaJual : idHargaJualNonSPLD},
    	    complete: function(xhr, status) {
                $("#pilihPartModal").modal('hide');
                $("#pilihPartContent").empty();
                //	shrinkTableLayout();

    		},
    		success:function(data,textStatus){
       		//	loadForm(data, textStatus);
       		   // alert("Sukses : "+data.namaGoods);


       		    $("#goods").val(data.namaGoods);
       		    $("#kodeGoods").val(data.kodeGoods);
            }

		});


   	}

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="hargaJualNonSPLDPerPart-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="hargaJualNonSPLDPerPart-form" style="display: none;"></div>
	</div>
    <div id="pilihPartModal" class="modal fade">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 1200px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 1200px;">
                    <div id="pilihPartContent"/>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="iu-content"></div>
                </div>
                <!-- dialog buttons -->
                <div class="modal-footer"><button id='closepilihPart' type="button" class="btn btn-primary">Close</button></div>
            </div>
        </div>
    </div>
</body>
</html>
