<%@ page import="com.kombos.parts.HargaJualNonSPLDPerPart" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'hargaJualNonSPLDPerPart.label', default: 'Per Parts')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="create-hargaJualNonSPLDPerPart" class="content scaffold-create" role="main">
			<legend><g:message code="default.create.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${hargaJualNonSPLDPerPartInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${hargaJualNonSPLDPerPartInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
			<g:formRemote useToken="true"  class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="reloadHargaJualNonSPLDPerPartTable();" update="hargaJualNonSPLDPerPart-form"
                url="[controller: 'hargaJualNonSPLDPerPart', action:'save']" asynchronous="false">
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
					<a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
					<g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}"
                                    onclick="return confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}');"/>
					</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
