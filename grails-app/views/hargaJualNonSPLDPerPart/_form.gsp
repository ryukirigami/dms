<%@ page import="com.kombos.parts.HargaJualNonSPLDPerPart" %>
<%@ page import="com.kombos.parts.Goods" %>

<g:javascript>

    $(function(){
			$('#namaParts').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/hargaJualNonSPLDPerPart/listParts', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});

		function detailParts(){
                var noGoods = $('#namaParts').val();
                $.ajax({
                    url:'${request.contextPath}/hargaJualNonSPLDPerPart/detailParts?noGoods='+noGoods,
                    type: "POST", // Always use POST when deleting data
                    success : function(data){
                        $('#idGoods').val("");
                        if(data.hasil=="ada"){
                            $('#idGoods').val(data.id);
                        }
                            console.log("idGoods : " + data.id)
                    }
                })
        }
    </g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: hargaJualNonSPLDPerPartInstance, field: 't160TMT', 'error')} required">
	<label class="control-label" for="t160TMT">
		<g:message code="hargaJualNonSPLDPerPart.t160TMT.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
    <g:hiddenField name="idHargaJual"  value="${hargaJualNonSPLDPerPartInstance?.id}"/>
    <ba:datePicker  name="t160TMT" precision="day"  value="${hargaJualNonSPLDPerPartInstance?.t160TMT}" format="dd/mm/yyyy" required="true" />

    </div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: hargaJualNonSPLDPerPartInstance, field: 'goods', 'error')} required">
    <label class="control-label" for="goods">
        <g:message code="hargaJualNonSPLDPerPart.goods.label" default="Goods" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        %{--<g:textField class="typeahead" autocomplete="off" id="goods" name="goods" optionKey="id" value="${hargaJualNonSPLDPerPartInstance?.goods?.m111Nama}"/>--}%
        <g:textField name="namaParts" id="namaParts" class="typeahead" maxlength="11" value="${hargaJualNonSPLDPerPartInstance?.goods?.m111ID?hargaJualNonSPLDPerPartInstance?.goods?.m111ID?.trim()+' || '+hargaJualNonSPLDPerPartInstance?.goods?.m111Nama:""}" autocomplete="off" onblur="detailParts();"/>
        <g:hiddenField name="goodsId" id="idGoods"  value="${hargaJualNonSPLDPerPartInstance?.goods?.id}" />
    </div>
</div>
%{--<div class="control-group fieldcontain">--}%
    %{--<label class="control-label" for="goods">--}%
        %{--<g:message code="hargaJualNonSPLDPerPart.kodegoods.label" default="Kode Parts" />--}%
        %{--<span class="required-indicator">*</span>--}%
    %{--</label>--}%
    %{--<div class="controls">--}%
        %{--<g:textField id="kodeGoods" name="kodeGoods" value="${fieldValue(bean: good, field: 'm111ID')}"  readonly="true"  class="many-to-one"/>--}%
    %{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain">--}%
	%{--<label class="control-label" for="goods">--}%
		%{--<g:message code="hargaJualNonSPLDPerPart.goods.label" default="Parts" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
   %{--<g:textField id="goods" name="goods" value="${fieldValue(bean: good, field: 'm111Nama')}"  readonly="true"  class="many-to-one"/>--}%
        %{--<a class="btn cancel" href="javascript:void(0);"--}%
           %{--onclick="loadPilihPartModal(${hargaJualNonSPLDPerPartInstance?.id});"><g:message--}%
                %{--code="default.button.cariPart.label" default="Cari Part" /></a>--}%
    %{--</div>--}%
%{--</div>--}%
<div class="control-group fieldcontain ${hasErrors(bean: hargaJualNonSPLDPerPartInstance, field: 't160PersenKenaikanHarga', 'error')} required">
	<label class="control-label" for="t160PersenKenaikanHarga">
		<g:message code="hargaJualNonSPLDPerPart.t160PersenKenaikanHarga.label" default="Persen Kenaikan Harga (%)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t160PersenKenaikanHarga" class="persen" value="${fieldValue(bean: hargaJualNonSPLDPerPartInstance, field: 't160PersenKenaikanHarga')}" required=""/>
	</div>
</div>

<g:javascript>

$(function(){
    $('.persen').autoNumeric('init',{vMin:'0',
        vMax:'999',
        mDec: '2',
        aSep:''

    });

})
</g:javascript>
