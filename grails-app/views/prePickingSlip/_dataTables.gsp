
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay" />

<table id="pickingSlip_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover table-selectable" style="table-layout: fixed; width: 100% ">
    <thead>
    <tr>
        <th style="vertical-align: middle;">
            <div style="height: 10px"> </div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Nomor WO</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Tanggal Dibutuhkan</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Nomor Polisi</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>SA</div>
        </th>

    </tr>

    </thead>
</table>

<g:javascript>
var pickingSlipTable;
var reloadPickingSlipTable;
$(function(){

reloadPickingSlipTable = function() {
		pickingSlipTable.fnDraw();
	}
 $('#clear').click(function(e){
        $('#noWo').val("");
        $('#search_Tanggal').val("");
        $('#search_Tanggal_day').val("");
        $('#search_Tanggal_month').val("");
        $('#search_Tanggal_year').val("");
        $('#search_Tanggal2').val("");
        $('#search_Tanggal2_day').val("");
        $('#search_Tanggal2_month').val("");
        $('#search_Tanggal2_year').val("");
        $("input[name=search_status][value=" + 0 + "]").attr('checked', 'checked');
        pickingSlipTable.fnDraw();

	});
	function getCheckedRadioId(name) {
         var elements = document.getElementsByName(name);

         for (var i=0, len=elements.length; i<len; ++i){
                if (elements[i].checked)
                    return elements[i].value;
         }

    }
    $('#view').click(function(e){
        e.stopPropagation();
		pickingSlipTable.fnDraw();
	});
	var anOpen = [];
	$('#pickingSlip_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = pickingSlipTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/prePickingSlip/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = pickingSlipTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			pickingSlipTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});
	
    $('#pickingSlip_datatables_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( pickingSlipTable, nEditing );
            editRow( pickingSlipTable, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( pickingSlipTable, nEditing );
            nEditing = null;
        }
        else {
            editRow( pickingSlipTable, nRow );
            nEditing = nRow;
        }
    } );
    
	reloadPickingSlipTable = function() {
		pickingSlipTable.fnDraw();
	}

 	pickingSlipTable = $('#pickingSlip_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "pickingSlip",
	"mDataProp": "t141Wo",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" value="'+data+'">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"sWidth":"220px",
	"bVisible": true
},
{
	"sName": "pickingSlip",
	"mDataProp": "tanggal",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"220px",
	"bVisible": true
},
{
	"sName": "pickingSlip",
	"mDataProp": "noPol",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"230px",
	"bVisible": true
},
{
	"sName": "reception",
	"mDataProp": "sa",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"230px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var noWo = $('#noWo').val();
                        if(noWo){
                            aoData.push(
                                    {"name": 'sCriteria_noWo', "value": noWo}
                            )
                        }
                        var Tanggal = $('#search_Tanggal').val();
						var TanggalDay = $('#search_Tanggal_day').val();
						var TanggalMonth = $('#search_Tanggal_month').val();
						var TanggalYear = $('#search_Tanggal_year').val();

						var Tanggal2 = $('#search_Tanggal2').val();
						var TanggalDay2 = $('#search_Tanggal2_day').val();
						var TanggalMonth2 = $('#search_Tanggal2_month').val();
						var TanggalYear2 = $('#search_Tanggal2_year').val();

						if(Tanggal){
							aoData.push(
									{"name": 'sCriteria_Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_Tanggal_dp', "value": Tanggal},
									{"name": 'sCriteria_Tanggal_day', "value": TanggalDay},
									{"name": 'sCriteria_Tanggal_month', "value": TanggalMonth},
									{"name": 'sCriteria_Tanggal_year', "value": TanggalYear}
							);
						}


						if(Tanggal2){
							aoData.push(
									{"name": 'sCriteria_Tanggal2', "value": "date.struct"},
									{"name": 'sCriteria_Tanggal2_dp', "value": Tanggal2},
									{"name": 'sCriteria_Tanggal2_day', "value": TanggalDay2},
									{"name": 'sCriteria_Tanggal2_month', "value": TanggalMonth2},
									{"name": 'sCriteria_Tanggal2_year', "value": TanggalYear2}
							);
						}
	                    var statusParts = getCheckedRadioId('search_status')

						if(statusParts){
							aoData.push(
									{"name": 'sCriteria_status', "value": statusParts}
							);
						}else{
						    aoData.push(
									{"name": 'sCriteria_status', "value": '0'}
							);
						}
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});



</g:javascript>



