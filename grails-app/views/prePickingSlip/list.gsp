
<%@ page import="com.kombos.parts.PickingSlipDetail" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="title" value="Pre Picking" />
    <title>${title}</title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();" />
        break;
    case '_DELETE_' :
        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});

         		break;
       }
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/pickingSlip/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);

   		%{--$.ajax({type:'POST', url:'${request.contextPath}/returns/edit/1',--}%
   		$.ajax({type:'POST', url:'${request.contextPath}/pickingSlip/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    loadForm = function(data, textStatus){
		$('#pickingSlip-form').empty();
    	$('#pickingSlip-form').append(data);
   	}

    shrinkTableLayout = function(){
    	if($("#pickingSlip-table").hasClass("span12")){
   			$("#pickingSlip-table").toggleClass("span12 span5");
        }
        $("#pickingSlip-form").css("display","block");
   	}

   	expandTableLayout = function(){
   		if($("#pickingSlip-table").hasClass("span5")){
   			$("#pickingSlip-table").toggleClass("span5 span12");
   		}
        $("#pickingSlip-form").css("display","none");
   	}

   	massDelete = function() {
   		var recordsToDelete = [];
		$("#pickingSlip-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			alert(id)
    			recordsToDelete.push(id);
    		}
		});

		var json = JSON.stringify(recordsToDelete);

		$.ajax({
    		url:'${request.contextPath}/pickingSlip/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadpickingSlipTable();
    		}
		});

   	}

});
        var checkin = $('#search_Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
            checkin.hide();
            $('#search_Tanggal2')[0].focus();
        }).data('datepicker');

        var checkout = $('#search_Tanggal2').datepicker({
            onRender: function(date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');

        printPrePickingSlip = function(){
           checkPrePicking =[];
           var noWO,noPickingSlip,tglPickingSlip,noPolisi,SA,cv;
            $("#pickingSlip-table tbody .row-select").each(function() {
                   if(this.checked){
                     var nRow = $(this).next("input:hidden").val();
					checkPrePicking.push(nRow);
                }
            });
            if(checkPrePicking.length<1 ){
                alert('Silahkan Pilih Salah satu WO Untuk Dicetak');
                return;
            }
           var idPickingSlip =  JSON.stringify(checkPrePicking);
           window.location = "${request.contextPath}/prePickingSlip/printPrePickingSlip?idPickingSlip="+idPickingSlip;
        }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left">${title}</span>
    <ul class="nav pull-right">

        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="pickingSlip-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <fieldset>
            <table style="padding-right: 10px">
                <tr>
                    <td style="width: 130px">
                        <label class="control-label" for="t951Tanggal">
                            <g:message code="auditTrail.wo.label" default="Nomor WO" />&nbsp;
                        </label>&nbsp;&nbsp;
                    </td>
                    <td>
                        <div id="filter_wo" class="controls">
                            <g:textField name="noWo" id="noWo" maxlength="30" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 130px">
                        <label class="control-label" for="t951Tanggal">
                            <g:message code="auditTrail.tanggal.label" default="Tanggal Dibutuhkan" />&nbsp;
                        </label>&nbsp;&nbsp;
                    </td>
                    <td>
                        <div id="filter_m777Tgl" class="controls">
                            <ba:datePicker id="search_Tanggal" name="search_Tanggal" precision="day" format="dd/MM/yyyy"  value="" />
                            &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                            <ba:datePicker id="search_Tanggal2" name="search_Tanggal2" precision="day" format="dd/MM/yyyy"  value=""  />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" for="userRole">
                            <g:message code="auditTrail.role.label" default="Status Part" />&nbsp;
                        </label>&nbsp;&nbsp;
                    </td>
                    <td>
                        <div id="filter_status" class="controls">
                            <input type="radio" name="search_status" id="search_status1"  value="0" />Belum Pre Picking
                            <input type="radio" name="search_status" id="search_status2" value="1" />Sudah Picking
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" >
                        <div class="controls" style="right: 0">
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="view" id="view" >View</button>
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel clear" name="clear" id="clear" >Clear</button>

                        </div>
                    </td>
                </tr>
            </table>

        </fieldset>
        <g:render template="dataTables" />
        <fieldset class="buttons controls" style="padding-top: 10px;">
            <button id='printPickingSlipData' onclick="printPrePickingSlip();" type="button" class="btn btn-primary">Print Pre Picking</button>
        </fieldset>

    </div>
    <div class="span7" id="pickingSlip-form" style="display: none;"></div>
</div>
</body>
</html>
<g:javascript>
    $("input[name=search_status][value=" + 0 + "]").attr('checked', 'checked');
</g:javascript>
