
<%@ page import="com.kombos.administrasi.BahanBakar" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'responProblemFinding.label', default: 'Respon Problem Finding')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var showRespon;
	var loadProblem;
	var loadFormJPBGR;
	var loadFormJPBBP;
	$(function(){ 


	showRespon = function(){
	       checkProblem =[];
               var id;
                $("#responProblemFinding-table tbody .row-select").each(function() {
                    if(this.checked){
                        id = $(this).next("input:hidden").val();
                        checkProblem.push(id);
                    }
                });
                if(checkProblem.length<1 || checkProblem.length>1){
                    alert('Pilih Satu dan hanya boleh satu yang dipilih');
                    return;
                }
                else{
                    var checkProblemMap = JSON.stringify(checkProblem);

                    loadProblem(id);
                }

            checkProblem = [];

	}


	loadProblem = function(id){
        $.ajax({type:'POST', url:'${request.contextPath}/responProblemFinding/check/',
   			data : {id : id},
   			success:function(data,textStatus){
   			console.log("status "+data.status);
   				if(data.status == "BP")
   				    loadFormJPBBP(id);
   				else if(data.status == "GR")
   				    loadFormJPBGR(id);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}


	loadFormJPBGR = function(id){


	 $('#main-content').empty();
        $('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/problemFindingGr/responProblem/',
				type: "POST",
				data : {id : id }, //, tag : "rpf"},
//				dataType: "html",
                complete : function (req, err) {
                    $('#main-content').append(req.responseText);
                     $('#spinner').fadeOut();
                   }

        });
	}

	loadFormJPBBP = function(id){
        $('#main-content').empty();
        $('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/BPProblemFindingList/respondProblem/',
				type: "GET",
				data : {id : id},
				dataType: "html",
                complete : function (req, err) {
                    $('#main-content').append(req.responseText);
                     $('#spinner').fadeOut();
                   }
        });
	}


	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});


    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/responProblemFinding/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    loadForm = function(data, textStatus){
		$('#responProblemFinding-form').empty();
    	$('#responProblemFinding-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#responProblemFinding-table").hasClass("span12")){
   			$("#responProblemFinding-table").toggleClass("span12 span5");
        }
        $("#responProblemFinding-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#responProblemFinding-table").hasClass("span5")){
   			$("#responProblemFinding-table").toggleClass("span5 span12");
   		}
        $("#responProblemFinding-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#responProblemFinding-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/responProblemFinding/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadJobForTeknisiTable();
    		}
		});
		
   	}
    woDetails = function(){
           checkJobForTeknisi =[];

            $("#responProblemFinding-table tbody .row-select").each(function() {
                 if(this.checked){
                    var nRow = $(this).next("input:hidden").val();
					checkJobForTeknisi.push(nRow);
					//alert(nRow)
                }
            });
            if(checkJobForTeknisi.length<1 || checkJobForTeknisi.length>1 ){
                alert('Silahkan Pilih Salah satu Teknisi Untuk Dicetak');
                return;

            }
           var idCheckJobForTeknisi =  JSON.stringify(checkJobForTeknisi);
           window.location = "${request.contextPath}/responProblemFinding/woDetails?idcheckJobForTeknisi="+idCheckJobForTeknisi;
    }

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
	</div>
	<div class="box">
		<div class="span12" id="responProblemFinding-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
             <table style="width: 900px; padding: 5px;">
                 <tr>
                     <td >Nomor WO</td>
                     <td style="width: 400px; ">: <g:textField name="nomorWO" id="nomorWO" maxlength="20" readonly="" value="${noWO}" /></td>
                     <td >Waktu Penyerahan</td>
                     <td>: <g:textField name="waktuPenyerahan" id="waktuPenyerahan" readonly="" value="${waktuPenyerahan}"/></td>
                 </tr>
                 <tr>
                     <td >Tanggal WO</td>
                     <td style="width: 400px; ">: <g:textField name="tglWO" id="tglWO" readonly="" value="${tanggal}" /></td>
                     <td >Foreman</td>
                     <td>: <g:textField name="foreman" id="foreman" readonly="" value="${foreman}"/></td>
                 </tr>
                 <tr>
                     <td >Nomor Polisi</td>
                     <td style="width: 400px; ">: <g:textField name="nopol" id="nopol" readonly="" value="${nopol}" /></td>
                     <td >Service Advisor</td>
                     <td>: <g:textField name="serviceAdvisor" id="serviceAdvisor" readonly="" value="${serviceAdvisor}" /></td>
                 </tr>
             </table>


			<g:render template="dataTables" />
            <fieldset class="buttons controls" style="padding-top: 10px;">
                <button id='printClaimData' onclick="showRespon()" type="button" class="btn btn-primary">Respon Problem</button>
            </fieldset>
		</div>
		<div class="span7" id="responProblemFinding-form" style="display: none;"></div>
	</div>
</body>
</html>
