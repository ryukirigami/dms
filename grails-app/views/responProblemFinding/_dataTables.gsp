
<%@ page import="com.kombos.administrasi.BahanBakar" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="ResponProblemFinding_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="ResponProblemFinding.teknisi.label" default="Jam Kirim Masalah" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="ResponProblemFinding.m110NamaJobForTeknisi.label" default="Nama Job" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="ResponProblemFinding.m110ID.label" default="Jenis Problem" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="ResponProblemFinding.m110NamaJobForTeknisi.label" default="Status" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="ResponProblemFinding.m110ID.label" default="Deskripsi Problem" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="ResponProblemFinding.m110NamaJobForTeknisi.label" default="Catatan Foreman" /></div>
            </th>
		</tr>
		%{--<tr>--}%
		%{----}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_teknisi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_teknisi" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_nopol" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_nopol" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%


            %{--<th style="border-top: none;padding: 5px;">--}%
                %{--<div id="filter_stallParkir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
                    %{--<input type="text" name="search_stallParkir" class="search_init" />--}%
                %{--</div>--}%
            %{--</th>--}%

            %{--<th style="border-top: none;padding: 5px;">--}%
                %{--<div id="filter_noWo" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
                    %{--<input type="text" name="search_noWo" class="search_init" />--}%
                %{--</div>--}%
            %{--</th>--}%

            %{--<th style="border-top: none;padding: 5px;">--}%
                %{--<div id="filter_tglTarget" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >--}%
                    %{--<input type="hidden" name="search_tglTarget" value="date.struct">--}%
                    %{--<input type="hidden" name="search_tglTarget_day" id="search_tglTarget_day" value="">--}%
                    %{--<input type="hidden" name="search_tglTarget_month" id="search_tglTarget_month" value="">--}%
                    %{--<input type="hidden" name="search_tglTarget_year" id="search_tglTarget_year" value="">--}%
                    %{--<input type="text" data-date-format="dd/mm/yyyy" name="search_tglTarget_dp" value="" id="search_tglTarget" class="search_init">--}%
                %{--</div>--}%
            %{--</th>--}%

            %{--<th style="border-top: none;padding: 5px;">--}%
                %{--<div id="filter_tglSelesai" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >--}%
                    %{--<input type="hidden" name="search_tglSelesai" value="date.struct">--}%
                    %{--<input type="hidden" name="search_tglSelesai_day" id="search_tglSelesai_day" value="">--}%
                    %{--<input type="hidden" name="search_tglSelesai_month" id="search_tglSelesai_month" value="">--}%
                    %{--<input type="hidden" name="search_tglSelesai_year" id="search_tglSelesai_year" value="">--}%
                    %{--<input type="text" data-date-format="dd/mm/yyyy" name="search_tglSelesai_dp" value="" id="search_tglSelesai" class="search_init">--}%
                %{--</div>--}%
            %{--</th>--}%
		%{--</tr>--}%
	</thead>
</table>

<g:javascript>
var JobForTeknisiTable;
var reloadJobForTeknisiTable;
$(function(){
	
	reloadJobForTeknisiTable = function() {
		JobForTeknisiTable.fnDraw();
	}


	$('#search_tglTarget').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglTarget_day').val(newDate.getDate());
			$('#search_tglTarget_month').val(newDate.getMonth()+1);
			$('#search_tglTarget_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			JobForTeknisiTable.fnDraw();
	});


	$('#search_tglSelesai').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglSelesai_day').val(newDate.getDate());
			$('#search_tglSelesai_month').val(newDate.getMonth()+1);
			$('#search_tglSelesai_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			JobForTeknisiTable.fnDraw();
	});
$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	JobForTeknisiTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	JobForTeknisiTable = $('#ResponProblemFinding_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "jamKirimMasalah",
	"mDataProp": "jamKirimMasalah",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "namaJob",
	"mDataProp": "namaJob",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "jenisProblem",
	"mDataProp": "jenisProblem",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "statusKendaraan",
	"mDataProp": "statusKendaraan",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "deskripsiProblem",
	"mDataProp": "deskripsiProblem",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "catatanForeman",
	"mDataProp": "catatanForeman",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}



],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        var noWO = '${noWO}';
                        if(noWO){
                            aoData.push(
                                {"name":'noWO', "value": noWO}
                            );
                        }

	
						var teknisi = $('#filter_teknisi input').val();
						if(teknisi){
							aoData.push(
									{"name": 'sCriteria_teknisi', "value": teknisi}
							);
						}
	
						var nopol = $('#filter_nopol input').val();
						if(nopol){
							aoData.push(
									{"name": 'sCriteria_nopol', "value": nopol}
							);
						}

						var stallParkir = $('#filter_stallParkir input').val();
						if(stallParkir){
							aoData.push(
									{"name": 'sCriteria_stallParkir', "value": stallParkir}
							);
						}

						var noWo = $('#filter_noWo input').val();
						if(noWo){
							aoData.push(
									{"name": 'sCriteria_noWo', "value": noWo}
							);
						}

                        var tglTarget = $('#search_tglTarget').val();
						var tglTargetDay = $('#search_tglTarget_day').val();
						var tglTargetMonth = $('#search_tglTarget_month').val();
						var tglTargetYear = $('#search_tglTarget_year').val();

						if(tglTarget){
							aoData.push(
									{"name": 'sCriteria_tglTarget', "value": "date.struct"},
									{"name": 'sCriteria_tglTarget_dp', "value": tglTarget},
									{"name": 'sCriteria_tglTarget_day', "value": tglTargetDay},
									{"name": 'sCriteria_tglTarget_month', "value": tglTargetMonth},
									{"name": 'sCriteria_tglTarget_year', "value": tglTargetYear}
							);
						}

						var tglSelesai = $('#search_tglSelesai').val();
						var tglSelesaiDay = $('#search_tglSelesai_day').val();
						var tglSelesaiMonth = $('#search_tglSelesai_month').val();
						var tglSelesaiYear = $('#search_tglSelesai_year').val();

						if(tglSelesai){
							aoData.push(
									{"name": 'sCriteria_tglSelesai', "value": "date.struct"},
									{"name": 'sCriteria_tglSelesai_dp', "value": tglSelesai},
									{"name": 'sCriteria_tglSelesai_day', "value": tglSelesaiDay},
									{"name": 'sCriteria_tglSelesai_month', "value": tglSelesaiMonth},
									{"name": 'sCriteria_tglSelesai_year', "value": tglSelesaiYear}
							);
						}
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
