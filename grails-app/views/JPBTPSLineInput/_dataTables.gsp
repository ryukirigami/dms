
<%@ page import="com.kombos.board.JPB" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="JPB_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="4">
                <div>FOREMAN</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="3" rowspan="2">
                <div>JOB PROGRESS BOARD (JPB)<br />BODY & PAINT</div>
            </th>

            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="21">
                <div><p id="lblTglView" class="lblTglView">${params?.tglHeader}</p></div>
            </th>
        </tr>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="21">
                <div>JAM</div>
            </th>
        </tr>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="2">
                <div>Group</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="2">
                <div>Teknisi</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="2">
                <div>Stall</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>07:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>08:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>09:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>10:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>11:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>12:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>13:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>14:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>15:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>16:00</div>
            </th>
        </tr>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div>&nbsp;</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
        </tr>

	</thead>
</table>

<g:javascript>
var JPBTable;
var reloadJPBTable;
var getForemanInfo;
var getTeknisiInfo;
$(function(){

	 getForemanInfo = function(id){
    //  $(document).tooltip({ content: "Awesome title!" }).show();

        var content = "nocontent";
            $.ajax({type:'POST', url:'${request.contextPath}/groupManPower/getManPowerInfo/'+id,
                success:function(data,textStatus){
                    if(data){

                       content = data.namaLengkap + " - " + data.jabatan;
                       content = content +"</br>Group : "+data.group;
                       content = content +"</br>Anggota Group : </br>"

                       if(data.anggota){
                            jQuery.each(data.anggota, function (index, value) {
                                content = content + "</br>"+value;
                            });
                       }


                     $("#foreman"+id).tooltip({
                        html : true,
                        title : content,
                        position: 'center right'
                    });

                    }

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){

                }
            });

    }

    getTeknisiInfo = function(idTeknisi, idJPB){
    //  $(document).tooltip({ content: "Awesome title!" }).show();

         var content = "nocontent";
        $.ajax({type:'GET', url:'${request.contextPath}/groupManPower/getTeknisiJPB/',
            data : {id : idTeknisi, idJPB : idJPB},
   			success:function(data,textStatus){
   				if(data){

   				   content = data.namaLengkap + " - " + data.jabatan;
                   content = content +"</br>Group : "+data.group;
                   content = content +"</br>Kepala Group : "+data.kepalaGroup;
                   content = content +"</br></br>";
                   content = content +"</br>Previous Job : "+data.prevJob;
                   content = content +"</br>No. WO : "+data.prevNoWO;
                   content = content +"</br>"+data.prevTime;

                   content = content +"</br></br>Current Job : "+data.currJob;
                   content = content +"</br>No. WO : "+data.currNoWO;
                   content = content +"</br>"+data.currTime;

                   content = content +"</br></br>Next Job : "+data.nextJob;
                   content = content +"</br>No. WO : "+data.nextNoWO;
                   content = content +"</br>"+data.nextTime;




   				 $("#teknisi"+idTeknisi).tooltip({
                    html : true,
                    title : content,
                    position: 'center right'
                });

   				}

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){

   			}
   		});

    }


	reloadJPBTable = function() {
		JPBTable.fnDraw();
	}

	
	$('#search_t351TglJamApp').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t351TglJamApp_day').val(newDate.getDate());
			$('#search_t351TglJamApp_month').val(newDate.getMonth()+1);
			$('#search_t351TglJamApp_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			JPBTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	JPBTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	JPBTable = $('#JPB_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                $(nRow).children().each(function(index, td) {
                    if(index >= 3 && index <= 22) {
                        if ($(td).html() === "BREAK") {
                            $(td).css("background-color", "grey");

                        }
                        if ($(td).html() === "V1") {
                            $(td).css("background-color", "#078DC6");
                        }
                        if ($(td).html() === "V") {
                            $(td).css("background-color", "#078DC6");
                        }
                        if ($(td).html() === "W1") {
                             $(td).css("background-color", "#FFDE00");
                        }
                        if ($(td).html() === "W") {
                            $(td).css("background-color", "#FFDE00");
                        }
                        if ($(td).html() === "X1") {
                            $(td).css("background-color", "#06B33A");
                        }
                        if ($(td).html() === "X") {
                            $(td).css("background-color", "#06B33A");
                        }
                        if ($(td).html() === "Y1") {
                            $(td).css("background-color", "#FF3229");
                        }
                        if ($(td).html() === "Y") {
                            $(td).css("background-color", "#FF3229");
                        }
                        if ($(td).html() === "Z1") {
                             $(td).css("background-color", "#ffe2e2");
                        }
                        if ($(td).html() === "Z") {
                            $(td).css("background-color", "#ffe2e2");
                        }
                        $(td).html("");
                    }
                });

			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "foreman",
	"mDataProp": "foreman",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"mRender": function ( data, type, row ) {
	    return '<a href="#" id="foreman'+row['foremanId']+'" onmouseover="getForemanInfo('+row['foremanId']+')" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
	},
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "group",
	"mDataProp": "group",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "teknisi",
	"mDataProp": "teknisi",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
	    return '<a href="#" id="teknisi'+row['teknisiId']+'" onmouseover="getTeknisiInfo('+row['teknisiId']+','+row['jpbId']+')" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "stall",
	"mDataProp": "stall",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "jam7",
	"mDataProp": "jam7",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam730",
	"mDataProp": "jam730",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam8",
	"mDataProp": "jam8",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam830",
	"mDataProp": "jam830",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam9",
	"mDataProp": "jam9",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam930",
	"mDataProp": "jam930",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam10",
	"mDataProp": "jam10",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam1030",
	"mDataProp": "jam1030",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam11",
	"mDataProp": "jam11",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam1130",
	"mDataProp": "jam1130",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam12",
	"mDataProp": "jam12",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam1230",
	"mDataProp": "jam1230",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam13",
	"mDataProp": "jam13",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam1330",
	"mDataProp": "jam1330",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam14",
	"mDataProp": "jam14",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam1430",
	"mDataProp": "jam1430",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam15",
	"mDataProp": "jam15",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam1530",
	"mDataProp": "jam1530",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam16",
	"mDataProp": "jam16",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam1630",
	"mDataProp": "jam1630",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            var tglView = $('#search_tglView').val();
            var tglViewDay = $('#search_tglView_day').val();
            var tglViewMonth = $('#search_tglView_month').val();
            var tglViewYear = $('#search_tglView_year').val();
            //alert("tglView=" + tglView + " tglViewDay=" + tglViewDay);
            if(tglView){
                aoData.push(
                        {"name": 'sCriteria_tglView', "value": "date.struct"},
                        {"name": 'sCriteria_tglView_dp', "value": tglView},
                        {"name": 'sCriteria_tglView_day', "value": tglViewDay},
                        {"name": 'sCriteria_tglView_month', "value": tglViewMonth},
                        {"name": 'sCriteria_tglView_year', "value": tglViewYear}
                );
            }
            aoData.push(
                    {"name": 'aksi', "value": "input"}
            );

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    new FixedColumns( JPBTable, {
		"iLeftWidth": 150,
		//"iLeftColumns": 2,
		"fnDrawCallback": function ( left, right ) {
			var that = this, groupVal = null, matches = 0, heights = [], index = -1;

			// Get the heights of the cells and remove redundant ones
			$('tbody tr td', left.body).each( function ( i ) {
				var currVal = this.innerHTML;

				// Reset values on new cell data.
				if (currVal != groupVal) {
					groupVal = currVal;
					index++;
					heights[index] = 0;
					matches = 0;
				} else  {
					matches++;
				}

				heights[ index ] += $(this.parentNode).height();
				if ( currVal == groupVal && matches > 0 ) {
					this.parentNode.parentNode.removeChild(this.parentNode);
				}
			} );

			// Now set the height of the cells which remain, from the summed heights
			$('tbody tr td', left.body).each( function ( i ) {
				that.fnSetRowHeight( this.parentNode, heights[ i ] );
			} );
		}
	} );

});


</g:javascript>


			
