
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
</g:javascript>

<table id="sa_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="335px">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <div>&nbsp;<input type="checkbox" class="select-all" />&nbsp;
                <g:message code="timeTrackingGR.sa.label" default="Service Advisor" /></div>
        </th>
    </tr>
    <tr>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_sa" style="padding-top: 0px;position:relative; margin-top: 0px;width: 320px;">
                <input type="text" name="search_sa" class="search_init" />
            </div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var saTable;
var reloadSaTable;
$(function(){
	
	reloadSaTable = function() {
		saTable.fnDraw();
	}

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	saTable.fnDraw();
		}
	});

	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	saTable = $('#sa_datatables').dataTable({
		"sScrollX": "335px",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSAList")}",
		"aoColumns": [

{
	"sName": "t015NamaBoard",
	"mDataProp": "namaSa",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"335px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
            var sa = $('#filter_sa input').val();
            if(sa){
                aoData.push(
                        {"name": 'sa', "value": sa}
                );
            }
	
            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
                    });
		}			
	});
	
});
</g:javascript>


			
