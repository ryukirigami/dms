
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
    function selectAll(){
        if($('input[name=cekJobDispatch]').is(':checked')){
            $(".sub-centang-jobdispatch").attr("checked",true);
        }else {
            $(".sub-centang-jobdispatch").attr("checked",false);
        }
    }

    $('#jobdispatch_datatables tbody tr').on('click', function () {
        console.log('klik');
        var jum=0;
        $("#jobdispatch_datatables tbody .sub-centang-jobdispatch").each(function() {
            if(this.checked){
                jum++;
            }
        });
        var rowCount = $('#jobdispatch_datatables tbody tr').length;
        if($(this).find('.sub-centang-jobdispatch').is(":checked")){
            if(rowCount == jum){
                $('.centang-jobdispatch').attr('checked', true);
            }
        } else {
            $('.centang-jobdispatch').attr('checked', false);
        }
    });
</g:javascript>

<table id="jobdispatch_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
        <tr>

            <th>
                &nbsp;<input type="checkbox" name="cekJobDispatch" id="cekJobDispatch" class="centang-jobdispatch" onclick="selectAll();" />&nbsp;
                <g:message code="timeTrackingGR.jenisPekerjaan.label" default="Job Dispatch Status" />
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                &nbsp;<input type="checkbox" class="sub-centang-jobdispatch" />&nbsp;
                Insurance
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;<input type="checkbox" class="sub-centang-jobdispatch" />&nbsp;
                Reception
            </td>
        </tr>        
    </tbody>
</table>