
<%@ page import="com.kombos.baseapp.utils.DatatablesUtilService; com.kombos.board.JPB" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="MechanicBoard_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="1">
                <div><p id="lblJamSekarang" class="lblJamSekarang"></p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="41">
                <div><p id="lblTglView" class="lblTglView">${params?.tglHeader}</p></div>
            </th>
        </tr>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="3">
                <div>Stall</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="3">
                <div>Teknisi</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="40">
                <div>JAM</div>
            </th>
        </tr>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
                <div>07.00 - 08.00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
                <div>08:00 - 09.00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
                <div>09:00 - 10.00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
                <div>10:00 - 11.00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
                <div>11:00 - 12.00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
                <div>12:00 - 13.00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
                <div>13:00 - 14.00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
                <div>14:00 - 15.00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
                <div>15:00 - 16.00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
                <div>16:00 - 17.00</div>
            </th>
        </tr>

    <tr>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
        <td style="border-bottom: none;padding: 5px;">
            <div> </div>
        </td>
    </tr>


    </thead>
</table>

<g:javascript>
    function checkTime(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        // add a zero in front of numbers<10
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('lblJamSekarang').innerHTML = h + ":" + m + ":" + s;
        t = setTimeout(function () {
            startTime()
        }, 500);
    }
    startTime();
var mechanicBoardTable;
var reloadMechanicBoardTable;


$(function(){

	reloadMechanicBoardTable = function() {
		mechanicBoardTable.fnDraw();
	}

	
	$('#search_t351TglJamApp').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t351TglJamApp_day').val(newDate.getDate());
			$('#search_t351TglJamApp_month').val(newDate.getMonth()+1);
			$('#search_t351TglJamApp_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			mechanicBoardTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	mechanicBoardTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	mechanicBoardTable = $('#MechanicBoard_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                $(nRow).children().each(function(index, td) {
                    var pertama = false;
                    if(index > 1 && index <= 41) {
                        s = $(td).find('span');
                        <%
                            int indLoop = 0;
                            for (cari in listKategori){
                        %>
                        if (s.hasClass("${cari}")) {
                            $(td).css("background-color", "${listWarna[indLoop]}");
                        <%  if(cari.toString().indexOf("1")<0){%>
                            $(td).html("");
                        <%  }
                            indLoop++;
                        %>
                        }
                        <%
                            }
                        %>
                    if (s.hasClass("NOW")) {
                       $(td).css("background-color", "#fabb88");
                    }

               }

              $(td).css("color", "black");
               if(aData["actual"] === true){
                   if (s.hasClass("A1")) {
                       $(td).css("background-color", "red");
                        $(td).css("color", "white");
                   } else if (s.hasClass("NOW")) {
                       $(td).css("background-color", "#fabb88");
                    } else
                   if (s.hasClass("A")) {
                       $(td).css("background-color", "red");
                        $(td).css("color", "white");
                       $(td).html("");
                   }else{
                       $(td).html("");
                   }
               }


               });
   return nRow;
   },
   "bSort": true,
   "bProcessing": true,
   "bServerSide": true,
   "sServerMethod": "POST",
   "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
   "sAjaxSource": "${g.createLink(action: "datatablesList")}",
                "aoColumns": [

        {
            "sName": "stall",
            "mDataProp": "stall",
            "aTargets": [3],
            "bSearchable": true,
            "bSortable": true,
            "sWidth":"120px",
            "bVisible": true
        }
        ,
        {
            "sName": "teknisi",
            "mDataProp": "teknisi",
            "aTargets": [2],
            "bSearchable": true,
            "bSortable": true,
            "sWidth":"120px",
            "bVisible": true
        }
        ,

        {
            "sName": "jam7",
            "mDataProp": "jam7",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam7"]+'</span>';
            },
            "bVisible": true
        }
        ,

        {
            "sName": "jam715",
            "mDataProp": "jam715",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam715"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam730",
            "mDataProp": "jam730",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam730"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam745",
            "mDataProp": "jam745",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam745"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam8",
            "mDataProp": "jam8",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam8"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam815",
            "mDataProp": "jam815",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam815"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam830",
            "mDataProp": "jam830",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam830"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam845",
            "mDataProp": "jam845",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam845"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam9",
            "mDataProp": "jam9",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam9"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam915",
            "mDataProp": "jam915",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam915"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam930",
            "mDataProp": "jam930",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam930"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam945",
            "mDataProp": "jam945",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam945"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam10",
            "mDataProp": "jam10",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam10"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1015",
            "mDataProp": "jam1015",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1015"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1030",
            "mDataProp": "jam1030",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1030"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1045",
            "mDataProp": "jam1045",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1045"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam11",
            "mDataProp": "jam11",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam11"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1115",
            "mDataProp": "jam1115",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1115"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1130",
            "mDataProp": "jam1130",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1130"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1145",
            "mDataProp": "jam1145",
            "bSearchable": true,
            "bSortable": false,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1145"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam12",
            "mDataProp": "jam12",
            "bSearchable": true,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam12"]+'</span>';
            },
            "bSortable": false,
            "bVisible": true
        }
        ,
        {
            "sName": "jam1215",
            "mDataProp": "jam1215",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1215"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1230",
            "mDataProp": "jam1230",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1230"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1245",
            "mDataProp": "jam1245",
            "bSearchable": true,
            "sWidth":"10px",
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1245"]+'</span>';
            },
            "bSortable": false,
            "bVisible": true
        }
        ,
        {
            "sName": "jam13",
            "mDataProp": "jam13",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam13"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1315",
            "mDataProp": "jam1315",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1315"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1330",
            "mDataProp": "jam1330",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1330"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1345",
            "mDataProp": "jam1345",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1345"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam14",
            "mDataProp": "jam14",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam14"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1415",
            "mDataProp": "jam1415",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1415"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1430",
            "mDataProp": "jam1430",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1430"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1445",
            "mDataProp": "jam1445",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1445"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam15",
            "mDataProp": "jam15",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam15"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1515",
            "mDataProp": "jam1515",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1515"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1530",
            "mDataProp": "jam1530",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1530"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1545",
            "mDataProp": "jam1545",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1545"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam16",
            "mDataProp": "jam16",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam16"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1615",
            "mDataProp": "jam1615",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1615"]+'</span>';
            },
            "bVisible": true
        },
        {
            "sName": "jam1630",
            "mDataProp": "jam1630",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1630"]+'</span>';
            },
            "bVisible": true
        }
        ,
        {
            "sName": "jam1645",
            "mDataProp": "jam1645",
            "bSearchable": true,
            "sWidth":"10px",
            "bSortable": false,
            "mRender": function ( data, type, row ) {
                return '<span class='+data+'>' + row["noPolJam1645"]+'</span>';
            },
            "bVisible": true
        }


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
               var sCriteria_cabang = $('#input_companyDealer').val();
               if(sCriteria_cabang){
                    aoData.push(
                        {"name": 'input_companyDealer', "value": sCriteria_cabang}
                    );
               }

                var tglView = $('#search_tglView').val();
                var tglViewDay = $('#search_tglView_day').val();
                var tglViewMonth = $('#search_tglView_month').val();
                var tglViewYear = $('#search_tglView_year').val();
                if(tglView){
                     aoData.push(
                         {"name": 'sCriteria_tglView', "value": "date.struct"},
                         {"name": 'sCriteria_tglView_dp', "value": tglView},
                         {"name": 'sCriteria_tglView_day', "value": tglViewDay},
                         {"name": 'sCriteria_tglView_month', "value": tglViewMonth},
                         {"name": 'sCriteria_tglView_year', "value": tglViewYear}
                     );
                }

            aoData.push(
                    {"name": 'aksi', "value": "view"}
            );

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    var timer = setInterval(function() {
        mechanicBoardTable.fnDraw();
    }, 180000);

});


</g:javascript>


			
