<%@ page import="com.kombos.administrasi.KategoriJob; com.kombos.board.JPB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="JPB" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var jpbView;
			var edit;
			$(function(){ 
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('JPB');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('JPB', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('JPB', '${request.contextPath}/JPB/massdelete', mechanicBoardTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

                $('#search_tglView').datepicker().on('changeDate', function(ev) {
                    var newDate = new Date(ev.date);
                    $('#search_tglView_day').val(newDate.getDate());
                    $('#search_tglView_month').val(newDate.getMonth()+1);
                    $('#search_tglView_year').val(newDate.getFullYear());
                    $(this).datepicker('hide');
        //			var oTable = $('#JPB_datatables').dataTable();
        //            oTable.fnReloadAjax();
            });

				jpbView = function(){
                    var oTable = $('#MechanicBoard_datatables').dataTable();
                    oTable.fnReloadAjax();
                    var from = $("#search_tglView").val().split("/");
                    var theDate = new Date();
                    theDate.setFullYear(from[2], from[1] - 1, from[0]);
                    //alert("theDate=" + theDate);
                    var m_names = new Array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                    var formattedDate =  theDate.getDate() + " " + m_names[theDate.getMonth()] + " " + theDate.getFullYear();
                    document.getElementById("lblTglView").innerHTML = formattedDate;
                }

                var reloadAll = function(){
                    var table = $('#MechanicBoard_datatables');
                    if(table.length > 0){
                        jpbInput();
                        setTimeout(function(){reloadAll()}, 10000);
                    }
                }

				show = function(id) {
					showInstance('JPB','${request.contextPath}/JPB/show/'+id);
				};

				edit = function(id) {
					editInstance('JPB','${request.contextPath}/JPB/edit/'+id);
				};
});
</g:javascript>
	</head>
	<body>
    <div class="navbar box-header no-border">
   		<span class="pull-left">Mechanical Board</span>
   	</div>

	<div class="box">
        <legend style="font-size: small">Mechanical Board</legend>

        <table style="width: 100%;border: 0px">
            <tr>
                <td>
                    <label class="control-label" for="lbl_companyDealer">
                        Nama Workshop
                    </label>
                </td>
                <td>
                    <div id="lbl_companyDealer" class="controls">
                        %{--<g:select id="input_companyDealer" disabled="" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop")}}" optionKey="id" required="" value="${session?.userCompanyDealer?.id}" class="many-to-one"/>--}%
                        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                            <g:select name="companyDealer.id" id="input_companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                        </g:if>
                        <g:else>
                            <g:select name="companyDealer.id" id="input_companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                        </g:else>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="lbl_tglView" style="width: 100px">
                        Tanggal
                    </label>
                </td>
                <td>
                    <div id="lbl_tglView" style="margin-bottom: 10px;">
                        <div id="filter_tglView" >
                            <input type="hidden" name="search_tglView" value="date.struct">
                            <input type="hidden" name="search_tglView_day" id="search_tglView_day" value="">
                            <input type="hidden" name="search_tglView_month" id="search_tglView_month" value="">
                            <input type="hidden" name="search_tglView_year" id="search_tglView_year" value="">
                            <input type="text" data-date-format="dd/mm/yyyy" name="search_tglView_dp" value="${new java.util.Date().format('dd/MM/yyyy')}" id="search_tglView" class="search_init" style="width: 120px;">
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button class="btn btn-primary" id="buttonView" onclick="jpbView()">View</button>
                </td>
            </tr>
        </table>

        <br/>

        <g:render template="dataTables" />

        <br/>

        <table>
            <tr>
                <%
                    def cariData = KategoriJob.createCriteria().list {order("m055KategoriJob")};
                    cariData.each {
                %>
                <td style="padding: 10px;align-content: center">
                    <table class="table table-bordered" style="width: 10%;background-color: ${it?.m055WarnaBord}">
                        <tr><td>&nbsp;&nbsp;</td></tr>
                    </table>
                    <br>${it?.m055KategoriJob}
                </td>
                <%
                    }
                %>
                <td style="padding: 10px">
                    <table class="table table-bordered" style="width: 10%;background-color: #fabb88">
                        <tr><td>&nbsp;&nbsp;</td></tr>
                    </table>
                    <br/>Actual Time
                </td>
                <td style="padding: 10px">
                    <table class="table table-bordered" style="width: 10%;background-color: #22f8a5">
                        <tr><td>&nbsp;&nbsp;</td></tr>
                    </table>
                    <br/>Final Inpection
                </td>
            </tr>
        </table>
	</div>
</body>
</html>
