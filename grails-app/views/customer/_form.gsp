<%@ page import="org.apache.commons.codec.binary.Base64;com.kombos.customerprofile.Warna; com.kombos.administrasi.FullModelCode; com.kombos.administrasi.Gear; com.kombos.administrasi.Engine; com.kombos.administrasi.Grade; com.kombos.administrasi.BaseModel; com.kombos.customerprofile.Hobby; com.kombos.maintable.Nikah; com.kombos.customerprofile.Agama; com.kombos.customerprofile.JenisIdCard; com.kombos.maintable.PeranCustomer; com.kombos.maintable.Company" %>

    <g:if test="${flash.message}">
        <div class="message" role="status">
            ${flash.message}
        </div>
    </g:if>

    <div class="control-group">
        <label class="control-label" for="t182ID" style="text-align: left">
            <g:message code="historyCustomer.pilihCustomer.label" default="Pilih Customer" />
        </label>
        <div class="controls">
            <g:select id="pilihCustomer" name="pilihCustomer" noSelection="['':'Tambah Customer']" from="[]" onchange="changeCustomer();" class="many-to-one"/>
            <input type="hidden" name="idVehicle" id="idVehicle" value="${historyCustomerVehicleInstance?.id ? historyCustomerVehicleInstance?.id: idHistVehicle }" />
        </div>
    </div>

    <div class="row-fluid">
        <div id="kiri" class="span4">
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182ID', 'error')} ">
                <label class="control-label" for="t182ID" style="text-align: left">
                    <g:message code="historyCustomer.t182ID.label" default="Kode Customer" />
                </label>
                <div class="controls">
                    <g:textField name="t182ID" id="t182ID" readonly="" value="${historyCustomerInstance?.t182ID}"/>
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182GelarD', 'error')} ">
                <label class="control-label" for="t182GelarD" style="text-align: left">
                    <g:message code="historyCustomer.t182GelarD.label" default="Gelar di depan" />
                </label>

                <div class="controls">
                    <g:textField name="t182GelarD" id="t182GelarD" maxlength="50" value="${historyCustomerInstance?.t182GelarD}"/>
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NamaDepan', 'error')} required">
                <label class="control-label" for="t182NamaDepan" style="text-align: left">
                    <g:message code="historyCustomer.t182NamaDepan.label" default="Nama Depan" />
                    <span class="required-indicator">*</span>
                </label>

                <div class="controls">
                    <g:textField name="t182NamaDepan" id="t182NamaDepan" maxlength="50" required="" value="${historyCustomerInstance?.t182NamaDepan}"/>
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NamaBelakang', 'error')} ">
                <label class="control-label" for="t182NamaBelakang" style="text-align: left">
                    <g:message code="historyCustomer.t182NamaBelakang.label" default="Nama Belakang" />
                </label>

                <div class="controls">
                    <g:textField name="t182NamaBelakang" id="t182NamaBelakang" maxlength="50"  value="${historyCustomerInstance?.t182NamaBelakang}"/>
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182GelarB', 'error')} ">
                <label class="control-label" for="t182GelarB" style="text-align: left">
                    <g:message code="historyCustomer.t182GelarB.label" default="Gelar di belakang" />
                </label>

                <div class="controls">
                    <g:textField name="t182GelarB" id="t182GelarB" maxlength="50" value="${historyCustomerInstance?.t182GelarB}"/>
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182JenisKelamin', 'error')} required">
                <label class="control-label" for="t182JenisKelamin" style="text-align: left">
                    <g:message code="historyCustomer.t182JenisKelamin.label" default="Jenis Kelamin" />
                    <span class="required-indicator">*</span>
                </label>

                <div class="controls">
                    <g:radioGroup name="t182JenisKelamin" id="t182JenisKelamin" values="['1','2']" labels="['Laki-laki','Perempuan']" required="" value="${historyCustomerInstance?.t182JenisKelamin}">
                        ${it.radio} ${it.label}
                    </g:radioGroup>
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182TglLahir', 'error')} ">
                <label class="control-label" for="t182TglLahir" style="text-align: left">
                    <g:message code="historyCustomer.t182TglLahir.label" default="Tanggal Lahir" />
                </label>

                <div class="controls">
                    <ba:datePicker name="t182TglLahir" precision="day" value="${historyCustomerInstance?.t182TglLahir}" format="dd/mm/yyyy" />
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'jenisCustomer', 'error')} ">
                <label class="control-label" for="jenisCustomer" style="text-align: left">
                    <g:message code="historyCustomer.jenisCustomer.label" default="Jenis Customer" />
                </label>

                <div class="controls">
                    <g:radioGroup name="jenisCustomer" id="jenisCustomer" values="['Personal','Corporate']" labels="['Personal','Corporate']" value="${historyCustomerInstance?.jenisCustomer}">
                        ${it.radio} ${it.label}
                    </g:radioGroup>
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'company', 'error')} ">
                <label class="control-label" for="company" style="text-align: left">
                    <g:message code="historyCustomer.company.label" default="Perusahaan" />
                </label>

                <div class="controls">
                    <g:select id="company" style="width:170px" name="company.id" noSelection="['':'Pilih Perusahaan']" from="${Company.createCriteria().list(){eq("staDel","0");order("namaPerusahaan")}}" optionValue="namaPerusahaan" optionKey="id" value="${historyCustomerInstance?.company?.id}" class="many-to-one"/>
                    <g:field type="button" style="height: 20px;width: 20px" class="btn cancel" onclick="addMore('company');" name="add" id="add" value="${message(code: 'historyCustomer.add.label', default: '...')}" />
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'peranCustomer', 'error')} ">
                <label class="control-label" for="peranCustomer" style="text-align: left">
                    <g:message code="historyCustomer.peranCustomer.label" default="Peran Customer" />
                </label>

                <div class="controls">
                    <g:select id="peranCustomer" required="" name="peranCustomer.id" noSelection="['':'Pilih Peran Customer']" from="${PeranCustomer.createCriteria().list(){eq("staDel","0");order("m115NamaPeranCustomer")}}" optionValue="m115NamaPeranCustomer" optionKey="id" value="${historyCustomerInstance?.peranCustomer?.id}" class="many-to-one"/>
                </div>
            </div>
            <br/>
            <div id="alamat_tabs">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#alamat_tabs-1" data-toggle="tab">
                            Alamat Korespodensi
                        </a>
                    </li>
                    <li class="">
                        <a href="#alamat_tabs-2" data-toggle="tab">
                            Alamat NPWP
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="alamat_tabs-1">
                    <br/>
                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182Alamat', 'error')} ">
                        <label class="control-label" for="t182Alamat" style="text-align: left">
                            <g:message code="historyCustomer.t182Alamat.label" default="Alamat Korespondensi Customer"/>
                            <span class="required-indicator">*</span>
                        </label>

                        <div class="controls">
                            <g:textArea name="t182Alamat" required=""  maxlength="50"
                                        value="${historyCustomerInstance?.t182Alamat}"/>
                        </div>
                    </div>

                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182RT', 'error')} ">
                        <label class="control-label" for="t182RT" style="text-align: left">
                            <g:message code="historyCustomer.t182RT.label" default="T182 RT"/> / <g:message code="historyCustomer.t182RW.label" default="T182 RW"/>
                        </label>

                        <div class="controls">
                            <g:textField onkeypress="return isNumberKey(event);" style="width:50px" name="t182RT" id="t182RT" maxlength="3"  value="${historyCustomerInstance?.t182RT}"/>
                            &nbsp;/&nbsp;
                            <g:textField onkeypress="return isNumberKey(event);" style="width:50px" name="t182RW" id="t182RW" maxlength="3"  value="${historyCustomerInstance?.t182RW}"/>
                        </div>
                    </div>

                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'provinsi', 'error')} ">
                        <label class="control-label" for="provinsi" style="text-align: left">
                            <g:message code="historyCustomer.provinsi.label" default="Provinsi"/>
                        </label>

                        <div class="controls">
                            <g:select id="provinsi" name="provinsi.id" noSelection="['':'Pilih Provinsi']" from="${com.kombos.administrasi.Provinsi.createCriteria().list{eq("staDel","0");order("m001NamaProvinsi")}}"
                                      optionKey="id" optionValue="m001NamaProvinsi" onchange="loadKabKota(1);" value="${historyCustomerInstance?.provinsi?.id}" class="many-to-one"/>
                        </div>
                    </div>


                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kabKota', 'error')} ">
                        <label class="control-label" for="kabKota" style="text-align: left">
                            <g:message code="historyCustomer.kabKota.label" default="Kab Kota"/>
                        </label>

                        <div class="controls">
                            <g:if test="${historyCustomerInstance?.provinsi?.id}">
                                <g:select id="kabKota" name="kabKota.id" noSelection="['':'Pilih Kab. / Kota']" from="${com.kombos.administrasi.KabKota.createCriteria().list{eq("staDel","0");provinsi{eq("id",historyCustomerInstance?.provinsi?.id)};order("m002NamaKabKota")}}" optionKey="id"
                                          optionValue="m002NamaKabKota" onchange="loadKecamatan(1);" value="${historyCustomerInstance?.kabKota?.id}" class="many-to-one"/>
                            </g:if>
                            <g:else>
                                <g:select id="kabKota" name="kabKota.id" onchange="loadKecamatan(1);" noSelection="['':'Pilih Kab. / Kota']" from="${[]}" class="many-to-one"/>
                            </g:else>
                        </div>
                    </div>


                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kecamatan', 'error')} ">
                        <label class="control-label" for="kecamatan" style="text-align: left">
                            <g:message code="historyCustomer.kecamatan.label" default="Kecamatan"/>
                        </label>

                        <div class="controls">
                            <g:if test="${historyCustomerInstance?.kabKota?.id}">
                                <g:select id="kecamatan" name="kecamatan.id" from="${com.kombos.administrasi.Kecamatan.createCriteria().list{eq("staDel","0");kabKota{eq("id",historyCustomerInstance?.kabKota?.id)};order("m003NamaKecamatan")}}"
                                          optionKey="id" onchange="loadKelurahan(1);" value="${historyCustomerInstance?.kecamatan?.id}" class="many-to-one"/>
                            </g:if>
                            <g:else>
                                <g:select id="kecamatan" onchange="loadKelurahan(1);" noSelection="['':'Pilih Kecamatan']"  name="kecamatan.id" from="${[]}" class="many-to-one"/>
                            </g:else>
                        </div>
                    </div>


                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kelurahan', 'error')} ">
                        <label class="control-label" for="kelurahan" style="text-align: left">
                            <g:message code="historyCustomer.kelurahan.label" default="Kelurahan"/>
                        </label>

                        <div class="controls">
                            <g:if test="${historyCustomerInstance?.kecamatan?.id}">
                                <g:select id="kelurahan" name="kelurahan.id" from="${com.kombos.administrasi.Kelurahan.createCriteria().list{eq("staDel","0");kecamatan{eq("id",historyCustomerInstance?.kecamatan?.id)};order("m004NamaKelurahan")}}"
                                          optionKey="id" value="${historyCustomerInstance?.kelurahan?.id}" class="many-to-one"/>
                            </g:if>
                            <g:else>
                                <g:select id="kelurahan" name="kelurahan.id" noSelection="['':'Pilih Kelurahan']" from="${[]}" class="many-to-one"/>
                            </g:else>
                        </div>
                    </div>

                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182KodePos', 'error')} ">
                        <label class="control-label" for="t182KodePos" style="text-align: left">
                            <g:message code="historyCustomer.t182KodePos.label" default="T182 Kode Pos"/>
                            %{--<span class="required-indicator">*</span>--}%
                        </label>

                        <div class="controls">
                            <g:textField onkeypress="return isNumberKey(event);" name="t182KodePos" id="t182KodePos" maxlength="5"
                                     value="${historyCustomerInstance?.t182KodePos}"/>
                        </div>
                    </div>

                </div>

                    <div class="tab-pane" id="alamat_tabs-2">
                    <br/>
                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182AlamatNPWP', 'error')} ">
                        <label class="control-label" for="t182AlamatNPWP" style="text-align: left">
                            <g:message code="historyCustomer.t182AlamatNPWP.label" default="T182 Alamat NPWP"/>
                            <span class="required-indicator">*</span>
                        </label>

                        <div class="controls">
                            <g:textArea name="t182AlamatNPWP" required="" maxlength="50" value="${historyCustomerInstance?.t182AlamatNPWP}"/>
                        </div>
                    </div>

                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182RTNPWP', 'error')} ">
                        <label class="control-label" for="t182RTNPWP" style="text-align: left">
                            <g:message code="historyCustomer.t182RTNPWP.label" default="RT"/> / <g:message code="historyCustomer.t182RWNPWP.label" default="RW"/>
                        </label>

                        <div class="controls">
                            <g:textField onkeypress="return isNumberKey(event);" style="width:50px" name="t182RTNPWP" id="t182RTNPWP" maxlength="3"  value="${historyCustomerInstance?.t182RTNPWP}"/>
                            &nbsp;/&nbsp;
                            <g:textField onkeypress="return isNumberKey(event);" style="width:50px" name="t182RWNPWP" id="t182RWNPWP" maxlength="3"  value="${historyCustomerInstance?.t182RWNPWP}"/>
                        </div>
                    </div>

                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'provinsi2', 'error')} ">
                        <label class="control-label" for="provinsi2" style="text-align: left">
                            <g:message code="historyCustomer.provinsi2.label" default="Provinsi2"/>
                        </label>

                        <div class="controls">
                            <g:select id="provinsi2" name="provinsi2.id" noSelection="['':'Pilih Provinsi']" from="${com.kombos.administrasi.Provinsi.list()}"
                                      optionKey="id" onchange="loadKabKota(2);" value="${historyCustomerInstance?.provinsi2?.id}" class="many-to-one"/>
                        </div>
                    </div>

                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kabKota2', 'error')} ">
                        <label class="control-label" for="kabKota2" style="text-align: left">
                            <g:message code="historyCustomer.kabKota2.label" default="Kab Kota2"/>
                        </label>

                        <div class="controls">
                            <g:if test="${historyCustomerInstance?.provinsi2?.id}">
                                <g:select id="kabKota2" name="kabKota2.id" noSelection="['':'Pilih Kab. / Kota']" from="${com.kombos.administrasi.KabKota.createCriteria().list{eq("staDel","0");provinsi{eq("id",historyCustomerInstance?.provinsi2?.id)};order("m002NamaKabKota")}}" optionKey="id"
                                          optionValue="m002NamaKabKota" onchange="loadKecamatan(2);" value="${historyCustomerInstance?.kabKota2?.id}" class="many-to-one"/>
                            </g:if>
                            <g:else>
                                <g:select id="kabKota2" name="kabKota2.id" onchange="loadKecamatan(2);" noSelection="['':'Pilih Kab. / Kota']" from="${[]}" class="many-to-one"/>
                            </g:else>
                        </div>
                    </div>

                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kecamatan2', 'error')} ">
                        <label class="control-label" for="kecamatan2" style="text-align: left">
                            <g:message code="historyCustomer.kecamatan2.label" default="Kecamatan2"/>
                        </label>

                        <div class="controls">
                            <g:if test="${historyCustomerInstance?.kabKota2?.id}">
                                <g:select id="kecamatan2" name="kecamatan2.id" from="${com.kombos.administrasi.Kecamatan.createCriteria().list{eq("staDel","0");kabKota{eq("id",historyCustomerInstance?.kabKota2?.id)};order("m003NamaKecamatan")}}"
                                          optionKey="id" onchange="loadKelurahan(2);" value="${historyCustomerInstance?.kecamatan2?.id}" class="many-to-one"/>
                            </g:if>
                            <g:else>
                                <g:select id="kecamatan2" onchange="loadKelurahan(2);" noSelection="['':'Pilih Kecamatan']"  name="kecamatan2.id" from="${[]}" class="many-to-one"/>
                            </g:else>
                        </div>
                    </div>

                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kelurahan2', 'error')} ">
                        <label class="control-label" for="kelurahan2" style="text-align: left">
                            <g:message code="historyCustomer.kelurahan2.label" default="Kelurahan2"/>
                        </label>

                        <div class="controls">
                            <g:if test="${historyCustomerInstance?.kecamatan2?.id}">
                                <g:select id="kelurahan2" name="kelurahan2.id" from="${com.kombos.administrasi.Kelurahan.createCriteria().list{eq("staDel","0");kecamatan{eq("id",historyCustomerInstance?.kecamatan2?.id)};order("m004NamaKelurahan")}}"
                                          optionKey="id" value="${historyCustomerInstance?.kelurahan2?.id}" class="many-to-one"/>
                            </g:if>
                            <g:else>
                                <g:select id="kelurahan2" name="kelurahan2.id" noSelection="['':'Pilih Kelurahan']" from="${[]}" class="many-to-one"/>
                            </g:else>
                        </div>
                    </div>


                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182KodePosNPWP', 'error')} ">
                        <label class="control-label" for="t182KodePosNPWP" style="text-align: left">
                            <g:message code="historyCustomer.t182KodePosNPWP.label" default="T182 Kode Pos NPWP"/>
                        </label>

                        <div class="controls">
                            <g:textField onkeypress="return isNumberKey(event);" name="t182KodePosNPWP" id="t182KodePosNPWP" maxlength="5"
                                value="${historyCustomerInstance?.t182KodePosNPWP}"/>
                        </div>
                    </div>

                </div>

                </div>

            </div>
            <br/>
            <legend style="font-size: 14px">Account Web Booking</legend>

            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182WebUserName', 'error')} ">
                <label class="control-label" for="t182WebUserName" style="text-align: left" style="text-align: left">
                    <g:message code="historyCustomer.t182WebUserName.label" default="Username"/>
                    <span class="required-indicator">*</span>
                </label>
    
                <div class="controls">
                    <g:textField name="t182WebUserName" id="t182WebUserName"  value="${historyCustomerInstance?.t182WebUserName}"/>
                </div>
            </div>

            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182WebPassword', 'error')} ">
                <label class="control-label" for="t182WebPassword" style="text-align: left">
                    <g:message code="historyCustomer.t182WebPassword.label" default="Password"/>
                </label>

                <div class="controls">
                    <g:passwordField name="t182WebPassword" id="t182WebPassword" value="${historyCustomerInstance?.t182WebPassword}"/>
                </div>
            </div>

            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182WebPassword', 'error')} ">
                <label class="control-label" for="t182WebPassword" style="text-align: left">
                    <g:message code="historyCustomer.t182WebPassword1.label" default="Konf. Password"/>
                </label>

                <div class="controls">
                    <g:passwordField name="t182WebPassword2" required="" id="t182WebPassword2" onkeyup="checkPass(); return false;"
                                     value="${historyCustomerInstance?.t182WebPassword}"/>
                    <g:hiddenField name="hasilPass" id="hasilPass" value="" required="" />
                    <span id="confirmMessage" class="confirmMessage"></span>
                </div>
            </div>
            <br/><br/>
            <span style="font-size: 20px;" ><b>Nomor HP Tidak Valid...</b></span>
            <a class="pull-right">
                &nbsp;&nbsp;
                <i>
                    <g:field type="button" style="padding: 5px;width: 80px" class="btn cancel"
                             name="more" id="more" value="${message(code: 'historyCustomer.more.label', default: '[More]')}" />
                </i>
                &nbsp;&nbsp;
            </a>
        </div>

        <div id="kanan" class="span8">
            <div class="row-fluid">
                <div id="kanan-kiri" class="span6">
                    <legend style="font-size: 14px">
                        <g:message code="historyCustomer.t182Foto.label" default="Foto" />
                    </legend>
                        <g:message code="default.upload.label" default="Upload" />

                        <input type="file" accept="image/*" name="myFile" id="myFile"
                               onchange="fileUpload(this.form, '${g.createLink(controller: 'customer', action: 'uploadImage')}', 'upload');
                               return false;"/><br/><br/>
                        <div id="upload">
                            <g:if test="${historyCustomerInstance?.t182Foto}">
                                <img width="200px"
                                     src="data:jpg;base64,${new String(new Base64().encode(historyCustomerInstance.t182Foto), "UTF-8")}"/></br></br>
                            </g:if>
                        </div>
                        <br/>

                    <legend style="font-size: 14px">
                        <g:message code="historyCustomer.kontak.label" default="Kontak" />
                    </legend>
                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoTelpRumah', 'error')} required">
                        <label class="control-label" for="t182NoTelpRumah" style="text-align: left">
                            <g:message code="historyCustomer.t182NoTelpRumah.label" default="Telp. Rumah"/>
                            <span class="required-indicator">*</span>
                        </label>

                        <div class="controls">
                            <g:textField onkeypress="return isNumberKey(event);" name="t182NoTelpRumah" id="t182NoTelpRumah" maxlength="50" value="${historyCustomerInstance?.t182NoTelpRumah}" required=""/>
                        </div>
                    </div>

                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoTelpKantor', 'error')} ">
                        <label class="control-label" for="t182NoTelpKantor" style="text-align: left">
                            <g:message code="historyCustomer.t182NoTelpKantor.label" default="Telp. Kantor"/>
                        </label>

                        <div class="controls">
                            <g:textField onkeypress="return isNumberKey(event);" name="t182NoTelpKantor" id="t182NoTelpKantor" maxlength="50" value="${historyCustomerInstance?.t182NoTelpKantor}"/>
                        </div>
                    </div>

                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoHp', 'error')} required">
                        <label class="control-label" for="t182NoHp" style="text-align: left">
                            <g:message code="historyCustomer.t182NoHp.label" default="Telp. Seluler"/>
                            <span class="required-indicator">*</span>
                        </label>

                        <div class="controls">
                            <g:textField required="" onkeypress="return isNumberKey(event);" name="t182NoHp" id="t182NoHp" maxlength="20" value="${historyCustomerInstance?.t182NoHp}"/>
                        </div>
                    </div>
                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoFax', 'error')} ">
                        <label class="control-label" for="t182NoFax" style="text-align: left">
                            <g:message code="historyCustomer.t182NoFax.label" default="Faximile"/>
                        </label>

                        <div class="controls">
                            <g:textField onkeypress="return isNumberKey(event);" name="t182NoFax" id="t182NoFax" maxlength="50" value="${historyCustomerInstance?.t182NoFax}"/>
                        </div>
                    </div>
                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182Email', 'error')} ">
                        <label class="control-label" for="t182Email" style="text-align: left">
                            <g:message code="historyCustomer.t182Email.label" default="Email"/>
                            %{--<span class="required-indicator">*</span>--}%
                        </label>

                        <div class="controls">
                            <g:field type="email" name="t182Email" id="t182Email" value="${historyCustomerInstance?.t182Email}" maxlength="50"/>
                        </div>
                    </div>
                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182StaTerimaMRS', 'error')} ">
                        <label class="control-label" for="t182StaTerimaMRS" style="text-align: left">
                            <g:message code="historyCustomer.t182StaTerimaMRS.label" default="Terima MRS?"/>
                        </label>

                        <div class="controls">
                            <g:radioGroup name="t182StaTerimaMRS" id="t182StaTerimaMRS" values="['0','1']" labels="['Ya','Tidak']" value="${historyCustomerInstance?.t182StaTerimaMRS}">
                                ${it.radio} ${it.label}
                            </g:radioGroup>
                        </div>
                    </div>

                </div>

                <div id="kanan-kanan" class="span6">
                    <legend style="font-size: 14px">
                        <g:message code="historyCustomer.identitas.label" default="Identitas" />
                    </legend>
                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'jenisIdCard', 'error')} ">
                        <label class="control-label" for="jenisIdCard" style="text-align: left">
                            <g:message code="historyCustomer.jenisIdCard.label" default="Jenis ID Card"/>
                        </label>

                        <div class="controls">
                            <g:select id="jenisIdCard" style="width:170px" name="jenisIdCard.id" noSelection="['':'Pilih Jenis ID Card']" from="${JenisIdCard.createCriteria().list(){eq("staDel","0");order("m060JenisIDCard")}}" optionValue="m060JenisIDCard" optionKey="id" value="${historyCustomerInstance?.jenisIdCard?.id}" class="many-to-one"/>
                            <g:field type="button" style="height: 20px;width: 20px" class="btn cancel" onclick="addMore('jenisIdCard');" name="add" id="add" value="${message(code: 'historyCustomer.add.label', default: '...')}" />
                        </div>
                    </div>
                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NomorIDCard', 'error')} ">
                        <label class="control-label" for="t182NomorIDCard" style="text-align: left">
                            <g:message code="historyCustomer.t182NomorIDCard.label" default="No.ID Card"/>
                        </label>

                        <div class="controls">
                            <g:textField onkeypress="return isNumberKey(event);" name="t182NomorIDCard" id="t182NomorIDCard" maxlength="20" value="${historyCustomerInstance?.t182NomorIDCard}"/>
                        </div>
                    </div>

                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NPWP', 'error')} ">
                        <label class="control-label" for="t182NPWP" style="text-align: left">
                            <g:message code="historyCustomer.t182NPWP.label" default="NPWP"/>
                        </label>

                        <div class="controls">
                            <g:textField onkeypress="return isNumberKey(event);" name="t182NPWP" id="t182NPWP" maxlength="20" value="${historyCustomerInstance?.t182NPWP}"/>
                        </div>
                    </div>

                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoFakturPajakStd', 'error')} ">
                        <label class="control-label" for="t182NoFakturPajakStd" style="text-align: left">
                            <g:message code="historyCustomer.t182NoFakturPajakStd.label" default="NFPS"/>
                            %{--<span class="required-indicator">*</span>--}%
                        </label>

                        <div class="controls">
                            <g:textField onkeypress="return isNumberKey(event);" name="t182NoFakturPajakStd" id="t182NoFakturPajakStd" maxlength="20" value="${historyCustomerInstance?.t182NoFakturPajakStd}"/>
                        </div>
                    </div>

                    <br/><br/>
                    <legend style="font-size: 14px">
                        <g:message code="historyCustomer.other.label" default="Lain-lain" />
                    </legend>
                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'agama', 'error')} ">
                        <label class="control-label" for="agama" style="text-align: left">
                            <g:message code="historyCustomer.agama.label" default="Agama"/>
                        </label>

                        <div class="controls">
                            <g:select id="agama" name="agama.id" noSelection="['':'Silahkan pilih']" from="${Agama.createCriteria().list(){eq("staDel","0");order("m061NamaAgama")}}" optionValue="m061NamaAgama" optionKey="id" value="${historyCustomerInstance?.agama?.id}" class="many-to-one"/>
                            <g:field type="button" style="height: 20px;width: 20px" class="btn cancel" onclick="addMore('agama');" name="add" id="add" value="${message(code: 'historyCustomer.add.label', default: '...')}" />
                        </div>
                    </div>
                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'nikah', 'error')} ">
                        <label class="control-label" for="nikah" style="text-align: left">
                            <g:message code="historyCustomer.nikah.label" default="Status"/>
                        </label>

                        <div class="controls">
                            <g:select id="nikah" name="nikah.id" noSelection="['':'Silahkan pilih']" from="${Nikah.createCriteria().list(){eq("staDel","0");order("m062StaNikah")}}" optionValue="m062StaNikah" optionKey="id" value="${historyCustomerInstance?.nikah?.id}" class="many-to-one"/>
                            <g:field type="button" style="height: 20px;width: 20px" class="btn cancel" onclick="addNikah();" name="add" id="addNikahBtn" value="${message(code: 'historyCustomer.add.label', default: '...')}" />
                            <g:textField name="inputNikah" id="inputNikah" />
                            <br/>
                            <g:field type="button"  onclick="saveNikah();" class="btn btn-primary create" name="saveNikahBtn" id="saveNikahBtn" value="${message(code: 'default.button.save.label', default: 'Save')}"/>
                            <g:field type="button"   onclick="cancelNikah();" class="btn  cancel" name="cancelNikahBtn" id="cancelNikahBtn" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}"/>
                        </div>
                    </div>
                    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'hobbies', 'error')} ">
                        <label class="control-label" for="hobbies" style="text-align: left">
                            <g:message code="historyCustomer.hobbies.label" default="Hobbies"/>
                        </label>

                        <div class="controls">
                            <g:select id="hobbies" name="hobbies.id" noSelection="['':'Silahkan pilih']"from="${Hobby.createCriteria().list(){eq("staDel","0");order("m063NamaHobby")}}" optionValue="m063NamaHobby" optionKey="id" value="${historyCustomerInstance?.hobbies?.id}" class="many-to-one"/>
                            <g:field type="button" style="height: 20px;width: 20px" class="btn cancel" onclick="addhobby();" name="add" id="addhobbyBtn" value="${message(code: 'historyCustomer.add.label', default: '...')}" />
                            <g:textField name="inputhobby" id="inputhobby" />
                            <br/>
                            <g:field type="button"  onclick="savehobby();" class="btn btn-primary create" name="savehobbyBtn" id="savehobbyBtn" value="${message(code: 'default.button.save.label', default: 'Save')}"/>
                            <g:field type="button"   onclick="cancelhobby();" class="btn  cancel" name="cancelhobbyBtn" id="cancelhobbyBtn" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}"/>
                        </div>
                    </div>
                </div>
                <br/><br/>
                <legend style="font-size: 14px">
                    <g:message code="historyCustomer.vehicle.label" default="Vehicle" />
                </legend>
                <div class="control-group">
                    <label class="control-label" for="noPol" style="text-align: left">
                        <g:message code="historyCustomer.noPol.label" default="Nomor Polisi"/>
                    </label>

                    <div class="controls">
                        <g:textField style="width: 98%" readonly="" name="noPol" id="noPol" maxlength="11" value="${historyCustomerVehicleInstance?.kodeKotaNoPol ? historyCustomerVehicleInstance?.kodeKotaNoPol?.m116ID+" "+historyCustomerVehicleInstance?.t183NoPolTengah+" "+historyCustomerVehicleInstance?.t183NoPolBelakang : ""}"/>
                    </div>
                </div>
                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 'customerVehicle', 'error')} ">
                    <label class="control-label" for="customerVehicle" style="text-align: left">
                        <g:message code="historyCustomer.customerVehicle.label" default="VinCode"/>
                    </label>

                    <div class="controls">
                        <g:textField style="width: 120px" name="t109WMI" id="t109WMI" value="${fullModelVinCode?.t109WMI}"/>
                        <g:select name="t109VDS" id="t109VDS" from="${com.kombos.administrasi.FullModelVinCode.createCriteria().list {eq("staDel","0");order("t109VDS")}}" optionKey="t109VDS" optionValue="t109VDS" />
                        %{--<g:textField style="width: 180px" name="t109VDS" id="t109VDS" value="${fullModelVinCode?.t109VDS}"/>--}%
                        <g:textField style="width: 60px" name="t109CekDigit" id="t109CekDigit" value="${fullModelVinCode?.t109CekDigit}"/>
                        <g:textField style="width: 180px" name="t109VIS" id="t109VIS" value="${fullModelVinCode?.t109VIS}"/>
                        <br/><br/>
                        <g:field type="button" style="width: 96%" class="btn cancel" onclick="validateVinCode();"
                                 name="validate" id="validate" value="${message(code: 'historyCustomer.validate.label', default: 'Validate VIN Code')}" />
                        <br/><br/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="baseModel" style="text-align: left">
                        <g:message code="historyCustomer.baseModel.label" default="Base Model"/>
                    </label>

                    <div class="controls">
                        <g:select style="width: 100%" id="baseModel" name="baseModel.id" from="${[]}" value="${historyCustomerVehicleInstance?.fullModelCode?.baseModel?.id}" class="many-to-one"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="grade" style="text-align: left">
                        <g:message code="historyCustomer.grade.label" default="Grade"/>
                    </label>

                    <div class="controls">
                        <g:select style="width: 100%" id="grade" name="grade.id" from="${[]}" value="${historyCustomerVehicleInstance?.fullModelCode?.grade?.id}" class="many-to-one"/>
                    </div>
                </div>
                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 'engine', 'error')} ">
                    <label class="control-label" for="engine" style="text-align: left">
                        <g:message code="historyCustomer.engine.label" default="Engine"/>
                    </label>

                    <div class="controls">
                        <g:select style="width: 100%" id="engine" name="engine.id" from="${[]}" value="${historyCustomerVehicleInstance?.fullModelCode?.engine?.id}" class="many-to-one"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="gear" style="text-align: left">
                        <g:message code="historyCustomer.gear.label" default="Gear"/>
                    </label>

                    <div class="controls">
                        <g:select style="width: 100%" id="gear" name="gear.id" from="${[]}" value="${historyCustomerVehicleInstance?.fullModelCode?.gear?.id}" class="many-to-one"/>
                    </div>
                </div>
                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 'fullModelCode', 'error')} ">
                    <label class="control-label" for="fullModelCode" style="text-align: left">
                        <g:message code="historyCustomer.fullModelCode.label" default="Full Model Code"/>
                    </label>

                    <div class="controls">
                        <g:select style="width: 100%" id="fullModelCode" name="fullModelCode.id" from="${[]}" value="${historyCustomerVehicleInstance?.fullModelCode?.id}" class="many-to-one"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="fullModelVinCodeBln" style="text-align: left">
                        <g:message code="historyCustomer.fullModelVinCode.label" default="Thn Bln Pembuatan"/>
                    </label>

                    <div class="controls">
                        <g:select name="fullModelVinCodeBln" id="fullModelVinCodeBln" from="${[]}"/>
                        <g:textField onkeypress="return isNumberKey(event);" name="fullModelVinCodeThn" id="fullModelVinCodeThn" maxlength="4" value=""/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="warna" style="text-align: left">
                        <g:message code="historyCustomer.warna.label" default="Warna"/>
                    </label>

                    <div class="controls">
                        <g:select  style="width: 100%" id="warna" name="warna.id" from="${Warna.createCriteria().list {eq("staDel","0");order("m092NamaWarna")}}" optionValue="m092NamaWarna" optionKey="id" value="${historyCustomerVehicleInstance?.warna?.id}" class="many-to-one"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="reception" style="text-align: left">
                        <g:message code="historyCustomer.reception.label" default="Kategori Customer"/>
                    </label>

                    <div class="controls">
                        <g:radioGroup name="reception" id="reception" values="['2','1','0']" labels="['Umum','Fleet','Asuransi']" value="${receptionInstance?.t401StaKategoriCustUmumSPKAsuransi}">
                            ${it.radio} ${it.label}
                        </g:radioGroup>
                    </div>
                </div>
            <g:if test="${menu!="sales"}">
                <a class="pull-right">
                    &nbsp;&nbsp;
                    <i>
                    <g:field type="button" style="padding: 5px;width: 130px" class="btn cancel" onclick="detailAsuransi();"
                             name="detail" id="detail" value="${message(code: 'historyCustomer.detailAsuransi.label', default: 'Detail Asuransi')}" />
                    </i>
                    &nbsp;&nbsp;
                </a>
            </g:if>
                <br/><br/><br/>

                <div class="control-group">
                    <label class="control-label" for="customerVehicle" style="text-align: left">
                        <g:message code="historyCustomer.upload.label" default="Upload Buku Service"/>
                    </label>

                    <div class="controls">
                        <input type="file" name="myDocument" id="myDocument"
                               onchange="uploadDokumen(this.form, '${g.createLink(controller: 'customer', action: 'uploadDokumen')}', 'uploadDokumen');
                               return false;"/><br/><br/>
                        <div id="uploadDokumen">
                        </div>
                        <br/>
                    </div>
                </div>
            </div>
        </div>

    </div>