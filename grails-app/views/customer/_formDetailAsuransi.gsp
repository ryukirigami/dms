<%@ page import="com.kombos.administrasi.VendorAsuransi" %>

<g:javascript>
    var jumCekbok;
    $.ajax({
        url: '${request.contextPath}/customer/getDokumen',
        type: 'POST',
        success: function(data){
            document.getElementById('cekArea').innerHTML = data[1]
            jumCekbok = data [0]
            for(var a=1 ; a<=data[0] ; a++){
                document.getElementById('cekbok'+a).checked = false;
            }
        },error: function(xhr, textStatus, errorThrown) { alert(textStatus);}
    });

    function changeJenisDokumen(){
        var id=$('#kelengkapanDokumenAsuransi').val();
        for(var a=1 ; a<=jumCekbok ; a++){
            document.getElementById('cekbok'+a).checked = false;
        }
        if(id!="" || id!=null){
            document.getElementById('cekbok'+id).checked = true;
            return
        }
       return
    }
</g:javascript>

<div id="alamat_tabs">
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#dokumen_asuransi" data-toggle="tab">
                Dokumen Asuransi
            </a>
        </li>
        <li class="">
            <a href="#upload_dokumen" data-toggle="tab">
                Upload Dokumen
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="dokumen_asuransi">
            <legend style="font-size: 14px;margin-left: 30px">Data Polis Asuransi</legend>
            <br/>
            <div style="margin-left: 30px">
                <g:checkBox checked="false" onclick="cekSama();" name="staSama" id="staSama" value="1" />
                &nbsp;
                <g:message code="historyCustomer.staSama.label" default="Sama Dengan Data Customer" />
                <br/>
            </div>

            <div class="row-fluid" style="margin-left: 30px">
                <div class="row-fluid">
                    <div id="kiri11" class="span4">
                        <div class="control-group">
                            <label class="control-label" for="vendorAsuransi" style="text-align: left">
                                <g:message code="customer.asuransi.label" default="Asuransi" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                <input type="hidden" name="idVehicle" id="idVehicle" value="${historyCustomerVehicleInstance?.id ? historyCustomerVehicleInstance?.id: idHistVehicle }" />
                                <g:select required="" name="vendorAsuransi.id" id="vendorAsuransi" onchange="changeVendor();" noSelection="['':'Pilih Asuransi']" from="${VendorAsuransi.createCriteria().list {eq("staDel","0");order("m193Nama")}}" optionValue="m193Nama" optionKey="id" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t193NomorPolis" style="text-align: left">
                                <g:message code="customer.t193NomorPolis.label" default="Nomor Polis" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                <g:textField required="" maxlength="10" name="t193NomorPolis" id="t193NomorPolis" onkeypress="return isNumberKey(event);" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t193NamaPolis" style="text-align: left">
                                <g:message code="customer.t193NamaPolis.label" default="Nama Polis" />
                            </label>
                            <div class="controls">
                                <g:textField name="t193NamaPolis" id="t193NamaPolis" maxlength="50" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t193TglAwal" style="text-align: left">
                                <g:message code="customer.tanggalBerlaku.label" default="Tanggal Berlaku" />
                            </label>
                            <div class="controls">
                                <ba:datePicker required="true" name="t193TglAwal" id="t193TglAwal" precision="day" format="dd/mm/yyyy" />
                                <br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                s.d.
                                <br/>
                                <ba:datePicker required="true" name="t193TglAkhir" id="t193TglAkhir" precision="day" format="dd/mm/yyyy" />
                            </div>
                        </div>
                    </div>

                    <div id="kanan11" class="span8">
                        <div class="control-group">
                            <label class="control-label" for="t193AlamatPolis" style="text-align: left">
                                <g:message code="customer.t193AlamatPolis.label" default="Alamat Customer" />
                            </label>
                            <div class="controls">
                                <g:textArea name="t193AlamatPolis" id="t193AlamatPolis" style="resize:none" maxlength="255" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t193TelpPolis" style="text-align: left">
                                <g:message code="customer.t193TelpPolis.label" default="Nomor Telepon" />
                            </label>
                            <div class="controls">
                                <g:textField name="t193TelpPolis" id="t193TelpPolis" maxlength="50" onkeypress="return isNumberKey(event);" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row-fluid" style="margin-left: 30px">
                <div class="row-fluid">
                    <div id="kiri12" class="span6">
                        <br/>
                        &nbsp;&nbsp;&nbsp;
                        <g:message code="customer.m193JmlToleransiAsuransiBP.label" default="Butuh SPK Asuransi Sebelum Production" />
                        &nbsp;
                        <g:radioGroup disabled="" name="m193JmlToleransiAsuransiBP" id="m193JmlToleransiAsuransiBP" values="['1','0']" labels="['Ya','Tidak']" value="" readonly="">
                            ${it.radio} ${it.label}
                        </g:radioGroup>
                        <br/>
                        <legend style="font-size: 14px">Tanggal Pengiriman Dokumen Asuransi</legend>
                        <br/>
                        <div class="control-group">
                            <label class="control-label" for="t193TglJanjiKirimDok" style="text-align: left">
                                <g:message code="customer.t193TglJanjiKirimDok.label" default="Tanggal Customer Janji Kirim Dokumen" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                <g:checkBox checked="false" name="t193StaJanjiKirimDok" id="t193StaJanjiKirimDok" value="1" />
                                <ba:datePicker required="true" name="t193TglJanjiKirimDok" precision="day" value="" format="dd/mm/yyyy" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t193TglKirimDok" style="text-align: left">
                                <g:message code="customer.m193JmlToleransiAsuransiBP.label" default="Tanggal Kirim Dokumen ke Asuransi" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                <g:checkBox checked="false" name="t193StaKirimDok" id="t193StaKirimDok" value="1" />
                                <ba:datePicker required="true" name="t193TglKirimDok" precision="day" value="" format="dd/mm/yyyy" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t193MetodeKirim" style="text-align: left">
                                <g:message code="customer.t193MetodeKirim.label" default="Metode Pengiriman" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                &nbsp;&nbsp;&nbsp;
                                <g:select required="" name="t193MetodeKirim" id="t193MetodeKirim" from="['Email','Fax','Surat']" noSelection="['':'Pilih Metode Pengiriman']" />
                            </div>
                        </div>
                        <br/><br/>
                        <legend style="font-size: 14px">Survey</legend>
                        <br/>
                        <div class="control-group">
                            <label class="control-label" for="t193TglReminderSurvey" style="text-align: left">
                                <g:message code="customer.t193TglReminderSurvey.label" default="Tanggal Reminder Survey" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                <g:checkBox checked="false" name="t193StaReminderSurvey" id="t193StaReminderSurvey" value="1" />
                                <ba:datePicker required="true" name="t193TglReminderSurvey" precision="day" value="" format="dd/mm/yyyy" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t193TglActualSurvey" style="text-align: left">
                                <g:message code="customer.t193TglActualSurvey.label" default="Tanggal Actual Survey" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                <g:checkBox checked="false" name="t193StaActualSurvey" id="t193StaActualSurvey" value="1" />
                                <ba:datePicker required="" name="t193TglActualSurvey" precision="day" value="" format="dd/mm/yyyy" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t193Catatan" style="text-align: left">
                                <g:message code="customer.t193Catatan.label" default="Catatan" />
                            </label>
                            <div class="controls">
                                &nbsp;&nbsp;&nbsp;
                                <g:textField name="t193Catatan" id="t193Catatan"  maxlength="255"/>
                            </div>
                        </div>
                        <br/><br/>
                        <legend style="font-size: 14px">SPK Asuransi</legend>
                        <br/>
                        <div class="control-group">
                            <label class="control-label" for="t193NomorSPK" style="text-align: left">
                                <g:message code="customer.t193NomorSPK.label" default="Nomor SPK" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                <g:checkBox checked="false" name="t193StaNomorSPK" id="t193StaNomorSPK" value="1" />
                                <g:textField required="" name="t193NomorSPK" id="t193NomorSPK"  maxlength="50" onkeypress="return isNumberKey(event);" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t193TanggalSPK" style="text-align: left">
                                <g:message code="customer.t193TanggalSPK.label" default="Tanggal SPK" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                &nbsp;&nbsp;&nbsp;
                                <ba:datePicker required="true" name="t193TanggalSPK" precision="day" value="" format="dd/mm/yyyy" />
                            </div>
                        </div>

                    </div>

                    <div id="kanan12" class="span6">
                        <br/>
                        &nbsp;&nbsp;&nbsp;
                        <g:message code="customer.t193StaGantiTambahan.label" default="Butuh SPK Tambahan" />
                        &nbsp;
                        <g:radioGroup name="t193StaGantiTambahan" id="t193StaGantiTambahan" values="['1','0']" labels="['Ya','Tidak']" value="">
                            ${it.radio} ${it.label}
                        </g:radioGroup>
                        <br/>
                        <legend style="font-size: 14px">SPK Asuransi Tambahan</legend>
                        <br/>
                        <div class="control-group">
                            <label class="control-label" for="t193NomorSPKTambah" style="text-align: left">
                                <g:message code="customer.t193NomorSPK.label" default="Nomor SPK" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                <g:textField style="width:275px" name="t193NomorSPKTambah" maxlength="50" id="t193NomorSPKTambah" onkeypress="return isNumberKey(event);" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t193TanggalSPKTambah" style="text-align: left">
                                <g:message code="customer.t193TanggalSPK.label" default="Tanggal SPK" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                <ba:datePicker name="t193TanggalSPKTambah" id="t193TanggalSPKTambah" precision="day" value="" format="dd/mm/yyyy" />
                                &nbsp;&nbsp;
                                <g:field type="button" onclick="addSPK();" class="btn" name="btnAddSPK" id="btnAddSPK" value="Add" />
                            </div>
                        </div>
                        <br/><br/>
                        <g:render template="dataTablesSPK" />
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="upload_dokumen">
            <div class="row-fluid" style="margin-left: 30px">
                <div id="kiri21" class="span6">
                    <legend style="font-size: 14px">Upload Dokumen Pendukung</legend>
                    <div class="control-group">
                        <label class="control-label" for="m193JmlToleransiAsuransiBP" style="text-align: left">
                            <g:message code="customer.m193JmlToleransiAsuransiBP.label" default="Jenis Dokumen Pendukung" />
                            <span class="required-indicator">*</span>
                        </label>
                        <div class="controls">
                            <g:select name="kelengkapanDokumenAsuransi.id" id="kelengkapanDokumenAsuransi" required="" noSelection="['':'Pilih jenis dokumen pendukung']" onchange="changeJenisDokumen();" from="${com.kombos.customerprofile.KelengkapanDokumenAsuransi.list()}" optionKey="id" optionValue="m195NamaDokumenAsuransi" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="t195Keterangan" style="text-align: left">
                            <g:message code="customer.t195Keterangan.label" default="Keterangan" />
                            <span class="required-indicator">*</span>
                        </label>
                        <div class="controls">
                            <g:textField name="t195Keterangan" id="t195Keterangan" required="" maxlength="50" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="t195Foto" style="text-align: left">
                            <g:message code="default.upload.label" default="Upload"/>
                            <span class="required-indicator">*</span>
                        </label>

                        <div class="controls">
                            <input type="file" required="" name="t195Foto" id="t195Foto"
                                   onchange="fileUploadPopup(this.form, '${g.createLink(controller: 'customer', action: 'uploadImagePopup')}', 'divT195Foto');
                                   return false;"/>
                            <br/><br/>
                            <div id="divT195Foto">
                            </div>
                            <br/>
                        </div>
                    </div>

                </div>

                <div id="kanan21" class="span6">
                    <legend style="font-size: 14px">Kelengkapan Dokumen Pendukung</legend>
                    <br/>
                    <div id="cekArea">

                    </div>
                </div>
                <br/>
                <g:render template="dataTablesDokumen" />
            </div>
        </div>
    </div>
</div>
