
<%@ page import="com.kombos.customerprofile.HistoryCustomer" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="customer_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.peranCustomer.label" default="Peran Customer" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.t182NamaDepan.label" default="Nama Depan" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.t182NamaBelakang.label" default="Nama Belakang" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="customer.tgglTransaksi.label" default="Tanggal Transaksi" /></div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var CustomerTable;
var reloadCustomerTable;
$(function(){
	reloadCustomerTable = function() {
		CustomerTable.fnDraw();
	}

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	CustomerTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	CustomerTable = $('#customer_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		bFilter: false,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            return nRow;
        },
		"bSort": true,
		"bProcessing": true,
		"bDestroy":true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "customerCarsDatatablesList")}",
		"aoColumns": [



{
	"sName": "",
	"mDataProp": "peran",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "t182NamaDepan",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "t182NamaBelakang",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "t182TglTransaksi",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}



],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            aoData.push(
                {"name": 'idVehicle', "value": ${historyCustomerVehicleInstance?.id ? historyCustomerVehicleInstance?.id: idHistVehicle }}
            );

            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
		}
	});

});
</g:javascript>



