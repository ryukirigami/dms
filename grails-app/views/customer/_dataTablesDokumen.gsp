
<%@ page import="com.kombos.customerprofile.DokumenSPK" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="dokumen_datatables_${idTable}" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed" style="table-layout: fixed; width: 1000px">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="customer.kelengkapanDokumenAsuransi.label" default="Dokumen Pendukung" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="customer.t195Keterangan.label" default="Keterangan" /></div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var DokumenTable;
var reloadDokumenTable;
$(function(){
	reloadDokumenTable = function() {
		DokumenTable.fnDraw();
	}


	DokumenTable = $('#dokumen_datatables_${idTable}').dataTable({
		"sScrollX": "95%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		bFilter: false,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            return nRow;
        },
		"bSort": true,
		"bProcessing": true,
		"bDestroy":true,
//		"bServerSide": true,
//		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		%{--"sAjaxSource": "${g.createLink(action: "datatablesList")}",--}%
		"aoColumns": [



{
	"sName": "",
	"mDataProp": "kelengkapanDokumenAsuransi",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"500px",
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "t195Keterangan",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"500px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
		}
	});

});
</g:javascript>



