<%@ page import="com.kombos.customerprofile.HistoryCustomer" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
        <title><g:message code="customer.label" default="Customer"/></title>
        <r:require modules="baseapplayout" />
    <g:javascript>
    var validateVinCode;
    var addMore;
    var closeModal;
    var keyword = '';
    var saveNikah;
    var cancelNikah;
    var addNikah;
    var cekSama;

    $(function(){
        $('#inputNikah').hide()
        $('#saveNikahBtn').hide()
        $('#cancelNikahBtn').hide()

        var root="${resource()}";
        var kodeCust = $('#t182ID').val();
        var idVehicle="${historyCustomerVehicleInstance?.id ? historyCustomerVehicleInstance?.id : idHistVehicle}";
        var url = root+'/customer/findCustomerTampil?idVehicle='+idVehicle+'&kodeCust='+kodeCust;
        jQuery('#pilihCustomer').load(url);

        validateVinCode = function(){
            var t109WMI = $('#t109WMI').val();
            var t109VDS = $('#t109VDS').val();
            var t109CekDigit = $('#t109CekDigit').val();
            var t109VIS = $('#t109VIS').val();
            $('#spinner').fadeIn(1);
            $.ajax({
                url: '${request.contextPath}/customer/validate',
                data : {t109WMI : t109WMI,t109VDS : t109VDS,t109CekDigit : t109CekDigit, t109VIS : t109VIS },
                type: 'POST',
                success: function(data){
                    if(data.length>1){
                        $('#baseModel').html(data[0].data);
                        $('#grade').html(data[1].data);
                        $('#engine').html(data[2].data);
                        $('#gear').html(data[3].data);
                        $('#fullModelCode').html(data[4].data);
                        $('#fullModelVinCodeThn').val(data[5].data);
                        $('#fullModelVinCodeBln').html(data[6].data);
                    }else{
                        $('#baseModel').html(data[0].data);
                        $('#grade').html(data[0].data);
                        $('#engine').html(data[0].data);
                        $('#gear').html(data[0]).data;
                        $('#fullModelCode').html(data[0].data);
                        $('#fullModelVinCodeBln').html(data[0].data);
                        $('#fullModelVinCodeThn').val('');
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        }

        addMore = function(from){
            keyword = from;
            $("#addMoreContent").empty();
            $.ajax({type:'POST', url:'${request.contextPath}/'+from,
                success:function(data,textStatus){
                        $("#addMoreContent").html(data);
                        $("#addMoreModal").modal({
                            "backdrop" : "static",
                            "keyboard" : false,
                            "show" : true
                        }).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });



        }

        closeModal = function(modal){
            if(keyword!=''){
                var root="${resource()}";
                var url = root+'/customer/findList?cari='+keyword;
                jQuery('#'+keyword).load(url);
            }
            $('#'+modal+'Modal').modal('hide');
        }

        addNikah = function(){
            $('#nikah').hide();
            $('#addNikahBtn').hide();
            $('#inputNikah').show();
            $('#saveNikahBtn').show();
            $('#cancelNikahBtn').show();
        }

        saveNikah = function(){
            var txtnikah = $('#inputNikah').val();
            if(txtnikah == ""){
                alert("Status tidak boleh kosong");
                return;
            }

            $.ajax({type:'POST', url:'${request.contextPath}/customer/insertNikah',
                data : {status : txtnikah},
                success:function(data,textStatus){
                    if(data){
                       if(data == "duplicate"){
                            alert("Data Status Sudah Ada");
                            $('#inputNikah').val("");
                            $("#inputNikah").focus();
                       }else if(data == "gagal"){
                            alert("Gagal Menyimpan");
                            $('#inputNikah').val("");
                            $("#inputNikah").focus();
                       }else{
                          $('#inputNikah').val("");
                          $('#nikah').html(data);
                          toastr.success("Data Status Berhasil disimpan");
                          cancelNikah();
                       }

                     }

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
   		    });

        };

        cancelNikah = function(){
            $('#nikah').show();
            $('#addNikahBtn').show();
            $('#inputNikah').hide();
            $('#saveNikahBtn').hide();
            $('#cancelNikahBtn').hide();
        }

        cekSama = function(){
            var depan = $('#t182NamaDepan').val();
            var belakang = $('#t182NamaBelakang').val();
            var alamat = $('#t182Alamat').val();
            var noTelp = $('#t182NoHp').val();
            if($('#staSama').prop('checked')){
                if(depan=="" || alamat== "" || noTelp==""){
                    alert('Data Customer Belum Lengkap');
                    document.getElementById('staSama').checked = false;
                    return
                }else{
                    $('#t193NamaPolis').val(depan+' '+belakang);
                    $('#t193AlamatPolis').val(alamat);
                    $('#t193TelpPolis').val(noTelp);
                    $('#t193NamaPolis').prop('disabled', true);
                    $('#t193AlamatPolis').prop('disabled', true);
                    $('#t193TelpPolis').prop('disabled', true);
                    return
                }
            }else{
                $('#t193NamaPolis').val('');
                $('#t193AlamatPolis').val('');
                $('#t193TelpPolis').val('');
                $('#t193NamaPolis').prop('disabled', false);
                $('#t193AlamatPolis').prop('disabled', false);
                $('#t193TelpPolis').prop('disabled', false);
                return
            }
        }
    });

    function changeCustomer(){
        var idCustomer = $('#pilihCustomer').val();
        var idVehicle = $('#idVehicle').val();
        $('#spinner').fadeIn(1);
        $.ajax({
            url: '${request.contextPath}/customer',
            data: { idCustomer: idCustomer , idVehicle : idVehicle },
            type: "GET",dataType:"html",
            complete : function (req, err) {
                $('#main-content').html(req.responseText);
                $('#spinner').fadeOut();
            }
        });
    }

    function berhasil(){
        toastr.success("Data Tersimpan");
        reloadCustomerTable();
//        window.location.replace('#/reception');
    }

    function fileUpload(form, action_url, div_id) {
        // Create the iframe...
        var ext = $('#myFile').val();

        if(validateExtension(ext , 'foto') == false){
           alert("Anda hanya dapat meng-upload file berekstensi JPG, JPEG, PNG atau GIF");
           document.getElementById(div_id).innerHTML = "Upload Failed";
           document.getElementById("myFile").focus();
           return;
        }else{
            var iframe = document.createElement("iframe");
            iframe.setAttribute("id", "upload_iframe");
            iframe.setAttribute("name", "upload_iframe");
            iframe.setAttribute("width", "0");
            iframe.setAttribute("height", "0");
            iframe.setAttribute("border", "0");
            iframe.setAttribute("style", "width: 0; height: 0; border: none;");

            // Add to document...
            form.parentNode.appendChild(iframe);
            window.frames['upload_iframe'].name = "upload_iframe";

            iframeId = document.getElementById("upload_iframe");

            // Add event...
            var eventHandler = function () {

                if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
                else iframeId.removeEventListener("load", eventHandler, false);

                // Message from server...
                if (iframeId.contentDocument) {
                    content = iframeId.contentDocument.body.innerHTML;
                } else if (iframeId.contentWindow) {
                    content = iframeId.contentWindow.document.body.innerHTML;
                } else if (iframeId.document) {
                    content = iframeId.document.body.innerHTML;
                }

                document.getElementById(div_id).innerHTML = content;

                // Del the iframe...
                setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
            }

            if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
            if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);

            // Set properties of form...
            form.setAttribute("target", "upload_iframe");
            form.setAttribute("action", action_url);
            form.setAttribute("method", "post");
            form.setAttribute("enctype", "multipart/form-data");
            form.setAttribute("encoding", "multipart/form-data");

            // Submit the form...
            form.submit();

            document.getElementById(div_id).innerHTML = "Uploading...";
        }
    }

    function fileUploadPopup(form, action_url, div_id) {
        // Create the iframe...
        var ext = $('#t195Foto').val();

        if(validateExtension(ext , 'foto') == false){
           alert("Anda hanya dapat meng-upload file berekstensi JPG, JPEG, PNG atau GIF");
           document.getElementById(div_id).innerHTML = "Upload Failed";
           document.getElementById("t195Foto").focus();
           return;
        }else{
            var iframe = document.createElement("iframe");
            iframe.setAttribute("id", "upload_iframe");
            iframe.setAttribute("name", "upload_iframe");
            iframe.setAttribute("width", "0");
            iframe.setAttribute("height", "0");
            iframe.setAttribute("border", "0");
            iframe.setAttribute("style", "width: 0; height: 0; border: none;");

            // Add to document...
            form.parentNode.appendChild(iframe);
            window.frames['upload_iframe'].name = "upload_iframe";

            iframeId = document.getElementById("upload_iframe");

            // Add event...
            var eventHandler = function () {

                if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
                else iframeId.removeEventListener("load", eventHandler, false);

                // Message from server...
                if (iframeId.contentDocument) {
                    content = iframeId.contentDocument.body.innerHTML;
                } else if (iframeId.contentWindow) {
                    content = iframeId.contentWindow.document.body.innerHTML;
                } else if (iframeId.document) {
                    content = iframeId.document.body.innerHTML;
                }

                document.getElementById(div_id).innerHTML = content;

                // Del the iframe...
                setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
            }

            if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
            if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);

            // Set properties of form...
            form.setAttribute("target", "upload_iframe");
            form.setAttribute("action", action_url);
            form.setAttribute("method", "post");
            form.setAttribute("enctype", "multipart/form-data");
            form.setAttribute("encoding", "multipart/form-data");

            // Submit the form...
            form.submit();

            document.getElementById(div_id).innerHTML = "Uploading...";
        }
    }

    function uploadDokumen(form, action_url, div_id) {
        // Create the iframe...
        var ext = $('#myDocument').val();

        if(validateExtension(ext,'document') == false){
           alert("Anda hanya dapat meng-upload file berekstensi JPG, PNG, PDF atau file microsoft word");
           document.getElementById(div_id).innerHTML = "Upload Failed";
           document.getElementById("myDocument").focus();
           return;
        }else{
            var iframe = document.createElement("iframe");
            iframe.setAttribute("id", "upload_iframe");
            iframe.setAttribute("name", "upload_iframe");
            iframe.setAttribute("width", "0");
            iframe.setAttribute("height", "0");
            iframe.setAttribute("border", "0");
            iframe.setAttribute("style", "width: 0; height: 0; border: none;");

            // Add to document...
            form.parentNode.appendChild(iframe);
            window.frames['upload_iframe'].name = "upload_iframe";

            iframeId = document.getElementById("upload_iframe");

            // Add event...
            var eventHandler = function () {

                if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
                else iframeId.removeEventListener("load", eventHandler, false);

                // Message from server...
                if (iframeId.contentDocument) {
                    content = iframeId.contentDocument.body.innerHTML;
                } else if (iframeId.contentWindow) {
                    content = iframeId.contentWindow.document.body.innerHTML;
                } else if (iframeId.document) {
                    content = iframeId.document.body.innerHTML;
                }

                document.getElementById(div_id).innerHTML = content;

                // Del the iframe...
                setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
            }

            if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
            if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);

            // Set properties of form...
            form.setAttribute("target", "upload_iframe");
            form.setAttribute("action", action_url);
            form.setAttribute("method", "post");
            form.setAttribute("enctype", "multipart/form-data");
            form.setAttribute("encoding", "multipart/form-data");

            // Submit the form...
            form.submit();

            document.getElementById(div_id).innerHTML = "Uploading...";
        }
    }

    function validateExtension(v,from){
          var allowedImage = new Array("jpg","JPG","jpeg","JPEG","gif","GIF","png","PNG");
          var allowedDokumen = new Array("jpg","JPG","jpeg","JPEG","png","PNG","PDF","pdf","doc","DOC","DOCX","docx");

          if(from=='foto'){
              for(var ct=0;ct < allowedImage.length ; ct++ ){
                  sample = v.lastIndexOf(allowedImage[ct]);
                  if(sample != -1){return true;}
              }
          }else if(from=='document'){
              for(var ct=0;ct < allowedDokumen.length ; ct++ ){
                  sample = v.lastIndexOf(allowedDokumen[ct]);
                  if(sample != -1){return true;}
              }
          }
          return false;
    }

    function addSPK(){
        var nomor = $('#t193NomorSPKTambah').val();
        var tanggal = $('#t193TanggalSPKTambah').val();
        if(nomor=="" || nomor==null || tanggal=="" || tanggal==null){
            alert('Nomor SPK dan tanggal SPK tambahan tidak boleh kosong')
        }else{
            var checkID =[];
            $('#sendProblemFinding-table tbody .row-select').each(function() {
                var id = $(this).next("input:hidden").val();
                checkID.push(id);
            });
            var hasil = true
            if(checkID.length>0){
                for(var c=0 ;c < checkID.length;c++){
                    console.log(checkID[c]+' ------ '+nomor)
                    if(checkID[c]==nomor){
                        hasil=false
                        if(hasil==false){
                        break;
                        }
                    }
                }
            }
            if(hasil){
                AddSPKTable.fnAddData({
                    't193NomorSPKTambah': nomor,
                    't193TanggalSPKTambah': tanggal
                });
            }else{
                alert('Data yang anda masukan sudah ada');
            }
                AddSPKTable.fnAddData({
                    't193NomorSPKTambah': nomor,
                    't193TanggalSPKTambah': tanggal
                });
            }
    }

    $('input:radio[name=jenisCustomer]').click(function(){
        if($('input:radio[name=jenisCustomer]:nth(0)').is(':checked')){
            $('#company').val('');
            $("#company").prop('disabled', true);
        }else{
            $("#company").prop('disabled', false);
        }
    });

    $('input:radio[name=t193StaGantiTambahan]').click(function(){
        if($('input:radio[name=t193StaGantiTambahan]:nth(0)').is(':checked')){
            $('#t193NomorSPKTambah').val('');
            $('#t193TanggalSPKTambah').val('');
            $("#t193NomorSPKTambah").prop('disabled', false);
            $("#t193TanggalSPKTambah").prop('disabled', false);
            $("#btnAddSPK").prop('disabled', false);
        }else{
            $('#t193NomorSPKTambah').val('');
            $('#t193TanggalSPKTambah').val('');
            $("#t193NomorSPKTambah").prop('disabled', true);
            $("#t193TanggalSPKTambah").prop('disabled', true);
            $("#btnAddSPK").prop('disabled', true);
        }
    });

    function changeVendor(){
        var id = $('#vendorAsuransi').val();
        if(id==""){
            $('input[name=m193JmlToleransiAsuransiBP]').attr('checked',false);
            return
        }
        $('#spinner').fadeIn(1);
        $.ajax({
            url: '${request.contextPath}/customer/changeVendor',
            data : {id : id},
            type: 'POST',
            success: function(data){
                var $radios = $('input:radio[name=m193JmlToleransiAsuransiBP]');
                if(data==1){
                    $radios.filter('[value=1]').prop('checked', true);
                }else{
                    $radios.filter('[value=0]').prop('checked', true);
                }
            },error: function(xhr, textStatus, errorThrown) { alert(textStatus);}
        });
    }

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function checkPass()
    {
        var pass1 = document.getElementById('t182WebPassword');
        var pass2 = document.getElementById('t182WebPassword2');
        var message = document.getElementById('confirmMessage');
        var goodColor = "#66cc66";
        var badColor = "#ff6666";
        if(pass1.value == pass2.value){
            $('#hasilPass').val('ada');
            pass2.style.backgroundColor = goodColor;
            message.style.color = goodColor;
            message.innerHTML = "Passwords Match!"
        }else{
            $('#hasilPass').val('');
            pass2.style.backgroundColor = badColor;
            message.style.color = badColor;
            message.innerHTML = "Passwords Do Not Match!"
        }
    }

    function loadKabKota(from){
        var root="${resource()}";
        var id;
        if(from==1){
            id=$('#provinsi').val();
        }else{
            id=$('#provinsi2').val();
        }

        var url = root+'/customer/findKab?provinsi.id='+id;
        if(from==1){
            jQuery('#kabKota').load(url);
        }else{
            jQuery('#kabKota2').load(url);
        }
    }

    function loadKecamatan(from){
        var root="${resource()}";
        var id;
        if(from==1){
            id=$('#kabKota').val();
        }else{
            id=$('#kabKota2').val();
        }

        var url = root+'/customer/findKecamatan?kabKota.id='+id;
        if(from==1){
            jQuery('#kecamatan').load(url);
        }else{
            jQuery('#kecamatan2').load(url);
        }
    }

    function loadKelurahan(from){
        var root="${resource()}";
        var id;
        if(from==1){
            id=$('#kecamatan').val();
        }else{
            id=$('#kecamatan2').val();
        }

        var url = root+'/customer/findKelurahan?kecamatan.id='+id;
        if(from==1){
            jQuery('#kelurahan').load(url);
        }else{
            jQuery('#kelurahan2').load(url);
        }
    }

    function detailAsuransi(){
        keyword = '';
        $("#detailAsuransiModal").modal({
            "backdrop" : "static",
            "keyboard" : true,
            "show" : true
        }).css({'width': '1400px','margin-left': function () {return -($(this).width() / 2);}});
    }

</g:javascript>

</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="customer.label" default="Customer"/></span>
</div>

<div class="box">
    <div class="span12" id="customer-table">
        <g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="berhasil();"
                      update="customer-form"
                      url="[controller: 'customer', action: 'save']" onFailure="toastr.error('Gagal disimpan');">
            <fieldset class="form">
                <g:render template="form"/>
            </fieldset>
            <br/>
            <g:render template="dataTables"/>
            <fieldset class="buttons controls">
                <div style="margin-left: 35%">
            <g:if test="${menu=="sales"}">
                <a class="btn cancel" href="javascript:void(0);"
                   onclick="window.location.replace('#/salesQuotation')"><g:message code="default.button.cancel.label"
                                                                               default="Cancel"/></a>
            </g:if>
                    <g:else>
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="window.location.replace('#/reception')"><g:message code="default.button.cancel.label"
                                                                                   default="Cancel"/></a>
            </g:else>

                    <g:submitButton class="btn btn-primary create" name="save"
                                    value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                    %{--onclick="return confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}');" />--}%
                </div>
            </fieldset>
        </g:formRemote>
    </div>

    <div class="span7" id="customer-form" style="display: none;"></div>

    <div id="detailAsuransiModal" class="modal hide" style="width: 1380px;">
        <div class="modal-dialog" style="width: 1380px;">
            <div class="modal-content" style="width: 1400px;">
                <div class="modal-body" style="max-height: 550px; width: 1350px;">
                    <div id="detailAsuransiContent">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <div class="iu-content">
                            <g:formRemote class="form-horizontal" name="createAsuransi" on404="alert('not found!');"
                                          onSuccess="alert('successs');" update="customerAsuransi-form"
                                          url="[controller: 'customer', action: 'saveAsuransi']"
                                          onFailure="toastr.error('Gagal disimpan');">
                                <fieldset class="form">
                                    <g:render template="formDetailAsuransi"/>
                                </fieldset>
                                <br/>

                                <fieldset class="buttons controls">
                                    <div class="pull-right">
                                        <a class="btn cancel" href="javascript:void(0);" onclick="closeModal('detailAsuransi');"><g:message
                                                code="default.button.close.label" default="Close"/></a>
                                        <g:submitButton class="btn btn-primary create" name="saveAsuransi"
                                                        value="${message(code: 'default.button.save.label', default: 'Save')}"/>
                                    </div>
                                </fieldset>

                                <br/>
                            </g:formRemote>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div id="addMoreModal" class="modal hide" style="width: 1200px;">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 1200px; height: 550">
                <div class="modal-body" style="max-height: 550px;">
                    <div id="addMoreContent">
                        <div class="iu-content"></div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button name="closeModal" type="button" onclick="closeModal('addMore');" class="btn cancel">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

