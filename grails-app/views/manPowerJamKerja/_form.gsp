<%@ page import="com.kombos.administrasi.NamaManPower; com.kombos.administrasi.ManPowerJamKerja" %>

<g:javascript>
    $(function(){
        $('#namaManPower').typeahead({
            source: function (query, process) {
                return $.get('${request.contextPath}/manPowerJamKerja/getNamaManPower', { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });
    });

    function cekJamAkhir(from){
        var from=from;
        var jamul1 = $('#t022JamMulai1_hour').val();
        var jamul2 = $('#t022JamMulai2_hour').val();
        var jamul3 = $('#t022JamMulai3_hour').val();
        var jamul4 = $('#t022JamMulai4_hour').val();
        var jamSel1 = $('#t022JamSelesai1_hour').val();
        var jamSel2 = $('#t022JamSelesai2_hour').val();
        var jamSel3 = $('#t022JamSelesai3_hour').val();
        var jamSel4 = $('#t022JamSelesai4_hour').val();
        var menMul1 = $('#t022JamMulai1_minute').val();
        var menMul2 = $('#t022JamMulai2_minute').val();
        var menMul3 = $('#t022JamMulai3_minute').val();
        var menMul4 = $('#t022JamMulai4_minute').val();
        var menSel1 = $('#t022JamSelesai1_minute').val();
        var menSel2 = $('#t022JamSelesai2_minute').val();
        var menSel3 = $('#t022JamSelesai3_minute').val();
        var menSel4 = $('#t022JamSelesai4_minute').val();

        if(from=='jamkerja1'){
            if(parseInt(jamul1)>parseInt(jamSel1)){
                if(parseInt(jamul1)==23 && parseInt(menMul1)==59){
                    $('#t022JamSelesai1_hour').val('00');
                    $('#t022JamSelesai1_minute').val('00');
                    return
                }

                $('#t022JamSelesai1_hour').val(jamul1);
                if(parseInt(menMul1)==59){
                    $('#t022JamSelesai1_hour').val(parseInt(jamul1)+1);
                    $('#t022JamSelesai1_minute').val('00');
                }
                return
            }
            if(parseInt(jamul1)==parseInt(jamSel1) && parseInt(menMul1) > parseInt(menSel1)){
                if(parseInt(menMul1)==59){
                    $('#t022JamSelesai1_hour').val(parseInt(jamul1)+1);
                }
                $('#t022JamSelesai1_minute').val(parseInt(menMul1)+1);
                return
            }
        }else if(from=='jamkerja2'){
            if(parseInt(jamul2)>parseInt(jamSel2)){
                if(parseInt(jamul2)==23 && parseInt(menMul2)==59){
                    $('#t022JamSelesai2_hour').val('00');
                    $('#t022JamSelesai2_minute').val('00');
                    return
                }

                $('#t022JamSelesai2_hour').val(jamul2);
                if(parseInt(menMul2)==59){
                    $('#t022JamSelesai2_hour').val(parseInt(jamul2)+1);
                    $('#t022JamSelesai2_minute').val('00');
                }
                return
            }
            if(parseInt(jamul2)==parseInt(jamSel2) && parseInt(menMul2) > parseInt(menSel2)){
                if(parseInt(menMul2)==59){
                    $('#t022JamSelesai2_hour').val(parseInt(jamul2)+1);
                }
                $('#t022JamSelesai2_minute').val(parseInt(menMul2)+1);
                return
            }
        }else if(from=='jamkerja3'){
            if(parseInt(jamul3)>parseInt(jamSel3)){
                if(parseInt(jamul3)==23 && parseInt(menMul3)==59){
                    $('#t022JamSelesai3_hour').val('00');
                    $('#t022JamSelesai3_minute').val('00');
                    return
                }

                $('#t022JamSelesai3_hour').val(jamul3);
                if(parseInt(menMul3)==59){
                    $('#t022JamSelesai3_hour').val(parseInt(jamul3)+1);
                    $('#t022JamSelesai3_minute').val('00');
                }
                return
            }
            if(parseInt(jamul3)==parseInt(jamSel3) && parseInt(menMul3) > parseInt(menSel3)){
                if(parseInt(menMul3)==59){
                    $('#t022JamSelesai3_hour').val(parseInt(jamul3)+1);
                }
                $('#t022JamSelesai3_minute').val(parseInt(menMul3)+1);
                return
            }
        }
        if(from=='jamkerja4'){
            if(parseInt(jamul4)>parseInt(jamSel4)){
                if(parseInt(jamul4)==23 && parseInt(menMul4)==59){
                    $('#t022JamSelesai4_hour').val('00');
                    $('#t022JamSelesai4_minute').val('00');
                    return
                }

                $('#t022JamSelesai4_hour').val(jamul4);
                if(parseInt(menMul4)==59){
                    $('#t022JamSelesai4_hour').val(parseInt(jamul4)+1);
                    $('#t022JamSelesai4_minute').val('00');
                }
                return
            }
            if(parseInt(jamul4)==parseInt(jamSel4) && parseInt(menMul4) > parseInt(menSel4)){
                if(parseInt(menMul4)==59){
                    $('#t022JamSelesai4_hour').val(parseInt(jamul4)+1);
                }
                $('#t022JamSelesai4_minute').val(parseInt(menMul4)+1);
                return
            }

        }
    };

    var checkin = $('#t022Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
        $('#t022TanggalAkhir')[0].focus();
    }).data('datepicker');
    
    var checkout = $('#t022TanggalAkhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
    
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerJamKerjaInstance, field: 'namaManPower', 'error')}">
	<label class="control-label" for="namaManPower">
		<g:message code="manPowerJamKerja.namaManPower.label" default="Nama Man Power/Group" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField required="" name="namaManPower" id="namaManPower"  class="typeahead" value="${manPowerJamKerjaInstance?.namaManPower?.t015NamaLengkap}" autocomplete="off"/>
    </div>
</div>
<g:javascript>
    document.getElementById("namaManPower").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: manPowerJamKerjaInstance, field: 't022Tanggal', 'error')} required">
	<label class="control-label" for="t022Tanggal">
		<g:message code="manPowerJamKerja.t022Tanggal.label" default="Periode Tanggal" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <ba:datePicker name="t022Tanggal" id="t022Tanggal" precision="day" value="${manPowerJamKerjaInstance?.t022Tanggal}" format="dd/mm/yyyy" required="true"/>
        <br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s.d.<br/><br/>
        <ba:datePicker name="t022TanggalAkhir" id="t022TanggalAkhir" precision="day" value="${manPowerJamKerjaInstance?.t022TanggalAkhir}" format="dd/mm/yyyy" required="true"/>
    </div>
</div>

<fieldset>
  <legend>Jam Kerja 1</legend>
  <div class="control-group fieldcontain ${hasErrors(bean: manPowerJamKerjaInstance, field: 't022JamMulai1', 'error')} required">
	<label class="control-label" for="t022JamMulai1">
		<g:message code="manPowerJamKerja.t022JamMulai1.label" default="Jam Mulai" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<select onchange="return cekJamAkhir('jamkerja1');" id="t022JamMulai1_hour" name="t022JamMulai1_hour" style="width: 60px" required="">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamMulai1?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamMulai1?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }

                    }
                }%
            </select> H :
            <select onchange="return cekJamAkhir('jamkerja1');" id="t022JamMulai1_minute" name="t022JamMulai1_minute" style="width: 60px" required="">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamMulai1?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamMulai1?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerJamKerjaInstance, field: 't022JamSelesai1', 'error')} required">
	<label class="control-label" for="t022JamSelesai1">
		<g:message code="manPowerJamKerja.t022JamSelesai1.label" default="Jam Selesai" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<select id="t022JamSelesai1_hour" onchange="return cekJamAkhir('jamkerja1');" name="t022JamSelesai1_hour" style="width: 60px" required="">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai1?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai1?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }

                    }
                }%
            </select> H :
            <select onchange="return cekJamAkhir('jamkerja1');" id="t022JamSelesai1_minute" name="t022JamSelesai1_minute" style="width: 60px" required="">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai1?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai1?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
	</div>
</div>
</fieldset>

<fieldset>
  <legend>Jam Kerja 2</legend>
  <div class="control-group fieldcontain ${hasErrors(bean: manPowerJamKerjaInstance, field: 't022JamMulai2', 'error')} required">
	<label class="control-label" for="t022JamMulai2">
		<g:message code="manPowerJamKerja.t022JamMulai2.label" default="Jam Mulai" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<select onchange="return cekJamAkhir('jamkerja2')" id="t022JamMulai2_hour" name="t022JamMulai2_hour" style="width: 60px" required="">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamMulai2?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamMulai2?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }

                    }
                }%
            </select> H :
            <select onchange="return cekJamAkhir('jamkerja2');" id="t022JamMulai2_minute" name="t022JamMulai2_minute" style="width: 60px" required="">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamMulai2?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamMulai2?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerJamKerjaInstance, field: 't022JamSelesai2', 'error')} required">
	<label class="control-label" for="t022JamSelesai2">
		<g:message code="manPowerJamKerja.t022JamSelesai2.label" default="Jam Selesai" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<select id="t022JamSelesai2_hour" onchange="return cekJamAkhir('jamkerja2');" name="t022JamSelesai2_hour" style="width: 60px" required="">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai2?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai2?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }

                    }
                }%
            </select> H :
            <select onchange="return cekJamAkhir('jamkerja2');"  id="t022JamSelesai2_minute" name="t022JamSelesai2_minute" style="width: 60px" required="">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai2?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai2?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
	</div>
</div>
</fieldset>

<fieldset>
  <legend>Jam Kerja 3</legend>
  <div class="control-group fieldcontain ${hasErrors(bean: manPowerJamKerjaInstance, field: 't022JamMulai3', 'error')} required">
	<label class="control-label" for="t022JamMulai3">
		<g:message code="manPowerJamKerja.t022JamMulai3.label" default="Jam Mulai" />
	</label>
	<div class="controls">
	<select onchange="return cekJamAkhir('jamkerja3');" id="t022JamMulai3_hour" name="t022JamMulai3_hour" style="width: 60px" required="">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamMulai3?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamMulai3?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }

                    }
                }%
            </select> H :
            <select onchange="return cekJamAkhir('jamkerja3');"  id="t022JamMulai3_minute" name="t022JamMulai3_minute" style="width: 60px" required="">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamMulai3?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamMulai3?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerJamKerjaInstance, field: 't022JamSelesai3', 'error')} required">
	<label class="control-label" for="t022JamSelesai3">
		<g:message code="manPowerJamKerja.t022JamSelesai3.label" default="Jam Selesai" />
	</label>
	<div class="controls">
	<select id="t022JamSelesai3_hour" onchange="return cekJamAkhir('jamkerja3');" name="t022JamSelesai3_hour" style="width: 60px" required="">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai3?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai3?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }

                    }
                }%
            </select> H :
            <select onchange="return cekJamAkhir('jamkerja3');"  id="t022JamSelesai3_minute" name="t022JamSelesai3_minute" style="width: 60px" required="">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai3?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai3?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
	</div>
</div>
</fieldset>

<fieldset>
  <legend>Jam Kerja 4</legend>
  <div class="control-group fieldcontain ${hasErrors(bean: manPowerJamKerjaInstance, field: 't022JamMulai4', 'error')} required">
	<label class="control-label" for="t022JamMulai4">
		<g:message code="manPowerJamKerja.t022JamMulai4.label" default="Jam Mulai" />
	</label>
	<div class="controls">
	<select onchange="return cekJamAkhir('jamkerja4');" id="t022JamMulai4_hour" name="t022JamMulai4_hour" style="width: 60px" required="">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamMulai4?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamMulai4?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }

                    }
                }%
            </select> H :
            <select onchange="return cekJamAkhir('jamkerja4');"  id="t022JamMulai4_minute" name="t022JamMulai4_minute" style="width: 60px" required="">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamMulai4?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamMulai4?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerJamKerjaInstance, field: 't022JamSelesai4', 'error')} required">
	<label class="control-label" for="t022JamSelesai4">
		<g:message code="manPowerJamKerja.t022JamSelesai4.label" default="Jam Selesai" />
	</label>
	<div class="controls">
	<select id="t022JamSelesai4_hour" onchange="return cekJamAkhir('jamkerja4');" name="t022JamSelesai4_hour" style="width: 60px" required="">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai4?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai4?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }

                    }
                }%
            </select> H :
            <select onchange="return cekJamAkhir('jamkerja4');" id="t022JamSelesai4_minute" name="t022JamSelesai4_minute" style="width: 60px" required="">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai4?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerJamKerjaInstance?.t022JamSelesai4?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
	</div>
</div>
</fieldset>

<g:javascript>
    document.getElementById("namaManPower").focus();
</g:javascript>