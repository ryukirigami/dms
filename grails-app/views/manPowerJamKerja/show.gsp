

<%@ page import="com.kombos.administrasi.ManPowerJamKerja" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'manPowerJamKerja.label', default: 'Jam Kerja Man Power')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteManPowerJamKerja;

$(function(){ 
	deleteManPowerJamKerja=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/manPowerJamKerja/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadManPowerJamKerjaTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-manPowerJamKerja" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="manPowerJamKerja"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${manPowerJamKerjaInstance?.namaManPower}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaManPower-label" class="property-label"><g:message
					code="manPowerJamKerja.namaManPower.label" default="Nama Man Power" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaManPower-label">
						%{--<ba:editableValue
								bean="${manPowerJamKerjaInstance}" field="namaManPower"
								url="${request.contextPath}/ManPowerJamKerja/updatefield" type="text"
								title="Enter namaManPower" onsuccess="reloadManPowerJamKerjaTable();" />--}%
							
								${manPowerJamKerjaInstance?.namaManPower?.t015NamaLengkap?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerJamKerjaInstance?.t022Tanggal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t022Tanggal-label" class="property-label"><g:message
					code="manPowerJamKerja.t022Tanggal.label" default="Periode Tanggal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t022Tanggal-label">
						%{--<ba:editableValue
								bean="${manPowerJamKerjaInstance}" field="t022Tanggal"
								url="${request.contextPath}/ManPowerJamKerja/updatefield" type="text"
								title="Enter t022Tanggal" onsuccess="reloadManPowerJamKerjaTable();" />--}%
							
								<g:formatDate date="${manPowerJamKerjaInstance?.t022Tanggal}" format="dd/MM/yyyy"/>
                                                                &nbsp;&nbsp;s.d.&nbsp;&nbsp;
                                                                <g:formatDate date="${manPowerJamKerjaInstance?.t022TanggalAkhir}" format="dd/MM/yyyy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerJamKerjaInstance?.t022JamMulai1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t022JamMulai1-label" class="property-label"><g:message
					code="manPowerJamKerja.t022JamMulai1.label" default="Jam Mulai 1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t022JamMulai1-label">
						%{--<ba:editableValue
								bean="${manPowerJamKerjaInstance}" field="t022JamMulai1"
								url="${request.contextPath}/ManPowerJamKerja/updatefield" type="text"
								title="Enter t022JamMulai1" onsuccess="reloadManPowerJamKerjaTable();" />--}%							
								<g:formatDate date="${manPowerJamKerjaInstance?.t022JamMulai1}" format="HH:mm"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerJamKerjaInstance?.t022JamSelesai1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t022JamSelesai1-label" class="property-label"><g:message
					code="manPowerJamKerja.t022JamSelesai1.label" default="Jam Selesai 1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t022JamSelesai1-label">
						%{--<ba:editableValue
								bean="${manPowerJamKerjaInstance}" field="t022JamSelesai1"
								url="${request.contextPath}/ManPowerJamKerja/updatefield" type="text"
								title="Enter t022JamSelesai1" onsuccess="reloadManPowerJamKerjaTable();" />--}%
							
								<g:formatDate date="${manPowerJamKerjaInstance?.t022JamSelesai1}" format="HH:mm"/>
                                                                
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerJamKerjaInstance?.t022JamMulai2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t022JamMulai2-label" class="property-label"><g:message
					code="manPowerJamKerja.t022JamMulai2.label" default="Jam Mulai 2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t022JamMulai2-label">
						%{--<ba:editableValue
								bean="${manPowerJamKerjaInstance}" field="t022JamMulai2"
								url="${request.contextPath}/ManPowerJamKerja/updatefield" type="text"
								title="Enter t022JamMulai2" onsuccess="reloadManPowerJamKerjaTable();" />
							--}%
								<g:formatDate date="${manPowerJamKerjaInstance?.t022JamMulai2}" format="HH:mm"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerJamKerjaInstance?.t022JamSelesai2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t022JamSelesai2-label" class="property-label"><g:message
					code="manPowerJamKerja.t022JamSelesai2.label" default="Jam Selesai 2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t022JamSelesai2-label">
						%{--<ba:editableValue
								bean="${manPowerJamKerjaInstance}" field="t022JamSelesai2"
								url="${request.contextPath}/ManPowerJamKerja/updatefield" type="text"
								title="Enter t022JamSelesai2" onsuccess="reloadManPowerJamKerjaTable();" />
							--}%
								<g:formatDate date="${manPowerJamKerjaInstance?.t022JamSelesai2}" format="HH:mm"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerJamKerjaInstance?.t022JamMulai3}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t022JamMulai3-label" class="property-label"><g:message
					code="manPowerJamKerja.t022JamMulai3.label" default="Jam Mulai 3" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t022JamMulai3-label">
						%{--<ba:editableValue
								bean="${manPowerJamKerjaInstance}" field="t022JamMulai3"
								url="${request.contextPath}/ManPowerJamKerja/updatefield" type="text"
								title="Enter t022JamMulai3" onsuccess="reloadManPowerJamKerjaTable();" />
							--}%
								<g:formatDate date="${manPowerJamKerjaInstance?.t022JamMulai3}" format="HH:mm"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerJamKerjaInstance?.t022JamSelesai3}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t022JamSelesai3-label" class="property-label"><g:message
					code="manPowerJamKerja.t022JamSelesai3.label" default="Jam Selesai 3" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t022JamSelesai3-label">
						%{--<ba:editableValue
								bean="${manPowerJamKerjaInstance}" field="t022JamSelesai3"
								url="${request.contextPath}/ManPowerJamKerja/updatefield" type="text"
								title="Enter t022JamSelesai3" onsuccess="reloadManPowerJamKerjaTable();" />
							--}%
								<g:formatDate date="${manPowerJamKerjaInstance?.t022JamSelesai3}" format="HH:mm"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerJamKerjaInstance?.t022JamMulai4}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t022JamMulai4-label" class="property-label"><g:message
					code="manPowerJamKerja.t022JamMulai4.label" default="Jam Mulai 4" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t022JamMulai4-label">
						%{--<ba:editableValue
								bean="${manPowerJamKerjaInstance}" field="t022JamMulai4"
								url="${request.contextPath}/ManPowerJamKerja/updatefield" type="text"
								title="Enter t022JamMulai4" onsuccess="reloadManPowerJamKerjaTable();" />
							--}%
								<g:formatDate date="${manPowerJamKerjaInstance?.t022JamMulai4}" format="HH:mm"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerJamKerjaInstance?.t022JamSelesai4}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t022JamSelesai4-label" class="property-label"><g:message
					code="manPowerJamKerja.t022JamSelesai4.label" default="Jam Selesai 4" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t022JamSelesai4-label">
						%{--<ba:editableValue
								bean="${manPowerJamKerjaInstance}" field="t022JamSelesai4"
								url="${request.contextPath}/ManPowerJamKerja/updatefield" type="text"
								title="Enter t022JamSelesai4" onsuccess="reloadManPowerJamKerjaTable();" />
							--}%
								<g:formatDate date="${manPowerJamKerjaInstance?.t022JamSelesai4}" format="HH:mm"/>
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${manPowerJamKerjaInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="manPowerJamKerja.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${manPowerJamKerjaInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${manPowerJamKerjaInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${manPowerJamKerjaInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="manPowerJamKerja.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${manPowerJamKerjaInstance}" field="dateCreated"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%

                        <g:formatDate date="${manPowerJamKerjaInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${manPowerJamKerjaInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="manPowerJamKerja.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${manPowerJamKerjaInstance}" field="createdBy"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${manPowerJamKerjaInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${manPowerJamKerjaInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="manPowerJamKerja.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${manPowerJamKerjaInstance}" field="lastUpdated"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%

                        <g:formatDate date="${manPowerJamKerjaInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${manPowerJamKerjaInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="manPowerJamKerja.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${manPowerJamKerjaInstance}" field="updatedBy"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${manPowerJamKerjaInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${manPowerJamKerjaInstance?.id}"
					update="[success:'manPowerJamKerja-form',failure:'manPowerJamKerja-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteManPowerJamKerja('${manPowerJamKerjaInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
