
<%@ page import="com.kombos.administrasi.ManPowerJamKerja" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        console.log(charCode)
        if (charCode > 31 && (charCode < 48 || charCode > 57)){
            if(charCode==58 && evt.shiftKey){
                return true;
            }
            return false;
        }else{
            console.log('true')
            return true;
        }
    }
</g:javascript>

<table id="manPowerJamKerja_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
    width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px; text-align: center" rowspan="2">
				<div><g:message code="manPowerJamKerja.namaManPower.label" default="Nama Man Power" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; text-align: center" colspan="2">
				<div><g:message code="manPowerJamKerja.t022Tanggal.label" default="Tanggal" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; text-align: center" colspan="2">
				<div><g:message code="manPowerJamKerja.t022JamKerja1.label" default="Jam Kerja 1" /></div>
			</th>
                        <th style="border-bottom: none;padding: 5px; text-align: center" colspan="2">
				<div><g:message code="manPowerJamKerja.t022JamKerja2.label" default="Jam Kerja 2" /></div>
			</th>
                        <th style="border-bottom: none;padding: 5px; text-align: center" colspan="2">
				<div><g:message code="manPowerJamKerja.t022JamKerja3.label" default="Jam Kerja 3" /></div>
			</th>
                        <th style="border-bottom: none;padding: 5px; text-align: center" colspan="2">
				<div><g:message code="manPowerJamKerja.t022JamKerja4.label" default="Jam Kerja 4" /></div>
			</th>
                </tr>
		<tr>

            <th style="border-bottom: none;padding: 5px; text-align: center">
                <div><g:message code="manPowerJamKerja.t022Tanggal.label" default="Mulai" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px; text-align: center">
                <div><g:message code="manPowerJamKerja.t022TanggalAKhir.label" default="Berakhir" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px; text-align: center">
				<div><g:message code="manPowerJamKerja.t022JamMulai1.label" default="Jam Mulai" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px; text-align: center">
				<div><g:message code="manPowerJamKerja.t022JamSelesai1.label" default="Jam Selesai" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; text-align: center">
				<div><g:message code="manPowerJamKerja.t022JamMulai2.label" default="Jam Mulai" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; text-align: center">
				<div><g:message code="manPowerJamKerja.t022JamSelesai2.label" default="Jam Selesai" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; text-align: center">
				<div><g:message code="manPowerJamKerja.t022JamMulai3.label" default="Jam Mulai" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;  text-align: center">
				<div><g:message code="manPowerJamKerja.t022JamSelesai3.label" default="Jam Selesai" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;  text-align: center">
				<div><g:message code="manPowerJamKerja.t022JamMulai4.label" default="Jam Mulai" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; text-align: center">
				<div><g:message code="manPowerJamKerja.t022JamSelesai4.label" default="Jam Selesai" /></div>
			</th>


		</tr>
                <tr>
                  <th style="border-top: none;padding: 5px;">
				<div id="filter_namaManPower" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaManPower" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t022Tanggal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t022Tanggal" value="date.struct">
					<input type="hidden" name="search_t022Tanggal_day" id="search_t022Tanggal_day" value="">
					<input type="hidden" name="search_t022Tanggal_month" id="search_t022Tanggal_month" value="">
					<input type="hidden" name="search_t022Tanggal_year" id="search_t022Tanggal_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t022Tanggal_dp" value="" id="search_t022Tanggal" class="search_init">
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t022TanggalAkhir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                    <input type="hidden" name="search_t022Tanggal" value="date.struct">
                    <input type="hidden" name="search_t022Tanggal_day" id="search_t022TanggalAkhir_day" value="">
                    <input type="hidden" name="search_t022Tanggal_month" id="search_t022TanggalAkhir_month" value="">
                    <input type="hidden" name="search_t022Tanggal_year" id="search_t022TanggalAkhir_year" value="">
                    <input type="text" data-date-format="dd/mm/yyyy" name="search_t022TanggalAkhir_dp" value="" id="search_t022TanggalAkhir" class="search_init">
                </div>
            </th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t022JamMulai1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" maxlength="5" onkeypress="return isNumberKey(event);" name="search_t022JamMulai1" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t022JamSelesai1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" maxlength="5" onkeypress="return isNumberKey(event);" name="search_t022JamSelesai1" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t022JamMulai2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" maxlength="5" onkeypress="return isNumberKey(event);" name="search_t022JamMulai2" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t022JamSelesai2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" maxlength="5" onkeypress="return isNumberKey(event);" name="search_t022JamSelesai2" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t022JamMulai3" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" maxlength="5" onkeypress="return isNumberKey(event);" name="search_t022JamMulai3" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t022JamSelesai3" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" maxlength="5" onkeypress="return isNumberKey(event);" name="search_t022JamSelesai3" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t022JamMulai4" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" maxlength="5" onkeypress="return isNumberKey(event);" name="search_t022JamMulai4" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t022JamSelesai4" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" maxlength="5" onkeypress="return isNumberKey(event);" name="search_t022JamSelesai4" class="search_init" />
				</div>
			</th>
                </tr>
	</thead>
</table>

<g:javascript>
var manPowerJamKerjaTable;
var reloadManPowerJamKerjaTable;
$(function(){
	
	reloadManPowerJamKerjaTable = function() {
		manPowerJamKerjaTable.fnDraw();
	}
	
    var recordsjamKerjaperpage = [];
    var anJamKerjaSelected;
    var jmlRecJamKerjaPerPage=0;
    var id;


	
	$('#search_t022Tanggal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t022Tanggal_day').val(newDate.getDate());
			$('#search_t022Tanggal_month').val(newDate.getMonth()+1);
			$('#search_t022Tanggal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			manPowerJamKerjaTable.fnDraw();	
	});

	

	$('#search_t022TanggalAkhir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t022TanggalAkhir_day').val(newDate.getDate());
			$('#search_t022TanggalAkhir_month').val(newDate.getMonth()+1);
			$('#search_t022TanggalAkhir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			manPowerJamKerjaTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	manPowerJamKerjaTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	manPowerJamKerjaTable = $('#manPowerJamKerja_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsJamKerja = $("#manPowerJamKerja_datatables tbody .row-select");
            var jmlJamKerjaCek = 0;
            var nRow;
            var idRec;
            rsJamKerja.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsjamKerjaperpage[idRec]=="1"){
                    jmlJamKerjaCek = jmlJamKerjaCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsjamKerjaperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecJamKerjaPerPage = rsJamKerja.length;
            if(jmlJamKerjaCek==jmlRecJamKerjaPerPage && jmlRecJamKerjaPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "namaManPower",
	"mDataProp": "namaManPower",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "t022Tanggal",
	"mDataProp": "t022Tanggal",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "t022TanggalAkhir",
	"mDataProp": "t022TanggalAkhir",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "t022JamMulai1",
	"mDataProp": "t022JamMulai1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t022JamSelesai1",
	"mDataProp": "t022JamSelesai1",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t022JamMulai2",
	"mDataProp": "t022JamMulai2",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t022JamSelesai2",
	"mDataProp": "t022JamSelesai2",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t022JamMulai3",
	"mDataProp": "t022JamMulai3",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t022JamSelesai3",
	"mDataProp": "t022JamSelesai3",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t022JamMulai4",
	"mDataProp": "t022JamMulai4",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t022JamSelesai4",
	"mDataProp": "t022JamSelesai4",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var namaManPower = $('#filter_namaManPower input').val();
						if(namaManPower){
							aoData.push(
									{"name": 'sCriteria_namaManPower', "value": namaManPower}
							);
						}

						var t022Tanggal = $('#search_t022Tanggal').val();
						var t022TanggalDay = $('#search_t022Tanggal_day').val();
						var t022TanggalMonth = $('#search_t022Tanggal_month').val();
						var t022TanggalYear = $('#search_t022Tanggal_year').val();
						
						if(t022Tanggal){
							aoData.push(
									{"name": 'sCriteria_t022Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_t022Tanggal_dp', "value": t022Tanggal},
									{"name": 'sCriteria_t022Tanggal_day', "value": t022TanggalDay},
									{"name": 'sCriteria_t022Tanggal_month', "value": t022TanggalMonth},
									{"name": 'sCriteria_t022Tanggal_year', "value": t022TanggalYear}
							);
						}

						var t022TanggalAkhir = $('#search_t022TanggalAkhir').val();
						var t022TanggalAkhirDay = $('#search_t022TanggalAkhir_day').val();
						var t022TanggalAkhirMonth = $('#search_t022TanggalAkhir_month').val();
						var t022TanggalAkhirYear = $('#search_t022TanggalAkhir_year').val();
						
						if(t022TanggalAkhir){
							aoData.push(
									{"name": 'sCriteria_t022TanggalAkhir', "value": "date.struct"},
									{"name": 'sCriteria_t022TanggalAkhir_dp', "value": t022TanggalAkhir},
									{"name": 'sCriteria_t022TanggalAkhir_day', "value": t022TanggalAkhirDay},
									{"name": 'sCriteria_t022TanggalAkhir_month', "value": t022TanggalAkhirMonth},
									{"name": 'sCriteria_t022TanggalAkhir_year', "value": t022TanggalAkhirYear}
							);
						}

						var t022JamMulai1 = $('#filter_t022JamMulai1 input').val();
                        if(t022JamMulai1){
                            aoData.push(
                                    {"name": 'sCriteria_t022JamMulai1', "value": t022JamMulai1}
                            );
                        }

                        var t022JamSelesai1 = $('#filter_t022JamSelesai1 input').val();
                        if(t022JamSelesai1){
                            aoData.push(
                                    {"name": 'sCriteria_t022JamSelesai1', "value": t022JamSelesai1}
                            );
                        }

                        var t022JamMulai2 = $('#filter_t022JamMulai2 input').val();
                        if(t022JamMulai2){
                            aoData.push(
                                    {"name": 'sCriteria_t022JamMulai2', "value": t022JamMulai2}
                            );
                        }

                        var t022JamSelesai2 = $('#filter_t022JamSelesai2 input').val();
                        if(t022JamSelesai2){
                            aoData.push(
                                    {"name": 'sCriteria_t022JamSelesai2', "value": t022JamSelesai2}
                            );
                        }

                        var t022JamMulai3 = $('#filter_t022JamMulai3 input').val();
                        if(t022JamMulai3){
                            aoData.push(
                                    {"name": 'sCriteria_t022JamMulai3', "value": t022JamMulai3}
                            );
                        }

                        var t022JamSelesai3 = $('#filter_t022JamSelesai3 input').val();
                        if(t022JamSelesai3){
                            aoData.push(
                                    {"name": 'sCriteria_t022JamSelesai3', "value": t022JamSelesai3}
                            );
                        }

                        var t022JamMulai4 = $('#filter_t022JamMulai4 input').val();
                        if(t022JamMulai4){
                            aoData.push(
                                    {"name": 'sCriteria_t022JamMulai4', "value": t022JamMulai4}
                            );
                        }

                        var t022JamSelesai4 = $('#filter_t022JamSelesai4 input').val();
                        if(t022JamSelesai4){
                            aoData.push(
                                    {"name": 'sCriteria_t022JamSelesai4', "value": t022JamSelesai4}
                            );
                        }
	
						var t022xNamaUser = $('#filter_t022xNamaUser input').val();
						if(t022xNamaUser){
							aoData.push(
									{"name": 'sCriteria_t022xNamaUser', "value": t022xNamaUser}
							);
						}
	
						var t022xNamaDivisi = $('#filter_t022xNamaDivisi input').val();
						if(t022xNamaDivisi){
							aoData.push(
									{"name": 'sCriteria_t022xNamaDivisi', "value": t022xNamaDivisi}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {
        $("#manPowerJamKerja_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsjamKerjaperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsjamKerjaperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

	$('#manPowerJamKerja_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsjamKerjaperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anJamKerjaSelected = manPowerJamKerjaTable.$('tr.row_selected');
            if(jmlRecJamKerjaPerPage == anJamKerjaSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsjamKerjaperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
