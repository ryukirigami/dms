

<%@ page import="com.kombos.customerprofile.JenisPerusahaan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'jenisPerusahaan.label', default: 'Jenis Perusahaan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteJenisPerusahaan;

$(function(){ 
	deleteJenisPerusahaan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/jenisPerusahaan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadJenisPerusahaanTable();
   				expandTableLayout('jenisPerusahaan');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-jenisPerusahaan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="jenisPerusahaan"
			class="table table-bordered table-hover">
			<tbody>
                <g:if test="${jenisPerusahaanInstance?.id}">
                    <tr>
                        <td class="span2" style="text-align: right;">
                            <span id="m093ID-label" class="property-label">
                                <g:message code="jenisPerusahaan.ID.label" default="ID" />:
                            </span>
                        </td>

                        <td class="span3">
                            <span class="property-value" aria-labelledby="m093ID-label">
                                <g:fieldValue bean="${jenisPerusahaanInstance}" field="id"/>
                            </span>
                        </td>
                    </tr>
                </g:if>
				
				<g:if test="${jenisPerusahaanInstance?.namaJenisPerusahaan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaJenisPerusahaan-label" class="property-label"><g:message
					code="jenisPerusahaan.namaJenisPerusahaan.label" default="Nama Jenis Perusahaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaJenisPerusahaan-label">
							
								<g:fieldValue bean="${jenisPerusahaanInstance}" field="namaJenisPerusahaan"/>
							
						</span></td>
					
				</tr>
				</g:if>

			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('jenisPerusahaan');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${jenisPerusahaanInstance?.id}"
					update="[success:'jenisPerusahaan-form',failure:'jenisPerusahaan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteJenisPerusahaan('${jenisPerusahaanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
