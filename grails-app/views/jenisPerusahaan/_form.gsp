<%@ page import="com.kombos.customerprofile.JenisPerusahaan" %>

<div class="control-group fieldcontain ${hasErrors(bean: jenisPerusahaanInstance, field: 'namaJenisPerusahaan', 'error')} required">
	<label class="control-label" for="namaJenisPerusahaan">
		<g:message code="jenisPerusahaan.namaJenisPerusahaan.label" default="Nama Jenis Perusahaan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="namaJenisPerusahaan" required="" value="${jenisPerusahaanInstance?.namaJenisPerusahaan}"/>
	</div>
</div>


