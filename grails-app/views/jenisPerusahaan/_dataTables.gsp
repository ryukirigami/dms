
<%@ page import="com.kombos.customerprofile.JenisPerusahaan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="jenisPerusahaan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="jenisPerusahaan.nomorPerusahaan.label" default="Nomor Perusahaan" /></div>
            </th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jenisPerusahaan.namaJenisPerusahaan.label" default="Nama Jenis Perusahaan" /></div>
			</th>
		</tr>
		<tr>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_nomorPerusahaan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_nomorPerusahaan" class="search_init" />
                </div>
            </th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_namaJenisPerusahaan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaJenisPerusahaan" class="search_init" />
				</div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var jenisPerusahaanTable;
var reloadJenisPerusahaanTable;
$(function(){
	
	reloadJenisPerusahaanTable = function() {
		jenisPerusahaanTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	jenisPerusahaanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	jenisPerusahaanTable = $('#jenisPerusahaan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "nomorPerusahaan",
	"mDataProp": "nomorPerusahaan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}
,
{
	"sName": "namaJenisPerusahaan",
	"mDataProp": "namaJenisPerusahaan",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        var nomorPerusahaan = $('#filter_nomorPerusahaan input').val();
						if(nomorPerusahaan){
							aoData.push(
									{"name": 'sCriteria_nomorPerusahaan', "value": nomorPerusahaan}
							);
						}
	
						var namaJenisPerusahaan = $('#filter_namaJenisPerusahaan input').val();
						if(namaJenisPerusahaan){
							aoData.push(
									{"name": 'sCriteria_namaJenisPerusahaan', "value": namaJenisPerusahaan}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
