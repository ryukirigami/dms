
<%@ page import="com.kombos.board.JPB" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="JPB_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%" style="table-layout: fixed; ">
    <col width="130px" />
    <col width="130px" />
    <col width="130px" />
    <col width="130px" />
    <col width="130px" />
    <col width="130px" />
    <col width="130px" />
    <col width="130px" />
    <col width="130px" />
    <col width="130px" />
	<thead>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="10">
                <div>TODAY APPOINTMENT BOARD (TAB)<br>BODY & PAINT</div>
            </th>
        </tr>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" >
                <div>07:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" >
                <div>08:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" >
                <div>09:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" >
                <div>10:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" >
                <div>11:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" >
                <div>12:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" >
                <div>13:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" >
                <div>14:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" >
                <div>15:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" >
                <div>16:00</div>
            </th>

        </tr>

	</thead>
</table>

<g:javascript>
var JPBTable;
var reloadJPBTable;
var getForemanInfo;
var getTeknisiInfo;

var getOnProgressInfo;
$(function(){

	 getForemanInfo = function(id){
    //  $(document).tooltip({ content: "Awesome title!" }).show();

        var content = "nocontent";
            $.ajax({type:'POST', url:'${request.contextPath}/groupManPower/getManPowerInfo/'+id,
                success:function(data,textStatus){
                    if(data){

                       content = data.namaLengkap + " - " + data.jabatan;
                       content = content +"</br>Group : "+data.group;
                       content = content +"</br>Anggota Group : </br>"

                       if(data.anggota){
                            jQuery.each(data.anggota, function (index, value) {
                                content = content + "</br>"+value;
                            });
                       }


                     $("#foreman"+id).tooltip({
                        html : true,
                        title : content,
                        position: 'center right'
                    });

                    }

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){

                }
            });

    }

    getOnProgressInfo = function(idJPB, tag, info){

         var content = "nocontent";

 //        console.log("info : "+info);

        if(info != "BREAK"){
        $.ajax({type:'GET', url:'${request.contextPath}/operation/getOnProgressOperation/',
            data : {bPOrGR : "0", idJPB : idJPB},
   			success:function(data,textStatus){
   				if(data){

   				   content = "No. WO : "+data.noWO;
                   content = content +"</br>No. Polisi : "+data.noPol;
                   content = content +"</br></br>Nama Job : "+data.namaJob;
                   content = content +"</br></br>Level : "+data.level;

                  if(info === "Y"){

                   content = content + "</br><span style='color:red'>JOB STOPPED</span></br>";
                   content = content + "</br><span style='color:red'>Alasan : "+data.alasan+"</span>";

                   }

                  if(info === "W"){

                   content = content + "</br><span style='color:red'>Reschedule Delivery</span></br>";
                   content = content + "</br><span style='color:red'>Alasan : "+data.alasan+"</span>";

                   }

                   if(info === "X"){

                   content = content + "</br><span style='color:red'>Reschedule JPB</span></br>";
                   content = content + "</br><span style='color:red'>Alasan : "+data.alasan+"</span>";

                   }

                   content = content +"</br></br>Current Progress : "+data.currProgress;
                   content = content +"</br>Start : "+data.start;
                   content = content +"</br>Target Finish : "+data.targetFinish;
                   content = content +"</br>Delivery Time : "+data.deliveryTime;


//   				 console.log("tag : "+tag);
  // 				 console.log("idjpb : "+idJPB);

   				 $("#jam"+tag+idJPB).tooltip({
                    html : true,
                    title : content,
                    position: 'center right'
                });

   				}

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){

   			}
   		});

        }

    }



    getTeknisiInfo = function(idTeknisi, idJPB){
    //  $(document).tooltip({ content: "Awesome title!" }).show();

         var content = "nocontent";
        $.ajax({type:'GET', url:'${request.contextPath}/groupManPower/getTeknisiJPB/',
            data : {id : idTeknisi, idJPB : idJPB},
   			success:function(data,textStatus){
   				if(data){

   				   content = data.namaLengkap + " - " + data.jabatan;
                   content = content +"</br>Group : "+data.group;
                   content = content +"</br>Kepala Group : "+data.kepalaGroup;
                   content = content +"</br></br>";
                   content = content +"</br>Previous Job : "+data.prevJob;
                   content = content +"</br>No. WO : "+data.prevNoWO;
                   content = content +"</br>"+data.prevTime;

                   content = content +"</br></br>Current Job : "+data.currJob;
                   content = content +"</br>No. WO : "+data.currNoWO;
                   content = content +"</br>"+data.currTime;

                   content = content +"</br></br>Next Job : "+data.nextJob;
                   content = content +"</br>No. WO : "+data.nextNoWO;
                   content = content +"</br>"+data.nextTime;




   				 $("#teknisi"+idTeknisi).tooltip({
                    html : true,
                    title : content,
                    position: 'center right'
                });

   				}

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){

   			}
   		});

    }


	reloadJPBTable = function() {
		JPBTable.fnDraw();
	}


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	JPBTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	JPBTable = $('#JPB_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        		return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesListBp")}",
		"aoColumns": [

{
	"sName": "jam7",
	"mDataProp": "jam7",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam7'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',7,\''+row['jam7']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam8",
	"mDataProp": "jam8",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam8'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',8,\''+row['jam8']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam9",
	"mDataProp": "jam9",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam9'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',9,\''+row['jam9']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam10",
	"mDataProp": "jam10",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam10'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',10,\''+row['jam10']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam11",
	"mDataProp": "jam11",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam11'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',11,\''+row['jam11']+'\')" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
    "bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam12",
	"mDataProp": "jam12",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam12'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',7,\''+row['jam12']+'\')" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
    "bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam13",
	"mDataProp": "jam13",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam13'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',13,\''+row['jam13']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam14",
	"mDataProp": "jam14",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam14'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',14,\''+row['jam14']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam15",
	"mDataProp": "jam15",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam15'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',15,\''+row['jam15']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam16",
	"mDataProp": "jam16",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam16'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',16,\''+row['jam16']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

});


</g:javascript>


			
