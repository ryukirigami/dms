
<%@ page import="com.kombos.board.JPB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="JPB" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			$(function(){ 

});
</g:javascript>
	</head>
	<body>

	<div class="box">
        <table class="display table table-bordered table-hover dataTable">
            <thead>
            <tr>
                <th colspan="2">
                    Informasi Body Paint
                </th>
            </tr>
            <tr>
                <th>Show</th>
                <th>No Show</th>
            </tr>

            </thead>
            <tbody>
            <tr>
                <td>${customerShowBP}</td>
                <td>${customerNoShowBP}</td>
            </tr>
            </tbody>
        </table>
        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
            <tr>
                <td>
                <div id="JPB-table">
                    <g:if test="${flash.message}">
                        <div class="message" role="status">
                            ${flash.message}
                        </div>
                    </g:if>

                    <g:render template="dataTablesBp" />
                </div>
                </td>
            </tr>
        </table>
	</div>
</body>
</html>
