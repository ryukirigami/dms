
<%@ page import="com.kombos.board.JPB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="Today Appointment Board" />
		<r:require modules="baseapplayout, baseapplist, bootstraptooltip" />
        <link rel="stylesheet" type="text/css" href="css/baseapp.css" />
		<g:javascript>
			var show;
			var edit;
			$(function(){
			    $('#board_bp').load('${request.contextPath}/TodayAppointmentBoard/bp');

                $('#tab_board_bp').click(function() {
                    $('#board_bp').load('${request.contextPath}/TodayAppointmentBoard/bp');
                });

                $('#tab_board_gr').click(function() {
                    $('#board_gr').load('${request.contextPath}/TodayAppointmentBoard/gr');
                });

            });


</g:javascript>
	</head>
	<body>
    <div class="navbar box-header no-border">
   		<span class="pull-left">Today Appointment Board</span>
   	</div>

	<div class="box">
        <div class="tabbable control-group">
            <ul class="nav nav-tabs">
                <li id="tab_board_bp" class="active">
                    <a href="#board_bp" data-toggle="tab">Today Appointment Board BP</a>
                </li>
                <li id="tab_board_gr" class="">
                    <a href="#board_gr" data-toggle="tab">Today Appointment Board GR</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="board_bp" />

                <div class="tab-pane" id="board_gr" />

            </div>
        </div>
	</div>
</body>
</html>
