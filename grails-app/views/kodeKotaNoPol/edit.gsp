<%@ page import="com.kombos.administrasi.KodeKotaNoPol" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'kodeKotaNoPol.label', default: 'Kode Kota NoPol')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>

    </head>
	<body>
		<div id="edit-kodeKotaNoPol" class="content scaffold-edit" role="main">
			<legend><g:message code="default.edit.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${kodeKotaNoPolInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${kodeKotaNoPolInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form method="post" >--}%
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="reloadKodeKotaNoPolTable();" update="kodeKotaNoPol-form"
				url="[controller: 'kodeKotaNoPol', action:'update']">
				<g:hiddenField name="id" value="${kodeKotaNoPolInstance?.id}" />
				<g:hiddenField name="version" value="${kodeKotaNoPolInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
                <fieldset class="buttons controls">
                    <g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                </fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
