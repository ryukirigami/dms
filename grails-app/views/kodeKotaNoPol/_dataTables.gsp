
<%@ page import="com.kombos.administrasi.KodeKotaNoPol" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="kodeKotaNoPol_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kodeKotaNoPol.m116ID.label" default="Kode Kota" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kodeKotaNoPol.m116NamaKota.label" default="Nama Kota" /></div>
			</th>

 
		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m116ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m116ID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m116NamaKota" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m116NamaKota" class="search_init" />
				</div>
			</th>
	 
		</tr>
	</thead>
</table>

<g:javascript>
var KodeKotaNoPolTable;
var reloadKodeKotaNoPolTable;
$(function(){
	
	reloadKodeKotaNoPolTable = function() {
		KodeKotaNoPolTable.fnDraw();
	}

	var recordsKodeKotaNoPolperpage = [];//new Array();
    var anKodeKotaNoPolSelected;
    var jmlRecKodeKotaNoPolPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	KodeKotaNoPolTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	KodeKotaNoPolTable = $('#kodeKotaNoPol_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsKodeKotaNoPol = $("#kodeKotaNoPol_datatables tbody .row-select");
            var jmlKodeKotaNoPolCek = 0;
            var nRow;
            var idRec;
            rsKodeKotaNoPol.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsKodeKotaNoPolperpage[idRec]=="1"){
                    jmlKodeKotaNoPolCek = jmlKodeKotaNoPolCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsKodeKotaNoPolperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecKodeKotaNoPolPerPage = rsKodeKotaNoPol.length;
            if(jmlKodeKotaNoPolCek==jmlRecKodeKotaNoPolPerPage && jmlRecKodeKotaNoPolPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m116ID",
	"mDataProp": "m116ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m116NamaKota",
	"mDataProp": "m116NamaKota",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

 
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m116ID = $('#filter_m116ID input').val();
						if(m116ID){
							aoData.push(
									{"name": 'sCriteria_m116ID', "value": m116ID}
							);
						}
	
						var m116NamaKota = $('#filter_m116NamaKota input').val();
						if(m116NamaKota){
							aoData.push(
									{"name": 'sCriteria_m116NamaKota', "value": m116NamaKota}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#kodeKotaNoPol_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsKodeKotaNoPolperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsKodeKotaNoPolperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#kodeKotaNoPol_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsKodeKotaNoPolperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anKodeKotaNoPolSelected = KodeKotaNoPolTable.$('tr.row_selected');
            if(jmlRecKodeKotaNoPolPerPage == anKodeKotaNoPolSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsKodeKotaNoPolperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
