

<%@ page import="com.kombos.administrasi.KodeKotaNoPol" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'kodeKotaNoPol.label', default: 'Kode Kota NoPol')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteKodeKotaNoPol;

$(function(){ 
	deleteKodeKotaNoPol=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/kodeKotaNoPol/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadKodeKotaNoPolTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-kodeKotaNoPol" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="kodeKotaNoPol"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${kodeKotaNoPolInstance?.m116ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m116ID-label" class="property-label"><g:message
					code="kodeKotaNoPol.m116ID.label" default="Kode Kota" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m116ID-label">
						%{--<ba:editableValue
								bean="${kodeKotaNoPolInstance}" field="m116ID"
								url="${request.contextPath}/KodeKotaNoPol/updatefield" type="text"
								title="Enter m116ID" onsuccess="reloadKodeKotaNoPolTable();" />--}%
							
								<g:fieldValue bean="${kodeKotaNoPolInstance}" field="m116ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kodeKotaNoPolInstance?.m116NamaKota}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m116NamaKota-label" class="property-label"><g:message
					code="kodeKotaNoPol.m116NamaKota.label" default="Nama Kota" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m116NamaKota-label">
						%{--<ba:editableValue
								bean="${kodeKotaNoPolInstance}" field="m116NamaKota"
								url="${request.contextPath}/KodeKotaNoPol/updatefield" type="text"
								title="Enter m116NamaKota" onsuccess="reloadKodeKotaNoPolTable();" />--}%
							
								<g:fieldValue bean="${kodeKotaNoPolInstance}" field="m116NamaKota"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
					<g:if test="${kodeKotaNoPolInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${kodeKotaNoPolInstance}" field="dateCreated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${kodeKotaNoPolInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kodeKotaNoPolInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="jenisStall.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${kodeKotaNoPolInstance}" field="createdBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${kodeKotaNoPolInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kodeKotaNoPolInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jenisStall.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${kodeKotaNoPolInstance}" field="lastUpdated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${kodeKotaNoPolInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kodeKotaNoPolInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="jenisStall.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${kodeKotaNoPolInstance}" field="updatedBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${kodeKotaNoPolInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kodeKotaNoPolInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${kodeKotaNoPolInstance}" field="lastUpdProcess"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${kodeKotaNoPolInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${kodeKotaNoPolInstance?.id}"
					update="[success:'kodeKotaNoPol-form',failure:'kodeKotaNoPol-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteKodeKotaNoPol('${kodeKotaNoPolInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
