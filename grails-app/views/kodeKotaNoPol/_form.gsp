<%@ page import="com.kombos.administrasi.KabKota; com.kombos.administrasi.Provinsi; com.kombos.administrasi.KodeKotaNoPol" %>


<div class="control-group fieldcontain ${hasErrors(bean: kodeKotaNoPolInstance, field: 'm116ID', 'error')} ">
	<label class="control-label" for="m116ID">
		<g:message code="kodeKotaNoPol.m116ID.label" default="Kode Kota" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m116ID" id="m116ID" maxlength="2" required="" value="${kodeKotaNoPolInstance?.m116ID}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kodeKotaNoPolInstance, field: 'm116NamaKota', 'error')} required">
	<label class="control-label" for="m116NamaKota">
		<g:message code="kodeKotaNoPol.m116NamaKota.label" default="Nama Kota" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	    <g:textField name="m116NamaKota" maxlength="50" required="" value="${kodeKotaNoPolInstance?.m116NamaKota}"/>
        %{--<g:select id="m116NamaKota" name="m116NamaKota" from="${KabKota.createCriteria().list {eq("staDel", "0");order("m002NamaKabKota","asc")}}" optionKey="id" required="" value="${kota}" optionValue="${{it.m002NamaKabKota}}" />--}%
    </div>
</div>

<g:javascript>
    document.getElementById("m116ID").focus();
    $(document).ready(function() {
        $("#m116ID").keyup(function(e) {
            var isi = $(e.target).val();
            $(e.target).val(isi.toUpperCase());
        });
    });


</g:javascript>