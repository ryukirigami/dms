
<%@ page import="com.kombos.maintable.HistoryApproval" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="historyApproval_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nama Kegiatan</div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div>Tanggal Permohonan</div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div>Nomor &lt;nama dokumen&gt;</div>
			</th>
			
			<th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyApproval.namaApproved.label" default="Approver" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyApproval.status.label" default="Status" /></div>
            </th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyApproval.tglJamApproved.label" default="Tanggal Approval" /></div>
			</th>

		
		</tr>
		<tr>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kegiatan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 250px;">
					<input type="text" name="search_kegiatan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
					<div id="filter_tglPermohonan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 140px;" >
					<input type="hidden" name="search_tglPermohonan" value="date.struct">
					<input type="hidden" name="search_tglPermohonan_day" id="search_tglPermohonan_day" value="">
					<input type="hidden" name="search_tglPermohonan_month" id="search_tglPermohonan_month" value="">
					<input type="hidden" name="search_tglPermohonan_year" id="search_tglPermohonan_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_tglPermohonan_dp" value="" id="search_tglPermohonan" class="search_init">
				</div>
			</th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_noDokumen" style="padding-top: 0px;position:relative; margin-top: 0px;width: 180px;">
					<input type="text" name="search_noDokumen" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_status" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_status" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_namaApproved" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaApproved" class="search_init" />
				</div>
			</th>
	
			
			<th style="border-top: none;padding: 5px;">
				<div id="filter_tglJamApproved" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_tglJamApproved" value="date.struct">
					<input type="hidden" name="search_tglJamApproved_day" id="search_tglJamApproved_day" value="">
					<input type="hidden" name="search_tglJamApproved_month" id="search_tglJamApproved_month" value="">
					<input type="hidden" name="search_tglJamApproved_year" id="search_tglJamApproved_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_tglJamApproved_dp" value="" id="search_tglJamApproved" class="search_init">
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var historyApprovalTable;
var reloadHistoryApprovalTable;
$(function(){
	
	reloadHistoryApprovalTable = function() {
		historyApprovalTable.fnDraw();
	}

	
	$('#search_tglJamApproved').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglJamApproved_day').val(newDate.getDate());
			$('#search_tglJamApproved_month').val(newDate.getMonth()+1);
			$('#search_tglJamApproved_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			historyApprovalTable.fnDraw();	
	});

	$('#search_tglPermohonan').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglPermohonan_day').val(newDate.getDate());
			$('#search_tglPermohonan_month').val(newDate.getMonth()+1);
			$('#search_tglPermohonan_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			historyApprovalTable.fnDraw();	
	});
	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	historyApprovalTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	historyApprovalTable = $('#historyApproval_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "kegiatan",
	"mDataProp": "kegiatan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tglPermohonan",
	"mDataProp": "tglPermohonan",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"140px",
	"bVisible": true
}
,

{
	"sName": "noDokumen",
	"mDataProp": "noDokumen",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "namaApproved",
	"mDataProp": "namaApproved",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"120px",
	"bVisible": true
}

,

{
	"sName": "status",
	"mDataProp": "status",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "tglJamApproved",
	"mDataProp": "tglJamApproved",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var kegiatan = $('#filter_kegiatan input').val();
						if(kegiatan){
							aoData.push(
									{"name": 'sCriteria_kegiatan', "value": kegiatan}
							);
						}
	
						var noDokumen = $('#filter_noDokumen input').val();
						if(noDokumen){
							aoData.push(
									{"name": 'sCriteria_noDokumen', "value": noDokumen}
							);
						}
	
						var namaApproved = $('#filter_namaApproved input').val();
						if(namaApproved){
							aoData.push(
									{"name": 'sCriteria_namaApproved', "value": namaApproved}
							);
						}
	
						var status = $('#filter_status input').val();
						if(status){
							aoData.push(
									{"name": 'sCriteria_status', "value": status}
							);
						}

						var tglJamApproved = $('#search_tglJamApproved').val();
						var tglJamApprovedDay = $('#search_tglJamApproved_day').val();
						var tglJamApprovedMonth = $('#search_tglJamApproved_month').val();
						var tglJamApprovedYear = $('#search_tglJamApproved_year').val();
						
						if(tglJamApproved){
							aoData.push(
									{"name": 'sCriteria_tglJamApproved', "value": "date.struct"},
									{"name": 'sCriteria_tglJamApproved_dp', "value": tglJamApproved},
									{"name": 'sCriteria_tglJamApproved_day', "value": tglJamApprovedDay},
									{"name": 'sCriteria_tglJamApproved_month', "value": tglJamApprovedMonth},
									{"name": 'sCriteria_tglJamApproved_year', "value": tglJamApprovedYear}
							);
						}
						
						var tglPermohonan = $('#search_tglPermohonan').val();
						var tglPermohonanDay = $('#search_tglPermohonan_day').val();
						var tglPermohonanMonth = $('#search_tglPermohonan_month').val();
						var tglPermohonanYear = $('#search_tglPermohonan_year').val();
						
						if(tglPermohonan){
							aoData.push(
									{"name": 'sCriteria_tglPermohonan', "value": "date.struct"},
									{"name": 'sCriteria_tglPermohonan_dp', "value": tglPermohonan},
									{"name": 'sCriteria_tglPermohonan_day', "value": tglPermohonanDay},
									{"name": 'sCriteria_tglPermohonan_month', "value": tglPermohonanMonth},
									{"name": 'sCriteria_tglPermohonan_year', "value": tglPermohonanYear}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
