<%@ page import="com.kombos.maintable.HistoryApproval" %>



<div class="control-group fieldcontain ${hasErrors(bean: historyApprovalInstance, field: 'approval', 'error')} ">
	<label class="control-label" for="approval">
		<g:message code="historyApproval.approval.label" default="Approval" />
		
	</label>
	<div class="controls">
	<g:select id="approval" name="approval.id" from="${com.kombos.maintable.ApprovalT770.list()}" optionKey="id" required="" value="${historyApprovalInstance?.approval?.id}" class="many-to-one"/>
	</div>
</div>
<g:javascript>
    document.getElementById("approval").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: historyApprovalInstance, field: 'keterangan', 'error')} ">
	<label class="control-label" for="keterangan">
		<g:message code="historyApproval.keterangan.label" default="Keterangan" />
		
	</label>
	<div class="controls">
	<g:textField name="keterangan" value="${historyApprovalInstance?.keterangan}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: historyApprovalInstance, field: 'level', 'error')} ">
	<label class="control-label" for="level">
		<g:message code="historyApproval.level.label" default="Level" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="level" value="${historyApprovalInstance.level}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: historyApprovalInstance, field: 'namaApproved', 'error')} ">
	<label class="control-label" for="namaApproved">
		<g:message code="historyApproval.namaApproved.label" default="Nama Approved" />
		
	</label>
	<div class="controls">
	<g:textField name="namaApproved" value="${historyApprovalInstance?.namaApproved}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: historyApprovalInstance, field: 'status', 'error')} ">
	<label class="control-label" for="status">
		<g:message code="historyApproval.status.label" default="Status" />
		
	</label>
	<div class="controls">
	<g:select name="status" from="${com.kombos.parts.StatusApproval?.values()}" keys="${com.kombos.parts.StatusApproval.values()*.name()}" required="" value="${historyApprovalInstance?.status?.name()}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: historyApprovalInstance, field: 'tglJamApproved', 'error')} ">
	<label class="control-label" for="tglJamApproved">
		<g:message code="historyApproval.tglJamApproved.label" default="Tgl Jam Approved" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="tglJamApproved" precision="day" value="${historyApprovalInstance?.tglJamApproved}" format="yyyy-MM-dd"/>
	</div>
</div>

