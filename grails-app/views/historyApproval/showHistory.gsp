

<%@ page import="com.kombos.maintable.HistoryApproval" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'historyApproval.label', default: 'HistoryApproval')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteHistoryApproval;

$(function(){ 
	deleteHistoryApproval=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/historyApproval/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadHistoryApprovalTable();
   				expandTableLayout('historyApproval');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-historyApproval" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="historyApproval"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${historyApprovalInstance?.approval}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="approval-label" class="property-label"><g:message
					code="historyApproval.approval.label" default="Approval" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="approval-label">
						%{--<ba:editableValue
								bean="${historyApprovalInstance}" field="approval"
								url="${request.contextPath}/HistoryApproval/updatefield" type="text"
								title="Enter approval" onsuccess="reloadHistoryApprovalTable();" />--}%
							
								<g:link controller="approvalT770" action="show" id="${historyApprovalInstance?.approval?.id}">${historyApprovalInstance?.approval?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyApprovalInstance?.keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="keterangan-label" class="property-label"><g:message
					code="historyApproval.keterangan.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="keterangan-label">
						%{--<ba:editableValue
								bean="${historyApprovalInstance}" field="keterangan"
								url="${request.contextPath}/HistoryApproval/updatefield" type="text"
								title="Enter keterangan" onsuccess="reloadHistoryApprovalTable();" />--}%
							
								<g:fieldValue bean="${historyApprovalInstance}" field="keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyApprovalInstance?.level}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="level-label" class="property-label"><g:message
					code="historyApproval.level.label" default="Level" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="level-label">
						%{--<ba:editableValue
								bean="${historyApprovalInstance}" field="level"
								url="${request.contextPath}/HistoryApproval/updatefield" type="text"
								title="Enter level" onsuccess="reloadHistoryApprovalTable();" />--}%
							
								<g:fieldValue bean="${historyApprovalInstance}" field="level"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyApprovalInstance?.namaApproved}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaApproved-label" class="property-label"><g:message
					code="historyApproval.namaApproved.label" default="Nama Approved" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaApproved-label">
						%{--<ba:editableValue
								bean="${historyApprovalInstance}" field="namaApproved"
								url="${request.contextPath}/HistoryApproval/updatefield" type="text"
								title="Enter namaApproved" onsuccess="reloadHistoryApprovalTable();" />--}%
							
								<g:fieldValue bean="${historyApprovalInstance}" field="namaApproved"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyApprovalInstance?.status}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="status-label" class="property-label"><g:message
					code="historyApproval.status.label" default="Status" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="status-label">
						%{--<ba:editableValue
								bean="${historyApprovalInstance}" field="status"
								url="${request.contextPath}/HistoryApproval/updatefield" type="text"
								title="Enter status" onsuccess="reloadHistoryApprovalTable();" />--}%
							
								<g:fieldValue bean="${historyApprovalInstance}" field="status"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyApprovalInstance?.tglJamApproved}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tglJamApproved-label" class="property-label"><g:message
					code="historyApproval.tglJamApproved.label" default="Tgl Jam Approved" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tglJamApproved-label">
						%{--<ba:editableValue
								bean="${historyApprovalInstance}" field="tglJamApproved"
								url="${request.contextPath}/HistoryApproval/updatefield" type="text"
								title="Enter tglJamApproved" onsuccess="reloadHistoryApprovalTable();" />--}%
							
								<g:formatDate date="${historyApprovalInstance?.tglJamApproved}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('historyApproval');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${historyApprovalInstance?.id}"
					update="[success:'historyApproval-form',failure:'historyApproval-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteHistoryApproval('${historyApprovalInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
