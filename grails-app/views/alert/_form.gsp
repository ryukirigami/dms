<%@ page import="com.kombos.maintable.Alert" %>

<div class="control-group fieldcontain ${hasErrors(bean: alertInstance, field: 'jenisAlert', 'error')} ">
	<label class="control-label" for="jenisAlert">
		<g:message code="alert.jenisAlert.label" default="Nama Alert" />
		
	</label>
	<div class="controls">
	${alertInstance?.jenisAlert?.m901NamaAlert}
	</div>
</div>
%{--<g:javascript>
    document.getElementById("jenisAlert").focus();
</g:javascript>--}%
<div class="control-group fieldcontain ${hasErrors(bean: alertInstance, field: 'noDokumen', 'error')} ">
	<label class="control-label" for="noDokumen">
		<g:message code="alert.noDokumen.label" default="No Dokumen" />
		
	</label>
	<div class="controls">
	${alertInstance?.noDokumen}
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: alertInstance, field: 'ket1', 'error')} ">
	<label class="control-label" for="ket1">
		Perlu tindak lanjut?
	</label>
	<div class="controls">
	${alertInstance?.jenisAlert?.m901StaPerluTindakLanjut}
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: alertInstance, field: 'tglJamAlert', 'error')} ">
	<label class="control-label" for="tglJamAlert">
		<g:message code="alert.tglJamAlert.label" default="Tgl Jam Alert" />
		
	</label>
	<div class="controls">
	${alertInstance?.tglJamAlert}
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: alertInstance, field: 'tipeAlert', 'error')} ">
	<label class="control-label" for="tipeAlert">
		<g:message code="alert.tipeAlert.label" default="Tipe Alert" />
		
	</label>
	<div class="controls">
	${alertInstance?.tipeAlert?.m902JenisAlert}
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: alertInstance, field: 'ket1', 'error')} ">
	<label class="control-label" for="ket1">
		<g:message code="alert.ket1.label" default="Pesan" />
		
	</label>
	<div class="controls">
	<g:textField name="ket1" value="${alertInstance?.ket1}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: alertInstance, field: 'ket1', 'error')} ">
	<label class="control-label" for="statusAksi">Status Aksi</label>
	<div class="controls">
		<g:radioGroup name="statusAksi" values="['BACA','TINDAK LANJUT']" value="${nextStatus}" labels="['BACA','TINDAK LANJUT']">
								${it.radio} <g:message code="${it.label}" />
		</g:radioGroup>	
	</div>
</div>
