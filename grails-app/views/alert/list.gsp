
<%@ page import="com.kombos.maintable.Alert" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'alert.label', default: 'Alert')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;  
	        var shrinkTableLayout;      
	        var expandTableLayout;
			$(function(){ 
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('alert');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('alert', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('alert', '${request.contextPath}/alert/massdelete', reloadAlertTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('alert','${request.contextPath}/alert/show/'+id);
				};
				
				%{--edit = function(id) {
					editInstance('alert','${request.contextPath}/alert/edit/'+id);
				};--}%

    edit = function(id) {
        console.log("masuk edit=" + edit);
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/alert/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   				$('#spinner').fadeOut();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    loadForm = function(data, textStatus){
		$('#alert-form').empty();
    	$('#alert-form').append(data);
   	}

    shrinkTableLayout = function(){
    	if($("#alert-table").hasClass("span12")){
   			$("#alert-table").toggleClass("span12 span5");
        }
        $("#alert-form").css("display","block");
   	}

   	expandTableLayout = function(){
   		if($("#alert-table").hasClass("span5")){
   			$("#alert-table").toggleClass("span5 span12");
   		}
        $("#alert-form").css("display","none");
   	}
                   
});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="alert-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="alert-form" style="display: none;"></div>
	</div>
</body>
</html>
