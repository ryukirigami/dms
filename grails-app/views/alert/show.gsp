

<%@ page import="com.kombos.maintable.Alert" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'alert.label', default: 'Alert')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteAlert;

$(function(){ 
	deleteAlert=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/alert/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadAlertTable();
   				expandTableLayout('alert');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-alert" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="alert"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${alertInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="alert.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="companyDealer"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadAlertTable();" />--}%
							
								${alertInstance?.companyDealer?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>

				<g:if test="${alertInstance?.jenisAlert}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jenisAlert-label" class="property-label"><g:message
					code="alert.jenisAlert.label" default="Jenis Alert" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jenisAlert-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="jenisAlert"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter jenisAlert" onsuccess="reloadAlertTable();" />--}%
							
								${alertInstance?.jenisAlert?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${alertInstance?.ket1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="ket1-label" class="property-label"><g:message
					code="alert.ket1.label" default="Ket1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="ket1-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="ket1"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter ket1" onsuccess="reloadAlertTable();" />--}%
							
								<g:fieldValue bean="${alertInstance}" field="ket1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${alertInstance?.ket2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="ket2-label" class="property-label"><g:message
					code="alert.ket2.label" default="Ket2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="ket2-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="ket2"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter ket2" onsuccess="reloadAlertTable();" />--}%
							
								<g:fieldValue bean="${alertInstance}" field="ket2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${alertInstance?.namaFK}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaFK-label" class="property-label"><g:message
					code="alert.namaFK.label" default="Nama FK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaFK-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="namaFK"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter namaFK" onsuccess="reloadAlertTable();" />--}%
							
								<g:fieldValue bean="${alertInstance}" field="namaFK"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${alertInstance?.namaTabel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaTabel-label" class="property-label"><g:message
					code="alert.namaTabel.label" default="Nama Tabel" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaTabel-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="namaTabel"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter namaTabel" onsuccess="reloadAlertTable();" />--}%
							
								<g:fieldValue bean="${alertInstance}" field="namaTabel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${alertInstance?.noDokumen}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="noDokumen-label" class="property-label"><g:message
					code="alert.noDokumen.label" default="No Dokumen" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="noDokumen-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="noDokumen"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter noDokumen" onsuccess="reloadAlertTable();" />--}%
							
								<g:fieldValue bean="${alertInstance}" field="noDokumen"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${alertInstance?.staBaca}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staBaca-label" class="property-label"><g:message
					code="alert.staBaca.label" default="Status Baca" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staBaca-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="staBaca"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter staBaca" onsuccess="reloadAlertTable();" />--}%
							
								<g:fieldValue bean="${alertInstance}" field="staBaca"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${alertInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="alert.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">

								<g:fieldValue bean="${alertInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>--}%
			
				<g:if test="${alertInstance?.staTindakLanjut}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staTindakLanjut-label" class="property-label"><g:message
					code="alert.staTindakLanjut.label" default="Status Tindak Lanjut" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staTindakLanjut-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="staTindakLanjut"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter staTindakLanjut" onsuccess="reloadAlertTable();" />--}%
							
								<g:fieldValue bean="${alertInstance}" field="staTindakLanjut"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${alertInstance?.tanggal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tanggal-label" class="property-label"><g:message
					code="alert.tanggal.label" default="Tanggal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tanggal-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="tanggal"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter tanggal" onsuccess="reloadAlertTable();" />--}%
							
								<g:formatDate date="${alertInstance?.tanggal}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${alertInstance?.tglJamAlert}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tglJamAlert-label" class="property-label"><g:message
					code="alert.tglJamAlert.label" default="Tgl Jam Alert" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tglJamAlert-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="tglJamAlert"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter tglJamAlert" onsuccess="reloadAlertTable();" />--}%
							
								<g:formatDate date="${alertInstance?.tglJamAlert}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${alertInstance?.tglJamBaca}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tglJamBaca-label" class="property-label"><g:message
					code="alert.tglJamBaca.label" default="Tgl Jam Baca" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tglJamBaca-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="tglJamBaca"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter tglJamBaca" onsuccess="reloadAlertTable();" />--}%
							
								<g:formatDate date="${alertInstance?.tglJamBaca}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${alertInstance?.tglJamTinjakLanjut}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tglJamTinjakLanjut-label" class="property-label"><g:message
					code="alert.tglJamTinjakLanjut.label" default="Tgl Jam Tinjak Lanjut" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tglJamTinjakLanjut-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="tglJamTinjakLanjut"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter tglJamTinjakLanjut" onsuccess="reloadAlertTable();" />--}%
							
								<g:fieldValue bean="${alertInstance}" field="tglJamTinjakLanjut"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${alertInstance?.tipeAlert}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tipeAlert-label" class="property-label"><g:message
					code="alert.tipeAlert.label" default="Tipe Alert" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tipeAlert-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="tipeAlert"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter tipeAlert" onsuccess="reloadAlertTable();" />--}%
							
								${alertInstance?.tipeAlert?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${alertInstance?.xNamaUserBaca}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="xNamaUserBaca-label" class="property-label"><g:message
					code="alert.xNamaUserBaca.label" default="Nama User Baca" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="xNamaUserBaca-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="xNamaUserBaca"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter xNamaUserBaca" onsuccess="reloadAlertTable();" />--}%
							
								<g:fieldValue bean="${alertInstance}" field="xNamaUserBaca"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${alertInstance?.xNamaUserTindakLanjut}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="xNamaUserTindakLanjut-label" class="property-label"><g:message
					code="alert.xNamaUserTindakLanjut.label" default="Nama User Tindak Lanjut" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="xNamaUserTindakLanjut-label">
						%{--<ba:editableValue
								bean="${alertInstance}" field="xNamaUserTindakLanjut"
								url="${request.contextPath}/Alert/updatefield" type="text"
								title="Enter xNamaUserTindakLanjut" onsuccess="reloadAlertTable();" />--}%
							
								<g:fieldValue bean="${alertInstance}" field="xNamaUserTindakLanjut"/>
							
						</span></td>
					
				</tr>
				</g:if>

                <g:if test="${alertInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="alert.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${alertInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Alert/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadAlertTable();" />--}%

                        <g:fieldValue bean="${alertInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
                </g:if>

                <g:if test="${alertInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="alert.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${alertInstance}" field="dateCreated"
                                url="${request.contextPath}/Alert/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadAlertTable();" />--}%

                        <g:formatDate date="${alertInstance?.dateCreated}" type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
                </g:if>

                <g:if test="${alertInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="alert.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${alertInstance}" field="createdBy"
                                url="${request.contextPath}/Alert/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadAlertTable();" />--}%

                        <g:fieldValue bean="${alertInstance}" field="createdBy"/>

                    </span></td>

                </tr>
                </g:if>

                <g:if test="${alertInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="alert.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${alertInstance}" field="lastUpdated"
                                url="${request.contextPath}/Alert/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadAlertTable();" />--}%

                        <g:formatDate date="${alertInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
                </g:if>

                <g:if test="${alertInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="alert.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${alertInstance}" field="updatedBy"
                                url="${request.contextPath}/Alert/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadAlertTable();" />--}%

                        <g:fieldValue bean="${alertInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
                </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('alert');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${alertInstance?.id}"
					update="[success:'alert-form',failure:'alert-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteAlert('${alertInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
