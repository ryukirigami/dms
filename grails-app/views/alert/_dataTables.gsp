
<%@ page import="com.kombos.maintable.Alert" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="alert_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="alert.jenisAlert.label" default="Nama Alert" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="alert.tujuan.label" default="Tujuan Alert" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="alert.namaFK.label" default="Nomor WO" /></div>
            </th>
			
			<th style="border-bottom: none;padding: 5px;">
                <div><g:message code="alert.nomorDokumen.label" default="Nomor Terkait" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="alert.tglJamAlert.label" default="Tanggal/Jam Alert" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="alert.statusAksi.label" default="Status Aksi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="alert.tglJamAksi.label" default="Tanggal/Jam Aksi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="alert.xNamaUserAksi.label" default="User Aksi" /></div>
			</th>
		
		</tr>
		<tr>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_jenisAlert" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_jenisAlert" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_xNamaUserBaca" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_xNamaUserBaca" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_namaFK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaFK" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_noDokumen" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_noDokumen" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_tglJamAlert" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_tglJamAlert" value="date.struct">
					<input type="hidden" name="search_tglJamAlert_day" id="search_tglJamAlert_day" value="">
					<input type="hidden" name="search_tglJamAlert_month" id="search_tglJamAlert_month" value="">
					<input type="hidden" name="search_tglJamAlert_year" id="search_tglJamAlert_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_tglJamAlert_dp" value="" id="search_tglJamAlert" class="search_init">
				</div>
			</th>
			
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staBaca" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_statusAksi" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_tanggalAksi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_tanggalAksi" value="date.struct">
					<input type="hidden" name="search_tanggalAksi_day" id="search_tanggalAksi_day" value="">
					<input type="hidden" name="search_tanggalAksi_month" id="search_tanggalAksi_month" value="">
					<input type="hidden" name="search_tanggalAksi_year" id="search_tanggalAksi_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_tanggalAksi_dp" value="" id="search_tanggalAksi" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_userAksi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_userAksi" class="search_init" />
				</div>
			</th>
			</tr>
	</thead>
</table>

<g:javascript>
var alertTable;
var reloadAlertTable;
$(function(){
	
	reloadAlertTable = function() {
		alertTable.fnDraw();
	}

	
	$('#search_tanggalAksi').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalAksi_day').val(newDate.getDate());
			$('#search_tanggalAksi_month').val(newDate.getMonth()+1);
			$('#search_tanggalAksi_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			alertTable.fnDraw();	
	});

	

	$('#search_tglJamAlert').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglJamAlert_day').val(newDate.getDate());
			$('#search_tglJamAlert_month').val(newDate.getMonth()+1);
			$('#search_tglJamAlert_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			alertTable.fnDraw();	
	});

	$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	alertTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	alertTable = $('#alert_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "jenisAlert",
	"mDataProp": "jenisAlert",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		/*return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="edit('+row['id']+');">'+data+'</a>';*/
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
//		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}
,

{
	"sName": "xNamaUserBaca",
	"mDataProp": "xNamaUserBaca",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "namaFK",
	"mDataProp": "namaFK",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "noDokumen",
	"mDataProp": "noDokumen",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "tglJamAlert",
	"mDataProp": "tglJamAlert",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "statusAksi",
	"mDataProp": "statusAksi",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "tglAksi",
	"mDataProp": "tglAksi",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "userAksi",
	"mDataProp": "userAksi",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var jenisAlert = $('#filter_jenisAlert input').val();
						if(jenisAlert){
							aoData.push(
									{"name": 'sCriteria_jenisAlert', "value": jenisAlert}
							);
						}

						var xNamaUserBaca = $('#filter_xNamaUserBaca input').val();
						if(xNamaUserBaca){
							aoData.push(
									{"name": 'sCriteria_xNamaUserBaca', "value": xNamaUserBaca}
							);
						}
	
						var namaFK = $('#filter_namaFK input').val();
						if(namaFK){
							aoData.push(
									{"name": 'sCriteria_namaFK', "value": namaFK}
							);
						}
	
						var noDokumen = $('#filter_noDokumen input').val();
						if(noDokumen){
							aoData.push(
									{"name": 'sCriteria_noDokumen', "value": namaFK}
							);
						}

						var statusAksi = $('#filter_statusAksi input').val();
						if(statusAksi){
							aoData.push(
									{"name": 'sCriteria_statusAksi', "value": statusAksi}
							);
						}
	
						
						var tglJamAlert = $('#search_tglJamAlert').val();
						var tglJamAlertDay = $('#search_tglJamAlert_day').val();
						var tglJamAlertMonth = $('#search_tglJamAlert_month').val();
						var tglJamAlertYear = $('#search_tglJamAlert_year').val();
						
						if(tglJamAlert){
							aoData.push(
									{"name": 'sCriteria_tglJamAlert', "value": "date.struct"},
									{"name": 'sCriteria_tglJamAlert_dp', "value": tglJamAlert},
									{"name": 'sCriteria_tglJamAlert_day', "value": tglJamAlertDay},
									{"name": 'sCriteria_tglJamAlert_month', "value": tglJamAlertMonth},
									{"name": 'sCriteria_tglJamAlert_year', "value": tglJamAlertYear}
							);
						}

						var tglJamAksi = $('#search_tglJamAksi').val();
						var tglJamAksiDay = $('#search_tglJamAksi_day').val();
						var tglJamAksiMonth = $('#search_tglJamAksi_month').val();
						var tglJamAksiYear = $('#search_tglJamAksi_year').val();
						
						if(tglJamAksi){
							aoData.push(
									{"name": 'sCriteria_tglJamAksi', "value": "date.struct"},
									{"name": 'sCriteria_tglJamAksi_dp', "value": tglJamAksi},
									{"name": 'sCriteria_tglJamAksi_day', "value": tglJamAksiDay},
									{"name": 'sCriteria_tglJamAksi_month', "value": tglJamAksiMonth},
									{"name": 'sCriteria_tglJamAksi_year', "value": tglJamAksiYear}
							);
						}
	
						var userAksi = $('#filter_userAksi input').val();
						if(userAksi){
							aoData.push(
									{"name": 'sCriteria_userAksi', "value": userAksi}
							);
						}
	
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>
