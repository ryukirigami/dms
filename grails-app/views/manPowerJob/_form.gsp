<%@ page import="com.kombos.administrasi.Operation; com.kombos.administrasi.NamaManPower; com.kombos.administrasi.ManPowerJob" %>

<g:javascript>
		$(function(){
			$('#namaManPower').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/manPowerStall/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
</g:javascript>

    <div class="control-group fieldcontain ${hasErrors(bean: manPowerJobInstance, field: 'namaManPower', 'error')} required">
        <label class="control-label" for="namaManPower">
            <g:message code="manPowerJob.namaManPower.label" default="Nama Man Power" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:textField name="namaManPower" id="namaManPower" class="typeahead" maxlength="200" value="${manPowerJobInstance?.namaManPower?.t015NamaLengkap}" autocomplete="off" required="true"/>
            %{--<g:select id="namaManPower" name="namaManPower.id" from="${NamaManPower.createCriteria().list{eq("staDel", "0");order("t015NamaLengkap","asc")}}" optionValue="${{it.t015NamaLengkap+" ("+it.t015NamaBoard+") " }}" optionKey="id" required="" value="${manPowerJobInstance?.namaManPower?.id}" class="many-to-one"/>--}%
        </div>
    </div>
<g:javascript>
    document.getElementById("namaManPower").focus();
</g:javascript>
    <div class="control-group fieldcontain ${hasErrors(bean: manPowerJobInstance, field: 'operation', 'error')} required">
        <label class="control-label" for="operation">
            <g:message code="manPowerJob.operation.label" default="Job" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:select id="operation" name="operation.id" from="${Operation.list()}"  optionValue="${{it.m053JobsId+" - "+it.m053NamaOperation}}" optionKey="id" required="" value="${manPowerJobInstance?.operation?.id}" class="many-to-one"/>
        </div>
    </div>


