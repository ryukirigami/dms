<%@ page import="com.kombos.administrasi.ManPowerJob" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="bootstrapeditable"/>
    <g:set var="entityName"
           value="${message(code: 'manPowerJob.label', default: 'Klasifikasi Job')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <g:javascript disposition="head">
var deleteManPowerJob;

$(function(){ 
	deleteManPowerJob=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/manPowerJob/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadManPowerJobTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

    </g:javascript>
</head>

<body>
<div id="show-manPowerJob" role="main">
<legend>
    <g:message code="default.show.label" args="[entityName]"/>
</legend>
<g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
</g:if>
<table id="manPowerJob"
       class="table table-bordered table-hover">
    <tbody>

    <g:if test="${manPowerJobInstance?.namaManPower}">
        <tr>
            <td class="span2" style="text-align: right;"><span
                    id="namaManPower-label" class="property-label"><g:message
                        code="manPowerJob.namaManPower.label" default="Nama Man Power"/>:</span></td>

            <td class="span3"><span class="property-value"
                                    aria-labelledby="namaManPower-label">
                %{--<ba:editableValue
                        bean="${manPowerJobInstance}" field="namaManPower"
                        url="${request.contextPath}/ManPowerJob/updatefield" type="text"
                        title="Enter namaManPower" onsuccess="reloadManPowerJobTable();" />--}%
                <g:fieldValue field="t015NamaLengkap" bean="${manPowerJobInstance.namaManPower}" />
            </span></td>

        </tr>
    </g:if>

    <g:if test="${manPowerJobInstance?.operation}">
        <tr>
            <td class="span2" style="text-align: right;"><span
                    id="operation-label" class="property-label"><g:message
                        code="manPowerJob.operation.label" default="Job"/>:</span></td>

            <td class="span3"><span class="property-value"
                                    aria-labelledby="operation-label">
                %{--<ba:editableValue
                        bean="${manPowerJobInstance}" field="operation"
                        url="${request.contextPath}/ManPowerJob/updatefield" type="text"
                        title="Enter operation" onsuccess="reloadManPowerJobTable();" />--}%
                <g:fieldValue field="operation" bean="${manPowerJobInstance}" />

            </span></td>

        </tr>
    </g:if>

    <g:if test="${manPowerJobInstance?.lastUpdProcess}">
        <tr>
            <td class="span2" style="text-align: right;"><span
                    id="lastUpdProcess-label" class="property-label"><g:message
                        code="manPowerJob.lastUpdProcess.label" default="last Upd Process" />:</span></td>

            <td class="span3"><span class="property-value"
                                    aria-labelledby="lastUpdProcess-label">
                %{--<ba:editableValue
                        bean="${manPowerJobInstance}" field="m013Telp2"
                        url="${request.contextPath}/ContactPerson/updatefield" type="text"
                        title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%

                <g:fieldValue field="lastUpdProcess" bean="${manPowerJobInstance}" />

            </span></td>

        </tr>
    </g:if>

    <g:if test="${manPowerJobInstance?.dateCreated}">
        <tr>
            <td class="span2" style="text-align: right;"><span
                    id="dateCreated-label" class="property-label"><g:message
                        code="manPowerJob.dateCreated.label" default="Date Created" />:</span></td>

            <td class="span3"><span class="property-value"
                                    aria-labelledby="dateCreated-label">
                %{--<ba:editableValue
                        bean="${manPowerJobInstance}" field="m013Telp2"
                        url="${request.contextPath}/ContactPerson/updatefield" type="text"
                        title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%

                <g:formatDate date="${manPowerJobInstance?.dateCreated}" type="datetime" style="MEDIUM" />

            </span></td>

        </tr>
    </g:if>

    <g:if test="${manPowerJobInstance?.createdBy}">
        <tr>
            <td class="span2" style="text-align: right;"><span
                    id="createdBy-label" class="property-label"><g:message
                        code="manPowerJob.createdBy.label" default="Created By" />:</span></td>

            <td class="span3"><span class="property-value"
                                    aria-labelledby="createdBy-label">
                %{--<ba:editableValue
                        bean="${manPowerJobInstance}" field="m013Telp2"
                        url="${request.contextPath}/ContactPerson/updatefield" type="text"
                        title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%

                <g:fieldValue field="createdBy" bean="${manPowerJobInstance}" />

            </span></td>

        </tr>
    </g:if>

    <g:if test="${manPowerJobInstance?.lastUpdated}">
        <tr>
            <td class="span2" style="text-align: right;"><span
                    id="lastUpdate-label" class="property-label"><g:message
                        code="manPowerJob.lastUpdate.label" default="Last Update" />:</span></td>

            <td class="span3"><span class="property-value"
                                    aria-labelledby="lastUpdate-label">
                %{--<ba:editableValue
                        bean="${manPowerJobInstance}" field="m013Telp2"
                        url="${request.contextPath}/ContactPerson/updatefield" type="text"
                        title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%

                <g:formatDate date="${manPowerJobInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

            </span></td>

        </tr>
    </g:if>

    <g:if test="${manPowerJobInstance?.updatedBy}">
        <tr>
            <td class="span2" style="text-align: right;"><span
                    id="updatedBy-label" class="property-label"><g:message
                        code="manPowerJob.updatedBy.label" default="Update By" />:</span></td>

            <td class="span3"><span class="property-value"
                                    aria-labelledby="updatedBy-label">
                %{--<ba:editableValue
                        bean="${manPowerJobInstance}" field="m013Telp2"
                        url="${request.contextPath}/ContactPerson/updatefield" type="text"
                        title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%

                <g:fieldValue field="updatedBy" bean="${manPowerJobInstance}" />

            </span></td>

        </tr>
    </g:if>


    </tbody>
</table>
<g:form class="form-horizontal">
    <fieldset class="buttons controls">
        <a class="btn cancel" href="javascript:void(0);"
           onclick="expandTableLayout();"><g:message
                code="default.button.cancel.label" default="Cancel"/></a>
        <g:remoteLink class="btn btn-primary edit" action="edit"
                      id="${manPowerJobInstance?.id}"
                      update="[success: 'manPowerJob-form', failure: 'manPowerJob-form']"
                      on404="alert('not found');">
            <g:message code="default.button.edit.label" default="Edit"/>
        </g:remoteLink>
        <ba:confirm id="delete" class="btn cancel"
                    message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
                    onsuccess="deleteManPowerJob('${manPowerJobInstance?.id}')"
                    label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
    </fieldset>
</g:form>
</div>
</body>
</html>
