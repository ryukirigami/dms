
<%@ page import="com.kombos.administrasi.ManPowerJob" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<g:javascript>
    $("#copy").click(function(){
        $('#spinner').fadeIn(1);
        $.ajax({
            url: '${request.contextPath}/manPowerJob/copy?namaManPowerSumber='+$("#namaManPowerSumber").val()+'&namaManPowerTujuan='+$("#namaManPowerTujuan").val(),
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, xhr) {
                if(data=="-1"){
                    //$('#serial').html(data);
                    alert("Man Power yang anda pilih sama");
                } else if(data=="1"){
                    alert("copy success");
                }else if(data=="2"){
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                alert(textStatus);
            },
   			complete:function(XMLHttpRequest,textStatus){
   			    $("#namaManPowerSumber").val('');
   			    $("#namaManPowerTujuan").val('');
   			    reloadManPowerJobTable();
   				$('#spinner').fadeOut();
   			}
        });
        //alert($("#section").val());
    });

    $(function(){
        $('#namaManPowerSumber').typeahead({
            source: function (query, process) {
                return $.get('${request.contextPath}/manPowerJob/getManPowerSumber', { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });
        $('#namaManPowerTujuan').typeahead({
            source: function (query, process) {
                return $.get('${request.contextPath}/manPowerJob/getManPower', { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });
    });
</g:javascript>

<table style="padding: 50px">
    <tr>
        <td style="padding: 5px">
            <div class="control-group fieldcontain ${hasErrors(bean: manPowerJobInstance, field: 'namaManPower', 'error')} ">
                <label class="control-label" for="namaManPowerSumber">
                    <g:message code="manPowerJob.namaManPowerSumber.label" default="Nama Man Power Sumber" />
                </label>
                <div class="controls">
                    <g:textField name="namaManPowerSumber" id="namaManPowerSumber"  class="typeahead" value="" autocomplete="off"/>
                </div>
            </div>
        </td>
        <td style="padding: 5px">
            <div class="control-group fieldcontain ${hasErrors(bean: manPowerJobInstance, field: 'namaManPower', 'error')}">
                <label class="control-label" for="namaManPowerTujuan">
                    <g:message code="manPowerJob.namaManPowerTujuan.label" default="Nama Man Power Tujuan" />
                </label>
                <div class="controls">
                    <g:textField name="namaManPowerTujuan" id="namaManPowerTujuan"  class="typeahead" value="" autocomplete="off"/>
                </div>
            </div>

        </td>
        <td style="padding: 5px">
            <div class="controls">
                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn-primary copy" name="copy" id="copy" value="Copy">Copy</button>
            </div>
        </td>
    </tr>
</table>

<table id="manPowerJob_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerJob.namaManPower.label" default="Nama Man Power" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerJob.operation.label" default="Job" /></div>
			</th>


		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_namaManPower" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaManPower" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_operation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_operation" class="search_init" />
				</div>
			</th>

		</tr>
	</thead>
</table>

<g:javascript>
var manPowerJobTable;
var reloadManPowerJobTable;
$(function(){
	
	reloadManPowerJobTable = function() {
		manPowerJobTable.fnDraw();
	}

	var recordsManPowerJobPerPage = [];
    var anManPowerJobSelected;
    var jmlRecManPowerJobPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	manPowerJobTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	manPowerJobTable = $('#manPowerJob_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsManPowerJob = $("#manPowerJob_datatables tbody .row-select");
            var jmlManPowerJobCek = 0;
            var nRow;
            var idRec;
            rsManPowerJob.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsManPowerJobPerPage[idRec]=="1"){
                    jmlManPowerJobCek = jmlManPowerJobCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsManPowerJobPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecManPowerJobPerPage = rsManPowerJob.length;
            if(jmlManPowerJobCek==jmlRecManPowerJobPerPage && jmlRecManPowerJobPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "namaManPower",
	"mDataProp": "namaManPower",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "operation",
	"mDataProp": "operation",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var namaManPower = $('#filter_namaManPower input').val();
						if(namaManPower){
							aoData.push(
									{"name": 'sCriteria_namaManPower', "value": namaManPower}
							);
						}
	
						var operation = $('#filter_operation input').val();
						if(operation){
							aoData.push(
									{"name": 'sCriteria_operation', "value": operation}
							);
						}
	
						var sertifikat = $('#filter_sertifikat input').val();
						if(sertifikat){
							aoData.push(
									{"name": 'sCriteria_sertifikat', "value": sertifikat}
							);
						}
	
//						var staDel = $('#filter_staDel input').val();
//						if(staDel){
//							aoData.push(
//									{"name": 'sCriteria_staDel', "value": staDel}
//							);
//						}
//
//						var createdBy = $('#filter_createdBy input').val();
//						if(createdBy){
//							aoData.push(
//									{"name": 'sCriteria_createdBy', "value": createdBy}
//							);
//						}
//
//						var updatedBy = $('#filter_updatedBy input').val();
//						if(updatedBy){
//							aoData.push(
//									{"name": 'sCriteria_updatedBy', "value": updatedBy}
//							);
//						}
//
//						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
//						if(lastUpdProcess){
//							aoData.push(
//									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
//							);
//						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#manPowerJob_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsManPowerJobPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsManPowerJobPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#manPowerJob_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsManPowerJobPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anManPowerJobSelected = manPowerJobTable.$('tr.row_selected');
            if(jmlRecManPowerJobPerPage == anManPowerJobSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsManPowerJobPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
