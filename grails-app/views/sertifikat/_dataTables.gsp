
<%@ page import="com.kombos.administrasi.Sertifikat" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="sertifikat_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="sertifikat.m016NamaSertifikat.label" default="Nama Sertifikat" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="sertifikat.m016Inisial.label" default="Inisial Sertifikat" /></div>
			</th>

%{--
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="sertifikat.staDel.label" default="Sta Del" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="sertifikat.m016ID.label" default="M016 ID" /></div>
			</th>
--}%
		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m016NamaSertifikat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m016NamaSertifikat" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m016Inisial" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m016Inisial" class="search_init" />
				</div>
			</th>
%{--	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m016ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m016ID" class="search_init" />
				</div>
			</th>
--}%

		</tr>
	</thead>
</table>

<g:javascript>
var sertifikatTable;
var reloadSertifikatTable;
$(function(){
	
	reloadSertifikatTable = function() {
		sertifikatTable.fnDraw();
	}

	var recordsSertifikatPerPage = [];
    var anSertifikatSelected;
    var jmlRecSertifikatPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	sertifikatTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	sertifikatTable = $('#sertifikat_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsSertifikat = $("#sertifikat_datatables tbody .row-select");
            var jmlSertifikatCek = 0;
            var nRow;
            var idRec;
            rsSertifikat.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsSertifikatPerPage[idRec]=="1"){
                    jmlSertifikatCek = jmlSertifikatCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsSertifikatPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecSertifikatPerPage = rsSertifikat.length;
            if(jmlSertifikatCek==jmlRecSertifikatPerPage && jmlRecSertifikatPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m016NamaSertifikat",
	"mDataProp": "m016NamaSertifikat",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m016Inisial",
	"mDataProp": "m016Inisial",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m016ID",
	"mDataProp": "m016ID",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m016NamaSertifikat = $('#filter_m016NamaSertifikat input').val();
						if(m016NamaSertifikat){
							aoData.push(
									{"name": 'sCriteria_m016NamaSertifikat', "value": m016NamaSertifikat}
							);
						}
	
						var m016Inisial = $('#filter_m016Inisial input').val();
						if(m016Inisial){
							aoData.push(
									{"name": 'sCriteria_m016Inisial', "value": m016Inisial}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var m016ID = $('#filter_m016ID input').val();
						if(m016ID){
							aoData.push(
									{"name": 'sCriteria_m016ID', "value": m016ID}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#sertifikat_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsSertifikatPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsSertifikatPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#sertifikat_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsSertifikatPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anSertifikatSelected = sertifikatTable.$('tr.row_selected');
            if(jmlRecSertifikatPerPage == anSertifikatSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsSertifikatPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
