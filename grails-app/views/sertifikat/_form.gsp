<%@ page import="com.kombos.administrasi.Sertifikat" %>



<div class="control-group fieldcontain ${hasErrors(bean: sertifikatInstance, field: 'm016NamaSertifikat', 'error')} required">
	<label class="control-label" for="m016NamaSertifikat">
		<g:message code="sertifikat.m016NamaSertifikat.label" default="Nama Sertifikat" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m016NamaSertifikat" maxlength="50" required="" value="${sertifikatInstance?.m016NamaSertifikat}"/>
	</div>
</div>

<g:javascript>
    document.getElementById("m016NamaSertifikat").focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: sertifikatInstance, field: 'm016Inisial', 'error')} required">
	<label class="control-label" for="m016Inisial">
		<g:message code="sertifikat.m016Inisial.label" default="Inisial Sertifikat" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m016Inisial" maxlength="8" required="" value="${sertifikatInstance?.m016Inisial}"/>
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: sertifikatInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="sertifikat.staDel.label" default="Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" maxlength="1" required="" value="${sertifikatInstance?.staDel}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: sertifikatInstance, field: 'm016ID', 'error')} required">
	<label class="control-label" for="m016ID">
		<g:message code="sertifikat.m016ID.label" default="M016 ID" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m016ID" maxlength="2" required="" value="${sertifikatInstance?.m016ID}"/>
	</div>
</div>
--}%

