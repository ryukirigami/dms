

<%@ page import="com.kombos.administrasi.Sertifikat" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'sertifikat.label', default: 'Sertifikat')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteSertifikat;

$(function(){ 
	deleteSertifikat=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/sertifikat/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadSertifikatTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-sertifikat" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="sertifikat"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${sertifikatInstance?.m016NamaSertifikat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m016NamaSertifikat-label" class="property-label"><g:message
					code="sertifikat.m016NamaSertifikat.label" default="Nama Sertifikat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m016NamaSertifikat-label">
						%{--<ba:editableValue
								bean="${sertifikatInstance}" field="m016NamaSertifikat"
								url="${request.contextPath}/Sertifikat/updatefield" type="text"
								title="Enter m016NamaSertifikat" onsuccess="reloadSertifikatTable();" />--}%
							
								<g:fieldValue bean="${sertifikatInstance}" field="m016NamaSertifikat"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${sertifikatInstance?.m016Inisial}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m016Inisial-label" class="property-label"><g:message
					code="sertifikat.m016Inisial.label" default="Inisial Sertifikat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m016Inisial-label">
						%{--<ba:editableValue
								bean="${sertifikatInstance}" field="m016Inisial"
								url="${request.contextPath}/Sertifikat/updatefield" type="text"
								title="Enter m016Inisial" onsuccess="reloadSertifikatTable();" />--}%
							
								<g:fieldValue bean="${sertifikatInstance}" field="m016Inisial"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${sertifikatInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="sertifikat.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${sertifikatInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/sertifikat/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadsertifikatTable();" />--}%

                        <g:fieldValue bean="${sertifikatInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${sertifikatInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="sertifikat.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${sertifikatInstance}" field="dateCreated"
                                url="${request.contextPath}/sertifikat/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadsertifikatTable();" />--}%

                        <g:formatDate date="${sertifikatInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${sertifikatInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="sertifikat.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${sertifikatInstance}" field="createdBy"
                                url="${request.contextPath}/sertifikat/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadsertifikatTable();" />--}%

                        <g:fieldValue bean="${sertifikatInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${sertifikatInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="sertifikat.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${sertifikatInstance}" field="lastUpdated"
                                url="${request.contextPath}/sertifikat/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadsertifikatTable();" />--}%

                        <g:formatDate date="${sertifikatInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${sertifikatInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="sertifikat.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${sertifikatInstance}" field="updatedBy"
                                url="${request.contextPath}/sertifikat/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadsertifikatTable();" />--}%

                        <g:fieldValue bean="${sertifikatInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
<!--			
				<g:if test="${sertifikatInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="sertifikat.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${sertifikatInstance}" field="staDel"
								url="${request.contextPath}/Sertifikat/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadSertifikatTable();" />--}%
							
								<g:fieldValue bean="${sertifikatInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${sertifikatInstance?.m016ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m016ID-label" class="property-label"><g:message
					code="sertifikat.m016ID.label" default="M016 ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m016ID-label">
						%{--<ba:editableValue
								bean="${sertifikatInstance}" field="m016ID"
								url="${request.contextPath}/Sertifikat/updatefield" type="text"
								title="Enter m016ID" onsuccess="reloadSertifikatTable();" />--}%
							
								<g:fieldValue bean="${sertifikatInstance}" field="m016ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
-->			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${sertifikatInstance?.id}"
					update="[success:'sertifikat-form',failure:'sertifikat-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteSertifikat('${sertifikatInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
