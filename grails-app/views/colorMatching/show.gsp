

<%@ page import="com.kombos.administrasi.ColorMatching" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'colorMatching.label', default: 'Color Matching')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteColorMatching;

$(function(){ 
	deleteColorMatching=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/colorMatching/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadColorMatchingTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-colorMatching" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="colorMatching"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${colorMatchingInstance?.m046TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m046TglBerlaku-label" class="property-label"><g:message
					code="colorMatching.m046TglBerlaku.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m046TglBerlaku-label">
						%{--<ba:editableValue
								bean="${colorMatchingInstance}" field="m046TglBerlaku"
								url="${request.contextPath}/ColorMatching/updatefield" type="text"
								title="Enter m046TglBerlaku" onsuccess="reloadColorMatchingTable();" />--}%
							
								<g:formatDate date="${colorMatchingInstance?.m046TglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${colorMatchingInstance?.m046Klasifikasi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m046Klasifikasi-label" class="property-label"><g:message
					code="colorMatching.m046Klasifikasi.label" default="Color Clasification" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m046Klasifikasi-label">
						%{--<ba:editableValue
								bean="${colorMatchingInstance}" field="m046Klasifikasi"
								url="${request.contextPath}/ColorMatching/updatefield" type="text"
								title="Enter m046Klasifikasi" onsuccess="reloadColorMatchingTable();" />--}%
							
								<g:fieldValue bean="${colorMatchingInstance}" field="m046Klasifikasi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${colorMatchingInstance?.m046JmlPanel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m046JmlPanel-label" class="property-label"><g:message
					code="colorMatching.m046JmlPanel.label" default="Jummlah Panel" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m046JmlPanel-label">
						%{--<ba:editableValue
								bean="${colorMatchingInstance}" field="m046JmlPanel"
								url="${request.contextPath}/ColorMatching/updatefield" type="text"
								title="Enter m046JmlPanel" onsuccess="reloadColorMatchingTable();" />--}%
							
								<g:fieldValue bean="${colorMatchingInstance}" field="m046JmlPanel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${colorMatchingInstance?.m046StdTime}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m046StdTime-label" class="property-label"><g:message
					code="colorMatching.m046StdTime.label" default="Standar Time" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m046StdTime-label">
						%{--<ba:editableValue
								bean="${colorMatchingInstance}" field="m046StdTime"
								url="${request.contextPath}/ColorMatching/updatefield" type="text"
								title="Enter m046StdTime" onsuccess="reloadColorMatchingTable();" />--}%
							
								<g:fieldValue bean="${colorMatchingInstance}" field="m046StdTime"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${colorMatchingInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="colorMatching.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${colorMatchingInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/ColorMatching/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadColorMatchingTable();" />--}%

                        <g:fieldValue bean="${colorMatchingInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${colorMatchingInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="colorMatching.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${colorMatchingInstance}" field="dateCreated"
                                url="${request.contextPath}/ColorMatching/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadColorMatchingTable();" />--}%

                        <g:formatDate date="${colorMatchingInstance?.dateCreated}" type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${colorMatchingInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="colorMatching.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${colorMatchingInstance}" field="createdBy"
								url="${request.contextPath}/ColorMatching/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadColorMatchingTable();" />--}%
							
								<g:fieldValue bean="${colorMatchingInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${colorMatchingInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="colorMatching.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${colorMatchingInstance}" field="lastUpdated"
								url="${request.contextPath}/ColorMatching/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadColorMatchingTable();" />--}%
							
								<g:formatDate date="${colorMatchingInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${colorMatchingInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="colorMatching.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${colorMatchingInstance}" field="updatedBy"
                                url="${request.contextPath}/ColorMatching/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadColorMatchingTable();" />--}%

                        <g:fieldValue bean="${colorMatchingInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            %{--
                        <g:if test="${colorMatchingInstance?.companyDealer}">
                            <tr>
                                <td class="span2" style="text-align: right;"><span
                                        id="companyDealer-label" class="property-label"><g:message
                                            code="colorMatching.companyDealer.label" default="Company Dealer" />:</span></td>

                                <td class="span3"><span class="property-value"
                                                        aria-labelledby="companyDealer-label">
                                    <ba:editableValue
                                            bean="${colorMatchingInstance}" field="companyDealer"
                                            url="${request.contextPath}/ColorMatching/updatefield" type="text"
                                            title="Enter companyDealer" onsuccess="reloadColorMatchingTable();" />

                        <g:link controller="companyDealer" action="show" id="${colorMatchingInstance?.companyDealer?.id}">${colorMatchingInstance?.companyDealer?.encodeAsHTML()}</g:link>

                    </span></td>

                </tr>
            </g:if>
--}%
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
                        code="default.button.close.label" default="Close" /></a>
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${colorMatchingInstance?.id}"
					update="[success:'colorMatching-form',failure:'colorMatching-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteColorMatching('${colorMatchingInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
