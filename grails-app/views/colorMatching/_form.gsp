<%@ page import="com.kombos.administrasi.ColorMatching" %>
<r:require modules="autoNumeric" />
<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init',{
            vMin:'0',
            aSep:'',
            mDec:''
        });
    });
</script>


<div class="control-group fieldcontain ${hasErrors(bean: colorMatchingInstance, field: 'm046TglBerlaku', 'error')} required">
	<label class="control-label" for="m046TglBerlaku">
		<g:message code="colorMatching.m046TglBerlaku.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="m046TglBerlaku" id="m046TglBerlaku" precision="day"  value="${colorMatchingInstance?.m046TglBerlaku}" format="dd/MM/yyyy" required="true" />
	</div>
</div>
<g:javascript>
    document.getElementById('m046TglBerlaku').focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: colorMatchingInstance, field: 'm046Klasifikasi', 'error')} required">
	<label class="control-label" for="m046Klasifikasi">
		<g:message code="colorMatching.m046Klasifikasi.label" default="Color Clasification" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:field name="m046Klasifikasi" type="number" value="${colorMatchingInstance.m046Klasifikasi}" required=""/>Coat--}%
        <g:select style="width: 185px;" name="m046Klasifikasi" from="${['2','3']}" value="${colorMatchingInstance.m046Klasifikasi}" required=""  class="many-to-one"/> Coat
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: colorMatchingInstance, field: 'm046JmlPanel', 'error')} required">
	<label class="control-label" for="m046JmlPanel">
		<g:message code="colorMatching.m046JmlPanel.label" default="Jumlah Panel" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m046JmlPanel" class="auto" value="${colorMatchingInstance.m046JmlPanel}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: colorMatchingInstance, field: 'm046StdTime', 'error')} required">
	<label class="control-label" for="m046StdTime">
		<g:message code="colorMatching.m046StdTime.label" default="Standar Time" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m046StdTime" class="auto" value="${fieldValue(bean: colorMatchingInstance, field: 'm046StdTime')}" required=""/>
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: colorMatchingInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="colorMatching.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${colorMatchingInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: colorMatchingInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="colorMatching.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${colorMatchingInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: colorMatchingInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="colorMatching.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${colorMatchingInstance?.lastUpdProcess}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: colorMatchingInstance, field: 'companyDealer', 'error')} ">
	<label class="control-label" for="companyDealer">
		<g:message code="colorMatching.companyDealer.label" default="Company Dealer" />
		
	</label>
	<div class="controls">
	<g:select id="companyDealer" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" value="${colorMatchingInstance?.companyDealer?.id}" class="many-to-one" noSelection="['null': '']"/>
	</div>
</div>
--}%
