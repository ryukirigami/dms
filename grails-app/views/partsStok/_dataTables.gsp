
<%@ page import="com.kombos.parts.PartsStok" %>

<r:require modules="baseapplayout,autoNumeric" />

<g:render template="../menu/maxLineDisplay"/>

<table id="partsStok_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsStok.kode.goods.label" default="Kode Part" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsStok.nama.goods.label" default="Nama Part" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsStok.t131Qty1.label" default="Harga Jual" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsStok.t131Qty1.label" default="Qty Total" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsStok.t131Qty1Free.label" default="Qty Free" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsStok.t131Qty1Reserved.label" default="Qty Reserved" /></div>
            </th>



            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsStok.t131Qty1WIP.label" default="Qty WIP" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsStok.t131Qty1BlockStok.label" default="Qty BlockStok" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsStok.t131LandedCost.label" default="Landed Cost" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsStok.icc.label" default="ICC" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsStok.location.label" default="Location" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsStok.location.label" default="Pembelian Terakhir" /></div>
            </th>

		</tr>
		<tr>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_kode_partsStok" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_kode_partsStok" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_nama_partsStok" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_nama_partsStok" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_harga_partsStok" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_harga_partsStok" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t131Qty1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t131Qty1" class="search_init  auto" />
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_t131Qty1Free" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t131Qty1Free" class="search_init auto" />
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_t131Qty1Reserved" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t131Qty1Reserved" class="search_init auto" />
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_t131Qty1WIP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t131Qty1WIP" class="search_init auto" />
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_t131Qty1BlockStok" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t131Qty1BlockStok" class="search_init auto" />
                </div>
            </th>



            <th style="border-top: none;padding: 5px;">
                <div id="filter_t131LandedCost" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t131LandedCost" class="search_init auto" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_icc" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_icc_partsStok" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_location" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_location_partsStok" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_pembelian" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">

                </div>
            </th>

		</tr>
	</thead>
</table>

<g:javascript>
var partsStokTable;
var reloadPartsStokTable;

$(function(){

    var recordspartsStokPerPage = [];
    var anpartsStokSelected;
    var jmlRecpartsStokPerPage=0;
    var id;


   $('.auto').autoNumeric('init');


	reloadPartsStokTable = function() {
		partsStokTable.fnDraw();
	}
    $('#view').click(function(e){
        e.stopPropagation();
		partsStokTable.fnDraw();
	});
	
	$('#search_t131Tanggal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t131Tanggal_day').val(newDate.getDate());
			$('#search_t131Tanggal_month').val(newDate.getMonth()+1);
			$('#search_t131Tanggal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			partsStokTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	partsStokTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	partsStokTable = $('#partsStok_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rspartsStok = $("#partsStok_datatables tbody .row-select");
            var jmlpartsStokCek = 0;
            var nRow;
            var idRec;
            rspartsStok.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordspartsStokPerPage[idRec]=="1"){
                    jmlpartsStokCek = jmlpartsStokCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspartsStokPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecpartsStokPerPage = rspartsStok.length;
            if(jmlpartsStokCek==jmlRecpartsStokPerPage && jmlRecpartsStokPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "goods",
	"mDataProp": "kodeGoods",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['kodeGoods']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "namaGoods",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "harga",
	"mDataProp": "harga",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t131Qty1",
	"mDataProp": "t131Qty1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t131Qty1Free",
	"mDataProp": "t131Qty1Free",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t131Qty1Reserved",
	"mDataProp": "t131Qty1Reserved",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t131Qty1WIP",
	"mDataProp": "t131Qty1WIP",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "t131Qty1BlockStock",
	"mDataProp": "t131Qty1BlockStock",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "t131LandedCost",
	"mDataProp": "t131LandedCost",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "icc",
	"mDataProp": "icc",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "location",
	"mDataProp": "location",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},

{
	"sName": "pembelianTerakhir",
	"mDataProp": "pembelianTerakhir",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
		
						var kodeGoods = $('#filter_kode_partsStok input').val();
						if(kodeGoods){
							aoData.push(
									{"name": 'sCriteria_kode_goods', "value": kodeGoods}
							);
						}

						var namaGoods = $('#filter_nama_partsStok input').val();
						if(namaGoods){
							aoData.push(
									{"name": 'sCriteria_nama_goods', "value": namaGoods}
							);
						}

						var status = $('#status').val();
						if(status){
							aoData.push(
									{"name": 'status', "value": status}
							);
						}
						
						var t131Qty1 = $('#filter_t131Qty1 input').val();
						if(t131Qty1){
							aoData.push(
									{"name": 'sCriteria_t131Qty1', "value": t131Qty1}
							);
						}
						
						var t131Qty2 = $('#filter_t131Qty2 input').val();
						if(t131Qty2){
							aoData.push(
									{"name": 'sCriteria_t131Qty2', "value": t131Qty2}
							);
						}

						var t131Qty1Free = $('#filter_t131Qty1Free input').val();
						if(t131Qty1Free){
							aoData.push(
									{"name": 'sCriteria_t131Qty1Free', "value": t131Qty1Free}
							);
						}

						var t131Qty2Free = $('#filter_t131Qty2Free input').val();
						if(t131Qty2Free){
							aoData.push(
									{"name": 'sCriteria_t131Qty2Free', "value": t131Qty2Free}
							);
						}

						var t131Qty1Reserved = $('#filter_t131Qty1Reserved input').val();
						if(t131Qty1Reserved){
							aoData.push(
									{"name": 'sCriteria_t131Qty1Reserved', "value": t131Qty1Reserved}
							);
						}

						var t131Qty2Reserved = $('#filter_t131Qty2Reserved input').val();
						if(t131Qty2Reserved){
							aoData.push(
									{"name": 'sCriteria_t131Qty2Reserved', "value": t131Qty2Reserved}
							);
						}

						var t131Qty1WIP = $('#filter_t131Qty1WIP input').val();
						if(t131Qty1WIP){
							aoData.push(
									{"name": 'sCriteria_t131Qty1WIP', "value": t131Qty1WIP}
							);
						}

						var t131Qty2WIP = $('#filter_t131Qty2WIP input').val();
						if(t131Qty2WIP){
							aoData.push(
									{"name": 'sCriteria_t131Qty2WIP', "value": t131Qty2WIP}
							);
						}

						var t131Qty1BlockStok = $('#filter_t131Qty1BlockStok input').val();
						if(t131Qty1BlockStok){
							aoData.push(
									{"name": 'sCriteria_t131Qty1BlockStok', "value": t131Qty1BlockStok}
							);
						}


                        var t131Qty2BlockStok = $('#filter_t131Qty2BlockStok input').val();
                            if(t131Qty2BlockStok){
                                aoData.push(
                                        {"name": 'sCriteria_t131Qty2BlockStok', "value": t131Qty2BlockStok}
							);
						}


						var t131LandedCost = $('#filter_t131LandedCost input').val();
						if(t131LandedCost){
							aoData.push(
									{"name": 'sCriteria_t131LandedCost', "value": t131LandedCost}
							);
						}

                        var icc = $('#filter_icc input').val();
						if(icc){
							aoData.push(
									{"name": 'sCriteria_icc', "value": icc}
							);
						}

                        var location = $('#filter_location input').val();
                            if(location){
							aoData.push(
									{"name": 'sCriteria_location', "value": location}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

		$('.select-all').click(function(e) {

        $("#partsStok_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordspartsStokPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordspartsStokPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#partsStok_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordspartsStokPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anpartsStokSelected = partsStokTable.$('tr.row_selected');

            if(jmlRecpartsStokPerPage == anpartsStokSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordspartsStokPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
