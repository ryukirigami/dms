<%@ page import="com.kombos.parts.PartsStok" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'partsStok.label', default: 'Parts Stok')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			var loadOnHandAdjusment;
            var goodsAdjustment;
			$(function(){

			    goodsAdjustment = function(){
			         var checkGoods =[];

                    $("#partsStok_datatables tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            checkGoods.push(id);
                         }
                    });

                     if(checkGoods.length<1){
                        toastr.error('Anda belum memilih data yang akan di view di On Hand Adjustment');
                         return false;
                    }else{
                        var idGoods = JSON.stringify(checkGoods);
                        checkGoods = []
                        return idGoods;
                    }

			    }


			    loadOnHandAdjusment = function(id) {
			        var goods = goodsAdjustment();

			        if(!goods){
			            return;
			        }

                    $('#spinner').fadeIn(1);
                    $.ajax({url: '${request.contextPath}/partsAdjust/',
                        type:"POST",dataType: "html",
                        data : {idGoods : goods},
                        complete : function (req, err) {
                            $('#main-content').html(req.responseText);
                            $('#spinner').fadeOut();
                            loading = false;
                        }
                    });
                };


			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('partsStok');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('partsStok', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('partsStok', '${request.contextPath}/partsStok/massdelete', reloadPartsStokTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('partsStok','${request.contextPath}/partsStok/show/'+id);
				};
				
				edit = function(id) {
					editInstance('partsStok','${request.contextPath}/partsStok/edit/'+id);
				};

				printStock = function(format){
				        var status = $('#status').val();
                       window.location = "${request.contextPath}/partsStok/printStock?status="+status+"&format="+format;
                }

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
	</div>
	<div class="box">
		<div class="span12" id="partsStok-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">
                                Status :
                            </label>
                        </td>
                        <td style="width: 200px">
                            <div id="filter_m777Tgl" class="controls">
                                <select name="status" id="status" style="width: 170px">
                                    <option value="0">Semua</option>
                                    <option value="1">New (< 3 Bln)</option>
                                    <option value="2">Medium ( 3 - 6 Bln)</option>
                                    <option value="3">Old ( > 6 Bln)</option>
                                </select>
                            </div>
                        </td>
                        <td style="width: 130px">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="view" >View</button>
                            </label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <table>
            <g:render template="dataTables" />
                    <g:field type="button" onclick="loadOnHandAdjusment();" class="btn btn-primary pull-right" name="open-view" id="open-view" value="View On Hand Adjustment" />
                    <button id='printStockData' onclick="printStock('pdf');" type="button" class="btn btn-primary">Print PDF</button>
                    <button id='printStockData' onclick="printStock('xls');" type="button" class="btn btn-primary">Print Excel</button>
            </table>
            <div style="clear: both;"></div>
		</div>
		<div class="span7" id="partsStok-form" style="display: none;"></div>
	</div>
</body>
</html>
