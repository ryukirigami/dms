<%@ page import="com.kombos.parts.PartsStok" %>



<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 'companyDealer', 'error')} ">
	<label class="control-label" for="companyDealer">
		<g:message code="partsStok.companyDealer.label" default="Company Dealer" />
		
	</label>
	<div class="controls">
	<g:select id="companyDealer" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" required="" value="${partsStokInstance?.companyDealer?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 'goods', 'error')} ">
	<label class="control-label" for="goods">
		<g:message code="partsStok.goods.label" default="Goods" />
		
	</label>
	<div class="controls">
	<g:select id="goods" name="goods.id" from="${com.kombos.parts.Goods.list()}" optionKey="id" required="" value="${partsStokInstance?.goods?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="partsStok.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${partsStokInstance?.lastUpdProcess}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 't131LandedCost', 'error')} ">
	<label class="control-label" for="t131LandedCost">
		<g:message code="partsStok.t131LandedCost.label" default="T131 Landed Cost" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t131LandedCost" value="${partsStokInstance.t131LandedCost}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 't131Qty1', 'error')} ">
	<label class="control-label" for="t131Qty1">
		<g:message code="partsStok.t131Qty1.label" default="T131 Qty1" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t131Qty1" value="${partsStokInstance.t131Qty1}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 't131Qty1BlockStock', 'error')} ">
	<label class="control-label" for="t131Qty1BlockStock">
		<g:message code="partsStok.t131Qty1BlockStock.label" default="T131 Qty1 Block Stock" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t131Qty1BlockStock" value="${partsStokInstance.t131Qty1BlockStock}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 't131Qty1Free', 'error')} ">
	<label class="control-label" for="t131Qty1Free">
		<g:message code="partsStok.t131Qty1Free.label" default="T131 Qty1 Free" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t131Qty1Free" value="${partsStokInstance.t131Qty1Free}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 't131Qty1Reserved', 'error')} ">
	<label class="control-label" for="t131Qty1Reserved">
		<g:message code="partsStok.t131Qty1Reserved.label" default="T131 Qty1 Reserved" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t131Qty1Reserved" value="${partsStokInstance.t131Qty1Reserved}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 't131Qty1WIP', 'error')} ">
	<label class="control-label" for="t131Qty1WIP">
		<g:message code="partsStok.t131Qty1WIP.label" default="T131 Qty1 WIP" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t131Qty1WIP" value="${partsStokInstance.t131Qty1WIP}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 't131Qty2', 'error')} ">
	<label class="control-label" for="t131Qty2">
		<g:message code="partsStok.t131Qty2.label" default="T131 Qty2" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t131Qty2" value="${partsStokInstance.t131Qty2}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 't131Qty2BlockStock', 'error')} ">
	<label class="control-label" for="t131Qty2BlockStock">
		<g:message code="partsStok.t131Qty2BlockStock.label" default="T131 Qty2 Block Stock" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t131Qty2BlockStock" value="${partsStokInstance.t131Qty2BlockStock}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 't131Qty2Free', 'error')} ">
	<label class="control-label" for="t131Qty2Free">
		<g:message code="partsStok.t131Qty2Free.label" default="T131 Qty2 Free" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t131Qty2Free" value="${partsStokInstance.t131Qty2Free}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 't131Qty2Reserved', 'error')} ">
	<label class="control-label" for="t131Qty2Reserved">
		<g:message code="partsStok.t131Qty2Reserved.label" default="T131 Qty2 Reserved" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t131Qty2Reserved" value="${partsStokInstance.t131Qty2Reserved}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 't131Qty2WIP', 'error')} ">
	<label class="control-label" for="t131Qty2WIP">
		<g:message code="partsStok.t131Qty2WIP.label" default="T131 Qty2 WIP" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t131Qty2WIP" value="${partsStokInstance.t131Qty2WIP}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 'staDel', 'error')} ">
	<label class="control-label" for="staDel">
		<g:message code="partsStok.staDel.label" default="T131 Sta Del" />
		
	</label>
	<div class="controls">
	<g:textField name="staDel" value="${partsStokInstance?.staDel}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsStokInstance, field: 't131Tanggal', 'error')} ">
	<label class="control-label" for="t131Tanggal">
		<g:message code="partsStok.t131Tanggal.label" default="T131 Tanggal" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="t131Tanggal" precision="day" value="${partsStokInstance?.t131Tanggal}" format="yyyy-MM-dd"/>
	</div>
</div>

