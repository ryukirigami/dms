

<%@ page import="com.kombos.parts.PartsStok" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'partsStok.label', default: 'PartsStok')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePartsStok;

$(function(){ 
	deletePartsStok=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/partsStok/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPartsStokTable();
   				expandTableLayout('partsStok');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-partsStok" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="partsStok"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${partsStokInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="partsStok.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="companyDealer"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:link controller="companyDealer" action="show" id="${partsStokInstance?.companyDealer?.id}">${partsStokInstance?.companyDealer?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="partsStok.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="createdBy"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:fieldValue bean="${partsStokInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="partsStok.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="dateCreated"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:formatDate date="${partsStokInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="partsStok.goods.label" default="Goods" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="goods"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter goods" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:link controller="goods" action="show" id="${partsStokInstance?.goods?.id}">${partsStokInstance?.goods?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="partsStok.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="lastUpdProcess"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:fieldValue bean="${partsStokInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="partsStok.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="lastUpdated"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:formatDate date="${partsStokInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.t131LandedCost}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t131LandedCost-label" class="property-label"><g:message
					code="partsStok.t131LandedCost.label" default="T131 Landed Cost" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t131LandedCost-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="t131LandedCost"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter t131LandedCost" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:fieldValue bean="${partsStokInstance}" field="t131LandedCost"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.t131Qty1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t131Qty1-label" class="property-label"><g:message
					code="partsStok.t131Qty1.label" default="T131 Qty1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t131Qty1-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="t131Qty1"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter t131Qty1" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:fieldValue bean="${partsStokInstance}" field="t131Qty1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.t131Qty1BlockStock}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t131Qty1BlockStock-label" class="property-label"><g:message
					code="partsStok.t131Qty1BlockStock.label" default="T131 Qty1 Block Stock" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t131Qty1BlockStock-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="t131Qty1BlockStock"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter t131Qty1BlockStock" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:fieldValue bean="${partsStokInstance}" field="t131Qty1BlockStock"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.t131Qty1Free}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t131Qty1Free-label" class="property-label"><g:message
					code="partsStok.t131Qty1Free.label" default="T131 Qty1 Free" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t131Qty1Free-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="t131Qty1Free"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter t131Qty1Free" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:fieldValue bean="${partsStokInstance}" field="t131Qty1Free"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.t131Qty1Reserved}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t131Qty1Reserved-label" class="property-label"><g:message
					code="partsStok.t131Qty1Reserved.label" default="T131 Qty1 Reserved" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t131Qty1Reserved-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="t131Qty1Reserved"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter t131Qty1Reserved" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:fieldValue bean="${partsStokInstance}" field="t131Qty1Reserved"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.t131Qty1WIP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t131Qty1WIP-label" class="property-label"><g:message
					code="partsStok.t131Qty1WIP.label" default="T131 Qty1 WIP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t131Qty1WIP-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="t131Qty1WIP"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter t131Qty1WIP" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:fieldValue bean="${partsStokInstance}" field="t131Qty1WIP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.t131Qty2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t131Qty2-label" class="property-label"><g:message
					code="partsStok.t131Qty2.label" default="T131 Qty2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t131Qty2-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="t131Qty2"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter t131Qty2" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:fieldValue bean="${partsStokInstance}" field="t131Qty2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.t131Qty2BlockStock}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t131Qty2BlockStock-label" class="property-label"><g:message
					code="partsStok.t131Qty2BlockStock.label" default="T131 Qty2 Block Stock" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t131Qty2BlockStock-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="t131Qty2BlockStock"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter t131Qty2BlockStock" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:fieldValue bean="${partsStokInstance}" field="t131Qty2BlockStock"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.t131Qty2Free}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t131Qty2Free-label" class="property-label"><g:message
					code="partsStok.t131Qty2Free.label" default="T131 Qty2 Free" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t131Qty2Free-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="t131Qty2Free"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter t131Qty2Free" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:fieldValue bean="${partsStokInstance}" field="t131Qty2Free"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.t131Qty2Reserved}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t131Qty2Reserved-label" class="property-label"><g:message
					code="partsStok.t131Qty2Reserved.label" default="T131 Qty2 Reserved" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t131Qty2Reserved-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="t131Qty2Reserved"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter t131Qty2Reserved" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:fieldValue bean="${partsStokInstance}" field="t131Qty2Reserved"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.t131Qty2WIP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t131Qty2WIP-label" class="property-label"><g:message
					code="partsStok.t131Qty2WIP.label" default="T131 Qty2 WIP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t131Qty2WIP-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="t131Qty2WIP"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter t131Qty2WIP" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:fieldValue bean="${partsStokInstance}" field="t131Qty2WIP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="partsStok.staDel.label" default="T131 Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="staDel"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:fieldValue bean="${partsStokInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.t131Tanggal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t131Tanggal-label" class="property-label"><g:message
					code="partsStok.t131Tanggal.label" default="T131 Tanggal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t131Tanggal-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="t131Tanggal"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter t131Tanggal" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:formatDate date="${partsStokInstance?.t131Tanggal}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsStokInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="partsStok.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${partsStokInstance}" field="updatedBy"
								url="${request.contextPath}/PartsStok/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadPartsStokTable();" />--}%
							
								<g:fieldValue bean="${partsStokInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('partsStok');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${partsStokInstance?.id}"
					update="[success:'partsStok-form',failure:'partsStok-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePartsStok('${partsStokInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
