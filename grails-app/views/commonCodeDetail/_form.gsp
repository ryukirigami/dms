<%@ page import="com.kombos.baseapp.CommonCodeDetail" %>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeDetailInstance, field: 'commoncode', 'error')} ">
    <label class="control-label" for="commoncode">
        <g:message code="commonCodeDetail.commoncode.label" default="Commoncode" />
    </label>
    <div class="controls">
        <g:textField name="commoncode" value="${commonCodeDetailInstance?.commoncode}"/>
        <div id=commoncode_label></div>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeDetailInstance, field: 'bicode', 'error')} ">
	<label class="control-label" for="bicode">
		<g:message code="commonCodeDetail.bicode.label" default="Bicode" />
	</label>
	<div class="controls">
	    <g:textField name="bicode" maxlength="5" value="${commonCodeDetailInstance?.bicode}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeDetailInstance, field: 'description', 'error')} ">
	<label class="control-label" for="description">
		<g:message code="commonCodeDetail.description.label" default="Description" />
	</label>
	<div class="controls">
	    <g:textField name="description" value="${commonCodeDetailInstance?.description}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeDetailInstance, field: 'parentcode', 'error')} ">
	<label class="control-label" for="parentcode">
		<g:message code="commonCodeDetail.parentcode.label" default="Parentcode" />
	</label>
	<div class="controls">
	    <g:textField name="parentcode" value="${commonCodeDetailInstance?.parentcode}"/>
        %{--<div id=parentcode_label></div>--}%
	</div>
</div>

<div id="parentbicode_div" class="control-group fieldcontain ${hasErrors(bean: commonCodeDetailInstance, field: 'parentbicode', 'error')} ">
	<label class="control-label" for="parentbicode">
		<g:message code="commonCodeDetail.parentbicode.label" default="Parentbicode" />
	</label>
	<div class="controls">
	    <g:textField name="parentbicode" value="${commonCodeDetailInstance?.parentbicode}"/>
        %{--<div id=parentbicode_label></div>--}%
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeDetailInstance, field: 'sequence', 'error')} required">
    <label class="control-label" for="sequence">
        <g:message code="commonCodeDetail.sequence.label" default="Sequence" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:field name="sequence" type="number" value="${commonCodeDetailInstance.sequence}" required=""/>
    </div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: commonCodeDetailInstance, field: 'createddate', 'error')} ">
	<label class="control-label" for="createddate">
		<g:message code="commonCodeDetail.createddate.label" default="Createddate" />
		
	</label>
	<div class="controls">
	<g:datePicker name="createddate" precision="day"  value="${commonCodeDetailInstance?.createddate}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeDetailInstance, field: 'createdhost', 'error')} ">
	<label class="control-label" for="createdhost">
		<g:message code="commonCodeDetail.createdhost.label" default="Createdhost" />
		
	</label>
	<div class="controls">
	<g:textField name="createdhost" value="${commonCodeDetailInstance?.createdhost}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeDetailInstance, field: 'lasteditby', 'error')} ">
	<label class="control-label" for="lasteditby">
		<g:message code="commonCodeDetail.lasteditby.label" default="Lasteditby" />
		
	</label>
	<div class="controls">
	<g:textField name="lasteditby" value="${commonCodeDetailInstance?.lasteditby}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeDetailInstance, field: 'lasteditdate', 'error')} ">
	<label class="control-label" for="lasteditdate">
		<g:message code="commonCodeDetail.lasteditdate.label" default="Lasteditdate" />
		
	</label>
	<div class="controls">
	<g:datePicker name="lasteditdate" precision="day"  value="${commonCodeDetailInstance?.lasteditdate}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeDetailInstance, field: 'lastedithost', 'error')} ">
	<label class="control-label" for="lastedithost">
		<g:message code="commonCodeDetail.lastedithost.label" default="Lastedithost" />
		
	</label>
	<div class="controls">
	<g:textField name="lastedithost" value="${commonCodeDetailInstance?.lastedithost}"/>
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: commonCodeDetailInstance, field: 'pkid', 'error')} required">
	<label class="control-label" for="pkid">
		<g:message code="commonCodeDetail.pkid.label" default="Pkid" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="pkid" type="number" value="${commonCodeDetailInstance.pkid}" required=""/>
	</div>
</div>
--}%


