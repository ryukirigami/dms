
<%@ page import="com.kombos.baseapp.CommonCodeDetail" %>
<grid:grid name='commonCodeDetailDatatables' 

		columns.refcommoncode.dataTables.sWidth='"8%"'
	
		columns.refid.dataTables.sWidth='"5%"'
	
		columns.bicode.dataTables.sWidth='"8%"'
	
		columns.description.dataTables.sWidth='"8%"'
	
		columns.parentcode.dataTables.sWidth='"8%"'
	
		columns.parentbicode.dataTables.sWidth='"8%"'
	
		columns.createddate.dataTables.sWidth='"5%"'
	
		columns.createdhost.dataTables.sWidth='"8%"'
	
		columns.lasteditby.dataTables.sWidth='"8%"'
	
		columns.lasteditdate.dataTables.sWidth='"5%"'
	
		columns.lastedithost.dataTables.sWidth='"8%"'
	
		columns.commoncode.dataTables.sWidth='"8%"'
	
		columns.pkid.dataTables.sWidth='"5%"'
	
		columns.sequence.dataTables.sWidth='"5%"'
	
		
></grid:grid>