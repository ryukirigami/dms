

<%@ page import="com.kombos.baseapp.CommonCodeDetail" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'commonCodeDetail.label', default: 'CommonCodeDetail')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteCommonCodeDetail;

$(function(){ 
	deleteCommonCodeDetail=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/commonCodeDetail/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadCommonCodeDetailTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-commonCodeDetail" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="commonCodeDetail"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${commonCodeDetailInstance?.refcommoncode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="refcommoncode-label" class="property-label"><g:message
					code="commonCodeDetail.refcommoncode.label" default="Refcommoncode" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="refcommoncode-label">
						<ba:editableValue
								bean="${commonCodeDetailInstance}" field="refcommoncode"
								url="${request.contextPath}/CommonCodeDetail/updatefield" type="text"
								title="Enter refcommoncode" onsuccess="reloadCommonCodeDetailTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeDetailInstance?.refid}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="refid-label" class="property-label"><g:message
					code="commonCodeDetail.refid.label" default="Refid" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="refid-label">
						<ba:editableValue
								bean="${commonCodeDetailInstance}" field="refid"
								url="${request.contextPath}/CommonCodeDetail/updatefield" type="text"
								title="Enter refid" onsuccess="reloadCommonCodeDetailTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeDetailInstance?.bicode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="bicode-label" class="property-label"><g:message
					code="commonCodeDetail.bicode.label" default="Bicode" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="bicode-label">
						<ba:editableValue
								bean="${commonCodeDetailInstance}" field="bicode"
								url="${request.contextPath}/CommonCodeDetail/updatefield" type="text"
								title="Enter bicode" onsuccess="reloadCommonCodeDetailTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeDetailInstance?.description}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="description-label" class="property-label"><g:message
					code="commonCodeDetail.description.label" default="Description" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="description-label">
						<ba:editableValue
								bean="${commonCodeDetailInstance}" field="description"
								url="${request.contextPath}/CommonCodeDetail/updatefield" type="text"
								title="Enter description" onsuccess="reloadCommonCodeDetailTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeDetailInstance?.parentcode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="parentcode-label" class="property-label"><g:message
					code="commonCodeDetail.parentcode.label" default="Parentcode" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="parentcode-label">
						<ba:editableValue
								bean="${commonCodeDetailInstance}" field="parentcode"
								url="${request.contextPath}/CommonCodeDetail/updatefield" type="text"
								title="Enter parentcode" onsuccess="reloadCommonCodeDetailTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeDetailInstance?.parentbicode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="parentbicode-label" class="property-label"><g:message
					code="commonCodeDetail.parentbicode.label" default="Parentbicode" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="parentbicode-label">
						<ba:editableValue
								bean="${commonCodeDetailInstance}" field="parentbicode"
								url="${request.contextPath}/CommonCodeDetail/updatefield" type="text"
								title="Enter parentbicode" onsuccess="reloadCommonCodeDetailTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeDetailInstance?.createddate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createddate-label" class="property-label"><g:message
					code="commonCodeDetail.createddate.label" default="Createddate" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createddate-label">
						<ba:editableValue
								bean="${commonCodeDetailInstance}" field="createddate"
								url="${request.contextPath}/CommonCodeDetail/updatefield" type="text"
								title="Enter createddate" onsuccess="reloadCommonCodeDetailTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeDetailInstance?.createdhost}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdhost-label" class="property-label"><g:message
					code="commonCodeDetail.createdhost.label" default="Createdhost" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdhost-label">
						<ba:editableValue
								bean="${commonCodeDetailInstance}" field="createdhost"
								url="${request.contextPath}/CommonCodeDetail/updatefield" type="text"
								title="Enter createdhost" onsuccess="reloadCommonCodeDetailTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeDetailInstance?.lasteditby}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lasteditby-label" class="property-label"><g:message
					code="commonCodeDetail.lasteditby.label" default="Lasteditby" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lasteditby-label">
						<ba:editableValue
								bean="${commonCodeDetailInstance}" field="lasteditby"
								url="${request.contextPath}/CommonCodeDetail/updatefield" type="text"
								title="Enter lasteditby" onsuccess="reloadCommonCodeDetailTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeDetailInstance?.lasteditdate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lasteditdate-label" class="property-label"><g:message
					code="commonCodeDetail.lasteditdate.label" default="Lasteditdate" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lasteditdate-label">
						<ba:editableValue
								bean="${commonCodeDetailInstance}" field="lasteditdate"
								url="${request.contextPath}/CommonCodeDetail/updatefield" type="text"
								title="Enter lasteditdate" onsuccess="reloadCommonCodeDetailTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeDetailInstance?.lastedithost}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastedithost-label" class="property-label"><g:message
					code="commonCodeDetail.lastedithost.label" default="Lastedithost" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastedithost-label">
						<ba:editableValue
								bean="${commonCodeDetailInstance}" field="lastedithost"
								url="${request.contextPath}/CommonCodeDetail/updatefield" type="text"
								title="Enter lastedithost" onsuccess="reloadCommonCodeDetailTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeDetailInstance?.commoncode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="commoncode-label" class="property-label"><g:message
					code="commonCodeDetail.commoncode.label" default="Commoncode" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="commoncode-label">
						<ba:editableValue
								bean="${commonCodeDetailInstance}" field="commoncode"
								url="${request.contextPath}/CommonCodeDetail/updatefield" type="text"
								title="Enter commoncode" onsuccess="reloadCommonCodeDetailTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeDetailInstance?.pkid}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="pkid-label" class="property-label"><g:message
					code="commonCodeDetail.pkid.label" default="Pkid" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="pkid-label">
						<ba:editableValue
								bean="${commonCodeDetailInstance}" field="pkid"
								url="${request.contextPath}/CommonCodeDetail/updatefield" type="text"
								title="Enter pkid" onsuccess="reloadCommonCodeDetailTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeDetailInstance?.sequence}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="sequence-label" class="property-label"><g:message
					code="commonCodeDetail.sequence.label" default="Sequence" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="sequence-label">
						<ba:editableValue
								bean="${commonCodeDetailInstance}" field="sequence"
								url="${request.contextPath}/CommonCodeDetail/updatefield" type="text"
								title="Enter sequence" onsuccess="reloadCommonCodeDetailTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${commonCodeDetailInstance?.pkid}"
					update="[success:'commonCodeDetail-form',failure:'commonCodeDetail-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteCommonCodeDetail('${commonCodeDetailInstance?.pkid}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
