
<%@ page import="com.kombos.baseapp.CommonCodeDetail" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'commonCodeDetail.label', default: 'CommonCodeDetail')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
        %{--<r:require modules="baseapp"/>--}%
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/commonCodeDetail/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/commonCodeDetail/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    <%
        if(params.commoncode){
    %>
    backToParentCommonCode = function(){
        $('#main-content').empty();
        $('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/commonCode/list?codetype=${params.codetype}&commoncode=${params.commoncode}',type: "POST",dataType: "html",
        	complete : function (req, err) {
            	$('#main-content').append(req.responseText);
            	 $('#spinner').fadeOut();
           	}
        });
    }
    <%}%>


    <%
        if(params.parentcode){
            CommonCodeDetail commonCodeDetail = CommonCodeDetail.findByBicode(params.parentcode)
            if(commonCodeDetail.parentbicode){
    %>
    backToParentCommonCodeDetail = function(){
        $('#main-content').empty();
        $('#spinner').fadeIn(1);

        $.ajax({url: '${request.contextPath}/commonCodeDetail/list?parentcode=${commonCodeDetail.parentbicode}',type: "POST",dataType: "html",
        	complete : function (req, err) {
            	$('#main-content').append(req.responseText);
            	 $('#spinner').fadeOut();
           	}
        });
    }
    <%
            }else{
    %>
    backToParentCommonCodeDetail = function(){
           $('#main-content').empty();
           $('#spinner').fadeIn(1);

           $.ajax({url: '${request.contextPath}/commonCodeDetail/list?commoncode=${commonCodeDetail.commoncode}',type: "POST",dataType: "html",
        	complete : function (req, err) {
            	$('#main-content').append(req.responseText);
            	 $('#spinner').fadeOut();
           	}
        });
    }
    <%
            }
        }
    %>
    showChildren = function(id){
        $('#main-content').empty();
        $('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/commonCodeDetail/list?parentcode='+id,type: "POST",dataType: "html",
        	complete : function (req, err) {
            	$('#main-content').append(req.responseText);
            	 $('#spinner').fadeOut();
           	}
        });
    }

    loadForm = function(data, textStatus){
		$('#commonCodeDetail-form').empty();
    	$('#commonCodeDetail-form').append(data);
        <%if(params.commoncode){%>
        jQuery('#commoncode').val('${params.commoncode}');
//        jQuery('#parentcode').val('${params.commoncode}');
//        jQuery('#parentbicode_div').hide();
        jQuery('#commoncode').hide();
//        jQuery('#parentcode').hide();
        jQuery('#commoncode_label').html('<label class="control-label" >${params.commoncode}</label>');
        jQuery('#parentcode_label').html('<label class="control-label" >${params.commoncode}</label>');
        <%}%>
        <%
        if(params.parentcode){
        CommonCodeDetail commonCodeDetail = CommonCodeDetail.findByBicode(params.parentcode)
        %>
        jQuery('#commoncode').val('${commonCodeDetail.commoncode}');
        jQuery('#parentcode').val('${commonCodeDetail.commoncode}');
        jQuery('#parentbicode').val('${commonCodeDetail.bicode}');
        jQuery('#commoncode').hide();
        jQuery('#parentcode').hide();
        jQuery('#parentbicode').hide();
        jQuery('#commoncode_label').html('<label class="control-label" >${commonCodeDetail.commoncode}</label>');
        jQuery('#parentcode_label').html('<label class="control-label" >${commonCodeDetail.commoncode}</label>');
        jQuery('#parentbicode_label').html('<label class="control-label" >${commonCodeDetail.bicode}</label>');
        <%}%>
   	}
    
    shrinkTableLayout = function(){
    	if($("#commonCodeDetail-table").hasClass("span12")){
   			$("#commonCodeDetail-table").toggleClass("span12 span5");
        }
        $("#commonCodeDetail-form").css("display","block");
       	shrinkCommonCodeDetailTable(); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#commonCodeDetail-table").hasClass("span5")){
   			$("#commonCodeDetail-table").toggleClass("span5 span12");
   		}
        $("#commonCodeDetail-form").css("display","none");
       	expandCommonCodeDetailTable(); 
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#commonCodeDetail-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/commonCodeDetail/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadCommonCodeDetailTable();
    		}
		});
		
   	}

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
        </br>
        <%if(params.commoncode){%>
        <span class="pull-left"><g:message code="default.backto.label" default="Back To"/>&nbsp;<a href="javascript:backToParentCommonCode()"><%=params.commoncode%></a></span>
        <%}%>
        <%if(params.parentcode){%>
        <span class="pull-left"><g:message code="default.backto.label" default="Back To"/>&nbsp;<a href="javascript:backToParentCommonCodeDetail()"><%=params.parentcode%></a></span>
        <%}%>
        <ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-remove"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			<li class="separator"></li>
		</ul>
	</div>

	%{--<div class="box">--}%
		%{--<div class="span12" id="commonCodeDetail-table">--}%
			%{--<g:if test="${flash.message}">--}%
				%{--<div class="message" role="status">--}%
					%{--${flash.message}--}%
				%{--</div>--}%
			%{--</g:if>--}%

			%{--<g:render template="dataTables" />--}%
		%{--</div>--}%
		%{--<div class="span7" id="commonCodeDetail-form" style="display: none;"></div>--}%
	%{--</div>--}%

    <div class="box">
    <div id="commonCodeDetail-table" class="span12">
        <g:render template="../menu/maxLineDisplay"/>
    <script type="text/javascript">
        var shrinkCommonCodeDetailTable;
        var reloadCommonCodeDetailTable;
        jQuery(function () {

            var oTable = $('#commonCodeDetailDatatables_datatable').dataTable({


                "sDom": "<'row-fluid'r>t<'row-fluid'<'span6'i><p>>",
                bFilter: false,
                "bStateSave": false,
                'sPaginationType': 'bootstrap',
                "fnInitComplete": function () {


                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                },
                "bSort": true,
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                    $('.search_init').each(function(idx){
                        aoData.push( {"name": "sSearch_"+(idx+1), "value": this.value} );
                    });

                    $.ajax({ "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData ,
                        "success": function (json) {
                            fnCallback(json);
                        }
                    });
                },
                <%if(params.commoncode){%>
                "sAjaxSource": "${request.getContextPath()}/commonCodeDetail/getList?commoncode=${params.commoncode}",
                <%}%>
                <%if(params.parentcode){%>
                "sAjaxSource": "${request.getContextPath()}/commonCodeDetail/getList?parentcode=${params.parentcode}",
                <%}%>
                "aoColumnDefs": [

                    {   "sName": "id",
                        "aTargets": [0],
                        "bSearchable": false,
                        "mRender": function ( data, type, row ) {
                            return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row[0]+'" title="Select this"><input type="hidden" value="'+row[0]+'">&nbsp;&nbsp;<a href="#" onclick="show('+"'"+row[0]+"'"+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+"'"+row[0]+"'"+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
                        },
                        "sWidth": "",
                        "bSortable": true,
                        "sWidth":'100%',
                        "sClass":'',
                        "bVisible": false
                    }  ,

                    {   "sName": "pkid",
                        "aTargets": [1],
                        "bSearchable": false,
                        "mRender": function ( data, type, row ) {
//                            return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row[0]+'" title="Select this"><input type="hidden" value="'+row[0]+'">&nbsp;&nbsp;<a href="#" onclick="show('+"'"+row[0]+"'"+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+"'"+row[0]+"'"+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="showChildren('+"'"+row[3]+"'"+');">&nbsp;&nbsp;Show Children&nbsp;&nbsp;</a>';
                            return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row[0]+'" title="Select this"><input type="hidden" value="'+row[0]+'">&nbsp;&nbsp;<a href="#" onclick="show('+"'"+row[0]+"'"+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+"'"+row[0]+"'"+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
                        },
                        "sWidth": "30%",
                        "bSortable": true,
                        "sClass":'',
                        "bVisible": true
                    }  ,
                    {   "sName": "commoncode",
                        "aTargets": [2],
                        "bSearchable": true,
                        "sWidth": "",
                        "bSortable": true,
                        "sWidth":"8%",
                        "sClass":'',
                        "bVisible": true
                    }  ,
                    {   "sName": "bicode",
                        "aTargets": [3],
                        "bSearchable": true,
                        "sWidth": "",
                        "bSortable": true,
                        "sWidth":"8%",
                        "sClass":'',
                        "bVisible": true
                    }  ,

                    {   "sName": "description",
                        "aTargets": [4],
                        "bSearchable": true,
                        "sWidth": "",
                        "bSortable": true,
                        "sWidth":"8%",
                        "sClass":'',
                        "bVisible": true
                    }  ,

                    {   "sName": "parentcode",
                        "aTargets": [5],
                        "bSearchable": true,
                        "sWidth": "",
                        "bSortable": true,
                        "sWidth":"8%",
                        "sClass":'',
                        "bVisible": true
                    }  ,

                    {   "sName": "parentbicode",
                        "aTargets": [6],
                        "bSearchable": true,
                        "sWidth": "",
                        "bSortable": true,
                        "sWidth":"8%",
                        "sClass":'',
                        "bVisible": true
                    }  ,

                    {   "sName": "sequence",
                        "aTargets": [7],
                        "bSearchable": true,
                        "sWidth": "",
                        "bSortable": true,
                        "sWidth":"5%",
                        "sClass":'',
                        "bVisible": true
                    }
                ]

            });

            $("#commonCodeDetailDatatables-filters tbody")
                    .append("&lt;tr&gt;&lt;td align=\"center\"&gt;app.commonCodeDetail.pkid.label&amp;nbsp;&amp;nbsp;&lt;/td&gt;&lt;td align=\"center\" id=\"filter_pkid\"&gt; &lt;input type=\"text\" name=\"search_pkid\" class=\"search_init\" size=\"10\"/&gt;&lt;/td&gt;&lt;/tr&gt;")
                    .append("&lt;tr&gt;&lt;td align=\"center\"&gt;app.commonCodeDetail.commoncode.label&amp;nbsp;&amp;nbsp;&lt;/td&gt;&lt;td align=\"center\" id=\"filter_commoncode\"&gt; &lt;input type=\"text\" name=\"search_commoncode\" class=\"search_init\" size=\"10\"/&gt;&lt;/td&gt;&lt;/tr&gt;")
                    .append("&lt;tr&gt;&lt;td align=\"center\"&gt;app.commonCodeDetail.bicode.label&amp;nbsp;&amp;nbsp;&lt;/td&gt;&lt;td align=\"center\" id=\"filter_bicode\"&gt; &lt;input type=\"text\" name=\"search_bicode\" class=\"search_init\" size=\"10\"/&gt;&lt;/td&gt;&lt;/tr&gt;")
                    .append("&lt;tr&gt;&lt;td align=\"center\"&gt;app.commonCodeDetail.description.label&amp;nbsp;&amp;nbsp;&lt;/td&gt;&lt;td align=\"center\" id=\"filter_description\"&gt; &lt;input type=\"text\" name=\"search_description\" class=\"search_init\" size=\"10\"/&gt;&lt;/td&gt;&lt;/tr&gt;")
                    .append("&lt;tr&gt;&lt;td align=\"center\"&gt;app.commonCodeDetail.parentcode.label&amp;nbsp;&amp;nbsp;&lt;/td&gt;&lt;td align=\"center\" id=\"filter_parentcode\"&gt; &lt;input type=\"text\" name=\"search_parentcode\" class=\"search_init\" size=\"10\"/&gt;&lt;/td&gt;&lt;/tr&gt;")
                    .append("&lt;tr&gt;&lt;td align=\"center\"&gt;app.commonCodeDetail.parentbicode.label&amp;nbsp;&amp;nbsp;&lt;/td&gt;&lt;td align=\"center\" id=\"filter_parentbicode\"&gt; &lt;input type=\"text\" name=\"search_parentbicode\" class=\"search_init\" size=\"10\"/&gt;&lt;/td&gt;&lt;/tr&gt;")
                    .append("&lt;tr&gt;&lt;td align=\"center\"&gt;app.commonCodeDetail.sequence.label&amp;nbsp;&amp;nbsp;&lt;/td&gt;&lt;td align=\"center\" id=\"filter_sequence\"&gt; &lt;input type=\"text\" name=\"search_sequence\" class=\"search_init\" size=\"10\"/&gt;&lt;/td&gt;&lt;/tr&gt;")
            ;

            $("#filter_refcommoncode input").bind('keypress', function(e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                    oTable.fnFilter(this.value, 0);
                }


            });
            $("#filter_refcommoncode input").click(function (e) {
                e.stopPropagation();
            });
            $("#filter_refcommoncode input").keypress(function (e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) { //Enter keycode
                    oTable.fnDraw();
                    e.stopPropagation();
                }
            });


            $("#filter_refid input").bind('keypress', function(e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                    oTable.fnFilter(this.value, 1);
                }


            });
            $("#filter_refid input").click(function (e) {
                e.stopPropagation();
            });
            $("#filter_refid input").keypress(function (e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) { //Enter keycode
                    oTable.fnDraw();
                    e.stopPropagation();
                }
            });


            $("#filter_bicode input").bind('keypress', function(e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                    oTable.fnFilter(this.value, 2);
                }


            });
            $("#filter_bicode input").click(function (e) {
                e.stopPropagation();
            });
            $("#filter_bicode input").keypress(function (e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) { //Enter keycode
                    oTable.fnDraw();
                    e.stopPropagation();
                }
            });


            $("#filter_description input").bind('keypress', function(e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                    oTable.fnFilter(this.value, 3);
                }


            });
            $("#filter_description input").click(function (e) {
                e.stopPropagation();
            });
            $("#filter_description input").keypress(function (e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) { //Enter keycode
                    oTable.fnDraw();
                    e.stopPropagation();
                }
            });


            $("#filter_parentcode input").bind('keypress', function(e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                    oTable.fnFilter(this.value, 4);
                }


            });
            $("#filter_parentcode input").click(function (e) {
                e.stopPropagation();
            });
            $("#filter_parentcode input").keypress(function (e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) { //Enter keycode
                    oTable.fnDraw();
                    e.stopPropagation();
                }
            });


            $("#filter_parentbicode input").bind('keypress', function(e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                    oTable.fnFilter(this.value, 5);
                }


            });
            $("#filter_parentbicode input").click(function (e) {
                e.stopPropagation();
            });
            $("#filter_parentbicode input").keypress(function (e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) { //Enter keycode
                    oTable.fnDraw();
                    e.stopPropagation();
                }
            });


            $("#filter_createddate input").bind('keypress', function(e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                    oTable.fnFilter(this.value, 6);
                }


            });
            $("#filter_createddate input").click(function (e) {
                e.stopPropagation();
            });



            $("#filter_createdhost input").bind('keypress', function(e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                    oTable.fnFilter(this.value, 7);
                }


            });
            $("#filter_createdhost input").click(function (e) {
                e.stopPropagation();
            });



            $("#filter_lasteditby input").bind('keypress', function(e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                    oTable.fnFilter(this.value, 8);
                }


            });
            $("#filter_lasteditby input").click(function (e) {
                e.stopPropagation();
            });



            $("#filter_lasteditdate input").bind('keypress', function(e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                    oTable.fnFilter(this.value, 9);
                }


            });
            $("#filter_lasteditdate input").click(function (e) {
                e.stopPropagation();
            });



            $("#filter_lastedithost input").bind('keypress', function(e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                    oTable.fnFilter(this.value, 10);
                }


            });
            $("#filter_lastedithost input").click(function (e) {
                e.stopPropagation();
            });



            $("#filter_commoncode input").bind('keypress', function(e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                    oTable.fnFilter(this.value, 11);
                }


            });
            $("#filter_commoncode input").click(function (e) {
                e.stopPropagation();
            });
            $("#filter_commoncode input").keypress(function (e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) { //Enter keycode
                    oTable.fnDraw();
                    e.stopPropagation();
                }
            });


            $("#filter_pkid input").bind('keypress', function(e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                    oTable.fnFilter(this.value, 12);
                }


            });
            $("#filter_pkid input").click(function (e) {
                e.stopPropagation();
            });
            $("#filter_pkid input").keypress(function (e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) { //Enter keycode
                    oTable.fnDraw();
                    e.stopPropagation();
                }
            });


            $("#filter_sequence input").bind('keypress', function(e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                    oTable.fnFilter(this.value, 13);
                }


            });
            $("#filter_sequence input").click(function (e) {
                e.stopPropagation();
            });
            $("#filter_sequence input").keypress(function (e) {
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) { //Enter keycode
                    oTable.fnDraw();
                    e.stopPropagation();
                }
            });


            shrinkCommonCodeDetailTable = function(){
                var oTable = $('#commonCodeDetailDatatables_datatable').dataTable();
                oTable.fnSetColumnVis( 3, false );
                oTable.fnSetColumnVis( 4, false );
                oTable.fnSetColumnVis( 5, false );
                oTable.fnSetColumnVis( 6, false );
                oTable.fnSetColumnVis( 7, false );
            }

            expandCommonCodeDetailTable = function(){
                var oTable = $('#commonCodeDetailDatatables_datatable').dataTable();
                oTable.fnSetColumnVis( 3, true );
                oTable.fnSetColumnVis( 4, true );
                oTable.fnSetColumnVis( 5, true );
                oTable.fnSetColumnVis( 6, true );
                oTable.fnSetColumnVis( 7, true );
            }

            reloadCommonCodeDetailTable = function(){
                var oTable = $('#commonCodeDetailDatatables_datatable').dataTable();
                oTable.fnReloadAjax();
            }

            $('#commonCodeDetailDatatables_datatable th .select-all').click(function(e) {
                var tc = $('#commonCodeDetailDatatables_datatable tbody .row-select').attr('checked', this.checked);

                if(this.checked)
                    tc.parent().parent().addClass('row_selected');
                else
                    tc.parent().parent().removeClass('row_selected');
                e.stopPropagation();
            });

            $('#commonCodeDetailDatatables_datatable tbody tr .row-select').live('click', function (e) {
                if(this.checked)
                    $(this).parent().parent().addClass('row_selected');
                else
                    $(this).parent().parent().removeClass('row_selected');
                e.stopPropagation();

            } );

            $('#commonCodeDetailDatatables_datatable tbody tr a').live('click', function (e) {
                e.stopPropagation();
            } );

            $('#commonCodeDetailDatatables_datatable tbody td').live('click', function(e) {
                if (e.target.tagName.toUpperCase() != "INPUT") {
                    var $tc = $(this).parent().find('input:checkbox'),
                            tv = $tc.attr('checked');
                    $tc.attr('checked', !tv);
                    $(this).parent().toggleClass('row_selected');
                }
            });
        });
    </script>

    <table id="commonCodeDetailDatatables_datatable" cellpadding="0" cellspacing="0"
        border="0"
        class="display table table-striped table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th>
                        <g:message code="app.commonCodeDetail.pkid.label" default="PKID"/>
                    </th>
                    <th>
                        <g:message code="app.commonCodeDetail.pkid.label" default="PKID"/>
                        <div id="filter_pkid" style="margin-top: 38px;">
                            <input type="text" name="search_pkid"
                                   class="search_init" />
                        </div>
                    </th>
                    <th>
                        <g:message code="app.commonCodeDetail.commoncode.label" default="Common Code"/>
                        <div id="filter_commoncode" style="margin-top: 38px;">
                            <input type="text" name="search_commoncode"
                                   class="search_init" />
                        </div>
                    </th>
                    <th>
                        <g:message code="app.commonCodeDetail.bicode.label" default="BI Code"/>
                        <div id="filter_bicode" style="margin-top: 38px;">
                            <input type="text" name="search_bicode"
                                   class="search_init" />
                        </div>
                    </th>
                    <th>
                        <g:message code="app.commonCodeDetail.description.label" default="Description"/>
                        <div id="filter_description" style="margin-top: 38px;">
                            <input type="text" name="search_description"
                                   class="search_init" />
                        </div>
                    </th>
                    <th>
                        <g:message code="app.commonCodeDetail.parentcode.label" default="Parent Code"/>
                        <div id="filter_parentcode" style="margin-top: 38px;">
                            <input type="text" name="search_parentcode"
                                   class="search_init" />
                        </div>
                    </th>
                    <th>
                        <g:message code="app.commonCodeDetail.parentbicode.label" default="Parent BI Code"/>
                        <div id="filter_parentbicode" style="margin-top: 38px;">
                            <input type="text" name="search_parentbicode"
                                   class="search_init" />
                        </div>
                    </th>
                    <th>
                        <g:message code="app.commonCodeDetail.sequence.label" default="Sequence"/>
                        <div id="filter_sequence" style="margin-top: 38px;">
                            <input type="text" name="search_sequence"
                                   class="search_init" />
                        </div>
                    </th>
                    %{--<th>--}%
                    %{--<g:message code="app.commonCodeDetail.refcommoncode.label" default="Ref Common Code"/>--}%
                    %{--</th>--}%
                    %{--<th>--}%
                    %{--<g:message code="app.commonCodeDetail.refid.label" default="Ref Id"/>--}%
                    %{--</th>--}%
                    %{--<th>--}%
                        %{--<g:message code="app.commonCodeDetail.createddate.label" default="Created Date"/>--}%
                    %{--</th>--}%
                    %{--<th>--}%
                        %{--<g:message code="app.commonCodeDetail.createdhost.label" default="Created Host"/>--}%
                    %{--</th>--}%
                    %{--<th>--}%
                        %{--<g:message code="app.commonCodeDetail.lasteditby.label" default="Last Edit By"/>--}%
                    %{--</th>--}%
                    %{--<th>--}%
                        %{--<g:message code="app.commonCodeDetail.lasteditdate.label" default="Last Edit Date"/>--}%
                    %{--</th>--}%
                    %{--<th>--}%
                        %{--<g:message code="app.commonCodeDetail.lastedithost.label" default="Last Edit Host"/>--}%
                    %{--</th>--}%
                </tr>
            </thead>
    </table>
    </div>
    <div class="span7" id="commonCodeDetail-form" style="display: none;"></div>
</div>
</body>
</html>
