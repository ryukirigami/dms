

<%@ page import="com.kombos.customerprofile.CustomerSurvey" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'customerSurvey.label', default: 'Customer Survey')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteCustomerSurvey;

$(function(){ 
	deleteCustomerSurvey=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/customerSurvey/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadCustomerSurveyTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-customerSurvey" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="customerSurvey"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${customerSurveyInstance?.t104Id}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t104Id-label" class="property-label"><g:message
					code="customerSurvey.t104Id.label" default="Id" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t104Id-label">
						%{--<ba:editableValue
								bean="${customerSurveyInstance}" field="t104Id"
								url="${request.contextPath}/CustomerSurvey/updatefield" type="text"
								title="Enter t104Id" onsuccess="reloadCustomerSurveyTable();" />--}%
							
								<g:fieldValue bean="${customerSurveyInstance}" field="t104Id"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${customerSurveyInstance?.t104Text}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t104Text-label" class="property-label"><g:message
                                code="customerSurvey.t104Text.label" default="Nama JD Power Survey" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t104Text-label">
                        %{--<ba:editableValue
                                bean="${customerSurveyInstance}" field="t104Text"
                                url="${request.contextPath}/CustomerSurvey/updatefield" type="text"
                                title="Enter t104Text" onsuccess="reloadCustomerSurveyTable();" />--}%

                        <g:fieldValue bean="${customerSurveyInstance}" field="t104Text"/>

                    </span></td>

                </tr>
            </g:if>

			
				%{--<g:if test="${customerSurveyInstance?.jenisSurvey}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="jenisSurvey-label" class="property-label"><g:message--}%
					%{--code="customerSurvey.jenisSurvey.label" default="Jenis Survey" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="jenisSurvey-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${customerSurveyInstance}" field="jenisSurvey"--}%
								%{--url="${request.contextPath}/CustomerSurvey/updatefield" type="text"--}%
								%{--title="Enter jenisSurvey" onsuccess="reloadCustomerSurveyTable();" />--}%
							%{----}%
								%{--<g:link controller="jenisSurvey" action="show" id="${customerSurveyInstance?.jenisSurvey?.id}">${customerSurveyInstance?.jenisSurvey?.encodeAsHTML()}</g:link>--}%
                                %{--<g:fieldValue bean="${customerSurveyInstance}" field="jenisSurvey"/>--}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				%{--<g:if test="${customerSurveyInstance?.customer}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="customer-label" class="property-label"><g:message--}%
					%{--code="customerSurvey.customer.label" default="Customer" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="customer-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${customerSurveyInstance}" field="customer"--}%
								%{--url="${request.contextPath}/CustomerSurvey/updatefield" type="text"--}%
								%{--title="Enter customer" onsuccess="reloadCustomerSurveyTable();" />--}%
							%{----}%
								%{--<g:link controller="customer" action="show" id="${customerSurveyInstance?.customer?.id}">${customerSurveyInstance?.customer?.encodeAsHTML()}</g:link>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				%{--<g:if test="${customerSurveyInstance?.historyCustomer}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="historyCustomer-label" class="property-label"><g:message--}%
					%{--code="customerSurvey.historyCustomer.label" default="History Customer" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="historyCustomer-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${customerSurveyInstance}" field="historyCustomer"--}%
								%{--url="${request.contextPath}/CustomerSurvey/updatefield" type="text"--}%
								%{--title="Enter historyCustomer" onsuccess="reloadCustomerSurveyTable();" />--}%
							%{----}%
								%{--<g:link controller="historyCustomer" action="show" id="${customerSurveyInstance?.historyCustomer?.id}">${customerSurveyInstance?.historyCustomer?.encodeAsHTML()}</g:link>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${customerSurveyInstance?.t104TglAwal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t104TglAwal-label" class="property-label"><g:message
					code="customerSurvey.t104TglAwal.label" default="Masa Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t104TglAwal-label">
						%{--<ba:editableValue
								bean="${customerSurveyInstance}" field="t104TglAwal"
								url="${request.contextPath}/CustomerSurvey/updatefield" type="text"
								title="Enter t104TglAwal" onsuccess="reloadCustomerSurveyTable();" />--}%
							
								<g:formatDate date="${customerSurveyInstance?.t104TglAwal}" />
                                s.d
                                <g:formatDate date="${customerSurveyInstance?.t104TglAkhir}" />
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${customerSurveyInstance?.t104TglAkhir}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="t104TglAkhir-label" class="property-label"><g:message--}%
					%{--code="customerSurvey.t104TglAkhir.label" default="Tgl Akhir" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="t104TglAkhir-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${customerSurveyInstance}" field="t104TglAkhir"--}%
								%{--url="${request.contextPath}/CustomerSurvey/updatefield" type="text"--}%
								%{--title="Enter t104TglAkhir" onsuccess="reloadCustomerSurveyTable();" />--}%
							%{----}%
								%{--<g:formatDate date="${customerSurveyInstance?.t104TglAkhir}" />--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			

			
				%{--<g:if test="${customerSurveyInstance?.t104xNamaUser}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="t104xNamaUser-label" class="property-label"><g:message--}%
					%{--code="customerSurvey.t104xNamaUser.label" default="T104x Nama User" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="t104xNamaUser-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${customerSurveyInstance}" field="t104xNamaUser"--}%
								%{--url="${request.contextPath}/CustomerSurvey/updatefield" type="text"--}%
								%{--title="Enter t104xNamaUser" onsuccess="reloadCustomerSurveyTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${customerSurveyInstance}" field="t104xNamaUser"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${customerSurveyInstance?.t104xNamaDivisi}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="t104xNamaDivisi-label" class="property-label"><g:message--}%
					%{--code="customerSurvey.t104xNamaDivisi.label" default="T104x Nama Divisi" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="t104xNamaDivisi-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${customerSurveyInstance}" field="t104xNamaDivisi"--}%
								%{--url="${request.contextPath}/CustomerSurvey/updatefield" type="text"--}%
								%{--title="Enter t104xNamaDivisi" onsuccess="reloadCustomerSurveyTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${customerSurveyInstance}" field="t104xNamaDivisi"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${customerSurveyInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="customerSurvey.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${customerSurveyInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/CustomerSurvey/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadCustomerSurveyTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${customerSurveyInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${customerSurveyInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="customerSurvey.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${customerSurveyInstance}" field="lastUpdProcess"
								url="${request.contextPath}/CustomerSurvey/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadCustomerSurveyTable();" />--}%
							
								<g:fieldValue bean="${customerSurveyInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${customerSurveyInstance?.customerVehicles}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="customerVehicles-label" class="property-label"><g:message--}%
					%{--code="customerSurvey.customerVehicles.label" default="Customer Vehicles" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="customerVehicles-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${customerSurveyInstance}" field="customerVehicles"--}%
								%{--url="${request.contextPath}/CustomerSurvey/updatefield" type="text"--}%
								%{--title="Enter customerVehicles" onsuccess="reloadCustomerSurveyTable();" />--}%
							%{----}%
								%{--<g:each in="${customerSurveyInstance.customerVehicles}" var="c">--}%
								%{--<g:link controller="customerVehicle" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link>--}%
								%{--</g:each>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${customerSurveyInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="customerSurvey.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${customerSurveyInstance}" field="dateCreated"
								url="${request.contextPath}/CustomerSurvey/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadCustomerSurveyTable();" />--}%
							
								<g:formatDate date="${customerSurveyInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${customerSurveyInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="customerSurvey.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${customerSurveyInstance}" field="createdBy"
                                url="${request.contextPath}/CustomerSurvey/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadCustomerSurveyTable();" />--}%

                        <g:fieldValue bean="${customerSurveyInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${customerSurveyInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="customerSurvey.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${customerSurveyInstance}" field="lastUpdated"
								url="${request.contextPath}/CustomerSurvey/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadCustomerSurveyTable();" />--}%
							
								<g:formatDate date="${customerSurveyInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${customerSurveyInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="customerSurvey.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${customerSurveyInstance}" field="updatedBy"
                                url="${request.contextPath}/CustomerSurvey/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadCustomerSurveyTable();" />--}%

                        <g:fieldValue bean="${customerSurveyInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${customerSurveyInstance?.id}"
					update="[success:'customerSurvey-form',failure:'customerSurvey-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteCustomerSurvey('${customerSurveyInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
