<%@ page import="com.kombos.customerprofile.CustomerSurvey" %>


<div class="control-group fieldcontain ${hasErrors(bean: customerSurveyInstance, field: 't104Text', 'error')} required">
    <label class="control-label" for="t104Text">
        <g:message code="customerSurvey.t104Text.label" default="Nama JD Power Survey" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textArea name="t104Text" required="" value="${customerSurveyInstance?.t104Text}"/>
    </div>
</div>
<g:javascript>
    var checkin = $('#t104TglAwal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
                checkin.hide();
                $('#t104TglAkhir')[0].focus();
            }).data('datepicker');

    var checkout = $('#t104TglAkhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout.hide();
            }).data('datepicker');

    document.getElementById("t104Text").focus();

</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: customerSurveyInstance, field: 't104TglAwal', 'error')} required">
	<label class="control-label" for="t104TglAwal">
		<g:message code="customerSurvey.t104TglAwal.label" default="Masa Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
    <ba:datePicker name="t104TglAwal" id="t104TglAwal" precision="day" format="dd/mm/yyyy" required="true" value="${customerSurveyInstance?.t104TglAwal}" /> s.d
    <ba:datePicker name="t104TglAkhir" id="t104TglAkhir" precision="day" format="dd/mm/yyyy" required="true" value="${customerSurveyInstance?.t104TglAkhir}" />
    </div>
</div>
