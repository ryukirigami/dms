
<%@ page import="com.kombos.customerprofile.CustomerSurvey" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="customerSurvey_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="customerSurvey.t104Id.label" default="Id" /></div>--}%
			%{--</th>--}%

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="customerSurvey.t104Text.label" default="Nama JD Power Survey" /></div>
        </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="customerSurvey.t104TglAwal.label" default="Tanggal Awal" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="customerSurvey.t104TglAkhir.label" default="Tanggal Akhir" /></div>
            </th>

            %{--<th style="border-bottom: none;padding: 5px;">--}%
            %{--<div><g:message code="customerSurvey.jenisSurvey.label" default="Jenis Survey" /></div>--}%
            %{--</th>--}%


            %{--<th style="border-bottom: none;padding: 5px;">--}%
            %{--<div><g:message code="customerSurvey.customer.label" default="Customer" /></div>--}%
            %{--</th>--}%


            %{--<th style="border-bottom: none;padding: 5px;">--}%
            %{--<div><g:message code="customerSurvey.historyCustomer.label" default="History Customer" /></div>--}%
            %{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="customerSurvey.t104xNamaUser.label" default="T104x Nama User" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="customerSurvey.t104xNamaDivisi.label" default="T104x Nama Divisi" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="customerSurvey.staDel.label" default="Sta Del" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="customerSurvey.createdBy.label" default="Created By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="customerSurvey.updatedBy.label" default="Updated By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="customerSurvey.lastUpdProcess.label" default="Last Upd Process" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="customerSurvey.customerVehicles.label" default="Customer Vehicles" /></div>--}%
			%{--</th>--}%


		</tr>
		<tr>


			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_t104Id" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_t104Id" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t104Text" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t104Text" class="search_init" />
                </div>
            </th>

            %{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_jenisSurvey" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_jenisSurvey" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%

			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_customer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_customer" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_historyCustomer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_historyCustomer" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t104TglAwal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t104TglAwal" value="date.struct">
					<input type="hidden" name="search_t104TglAwal_day" id="search_t104TglAwal_day" value="">
					<input type="hidden" name="search_t104TglAwal_month" id="search_t104TglAwal_month" value="">
					<input type="hidden" name="search_t104TglAwal_year" id="search_t104TglAwal_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t104TglAwal_dp" value="" id="search_t104TglAwal" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t104TglAkhir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t104TglAkhir" value="date.struct">
					<input type="hidden" name="search_t104TglAkhir_day" id="search_t104TglAkhir_day" value="">
					<input type="hidden" name="search_t104TglAkhir_month" id="search_t104TglAkhir_month" value="">
					<input type="hidden" name="search_t104TglAkhir_year" id="search_t104TglAkhir_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t104TglAkhir_dp" value="" id="search_t104TglAkhir" class="search_init">
				</div>
			</th>


			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_t104xNamaUser" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_t104xNamaUser" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_t104xNamaDivisi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_t104xNamaDivisi" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_staDel" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%

			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_createdBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_updatedBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_lastUpdProcess" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_customerVehicles" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_customerVehicles" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%


		</tr>
	</thead>
</table>

<g:javascript>
var customerSurveyTable;
var reloadCustomerSurveyTable;
$(function(){

	reloadCustomerSurveyTable = function() {
		customerSurveyTable.fnDraw();
	}

	var recordsCustomerSurveyPerPage = [];//new Array();
    var anCustomerSurveySelected;
    var jmlRecCustomerSurveyPerPage=0;
    var id;


	$('#search_t104TglAwal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t104TglAwal_day').val(newDate.getDate());
			$('#search_t104TglAwal_month').val(newDate.getMonth()+1);
			$('#search_t104TglAwal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			customerSurveyTable.fnDraw();
	});



	$('#search_t104TglAkhir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t104TglAkhir_day').val(newDate.getDate());
			$('#search_t104TglAkhir_month').val(newDate.getMonth()+1);
			$('#search_t104TglAkhir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			customerSurveyTable.fnDraw();
	});




$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	customerSurveyTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	customerSurveyTable = $('#customerSurvey_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsCustomerSurvey = $("#customerSurvey_datatables tbody .row-select");
            var jmlCustomerSurveyCek = 0;
            var nRow;
            var idRec;
            rsCustomerSurvey.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsCustomerSurveyPerPage[idRec]=="1"){
                    jmlCustomerSurveyCek = jmlCustomerSurveyCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsCustomerSurveyPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecCustomerSurveyPerPage = rsCustomerSurvey.length;
            if(jmlCustomerSurveyCek==jmlRecCustomerSurveyPerPage && jmlRecCustomerSurveyPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

//{
//	"sName": "t104Id",
//	"mDataProp": "t104Id",
//	"aTargets": [0],
//
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"300px",
//	"bVisible": true
//}
//
//,


{
	"sName": "t104Text",
	"mDataProp": "t104Text",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
//
//{
//	"sName": "jenisSurvey",
//	"mDataProp": "jenisSurvey",
//	"aTargets": [1],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "customer",
//	"mDataProp": "customer",
//	"aTargets": [2],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "historyCustomer",
//	"mDataProp": "historyCustomer",
//	"aTargets": [3],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}



{
	"sName": "t104TglAwal",
	"mDataProp": "t104TglAwal",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t104TglAkhir",
	"mDataProp": "t104TglAkhir",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

//,
//
//{
//	"sName": "t104xNamaUser",
//	"mDataProp": "t104xNamaUser",
//	"aTargets": [7],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "t104xNamaDivisi",
//	"mDataProp": "t104xNamaDivisi",
//	"aTargets": [8],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "staDel",
//	"mDataProp": "staDel",
//	"aTargets": [9],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "createdBy",
//	"mDataProp": "createdBy",
//	"aTargets": [10],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "updatedBy",
//	"mDataProp": "updatedBy",
//	"aTargets": [11],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "lastUpdProcess",
//	"mDataProp": "lastUpdProcess",
//	"aTargets": [12],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "customerVehicles",
//	"mDataProp": "customerVehicles",
//	"aTargets": [13],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var t104Id = $('#filter_t104Id input').val();
						if(t104Id){
							aoData.push(
									{"name": 'sCriteria_t104Id', "value": t104Id}
							);
						}

//						var jenisSurvey = $('#filter_jenisSurvey input').val();
//						if(jenisSurvey){
//							aoData.push(
//									{"name": 'sCriteria_jenisSurvey', "value": jenisSurvey}
//							);
//						}

						var customer = $('#filter_customer input').val();
						if(customer){
							aoData.push(
									{"name": 'sCriteria_customer', "value": customer}
							);
						}

						var historyCustomer = $('#filter_historyCustomer input').val();
						if(historyCustomer){
							aoData.push(
									{"name": 'sCriteria_historyCustomer', "value": historyCustomer}
							);
						}

						var t104TglAwal = $('#search_t104TglAwal').val();
						var t104TglAwalDay = $('#search_t104TglAwal_day').val();
						var t104TglAwalMonth = $('#search_t104TglAwal_month').val();
						var t104TglAwalYear = $('#search_t104TglAwal_year').val();

						if(t104TglAwal){
							aoData.push(
									{"name": 'sCriteria_t104TglAwal', "value": "date.struct"},
									{"name": 'sCriteria_t104TglAwal_dp', "value": t104TglAwal},
									{"name": 'sCriteria_t104TglAwal_day', "value": t104TglAwalDay},
									{"name": 'sCriteria_t104TglAwal_month', "value": t104TglAwalMonth},
									{"name": 'sCriteria_t104TglAwal_year', "value": t104TglAwalYear}
							);
						}

						var t104TglAkhir = $('#search_t104TglAkhir').val();
						var t104TglAkhirDay = $('#search_t104TglAkhir_day').val();
						var t104TglAkhirMonth = $('#search_t104TglAkhir_month').val();
						var t104TglAkhirYear = $('#search_t104TglAkhir_year').val();

						if(t104TglAkhir){
							aoData.push(
									{"name": 'sCriteria_t104TglAkhir', "value": "date.struct"},
									{"name": 'sCriteria_t104TglAkhir_dp', "value": t104TglAkhir},
									{"name": 'sCriteria_t104TglAkhir_day', "value": t104TglAkhirDay},
									{"name": 'sCriteria_t104TglAkhir_month', "value": t104TglAkhirMonth},
									{"name": 'sCriteria_t104TglAkhir_year', "value": t104TglAkhirYear}
							);
						}

						var t104Text = $('#filter_t104Text input').val();
						if(t104Text){
							aoData.push(
									{"name": 'sCriteria_t104Text', "value": t104Text}
							);
						}

						var t104xNamaUser = $('#filter_t104xNamaUser input').val();
						if(t104xNamaUser){
							aoData.push(
									{"name": 'sCriteria_t104xNamaUser', "value": t104xNamaUser}
							);
						}

						var t104xNamaDivisi = $('#filter_t104xNamaDivisi input').val();
						if(t104xNamaDivisi){
							aoData.push(
									{"name": 'sCriteria_t104xNamaDivisi', "value": t104xNamaDivisi}
							);
						}

						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}

						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}

						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						var customerVehicles = $('#filter_customerVehicles input').val();
						if(customerVehicles){
							aoData.push(
									{"name": 'sCriteria_customerVehicles', "value": customerVehicles}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
		});
		$('.select-all').click(function(e) {

        $("#customerSurvey_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsCustomerSurveyPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsCustomerSurveyPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#customerSurvey_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsCustomerSurveyPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anCustomerSurveySelected = customerSurveyTable.$('tr.row_selected');
            if(jmlRecCustomerSurveyPerPage == anCustomerSurveySelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsCustomerSurveyPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>



