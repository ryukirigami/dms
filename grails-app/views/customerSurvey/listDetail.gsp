
<%@ page import="com.kombos.administrasi.BaseModel; com.kombos.customerprofile.CustomerVehicle;com.kombos.administrasi.ModelName;" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'customerVehicle.label', default: 'Customer Vehicle')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />

    <style>
    #spinner {
        z-index: 1000;
    }

    legend {
        display: block;
        width: 100%;
        padding: 0;
        /* margin-bottom: 20px; */
        font-size: 21px;
        line-height: 20px;
        color: #333333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    td {
        padding-bottom: 10px;
        padding-left: 5px;
        font-size: x-small;
    }
    </style>
<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var setJDPower;
	var getCustomerSurveyInfo;

	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});
        $('#view').click(function(e){
            reloadCustomerVehicleTable();
        });
        $('#clear').click(function(e){
            $('#kata_kunci').val("");
            $('#kriteria').val("");
        });
    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/customerVehicle/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/customerVehicle/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#customerVehicle-form').empty();
    	$('#customerVehicle-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#customerVehicle-table").hasClass("span12")){
   			$("#customerVehicle-table").toggleClass("span12 span5");
        }
        $("#customerVehicle-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#customerVehicle-table").hasClass("span5")){
   			$("#customerVehicle-table").toggleClass("span5 span12");
   		}
        $("#customerVehicle-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#customerVehicle-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/customerVehicle/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadCustomerVehicleTable();
    		}
		});
		
   	}

   	setJDPower = function() {
   		var data = [];
		$("#customerVehicle-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			data.push(id);
    		}
		});

		var json = JSON.stringify(data);
        var id = $("#customerSurvey").val();


		$.ajax({
    		url:'${request.contextPath}/customerSurvey/setJDPower',
    		type: "POST", // Always use POST when deleting data
    		data: {
    		    id:id,
    		    ids: json
    		},
    		success:function(data, status) {
                toastr.success("Sukses : " + data.suksesSave + ", Gagal :  " + data.gagalSave);
    		},
    		complete: function(xhr, status) {
                reloadCustomerVehicleTable();
    		}
		});
   	}

    getCustomerSurveyInfo = function(){
        var customerSurveyId=document.getElementById('customerSurvey').value
        $.ajax({
    		url:'${request.contextPath}/customerSurvey/getCustomerSurveyInfo',
    		type: "POST", // Always use POST when deleting data
    		data: {customerSurvey:customerSurveyId},
    		success: function(data, status) {
                $('#t104TglAwal').val(data.tglAwal);
                $('#t104TglAkhir').val(data.tglAkhir);
    		}
		});

    }
});
</g:javascript>

        <g:set var="koloms" value="${[
                [id: "fullNoPol", nama: "Nomor Polisi"],
                [id: "model", nama: "Model Kendaraan"],
                [id: "t103VinCode", nama: "VIN Code"]
        ]}"/>


        <g:javascript disposition="head">

        var i = 1;

        $(function () {

            completeProcess = function () {

            }

            ubahKolom = function(kolom){
                    <g:each in="${koloms}" var="kolom">
                        $( "#${kolom.id}_div" ).hide();
                    </g:each>
                        $( "#"+kolom+"_div" ).show();
                }
            });
        </g:javascript>

	</head>
	<body>
    <ul class="nav nav-tabs">
        <li><a href="javascript:loadPath('customerSurvey/');">Master JD Power Survey</a></li>
        <li class="active"><a href="#">Detail JD Survey</a></li>
    </ul>

    <div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>


    <div class="box">
        <legend style="font-size: small">Search Criteria:</legend>
        <div id="filter_view" class="controls">
            <select id="kriteria">
                <option value="">Pencarian</option>
                <option value="noPol">No Polisi</option>
                <option value="model">Model</option>
                <option value="vinCode">Vin Code</option>
            </select>
        </div>
        <div id="kata_kuncis" class="controls">
            <g:textField name="kata_kunci" id="kata_kunci" autocomplete="off"/>
        </div>
        <div class="controls" style="right: 0">
            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="view" >Search</button>
            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel" name="clear" id="clear" >Clear</button>
        </div>
    </div>

    <div class="box">
        <div class="span12" id="customerVehicle-table">
            <g:if test="${flash.message}">
                <div class="message" role="status">
                    ${flash.message}
                </div>
            </g:if>

            <g:render template="dataTablesDetail" />
        </div>
        <div class="span7" id="customerVehicle-form" style="display: none;"></div>
    </div>

    <div class="box">
        <div class="control-group fieldcontain ${hasErrors(bean: customerSurveyInstance, field: 't104TglAwal', 'error')} required">
            <label class="control-label" for="t104TglAwal">
                <g:message code="customerSurvey.CustomerSurvey.label" default="Customer Survey" />
            </label>
            <div class="controls">
                <g:select id="customerSurvey" name="customerSurvey.id" from="${com.kombos.customerprofile.CustomerSurvey.list()}" optionKey="id" value="${customerSurveyInstance?.id}" class="many-to-one" noSelection="['null': '']" onchange="getCustomerSurveyInfo()"/>

            </div>

            <label class="control-label" for="t104TglAwal">
                <g:message code="customerSurvey.t104TglAwal.label" default="Tgl Awal" />
            </label>
            <div class="controls">
                <input type="text" disabled="" name="t104TglAwal" id="t104TglAwal" />
                -
                <input type="text" disabled="" name="t104TglAkhir" id="t104TglAkhir" />
            </div>
        </div>


        <div class="control-group fieldcontain ${hasErrors(bean: customerSurveyInstance, field: 't104TglAkhir', 'error')} required">
            <div class="controls">
                <input type="button" onclick="setJDPower()" value="Set JD Power Survey" class="btn btn-primary create"/>
            </div>
        </div>
    </div>

</body>
</html>
