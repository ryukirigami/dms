
<%@ page import="com.kombos.customerprofile.CustomerVehicle" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="customerVehicle_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerVehicle.t103VinCode.label" default="Vin Code" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="customerVehicle.nomorPolisi.label" default="Nomor Polisi" /></div>
            </th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerVehicle.fullModelCode.label" default="Full Model Code" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerVehicle.model.label" default="Model" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerVehicle.tanggalDEC.label" default="Tanggal DEC" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerVehicle.nomorMesin.label" default="Nomor Mesin" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerVehicle.warna.label" default="Warna" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerVehicle.nomorKunci.label" default="Nomor Kunci" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="customerVehicle.tanggalSTNK.label" default="Tanggal STNK" /></div>
            </th>
		</tr>
		<tr style="display: none;">
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t103VinCode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t103VinCode" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_nomorPolisi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_nomorPolisi" class="search_init" />
                </div>
            </th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_fullModelCode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_fullModelCode" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_model" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_model" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_tanggalDEC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_tanggalDEC" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_nomorMesin" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_nomorMesin" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_warna" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_warna" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_nomorKunci" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_nomorKunci" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_tanggalSTNK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_tanggalSTNK" class="search_init" />
                </div>
            </th>
		</tr>
	</thead>
</table>

<g:javascript>
var customerVehicleTable;
var reloadCustomerVehicleTable;
var newUrl;

function initDataTable(){
    customerVehicleTable = $('#customerVehicle_datatables').dataTable({
                "sScrollX": "100%",
                "bScrollCollapse": true,
                "bAutoWidth" : false,
                "bPaginate" : true,
                "sInfo" : "",
                "sInfoEmpty" : "",
                "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
                bFilter: true,
                "bStateSave": false,
                'sPaginationType': 'bootstrap',
                "fnInitComplete": function () {
                    this.fnAdjustColumnSizing(true);
        			setCheckBoxVehicle();
                   },
                   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    return nRow;
                   },
                "bSort": true,
                "bProcessing": true,
                "bServerSide": true,
                "sServerMethod": "POST",
                "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
                "sAjaxSource": "${g.createLink(action: "datatablesListDetail")}",
                "aoColumns": [

        {
            "sName": "t103VinCode",
            "mDataProp": "t103VinCode",
            "aTargets": [0],
            "mRender": function ( data, type, row ) {
                return '<input type="checkbox" id="'+row['id']+'_Checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'"><input class="vehicle_exist" type="hidden" id="'+row['id']+'_Exist" value="'+row['exist']+'">&nbsp;&nbsp;'+data;
            },
            "bSearchable": true,
            "bSortable": true,
            "sWidth":"300px",
            "bVisible": true
        }

        ,{
            "sName": "nomorPolisi",
            "mDataProp": "nomorPolisi",
            "aTargets": [9],
            "bSearchable": true,
            "bSortable": true,
            "sWidth":"200px",
            "bVisible": true
        }
        ,{
            "sName": "fullModelCode",
            "mDataProp": "fullModelCode",
            "aTargets": [2],
            "bSearchable": true,
            "bSortable": true,
            "sWidth":"200px",
            "bVisible": true
        }

        ,{
            "sName": "model",
            "mDataProp": "model",
            "aTargets": [3],
            "bSearchable": true,
            "bSortable": true,
            "sWidth":"200px",
            "bVisible": true
        }

        ,{
            "sName": "tanggalDEC",
            "mDataProp": "tanggalDEC",
            "aTargets": [4],
            "bSearchable": true,
            "bSortable": true,
            "sWidth":"200px",
            "bVisible": true
        }

        ,{
            "sName": "nomorMesin",
            "mDataProp": "nomorMesin",
            "aTargets": [5],
            "bSearchable": true,
            "bSortable": true,
            "sWidth":"200px",
            "bVisible": true
        }

        ,{
            "sName": "warna",
            "mDataProp": "warna",
            "aTargets": [6],
            "bSearchable": true,
            "bSortable": true,
            "sWidth":"200px",
            "bVisible": true
        }

        ,{
            "sName": "nomorKunci",
            "mDataProp": "nomorKunci",
            "aTargets": [7],
            "bSearchable": true,
            "bSortable": true,
            "sWidth":"200px",
            "bVisible": true
        }

        ,{
            "sName": "tanggalSTNK",
            "mDataProp": "tanggalSTNK",
            "aTargets": [8],
            "bSearchable": true,
            "bSortable": true,
            "sWidth":"200px",
            "bVisible": true
        }
        ],
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                                var kriteria = $('#kriteria').val();
                                if(kriteria){
                                    aoData.push(
                                            {"name": 'kriteria', "value": kriteria}
                                    );
                                }

                                var kata_kunci = $('#kata_kunci').val();
                                if(kata_kunci){
                                    aoData.push(
                                            {"name": 'kata_kunci', "value": kata_kunci}
                                    );
                                }
                                $.ajax({ "dataType": 'json',
                                    "type": "POST",
                                    "url": sSource,
                                    "data": aoData ,
                                    "success": function (json) {
                                        fnCallback(json);
                                       },
                                    "complete": function () {
                                       }
                                });
                }
            });
}

function setCheckBoxVehicle(){
    $(".vehicle_exist").each(
        function() {
            var idObject = this.id;
            var idValue = ""+document.getElementById(this.id).value;
            idObject = "#"+idObject.replace("_Exist","_Checkbox");
            if(idValue == "1"){
                $(idObject).prop('checked', true);
            }
        }
    );
}

$(function(){

	reloadCustomerVehicleTable = function(id) {

            customerVehicleTable.fnDraw();

	}



$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	customerVehicleTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

    newURL = "${request.contextPath}/customerSurvey/datatablesListDetail";
    initDataTable();
});
</g:javascript>



