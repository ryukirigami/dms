<table id="operation_datatables_input" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	style="margin-left: 0px; width: 2100px;">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px; width:300px;">
				<div><g:message code="operation.section.label" default="Section" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; width:200px;">
				<div><g:message code="operation.serial.label" default="Serial" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; width:200px;">
				<div><g:message code="operation.m053Id.label" default="Kd Repair" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; width:200px;">
				<div><g:message code="operation.m053NamaOperation.label" default="Nama Repair" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; width:200px;">
				<div><g:message code="operation.m053StaLift.label" default="Butuh Lift" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; width:200px;">
				<div><g:message code="operation.m053StaPaket.label" default="Status Paket" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; width:200px;">
				<div><g:message code="operation.kategoriJob.label" default="Kategori Job" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; width:200px;">
				<div><g:message code="operation.m053StaPaint.label" default="Paint" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; width:200px;">
				<div><g:message code="operation.m053Ket.label" default="Keterangan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; width:200px;">
				<div><g:message code="operation.m053JobsId.label" default="M053 Jobs Id" /></div>
			</th>
		
		</tr>

		<!--tr style="display:none;">
			<th style="border-top: none;padding: 5px;">
				<div id="filter_section" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_section" id="sectionFilter" class="search_init" autocomplete="off" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_serial" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_serial"id="serialFilter" class="search_init" autocomplete="off"/>
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m053Id" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m053Id" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m053NamaOperation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m053NamaOperation" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m053StaLift" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<select name="search_m053StaLift" style="width:80%" class="search_init">
                        <option value=""></option>
                        <option value="1">Ya</option>
                        <option value="0">Tidak</option>
                    </select>
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m053StaPaket" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<select name="search_m053StaPaket" style="width:80%" class="search_init">
                        <option value=""></option>
                        <option value="1">Ya</option>
                        <option value="0">Tidak</option>
                    </select>
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kategoriJob" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kategoriJob" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m053StaPaint" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<select name="search_m053StaPaint" style="width:80%" class="search_init">
                        <option value=""></option>
                        <option value="1">Ya</option>
                        <option value="0">Tidak</option>
                    </select>
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m053Ket" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m053Ket" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m053JobsId" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m053JobsId" class="search_init" />
				</div>
			</th>

		</tr-->
	</thead>
</table>

<g:javascript>
var operationTableInput;
var reloadoperationTableInput;
$(function(){
    $('#sectionFilter').typeahead({
        source: function (query, process) {
            return $.get('${request.contextPath}/operation/getSection', { query: query }, function (data) {
                return process(data.options);
            });
        }
    });

    $('#serialFilter').typeahead({
        source: function (query, process) {
            return $.get('${request.contextPath}/operation/getSerial', { query: query }, function (data) {
                return process(data.options);
            });
        }
    });
	
	$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	operationTableInput.fnDraw();
		}
	});
	
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});
	
	reloadoperationTableInput = function() {
		operationTableInput.fnDraw();
	}
	
	operationTableInput = $('#operation_datatables_input').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(controller: "operation", action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "section",
	"mDataProp": "section",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "serial",
	"mDataProp": "serial",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m053Id",
	"mDataProp": "m053Id",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m053NamaOperation",
	"mDataProp": "m053NamaOperation",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m053StaLift",
	"mDataProp": "m053StaLift",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m053StaPaket",
	"mDataProp": "m053StaPaket",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "kategoriJob",
	"mDataProp": "kategoriJob",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m053StaPaint",
	"mDataProp": "m053StaPaint",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m053Ket",
	"mDataProp": "m053Ket",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m053JobsId",
	"mDataProp": "m053JobsId",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
			
            var section = $('#filter_section input').val();
            if(section){
                aoData.push(
                        {"name": 'sCriteria_section', "value": section}
                );
            }

            var serial = $('#filter_serial input').val();
            if(serial){
                aoData.push(
                        {"name": 'sCriteria_serial', "value": serial}
                );
            }

            var m053Id = $('#filter_m053Id input').val();
            if(m053Id){
                aoData.push(
                        {"name": 'sCriteria_m053Id', "value": m053Id}
                );
            }

            var m053NamaOperation = $('#filter_m053NamaOperation input').val();
            if(m053NamaOperation){
                aoData.push(
                        {"name": 'sCriteria_m053NamaOperation', "value": m053NamaOperation}
                );
            }

            var m053StaLift = $('#filter_m053StaLift select').val();
            if(m053StaLift){
                aoData.push(
                        {"name": 'sCriteria_m053StaLift', "value": m053StaLift}
                );
            }

            var m053StaPaket = $('#filter_m053StaPaket select').val();
            if(m053StaPaket){
                aoData.push(
                        {"name": 'sCriteria_m053StaPaket', "value": m053StaPaket}
                );
            }

            var kategoriJob = $('#filter_kategoriJob input').val();
            if(kategoriJob){
                aoData.push(
                        {"name": 'sCriteria_kategoriJob', "value": kategoriJob}
                );
            }

            var m053StaPaint = $('#filter_m053StaPaint select').val();
            if(m053StaPaint){
                aoData.push(
                        {"name": 'sCriteria_m053StaPaint', "value": m053StaPaint}
                );
            }

            var m053Ket = $('#filter_m053Ket input').val();
            if(m053Ket){
                aoData.push(
                        {"name": 'sCriteria_m053Ket', "value": m053Ket}
                );
            }

            var m053JobsId = $('#filter_m053JobsId input').val();
            if(m053JobsId){
                aoData.push(
                        {"name": 'sCriteria_m053JobsId', "value": m053JobsId}
                );
            }
			
            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
		}			
	});
});
</g:javascript>