
<%@ page import="com.kombos.reception.Reception" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="reception_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.namaCustomer.label" default="Nama Customer" /></div>
			</th>
			
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.nomorHp.label" default="Alamat Customer" /></div>
			</th>
			
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.nomorHp.label" default="Nomor Hp" /></div>
			</th>
			
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.kendaraan.label" default="Kendaraan" /></div>
			</th>
			
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.noPolKendaraan.label" default="Nomor Polisi" /></div>
			</th>
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401NoWO.label" default="Nomor WO" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TanggalWO.label" default="Tanggal WO" /></div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var receptionTable;
var reloadReceptionTable;
$(function(){
	
	reloadReceptionTable = function() {
	    var valid = true;
	    if ($("#tanggalReceptionCb").is(":checked")) {
            if ($("#tanggalReceptionTo").val().length < 1) {
                $("#tanggalReceptionTo").focus();
                valid = false;
            }
            if ($("#tanggalReception").val().length < 1) {
                $("#tanggalReception").focus();
                valid = false;
            }

	    }
	    if (valid) {
	        receptionTable.fnDraw();
	    }

	}
	
	$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	receptionTable.fnDraw();
		}
	});
	
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	receptionTable = $('#reception_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

	{
		"sName": "namaCustomer",
		"mDataProp": "namaCustomer",
		"aTargets": [0],
		"mRender": function ( data, type, row ) {
			return '&nbsp;'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="editJobPart('+row['id']+');">&nbsp;&nbsp;Edit Job & Part&nbsp;&nbsp;</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;Edit Reception&nbsp;&nbsp;</a>';
		},
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"300px",
		"bVisible": true
	}
	
	,

	{
		"sName": "alamatCustomer",
		"mDataProp": "alamatCustomer",
		"aTargets": [1],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "nomorHp",
		"mDataProp": "nomorHp",
		"aTargets": [2],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}
	
	,

	{
		"sName": "kendaraan",
		"mDataProp": "kendaraan",
		"aTargets": [3],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}
	
	,

	{
		"sName": "noPolKendaraan",
		"mDataProp": "noPolKendaraan",
		"aTargets": [4],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}
	
	,
	
	{
		"sName": "NoWO",
		"mDataProp": "NoWO",
		"aTargets": [5],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "tanggalWO",
		"mDataProp": "tanggalWO",
		"aTargets": [6],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

//                        if(document.getElementById('tanggalReceptionCb').checked == true){
	                    if ($("#tanggalReceptionCb").is(":checked")) {
	                        var tglReception = $('#tanggalReception').val();
                            var tglReceptionDay = $('#tanggalReception_day').val();
                            var tglReceptionMonth = $('#tanggalReception_month').val();
                            var tglReceptionYear = $('#tanggalReception_year').val();

                            var tglReceptionTo = $('#tanggalReceptionTo').val();
                            var tglReceptionToDay = $('#tanggalReceptionTo_day').val();
                            var tglReceptionToMonth = $('#tanggalReceptionTo_month').val();
                            var tglReceptionToYear = $('#tanggalReceptionTo_year').val();

                            if (tglReception) {
                                aoData.push(
									{"name": 'sCriteria_tanggalReception', "value": "date.struct"},
									{"name": 'sCriteria_tanggalReception_dp', "value": tglReception},
									{"name": 'sCriteria_tanggalReception_day', "value": tglReceptionDay},
									{"name": 'sCriteria_tanggalReception_month', "value": tglReceptionMonth},
									{"name": 'sCriteria_tanggalReception_year', "value": tglReceptionYear}
							    );
                            }

                            if (tglReceptionTo) {
                                aoData.push(
									{"name": 'sCriteria_tanggalReceptionTo', "value": "date.struct"},
									{"name": 'sCriteria_tanggalReceptionTo_dp', "value": tglReceptionTo},
									{"name": 'sCriteria_tanggalReceptionTo_day', "value": tglReceptionToDay},
									{"name": 'sCriteria_tanggalReceptionTo_month', "value": tglReceptionToMonth},
									{"name": 'sCriteria_tanggalReceptionTo_year', "value": tglReceptionToYear}
							    );
                            }
	                    }

                        if(document.getElementById('kategoriPencarianCb').checked == true){
                            var cat = $("#kategoriPencarian").val();
                                if(cat){
                                    aoData.push(
                                        {"name": 'cat', "value": cat}
                                    )
                                }

                            var keyword = $("#kataKunci").val();
                                if (keyword) {
                                    aoData.push(
                                        {"name": 'keyword', "value": keyword}
                                    )
                                }
                        }

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
