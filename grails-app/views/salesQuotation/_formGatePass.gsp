<%@ page import="com.kombos.reception.GatePassT800" %>


<g:javascript>
         $(function(){
               $('input:radio[name=gatePass]').click(function(){
                if($('input:radio[name=gatePass]:nth(1)').is(':checked')){
                    $("#t800xNamaUser").prop('disabled', false);

                }else{
                    $("#t800xNamaUser").val("")
                    $("#t800xNamaUser").prop('disabled', true);
                }
               });
                $('#nopol').change(function(){
                    var nopol=$('#nopol').val();

                    $.ajax({
                        url:'${request.contextPath}/gatePassT800/cekData',
                        type:'POST',
                        data:'id=' + nopol,
                        dataType: 'json',
    		            success: function(data,textStatus,xhr){
    		                if(data.length>0){
    		                    $('#namaCustomer').attr('value',data[0].namaCustomer);
                                $('#model').attr('value',data[0].models);
                                $('#warna').attr('value',data[0].warna);
    		                }else{
                                $('#namaCustomer').attr('value','');
                                $('#model').attr('value','');
                                $('#warna').attr('value','');
    		                }

    		            },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(data,textStatus){
                        }
                   });
                  }
             );
        });
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: gatePassT800Instance, field: 'reception', 'error')} ">
	<label class="control-label" for="reception">
		<g:message code="gatePassT800.reception.label" default="No Pol" />
		
	</label>
	<div class="controls">
	    <g:select id="nopol" name="reception.id" from="${com.kombos.customerprofile.HistoryCustomerVehicle.list()}" optionKey="id" required="" value="${gatePassT800Instance?.reception?.historyCustomerVehicle?.id}" class="many-to-one" noSelection="['':'- Choose -']"/>
	</div>i
</div>
<g:javascript>
  //  document.getElementById("reception").focus();
</g:javascript>
<div class="control-group">
	<label class="control-label" for="t800StaDel">
		<g:message code="gatePassT800.t800StaDel.label" default="Nama Customer" />
	</label>
	<div class="controls">
        <g:textField name="namaCustomer" id="namaCustomer" value="${gatePassT800Instance?.reception?.historyCustomer ? gatePassT800Instance?.reception?.historyCustomer?.t182NamaDepan+" "+gatePassT800Instance?.reception?.historyCustomer?.t182NamaBelakang : ""}" readonly="" />
	</div>
</div>
<div class="control-group">
    <label class="control-label" for="t800StaDel">
        <g:message code="gatePassT800.t800StaDel.label" default="Model Vehicle" />
    </label>
    <div class="controls">
        <g:textField name="model" id="model" value="${gatePassT800Instance?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel}" readonly="" />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="t800StaDel">
        <g:message code="gatePassT800.t800StaDel.label" default="Warna Vehicle" />
    </label>
    <div class="controls">
        <g:textField name="warna" id="warna" value="${gatePassT800Instance?.reception?.historyCustomerVehicle?.warna?.m092NamaWarna}" readonly="" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: gatePassT800Instance, field: 'gatePass', 'error')} ">
    <label class="control-label" for="gatePass">
        <g:message code="gatePassT800.gatePass.label" default="Gate Pass" />

    </label>
    <div class="controls">
        %{--<g:select id="gatePass" name="gatePass.id" from="${com.kombos.reception.GatePass.list()}" optionKey="id" required="" value="${gatePassT800Instance?.gatePass?.id}" class="many-to-one" noSelection="['':'- Choose -']" />--}%
        <g:radioGroup name="gatePass" id="gatePass" values="['1','2']" labels="['Gatepass','Surat Jalan Test Kendaraan']" value="${gatePassT800Instance?.gatePass?.m800Id}" >
            ${it.radio} ${it.label}
        </g:radioGroup>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: gatePassT800Instance, field: 't800xNamaUser', 'error')} ">
    <label class="control-label" for="t800xNamaUser">
        <g:message code="gatePassT800.t800xNamaUser.label" default="Nama Pegawai" />

    </label>
    <div class="controls">
        <g:textField name="t800xNamaUser" id="t800xNamaUser" value="${gatePassT800Instance?.t800xNamaUser}" required="" />
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: gatePassT800Instance, field: 't800xKet', 'error')} ">
	<label class="control-label" for="t800xKet">
		<g:message code="gatePassT800.t800xKet.label" default="Catatan" />
		
	</label>
	<div class="controls">
	    <g:textArea cols="50" rows="5" name="t800xKet" value="${gatePassT800Instance?.t800xKet}" />
	</div>
</div>
