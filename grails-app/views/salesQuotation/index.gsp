
<%@ page import="com.kombos.reception.Reception" %>
<%@ page import="com.kombos.administrasi.KategoriJob"%>
<%@ page import="com.kombos.administrasi.CompanyDealer"%>
<%@ page import="com.kombos.administrasi.Operation" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'salesQuotation.label', default: 'Estimasi Service')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
        <r:require modules="baseapplayout, baseapplist, autoNumeric"/>
		<g:javascript>
		var newAppointment;
	    var namaPemilik = "";
        var alamatPemilik = "";
        var telpPemilik = "";
        var mobilPemilik = "";
        var tglBayarBooking = "";
        var tgljanjiDatang = "";
		var openCancelEditBookingFee;
        var isCharOnly;
        var isNumberKey;
        var dataCustomer;
		var savePartApp;
		var gatePass;
		var idVincode = '-1';
		var idJobTempPart='-1';
		var selectAllCheckbox;
        var unselectAllCheckbox;
        var deleteCheckbox;
		var showReception;
		var openRequestPart;
		var openEstimasi;
		var openInputJobDanKeluhan;
		var showAppSummary;
		var showPrediagnose;
		var showApproval;
		var staValidate = 0;
		var openCustomer;
		var nilaiSpk = -1;
		var showApprovalSPK;
		var staNewEdit = "";
		var isValidated = false;
		var showApprovalPiutang;

		$(function(){

			savePartApp = function() {
                var partparams = {};
                partparams['idReception'] =  $('#receptionId').val();
                partparams['idJob'] =  idJobTempPart;

		        var checkPart =[];
                var id = 1;
                $("#inputPart_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var nRow = $(this).parents('tr')[0];
                        var aData = inputPartTable.fnGetData(nRow);
                        partparams['good' + id] = aData['id'];
                        partparams['prosesBP' + id] = $('#prosesBP' + aData['id']).val();
                        partparams['qty' + id] = $('#qty' + aData['id']).val();
                        checkPart.push(id);
                       id++;
                    }
                });

                partparams['countRowPart'] = id;
                $.ajax({url: '${request.contextPath}/salesQuotation/savePartInput',
					type: "POST",
					data: partparams,
					success: function(data) {
				        toastr.success('Data parts berhasil ditambahkan');
				        $('#appointmentInputPartModal').modal('hide');
				        reloadjobnPartsTable();
					},
					complete : function (req, err) {
						$('#spinner').fadeOut();
						loading = false;
					}
				});
		    }

            isCharOnly = function(e) {
                e = e || event;
                return /[a-zA-Z-]/i.test(
                        String.fromCharCode(e.charCode || e.keyCode)
                ) || !e.charCode && e.keyCode  < 48;
            }

            isNumberKey = function(evt){
                var charCode = (evt.which) ? evt.which : evt.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57)){
                    return false;
                }
                else{
                    return true;
                }
            }

            $(".charonly").bind('keypress',isCharOnly);

            $(".numberonly").bind('keypress',isNumberKey);

            dataCustomer = function(){
                var kodeKota=$('#kodeKota').val();
                var noPolTengah=$('#nomorTengah').val();
                var noPolBelakang=$('#nomorBelakang').val();

                if(kodeKota=="" || kodeKota==null || noPolBelakang=="" || noPolBelakang==null || noPolTengah=="" || noPolTengah==null){
                    alert('Anda belum memasukan nomor polisi secara lengkap')
                }else{
                    $('#spinner').fadeIn(1);
                    var notNull = false;
                    var idKirim = []
                    $.ajax({
                        url:'${request.contextPath}/salesQuotation/findNoPol',
                        type: "POST",
                        data: { kode: kodeKota , tengah : noPolTengah , belakang : noPolBelakang },
                        success : function(data){
                            if(data.length>2){
                                notNull = true;
                                idKirim.push(data[data.length-1].id)
                                idKirim.push(data[0].id)
                            }else{
                                alert('\t\t\tData customer untuk nomor polisi tersebut belum ada. \nSilahkan isi data customer di menu Customer Profile => Master Customer => Customer');
                            }
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal server error');
                        },
                        complete:function(XMLHttpRequest,textStatus){
                            openCustomer(notNull,idKirim);
                        }
                    });
                }
            }

            openCustomer = function(notNull,idKirim){
                if(notNull==true){
                    window.location.href='#/customer'
                    $('#spinner').fadeIn(1);
                    $.ajax({
                        url: '${request.contextPath}/customer',
                        data: { idCustomer: idKirim[0] , idVehicle : idKirim[1] },
                        type: "POST",dataType:"html",
                        complete : function (req, err) {
                            $('#main-content').html(req.responseText);
                            $('#spinner').fadeOut();
                        }
                    });
                }
            }

			selectAllCheckbox = function(className){
				$('.'+className).prop('checked', true);
			}

			unselectAllCheckbox = function(className){
				$('.'+className).prop('checked', false);
			}

			deleteCheckbox = function(className){
				$('.'+className).each(function() {
					if(this.checked){
						$(this).closest("tr").remove();
					}
				});
			}

            openInputJobDanKeluhan = function() {
                $("#dialog-reception-jobkeluhan").modal({
                    "backdrop" : "static",
                    "keyboard" : true,
                    "show" : true
                }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});
            }

            $('#receptionId').val('${receptionInstance?.id}');
    });

        function saveHarga(id){
            var harga = $('#jasa-'+id).val()
            var params;
            params = {
                id: id,
                harga: harga,
                dari : "jasa"
            }
            $.post("${request.getContextPath()}/salesQuotation/updateHarga", params, function(data) {
                if (data){
                    reloadjobnPartsTable();
                }else{
                    alert("Internal Server Error");
                }
            });
        }
    function newReception(){
        $("#lblAksiReception").text("New Data");
        staNewEdit = "getNewReception";
        $('#new_reception').hide();
        $('#edit_reception').hide();
        $('#reset').show();
        initBeforeSave();
    }

    function editReception(){
        $("#lblAksiReception").text("Edit Data");
        staNewEdit = "getReception";
        $('#new_reception').hide();
        $('#edit_reception').hide();
        $('#reset').show();
    }

    function openDialog(divModal){
        var depan = $('#kodeKota').val();
        var tengah=$('#nomorTengah').val();
        var belakang=$('#nomorBelakang').val();
        if(divModal=="dialog-reception-jobkeluhan"){
            var pemilik = document.getElementById('lblpemakai').outerHTML;
            if(depan=="" || tengah=="" || belakang==""){
                alert('Data nomor polisi belum lengkap')
                return
            }
            if(staValidate==0){
                alert('Validasi terlebih dahulu nomor polisi dengan menekan tombol enter pada kolom nomor polisi belakang');
                return
            }
            if(idVincode=="-1"){
                alert('Nomor polisi tidak dikenal');
                return
            }
            openJobKeluhan();
        }else if(divModal=="dialog-reception-parts"){
            var length=0;
            var idJobTemp;
            $("#job_n_parts_datatables tbody .row-select").each(function() {
                if(this.checked){
                    length++;
                    idJobTemp = $(this).next("input:hidden").val();
                }
            });
            if(length==0){
                alert('Pilih satu job yang akan ditambahkan part');
                return;
            }
            if(length>1){
                alert('Anda hanya dapat memilih satu job');
                return;
            }
            idJobTempPart = idJobTemp;
        }
        $( "#"+divModal ).modal("show");
    }

    function closeDialog(divModal){
        $( "#"+divModal ).modal("hide");
    }

	function initButtonPrintWoBpGr(id){
	        console.log("INI NIH "+id);
			$('.btnPrintWo').show();
			$('#printWoBpGr').html("<a id='hrefPrintWoBp' href='${request.contextPath}/salesQuotation/printWoBp?id="+id+"' >Print WO BP</a><a id='hrefPrintWoGr' href='${request.contextPath}/salesQuotation/printWoGr?id="+id+"' >Print WO GR</a>");
		}

	function initButtonJobAndKeluhan(id){
			$('#input_job_dan_keluhan').show();

			$.ajax({
				url:'${request.getContextPath()}/salesQuotation/getDataKeluhan?id='+id,
				type: 'POST',
				success: function (res) {
					$('#tableKeluhan').find("tr:gt(0)").remove();
					$('#tableKeluhan').append(res);
				},
				error: function (data, status, e){
					alert(e);
				},
				complete: function(xhr, status) {

				},
				cache: false,
				contentType: false,
				processData: false
			});

			$.ajax({
				url:'${request.getContextPath()}/salesQuotation/getDataJob?id='+id,
				type: 'POST',
				success: function (res) {
					$('#tableJob').find("tr:gt(0)").remove();
					$('#tableJob').append(res);
				},
				error: function (data, status, e){
					alert(e);
				},
				complete: function(xhr, status) {

				},
				cache: false,
				contentType: false,
				processData: false
			});
		}

	function saveReception(){
	        var jum = $('#lblfooternominal').text();
	        var jumJam = $('#lblfooterrate').text();
	        var dataSave = 0;
            $("#job_n_parts_datatables tbody .row-select").each(function() {
                dataSave++;
            });

            if(dataSave<=0){
                alert('Belum ada job yang diinputkan');
                return;
            }
            var noDepan = $('#kodeKota').val();
            var noTengah = $('#nomorTengah').val();
            var noBelakang = $('#nomorBelakang').val();
		    var receptionID = $('#receptionId').val();
		    $.ajax({
                url:'${request.getContextPath()}/salesQuotation/saveReception',
				type: 'POST',
				data : {idReception : receptionID,kodeKotaNoPol : noDepan,noPolTengah : noTengah, noPolBelakang : noBelakang,jumlah : jum,jumlahJam : jumJam},
			    success:function(data,textStatus){
                    if(data=="ok"){
                        initButtonPrintWoBpGr(receptionID);
                        if(staNewEdit=="getNewReception"){
                            initAfterSave();
                        }
                        toastr.success('Data berhasil disimpan');
                    }else if(data=="nocustomerin"){
                        toastr.error('Tidak ada data customer in untuk nomor polisi tersebut');
                    }else{
                        toastr.error('Nomor Polisi Salah');
                    }
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){alert('Internal Server Error')},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
			});
    }

    function initBeforeSave(){
        $('#input_job_dan_keluhan').show();
        $('#input_parts').show();
        $('#btnDelete2').show();
        $('#save_reception').show();
    }

    function initAfterSave(){
        $('#print_wo_bp').show();
        $('#print_wo_gr').show();
        $('#createR').show();
    }

	$("#nomorBelakang").bind('keypress', function(e) {
		    cekJob = [];
            $('#btnAsuransi').hide();
            var noDepan = $('#kodeKota').val();
            var noTengah = $('#nomorTengah').val();
            var noBelakang = $('#nomorBelakang').val();
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13 && isValidated==false) {
                if(staNewEdit==""){
                    alert('Pilih new atau edit')
                    return
                }
                e.preventDefault();
                e.stopPropagation();
                if(noDepan=="" || noTengah=="" || noBelakang==""){
                    alert('Data nomor polisi belum lengkap');
                    return
                }else{
                    var dataUrl = '${request.contextPath}/salesQuotation/'+staNewEdit
                    $.ajax({
                        url: dataUrl,
                        type: "POST",
                        data: {kodeKotaNoPol : noDepan,noPolTengah : noTengah, noPolBelakang : noBelakang,idReception : $('#receptionId').val()},
                        success: function(data) {
                            var kosong = false;
                            if(data[0].hasil=="ada"){
                                $('#lblTgglWO').show();
                                $('#lblNoWO').show();
                                if(staNewEdit=="getReception"){
                                    initBeforeSave();
                                    initAfterSave();
                                    initButtonPrintWoBpGr(data[0].idReception);
                                }else{
                                    isValidated = true;
                                }
                                staValidate=1;
                                document.getElementById('lblfa').innerHTML = data[0].fa
                                document.getElementById('lbltpss').innerHTML = data[0].tpss
                                document.getElementById('lblfu').innerHTML = data[0].fu
                                document.getElementById('lblpks').innerHTML = data[0].pks
                                document.getElementById('lblpemakai').innerHTML = data[0].pemakai
                                document.getElementById('lbldriver').innerHTML = data[0].driver
                                document.getElementById('lblmobil').innerHTML = data[0].mobil
                                $('#txtJobSuggest').val(data[0].jobSuggest);
                                namaPemilik = data[0].pemakai;
                                alamatPemilik = data[0].alamat;
                                telpPemilik = data[0].telp;
                                mobilPemilik = data[0].mobil;
                                tglBayarBooking = data[0].tanggalBayar
                                tgljanjiDatang = data[0].tanggalDatang
                                if(data[0].idReception){
                                    $('#receptionId').val(data[0].idReception);
//                                    initAll(data[0].idReception);
                                }
                                if(data[0].tgglReception){
                                    console.log(data[0].tgglReception)
                                    $('#receptionTggl').val(data[0].tgglReception);
                                    document.getElementById('lblTgglWO').innerHTML=data[0].tgglReception
                                }
                                if(data[0].noWO){
                                    $('#receptionNoWO').val(data[0].noWO);
                                    document.getElementById('lblNoWO').innerHTML = data[0].noWO
                                }
                                if(data[0].staJDPower=="0"){
                                    $('#lbljdpower').show();
                                }
                                idVincode=data[0].idCV;
                                nilaiSpk=data[0].spk;
                                reloadjobnPartsTable();
                            }else if(data[0].hasil=="replaced"){
                                kosong = true;
                                alert('Nomor polisi sudah tidak berlaku');
                            }else if(data[0].hasil=="nothing"){
                                kosong = true;
                                alert('Data dengan nomor polisi tersebut tidak ditemukan');
                            }else if(data[0].hasil=="noreception"){
                                kosong = true;
                                alert('Belum ada data reception untuk nomor polisi tersebut');
                            }
                            if(kosong){
                                idVincode='-1';
                                $('#lblfooternominal').text('');
                                $('#lblfooterrate').text('');
                                $('#divSpk').hide();
                                $('#receptionId').val('${receptionInstance?.id}');
                                $('#receptionTggl').val('${receptionInstance?.dateCreated ? receptionInstance?.dateCreated.format("dd MMMM yyyy") : new java.util.Date().format("dd MMMM yyyy")}');
                                $('#receptionNoWO').val('${receptionInstance?.t401NoWO}');
                                document.getElementById('lblTgglWO').innerHTML=$('#receptionTggl').val();
                                document.getElementById('lblNoWO').innerHTML = $('#receptionNoWO').val()
                                document.getElementById('lblfa').innerHTML = ""
                                document.getElementById('lbltpss').innerHTML = ""
                                document.getElementById('lblfu').innerHTML = ""
                                document.getElementById('lblpks').innerHTML = ""
                                document.getElementById('lblpemakai').innerHTML = ""
                                document.getElementById('lbldriver').innerHTML = ""
                                document.getElementById('lblmobil').innerHTML = ""
                                $('#lbljdpower').hide();
                                reloadjobnPartsTable();
                            }
                        },
                        error: function (data, status, e){
                            alert(e);
                        }
                    });
                }
            }
        });

    function cekJenis(){
        if($('input:radio[name=jenisApp]:nth(0)').is(':checked')){
            cekRadio=0;
            $('#btncekJpbGr').attr("disabled",false)
            $('#btncekJpbBp').attr("disabled",true)
            $('#print_wo_gr').attr("disabled",false)
            $('#print_wo_bp').attr("disabled",true)
            $('#input_estimasi_bp').attr("disabled",true)
            $('#SpkBp').hide()
        }else{
            cekRadio=1;
            $('#btncekJpbGr').attr("disabled",true)
            $('#btncekJpbBp').attr("disabled",false)
            $('#print_wo_gr').attr("disabled",true)
            $('#print_wo_bp').attr("disabled",false)
            $('#input_estimasi_bp').attr("disabled",false)
            $('#SpkBp').show()
        }
    }

    function doDelete(){
            var jobDelete = [];
            var partsDelete = []
            $("#job_n_parts_datatables tbody .row-select").each(function() {
                if(this.checked){
                    var idDelete = $(this).next("input:hidden").val()
                    if(idDelete.indexOf("#")>-1){
                        idDelete = idDelete.substring(0,idDelete.indexOf("#"))
                        partsDelete.push(idDelete);
                    }else{
                        jobDelete.push(idDelete);
                    }
                }
            });
            if(jobDelete.length<1 && partsDelete.length<1){
                alert('Anda belum memilih data yang akan dihapus.')
                return
            }
            var json = JSON.stringify(jobDelete);
            var json2 = JSON.stringify(partsDelete);
            $.ajax({
                url:'${request.contextPath}/salesQuotation/massdelete',
                type: "POST", // Always use POST when deleting data
                data: { jobs: json, parts : json2 },
                complete: function(xhr, status) {
                    reloadjobnPartsTable();
                }
            });

        }


	function printWoBp(){
			var href = $('#hrefPrintWoBp').attr('href');
			window.location.href = href;
		}

    function printWoGr(){
			var href = $('#hrefPrintWoGr').attr('href');
			window.location.href = href;
		}

    function saveChangeData(id){
        $('#spinner').fadeIn(1);
        $.ajax({
                url:'${request.contextPath}/salesQuotation/changeValue',
                type: "POST", // Always use POST when deleting data
                data: { id : id , nilai : $("#"+id).val() },
                success: function(data) {
                    $('#spinner').fadeOut();
                },error:function(){
                    alert('Internal Server Error');
                }
            });
    }

    function doReset(){
        $('#lblAksiReception').text("Estimasi Service");
        loadPath('salesQuotation');
    }
</g:javascript>
        <style>
        body .modal-reception {
            width: 1020px;
            margin-left: -510px;
        }
        .datepicker{z-index:1151;}
        </style>
	</head>
	<body>
    <!-- start dialog pop up section-->
	<g:render template="../menu/maxLineDisplay"/>

    <div id="dialog-reception-jobkeluhan" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionJobKeluhan"/>
    </div>
	
	<div id="dialog-reception-searchparts" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionSearchParts"/>
    </div>
	
    <div id="dialog-reception-inputjob" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionInputJob"/>
    </div>
	
	<div id="dialog-reception-parts" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionParts"/>
    </div>
	
	<div id="dialog-reception-searchjob" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionSearchJob"/>
    </div>
	
	<div id="dialog-reception-inputparts" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionInputParts"/>
    </div>
	
	<div id="dialog-reception-joborder" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionJobOrder"/>
    </div>
	
	<div id="dialog-reception-paketjob" class="modal hide fade in modal-reception" style=" ;display: none; ">
        <g:render template="receptionPaketJob"/>
    </div>


        <div class="navbar box-header no-border">
            <span class="pull-left" id="lblAksiReception">Estimasi Service</span>
        </div>
        <div id="appointment" class="box">
            <div class="row-fluid">
                <div class="span6 form-horizontal" id="no_pol_search">
                    <g:field type="button" onclick="newReception();" class="btn" name="new_reception" id="new_reception" value="New" />
                    <g:field type="button" onclick="editReception();" class="btn" name="edit_reception" id="edit_reception" value="Edit" />
                    <g:field type="button" style="display:none" onclick="doReset();" class="btn" name="reset" id="reset" value="Reset" />
                    <div class="control-group"><label class="control-label" for="kodeKota" style="font-size: 20px; height: 50px; text-align: left;line-height: 50px;">Nomor Polisi
                        <span class="required-indicator">*</span>
                        </label>
                        <div class="controls" style="margin-left: 100px;">
                            <g:select style="width: 95px; font-size: 30px; height: 61px; border-radius: 0;" id="kodeKota" name="kodeKota" from="${com.kombos.administrasi.KodeKotaNoPol.createCriteria().list(){eq("staDel","0");order("m116ID")}}" optionValue="${{it.m116ID}}" optionKey="id" required="" value="${(!request.getRequestURI().contains("edit"))?"":customerInInstance?.customerVehicle.currentCond.kodeKotaNoPol}" class="many-to-one"/>
                            <g:textField class="numberonly" name="nomorTengah" id="nomorTengah" maxlength="5" value="" style="width: 95px; font-size: 30px; height: 50px; border-radius: 0;"/>
                            <g:textField class="charonly" name="nomorBelakang" id="nomorBelakang" maxlength="3" value="" style="width: 70px; font-size: 30px; height: 50px; border-top-left-radius: 0; border-bottom-left-radius: 0;"/>
                            <input type="hidden" id="receptionId" name="receptionId" />
							<input type="hidden" id="receptionNoWO" name="receptionNoWO" />
							<input type="hidden" id="receptionTggl" name="receptionTggl" />
						</div>
                    </div>
                    <br/>
                </div>
                <div class="span4 offset2" id="tgl_appointment">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>
                                <table class="table table-bordered table-dark table-hover">
                                    <tbody>
                                    <tr>
                                        <td style="width: 100px">
                                            <span class="property-label">
                                                <label id="lblbooking">Estimasi Service</label>
                                            </span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td>
                                <table class="table table-bordered table-dark table-hover">
                                    <tbody>
                                    <tr>
                                        <td><span
                                                class="property-label">Tanggal Estimasi</span></td>
                                        <td><span id="lblTgglWO" style="display: none" class="property-value">
                                            ${receptionInstance?.dateCreated ? receptionInstance?.dateCreated.format("dd MMMM yyyy") : new java.util.Date().format("dd MMMM yyyy") }
                                        </span></td>
                                    </tr>
                                    <tr>
                                        <td><span
                                                class="property-label">No. Reception</span></td>
                                        <td><span id="lblNoWO" style="display: none" class="property-value">
                                            ${receptionInstance?.t401NoWO}
                                        </span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row-fluid" style="margin-top: 30px;">
                <div class="span4">
                    <span style="font-size: 30px;color: red;display: none" id="lbljdpower" class="property-value">
                        <b>JD POWER</b>
                    </span>
                <br/>
                <table class="table table-bordered table-dark table-hover">
                <tbody>
                    <tr>
                        <td style="width: 35%"><span
                            class="property-label">FA</span></td>
                        <td style="width: 65%"><span class="property-value">
                            <label id="lblfa"></label>
                        </span></td>
                    </tr>
                    <tr>
                        <td><span
                            class="property-label">TPSS / SCC</span></td>
                        <td><span class="property-value">
                            <label id="lbltpss"></label>
                        </span></td>
                    </tr>
                    <tr>
                        <td><span
                            class="property-label">FOLLOW UP</span></td>
                        <td><span class="property-value">
                            <label id="lblfu"></label>
                        </span></td>
                    </tr>
                    <tr>
                        <td><span
                            class="property-label">PKS</span></td>
                        <td><span class="property-value">
                            <label id="lblpks"></label>
                        </span></td>
                    </tr>
                </tbody>
                </table>
                <table class="table table-bordered table-dark table-hover">
                <tbody>
                    <tr>
                        <td style="width: 35%"><span
                            class="property-label">Nama Pemakai</span></td>
                        <td style="width: 65%"><span class="property-value">
                            <label id="lblpemakai"></label>
                        </span></td>
                    </tr>
                    <tr>
                        <td><span
                            class="property-label">Nama Driver</span></td>
                        <td><span class="property-value">
                            <label id="lbldriver"></label>
                        </span></td>
                    </tr>
                    <tr>
                        <td><span
                            class="property-label">Mobil</span></td>
                        <td><span class="property-value">
                            <label id="lblmobil"></label>
                        </span></td>
                    </tr>
                </tbody>
                </table>
                </div>
                <div class="span8" id="appointment_table">
                <fieldset class="buttons controls" style="">
                        <g:field type="button" style="display:none;" onclick="openDialog('dialog-reception-jobkeluhan');" class="btn" name="input_job_dan_keluhan" id="input_job_dan_keluhan" value="Input Job & Keluhan" />
                        <g:field type="button" style="display:none;" onclick="openDialog('dialog-reception-parts');" class="btn" name="input_parts" id="input_parts" value="Input Parts" />
                        <input type="button" style="display:none;" value="Delete" id="btnDelete2" class="btn" onclick="doDelete();">
                </fieldset>
                    <g:render template="jobnPartsDataTables" />
                    <fieldset class="buttons controls" style="">
                        <input type="button" style="display:none;" value="Request Parts" id="request_parts" class="btn" onclick="openRequestPart();">
                    </fieldset>
                </div>
            </div>
            <div class="row-fluid" id="buttons">
				<div id="printWoBpGr" style="display:none;"/>
                <fieldset class="buttons controls pull-right" style="padding-top: 10px;">
                        <g:field style="display:none;" type="button" onclick="printWoGr();" class="btn btnPrintWo" name="print_wo_bp" id="print_wo_bp" value="Print Estimasi Service" />
                        %{--<g:field style="display:none;" type="button" onclick="createReception();" class="btn btn-primary" name="createR" id="createR" value="Create Reception" />--}%
                        <g:field type="button" style="display:none;" onclick="saveReception();" class="btn" name="save_reception" id="save_reception" value="Save" />
                </fieldset>
            </div>
        </div>
    </body>
</html>
