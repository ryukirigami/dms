<%@ page import="com.kombos.parts.RequestStatus" %>
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay" />
<table id="ckp_datatables" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none; padding: 5px;">
				<div>Kode Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nama Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;" colspan="2">
				<div>Request</div>
			</th>
			<th style="border-bottom: none; padding: 5px;" colspan="2">
				<div>Availability</div>
			</th>
			<th style="border-bottom: none; padding: 5px;" colspan="2">
				<div>Order</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>ETA</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Tipe</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>RPP</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>DP</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Harga</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Total</div>
			</th>	
		</tr>
		<tr>
			<th style="border-top: none; padding: 5px;"/>
			<th style="border-top: none; padding: 5px;"/>
			<th style="padding: 5px;">
				<div>Qty</div>
			</th>
			<th style="padding: 5px;">
				<div>Satuan</div>
			</th>
			<th style="padding: 5px;">
				<div>Qty</div>
			</th>
			<th style="padding: 5px;">
				<div>Satuan</div>
			</th>
			<th style="padding: 5px;">
				<div>Qty</div>
			</th>
			<th style="padding: 5px;">
				<div>Satuan</div>
			</th>
			<th style="border-top: none; padding: 5px;"/>
			<th style="border-top: none; padding: 5px;">
				<div>Order</div>
			</th>
			<th style="border-top: none; padding: 5px;">
				<div>(hari)</div>
			</th>
			<th style="border-top: none; padding: 5px;">
				<div>(Rp)</div>
			</th>
			<th style="border-top: none; padding: 5px;">
				<div>(Rp)</div>
			</th>
			<th style="border-top: none; padding: 5px;">
				<div>(Rp)</div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var ckpTable;
var reloadCkpTable;
var checkOnValue;
var checked = [];
$(function(){
	checkOnValue = function() {
		var $this   = $(this);
		var value = $this.val();
		var nRow = $this.parents('tr')[0];
		var checkInput = $('input[type="checkbox"]', nRow);
		if(value){							 
			checkInput.attr("checked","checked");
		} else {
			checkInput.removeAttr("checked");
		}
	}
	reloadCkpTable = function() {
		ckpTable.fnDraw();
	}

 	ckpTable = $('#ckp_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			var aData = ckpTable.fnGetData(nRow);
		        var checkbox = $('.row-select', nRow);
		        if(checked.indexOf(""+aData['id'])>=0){
		              checkbox.attr("checked",true);
		              checkbox.parent().parent().addClass('row_selected');
		        }

		        checkbox.click(function (e) {
            	    var tc = $(this);

                    if(this.checked)
            			tc.parent().parent().addClass('row_selected');
            		else {
            			tc.parent().parent().removeClass('row_selected');
            			var selectAll = $('.select-all');
            			selectAll.removeAttr('checked');
                    }
            	 	e.stopPropagation();
                });
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "requestPartDatatablesList")}",
		"aoColumns": [

{
	"sName": "kodePart",
	"mDataProp": "kodePart",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;' + data;
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "namaPart",
	"mDataProp": "namaPart",
	"aTargets": [1],
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
},
{
	"sName": "requestQty",
	"mDataProp": "requestQty",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else 
				            				return '<span></span>';
								},
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "requestSatuan",
	"mDataProp": "requestSatuan",
	"aTargets": [3],
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "availabilityQty",
	"mDataProp": "availabilityQty",
	"aTargets": [4],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else 
				            				return '<span></span>';
								},
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "availabilitySatuan",
	"mDataProp": "availabilitySatuan",
	"aTargets": [5],
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "orderQty",
	"mDataProp": "orderQty",
	"aTargets": [6],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else 
				            				return '<span></span>';
								},
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "orderSatuan",
	"mDataProp": "orderSatuan",
	"aTargets": [7],
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
},{
	"sName": "eta",
	"mDataProp": "eta",
	"aTargets": [8],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else 
				            				return '<span></span>';
								},
	"bSortable": true,
	"sWidth":"50px",
	"bVisible": true
},{
	"sName": "tipeOrder",
	"mDataProp": "tipeOrder",
	"aTargets": [9],
	"mRender": function ( data, type, row ) {
		if(data === null || data === ''){
			var tipeOrders = [];
			<g:each in="${com.kombos.parts.PartTipeOrder.list()}">
                tipeOrders.push({id: ${it.id}, label: '${it.m112TipeOrder}'});
			</g:each>
			
			var tipeOrderCombo = '<select id="tipeOrder'+row['id']+'" name="tipeOrder.id" class="many-to-one inline-edit" style="width:105px;" >';
			tipeOrderCombo += '<option value="null">[Pilih tipe order...]</option>';
			for(var i = 0; i < tipeOrders.length; i++){
				if(data === tipeOrders[i].id)
					tipeOrderCombo += '<option value="'+tipeOrders[i].id+'" selected="selected">'+tipeOrders[i].label+'</option>';
				else
					tipeOrderCombo += '<option value="'+tipeOrders[i].id+'">'+tipeOrders[i].label+'</option>';
			}
			tipeOrderCombo += '</select>';
			return tipeOrderCombo;
		} else {
			return data;
		}
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},{
	"sName": "rpp",
	"mDataProp": "rpp",
	"aTargets": [10],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else 
				            				return '<span></span>';
								},
	"bSortable": true,
	"sWidth":"80px",
	"bVisible": true
},{
	"sName": "dp",
	"mDataProp": "dp",
	"aTargets": [11],
	"mRender": function ( data, type, row ) {
		if(data === null || data === 'null' || data === '' ){
			return '<input type="text" style="width:100px;" value="'+data+'" class="pull-right inline-edit numeric" id="dp'+row['id']+'">';
		} else {
			return '<span class="pull-right numeric">'+data+'</span>';							
		}
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},{
	"sName": "harga",
	"mDataProp": "harga",
	"aTargets": [12],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else 
				            				return '<span></span>';
								},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},{
	"sName": "total",
	"mDataProp": "total",
	"aTargets": [13],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else 
				            				return '<span></span>';
								},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                         var selectAll = $('.select-all');
                        selectAll.removeAttr('checked');
                        $("#ckp_datatables tbody .row-select").each(function() {
                            var id = $(this).next("input:hidden").val();
                            if(checked.indexOf(""+id)>=0){
                                checked[checked.indexOf(""+id)]="";
                            }
                            if(this.checked){
                                checked.push(""+id);
                            }
                        });
			            aoData.push(
						    {"name": 'receptionId', "value": $('#receptionId').val()}
						);

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
								$('#ckp_datatables').find('input.numeric').autoNumeric('init',{
									vMin:'0.00',
									vMax:'999999999999999999.00',
									aSep:''
								}).keypress(checkOnValue)
								.change(checkOnValue);
								$('#ckp_datatables').find('select')
								.keypress(checkOnValue)
								.change(checkOnValue);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});



</g:javascript>



