

<%@ page import="com.kombos.reception.Reception" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'reception.label', default: 'Reception')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteReception;

$(function(){ 
	deleteReception=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/reception/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadReceptionTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-reception" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="reception"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${receptionInstance?.t401NoWO}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401NoWO-label" class="property-label"><g:message
					code="reception.t401NoWO.label" default="T401 No WO" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401NoWO-label">
								<g:fieldValue bean="${receptionInstance}" field="t401NoWO"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TanggalWO}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TanggalWO-label" class="property-label"><g:message
					code="reception.t401TanggalWO.label" default="T401 Tanggal WO" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TanggalWO-label">
								<g:formatDate date="${receptionInstance?.t401TanggalWO}" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401StaReceptionEstimasiSalesQuotation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401StaReceptionEstimasiSalesQuotation-label" class="property-label"><g:message
					code="reception.t401StaReceptionEstimasiSalesQuotation.label" default="T401 Sta Reception Estimasi Sales Quotation" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401StaReceptionEstimasiSalesQuotation-label">
								<g:fieldValue bean="${receptionInstance}" field="t401StaReceptionEstimasiSalesQuotation"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.customerIn}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="customerIn-label" class="property-label"><g:message
					code="reception.customerIn.label" default="Customer In" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="customerIn-label">
						<g:link controller="customerIn" action="show" id="${receptionInstance?.customerIn?.id}">${receptionInstance?.customerIn?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.customerVehicle}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="customerVehicle-label" class="property-label"><g:message
					code="reception.customerVehicle.label" default="Customer Vehicle" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="customerVehicle-label">
						<g:link controller="customerVehicle" action="show" id="${receptionInstance?.customerVehicle?.id}">${receptionInstance?.customerVehicle?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.historyCustomerVehicle}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="historyCustomerVehicle-label" class="property-label"><g:message
					code="reception.historyCustomerVehicle.label" default="History Customer Vehicle" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="historyCustomerVehicle-label">
						<g:link controller="historyCustomerVehicle" action="show" id="${receptionInstance?.historyCustomerVehicle?.id}">${receptionInstance?.historyCustomerVehicle?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.operation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="operation-label" class="property-label"><g:message
					code="reception.operation.label" default="Operation" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="operation-label">
						<g:link controller="operation" action="show" id="${receptionInstance?.operation?.id}">${receptionInstance?.operation?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.namaManPower}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaManPower-label" class="property-label"><g:message
					code="reception.namaManPower.label" default="Nama Man Power" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaManPower-label">
						<g:link controller="namaManPower" action="show" id="${receptionInstance?.namaManPower?.id}">${receptionInstance?.namaManPower?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.customer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="customer-label" class="property-label"><g:message
					code="reception.customer.label" default="Customer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="customer-label">
						<g:link controller="customer" action="show" id="${receptionInstance?.customer?.id}">${receptionInstance?.customer?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.historyCustomer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="historyCustomer-label" class="property-label"><g:message
					code="reception.historyCustomer.label" default="History Customer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="historyCustomer-label">
						<g:link controller="historyCustomer" action="show" id="${receptionInstance?.historyCustomer?.id}">${receptionInstance?.historyCustomer?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401StaApp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401StaApp-label" class="property-label"><g:message
					code="reception.t401StaApp.label" default="T401 Sta App" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401StaApp-label">
						<g:fieldValue bean="${receptionInstance}" field="t401StaApp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401NoAppointment}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401NoAppointment-label" class="property-label"><g:message
					code="reception.t401NoAppointment.label" default="T401 No Appointment" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401NoAppointment-label">
						<g:fieldValue bean="${receptionInstance}" field="t401NoAppointment"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401StaKategoriCustUmumSPKAsuransi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401StaKategoriCustUmumSPKAsuransi-label" class="property-label"><g:message
					code="reception.t401StaKategoriCustUmumSPKAsuransi.label" default="T401 Sta Kategori Cust Umum SPKA suransi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401StaKategoriCustUmumSPKAsuransi-label">
						<g:fieldValue bean="${receptionInstance}" field="t401StaKategoriCustUmumSPKAsuransi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401BawaSPK}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401BawaSPK-label" class="property-label"><g:message
					code="reception.t401BawaSPK.label" default="T401 Bawa SPK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401BawaSPK-label">
						<g:fieldValue bean="${receptionInstance}" field="t401BawaSPK"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.sPK}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="sPK-label" class="property-label"><g:message
					code="reception.sPK.label" default="SPK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="sPK-label">
						<g:link controller="SPK" action="show" id="${receptionInstance?.sPK?.id}">${receptionInstance?.sPK?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401BawaAsuransi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401BawaAsuransi-label" class="property-label"><g:message
					code="reception.t401BawaAsuransi.label" default="T401 Bawa Asuransi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401BawaAsuransi-label">
						<g:fieldValue bean="${receptionInstance}" field="t401BawaAsuransi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.sPkAsuransi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="sPkAsuransi-label" class="property-label"><g:message
					code="reception.sPkAsuransi.label" default="SP k Asuransi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="sPkAsuransi-label">
						<g:link controller="SPKAsuransi" action="show" id="${receptionInstance?.sPkAsuransi?.id}">${receptionInstance?.sPkAsuransi?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TglJamCetakWO}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TglJamCetakWO-label" class="property-label"><g:message
					code="reception.t401TglJamCetakWO.label" default="T401 Tgl Jam Cetak WO" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TglJamCetakWO-label">
						<g:formatDate date="${receptionInstance?.t401TglJamCetakWO}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401StaAmbilWO}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401StaAmbilWO-label" class="property-label"><g:message
					code="reception.t401StaAmbilWO.label" default="T401 Sta Ambil WO" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401StaAmbilWO-label">
						<g:fieldValue bean="${receptionInstance}" field="t401StaAmbilWO"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TglJamAmbilWO}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TglJamAmbilWO-label" class="property-label"><g:message
					code="reception.t401TglJamAmbilWO.label" default="T401 Tgl Jam Ambil WO" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TglJamAmbilWO-label">
						<g:formatDate date="${receptionInstance?.t401TglJamAmbilWO}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401PermintaanCust}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401PermintaanCust-label" class="property-label"><g:message
					code="reception.t401PermintaanCust.label" default="T401 Permintaan Cust" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401PermintaanCust-label">
						<g:fieldValue bean="${receptionInstance}" field="t401PermintaanCust"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401StaTinggalMobil}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401StaTinggalMobil-label" class="property-label"><g:message
					code="reception.t401StaTinggalMobil.label" default="T401 Sta Tinggal Mobil" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401StaTinggalMobil-label">
						<g:fieldValue bean="${receptionInstance}" field="t401StaTinggalMobil"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401StaTinggalParts}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401StaTinggalParts-label" class="property-label"><g:message
					code="reception.t401StaTinggalParts.label" default="T401 Sta Tinggal Parts" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401StaTinggalParts-label">
						<g:fieldValue bean="${receptionInstance}" field="t401StaTinggalParts"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401StaCustomerTunggu}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401StaCustomerTunggu-label" class="property-label"><g:message
					code="reception.t401StaCustomerTunggu.label" default="T401 Sta Customer Tunggu" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401StaCustomerTunggu-label">
						<g:fieldValue bean="${receptionInstance}" field="t401StaCustomerTunggu"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401StaObatJalan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401StaObatJalan-label" class="property-label"><g:message
					code="reception.t401StaObatJalan.label" default="T401 Sta Obat Jalan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401StaObatJalan-label">
						<g:fieldValue bean="${receptionInstance}" field="t401StaObatJalan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TglStaObatJalan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TglStaObatJalan-label" class="property-label"><g:message
					code="reception.t401TglStaObatJalan.label" default="T401 Tgl Sta Obat Jalan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TglStaObatJalan-label">
						<g:formatDate date="${receptionInstance?.t401TglStaObatJalan}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401StaButuhSPKSebelumProd}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401StaButuhSPKSebelumProd-label" class="property-label"><g:message
					code="reception.t401StaButuhSPKSebelumProd.label" default="T401 Sta Butuh SPKS ebelum Prod" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401StaButuhSPKSebelumProd-label">
						<g:fieldValue bean="${receptionInstance}" field="t401StaButuhSPKSebelumProd"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TglJamKirimBerkasAsuransi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TglJamKirimBerkasAsuransi-label" class="property-label"><g:message
					code="reception.t401TglJamKirimBerkasAsuransi.label" default="T401 Tgl Jam Kirim Berkas Asuransi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TglJamKirimBerkasAsuransi-label">
						<g:formatDate date="${receptionInstance?.t401TglJamKirimBerkasAsuransi}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TglJamJanjiPenyerahan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TglJamJanjiPenyerahan-label" class="property-label"><g:message
					code="reception.t401TglJamJanjiPenyerahan.label" default="T401 Tgl Jam Janji Penyerahan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TglJamJanjiPenyerahan-label">
						<g:formatDate date="${receptionInstance?.t401TglJamJanjiPenyerahan}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TglJamRencana}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TglJamRencana-label" class="property-label"><g:message
					code="reception.t401TglJamRencana.label" default="T401 Tgl Jam Rencana" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TglJamRencana-label">
						<g:formatDate date="${receptionInstance?.t401TglJamRencana}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TglJamJanjiBayarDP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TglJamJanjiBayarDP-label" class="property-label"><g:message
					code="reception.t401TglJamJanjiBayarDP.label" default="T401 Tgl Jam Janji Bayar DP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TglJamJanjiBayarDP-label">
						<g:formatDate date="${receptionInstance?.t401TglJamJanjiBayarDP}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TglJamPenyerahan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TglJamPenyerahan-label" class="property-label"><g:message
					code="reception.t401TglJamPenyerahan.label" default="T401 Tgl Jam Penyerahan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TglJamPenyerahan-label">
						<g:formatDate date="${receptionInstance?.t401TglJamPenyerahan}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401StaOkCancelReSchedule}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401StaOkCancelReSchedule-label" class="property-label"><g:message
					code="reception.t401StaOkCancelReSchedule.label" default="T401 Sta Ok Cancel Re Schedule" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401StaOkCancelReSchedule-label">
						<g:fieldValue bean="${receptionInstance}" field="t401StaOkCancelReSchedule"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TglJamPenyerahanReSchedule}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TglJamPenyerahanReSchedule-label" class="property-label"><g:message
					code="reception.t401TglJamPenyerahanReSchedule.label" default="T401 Tgl Jam Penyerahan Re Schedule" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TglJamPenyerahanReSchedule-label">
						<g:formatDate date="${receptionInstance?.t401TglJamPenyerahanReSchedule}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401AlasanReSchedule}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401AlasanReSchedule-label" class="property-label"><g:message
					code="reception.t401AlasanReSchedule.label" default="T401 Alasan Re Schedule" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401AlasanReSchedule-label">
						<g:fieldValue bean="${receptionInstance}" field="t401AlasanReSchedule"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401GambarWAC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401GambarWAC-label" class="property-label"><g:message
					code="reception.t401GambarWAC.label" default="T401 Gambar WAC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401GambarWAC-label">
						
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401CatatanWAC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401CatatanWAC-label" class="property-label"><g:message
					code="reception.t401CatatanWAC.label" default="T401 Catatan WAC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401CatatanWAC-label">
						<g:fieldValue bean="${receptionInstance}" field="t401CatatanWAC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401KmSaatIni}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401KmSaatIni-label" class="property-label"><g:message
					code="reception.t401KmSaatIni.label" default="T401 Km Saat Ini" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401KmSaatIni-label">
						<g:fieldValue bean="${receptionInstance}" field="t401KmSaatIni"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401Bensin}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401Bensin-label" class="property-label"><g:message
					code="reception.t401Bensin.label" default="T401 Bensin" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401Bensin-label">
						<g:fieldValue bean="${receptionInstance}" field="t401Bensin"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401Catatan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401Catatan-label" class="property-label"><g:message
					code="reception.t401Catatan.label" default="T401 Catatan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401Catatan-label">
						<g:fieldValue bean="${receptionInstance}" field="t401Catatan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.tipeKerusakan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tipeKerusakan-label" class="property-label"><g:message
					code="reception.tipeKerusakan.label" default="Tipe Kerusakan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tipeKerusakan-label">
						<g:link controller="tipeKerusakan" action="show" id="${receptionInstance?.tipeKerusakan?.id}">${receptionInstance?.tipeKerusakan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401StaTPSLine}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401StaTPSLine-label" class="property-label"><g:message
					code="reception.t401StaTPSLine.label" default="T401 Sta TPSL ine" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401StaTPSLine-label">
						<g:fieldValue bean="${receptionInstance}" field="t401StaTPSLine"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.colorMatchingKlasifikasi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="colorMatchingKlasifikasi-label" class="property-label"><g:message
					code="reception.colorMatchingKlasifikasi.label" default="Color Matching Klasifikasi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="colorMatchingKlasifikasi-label">
						<g:link controller="colorMatching" action="show" id="${receptionInstance?.colorMatchingKlasifikasi?.id}">${receptionInstance?.colorMatchingKlasifikasi?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.colorMatchingJmlPanel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="colorMatchingJmlPanel-label" class="property-label"><g:message
					code="reception.colorMatchingJmlPanel.label" default="Color Matching Jml Panel" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="colorMatchingJmlPanel-label">
						<g:link controller="colorMatching" action="show" id="${receptionInstance?.colorMatchingJmlPanel?.id}">${receptionInstance?.colorMatchingJmlPanel?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401Proses1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401Proses1-label" class="property-label"><g:message
					code="reception.t401Proses1.label" default="T401 Proses1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401Proses1-label">
						<g:fieldValue bean="${receptionInstance}" field="t401Proses1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401Proses2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401Proses2-label" class="property-label"><g:message
					code="reception.t401Proses2.label" default="T401 Proses2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401Proses2-label">
						<g:fieldValue bean="${receptionInstance}" field="t401Proses2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401Proses3}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401Proses3-label" class="property-label"><g:message
					code="reception.t401Proses3.label" default="T401 Proses3" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401Proses3-label">
						<g:fieldValue bean="${receptionInstance}" field="t401Proses3"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401Proses4}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401Proses4-label" class="property-label"><g:message
					code="reception.t401Proses4.label" default="T401 Proses4" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401Proses4-label">
						<g:fieldValue bean="${receptionInstance}" field="t401Proses4"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401Proses5}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401Proses5-label" class="property-label"><g:message
					code="reception.t401Proses5.label" default="T401 Proses5" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401Proses5-label">
						<g:fieldValue bean="${receptionInstance}" field="t401Proses5"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TotalProses}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TotalProses-label" class="property-label"><g:message
					code="reception.t401TotalProses.label" default="T401 Total Proses" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TotalProses-label">
						<g:fieldValue bean="${receptionInstance}" field="t401TotalProses"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401HP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401HP-label" class="property-label"><g:message
					code="reception.t401HP.label" default="T401 HP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401HP-label">
						<g:fieldValue bean="${receptionInstance}" field="t401HP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401HargaRp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401HargaRp-label" class="property-label"><g:message
					code="reception.t401HargaRp.label" default="T401 Harga Rp" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401HargaRp-label">
						<g:fieldValue bean="${receptionInstance}" field="t401HargaRp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401DiscRp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401DiscRp-label" class="property-label"><g:message
					code="reception.t401DiscRp.label" default="T401 Disc Rp" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401DiscRp-label">
						<g:fieldValue bean="${receptionInstance}" field="t401DiscRp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401DiscPersen}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401DiscPersen-label" class="property-label"><g:message
					code="reception.t401DiscPersen.label" default="T401 Disc Persen" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401DiscPersen-label">
						<g:fieldValue bean="${receptionInstance}" field="t401DiscPersen"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TotalRp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TotalRp-label" class="property-label"><g:message
					code="reception.t401TotalRp.label" default="T401 Total Rp" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TotalRp-label">
						<g:fieldValue bean="${receptionInstance}" field="t401TotalRp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401DPRp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401DPRp-label" class="property-label"><g:message
					code="reception.t401DPRp.label" default="T401 DPR p" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401DPRp-label">
						<g:fieldValue bean="${receptionInstance}" field="t401DPRp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TagihanRp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TagihanRp-label" class="property-label"><g:message
					code="reception.t401TagihanRp.label" default="T401 Tagihan Rp" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TagihanRp-label">
						<g:fieldValue bean="${receptionInstance}" field="t401TagihanRp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401SpDiscRp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401SpDiscRp-label" class="property-label"><g:message
					code="reception.t401SpDiscRp.label" default="T401 Sp Disc Rp" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401SpDiscRp-label">
						<g:fieldValue bean="${receptionInstance}" field="t401SpDiscRp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.alasanCancel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="alasanCancel-label" class="property-label"><g:message
					code="reception.alasanCancel.label" default="Alasan Cancel" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="alasanCancel-label">
						<g:link controller="alasanCancel" action="show" id="${receptionInstance?.alasanCancel?.id}">${receptionInstance?.alasanCancel?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401NamaSA}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401NamaSA-label" class="property-label"><g:message
					code="reception.t401NamaSA.label" default="T401 Nama SA" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401NamaSA-label">
						<g:fieldValue bean="${receptionInstance}" field="t401NamaSA"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401staReminderSBE}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401staReminderSBE-label" class="property-label"><g:message
					code="reception.t401staReminderSBE.label" default="T401sta Reminder SBE" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401staReminderSBE-label">
						<g:fieldValue bean="${receptionInstance}" field="t401staReminderSBE"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TglJamNotifikasi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TglJamNotifikasi-label" class="property-label"><g:message
					code="reception.t401TglJamNotifikasi.label" default="T401 Tgl Jam Notifikasi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TglJamNotifikasi-label">
						<g:formatDate date="${receptionInstance?.t401TglJamNotifikasi}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401StaPDI}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401StaPDI-label" class="property-label"><g:message
					code="reception.t401StaPDI.label" default="T401 Sta PDI" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401StaPDI-label">
						<g:fieldValue bean="${receptionInstance}" field="t401StaPDI"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401StaInvoice}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401StaInvoice-label" class="property-label"><g:message
					code="reception.t401StaInvoice.label" default="T401 Sta Invoice" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401StaInvoice-label">
						<g:fieldValue bean="${receptionInstance}" field="t401StaInvoice"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401NamaSAPenerimaBPCenter}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401NamaSAPenerimaBPCenter-label" class="property-label"><g:message
					code="reception.t401NamaSAPenerimaBPCenter.label" default="T401 Nama SAP enerima BPC enter" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401NamaSAPenerimaBPCenter-label">
						<g:fieldValue bean="${receptionInstance}" field="t401NamaSAPenerimaBPCenter"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401NoExplanation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401NoExplanation-label" class="property-label"><g:message
					code="reception.t401NoExplanation.label" default="T401 No Explanation" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401NoExplanation-label">
						<g:fieldValue bean="${receptionInstance}" field="t401NoExplanation"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TglJamExplanation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TglJamExplanation-label" class="property-label"><g:message
					code="reception.t401TglJamExplanation.label" default="T401 Tgl Jam Explanation" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TglJamExplanation-label">
						<g:formatDate date="${receptionInstance?.t401TglJamExplanation}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401TglJamExplanationSelesai}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401TglJamExplanationSelesai-label" class="property-label"><g:message
					code="reception.t401TglJamExplanationSelesai.label" default="T401 Tgl Jam Explanation Selesai" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401TglJamExplanationSelesai-label">
						<g:formatDate date="${receptionInstance?.t401TglJamExplanationSelesai}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401NamaSAExplaination}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401NamaSAExplaination-label" class="property-label"><g:message
					code="reception.t401NamaSAExplaination.label" default="T401 Nama SAE xplaination" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401NamaSAExplaination-label">
						<g:fieldValue bean="${receptionInstance}" field="t401NamaSAExplaination"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.stall}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="stall-label" class="property-label"><g:message
					code="reception.stall.label" default="Stall" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="stall-label">
						<g:link controller="stall" action="show" id="${receptionInstance?.stall?.id}">${receptionInstance?.stall?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401Area}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401Area-label" class="property-label"><g:message
					code="reception.t401Area.label" default="T401 Area" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401Area-label">
						<g:fieldValue bean="${receptionInstance}" field="t401Area"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.repairDifficulty}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="repairDifficulty-label" class="property-label"><g:message
					code="reception.repairDifficulty.label" default="Repair Difficulty" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="repairDifficulty-label">
						<g:link controller="repairDifficulty" action="show" id="${receptionInstance?.repairDifficulty?.id}">${receptionInstance?.repairDifficulty?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.bumperPaintngTimeCompanyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="bumperPaintngTimeCompanyDealer-label" class="property-label"><g:message
					code="reception.bumperPaintngTimeCompanyDealer.label" default="Bumper Paintng Time Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="bumperPaintngTimeCompanyDealer-label">
						<g:link controller="bumperPaintingTime" action="show" id="${receptionInstance?.bumperPaintngTimeCompanyDealer?.id}">${receptionInstance?.bumperPaintngTimeCompanyDealer?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.bumperPaintngTimeTglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="bumperPaintngTimeTglBerlaku-label" class="property-label"><g:message
					code="reception.bumperPaintngTimeTglBerlaku.label" default="Bumper Paintng Time Tgl Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="bumperPaintngTimeTglBerlaku-label">
						<g:link controller="bumperPaintingTime" action="show" id="${receptionInstance?.bumperPaintngTimeTglBerlaku?.id}">${receptionInstance?.bumperPaintngTimeTglBerlaku?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.bumperPaintngTimeMasterPanel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="bumperPaintngTimeMasterPanel-label" class="property-label"><g:message
					code="reception.bumperPaintngTimeMasterPanel.label" default="Bumper Paintng Time Master Panel" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="bumperPaintngTimeMasterPanel-label">
						<g:link controller="bumperPaintingTime" action="show" id="${receptionInstance?.bumperPaintngTimeMasterPanel?.id}">${receptionInstance?.bumperPaintngTimeMasterPanel?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401xKet}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401xKet-label" class="property-label"><g:message
					code="reception.t401xKet.label" default="T401x Ket" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401xKet-label">
						<g:fieldValue bean="${receptionInstance}" field="t401xKet"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401xNamaUser}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401xNamaUser-label" class="property-label"><g:message
					code="reception.t401xNamaUser.label" default="T401x Nama User" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401xNamaUser-label">
						<g:fieldValue bean="${receptionInstance}" field="t401xNamaUser"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.t401xDivisi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t401xDivisi-label" class="property-label"><g:message
					code="reception.t401xDivisi.label" default="T401x Divisi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t401xDivisi-label">
						<g:fieldValue bean="${receptionInstance}" field="t401xDivisi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.jpb}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jpb-label" class="property-label"><g:message
					code="reception.jpb.label" default="Jpb" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jpb-label">
						<g:link controller="JPB" action="show" id="${receptionInstance?.jpb?.id}">${receptionInstance?.jpb?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="reception.staDel.label" default="T401 Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						<g:fieldValue bean="${receptionInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="reception.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						<g:fieldValue bean="${receptionInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="reception.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						<g:fieldValue bean="${receptionInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="reception.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						<g:fieldValue bean="${receptionInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="reception.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						<g:formatDate date="${receptionInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${receptionInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="reception.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						<g:formatDate date="${receptionInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${receptionInstance?.id}"
					update="[success:'reception-form',failure:'reception-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteReception('${receptionInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
