<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="job_order_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%"
	style="margin-left: 0px; width: 930px;"
	>
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nama Job</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Rate</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Status Warranty</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nominal</div>
			</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="3"><span>TOTAL SEBELUM DISCOUNT</span></td>
			<td><span class="pull-right numeric" id="lblnominaljoborder"></span></td>
		</tr>
	</tfoot>
</table>

<g:javascript>
var joborderTable;
var reloadJoborderTable;
var selected_job;
$(function(){
	
	reloadJoborderTable = function() {
		joborderTable.fnDraw();
	}

	joborderTable = $('#job_order_datatables').dataTable({
		"sScrollX": "833px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t",
		bFilter: false,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			var aData = joborderTable.fnGetData(nRow);
			if(aData.staPart!="tidak"){
                $.ajax({type:'POST',
                    data : aData,
                    url:'${request.contextPath}/salesQuotation/partList',
                    success:function(data,textStatus){
                        var nDetailsRow = joborderTable.fnOpen(nRow,data,'details');
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
    	   		});
            }

            $('#lblnominaljoborder').text(aData.totalNominal);
            return nRow;
		},
		"fnDrawCallback":function(oSettings){
			if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
				$(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
			}
		},
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "jobnPartsOrderDatatablesList")}",
		"aoColumns": [
{
	"sName": "namaJob",
	"mDataProp": "namaJob",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['idJob']+'" title="Select this"><input type="hidden" value="'+row['idJob']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"527px",
	"bVisible": true
},{
	"sName": "rate",
	"mDataProp": "rate",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		if(data != 'null')
			return '<span class="pull-right numeric">'+data+'</span>';
		else 
			return '<span></span>';
	},
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"69px",
	"bVisible": true
},{
	"sName": "statusWarranty",
	"mDataProp": "statusWarranty",
	"aTargets": [2],
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"73px",
	"bVisible": true
},{
	"sName": "nominal",
	"mDataProp": "nominal",
	"aTargets": [3],
	"mRender": function ( data, type, row ) {
		if(data != 'null')
			return '<span class="pull-right numeric">'+data+'</span>';
		else 
			return '<span></span>';
	},
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"119px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						var kodeKota = $('#kodeKota');
			            var nomorTengah = $('#nomorTengah');
			            var nomorBelakang = $('#nomorBelakang');
			            if(kodeKota.val()){
			                aoData.push(
									{"name": 'sCriteria_kodeKota', "value": kodeKota.val()}
							);
			            }
			            if(nomorTengah.val()){
			                aoData.push(
									{"name": 'sCriteria_nomorTengah', "value": nomorTengah.val()}
							);
			            }
			            if(nomorBelakang.val()){
			                aoData.push(
									{"name": 'sCriteria_nomorBelakang', "value": nomorBelakang.val()}
							);
			            }
						
						var receptionId = $('#receptionId').val();
						
						aoData.push(
									{"name": 'sReceptionId', "value": receptionId}
						);
						
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>