<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="history_service_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div>Tanggal</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Job</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Parts</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Job Suggest</div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var historyServiceTable;
var reloadhistoryServiceTable;
$(function(){
	
	reloadhistoryServiceTable = function() {
		historyServiceTable.fnDraw();
	}

	historyServiceTable = $('#history_service_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t<'row-fluid'<'span12'p>>",
		bFilter: false,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		},
		"fnDrawCallback":function(oSettings){
			if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
				$(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
			}
		},
		"bSort": true,
		"bProcessing": false,
		"bServerSide": false,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		//"sAjaxSource": "{g.createLink(action: "historyServiceDatatablesList")}",
		"aoColumns": [
{
	"sName": "tanggal",
	"mDataProp": "tanggal",
	"aTargets": [0],
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},{
	"sName": "job",
	"mDataProp": "job",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		if(data != null){
			if($.isArray(data)){
				var res = '<ul style="margin-left: 10px;">';
				for(var i = 0; i < data.length; i++){
					res = res + '<li>'+data[i]+'</li>';
				}
				res = res + '</ul>';
				return res;
			} else {
				return '<span>'+data+'</span>';
			}
		}else 
			return '<span></span>';
	},
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
},{
	"sName": "parts",
	"mDataProp": "parts",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
		if(data != null){
			if($.isArray(data)){
				var res = '<ul style="margin-left: 10px;">';
				for(var i = 0; i < data.length; i++){
					res = res + '<li>'+data[i]+'</li>';
				}
				res = res + '</ul>';
				return res;
			} else {
				return '<span>'+data+'</span>';
			}
		}else 
			return '<span></span>';
	},
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
},{
	"sName": "jobSuggest",
	"mDataProp": "jobSuggest",
	"aTargets": [3],
	"mRender": function ( data, type, row ) {
		if(data != null){
			if($.isArray(data)){
				var res = '<ul style="margin-left: 10px;">';
				for(var i = 0; i < data.length; i++){
					res = res + '<li>'+data[i]+'</li>';
				}
				res = res + '</ul>';
				return res;
			} else {
				return '<span>'+data+'</span>';
			}
		}else 
			return '<span></span>';
	},
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
