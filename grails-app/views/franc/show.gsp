

<%@ page import="com.kombos.parts.Franc" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'franc.label', default: 'Franchise')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteFranc;

$(function(){ 
	deleteFranc=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/franc/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadFrancTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-franc" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="franc"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${francInstance?.m117ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m117ID-label" class="property-label"><g:message
					code="franc.m117ID.label" default="Kode Franchise" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m117ID-label">
						%{--<ba:editableValue
								bean="${francInstance}" field="m117ID"
								url="${request.contextPath}/Franc/updatefield" type="text"
								title="Enter m117ID" onsuccess="reloadFrancTable();" />--}%
							
								<g:fieldValue bean="${francInstance}" field="m117ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${francInstance?.m117NamaFranc}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m117NamaFranc-label" class="property-label"><g:message
					code="franc.m117NamaFranc.label" default="Nama Franchise" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m117NamaFranc-label">
						%{--<ba:editableValue
								bean="${francInstance}" field="m117NamaFranc"
								url="${request.contextPath}/Franc/updatefield" type="text"
								title="Enter m117NamaFranc" onsuccess="reloadFrancTable();" />--}%
							
								<g:fieldValue bean="${francInstance}" field="m117NamaFranc"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${francInstance?.m117Ket}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m117Ket-label" class="property-label"><g:message
					code="franc.m117Ket.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m117Ket-label">
						%{--<ba:editableValue
								bean="${francInstance}" field="m117Ket"
								url="${request.contextPath}/Franc/updatefield" type="text"
								title="Enter m117Ket" onsuccess="reloadFrancTable();" />--}%
							
								<g:fieldValue bean="${francInstance}" field="m117Ket"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${francInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="franc.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${francInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Franc/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadFrancTable();" />--}%

                        <g:fieldValue bean="${francInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${francInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="franc.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${francInstance}" field="dateCreated"
                                url="${request.contextPath}/Franc/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadFrancTable();" />--}%

                        <g:formatDate date="${francInstance?.dateCreated}" type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${francInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="franc.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${francInstance}" field="createdBy"
								url="${request.contextPath}/Franc/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadFrancTable();" />--}%
							
								<g:fieldValue bean="${francInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${francInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="franc.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${francInstance}" field="lastUpdated"
								url="${request.contextPath}/Franc/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadFrancTable();" />--}%
							
								<g:formatDate date="${francInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${francInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="franc.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${francInstance}" field="updatedBy"
                                url="${request.contextPath}/Franc/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadFrancTable();" />--}%

                        <g:fieldValue bean="${francInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${francInstance?.id}"
					update="[success:'franc-form',failure:'franc-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteFranc('${francInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
