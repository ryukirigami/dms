
<%@ page import="com.kombos.parts.Franc" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="franc_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="franc.m117ID.label" default="Kode Franchise" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="franc.m117NamaFranc.label" default="Nama Franchise" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="franc.m117Ket.label" default="Keterangan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="franc.m117NomorAkun.label" default="Nomor Akun" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="franc.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="franc.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m117ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m117ID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m117NamaFranc" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m117NamaFranc" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m117Ket" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m117Ket" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m117NomorAkun" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m117NomorAkun" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var francTable;
var reloadFrancTable;
$(function(){
	
	reloadFrancTable = function() {
		francTable.fnDraw();
	}

	var recordsFrancPerPage = [];
    var anFrancSelected;
    var jmlRecFrancPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	francTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	francTable = $('#franc_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsFranc = $("#franc_datatables tbody .row-select");
            var jmlFrancCek = 0;
            var nRow;
            var idRec;
            rsFranc.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsFrancPerPage[idRec]=="1"){
                    jmlFrancCek = jmlFrancCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsFrancPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecFrancPerPage = rsFranc.length;
            if(jmlFrancCek==jmlRecFrancPerPage && jmlRecFrancPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m117ID",
	"mDataProp": "m117ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m117NamaFranc",
	"mDataProp": "m117NamaFranc",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m117Ket",
	"mDataProp": "m117Ket",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m117NomorAkun",
	"mDataProp": "m117NomorAkun",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m117ID = $('#filter_m117ID input').val();
						if(m117ID){
							aoData.push(
									{"name": 'sCriteria_m117ID', "value": m117ID}
							);
						}
	
						var m117NamaFranc = $('#filter_m117NamaFranc input').val();
						if(m117NamaFranc){
							aoData.push(
									{"name": 'sCriteria_m117NamaFranc', "value": m117NamaFranc}
							);
						}
	
						var m117Ket = $('#filter_m117Ket input').val();
						if(m117Ket){
							aoData.push(
									{"name": 'sCriteria_m117Ket', "value": m117Ket}
							);
						}

						var m117NomorAkun = $("#filter_m117NomorAkun input").val();
						if (m117NomorAkun) {
						    aoData.push(
						        {"name": "sCriteria_m117NomorAkun", "value": m117NomorAkun}
						    );
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#franc_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsFrancPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsFrancPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#franc_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsFrancPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anFrancSelected = francTable.$('tr.row_selected');

            if(jmlRecFrancPerPage == anFrancSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsFrancPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
