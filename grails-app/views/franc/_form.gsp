<%@ page import="com.kombos.finance.AccountNumber; com.kombos.parts.Franc" %>
<g:javascript>
    $(document).ready(function() {
        if($('#m117Ket').val().length>0){
            $('#hitung').text(255 - $('#m117Ket').val().length);
        }
        $('#m117Ket').keyup(function() {
            var len = this.value.length;
            if (len >= 255) {
                this.value = this.value.substring(0, 255);
                len = 255;
            }
            $('#hitung').text(255 - len);
        });
    });

    $(function(){
			$('#m117NomorAkun').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/franc/listAccount', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});

    function detailAccount(){
                var noAccount = $('#m117NomorAkun').val();
                $.ajax({
                    url:'${request.contextPath}/franc/detailAccount?noAccount='+noAccount,
                    type: "POST", // Always use POST when deleting data
                    success : function(data){
                        $('#idM117NomorAkun').val("");
                        $('#accountName').val("");
                        if(data.hasil=="ada"){
                            $('#idM117NomorAkun').val(data.id);
                            console.log("idM117NomorAkun : " + data.id)
                        }
                    }
                })
        }
</g:javascript>


<div class="control-group fieldcontain ${hasErrors(bean: francInstance, field: 'm117ID', 'error')} required">
	<label class="control-label" for="m117ID">
		<g:message code="franc.m117ID.label" default="Kode Franchise" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m117ID" maxlength="4" value="${francInstance.m117ID}" required=""/>
	</div>
</div>
<g:javascript>
    document.getElementById("m117ID").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: francInstance, field: 'm117NamaFranc', 'error')} required">
	<label class="control-label" for="m117NamaFranc">
		<g:message code="franc.m117NamaFranc.label" default="Nama Franchise" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m117NamaFranc" maxlength="50" required="" value="${francInstance?.m117NamaFranc}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: francInstance, field: 'm117Ket', 'error')} ">
	<label class="control-label" for="m117Ket">
		<g:message code="franc.m117Ket.label" default="Keterangan" />
		
	</label>
	<div class="controls">
	<g:textArea name="m117Ket" id="m117Ket" value="${francInstance?.m117Ket}"/>
        <br/>
        <span id="hitung">255</span> Karakter Tersisa.
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: francInstance, field: 'm117NomorAkun', 'error')} ">
    <label class="control-label" for="m117NomorAkun">
        <g:message code="franc.m117NomorAkun.label" default="Nomor Akun" />

    </label>
    <div class="controls">
        %{--<g:select name="m117NomorAkun.id" from="${AccountNumber.createCriteria().list(){eq("staDel","0") eq("accountMutationType", "MUTASI")}}"--}%
                  %{--noSelection="['':'']" optionKey="id" optionValue="${{it.accountNumber + " " + "(" + it.accountName + ")"}}" class="many-to-one" value="${francInstance?.m117NomorAkun?.id}" />--}%
        %{--<g:textField class="typeahead" autocomplete="off" id="m117NomorAkun" name="m117NomorAkun" optionKey="id" value="${francInstance?.m117NomorAkun}"/>--}%
        <g:textField name="nomorAkun" id="m117NomorAkun" class="typeahead" value="${francInstance?.m117NomorAkun?.accountNumber?francInstance?.m117NomorAkun?.accountNumber+' || '+francInstance?.m117NomorAkun?.accountName:""}" autocomplete="off" onblur="detailAccount();"/>
        <g:hiddenField name="m117NomorAkun.id" id="idM117NomorAkun"  value="${francInstance?.m117NomorAkun?.id}" />
    </div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: francInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="franc.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${francInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: francInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="franc.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${francInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: francInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="franc.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${francInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

