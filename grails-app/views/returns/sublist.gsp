<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var returnsSubTable_${idTable};
$(function(){ 
var anOpen = [];
	$('#returns_datatables_sub_${idTable} td.subcontrol').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = returnsSubTable_${idTable}.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/returns/subsublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = returnsSubTable_${idTable}.fnOpen(nTr,data,'details');
    				$('div.innerinnerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerinnerDetails', $(nTr).next()[0]).slideUp( function () {
      			returnsSubTable_${idTable}.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    $('#returns_datatables_sub_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( returnsSubTable_${idTable}, nEditing );
            editRow( returnsSubTable_${idTable}, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( returnsSubTable_${idTable}, nEditing );
            nEditing = null;
        }
        else {
            editRow( returnsSubTable_${idTable}, nRow );
            nEditing = nRow;
        }
    } );
    if(returnsSubTable_${idTable})
    	returnsSubTable_${idTable}.dataTable().fnDestroy();
returnsSubTable_${idTable} = $('#returns_datatables_sub_${idTable}').dataTable({
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "goodsReceive",
	"mDataProp": "t167NoReff",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this">  <input type="hidden" value="'+row['t172ID']+'.'+data+'">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"sWidth":"124px",
	"bVisible": true
},
{
	"sName": "goodsReceive",
	"mDataProp": "t167TglReff",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"300px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"200px",
	"sDefaultContent": '',
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'vendor', "value": "${vendor}"},
									{"name": 't172TglJamReturn', "value": "${t172TglJamReturn}"},
									{"name": 't172ID', "value": "${t172ID}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
    </g:javascript>
</head>
<body>
<div class="innerDetails">
    <table id="returns_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Nomor Reff</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Tanggal reff</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;" />
            <th style="border-bottom: none; padding: 5px; width: 200px;" />
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
