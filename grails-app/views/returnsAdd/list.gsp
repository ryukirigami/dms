
<%@ page import="com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'returnsAdd.label', default: 'Input Parts Return')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
        return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/ReturnsAdd/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/ReturnsAdd/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    loadForm = function(data, textStatus){
		$('#ReturnsAdd-form').empty();
    	$('#ReturnsAdd-form').append(data);
   	}

    shrinkTableLayout = function(){
    	if($("#ReturnsAdd-table").hasClass("span12")){
   			$("#ReturnsAdd-table").toggleClass("span12 span5");
        }
        $("#ReturnsAdd-form").css("display","block");
   	}

   	expandTableLayout = function(){
   		if($("#ReturnsAdd-table").hasClass("span5")){
   			$("#ReturnsAdd-table").toggleClass("span5 span12");
   		}
        $("#ReturnsAdd-form").css("display","none");
   	}
   	
   massDelete = function() {
   		var recordsToDelete = [];
		$("#ReturnsAdd-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});

		var json = JSON.stringify(recordsToDelete);

		$.ajax({
    		url:'${request.contextPath}/returnsAdd/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadRequestTable();
    		}
		});
		
   	}
   });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="ReturnsAdd-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <g:render template="dataTables" />
        <g:field type="button" onclick="tambahReq();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.upload.label', default: 'Add Selected')}" />
        <button id='closeSpkDetail' type="button" class="btn btn-primary pull-right">Close</button>
    </div>
    <div class="span7" id="ReturnsAdd-form" style="display: none;"></div>
</div>
</body>
</html>
