<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.hrd.TrainingType" %>

<div class="control-group fieldcontain ${hasErrors(bean: trainingTypeInstance, field: 'tipeTraining', 'error')} required">
	<label class="control-label" for="tipeTraining">
		<g:message code="trainingType.tipeTraining.label" default="Tipe Training " />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="tipeTraining" required="" value="${trainingTypeInstance?.tipeTraining}" size="32" maxlength="32"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingTypeInstance, field: 'singkatan', 'error')} ">
    <label class="control-label" for="singkatan">
        <g:message code="trainingType.singkatan.label" default="Singkatan" />

    </label>
    <div class="controls">
        <g:textField name="singkatan" value="${trainingTypeInstance?.singkatan}" maxlength="16"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingTypeInstance, field: 'bagian', 'error')} required">
	<label class="control-label" for="bagian">
		<g:message code="trainingType.bagian.label" default="Bagian " />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="bagian" name="bagian.id" from="${com.kombos.administrasi.Divisi.createCriteria().list {eq("companyDealer", CompanyDealer.findById(session.userCompanyDealerId))}}" optionKey="id" required="" value="${trainingTypeInstance?.bagian?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingTypeInstance, field: 'durasi', 'error')} ">
	<label class="control-label" for="durasi">
		<g:message code="trainingType.durasi.label" default="Durasi" />
		
	</label>
	<div class="controls">
	<g:textField name="durasi" value="${trainingTypeInstance?.durasi}" maxlength="16"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingTypeInstance, field: 'intEks', 'error')} ">
	<label class="control-label" for="intEks">
		<g:message code="trainingType.intEks.label" default="Int Eks" />
		
	</label>
	<div class="controls">
        <g:radioGroup name="intEks" id="intEks" value="${trainingTypeInstance?.intEks}" required=""
                      labels="['Internal TAM','Internal CVK','Eksternal']"
                      values="[1,2,3]">
            ${it.radio} ${it.label}
        </g:radioGroup>
	%{--<g:textField name="intEks" value="${trainingTypeInstance?.intEks}"/>--}%
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingTypeInstance, field: 'sertifikasi', 'error')} ">
	<label class="control-label" for="sertifikasi">
		<g:message code="trainingType.sertifikasi.label" default="Sertifikasi" />
		
	</label>
	<div class="controls">
        <g:radioGroup name="sertifikasi" id="sertifikasi" value="${trainingTypeInstance?.sertifikasi}" required=""
                      labels="['Ya','Tidak']"
                      values="[0,1]">
            ${it.radio} ${it.label}
        </g:radioGroup>
	%{--<g:textField name="sertifikasi" value="${trainingTypeInstance?.sertifikasi}"/>--}%
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingTypeInstance, field: 'tempatTraining', 'error')} ">
	<label class="control-label" for="tempatTraining">
		<g:message code="trainingType.tempatTraining.label" default="Tempat Training" />
		
	</label>
	<div class="controls">
	<g:textField name="tempatTraining" value="${trainingTypeInstance?.tempatTraining}" maxlength="64"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingTypeInstance, field: 'keterangan', 'error')} ">
    <label class="control-label" for="keterangan">
        <g:message code="trainingType.keterangan.label" default="Keterangan" />

    </label>
    <div class="controls">
        %{--<g:textArea name="keterangan" value="${trainingTypeInstance?.keterangan}"></g:textArea>--}%
        <g:textField name="keterangan" value="${trainingTypeInstance?.keterangan}" maxlength="64"/>
    </div>
</div>