
<%@ page import="com.kombos.hrd.TrainingType" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="trainingType_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="trainingType.tipeTraining.label" default="Tipe Training" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="trainingType.singkatan.label" default="Singkatan" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="trainingType.bagian.label" default="Bagian" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="trainingType.durasi.label" default="Durasi" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="trainingType.intEks.label" default="Int Eks" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="trainingType.sertifikasi.label" default="Sertifikasi" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="trainingType.tempatTraining.label" default="Tempat Training" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="trainingType.keterangan.label" default="Keterangan" /></div>
            </th>

		</tr>
		<tr>
		
			<th style="border-top: none;padding: 5px;">
				<div id="filter_tipeTraining" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_tipeTraining" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_singkatan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_singkatan" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
				<div id="filter_bagian" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_bagian" class="search_init" />
                    %{--<g:select id="bagian" name="bagian.id" from="${com.kombos.hrd.TrainingType.list()}}" optionKey="id" required="" value="${trainingTypeInstance?.bagian?.id}" class="many-to-one"/>--}%
                </div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_durasi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_durasi" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_intEks" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					%{--<input type="text" name="search_intEks" class="search_init" />--}%
                    <select name="search_intEks" id="search_intEks" style="width:100%" onchange="reloadTrainingTypeTable();">
                        <option value="">All</option>
                        <option value="1">Internal TAM</option>
                        <option value="2">Internal CVK</option>
                        <option value="3">Eksternal</option>
                    </select>
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_sertifikasi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					%{--<input type="text" name="search_sertifikasi" class="search_init" />--}%
                    <select name="search_sertifikasi" id="search_sertifikasi" style="width:100%" onchange="reloadTrainingTypeTable();">
                        <option value="">All</option>
                        <option value="0">Ya</option>
                        <option value="1">Tidak</option>
                    </select>
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_tempatTraining" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_tempatTraining" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_keterangan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_keterangan" class="search_init" />
                </div>
            </th>

		</tr>
	</thead>
</table>

<g:javascript>
var trainingTypeTable;
var reloadTrainingTypeTable;
$(function(){
	
	reloadTrainingTypeTable = function() {
		trainingTypeTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	trainingTypeTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	trainingTypeTable = $('#trainingType_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "tipeTraining",
	"mDataProp": "tipeTraining",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "singkatan",
	"mDataProp": "singkatan",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "bagian",
	"mDataProp": "bagian",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "durasi",
	"mDataProp": "durasi",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "intEks",
	"mDataProp": "intEks",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "sertifikasi",
	"mDataProp": "sertifikasi",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tempatTraining",
	"mDataProp": "tempatTraining",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "keterangan",
	"mDataProp": "keterangan",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var tipeTraining = $('#filter_tipeTraining input').val();
						if(tipeTraining){
							aoData.push(
									{"name": 'sCriteria_tipeTraining', "value": tipeTraining}
							);
						}
	
						var singkatan = $('#filter_singkatan input').val();
						if(singkatan){
							aoData.push(
									{"name": 'sCriteria_singkatan', "value": singkatan}
							);
						}
	
						var bagian = $('#filter_bagian input').val();
						if(bagian){
							aoData.push(
									{"name": 'sCriteria_bagian', "value": bagian}
							);
						}
	
						var durasi = $('#filter_durasi input').val();
						if(durasi){
							aoData.push(
									{"name": 'sCriteria_durasi', "value": durasi}
							);
						}
	
						var intEks = $('#search_intEks').val();
						if(intEks){
							aoData.push(
									{"name": 'sCriteria_intEks', "value": intEks}
							);
						}
	
						var sertifikasi = $('#search_sertifikasi').val();
						if(sertifikasi){
							aoData.push(
									{"name": 'sCriteria_sertifikasi', "value": sertifikasi}
							);
						}
	
						var tempatTraining = $('#filter_tempatTraining input').val();
						if(tempatTraining){
							aoData.push(
									{"name": 'sCriteria_tempatTraining', "value": tempatTraining}
							);
						}

						var keterangan = $('#filter_keterangan input').val();
						if(keterangan){
							aoData.push(
									{"name": 'sCriteria_keterangan', "value": keterangan}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
