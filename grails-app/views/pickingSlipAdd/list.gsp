
<%@ page import="com.kombos.parts.PickingSlipDetail" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'SOQ.label', default: 'Input Picking Slip')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){


	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
        return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/pickingSlipInput/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/pickingSlipInput/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    loadForm = function(data, textStatus){
		$('#pickingSlip-form').empty();
    	$('#pickingSlip-form').append(data);
   	}

    shrinkTableLayout = function(){
    	if($("#pickingSlip-table").hasClass("span12")){
   			$("#pickingSlip-table").toggleClass("span12 span5");
        }
        $("#pickingSlip-form").css("display","block");
   	}

   	expandTableLayout = function(){
   		if($("#pickingSlip-table").hasClass("span5")){
   			$("#pickingSlip-table").toggleClass("span5 span12");
   		}
        $("#pickingSlip-form").css("display","none");
   	}

   	massDelete = function() {
   		var recordsToDelete = [];
		$("#pickingSlip-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});

		var json = JSON.stringify(recordsToDelete);

		$.ajax({
    		url:'${request.contextPath}/pickingSlipInput/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadPickingSlipTable();
    		}
		});
		
   	}
   });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"> Input Picking Slip</span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="pickingSlipAdd-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <g:render template="dataTables" />
        <g:field type="button" onclick="tambahPick();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.upload.label', default: 'Add Selected')}" />
        <button id='closeSpkDetail' style="margin-left: 700px" type="button" class="btn btn-cancel">Close</button>
    </div>
    <div class="span7" id="pickingSlipAdd-form" style="display: none;"></div>
</div>
</body>
</html>
