
<%@ page import="com.kombos.parts.PickingSlipDetail" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="pickingSlipAdd_datatables_${idTable}" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered  fixed" style="table-layout: fixed;width: 900px">
    <col width="200px" />
    <col width="250px" />
    <col width="100px" />
    <col width="100px" />
    <col width="100px" />
    <col width="100px" />
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px; width: 200px">
            <div>&nbsp; &nbsp; <input type="checkbox" class="pull-left select-all" aria-label="Select all" title="Select all"/>
                <g:message code="pickingSlipAdd.goods.label" default="Kode Parts" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px; width: 250px">
            <div><g:message code="pickingSlipAdd.goods.label" default="Nama Parts" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px; width: 100px">
            <div><g:message code="pickingSlipAdd.goods.label" default="Harga Beli" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px; width: 100px">
            <div><g:message code="pickingSlipAdd.goods.label" default="Harga Jual" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px; width: 100px">
            <div><g:message code="pickingSlipAdd.goods.label" default="Qty Hrs Di Ambil" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px; width: 100px">
            <div><g:message code="pickingSlipAdd.goods.label" default="Qty Gudang" /></div>
        </th>
    </tr>
    <tr>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;">
                <input type="text" name="search_goods" class="search_init" />
            </div>
         </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_goods2" style="padding-top: 0px;position:relative; margin-top: 0px;">
                <input type="text" name="search_goods2" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
        </th>

        <th style="border-top: none;padding: 5px;">
        </th>

        <th style="border-top: none;padding: 5px;">
        </th>

        <th style="border-top: none;padding: 5px;">
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var pickingSlipAddTable;
var reloadpickingSlipAddTable;
$(function(){

	reloadpickingSlipAddTable = function() {
		pickingSlipAddTable.fnDraw();
	}

var recordsClaimPerPage = [];
    var anClaimSelected;
    var jmlRecClaimPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	pickingSlipAddTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});
    if(pickingSlipAddTable)
        pickingSlipAddTable.dataTable().fnDestroy();
	pickingSlipAddTable = $('#pickingSlipAdd_datatables_${idTable}').dataTable({
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsClaim = $("#pickingSlipAdd_datatables_${idTable} tbody .row-select");
            var jmlClaimCek = 0;
            var nRow;
            var idRec;
            rsClaim.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsClaimPerPage[idRec]=="1"){
                    jmlClaimCek = jmlClaimCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsClaimPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecClaimPerPage = rsClaim.length;
            if(jmlClaimCek==jmlRecClaimPerPage && jmlRecClaimPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'"> &nbsp; '+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "goods2",
	"mDataProp": "goods2",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"250px",
	"bVisible": true
},


{
	"sName": "hargaBeli",
	"mDataProp": "hargaBeli",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},


{
	"sName": "hargaJual",
	"mDataProp": "hargaJual",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},


{
	"sName": "qtyHrsDiambil",
	"mDataProp": "qtyHrsDiambil",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},


{
	"sName": "stockGudang",
	"mDataProp": "stockGudang",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var tglpo = $('#search_tglpo').val();
						var tglpoDay = $('#search_tglpo_day').val();
						var tglpoMonth = $('#search_tglpo_month').val();
						var tglpoYear = $('#search_tglpo_year').val();

						aoData.push(
						    {"name": 'idWO', "value": "${idWO}"}
						);

						if(tglpo){
							aoData.push(
									{"name": 'sCriteria_tglpo', "value": "date.struct"},
									{"name": 'sCriteria_tglpo_dp', "value": tglpo},
									{"name": 'sCriteria_tglpo_day', "value": tglpoDay},
									{"name": 'sCriteria_tglpo_month', "value": tglpoMonth},
									{"name": 'sCriteria_tglpo_year', "value": tglpoYear}
							);
						}

						var tglinvoice = $('#search_tglinvoice').val();
						var tglinvoiceDay = $('#search_tglinvoice_day').val();
						var tglinvoiceMonth = $('#search_tglinvoice_month').val();
						var tglinvoiceYear = $('#search_tglinvoice_year').val();

						if(tglinvoice){
							aoData.push(
									{"name": 'sCriteria_tglinvoice', "value": "date.struct"},
									{"name": 'sCriteria_tglinvoice_dp', "value": tglinvoice},
									{"name": 'sCriteria_tglinvoice_day', "value": tglinvoiceDay},
									{"name": 'sCriteria_tglinvoice_month', "value": tglinvoiceMonth},
									{"name": 'sCriteria_tglinvoice_year', "value": tglinvoiceYear}
							);
						}
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
						var goods2 = $('#filter_goods2 input').val();
						if(goods2){
							aoData.push(
									{"name": 'sCriteria_goods2', "value": goods2}
							);
						}

                        var exist =[];
						var rows = $("#pickingSlip_datatables").dataTable().fnGetNodes();
                        for(var i=0;i<rows.length;i++)
                        {
							var id = $(rows[i]).find("#idPopUp").val();
							exist.push(id);
						}
						if(exist.length > 0){
							aoData.push(
										{"name": 'sCriteria_exist', "value": JSON.stringify(exist)}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#pickingSlipAdd_datatables_${idTable} tbody .row-select").each(function() {
            if(this.checked){
                recordsClaimPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsClaimPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#pickingSlipAdd_datatables_${idTable} tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsClaimPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anClaimSelected = pickingSlipAddTable.$('tr.row_selected');

            if(jmlRecClaimPerPage == anClaimSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsClaimPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>