
<%@ page import="com.kombos.parts.DeliveryOrder" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="deliveryOrder_datatables" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover table-selectable fixed" style="table-layout: fixed; ">
    <col width="250px" />
    <col width="250px" />
    <col width="250px" />
    <col width="200px" />
    <col width="120px" />
    <col width="100px" />
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="deliveryOrder.sorNumber.label" default="Nomor Sales Order" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="deliveryOrder.doNumber.label" default="Nomor Delivery Order" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="salesOrder.customerName.label" default="Nama Pelanggan" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="deliveryOrder.limitDate.label" default="DO Berlaku s.d." /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="deliveryOrder.intext.label" default="Internal / External" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="deliveryOrder.intext.label" default="Status" /></div>
        </th>

    </tr>
    <tr>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_sorNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 250px;">
                <input type="text" name="search_sorNumber" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_doNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 250px;">
                <input type="text" name="search_doNumber" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_customerName" style="padding-top: 0px;position:relative; margin-top: 0px;width: 250px;">
                <input type="text" name="search_customerName" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_limitDate" style="padding-top: 0px;position:relative; margin-top: 0px;width: 150px;" >
                <input type="hidden" name="search_tggllimitDate" value="date.struct">
                <input type="hidden" name="search_tggllimitDate_day" id="search_tggllimitDate_day" value="">
                <input type="hidden" name="search_tggllimitDate_month" id="search_tggllimitDate_month" value="">
                <input type="hidden" name="search_tggllimitDate_year" id="search_tggllimitDate_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_tggllimitDate_dp" value="" id="search_tggllimitDate" class="search_init">
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_intext" style="padding-top: 0px;position:relative; margin-top: 0px;width: 100px;">
                <input type="text" name="search_intext" class="search_init" />
            </div>
        </th>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_status" style="padding-top: 0px;position:relative; margin-top: 0px;width: 70px;">
                <input type="text" name="search_status" class="search_init" />
            </div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var deliveryOrderTable;
var reloadDeliveryOrderTable;
$(function(){
    var anOpen = [];
	$('#deliveryOrder_datatables td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = deliveryOrderTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/deliveryOrder/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = deliveryOrderTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			deliveryOrderTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

	reloadDeliveryOrderTable = function() {
		deliveryOrderTable.fnDraw();
	}

	$('#search_tggllimitDate').datepicker().on('changeDate', function(ev) {
    var newDate = new Date(ev.date);
    $('#search_tggllimitDate_day').val(newDate.getDate());
    $('#search_tggllimitDate_month').val(newDate.getMonth()+1);
    $('#search_tggllimitDate_year').val(newDate.getFullYear());
    $(this).datepicker('hide');
    deliveryOrderTable.fnDraw();
    })

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	deliveryOrderTable.fnDraw();
		}
	});


    deliveryOrderTable = $('#deliveryOrder_datatables').dataTable({
		"sScrollX": "99%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"bDestroy": true,
		"aoColumns": [

{
	"sName": "sorNumber",
	"mDataProp": "sorNumber",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="doId" value="'+row['id']+'">&nbsp;&nbsp;'+
	       '<a href="javascript:void(0);" onclick="showDetail('+row['id']+');">'+data+'</a>';
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "doNumber",
	"mDataProp": "doNumber",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "customerName",
	"mDataProp": "customerName",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "limitDate",
	"mDataProp": "limitDate",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "intext",
	"mDataProp": "intext",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"120px",
	"bVisible": true
}

,

{
	"sName": "status",
	"mDataProp": "status",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}

],
    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                 var sorNumber = $('#filter_sorNumber input').val();
				 if(sorNumber){
					aoData.push(
				    	{"name": 'sCriteria_sorNumber', "value": sorNumber}
					);
                 }
                 var status = $('#filter_status input').val();
				 if(status){
					aoData.push(
				    	{"name": 'sCriteria_status', "value": status}
					);
                 }

                 var doNumber = $('#filter_doNumber input').val();
				 if(doNumber){
					aoData.push(
				    	{"name": 'sCriteria_doNumber', "value": doNumber}
					);
                 }

                var customerName = $('#filter_customerName input').val();
				 if(customerName){
					aoData.push(
				    	{"name": 'sCriteria_customerName', "value": customerName}
					);
                 }


                var tggllimitDate = $('#search_tggllimitDate').val();
                var tggllimitDateDay = $('#search_tggllimitDate_day').val();
                var tggllimitDateMonth = $('#search_tggllimitDate_month').val();
                var tggllimitDateYear = $('#search_tggllimitDate_year').val();

                if(tggllimitDate){
                    aoData.push(
                            {"name": 'sCriteria_tggllimitDate', "value": "date.struct"},
                            {"name": 'sCriteria_tggllimitDate_dp', "value": tggllimitDate},
                            {"name": 'sCriteria_tggllimitDate_day', "value": tggllimitDateDay},
                            {"name": 'sCriteria_tggllimitDate_month', "value": tggllimitDateMonth},
                            {"name": 'sCriteria_tggllimitDate_year', "value": tggllimitDateYear}
                    );
                }

                 var intext = $('#filter_intext input').val();
				 if(intext){
					aoData.push(
				    	{"name": 'sCriteria_intext', "value": intext}
					);
                 }

                    $.ajax({ "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData ,
                        "success": function (json) {
                            fnCallback(json);
                           },
                        "complete": function () {
                           }
    });
}
});
});


</g:javascript>