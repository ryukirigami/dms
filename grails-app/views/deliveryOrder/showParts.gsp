<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 05/12/14
  Time: 16:31
--%>
<%@ page import="com.kombos.parts.Konversi; com.kombos.parts.Franc; com.kombos.parts.KlasifikasiGoods; com.kombos.parts.DeliveryOrder" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title>Show Detail</title>
    <r:require modules="baseapplayout, baseapplist" />
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="deliveryOrder.formAddParts.label" default="Show Detail" /></span>
</div>

<div class="box" style="border: none;">
    <div class="span12">
        <div class="row-fluid">
            <div class="span6">
                <div id="penerimaan-form" class="form-horizontal">
                    <fieldset class="form">
                        <div class="control-group fieldcontain">
                            <label for="kodeJurnal" class="control-label">
                                Nama Pelanggan
                            </label>
                            <div class="controls">
                                <input type="text" value="${sorNumber?.customerName}"   readonly>
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="kodeJurnal" class="control-label">
                                Tanggal SO
                            </label>
                            <div class="controls">
                                <input type="text" value="${sorNumber?.deliveryDate.format('dd/MM/yyyy')}"   readonly>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="span6">
                <div class="form-horizontal">
                    <fieldset class="form">
                        <div class="control-group fieldcontain">
                            <label for="totalDebet" class="control-label">Penjualan Untuk</label>
                            <div class="controls">
                                <input type="text" value="${sorNumber?.tradingFor}"   readonly>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="form-horizontal">
                    <fieldset class="form">
                        <div class="control-group fieldcontain">
                            <label for="totalDebet" class="control-label">No SO</label>
                            <div class="controls">
                                <input type="text" value="${sorNumber?.sorNumber}"   readonly>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
    <div class="span12" id="detail-form" style="display: block; margin-top: 20px;">
        <legend>Detail</legend>
        <table id="detail-table" class="table table-bordered">
            <thead>
            <th>No</th>
            <th>Kode Goods</th>
            <th>Nama Goods</th>
            <th>Qty</th>
            <th>Harga perUnit</th>
            <th>Satuan</th>
            <th>Disc</th>
            <th>Jumlah</th>
            </thead>
            <tbody>
            <g:if test="${sorNumberDetail}">
                <% def konv  =  new Konversi()
                def totalAll = 0
                %>
                <g:each in="${sorNumberDetail}" status="i" var="j" >
                    <% def harga =  j?.unitPrice * j.quantity
                       def disc =  harga * j?.discount
                       def total =  harga - disc
                       totalAll += total
                    %>
                    <tr>
                        <td>${i + 1}</td>
                        <td>${j?.materialCode?.m111ID}</td>
                        <td>${j?.materialCode?.m111Nama}</td>
                        <td>${j?.quantity}</td>
                        <td>${konv.toRupiah2(j?.unitPrice)}</td>
                        <td>${j?.materialCode?.satuan}</td>
                        <td>${j?.discount}</td>
                        <td>${konv.toRupiah2(total)}</td>
                    </tr>
                </g:each>
                <tr style="background-color: #F1F0F0">
                    <td> </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                    <td> Jumlah </td>
                    <td colspan="7">${konv.toRupiah2(totalAll)}</td>
                </tr>
            </g:if>
            </tbody>
        </table>
        <button class="btn cancel" data-dismiss="modal" name="close" id="closeTrxDetail" style="margin-top: 13px;">Tutup</button>
    </div>
</div>
</body>
</html>
