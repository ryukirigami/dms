
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="goods_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped fixed table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.m111ID.label" default="Kode Goods" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.m111Nama.label" default="Nama Goods" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.qty.label" default="Kuantitas" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.hargasatuan.label" default="Harga Per Unit" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.satuan.label" default="Satuan" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.qtyStock.label" default="Pemotongan Harga (%)" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.satuan.label" default="Jumlah Material" /></div>
        </th>


    </tr>
    </thead>
    <tfoot>
    <tr>
        <td colspan="6"><span>Total (Total Jumlah Material - PPN)</span></td>
        <td ><span id="total" style="text-align: right; font-weight: bold">0</span></td>
    </tr>
    </tfoot>
</table>
<style type="text/css">
. myClass{ text-align: right;font-weight:bold }
</style>
<g:javascript>
var goodsTable;
var reloadGoodsTable;
var idSOParam = "-1";
$(function(){

	reloadGoodsTable = function(idSOReload) {
	    idSOParam = idSOReload;
		goodsTable.fnDraw();
	}

	goodsTable = $('#goods_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSOList")}",
		"aoColumns": [

{
	"sName": "m111IDAdd",
	"mDataProp": "m111IDAdd",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "m111NamaAdd",
	"mDataProp": "m111NamaAdd",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"450px",
	"bVisible": true
}

,

{
	"sName": "qtyAdd",
	"mDataProp": "qtyAdd",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "hargaAdd",
	"mDataProp": "hargaAdd",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "satuanAdd",
	"mDataProp": "satuanAdd",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "discountAdd",
	"mDataProp": "discountAdd",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "jumlahAdd",
	"mDataProp": "jumlahAdd",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"mRender": function ( data, type, row ) {
        return '<span style="text-align:right">'+data+'</span>';
	},
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        aoData.push(
                                {"name": 'idSO', "value": idSOParam}
                        );

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
							    $('#total').text(json.total);
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>