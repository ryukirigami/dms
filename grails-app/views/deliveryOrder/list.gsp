
<%@ page import="com.kombos.parts.DeliveryOrder" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'deliveryOrder.label', default: 'Delivery Order')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			$(function(){ 
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :
							loadPath("deliveryOrder/formAddParts");
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
//										massDelete('deliveryOrder', '${request.contextPath}/deliveryOrder/massdelete', reloadDeliveryOrderTable);
                                        massDelete();
									}
								});                    
							
							break;
				   }    
				   return false;
				});

                massDelete = function() {
                    var recordsToDelete = [];
                    $("#deliveryOrder-table tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            recordsToDelete.push(id);
                        }

                    });

                    var json = JSON.stringify(recordsToDelete);

                    $.ajax({
                        url:'${request.contextPath}/deliveryOrder/massdelete',
                        type: "POST", // Always use POST when deleting data
                        data: { ids: json },
                        complete: function(xhr, status) {
                            reloadDeliveryOrderTable();
                        }
                    });
                }

				show = function(id) {
					showInstance('deliveryOrder','${request.contextPath}/deliveryOrder/show/'+id);
				};
				
				edit = function(id) {
					editInstance('deliveryOrder','${request.contextPath}/deliveryOrder/edit/'+id);
				};

				shrinkTableLayout = function(){
                    $("#deliveryOrder-table").hide();
                    $("#deliveryOrder-form").css("display","block");
                }

                expandTableLayout = function(){
                    try{
                        reloadComplaintTable();
                    }catch(e){}
                    $("#deliveryOrder-table").show();
                    $("#deliveryOrder-form").css("display","none");
                }

                showDetail = function(id){
                    $("#detailContent").empty();
                    $.ajax({
                        type:'POST',
                        url:'${request.contextPath}/deliveryOrder/showParts',
                        data : {id : id},
                        success:function(data,textStatus){
                                $("#detailContent").html(data);
                                $("#detailModal").modal({
                                    "backdrop" : "dynamic",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                }

});
        printDeliveryOrder = function(){
           checkDeliveryOrder =[];
            $("#deliveryOrder-table tbody .row-select").each(function() {
                if(this.checked){
                 var nRow = $(this).next("#doId").val()?$(this).next("#doId").val():"-"
                    if(nRow!="-"){
                        checkDeliveryOrder.push(nRow);
                    }
                }
            });
            if(checkDeliveryOrder.length<1 ){
                alert('Silahkan Pilih Salah Satu No. Delivery Order Untuk Dicetak');
                return;
            }
           var idDeliveryOrder =  JSON.stringify(checkDeliveryOrder);
           window.location = "${request.contextPath}/deliveryOrder/printDeliveryOrder?idDeliveryOrder="+idDeliveryOrder;
        }
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="deliveryOrder-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
            <fieldset class="buttons controls" style="padding-top: 10px;">
                <button id='printDeliveryOrderData' onclick="printDeliveryOrder();" type="button" class="btn btn-primary">Print Sales Order</button>
            </fieldset>
		</div>
		<div class="span11" id="deliveryOrder-form" style="display: none;"></div>
	</div>
    <div id="detailModal" class="modal fade">
        <div class="modal-dialog" style="width: 1090px;">
            <div class="modal-content" style="width: 1090px;">
                <div class="modal-body" style="max-height: 450px;">
                    <div id="detailContent"/>
                    <div class="iu-content"></div>

                </div>
            </div>
        </div>
    </div>
</body>
</html>
