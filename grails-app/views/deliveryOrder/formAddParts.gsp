<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 05/12/14
  Time: 16:31
--%>
<%@ page import="com.kombos.parts.Franc; com.kombos.parts.KlasifikasiGoods; com.kombos.parts.DeliveryOrder" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="deliveryOrder.formAddParts.label" default="Create Delivery Order" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
        var doSave;
        var staDetail="1";
        var idSO = "-1"
        $(function(){
            doSave = function(){
                var aoData = []
                var checkGoods =[];
                var checkDelGoods =[];

                $("#goods_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        checkGoods.push(id);
                    }else{
                        var idDel = $(this).next("input:hidden").val();
                        checkDelGoods.push(id);
                    }
                });

                if(checkGoods.length<1){
                    alert('Anda belum memilih data yang akan ditambahkan');
                }else if($('#limitDate').val()==""){
                    alert('Data Belum Lengkap');
                }else{
                    aoData.push(
                        {"name": 'doNumber', "value": $('#doNumber').val()},
                        {"name": 'sorNumber', "value": $('#sorNumber').val()},
                        {"name": 'limitDate', "value": $('#limitDate').val()},
                        {"name": 'warehouse', "value": $('#warehouse').val()},
                        {"name": 'intext', "value": $('#intext').val()},
                        {"name": 'paymentCondition', "value": $('#paymentCondition').val()}
                    )
                    if(confirm("Anda yakin data akan disimpan?")){
                        $.ajax({
                            url:'${request.contextPath}/deliveryOrder/save',
                            type: "POST", // Always use POST when deleting data
                            data : aoData,
                            success : function(data){
                                if(data=="OK"){
                                    toastr.success('<div>Data Berhasil disimpan.</div>');
                                    loadPath('deliveryOrder/list');
                                }else{
                                    alert('Internal Server Error');
                                }
                            },
                            error: function(xhr, textStatus, errorThrown) {
                                alert('Internal Server Error');
                            }
                        });
                    }
                 }
            }

            $('#sorNumber').typeahead({
                source: function (query, process) {
                    staDetail="1";
                    return $.get('${request.contextPath}/deliveryOrder/listSO', { query: query }, function (data) {
                        return process(data.options);
                    });
                }
            });
        })

        function detailSO(){
            if(staDetail=="1"){
                var noSo = $('#sorNumber').val();
                $.ajax({
                    url:'${request.contextPath}/deliveryOrder/detailSO?sorNumber='+noSo,
                    type: "POST", // Always use POST when deleting data
                    success : function(data){
                        $('#deliveryDate').val("");
                        $('#paymentType').val("");
                        $('#tradingFor').val("");
                        $('#customerName').val("");
                        $('#intext').val("");
                        if(data.hasil=="ada"){
                            idSO = data.idSO
                            reloadTabel(idSO);
                            $('#deliveryDate').val(data.deliveryDate);
                            $('#paymentType').val(data.paymentType);
                            $('#tradingFor').val(data.tradingFor);
                            $('#customerName').val(data.customerName);
                            $('#intext').val(data.intext);
                        }
                    }
                })
            }
            idSO = "-1"
            reloadTabel(idSO);
        }

        function reloadTabel(idSOReload){
            reloadGoodsTable(idSOReload);
        }

     </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="deliveryOrder.formAddParts.label" default="Create Delivery Order" /></span>
</div>
<div class="box">
    <div class="span12" id="formAddParts-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
            <div class="row-fluid">
                <div id="kiri" class="span5">
                    <table>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="deliveryOrder.doNumber.label" default="Nomor Delivery Order" />
                            </td>
                            <td style="padding: 10px">
                                <g:textField name="doNumber" id="doNumber" value="${nomorDO}" disabled="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="deliveryOrder.sorNumber.deliveryDate.label" default="Tanggal Penyerahan" />
                            </td>
                            <td style="padding: 10px">
                                <g:textField disabled="" name="deliveryDate" id="deliveryDate" value="${deliveryOrderInstance?.sorNumber?.deliveryDate}" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="deliveryOrder.tradingFor.label" default="Penjualan Untuk" />
                            </td>
                            <td style="padding: 10px">
                                <g:textField id="tradingFor" disabled="" name="tradingFor" value="${deliveryOrderInstance?.sorNumber?.tradingFor}"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="deliveryOrder.customerName.label" default="Nama Pelanggan" />
                            </td>
                            <td style="padding: 10px">
                                <g:textField disabled="" name="customerName" id="customerName" value="${deliveryOrderInstance?.sorNumber?.customerName}" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="deliveryOrder.deliveryLocation.label" default="Internal / External" />
                            </td>
                            <td style="padding: 10px">
                                <g:textField disabled="" name="intext" id="intext" value="${deliveryOrderInstance?.intext}" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="kanan" class="span7">
                    <table>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="deliveryOrder.sorNumber.label" default="Nomor Sales Order" />
                            </td>
                            <td style="padding: 10px">
                                <g:textField name="sorNumber" id="sorNumber" onblur="detailSO();" value="${deliveryOrderInstance?.sorNumber?.sorNumber}" class="typeahead" autocomplete="off"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="deliveryOrder.sorNumber.paymentType.label" default="Tipe Pembayaran" />
                            </td>
                            <td style="padding: 10px">
                                <g:textField disabled="" name="paymentType" id="paymentType" value="${deliveryOrderInstance?.sorNumber?.paymentType}" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="deliveryOrder.limitDate.label" default="D/O Berlaku s.d." />
                            </td>
                            <td style="padding: 10px">
                                <ba:datePicker name="limitDate" id="limitDate" precision="day" value="${deliveryOrderInstance?.limitDate}" format="dd-MM-yyyy"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="deliveryOrder.warehouse.label" default="Ex Gudang" />
                            </td>
                            <td style="padding: 10px">
                                <g:textField name="warehouse" id="warehouse" value="${deliveryOrderInstance?.warehouse}" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="deliveryOrder.paymentCondition.label" default="Syarat Pembayaran" />
                            </td>
                            <td style="padding: 10px">
                                <g:textField name="paymentCondition" id="paymentCondition" value="${deliveryOrderInstance?.paymentCondition}" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <br/>
            <br/>
            <br/>
            <g:render template="dataTablesAdd" />
            <fieldset class="buttons controls">
                <a class="btn cancel" onclick="loadPath('deliveryOrder/index');">
                    <g:message code="default.button.cancel.label" default="Cancel" />
                </a>
                <button class="btn btn-primary" name="save" onclick="doSave();" >Save</button>
            </fieldset>
        </div>
    <div class="span7" id="formAddParts-form" style="display: none;"></div>
</div>
<div id="addPartsModal" class="modal fade">
    <div class="modal-dialog" style="width: 1090px;">
        <div class="modal-content" style="width: 1090px;">
            <div class="modal-body" style="max-height: 800px;">
                <div id="addPartsContent"/>
                <div class="iu-content"></div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
