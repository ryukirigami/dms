<%@ page import="com.kombos.administrasi.BaseModel; com.kombos.administrasi.Operation; com.kombos.administrasi.Serial; com.kombos.administrasi.Section; com.kombos.administrasi.HargaJobSublet" %>


<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init',{
            mDec:'',
            aSep:''
        });
    });
</script>

<div class="control-group fieldcontain ${hasErrors(bean: hargaJobSubletInstance, field: 't116TMT', 'error')} required">
    <label class="control-label" for="t116TMT">
        <g:message code="hargaJobSublet.t116TMT.label" default="Tanggal Berlaku" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <ba:datePicker name="t116TMT" precision="day"  value="${hargaJobSubletInstance?.t116TMT}"  format="dd/mm/yyyy" required="true" />
    </div>
</div>
<g:javascript>
    document.getElementById("t116TMT").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: hargaJobSubletInstance, field: 'section', 'error')} required">
    <label class="control-label" for="section">
        <g:message code="hargaJobSublet.section.label" default="Nama Section" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="section" name="section.id" from="${Section.createCriteria().list(){eq("staDel", "0");order("m051NamaSection", "asc")}}" optionKey="id" required="" value="${hargaJobSubletInstance?.section?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: hargaJobSubletInstance, field: 'serial', 'error')} required">
    <label class="control-label" for="serial">
        <g:message code="hargaJobSublet.serial.label" default="Nama Serial" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="serial" name="serial.id" from="${Serial.createCriteria().list(){eq("staDel", "0");order("m052NamaSerial", "asc")}}" optionKey="id" required="" value="${hargaJobSubletInstance?.serial?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: hargaJobSubletInstance, field: 'operation', 'error')} required">
	<label class="control-label" for="operation">
		<g:message code="hargaJobSublet.operation.label" default="Nama Repair" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="operation" name="operation.id" from="${Operation.createCriteria().list(){eq("staDel", "0");order("m053NamaOperation", "asc")}}" optionKey="id" required="" value="${hargaJobSubletInstance?.operation?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: hargaJobSubletInstance, field: 'baseModel', 'error')} required">
	<label class="control-label" for="baseModel">
		<g:message code="hargaJobSublet.baseModel.label" default="Nama Base Model" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="baseModel" name="baseModel.id" from="${BaseModel.createCriteria().list(){eq("staDel", "0");order("m102NamaBaseModel", "asc")}}" optionKey="id" required="" optionValue="m102NamaBaseModel" value="${hargaJobSubletInstance?.baseModel?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: hargaJobSubletInstance, field: 't116Harga', 'error')} required">
	<label class="control-label" for="t116Harga">
		<g:message code="hargaJobSublet.t116Harga.label" default="Harga Beli(Rp)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t116Harga" value="${fieldValue(bean: hargaJobSubletInstance, field: 't116Harga')}" required="" maxlength="12" class="auto"/>
    </div>
</div>
