
<%@ page import="com.kombos.administrasi.HargaJobSublet" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</g:javascript>
<table id="hargaJobSublet_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="hargaJobSublet.t116TMT.label" default="Tgl Berlaku" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="hargaJobSublet.section.label" default="Section" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="hargaJobSublet.serial.label" default="Serial" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="hargaJobSublet.operation.label" default="Repair" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="hargaJobSublet.baseModel.label" default="Base Model" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="hargaJobSublet.t116Harga.label" default="Harga (Rp)" /></div>
			</th>


		
		</tr>
		<tr>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t116TMT" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                    <input type="hidden" name="search_t116TMT" value="date.struct">
                    <input type="hidden" name="search_t116TMT_day" id="search_t116TMT_day" value="">
                    <input type="hidden" name="search_t116TMT_month" id="search_t116TMT_month" value="">
                    <input type="hidden" name="search_t116TMT_year" id="search_t116TMT_year" value="">
                    <input type="text" data-date-format="dd/mm/yyyy" name="search_t116TMT_dp" value="" id="search_t116TMT" class="search_init">
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_section" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_section" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_serial" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_serial" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
				<div id="filter_operation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_operation" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_baseModel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_baseModel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t116Harga" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t116Harga" class="search_init" onkeypress="return isNumberKey(event)"/>
				</div>
			</th>

		</tr>
	</thead>
</table>

<g:javascript>
var hargaJobSubletTable;
var reloadHargaJobSubletTable;
$(function(){
	
	reloadHargaJobSubletTable = function() {
		hargaJobSubletTable.fnDraw();
	}
var recordsHargaJobSubletperpage = [];//new Array();
    var anHargaJobSubletSelected;
    var jmlRecHargaJobSubletPerPage=0;
    var id;
	
	$('#search_t116TMT').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t116TMT_day').val(newDate.getDate());
			$('#search_t116TMT_month').val(newDate.getMonth()+1);
			$('#search_t116TMT_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			hargaJobSubletTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	hargaJobSubletTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	hargaJobSubletTable = $('#hargaJobSublet_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsHargaJobSublet = $("#hargaJobSublet_datatables tbody .row-select");
            var jmlHargaJobSubletCek = 0;
            var nRow;
            var idRec;
            rsHargaJobSublet.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsHargaJobSubletperpage[idRec]=="1"){
                    jmlHargaJobSubletCek = jmlHargaJobSubletCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsHargaJobSubletperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecHargaJobSubletPerPage = rsHargaJobSublet.length;
            if(jmlHargaJobSubletCek==jmlRecHargaJobSubletPerPage && jmlRecHargaJobSubletPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
	"sName": "t116TMT",
	"mDataProp": "t116TMT",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "section",
	"mDataProp": "section",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "serial",
	"mDataProp": "serial",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


,
{
	"sName": "operation",
	"mDataProp": "operation",
	"aTargets": [3],

	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "baseModel",
	"mDataProp": "baseModel",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,



{
	"sName": "t116Harga",
	"mDataProp": "t116Harga",
	"aTargets": [5],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        if(data != null)
        return '<span class="pull-right numeric">'+data+'</span>';
        else
        return '<span></span>';
    },
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var operation = $('#filter_operation input').val();
						if(operation){
							aoData.push(
									{"name": 'sCriteria_operation', "value": operation}
							);
						}
	
						var baseModel = $('#filter_baseModel input').val();
						if(baseModel){
							aoData.push(
									{"name": 'sCriteria_baseModel', "value": baseModel}
							);
						}
	
						var section = $('#filter_section input').val();
						if(section){
							aoData.push(
									{"name": 'sCriteria_section', "value": section}
							);
						}
	
						var serial = $('#filter_serial input').val();
						if(serial){
							aoData.push(
									{"name": 'sCriteria_serial', "value": serial}
							);
						}

						var t116TMT = $('#search_t116TMT').val();
						var t116TMTDay = $('#search_t116TMT_day').val();
						var t116TMTMonth = $('#search_t116TMT_month').val();
						var t116TMTYear = $('#search_t116TMT_year').val();
						
						if(t116TMT){
							aoData.push(
									{"name": 'sCriteria_t116TMT', "value": "date.struct"},
									{"name": 'sCriteria_t116TMT_dp', "value": t116TMT},
									{"name": 'sCriteria_t116TMT_day', "value": t116TMTDay},
									{"name": 'sCriteria_t116TMT_month', "value": t116TMTMonth},
									{"name": 'sCriteria_t116TMT_year', "value": t116TMTYear}
							);
						}
	
						var t116Harga = $('#filter_t116Harga input').val();
						if(t116Harga){
							aoData.push(
									{"name": 'sCriteria_t116Harga', "value": t116Harga}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
                                $('td span.numeric').text(function(index, text) {
                                    return $.number(text,0);
                                    });
							}
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#hargaJobSublet_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsHargaJobSubletperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsHargaJobSubletperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#hargaJobSublet_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsHargaJobSubletperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anHargaJobSubletSelected = hargaJobSubletTable.$('tr.row_selected');
            if(jmlRecHargaJobSubletPerPage == anHargaJobSubletSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsHargaJobSubletperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
