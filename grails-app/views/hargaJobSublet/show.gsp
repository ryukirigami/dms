

<%@ page import="com.kombos.administrasi.HargaJobSublet" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'hargaJobSublet.label', default: 'Tarif Job Sublet')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteHargaJobSublet;

$(function(){ 
	deleteHargaJobSublet=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/hargaJobSublet/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadHargaJobSubletTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-hargaJobSublet" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="hargaJobSublet"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${hargaJobSubletInstance?.t116TMT}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t116TMT-label" class="property-label"><g:message
                                code="hargaJobSublet.t116TMT.label" default="Tanggal Berlaku" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t116TMT-label">
                        %{--<ba:editableValue
                                bean="${hargaJobSubletInstance}" field="t116TMT"
                                url="${request.contextPath}/HargaJobSublet/updatefield" type="text"
                                title="Enter t116TMT" onsuccess="reloadHargaJobSubletTable();" />--}%

                        <g:formatDate date="${hargaJobSubletInstance?.t116TMT}" />

                    </span></td>

                </tr>
            </g:if>
            <g:if test="${hargaJobSubletInstance?.section}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="section-label" class="property-label"><g:message
                                code="hargaJobSublet.section.label" default="Nama Section" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="section-label">
                        %{--<ba:editableValue
                                bean="${hargaJobSubletInstance}" field="section"
                                url="${request.contextPath}/HargaJobSublet/updatefield" type="text"
                                title="Enter section" onsuccess="reloadHargaJobSubletTable();" />--}%

                        %{--<g:link controller="section" action="show" id="${hargaJobSubletInstance?.section?.id}">${hargaJobSubletInstance?.section?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${hargaJobSubletInstance?.section}" field="m051NamaSection"/>
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${hargaJobSubletInstance?.serial}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="serial-label" class="property-label"><g:message
                                code="hargaJobSublet.serial.label" default="Nama Serial" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="serial-label">
                        %{--<ba:editableValue
                                bean="${hargaJobSubletInstance}" field="serial"
                                url="${request.contextPath}/HargaJobSublet/updatefield" type="text"
                                title="Enter serial" onsuccess="reloadHargaJobSubletTable();" />--}%

                        %{--<g:link controller="serial" action="show" id="${hargaJobSubletInstance?.serial?.id}">${hargaJobSubletInstance?.serial?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${hargaJobSubletInstance?.serial}" field="m052NamaSerial"/>
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${hargaJobSubletInstance?.operation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="operation-label" class="property-label"><g:message
					code="hargaJobSublet.operation.label" default="Nama Repair" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="operation-label">
						%{--<ba:editableValue
								bean="${hargaJobSubletInstance}" field="operation"
								url="${request.contextPath}/HargaJobSublet/updatefield" type="text"
								title="Enter operation" onsuccess="reloadHargaJobSubletTable();" />--}%
							
								%{--<g:link controller="operation" action="show" id="${hargaJobSubletInstance?.operation?.id}">${hargaJobSubletInstance?.operation?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${hargaJobSubletInstance?.operation}" field="m053NamaOperation"/>
						</span></td>
					
				</tr>
			</g:if>
			
			<g:if test="${hargaJobSubletInstance?.baseModel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="baseModel-label" class="property-label"><g:message
					code="hargaJobSublet.baseModel.label" default="Nama Base Model" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="baseModel-label">
						%{--<ba:editableValue
								bean="${hargaJobSubletInstance}" field="baseModel"
								url="${request.contextPath}/HargaJobSublet/updatefield" type="text"
								title="Enter baseModel" onsuccess="reloadHargaJobSubletTable();" />--}%
							
								%{--<g:link controller="baseModel" action="show" id="${hargaJobSubletInstance?.baseModel?.id}">${hargaJobSubletInstance?.baseModel?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${hargaJobSubletInstance?.baseModel}" field="m102NamaBaseModel"/>
						</span></td>
					
				</tr>
				</g:if>
			

			

				<g:if test="${hargaJobSubletInstance?.t116Harga}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t116Harga-label" class="property-label"><g:message
					code="hargaJobSublet.t116Harga.label" default="Harga (Rp)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t116Harga-label">
						%{--<ba:editableValue
								bean="${hargaJobSubletInstance}" field="t116Harga"
								url="${request.contextPath}/HargaJobSublet/updatefield" type="text"
								title="Enter t116Harga" onsuccess="reloadHargaJobSubletTable();" />--}%
							
								<g:fieldValue bean="${hargaJobSubletInstance}" field="t116Harga"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${hargaJobSubletInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="hargaJobSublet.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${hargaJobSubletInstance}" field="lastUpdProcess"
								url="${request.contextPath}/HargaJobSublet/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadHargaJobSubletTable();" />--}%
							
								<g:fieldValue bean="${hargaJobSubletInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${hargaJobSubletInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="hargaJobSublet.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${hargaJobSubletInstance}" field="dateCreated"
								url="${request.contextPath}/HargaJobSublet/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadHargaJobSubletTable();" />--}%
							
								<g:formatDate date="${hargaJobSubletInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>



            <g:if test="${hargaJobSubletInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="hargaJobSublet.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${hargaJobSubletInstance}" field="createdBy"
                                url="${request.contextPath}/HargaJobSublet/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadHargaJobSubletTable();" />--}%

                        <g:fieldValue bean="${hargaJobSubletInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${hargaJobSubletInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="hargaJobSublet.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${hargaJobSubletInstance}" field="lastUpdated"
								url="${request.contextPath}/HargaJobSublet/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadHargaJobSubletTable();" />--}%
							
								<g:formatDate date="${hargaJobSubletInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${hargaJobSubletInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="hargaJobSublet.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${hargaJobSubletInstance}" field="updatedBy"
                                url="${request.contextPath}/HargaJobSublet/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadHargaJobSubletTable();" />--}%

                        <g:fieldValue bean="${hargaJobSubletInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${hargaJobSubletInstance?.id}"
					update="[success:'hargaJobSublet-form',failure:'hargaJobSublet-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteHargaJobSublet('${hargaJobSubletInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
