<%@ page import="com.kombos.reception.Reception; com.kombos.maintable.AntriCuci" %>

<g:javascript>
		$(function(){
			$('#noPol').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/antriCuci/dataNopol', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
            cek = function(){
               var noPol = $('#noPol').val();
               $.ajax({
                    url:'${request.contextPath}/antriCuci/cek',
                    type: "POST", // Always use POST when deleting data
                    data : {noPol:noPol},
                    success : function(data){
                           $("#data").html(data)
                    },
                    error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                    }
                });
            }
		});
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: antriCuciInstance, field: 'reception', 'error')} ">
    <label class="control-label" for="nopol">
        Nomor Polisi *
    </label>

    <div class="controls">
        <g:if test="${antri}">
            <label id="nopol">
                ${antriCuciInstance?.reception?.historyCustomerVehicle?.fullNoPol}
            </label>
        </g:if>
        <g:else>
            <g:textField name="noPol" id="noPol" class="typeahead" onblur="cek()" value="${antriCuciInstance?.reception?.historyCustomerVehicle?.fullNoPol}" autocomplete="off" maxlength="220"  />
        </g:else>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: antriCuciInstance, field: 'reception', 'error')} ">
    <label class="control-label" for="receptionWo">
        <g:message code="antriCuci.reception.label" default="Reception" />

    </label>

    <div class="controls">
        <label id="receptionWo">
            <g:if test="${antriCuciInstance?.reception?.t401NoWO}">
                ${antriCuciInstance?.reception?.t401NoWO}
            </g:if><g:else>
                <span id="data"></span>
            </g:else>
        </label>
    </div>
</div>

<g:if test="${antri}">
    <div class="control-group fieldcontain ${hasErrors(bean: antriCuciInstance, field: 't600NomorAntrian', 'error')} ">
        <label class="control-label" for="t600NomorAntrian">
            <g:if test="${antri}">
                Nomor Antrian Lama
            </g:if>
            <g:else>
                <g:message code="antriCuci.t600NomorAntrian.label" default="T600 Nomor Antrian"/>

            </g:else>
        </label>

        <div class="controls">
            <g:if test="${antri}">
                <label id="t600NomorAntrian">
                    ${antriCuciInstance.t600NomorAntrian}
                </label>
            </g:if>
            <g:else>
                <g:field name="t600NomorAntrian" type="number" value="${antriCuciInstance.t600NomorAntrian}"/>
            </g:else>
        </div>
    </div>
</g:if>




<g:if test="${antri}">
    <div class="control-group fieldcontain ${hasErrors(bean: antriCuciInstance, field: 'alasanUbahUrutan', 'error')} ">
        <label class="control-label" for="alasanUbahUrutan">
            <g:message code="antriCuci.alasanUbahUrutan.label" default="Alasan Ubah Urutan"/>

        </label>

        <div class="controls">
            <g:textArea name="t600alasanUbahUrutan" value="${antriCuciInstance?.t600alasanUbahUrutan}" />
        </div>
    </div>
</g:if>
<g:else>
    <div class="control-group fieldcontain ${hasErrors(bean: antriCuciInstance, field: 't600xKet', 'error')} ">
        <label class="control-label" for="t600xKet">
            <g:message code="antriCuci.t600xKet.label" default="T600x Ket"/>
            <span class="required-indicator">*</span>
        </label>

        <div class="controls">
            <g:textArea name="t600xKet" rows="4" value="${antriCuciInstance?.t600xKet}" required="required"/>
        </div>
    </div>
</g:else>


<div class="control-group fieldcontain ${hasErrors(bean: antriCuciInstance, field: 't600Tanggal', 'error')} ">
    <label class="control-label" for="t600Tanggal">
        <g:message code="antriCuci.t600Tanggal.label" default="T600 Tanggal"/> *

    </label>

    <div class="controls">

        <g:if test="${antri}">
            <label id="t600Tanggal">
                %{--<g:formatDate format="dd-MM-yyyy" date="${antriCuciInstance?.t600Tanggal}"/>--}%
                ${antriCuciInstance?.t600Tanggal}
            </label>
        </g:if>
        <g:else>
            %{--<ba:datePicker format="dd-MM-yyyy" name="t600Tanggal" value="${antriCuciInstance?.t600Tanggal}"/>--}%
            ${new Date().format("dd MMM yyyy")}
        </g:else>

    </div>
</div>