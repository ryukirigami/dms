
<%@ page import="com.kombos.maintable.AntriCuci" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="antriCuci_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div>No</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Nomor WO</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Nomor Polisi</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Tanggal WO</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Start</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Stop</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Waktu Serah</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Waktu Production</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Final Inspection</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Wkt Dtg</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Keterangan</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>SA</div>
        </th>



    </tr>

    </thead>
</table>


<g:javascript>
var antriCuciTable;
var reloadAntriCuciTable;
$(function(){
	
	reloadAntriCuciTable = function() {
		antriCuciTable.fnDraw();
	}

    var recordsantriCuciPerPage = [];
    var anantriCuciSelected;
    var jmlRecantriCuciPerPage=0;
    var id;

	antriCuciTable = $('#antriCuci_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsantriCuci = $("#antriCuci_datatables tbody .row-select");
            var jmlantriCuciCek = 0;
            var nRow;
            var idRec;
            rsantriCuci.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsantriCuciPerPage[idRec]=="1"){
                    jmlantriCuciCek = jmlantriCuciCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsantriCuciPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecantriCuciPerPage = rsantriCuci.length;
            if(jmlantriCuciCek==jmlRecantriCuciPerPage){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "no",
	"mDataProp": "no",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"300px",
	"bVisible": true
}
,

{
	"sName": "noWo",
	"mDataProp": "noWo",
	"aTargets": [2],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "nopol",
	"mDataProp": "nopol",
	"aTargets": [3],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tanggalWo",
	"mDataProp": "tanggalWo",
	"aTargets": [4],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "start",
	"mDataProp": "start",
	"aTargets": [5],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "end",
	"mDataProp": "end",
	"aTargets": [6],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "waktuSerah",
	"mDataProp": "waktuSerah",
	"aTargets": [7],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "waktuProduction",
	"mDataProp": "waktuProduction",
	"aTargets": [8],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "finalInspection",
	"mDataProp": "finalInspection",
	"aTargets": [9],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "wktDtg",
	"mDataProp": "wktDtg",
	"aTargets": [10],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "keterangan",
	"mDataProp": "keterangan",
	"aTargets": [11],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "SA",
	"mDataProp": "SA",
	"aTargets": [12],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        if(document.getElementById('statusCuciCheck').checked == true){
                            var statusCuci = $('#statusCuci').val();
                            if(statusCuci){
                                aoData.push(
                                        {"name": 'statusCuci', "value": statusCuci}
                                );
                            }
						}

						if(document.getElementById('statusProductionCheck').checked == true){
                            var statusProduction = $('#statusProduction').val();
                            if(statusProduction){
                                aoData.push(
                                        {"name": 'statusProduction', "value": statusProduction}
                                );
                            }
						}


						if(document.getElementById('statusKategoriCheck').checked == true){
                            var statusKategori = $('#statusKategori').val();
                            if(statusKategori){
                                aoData.push(
                                        {"name": 'statusKategori', "value": statusKategori}
                                );
                            }
						}


						if(document.getElementById('statusKategoriCheck').checked == true){
                            var kataKunci = $('#kataKunci').val();
                            if(kataKunci){
                                aoData.push(
                                        {"name": 'kataKunci', "value": kataKunci}
                                );
                            }
						}
	

						if(document.getElementById('tanggalWOCheck').checked == true){
                            var tanggalWOMulai = $('#tanggalWOMulai').val();
                            var tanggalWOMulaiDay = $('#tanggalWOMulai_day').val();
                            var tanggalWOMulaiMonth = $('#tanggalWOMulai_month').val();
                            var tanggalWOMulaiYear = $('#tanggalWOMulai_year').val();

                            if(tanggalWOMulai){
                                aoData.push(
                                        {"name": 'tanggalWOMulai', "value": "date.struct"},
                                        {"name": 'tanggalWOMulai_dp', "value": tanggalWOMulai},
                                        {"name": 'tanggalWOMulai_day', "value": tanggalWOMulaiDay},
                                        {"name": 'tanggalWOMulai_month', "value": tanggalWOMulaiMonth},
                                        {"name": 'tanggalWOMulai_year', "value": tanggalWOMulaiYear}
                                );
                            }

                            var tanggalWOSelesai = $('#tanggalWOSelesai').val();
                            var tanggalWOSelesaiDay = $('#tanggalWOSelesai_day').val();
                            var tanggalWOSelesaiMonth = $('#tanggalWOSelesai_month').val();
                            var tanggalWOSelesaiYear = $('#tanggalWOSelesai_year').val();

                            if(tanggalWOSelesai){
                                aoData.push(
                                        {"name": 'tanggalWOSelesai', "value": "date.struct"},
                                        {"name": 'tanggalWOSelesai_dp', "value": tanggalWOSelesai},
                                        {"name": 'tanggalWOSelesai_day', "value": tanggalWOSelesaiDay},
                                        {"name": 'tanggalWOSelesai_month', "value": tanggalWOSelesaiMonth},
                                        {"name": 'tanggalWOSelesai_year', "value": tanggalWOSelesaiYear}
                                );
                            }
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#antriCuci_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsantriCuciPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsantriCuciPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#antriCuci_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsantriCuciPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anantriCuciSelected = antriCuciTable.$('tr.row_selected');

            if(jmlRecantriCuciPerPage == anantriCuciSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsantriCuciPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
