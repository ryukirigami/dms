<%@ page import="com.kombos.maintable.AntriCuci" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="bootstrapeditable"/>
    <g:set var="entityName"
           value="${message(code: 'antriCuci.label', default: 'Antrian Cuci')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <g:javascript disposition="head">
var deleteAntriCuci;

$(function(){ 
	deleteAntriCuci=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/antriCuci/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadAntriCuciTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

    </g:javascript>
</head>

<body>
<div id="show-antriCuci" role="main">
<legend>
    <g:message code="default.show.label" args="[entityName]"/>
</legend>
<g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
</g:if>
<table id="antriCuci"
       class="table table-bordered table-hover">
<tbody>

<g:if test="${antriCuciInstance?.companyDealer}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="companyDealer-label" class="property-label"><g:message
                    code="antriCuci.companyDealer.label" default="Company Dealer"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="companyDealer-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="companyDealer"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter companyDealer" onsuccess="reloadAntriCuciTable();" />--}%

            <g:fieldValue bean="${antriCuciInstance?.companyDealer}" field="m011NamaWorkshop"/>
        </span></td>

    </tr>
</g:if>

<g:if test="${antriCuciInstance?.t600Tanggal}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t600Tanggal-label" class="property-label"><g:message
                    code="antriCuci.t600Tanggal.label" default="T600 Tanggal"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t600Tanggal-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="t600Tanggal"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter t600Tanggal" onsuccess="reloadAntriCuciTable();" />--}%

            <g:formatDate date="${antriCuciInstance?.t600Tanggal}"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${antriCuciInstance?.t600NomorAntrian}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t600NomorAntrian-label" class="property-label"><g:message
                    code="antriCuci.t600NomorAntrian.label" default="T600 Nomor Antrian"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t600NomorAntrian-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="t600NomorAntrian"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter t600NomorAntrian" onsuccess="reloadAntriCuciTable();" />--}%

            <g:fieldValue bean="${antriCuciInstance}" field="t600NomorAntrian"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${antriCuciInstance?.reception}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="reception-label" class="property-label"><g:message
                    code="antriCuci.reception.label" default="Reception"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="reception-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="reception"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter reception" onsuccess="reloadAntriCuciTable();" />--}%

            <g:fieldValue bean="${antriCuciInstance?.reception}" field="t401NoWO"/>
        </span></td>

    </tr>
</g:if>

<g:if test="${antriCuciInstance?.t600alasanUbahUrutan}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="alasanUbahUrutan-label" class="property-label"><g:message
                    code="antriCuci.alasanUbahUrutan.label" default="Alasan Ubah Urutan"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="alasanUbahUrutan-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="alasanUbahUrutan"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter alasanUbahUrutan" onsuccess="reloadAntriCuciTable();" />--}%

            <g:fieldValue bean="${antriCuciInstance}" field="t600alasanUbahUrutan"/>
        </span></td>

    </tr>
</g:if>

<g:if test="${antriCuciInstance?.t600staOkCancelSelesai}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t600staOkCancelSelesai-label" class="property-label"><g:message
                    code="antriCuci.t600staOkCancelSelesai.label" default="T600sta Ok Cancel Selesai"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t600staOkCancelSelesai-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="t600staOkCancelSelesai"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter t600staOkCancelSelesai" onsuccess="reloadAntriCuciTable();" />--}%

            <g:fieldValue bean="${antriCuciInstance}" field="t600staOkCancelSelesai"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${antriCuciInstance?.t600StaCuciSebelumProd}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t600StaCuciSebelumProd-label" class="property-label"><g:message
                    code="antriCuci.t600StaCuciSebelumProd.label" default="T600 Sta Cuci Sebelum Prod"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t600StaCuciSebelumProd-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="t600StaCuciSebelumProd"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter t600StaCuciSebelumProd" onsuccess="reloadAntriCuciTable();" />--}%

            <g:fieldValue bean="${antriCuciInstance}" field="t600StaCuciSebelumProd"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${antriCuciInstance?.t600xKet}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t600xKet-label" class="property-label"><g:message
                    code="antriCuci.t600xKet.label" default="T600x Ket"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t600xKet-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="t600xKet"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter t600xKet" onsuccess="reloadAntriCuciTable();" />--}%

            <g:fieldValue bean="${antriCuciInstance}" field="t600xKet"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${antriCuciInstance?.t600xNamaUser}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t600xNamaUser-label" class="property-label"><g:message
                    code="antriCuci.t600xNamaUser.label" default="T600x Nama User"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t600xNamaUser-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="t600xNamaUser"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter t600xNamaUser" onsuccess="reloadAntriCuciTable();" />--}%

            <g:fieldValue bean="${antriCuciInstance}" field="t600xNamaUser"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${antriCuciInstance?.t600xDivisi}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t600xDivisi-label" class="property-label"><g:message
                    code="antriCuci.t600xDivisi.label" default="T600x Divisi"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t600xDivisi-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="t600xDivisi"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter t600xDivisi" onsuccess="reloadAntriCuciTable();" />--}%

            <g:fieldValue bean="${antriCuciInstance}" field="t600xDivisi"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${antriCuciInstance?.actualCuci}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="actualCuci-label" class="property-label"><g:message
                    code="antriCuci.actualCuci.label" default="Actual Cuci"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="actualCuci-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="actualCuci"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter actualCuci" onsuccess="reloadAntriCuciTable();" />--}%
            <g:fieldValue bean="${antriCuciInstance?.actualCuci}" field="t602Ket"/>

        </span></td>

    </tr>
</g:if>

%{--<g:if test="${antriCuciInstance?.t600StaDel}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t600StaDel-label" class="property-label"><g:message--}%
                    %{--code="antriCuci.t600StaDel.label" default="T600 Sta Del"/>:</span></td>--}%

        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t600StaDel-label">--}%
            %{--<ba:editableValue--}%
                    %{--bean="${antriCuciInstance}" field="t600StaDel"--}%
                    %{--url="${request.contextPath}/AntriCuci/updatefield" type="text"--}%
                    %{--title="Enter t600StaDel" onsuccess="reloadAntriCuciTable();" />--}%

            %{--<g:fieldValue bean="${antriCuciInstance}" field="t600StaDel"/>--}%

        %{--</span></td>--}%

    %{--</tr>--}%
%{--</g:if>--}%

<g:if test="${antriCuciInstance?.lastUpdProcess}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="lastUpdProcess-label" class="property-label"><g:message
                    code="antriCuci.lastUpdProcess.label" default="Last Upd Process"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="lastUpdProcess-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="lastUpdProcess"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter lastUpdProcess" onsuccess="reloadAntriCuciTable();" />--}%

            <g:fieldValue bean="${antriCuciInstance}" field="lastUpdProcess"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${antriCuciInstance?.dateCreated}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="dateCreated-label" class="property-label"><g:message
                    code="antriCuci.dateCreated.label" default="Date Created"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="dateCreated-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="dateCreated"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter dateCreated" onsuccess="reloadAntriCuciTable();" />--}%

            <g:formatDate date="${antriCuciInstance?.dateCreated}" type="datetime" style="MEDIUM"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${antriCuciInstance?.createdBy}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="createdBy-label" class="property-label"><g:message
                    code="antriCuci.createdBy.label" default="Created By"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="createdBy-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="createdBy"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter createdBy" onsuccess="reloadAntriCuciTable();" />--}%

            <g:fieldValue bean="${antriCuciInstance}" field="createdBy"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${antriCuciInstance?.lastUpdated}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="lastUpdated-label" class="property-label"><g:message
                    code="antriCuci.lastUpdated.label" default="Last Updated"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="lastUpdated-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="lastUpdated"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter lastUpdated" onsuccess="reloadAntriCuciTable();" />--}%

            <g:formatDate date="${antriCuciInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${antriCuciInstance?.updatedBy}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="updatedBy-label" class="property-label"><g:message
                    code="antriCuci.updatedBy.label" default="Updated By"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="updatedBy-label">
            %{--<ba:editableValue
                    bean="${antriCuciInstance}" field="updatedBy"
                    url="${request.contextPath}/AntriCuci/updatefield" type="text"
                    title="Enter updatedBy" onsuccess="reloadAntriCuciTable();" />--}%

            <g:fieldValue bean="${antriCuciInstance}" field="updatedBy"/>

        </span></td>

    </tr>
</g:if>

</tbody>
</table>
<g:form class="form-horizontal">
    <fieldset class="buttons controls">
        <a class="btn cancel" href="javascript:void(0);"
           onclick="expandTableLayout();"><g:message
                code="default.button.cancel.label" default="Cancel"/></a>
        <g:remoteLink class="btn btn-primary edit" action="edit"
                      id="${antriCuciInstance?.id}"
                      update="[success: 'antriCuci-form', failure: 'antriCuci-form']"
                      on404="alert('not found');">
            <g:message code="default.button.edit.label" default="Edit"/>
        </g:remoteLink>
        <ba:confirm id="delete" class="btn cancel"
                    message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
                    onsuccess="deleteAntriCuci('${antriCuciInstance?.id}')"
                    label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
    </fieldset>
</g:form>
</div>
</body>
</html>
