<%@ page import="com.kombos.maintable.AntriCuci" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'antriCuci.label', default: 'Antrian Cuci')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="baseapplayout"/>
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();"/>
        break;
    case '_DELETE_' :
        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/antriCuci/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/antriCuci/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#antriCuci-form').empty();
    	$('#antriCuci-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#antriCuci-table").hasClass("span12")){
   			$("#antriCuci-table").toggleClass("span12 span5");
        }
        $("#antriCuci-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#antriCuci-table").hasClass("span5")){
   			$("#antriCuci-table").toggleClass("span5 span12");
   		}
        $("#antriCuci-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#antriCuci-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/antriCuci/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadAntriCuciTable();
    		}
		});



   	}


   	    clearSearch = function(){
            var inputs, index;

            inputs = document.getElementsByTagName('input');
            for (index = 0; index < inputs.length; ++index) {
                inputs[index].value = '';
                inputs[index].checked = false;
            }
            reloadAntriCuciTable();
        }


		ubahPrioritas = function(){
		    var recordsToDelete = [];
            $("#antriCuci-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    recordsToDelete.push(id);
                }
            });
            if(recordsToDelete.length == 0){
                alert("Pilih salah satu kendaraan")
                return
            }

            var id = recordsToDelete[0]

            shrinkTableLayout();
            $('#spinner').fadeIn(1);
            $.ajax({type:'POST', url:'${request.contextPath}/antriCuci/edit/'+id+'?antri=true',
                success:function(data,textStatus){
                    loadForm(data, textStatus);
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
		}

});
    </g:javascript>
</head>

<body>

<div class="box">
    <legend>Search Antrian Cuci</legend>
    <table style="width: 60%">
        <tr>
            <td>Tanggal WO</td>
            <td><g:checkBox name="tanggalWOCheck"/></td>
            <td>
                <ba:datePicker format="dd/MM/yyyy" name="tanggalWOMulai"/>
                s.d
                <ba:datePicker format="dd/MM/yyyy" name="tanggalWOSelesai"/>
            </td>
        </tr>
        <tr>
            <td>Status Cuci </td>
            <td><g:checkBox name="statusCuciCheck"/></td>
            <td>
                <g:select optionKey="id" optionValue="nama" id="statusCuci" name="statusCuci" from="${
                [
                        [id:"0",nama:"SEMUA"],
                        [id:"1",nama:"BELUM CUCI"],
                        [id:"2",nama:"SEDANG CUCI"],
                        [id:"3",nama:"SELESAI CUCI"]
                ]
                }"/>
            </td>
        </tr>
        <tr>
            <td>Setelah/Sebelum Production  </td>
            <td><g:checkBox id="statusProductionCheck" name="statusProductionCheck"/></td>
            <td>
                <g:select optionKey="id" optionValue="nama" id="statusProduction" name="statusProduction" from="${
                    [
                            [id:"0",nama:"SETELAH DAN SEBELUM"],
                            [id:"1",nama:"SETELAH"],
                            [id:"2",nama:"SEBELUM"]
                    ]
                }"/>
            </td>
        </tr>
        <tr>
            <td>Kategori  </td>
            <td><g:checkBox id="statusKategoriCheck" name="statusKategoriCheck"/></td>
            <td>
                <g:select optionKey="id" optionValue="nama" id="statusKategori" name="statusKategori" from="${
                    [
                            [id:"0",nama:"NOMOR WO"],
                            [id:"1",nama:"NOMOR POLISI"]
                    ]
                }"/>
            </td>
        </tr>
        <tr>
            <td>Kata Kunci</td>
            <td></td>
            <td>
                <g:textField name="kataKunci" id="kataKunci"/>
            </td>
        </tr>
        <tr>
            <td colspan="3">

                <fieldset class="buttons controls">

                    %{--<button class="btn btn-primary" id="buttonEdit" onclick="onEdit()">Edit</button>--}%
                    <button class="btn btn-primary" id="buttonSave" onclick="reloadAntriCuciTable();">Search</button>
                    %{--<button class="btn btn-primary" id="buttonCancel" onclick="onCancel()">Cancel</button>--}%

                    <button class="btn btn-primary" id="buttonClose" onclick="clearSearch()">Clear Search</button>




                </fieldset>

            </td>
        </tr>
    </table>

</div>

<div class="box">
    <div class="span12" id="antriCuci-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <g:render template="dataTables"/>
        <br>
        <button class="btn btn-primary" id="buttonUbahPrioritas" onclick="ubahPrioritas();">Ubah Prioritas</button>
    </div>

    <div class="span7" id="antriCuci-form" style="display: none;"></div>
</div>
</body>
</html>
