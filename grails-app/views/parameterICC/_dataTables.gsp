
<%@ page import="com.kombos.parts.ParameterICC" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>

<table id="parameterICC_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="parameterICC.m155Tanggal.label" default="Tanggal Penetapan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="parameterICC.m155KodeICC.label" default="Kode ICC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="parameterICC.m155NamaICC.label" default="Nama ICC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="parameterICC.m155NilaiDAD1.label" default="Range DAD" /></div>
			</th>


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="parameterICC.m155NilaiDAD2.label" default="M155 Nilai DAD 2" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="parameterICC.m155ID.label" default="M155 ID" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="parameterICC.companyDealer.label" default="Company Dealer" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="parameterICC.staDel.label" default="Sta Del" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="parameterICC.createdBy.label" default="Created By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="parameterICC.updatedBy.label" default="Updated By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="parameterICC.lastUpdProcess.label" default="Last Upd Process" /></div>--}%
			%{--</th>--}%

		
		</tr>
		<tr>
		

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m155Tanggal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m155Tanggal" value="date.struct">
					<input type="hidden" name="search_m155Tanggal_day" id="search_m155Tanggal_day" value="">
					<input type="hidden" name="search_m155Tanggal_month" id="search_m155Tanggal_month" value="">
					<input type="hidden" name="search_m155Tanggal_year" id="search_m155Tanggal_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m155Tanggal_dp" value="" id="search_m155Tanggal" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m155KodeICC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m155KodeICC" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m155NamaICC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m155NamaICC" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m155NilaiDAD" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" id="search_m155NilaiDAD" onkeypress="return isNumberKey(event)" name="search_m155NilaiDAD" class="search_init" />
                    %{--- <input type="text" id="search_m155NilaiDAD2" name="search_m155NilaiDAD2" class="search_init" style="width: 30px;"/>--}%
					%{--<input type="text" name="search_m155NilaiDAD1" class="search_init" />--}%
				</div>
			</th>
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m155NilaiDAD2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m155NilaiDAD2" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m155ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m155ID" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_companyDealer" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_staDel" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_createdBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_updatedBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_lastUpdProcess" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%


		</tr>
	</thead>
</table>

<g:javascript>
var parameterICCTable;
var reloadParameterICCTable;
$(function(){
	
	reloadParameterICCTable = function() {
		parameterICCTable.fnDraw();
	}

	
	$('#search_m155Tanggal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m155Tanggal_day').val(newDate.getDate());
			$('#search_m155Tanggal_month').val(newDate.getMonth()+1);
			$('#search_m155Tanggal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			parameterICCTable.fnDraw();	
	});

	var recordsParameterICCPerPage = [];
    var anParameterICCSelected;
    var jmlRecParameterICCPerPage=0;
    var id;


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	parameterICCTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	parameterICCTable = $('#parameterICC_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsParameterICC = $("#parameterICC_datatables tbody .row-select");
            var jmlParameterICCCek = 0;
            var nRow;
            var idRec;
            rsParameterICC.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsParameterICCPerPage[idRec]=="1"){
                    jmlParameterICCCek = jmlParameterICCCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsParameterICCPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecParameterICCPerPage = rsParameterICC.length;
            if(jmlParameterICCCek==jmlRecParameterICCPerPage && jmlRecParameterICCPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m155Tanggal",
	"mDataProp": "m155Tanggal",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m155KodeICC",
	"mDataProp": "m155KodeICC",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m155NamaICC",
	"mDataProp": "m155NamaICC",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m155NilaiDAD1",
	"mDataProp": "m155NilaiDAD1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

//,
//
//{
//	"sName": "m155NilaiDAD2",
//	"mDataProp": "m155NilaiDAD2",
//	"aTargets": [4],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": false
//}
//
//,
//
//{
//	"sName": "m155ID",
//	"mDataProp": "m155ID",
//	"aTargets": [5],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "companyDealer",
//	"mDataProp": "companyDealer",
//	"aTargets": [6],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "staDel",
//	"mDataProp": "staDel",
//	"aTargets": [7],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "createdBy",
//	"mDataProp": "createdBy",
//	"aTargets": [8],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "updatedBy",
//	"mDataProp": "updatedBy",
//	"aTargets": [9],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "lastUpdProcess",
//	"mDataProp": "lastUpdProcess",
//	"aTargets": [10],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m155Tanggal = $('#search_m155Tanggal').val();
						var m155TanggalDay = $('#search_m155Tanggal_day').val();
						var m155TanggalMonth = $('#search_m155Tanggal_month').val();
						var m155TanggalYear = $('#search_m155Tanggal_year').val();
						
						if(m155Tanggal){
							aoData.push(
									{"name": 'sCriteria_m155Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_m155Tanggal_dp', "value": m155Tanggal},
									{"name": 'sCriteria_m155Tanggal_day', "value": m155TanggalDay},
									{"name": 'sCriteria_m155Tanggal_month', "value": m155TanggalMonth},
									{"name": 'sCriteria_m155Tanggal_year', "value": m155TanggalYear}
							);
						}
	
						var m155KodeICC = $('#filter_m155KodeICC input').val();
						if(m155KodeICC){
							aoData.push(
									{"name": 'sCriteria_m155KodeICC', "value": m155KodeICC}
							);
						}
	
						var m155NamaICC = $('#filter_m155NamaICC input').val();
						if(m155NamaICC){
							aoData.push(
									{"name": 'sCriteria_m155NamaICC', "value": m155NamaICC}
							);
						}
	
						var m155NilaiDAD = $('#filter_m155NilaiDAD input').val();
						if(m155NilaiDAD){//} && m155NilaiDAD2){
                            aoData.push(
                            {"name": 'sCriteria_m155NilaiDAD', "value": m155NilaiDAD}
                            );
                        }

//						if(m155NilaiDAD1){
//							aoData.push(
//									{"name": 'sCriteria_m155NilaiDAD1', "value": m155NilaiDAD1}
//							);
//						}
	
//						var m155NilaiDAD2 = $('#filter_m155NilaiDAD2 input').val();
//						if(m155NilaiDAD2){
//							aoData.push(
//									{"name": 'sCriteria_m155NilaiDAD2', "value": m155NilaiDAD2}
//							);
//						}
//
//						var m155ID = $('#filter_m155ID input').val();
//						if(m155ID){
//							aoData.push(
//									{"name": 'sCriteria_m155ID', "value": m155ID}
//							);
//						}
//
//						var companyDealer = $('#filter_companyDealer input').val();
//						if(companyDealer){
//							aoData.push(
//									{"name": 'sCriteria_companyDealer', "value": companyDealer}
//							);
//						}
//
//						var staDel = $('#filter_staDel input').val();
//						if(staDel){
//							aoData.push(
//									{"name": 'sCriteria_staDel', "value": staDel}
//							);
//						}
//
//						var createdBy = $('#filter_createdBy input').val();
//						if(createdBy){
//							aoData.push(
//									{"name": 'sCriteria_createdBy', "value": createdBy}
//							);
//						}
//
//						var updatedBy = $('#filter_updatedBy input').val();
//						if(updatedBy){
//							aoData.push(
//									{"name": 'sCriteria_updatedBy', "value": updatedBy}
//							);
//						}
//
//						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
//						if(lastUpdProcess){
//							aoData.push(
//									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
//							);
//						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#parameterICC_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsParameterICCPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsParameterICCPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#parameterICC_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsParameterICCPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anParameterICCSelected = parameterICCTable.$('tr.row_selected');
            if(jmlRecParameterICCPerPage == anParameterICCSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsParameterICCPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
