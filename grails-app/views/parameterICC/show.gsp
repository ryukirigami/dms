

<%@ page import="com.kombos.parts.ParameterICC" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'parameterICC.label', default: 'Kode ICC')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteParameterICC;

$(function(){ 
	deleteParameterICC=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/parameterICC/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadParameterICCTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-parameterICC" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="parameterICC"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${parameterICCInstance?.m155Tanggal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m155Tanggal-label" class="property-label"><g:message
					code="parameterICC.m155Tanggal.label" default="Tanggal Penetapan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m155Tanggal-label">
						%{--<ba:editableValue
								bean="${parameterICCInstance}" field="m155Tanggal"
								url="${request.contextPath}/ParameterICC/updatefield" type="text"
								title="Enter m155Tanggal" onsuccess="reloadParameterICCTable();" />--}%
							
								<g:formatDate date="${parameterICCInstance?.m155Tanggal}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${parameterICCInstance?.m155KodeICC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m155KodeICC-label" class="property-label"><g:message
					code="parameterICC.m155KodeICC.label" default="Kode ICC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m155KodeICC-label">
						%{--<ba:editableValue
								bean="${parameterICCInstance}" field="m155KodeICC"
								url="${request.contextPath}/ParameterICC/updatefield" type="text"
								title="Enter m155KodeICC" onsuccess="reloadParameterICCTable();" />--}%
							
								<g:fieldValue bean="${parameterICCInstance}" field="m155KodeICC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${parameterICCInstance?.m155NamaICC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m155NamaICC-label" class="property-label"><g:message
					code="parameterICC.m155NamaICC.label" default="Nama ICC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m155NamaICC-label">
						%{--<ba:editableValue
								bean="${parameterICCInstance}" field="m155NamaICC"
								url="${request.contextPath}/ParameterICC/updatefield" type="text"
								title="Enter m155NamaICC" onsuccess="reloadParameterICCTable();" />--}%
							
								<g:fieldValue bean="${parameterICCInstance}" field="m155NamaICC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${parameterICCInstance?.m155NilaiDAD1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m155NilaiDAD1-label" class="property-label"><g:message
					code="parameterICC.m155NilaiDAD1.label" default="Range DAD" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m155NilaiDAD1-label">
						%{--<ba:editableValue
								bean="${parameterICCInstance}" field="m155NilaiDAD1"
								url="${request.contextPath}/ParameterICC/updatefield" type="text"
								title="Enter m155NilaiDAD1" onsuccess="reloadParameterICCTable();" />--}%
							
								<g:fieldValue bean="${parameterICCInstance}" field="m155NilaiDAD1"/>&nbsp; -
                                <g:fieldValue bean="${parameterICCInstance}" field="m155NilaiDAD2"/>&nbsp;
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${parameterICCInstance?.m155NilaiDAD2}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m155NilaiDAD2-label" class="property-label"><g:message--}%
					%{--code="parameterICC.m155NilaiDAD2.label" default="M155nilai DAD 2" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m155NilaiDAD2-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${parameterICCInstance}" field="m155NilaiDAD2"--}%
								%{--url="${request.contextPath}/ParameterICC/updatefield" type="text"--}%
								%{--title="Enter m155NilaiDAD2" onsuccess="reloadParameterICCTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${parameterICCInstance}" field="m155NilaiDAD2"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${parameterICCInstance?.m155ID}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m155ID-label" class="property-label"><g:message--}%
					%{--code="parameterICC.m155ID.label" default="M155 ID" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m155ID-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${parameterICCInstance}" field="m155ID"--}%
								%{--url="${request.contextPath}/ParameterICC/updatefield" type="text"--}%
								%{--title="Enter m155ID" onsuccess="reloadParameterICCTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${parameterICCInstance}" field="m155ID"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${parameterICCInstance?.companyDealer}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="companyDealer-label" class="property-label"><g:message--}%
					%{--code="parameterICC.companyDealer.label" default="Company Dealer" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="companyDealer-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${parameterICCInstance}" field="companyDealer"--}%
								%{--url="${request.contextPath}/ParameterICC/updatefield" type="text"--}%
								%{--title="Enter companyDealer" onsuccess="reloadParameterICCTable();" />--}%
							%{----}%
								%{--<g:link controller="companyDealer" action="show" id="${parameterICCInstance?.companyDealer?.id}">${parameterICCInstance?.companyDealer?.encodeAsHTML()}</g:link>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${parameterICCInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="parameterICC.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${parameterICCInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/ParameterICC/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadParameterICCTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${parameterICCInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${parameterICCInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="parameterICC.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${parameterICCInstance}" field="lastUpdProcess"
								url="${request.contextPath}/ParameterICC/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadParameterICCTable();" />--}%
							
								<g:fieldValue bean="${parameterICCInstance}" field="lastUpdProcess" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${parameterICCInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="parameterICC.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${parameterICCInstance}" field="dateCreated"
								url="${request.contextPath}/ParameterICC/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadParameterICCTable();" />--}%
							
								<g:formatDate date="${parameterICCInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${parameterICCInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="parameterICC.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${parameterICCInstance}" field="createdBy"
                                url="${request.contextPath}/ParameterICC/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadParameterICCTable();" />--}%

                        <g:fieldValue bean="${parameterICCInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${parameterICCInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="parameterICC.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${parameterICCInstance}" field="lastUpdated"
								url="${request.contextPath}/ParameterICC/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadParameterICCTable();" />--}%
							
								<g:formatDate date="${parameterICCInstance?.lastUpdated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${parameterICCInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="parameterICC.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${parameterICCInstance}" field="updatedBy"
                                url="${request.contextPath}/ParameterICC/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadParameterICCTable();" />--}%

                        <g:fieldValue bean="${parameterICCInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${parameterICCInstance?.id}"
					update="[success:'parameterICC-form',failure:'parameterICC-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteParameterICC('${parameterICCInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
