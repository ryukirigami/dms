
<%@ page import="com.kombos.administrasi.JenisAlert" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="jenisAlert_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="jenisAlert.m901Id.label" default="Nomor Alert" /></div>
            </th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jenisAlert.m901NamaAlert.label" default="Nama Alert" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jenisAlert.m901NamaFormTindakLanjut.label" default="Nama Form Tindak Lanjut" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jenisAlert.m901StaPerluTindakLanjut.label" default="Perlu Tindak Lanjut" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="jenisAlert.lastUpdProcess.label" default="Last Upd Process" /></div>
            </th>

		
		</tr>
		<tr>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m901Id" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m901Id" class="search_init" />
                </div>
            </th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m901NamaAlert" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m901NamaAlert" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m901NamaFormTindakLanjut" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m901NamaFormTindakLanjut" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m901StaPerluTindakLanjut" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					%{--<input type="text" name="search_m901StaPerluTindakLanjut" class="search_init" />--}%
                    <select name="search_m901StaPerluTindakLanjut" id="search_m901StaPerluTindakLanjut" style="width:100%" onchange="reloadJenisAlertTable();">
                        <option value="">All</option>
                        <option value="0">Ya</option>
                        <option value="1">Tidak</option>
                    </select>
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_lastUpdProcess" class="search_init" />
                </div>
            </th>


		</tr>
	</thead>
</table>

<g:javascript>
var jenisAlertTable;
var reloadJenisAlertTable;
$(function(){
	
	reloadJenisAlertTable = function() {
		jenisAlertTable.fnDraw();
	}

	var recordsJenisAlertPerPage = [];
    var anJenisAlertSelected;
    var jmlRecJenisAlertPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	jenisAlertTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	jenisAlertTable = $('#jenisAlert_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsJenisAlert = $("#jenisAlert_datatables tbody .row-select");
            var jmlJenisAlertCek = 0;
            var nRow;
            var idRec;
            rsJenisAlert.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsJenisAlertPerPage[idRec]=="1"){
                    jmlJenisAlertCek = jmlJenisAlertCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsJenisAlertPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecJenisAlertPerPage = rsJenisAlert.length;
            if(jmlJenisAlertCek==jmlRecJenisAlertPerPage && jmlRecJenisAlertPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m901Id",
	"mDataProp": "m901Id",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m901NamaAlert",
	"mDataProp": "m901NamaAlert",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m901NamaFormTindakLanjut",
	"mDataProp": "m901NamaFormTindakLanjut",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m901StaPerluTindakLanjut",
	"mDataProp": "m901StaPerluTindakLanjut",
	"aTargets": [3],
	"mRender": function ( data, type, row ) {
            if(data=="0"){
                  return "Ya";
             }else
             {
                  return "Tidak";
             }

        },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": false
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m901Id = $('#filter_m901Id input').val();
						if(m901Id){
							aoData.push(
									{"name": 'sCriteria_m901Id', "value": m901Id}
							);
						}

						var m901NamaAlert = $('#filter_m901NamaAlert input').val();
						if(m901NamaAlert){
							aoData.push(
									{"name": 'sCriteria_m901NamaAlert', "value": m901NamaAlert}
							);
						}
	
						var m901NamaFormTindakLanjut = $('#filter_m901NamaFormTindakLanjut input').val();
						if(m901NamaFormTindakLanjut){
							aoData.push(
									{"name": 'sCriteria_m901NamaFormTindakLanjut', "value": m901NamaFormTindakLanjut}
							);
						}
	
						var m901StaPerluTindakLanjut = $('#search_m901StaPerluTindakLanjut').val();
						if(m901StaPerluTindakLanjut){
							aoData.push(
									{"name": 'sCriteria_m901StaPerluTindakLanjut', "value": m901StaPerluTindakLanjut}
							);
						}

						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#jenisAlert_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsJenisAlertPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsJenisAlertPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#jenisAlert_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsJenisAlertPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anJenisAlertSelected = jenisAlertTable.$('tr.row_selected');
            if(jmlRecJenisAlertPerPage == anJenisAlertSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsJenisAlertPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
