

<%@ page import="com.kombos.administrasi.JenisAlert" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'jenisAlert.label', default: 'Jenis Alert')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteJenisAlert;

$(function(){ 
	deleteJenisAlert=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/jenisAlert/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadJenisAlertTable();
   				expandTableLayout('jenisAlert');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-jenisAlert" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="jenisAlert"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${jenisAlertInstance?.m901Id}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m901Id-label" class="property-label"><g:message
                                code="jenisAlert.m901Id.label" default="Nomor Alert" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m901Id-label">
                        %{--<ba:editableValue
                                bean="${jenisAlertInstance}" field="m901Id"
                                url="${request.contextPath}/JenisAlert/updatefield" type="text"
                                title="Enter m901Id" onsuccess="reloadJenisAlertTable();" />--}%

                        <g:fieldValue bean="${jenisAlertInstance}" field="m901Id"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${jenisAlertInstance?.m901NamaAlert}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m901NamaAlert-label" class="property-label"><g:message
                                code="jenisAlert.m901NamaAlert.label" default="Nama Alert" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m901NamaAlert-label">
                        %{--<ba:editableValue
                                bean="${jenisAlertInstance}" field="m901NamaAlert"
                                url="${request.contextPath}/JenisAlert/updatefield" type="text"
                                title="Enter m901NamaAlert" onsuccess="reloadJenisAlertTable();" />--}%

                        <g:fieldValue bean="${jenisAlertInstance}" field="m901NamaAlert"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${jenisAlertInstance?.m901NamaFormTindakLanjut}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m901NamaFormTindakLanjut-label" class="property-label"><g:message
                                code="jenisAlert.m901NamaFormTindakLanjut.label" default="Nama Form Tindak Lanjut" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m901NamaFormTindakLanjut-label">
                        %{--<ba:editableValue
                                bean="${jenisAlertInstance}" field="m901NamaFormTindakLanjut"
                                url="${request.contextPath}/JenisAlert/updatefield" type="text"
                                title="Enter m901NamaFormTindakLanjut" onsuccess="reloadJenisAlertTable();" />--}%

                        <g:fieldValue bean="${jenisAlertInstance}" field="m901NamaFormTindakLanjut"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${jenisAlertInstance?.m901StaPerluTindakLanjut}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m901StaPerluTindakLanjut-label" class="property-label"><g:message
                                code="jenisAlert.m901StaPerluTindakLanjut.label" default="Perlu Tindak Lanjut" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m901StaPerluTindakLanjut-label">
                        %{--<ba:editableValue
                                bean="${jenisAlertInstance}" field="m901StaPerluTindakLanjut"
                                url="${request.contextPath}/JenisAlert/updatefield" type="text"
                                title="Enter m901StaPerluTindakLanjut" onsuccess="reloadJenisAlertTable();" />--}%

                        %{--<g:fieldValue bean="${jenisAlertInstance}" field="m901StaPerluTindakLanjut"/>--}%
                        <g:if test="${jenisAlertInstance?.m901StaPerluTindakLanjut == '0'}">
                            <g:message code="jenisAlert.m901StaPerluTindakLanjut.label" default="Ya" />
                        </g:if>
                        <g:else>
                            <g:message code="jenisAlert.m901StaPerluTindakLanjut.label" default="Tidak" />
                        </g:else>
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${jenisAlertInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="jenisAlert.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${jenisAlertInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/JenisAlert/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadJenisAlertTable();" />--}%

                        <g:fieldValue bean="${jenisAlertInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${jenisAlertInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="jenisAlert.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${jenisAlertInstance}" field="dateCreated"
								url="${request.contextPath}/JenisAlert/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJenisAlertTable();" />--}%
							
								<g:formatDate date="${jenisAlertInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>


            <g:if test="${jenisAlertInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="jenisAlert.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${jenisAlertInstance}" field="createdBy"
                                url="${request.contextPath}/JenisAlert/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadJenisAlertTable();" />--}%

                        <g:fieldValue bean="${jenisAlertInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${jenisAlertInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jenisAlert.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${jenisAlertInstance}" field="lastUpdated"
								url="${request.contextPath}/JenisAlert/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJenisAlertTable();" />--}%
							
								<g:formatDate date="${jenisAlertInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${jenisAlertInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="jenisAlert.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${jenisAlertInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/JenisAlert/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadJenisAlertTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${jenisAlertInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${jenisAlertInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="jenisAlert.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${jenisAlertInstance}" field="updatedBy"
								url="${request.contextPath}/JenisAlert/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJenisAlertTable();" />--}%
							
								<g:fieldValue bean="${jenisAlertInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('jenisAlert');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${jenisAlertInstance?.id}"
					update="[success:'jenisAlert-form',failure:'jenisAlert-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteJenisAlert('${jenisAlertInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
