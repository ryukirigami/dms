<%--
  Created by IntelliJ IDEA.
  User: Mohammad Fauzi
  Date: 3/23/14
  Time: 8:19 PM
--%>

<%@ page import="com.kombos.administrasi.ModelName; com.kombos.maintable.Company; com.kombos.maintable.Nikah; com.kombos.customerprofile.Agama; com.kombos.customerprofile.Hobby; com.kombos.maintable.Retention" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Send SMS Retention</title>
    <r:require modules="baseapplayout"/>
    <style>
    #spinner {
        z-index: 1000;
    }

    legend {
        display: block;
        width: 100%;
        padding: 0;
        /* margin-bottom: 20px; */
        font-size: 21px;
        line-height: 20px;
        color: #333333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    td {
        padding-bottom: 10px;
        padding-left: 5px;
        font-size: x-small;
    }
    </style>

    <%
        def data = new HashMap()
        session.searchCriteriaRetention = data;
    %>

    <g:set var="koloms" value="${[
            [id: "t182NamaDepan", nama: "Nama Depan"],
            [id: "t182NamaBelakang", nama: "Nama Belakang"],
            [id: "t182TglLahir", nama: "Tanggal Lahir"],
            [id: "t182JenisKelamin", nama: "Jenis Kelamin"],
            [id: "t183TglSTNK", nama: "Tanggal STNK"],
            [id: "t110fullModelCode", nama: "Full Model Code"],
            [id: "hobbies", nama: "Hobby"],
            [id: "t182NoHp", nama: "Nomor Handphone"],
            [id: "nikah", nama: "Status Pernikahan"],
            [id: "company", nama: "Nama Perusahaan"],
            [id: "fullNoPol", nama: "Nomor Polisi"],
            [id: "modelName", nama: "Model Kendaraan"],
            [id: "t401TanggalWO", nama: "Tanggal Service"],
            [id: "t183TglDEC", nama: "Tanggal DEC"],
            [id: "t103VinCode", nama: "VIN Code"],
            [id: "t401NoWO", nama: "Nomor WO"]
    ]}"/>

    <g:set var="kondisis" value="${[
            [id: "LIKE", nama: "Mengandung"],
            [id: "=", nama: "Sama Dengan"],
            [id: "<", nama: "Lebih Kecil"],
            [id: ">", nama: "Lebih Besar"],
            [id: "<=", nama: "Lebih Kecil atau Sama Dengan"],
            [id: ">=", nama: "Lebih Besar atau Sama Dengan"],
            [id: "!=", nama: "Tidak sama dengan"],
            [id: "BETWEEN", nama: "Di antara"]
    ]}"/>

    <g:javascript disposition="head">
        $(function () {



            completeProcess = function () {

            }

            jQuery("#buttonSave").click(function (event) {
                $("#typeSave").val("Send SMS");
            });


            jQuery("#buttonExport").click(function (event) {
                $("#typeSave").val("Export to excel");
            });


            jQuery("#addCriteria").click(function (event) {

                var andOr = document.getElementById("andOr").value;
                var kolom = document.getElementById("kolom").value;
                var textKolom = $("#kolom option:selected").text();
                if(kolom == ''){
                    alert("Pilih salah satu kolom");
                    event.preventDefault();
                    return;
                }
                var kondisi = document.getElementById("kondisi").value;
                var textkondisi = $("#kondisi option:selected").text();
                if(kondisi == ''){
                    alert("Pilih salah satu kondisi");
                    event.preventDefault();
                    return;
                }
                var val = '';
                var val1 = '';

                var valText = '';

                var kolomId = document.getElementById(kolom);
                var kolomId_start = document.getElementById(kolom+"_start");
                var kolomId_end = document.getElementById(kolom+"_end");
                if(kolomId){
                    val = kolomId.value;
                    valText = $("#"+kolom+" option:selected").text();
                }
                if(kolomId_start){
                    val = kolomId_start.value;
                }
                if(kolomId_end){
                    val1 = kolomId_end.value;
                }

                if(val == ''){
                    alert("Masukkan nilai");
                    event.preventDefault();
                    return;
                }

                var id = Math.floor((Math.random()*10000000)+1);
                var tableCriteria = document.getElementById("tableCriteria");
                var row = tableCriteria.insertRow(1);
                row.setAttribute("id", "id"+id);
                var cell1 = row.insertCell(0);
                cell1.setAttribute("style", "border-style: solid;border-width: 1px;");
                var cell2 = row.insertCell(1);
                cell2.setAttribute("style", "border-style: solid;border-width: 1px;");

                var input = "<a href='#' onclick='hapusKolomId("+id+");'>Hapus</a>";



                $('#spinner').fadeIn();
                 $.ajax({type: 'POST', url: '${request.contextPath}/sendSMSRetention/addParameterSearch?kondisi='+encodeURIComponent(kondisi)+'&andOr='+andOr+'&id='+id+'&kolom=' + kolom+'&nilai='+encodeURIComponent(val)+'&nilai1='+encodeURIComponent(val1),
                    success: function (data, textStatus) {
                        cell1.innerHTML = andOr+" "+textKolom+" "+textkondisi+" "+(valText == '' ? val : valText)+" "+val1;
                        cell2.innerHTML = input;
                        $('#spinner').fadeOut();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $('#spinner').fadeOut();
                    },
                    complete: function (XMLHttpRequest, textStatus) {
                        $('#spinner').fadeOut();
                    }
                });

                event.preventDefault(); //to stop the default loading
            });

            jQuery("#buttonSearch").click(function (event) {
                reloadHistoryCustomerTable();
                event.preventDefault(); //to stop the default loading
            });

            jQuery("#buttonClearSearch").click(function (event) {
               var tableCriteria = document.getElementById("tableCriteria");
               while(tableCriteria.rows.length > 1)
                {
                        tableCriteria.deleteRow (1);
                }
                $('#spinner').fadeIn();
                 $.ajax({type: 'POST', url: '${request.contextPath}/sendSMSRetention/clearParameterSearch',
                    success: function (data, textStatus) {
                        $('#spinner').fadeOut();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $('#spinner').fadeOut();
                    },
                    complete: function (XMLHttpRequest, textStatus) {
                        $('#spinner').fadeOut();
                    }
                });
                event.preventDefault(); //to stop the default loading
            });


            hapusKolomId = function(rowid){

                $('#spinner').fadeIn();
                 $.ajax({type: 'POST', url: '${request.contextPath}/sendSMSRetention/hapusParameterSearch?id='+rowid,
                    success: function (data, textStatus) {
                        var row = document.getElementById("id"+rowid);
                        row.parentNode.removeChild(row);
                        $('#spinner').fadeOut();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $('#spinner').fadeOut();
                    },
                    complete: function (XMLHttpRequest, textStatus) {
                        $('#spinner').fadeOut();
                    }
                });


            }

            doResponse = function (data) {
                jQuery('#spinner').fadeOut();
                if (data.status == 'SIP') {
                    alert(data.typeSave+' success');
                    if(data.typeSave != 'Send SMS'){
                        window.location = '${request.contextPath}/sendSMSRetention/downloadFile?file='+data.file;
                    }

                } else {
                    if(data.status.toUpperCase().indexOf('SIP')>=0){
                        alert('Sms success\nAda beberapa yang gagal karena data belum lengkap')
                    }else{
                        alert(data.typeSave+' fail\n' + data.error);
                    }
                }

            }


            ubahKolom = function(kolom){
                    <g:each in="${koloms}" var="kolom">
        $( "#${kolom.id}_div" ).hide();
    </g:each>
        $( "#"+kolom+"_div" ).show();
}

});
    </g:javascript>
</head>

<body>

<g:formRemote name="form" id="form" on404="alert('not found!')" update="updateMe"
              method="post"
              onComplete="completeProcess()"
              onSuccess="doResponse(data)"
              url="[controller: 'sendSMSRetention', action: 'doSave']">

    <g:hiddenField name="typeSave" value="Send SMS"/>

<div class="box">
    <legend style="font-size: small">Search Criteria:</legend>
    <table style="width: 100%;border: 0px;">
        <tr>
            <td style="width: 90%">
                <table style="width: 100%;border: 0px;">
                    <tr>

                        <td><g:select name="andOr" from="${["DAN", "ATAU"]}"/></td>
                        <td>
                            <g:select noSelection="${['': '-']}" onchange="ubahKolom(this.value)" name="kolom"
                                      from="${koloms}" optionKey="id"
                                      optionValue="nama"/>
                        </td>
                        <td><g:select noSelection="${['': '-']}" name="kondisi" from="${kondisis}" optionKey="id"
                                      optionValue="nama"/></td>
                        <td style="width: 80%;vertical-align: middle">
                            <div id="t182NamaDepan_div" style="display: none;">
                                <g:textField name="t182NamaDepan"/>
                            </div>

                            <div id="t182NamaBelakang_div" style="display: none;">
                                <g:textField name="t182NamaBelakang"/>
                            </div>

                            <div id="t182TglLahir_div" style="display: none;">
                                <ba:datePicker format="dd/MM/yyyy" name="t182TglLahir_start"/>
                                <ba:datePicker format="dd/MM/yyyy" name="t182TglLahir_end"/>
                            </div>

                            <div id="t182JenisKelamin_div" style="display: none;">
                                <g:select name="t182JenisKelamin"
                                          from="${[[id: "1", nama: "Laki-Laki"], [id: "2", nama: "Perempuan"]]}"
                                          optionValue="nama" optionKey="id"/>
                            </div>

                            <div id="t183TglSTNK_div" style="display: none;">
                                <ba:datePicker format="dd/MM/yyyy" name="t183TglSTNK_start"/>
                                <ba:datePicker format="dd/MM/yyyy" name="t183TglSTNK_end"/>
                            </div>

                            <div id="hobbies_div" style="display: none;">
                                <g:select name="hobbies"
                                          from="${Hobby.list()}"
                                          optionValue="m063NamaHobby" optionKey="id"/>
                            </div>

                            <div id="t182NoHp_div" style="display: none;">
                                <g:textField name="t182NoHp"/>
                            </div>

                            <div id="nikah_div" style="display: none;">
                                <g:select name="nikah"
                                          from="${Nikah.list()}"
                                          optionValue="m062StaNikah" optionKey="id"/>
                            </div>

                            <div id="company_div" style="display: none;">
                                <g:select name="company"
                                          from="${Company.list()}"
                                          optionValue="namaPerusahaan" optionKey="id"/>
                            </div>

                            <div id="fullNoPol_div" style="display: none;">
                                <g:textField name="fullNoPol"/>
                            </div>

                            <div id="modelName_div" style="display: none;">
                                <g:select name="modelName"
                                          from="${ModelName.list()}"
                                          optionValue="m104NamaModelName" optionKey="id"/>
                            </div>

                            <div id="t401TanggalWO_div" style="display: none;">
                                <ba:datePicker format="dd/MM/yyyy" name="t401TanggalWO_start"/>
                                <ba:datePicker format="dd/MM/yyyy" name="t401TanggalWO_end"/>
                            </div>

                            <div id="t183TglDEC_div" style="display: none;">
                                <ba:datePicker format="dd/MM/yyyy" name="t183TglDEC_start"/>
                                <ba:datePicker format="dd/MM/yyyy" name="t183TglDEC_end"/>
                            </div>

                            <div id="t103VinCode_div" style="display: none;">
                                <g:textField name="t103VinCode"/>
                            </div>

                            <div id="t401NoWO_div" style="display: none;">
                                <g:textField name="t401NoWO"/>
                            </div>

                        </td>
                        <td style="width: 50px">
                            <button id="addCriteria">+</button>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
        <tr >
            <td>
                <table id="tableCriteria" style="border-style: solid;width: 100%;border-width: 1px;">
                    <thead>
                    <tr>
                        <td>
                            Search Criteria
                        </td>
                        <td>
                        </td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>

                <fieldset class="buttons controls" style="text-align: right;">

                    <button class="btn btn-primary" id="buttonSearch">Search</button>

                    <button class="btn btn-primary" id="buttonClearSearch">Clear Search</button>

                </fieldset>

            </td>
        </tr>
    </table>

</div>

<div class="box">
    <legend style="font-size: small">Search Result:</legend>
    <g:render template="dataTables"/>
</div>

<div class="box">
    <legend style="font-size: small">SMS Retention:</legend>
    <table style="width: 100%;border: 0px;">
        <tr>
            <td>Jenis SMS Retention</td>
            <td>:</td>
            <td style="width: 70%;">
                <g:set var="retentions" value="${Retention.withCriteria {notEqual("m207StaOtomatis", "1")}}"/>
                <script type="application/javascript">
                    var retentions = []
                    <g:each in="${retentions}" var="retention">
                    retentions.push([${retention.id}, "${retention.m207FormatSms}"]);
                    </g:each>
                    function changeSMSFormat(retentionID) {
                        $('#formatSMS').val('');
                        for (var i = 0; i < retentions.length; i++) {
                            var id = retentions[i][0];
                            var format = retentions[i][1];
                            if (id == retentionID) {
                                $('#formatSMS').val(format);
                                break;
                            }
                        }
                    }
                </script>
                <g:select onchange="changeSMSFormat(this.value);"
                          noSelection="${['-1': '-- Jenis SMS Retention --']}"
                          name="retention" from="${retentions}" optionKey="id"
                          optionValue="m207NamaRetention"/>
            </td>
        </tr>
        <tr>
            <td>Format SMS Retention</td>
            <td>:</td>
            <td style="width: 70%;">
                <g:textArea id="formatSMS" name="formatSMS" style="width:100%; height: 70px"/>
            </td>
        </tr>

    </table>
</div>

<fieldset class="buttons controls" style="text-align: right;">

    %{--<button class="btn btn-primary" id="buttonEdit" onclick="onEdit()">Edit</button>--}%
    <button class="btn btn-primary" id="buttonSave">Send SMS Now</button>
    %{--<button class="btn btn-primary" id="buttonCancel" onclick="onCancel()">Cancel</button>--}%

    <button class="btn btn-primary" id="buttonExport">Export to excel</button>

    <button class="btn btn-primary" id="buttonClose" onclick="window.location.replace('#/home');">Close</button>

</fieldset>

</g:formRemote>
</body>
</html>