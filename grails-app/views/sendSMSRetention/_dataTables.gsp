
<%@ page import="com.kombos.customerprofile.HistoryCustomer" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="historyCustomer_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.t182NamaDepan.label" default="Nama Depan" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.t182NamaBelakang.label" default="Nama Belakang" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;width: 40%;">
            <div><g:message code="historyCustomer.t182TglLahir.label" default="Alamat" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustumerVehicle.t183TglSTNK.label" default="Tgl. STNK" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;width: 40%;">
            <div><g:message code="historyCustomerVehicle.fullModelCode.label" default="Full Model Code" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div>Hobby</div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div>Nomor Handphone</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Status</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.company.label" default="Company" /></div>
        </th>

    </tr>

    </thead>
</table>

<g:javascript>
var historyCustomerTable;
var reloadHistoryCustomerTable;
$(function(){

	reloadHistoryCustomerTable = function() {
		historyCustomerTable.fnDraw();
	}

	historyCustomerTable = $('#historyCustomer_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "t182NamaDepan",
	"mDataProp": "t182NamaDepan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input name="customer" value="'+row['id']+'" type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"/>&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "t182NamaBelakang",
	"mDataProp": "t182NamaBelakang",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "t182TglLahir",
	"mDataProp": "t182TglLahir",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "t183TglSTNK",
	"mDataProp": "t183TglSTNK",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "fullModelCode",
	"mDataProp": "fullModelCode",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "hobby",
	"mDataProp": "hobby",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "t182NoHp",
	"mDataProp": "t182NoHp",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "nikah",
	"mDataProp": "nikah",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "company",
	"mDataProp": "company",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ){

$.ajax({ "dataType": 'json',
    "type": "POST",
    "url": sSource,
    "data": aoData ,
    "success": function (json) {
        fnCallback(json);
       },
    "complete": function () {
       }
});
}
});
});
</g:javascript>