

<%@ page import="com.kombos.finance.MappingJournalDetail" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'mappingJournalDetail.label', default: 'MappingJournalDetail')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteMappingJournalDetail;

$(function(){ 
	deleteMappingJournalDetail=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/mappingJournalDetail/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadMappingJournalDetailTable();
   				expandTableLayout('mappingJournalDetail');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-mappingJournalDetail" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="mappingJournalDetail"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${mappingJournalDetailInstance?.accountNumber}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="accountNumber-label" class="property-label"><g:message
					code="mappingJournalDetail.accountNumber.label" default="Account Number" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="accountNumber-label">
						%{--<ba:editableValue
								bean="${mappingJournalDetailInstance}" field="accountNumber"
								url="${request.contextPath}/MappingJournalDetail/updatefield" type="text"
								title="Enter accountNumber" onsuccess="reloadMappingJournalDetailTable();" />--}%
							
								<g:link controller="accountNumber" action="show" id="${mappingJournalDetailInstance?.accountNumber?.id}">${mappingJournalDetailInstance?.accountNumber?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mappingJournalDetailInstance?.accountTransactionType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="accountTransactionType-label" class="property-label"><g:message
					code="mappingJournalDetail.accountTransactionType.label" default="Account Transaction Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="accountTransactionType-label">
						%{--<ba:editableValue
								bean="${mappingJournalDetailInstance}" field="accountTransactionType"
								url="${request.contextPath}/MappingJournalDetail/updatefield" type="text"
								title="Enter accountTransactionType" onsuccess="reloadMappingJournalDetailTable();" />--}%
							
								<g:fieldValue bean="${mappingJournalDetailInstance}" field="accountTransactionType"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mappingJournalDetailInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="mappingJournalDetail.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${mappingJournalDetailInstance}" field="createdBy"
								url="${request.contextPath}/MappingJournalDetail/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadMappingJournalDetailTable();" />--}%
							
								<g:fieldValue bean="${mappingJournalDetailInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mappingJournalDetailInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="mappingJournalDetail.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${mappingJournalDetailInstance}" field="dateCreated"
								url="${request.contextPath}/MappingJournalDetail/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadMappingJournalDetailTable();" />--}%
							
								<g:formatDate date="${mappingJournalDetailInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mappingJournalDetailInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="mappingJournalDetail.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${mappingJournalDetailInstance}" field="lastUpdProcess"
								url="${request.contextPath}/MappingJournalDetail/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadMappingJournalDetailTable();" />--}%
							
								<g:fieldValue bean="${mappingJournalDetailInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mappingJournalDetailInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="mappingJournalDetail.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${mappingJournalDetailInstance}" field="lastUpdated"
								url="${request.contextPath}/MappingJournalDetail/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadMappingJournalDetailTable();" />--}%
							
								<g:formatDate date="${mappingJournalDetailInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mappingJournalDetailInstance?.mappingJournal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="mappingJournal-label" class="property-label"><g:message
					code="mappingJournalDetail.mappingJournal.label" default="Mapping Journal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="mappingJournal-label">
						%{--<ba:editableValue
								bean="${mappingJournalDetailInstance}" field="mappingJournal"
								url="${request.contextPath}/MappingJournalDetail/updatefield" type="text"
								title="Enter mappingJournal" onsuccess="reloadMappingJournalDetailTable();" />--}%
							
								<g:link controller="mappingJournal" action="show" id="${mappingJournalDetailInstance?.mappingJournal?.id}">${mappingJournalDetailInstance?.mappingJournal?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mappingJournalDetailInstance?.nomorUrut}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="nomorUrut-label" class="property-label"><g:message
					code="mappingJournalDetail.nomorUrut.label" default="Nomor Urut" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="nomorUrut-label">
						%{--<ba:editableValue
								bean="${mappingJournalDetailInstance}" field="nomorUrut"
								url="${request.contextPath}/MappingJournalDetail/updatefield" type="text"
								title="Enter nomorUrut" onsuccess="reloadMappingJournalDetailTable();" />--}%
							
								<g:fieldValue bean="${mappingJournalDetailInstance}" field="nomorUrut"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mappingJournalDetailInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="mappingJournalDetail.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${mappingJournalDetailInstance}" field="staDel"
								url="${request.contextPath}/MappingJournalDetail/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadMappingJournalDetailTable();" />--}%
							
								<g:fieldValue bean="${mappingJournalDetailInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mappingJournalDetailInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="mappingJournalDetail.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${mappingJournalDetailInstance}" field="updatedBy"
								url="${request.contextPath}/MappingJournalDetail/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadMappingJournalDetailTable();" />--}%
							
								<g:fieldValue bean="${mappingJournalDetailInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('mappingJournalDetail');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${mappingJournalDetailInstance?.id}"
					update="[success:'mappingJournalDetail-form',failure:'mappingJournalDetail-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteMappingJournalDetail('${mappingJournalDetailInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
