<%@ page import="com.kombos.finance.MappingJournalDetail" %>


<div class="control-group fieldcontain ${hasErrors(bean: mappingJournalDetailInstance, field: 'accountTransactionType', 'error')} ">
    <label class="control-label" for="accountTransactionType">
        Id

    </label>
    <div class="controls">
        <g:textField name="accountTransactionType" value="${mappingJournalDetailInstance?.id}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: mappingJournalDetailInstance, field: 'accountNumber', 'error')} ">
	<label class="control-label" for="accountNumber">
		<g:message code="mappingJournalDetail.accountNumber.label" default="Account Number" />
		
	</label>
	<div class="controls">
	<g:select id="accountNumber" name="accountNumber.id" from="${com.kombos.finance.AccountNumber.list()}" optionKey="id" required="" value="${mappingJournalDetailInstance?.accountNumber?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: mappingJournalDetailInstance, field: 'accountTransactionType', 'error')} ">
	<label class="control-label" for="accountTransactionType">
		<g:message code="mappingJournalDetail.accountTransactionType.label" default="Account Transaction Type" />
		
	</label>
	<div class="controls">
	<g:textField name="accountTransactionType" value="${mappingJournalDetailInstance?.accountTransactionType}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: mappingJournalDetailInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="mappingJournalDetail.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${mappingJournalDetailInstance?.lastUpdProcess}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: mappingJournalDetailInstance, field: 'mappingJournal', 'error')} ">
	<label class="control-label" for="mappingJournal">
		<g:message code="mappingJournalDetail.mappingJournal.label" default="Mapping Journal" />
		
	</label>
	<div class="controls">
	<g:select id="mappingJournal" name="mappingJournal.id" from="${com.kombos.finance.MappingJournal.list()}" optionKey="id" required="" value="${mappingJournalDetailInstance?.mappingJournal?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: mappingJournalDetailInstance, field: 'nomorUrut', 'error')} ">
	<label class="control-label" for="nomorUrut">
		<g:message code="mappingJournalDetail.nomorUrut.label" default="Nomor Urut" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="nomorUrut" value="${mappingJournalDetailInstance.nomorUrut}" />
	</div>
</div>

