

<%@ page import="com.kombos.administrasi.ManPower" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'manPower.label', default: 'Jabatan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteManPower;

$(function(){ 
	deleteManPower=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/manPower/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadManPowerTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-manPower" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="manPower"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${manPowerInstance?.m014JabatanManPower}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m014JabatanManPower-label" class="property-label"><g:message
					code="manPower.m014JabatanManPower.label" default="Jabatan Man Power" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m014JabatanManPower-label">
						%{--<ba:editableValue
								bean="${manPowerInstance}" field="m014JabatanManPower"
								url="${request.contextPath}/ManPower/updatefield" type="text"
								title="Enter m014JabatanManPower" onsuccess="reloadManPowerTable();" />--}%
							
								<g:fieldValue bean="${manPowerInstance}" field="m014JabatanManPower"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerInstance?.m014Inisial}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m014Inisial-label" class="property-label"><g:message
					code="manPower.m014Inisial.label" default="Inisial Pekerjaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m014Inisial-label">
						%{--<ba:editableValue
								bean="${manPowerInstance}" field="m014Inisial"
								url="${request.contextPath}/ManPower/updatefield" type="text"
								title="Enter m014Inisial" onsuccess="reloadManPowerTable();" />--}%
							
								<g:fieldValue bean="${manPowerInstance}" field="m014Inisial"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerInstance?.m014staGRBPADM}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m014staGRBPADM-label" class="property-label"><g:message
					code="manPower.m014staGRBPADM.label" default="Tipe Pekerjaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m014staGRBPADM-label">
						%{--<ba:editableValue
								bean="${manPowerInstance}" field="m014staGRBPADM"
								url="${request.contextPath}/ManPower/updatefield" type="text"
								title="Enter m014staGRBPADM" onsuccess="reloadManPowerTable();" />--}%
								
								<g:if test="${manPowerInstance.m014staGRBPADM == '0'}">
		    						<g:message code="manPower.m014staGRBPADM.label" default="BP" />
              					</g:if>
              					<g:if test="${manPowerInstance.m014staGRBPADM == '1'}">
                                    <g:message code="manPower.m014staGRBPADM.label" default="GR" />
              					</g:if>
                                <g:if test="${manPowerInstance.m014staGRBPADM == '2'}">
                                    <g:message code="manPower.m014staGRBPADM.label" default="ADM" />
                                </g:if>
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${manPowerInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="manPower.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${manPowerInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/manPower/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadmanPowerTable();" />--}%

                        <g:fieldValue bean="${manPowerInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${manPowerInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="manPower.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${manPowerInstance}" field="dateCreated"
                                url="${request.contextPath}/manPower/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadmanPowerTable();" />--}%

                        <g:formatDate date="${manPowerInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${manPowerInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="manPower.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${manPowerInstance}" field="createdBy"
                                url="${request.contextPath}/manPower/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadmanPowerTable();" />--}%

                        <g:fieldValue bean="${manPowerInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${manPowerInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="manPower.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${manPowerInstance}" field="lastUpdated"
                                url="${request.contextPath}/manPower/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadmanPowerTable();" />--}%

                        <g:formatDate date="${manPowerInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${manPowerInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="manPower.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${manPowerInstance}" field="updatedBy"
                                url="${request.contextPath}/manPower/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadmanPowerTable();" />--}%

                        <g:fieldValue bean="${manPowerInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
<!--			
				<g:if test="${manPowerInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="manPower.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${manPowerInstance}" field="staDel"
								url="${request.contextPath}/ManPower/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadManPowerTable();" />--}%
							
								<g:fieldValue bean="${manPowerInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerInstance?.m014ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m014ID-label" class="property-label"><g:message
					code="manPower.m014ID.label" default="M014 ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m014ID-label">
						%{--<ba:editableValue
								bean="${manPowerInstance}" field="m014ID"
								url="${request.contextPath}/ManPower/updatefield" type="text"
								title="Enter m014ID" onsuccess="reloadManPowerTable();" />--}%
							
								<g:fieldValue bean="${manPowerInstance}" field="m014ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
-->			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${manPowerInstance?.id}"
					update="[success:'manPower-form',failure:'manPower-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteManPower('${manPowerInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
