<%@ page import="com.kombos.administrasi.ManPower" %>



<div class="control-group fieldcontain ${hasErrors(bean: manPowerInstance, field: 'm014JabatanManPower', 'error')} required">
	<label class="control-label" for="m014JabatanManPower">
		<g:message code="manPower.m014JabatanManPower.label" default="Jabatan Man Power" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m014JabatanManPower" maxlength="20" required="" value="${manPowerInstance?.m014JabatanManPower}"/>
	</div>
</div>

<g:javascript>
    document.getElementById("m014JabatanManPower").focus();
    //    document.getElementById("m014staGRBPADM").checked();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerInstance, field: 'm014Inisial', 'error')} required">
	<label class="control-label" for="m014Inisial">
		<g:message code="manPower.m014Inisial.label" default="Inisial Pekerjaan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m014Inisial" maxlength="4" required="" value="${manPowerInstance?.m014Inisial}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerInstance, field: 'm014staGRBPADM', 'error')} required">
	<label class="control-label" for="m014staGRBPADM">
		<g:message code="manPower.m014staGRBPADM.label" default="Tipe Pekerjaan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:radioGroup required="" name="m014staGRBPADM" values="['0','1','2']" labels="['BP','GR','ADM']" value="${manPowerInstance?.m014staGRBPADM}"  >
        ${it.radio} ${it.label}
    </g:radioGroup>
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: manPowerInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="manPower.staDel.label" default="Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" maxlength="1" required="" value="${manPowerInstance?.staDel}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerInstance, field: 'm014ID', 'error')} ">
	<label class="control-label" for="m014ID">
		<g:message code="manPower.m014ID.label" default="M014 ID" />
		
	</label>
	<div class="controls">
	<g:textField name="m014ID" maxlength="2" value="${manPowerInstance?.m014ID}"/>
	</div>
</div>
--}%
