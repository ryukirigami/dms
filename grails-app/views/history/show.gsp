

<%@ page import="com.kombos.hrd.History" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'history.label', default: 'History')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteHistory;

$(function(){ 
	deleteHistory=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/history/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadHistoryTable();
   				expandTableLayout('history');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-history" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="history"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${historyInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="history.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${historyInstance}" field="createdBy"
								url="${request.contextPath}/History/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadHistoryTable();" />--}%
							
								<g:fieldValue bean="${historyInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="history.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${historyInstance}" field="dateCreated"
								url="${request.contextPath}/History/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadHistoryTable();" />--}%
							
								<g:formatDate date="${historyInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyInstance?.history}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="history-label" class="property-label"><g:message
					code="history.history.label" default="History" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="history-label">
						%{--<ba:editableValue
								bean="${historyInstance}" field="history"
								url="${request.contextPath}/History/updatefield" type="text"
								title="Enter history" onsuccess="reloadHistoryTable();" />--}%
							
								<g:fieldValue bean="${historyInstance}" field="history"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyInstance?.keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="keterangan-label" class="property-label"><g:message
					code="history.keterangan.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="keterangan-label">
						%{--<ba:editableValue
								bean="${historyInstance}" field="keterangan"
								url="${request.contextPath}/History/updatefield" type="text"
								title="Enter keterangan" onsuccess="reloadHistoryTable();" />--}%
							
								<g:fieldValue bean="${historyInstance}" field="keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="history.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${historyInstance}" field="lastUpdProcess"
								url="${request.contextPath}/History/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadHistoryTable();" />--}%
							
								<g:fieldValue bean="${historyInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="history.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${historyInstance}" field="lastUpdated"
								url="${request.contextPath}/History/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadHistoryTable();" />--}%
							
								<g:formatDate date="${historyInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="history.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${historyInstance}" field="staDel"
								url="${request.contextPath}/History/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadHistoryTable();" />--}%
							
								<g:fieldValue bean="${historyInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="history.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${historyInstance}" field="updatedBy"
								url="${request.contextPath}/History/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadHistoryTable();" />--}%
							
								<g:fieldValue bean="${historyInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('history');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${historyInstance?.id}"
					update="[success:'history-form',failure:'history-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteHistory('${historyInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
