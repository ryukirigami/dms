<%@ page import="com.kombos.hrd.History" %>

<g:javascript>
    $(function() {
        $("#history").focus();
    })
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: historyInstance, field: 'history', 'error')} ">
	<label class="control-label" for="history">
		<g:message code="history.history.label" default="History" />
		
	</label>
	<div class="controls">
	<g:textField name="history" value="${historyInstance?.history}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: historyInstance, field: 'keterangan', 'error')} ">
	<label class="control-label" for="keterangan">
		<g:message code="history.keterangan.label" default="Keterangan" />
		
	</label>
	<div class="controls">
	<g:textField name="keterangan" maxlength="50" value="${historyInstance?.keterangan}" />
	</div>
</div>


