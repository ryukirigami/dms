<%@ page import="com.kombos.administrasi.Materai" %>
<g:javascript>
    $(function(){
        $('.rupiah').autoNumeric('init',{
            vMin:'0',
            vMax:'99999999',
            mDec: null,
            aSep:''
        });
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: materaiInstance, field: 'm705TglBerlaku', 'error')} required">
	<label class="control-label" for="m705TglBerlaku">
		<g:message code="materai.m705TglBerlaku.label" default="Tanggal Berlaku" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<ba:datePicker name="m705TglBerlaku" precision="day" value="${materaiInstance?.m705TglBerlaku}" format="dd-MM-yyyy" required="true"/>
	</div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: materaiInstance, field: 'm705Nilai1', 'error')} required">
	<label class="control-label" for="m705Nilai1">
		<g:message code="materai.m705Nilai1.label" default="Jumlah Tagihan Awal" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
        <g:textField class="rupiah" name="m705Nilai1" value="${materaiInstance?.m705Nilai1}" required=""/>
	%{--<g:field type="number" name="m705Nilai1" value="${materaiInstance.m705Nilai1}" required=""/>--}%
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: materaiInstance, field: 'm705Nilai2', 'error')} required">
	<label class="control-label" for="m705Nilai2">
		<g:message code="materai.m705Nilai2.label" default="Jumlah Tagihan Akhir" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
        <g:textField class="rupiah greaterThan" name="m705Nilai2" value="${materaiInstance?.m705Nilai2}" required="" data-min="m705Nilai1"/>
	%{--<g:field type="number" name="m705Nilai2" value="${materaiInstance.m705Nilai2}" />--}%
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: materaiInstance, field: 'm705NilaiMaterai', 'error')} required">
	<label class="control-label" for="m705NilaiMaterai">
		<g:message code="materai.m705NilaiMaterai.label" default="Nilai Materai" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
        <g:textField class="rupiah" name="m705NilaiMaterai" value="${materaiInstance?.m705NilaiMaterai}" required=""/>
	%{--<g:field type="number" name="m705NilaiMaterai" value="${materaiInstance.m705NilaiMaterai}" />--}%
	</div>
</div>

