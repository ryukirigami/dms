<%@ page import="com.kombos.administrasi.Materai" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'materai.label', default: 'Materai')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-materai" class="content scaffold-edit" role="main">
			<legend><g:message code="default.edit.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${materaiInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${materaiInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
            <g:javascript>
                var submitForm;
                $(function(){
                   submitForm = function(){
                        if($('#editMaterai').valid()){
                            $.ajax({
                                type:'POST',
                                data:jQuery('#editMaterai').serialize(),
                                url:'${request.getContextPath()}/materai/update',
                                success:function(data,textStatus){
                                    jQuery('#materai-form').html(data)
                                    reloadMateraiTable();
                                },
                                error:function(XMLHttpRequest,textStatus,errorThrown){}});
                        }
                    }

                    $.validator.addMethod("greaterThan", function (value, element, param) {
                        var $element = $(element)
                            , $min;

                        if (typeof(param) === "string") {
                            $min = $(param);
                        } else {
                            $min = $("#" + $element.data("min"));
                        }

                        if (this.settings.onfocusout) {
                            $min.off(".validate-greaterThan").on("blur.validate-greaterThan", function () {
                                $element.valid();
                            });
                        }

                        return parseFloat(value) > parseFloat($min.val());
                    }, "Jumlah tagihan akhir < jumlah tagihan awal.");

                    $.validator.addClassRules({
                        greaterThan: {
                            greaterThan: true
                        }
                    });

                });
            </g:javascript>

            <form id="editMaterai" class="form-horizontal" action="${request.getContextPath()}/materai/update" method="post" onsubmit="submitForm();return false;">

            %{--<g:form method="post" >--}%
			%{--<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="reloadMateraiTable();" update="materai-form"--}%
				%{--url="[controller: 'materai', action:'update']">--}%
				<g:hiddenField name="id" value="${materaiInstance?.id}" />
				<g:hiddenField name="version" value="${materaiInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
                <fieldset class="buttons controls">
                    <g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                </fieldset>
			%{--</g:formRemote>--}%
			%{--</g:form>--}%
             </form>
		</div>
	</body>
</html>
