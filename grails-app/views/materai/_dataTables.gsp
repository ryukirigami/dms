
<%@ page import="com.kombos.administrasi.Materai" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="materai_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="materai.m705TglBerlaku.label" default="Tanggal Berlaku" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="materai.m705Nilai1.label" default="Jumlah Tagihan Awal" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="materai.m705Nilai2.label" default="Jumlah Tagihan Akhir" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="materai.m705NilaiMaterai.label" default="Nilai Materai" /></div>
			</th>
		</tr>

	</thead>
</table>

<g:javascript>
var materaiTable;
var reloadMateraiTable;
$(function(){
	
	reloadMateraiTable = function() {
		materaiTable.fnDraw();
	}

	
	$('#search_m705TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m705TglBerlaku_day').val(newDate.getDate());
			$('#search_m705TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m705TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			materaiTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	materaiTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	materaiTable = $('#materai_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//		        var aData = materaiTable.fnGetData(nRow);
//		        var checkbox = $('.row-select', nRow);
//		        if(checked.indexOf(""+aData['id'])>=0){
//		              checkbox.attr("checked",true);
//		              checkbox.parent().parent().addClass('row_selected');
//		        }
//
//		        checkbox.click(function (e) {
//            	    var tc = $(this);
//
//                    if(this.checked)
//            			tc.parent().parent().addClass('row_selected');
//            		else {
//            			tc.parent().parent().removeClass('row_selected');
//            			var selectAll = $('.select-all');
//            			selectAll.removeAttr('checked');
//                    }
//            	 	e.stopPropagation();
//                });
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m705TglBerlaku",
	"mDataProp": "m705TglBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "m705Nilai1",
	"mDataProp": "m705Nilai1",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m705Nilai2",
	"mDataProp": "m705Nilai2",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m705NilaiMaterai",
	"mDataProp": "m705NilaiMaterai",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                var selectAll = $('.select-all');
                selectAll.removeAttr('checked');
                 $("#materai_datatables tbody .row-select").each(function() {
                            var id = $(this).next("input:hidden").val();
                            if(checked.indexOf(""+id)>=0){
                                checked[checked.indexOf(""+id)]="";
                            }
                            if(this.checked){
                                checked.push(""+id);
                            }
                        });
				$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
