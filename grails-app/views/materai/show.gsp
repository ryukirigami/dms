

<%@ page import="com.kombos.administrasi.Materai" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'materai.label', default: 'Materai')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteMaterai;

$(function(){ 
	deleteMaterai=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/materai/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadMateraiTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-materai" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="materai"
			class="table table-bordered table-hover">
			<tbody>

				<g:if test="${materaiInstance?.m705TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m705TglBerlaku-label" class="property-label"><g:message
					code="materai.m705TglBerlaku.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m705TglBerlaku-label">
						<g:formatDate date="${materaiInstance?.m705TglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
				
				
				<g:if test="${materaiInstance?.m705Nilai1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m705Nilai1-label" class="property-label"><g:message
					code="materai.m705Nilai1.label" default="Jumlah Tagihan Awal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m705Nilai1-label">
						%{--<ba:editableValue
								bean="${materaiInstance}" field="m705Nilai1"
								url="${request.contextPath}/Materai/updatefield" type="text"
								title="Enter m705Nilai1" onsuccess="reloadMateraiTable();" />--}%
							
								<g:fieldValue bean="${materaiInstance}" field="m705Nilai1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${materaiInstance?.m705Nilai2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m705Nilai2-label" class="property-label"><g:message
					code="materai.m705Nilai2.label" default="Jumlah Tagihan Akhir" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m705Nilai2-label">
						%{--<ba:editableValue
								bean="${materaiInstance}" field="m705Nilai2"
								url="${request.contextPath}/Materai/updatefield" type="text"
								title="Enter m705Nilai2" onsuccess="reloadMateraiTable();" />--}%
							
								<g:fieldValue bean="${materaiInstance}" field="m705Nilai2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${materaiInstance?.m705NilaiMaterai}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m705NilaiMaterai-label" class="property-label"><g:message
					code="materai.m705NilaiMaterai.label" default="Nilai Materai" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m705NilaiMaterai-label">
						%{--<ba:editableValue
								bean="${materaiInstance}" field="m705NilaiMaterai"
								url="${request.contextPath}/Materai/updatefield" type="text"
								title="Enter m705NilaiMaterai" onsuccess="reloadMateraiTable();" />--}%
							
								<g:fieldValue bean="${materaiInstance}" field="m705NilaiMaterai"/>
							
						</span></td>
					
				</tr>
				</g:if>
            <g:if test="${materaiInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="materaiInstance.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="createdBy"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${materaiInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${materaiInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="materaiInstance.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="updatedBy"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${materaiInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${materaiInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="materaiInstance.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${materaiInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${materaiInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="materaiInstance.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="dateCreated"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadGoodsTable();" />--}%

                        <g:formatDate date="${materaiInstance?.dateCreated}" format="EEEE, dd MMMM yyyy HH:mm" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${materaiInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="materaiInstance.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="lastUpdated"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadGoodsTable();" />--}%

                        <g:formatDate date="${materaiInstance?.lastUpdated}" format="EEEE, dd MMMM yyyy HH:mm"/>

                    </span></td>

                </tr>
            </g:if>
			
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${materaiInstance?.id}"
					update="[success:'materai-form',failure:'materai-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteMaterai('${materaiInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
