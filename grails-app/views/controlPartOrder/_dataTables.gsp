
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="controlPartOrder_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
    <thead>
    <tr>
        <th></th>
        <th style="border-bottom: none;padding: 5px;">
            <g:message code="controlPartOrder.noApp.label" default="Nomor Appointment"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="controlPartOrder.t132Tanggal.label" default="Tanggal Jam Digunakan"/>
        </th>

        <th style="border-bottom: none; padding: 5px;"/>

    </tr>
    </thead>
</table>

<g:javascript>
var controlPartOrderTable;
var reloadControlPartOrderTable;
$(function(){
$('#clear').click(function(e){
        $('#search_t017Tanggal').val("");
        $('#search_t017Tanggal_day').val("");
        $('#search_t017Tanggal_month').val("");
        $('#search_t017Tanggal_year').val("");
        $('#search_t017Tanggal2').val("");
        $('#search_t017Tanggal2_day').val("");
        $('#search_t017Tanggal2_month').val("");
        $('#search_t017Tanggal2_year').val("");
        e.stopPropagation();
        controlPartOrderTable.fnDraw();
	});
    $('#view').click(function(e){
        e.stopPropagation();
		controlPartOrderTable.fnDraw();
	});
    var anOpen = [];
	$('#controlPartOrder_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = controlPartOrderTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/controlPartOrder/dataTablesSub',
	   			success:function(data,textStatus){
	   				var nDetailsRow = controlPartOrderTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			controlPartOrderTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});


	reloadControlPartOrderTable = function() {
		controlPartOrderTable.fnDraw();
	}


    controlPartOrderTable = $('#controlPartOrder_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"bDestroy": true,
		"aoColumns": [
{
   "mDataProp": null,
   "sClass": "control center",
   "bSortable": false,
   "sWidth":"10px",
   "sDefaultContent": '<i class="icon-plus"></i>'
}
,
{
	"sName": "noApp",
	"mDataProp": "noApp",
	"aTargets": [0],
	%{--"mRender": function ( data, type, row ) {--}%
	    %{--return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"><input type="hidden" name="nomorStok" id="nomorStok" value="'+data+'">&nbsp;&nbsp;'+data+'</a><a class="pull-right cell-action" name="pencilEdit" id="pencilEdit" href="javascript:void(0);" onClick="edit(\''+data+'\')">&nbsp;&nbsp;<i class="icon-pencil" ></i>&nbsp;&nbsp;</a>';--}%
	%{--},--}%
	"bSortable": false,
	"sWidth":"396px",
	"bVisible": true
}
,
{
	"sName": "tgglApp",
	"mDataProp": "tgglApp",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"396px",
	"bVisible": true
}
,
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"396px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "idApp",
	"mDataProp": "idApp",
	"bSortable": false,
	"sWidth":"0px",
	"sDefaultContent": '',
	"bVisible": false
}

],
    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
    
                        var t017Tanggal = $('#search_t017Tanggal').val();
						var t017TanggalDay = $('#search_t017Tanggal_day').val();
						var t017TanggalMonth = $('#search_t017Tanggal_month').val();
						var t017TanggalYear = $('#search_t017Tanggal_year').val();

						var t017Tanggal2 = $('#search_t017Tanggal2').val();
						var t017TanggalDay2 = $('#search_t017Tanggal2_day').val();
						var t017TanggalMonth2 = $('#search_t017Tanggal2_month').val();
						var t017TanggalYear2 = $('#search_t017Tanggal2_year').val();

						if(t017Tanggal){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal_dp', "value": t017Tanggal},
									{"name": 'sCriteria_t017Tanggal_day', "value": t017TanggalDay},
									{"name": 'sCriteria_t017Tanggal_month', "value": t017TanggalMonth},
									{"name": 'sCriteria_t017Tanggal_year', "value": t017TanggalYear}
							);
						}


						if(t017Tanggal2){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal2', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal2_dp', "value": t017Tanggal2},
									{"name": 'sCriteria_t017Tanggal2_day', "value": t017TanggalDay2},
									{"name": 'sCriteria_t017Tanggal2_month', "value": t017TanggalMonth2},
									{"name": 'sCriteria_t017Tanggal2_year', "value": t017TanggalYear2}
							);
						}

    $.ajax({ "dataType": 'json',
        "type": "POST",
        "url": sSource,
        "data": aoData ,
        "success": function (json) {
            fnCallback(json);
           },
        "complete": function () {
           }
    });
}
});
});

</g:javascript>
