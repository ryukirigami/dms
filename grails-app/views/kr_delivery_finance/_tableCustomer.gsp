
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="customer_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable fixed" style="table-layout: fixed; ">

    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px; width: 150px;">
            <div>&nbsp;<input type="checkbox" class="select-all" />&nbsp;
                <g:message code="timeTrackingGR.sa.label" default="Nomor Customer" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px; width: 200px;">
                <g:message code="timeTrackingGR.sa.label" default="Nama Customer" />
        </th>
    </tr>
    <tr>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_kode" style="padding-top: 0px;position:relative; margin-top: 0px; width: 150px;">
                <input type="text" name="search_kode" class="search_init" />
            </div>
        </th>
        <th style="border-top: none;padding: 5px; ">
            <div id="filter_nama" style="padding-top: 0px;position:relative; margin-top: 0px; width: 200px;">
                <input type="text" name="search_nama" class="search_init" />
            </div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var customerTable;
var reloadcustomerTable;
$(function(){
	
	reloadcustomerTable = function() {
		customerTable.fnDraw();
	}
    var recordscustomerperpage = [];//new Array();
    var ancustomerSelected;
    var jmlReccustomerPerPage=0;
    var id;
    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	customerTable.fnDraw();
		}
	});

	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	customerTable = $('#customer_datatables').dataTable({
		"sScrollX": "400px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rscustomer = $("#customer_datatables tbody .row-select");
            var jmlcustomerCek = 0;
            var nRow;
            var idRec;
            rscustomer.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordscustomerperpage[idRec]=="1"){
                    jmlcustomerCek = jmlcustomerCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordscustomerperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlReccustomerPerPage = rscustomer.length;
            if(jmlcustomerCek == jmlReccustomerPerPage && jmlReccustomerPerPage > 0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesCustomerList")}",
		"aoColumns": [

{
	"sName": "id",
	"mDataProp": "kode",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" name="customer" value="'+row['id']+'" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp; '+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},{
	"sName": "fullNama",
	"mDataProp": "nama",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"250px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
            var kodeCustomer = $('#filter_kode input').val();
            if(kodeCustomer){
                aoData.push(
                        {"name": 'kodeCustomer', "value": kodeCustomer}
                );
            }
            var namaCustomer = $('#filter_nama input').val();
            if(namaCustomer){
                aoData.push(
                        {"name": 'namaCustomer', "value": namaCustomer}
                );
            }
	
            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
                    });
		}			
	});
	$('.select-all').click(function(e) {

        $("#customer_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordscustomerperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordscustomerperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#customer_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordscustomerperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            ancustomerSelected = customerTable.$('tr.row_selected');
            if(jmlReccustomerPerPage == ancustomerSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordscustomerperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
