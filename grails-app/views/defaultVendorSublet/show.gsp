

<%@ page import="com.kombos.administrasi.DefaultVendorSublet" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'defaultVendorSublet.label', default: 'Default Vendor Job Sublet')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteDefaultVendorSublet;

$(function(){ 
	deleteDefaultVendorSublet=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/defaultVendorSublet/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadDefaultVendorSubletTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-defaultVendorSublet" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="defaultVendorSublet"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${defaultVendorSubletInstance?.section}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="section-label" class="property-label"><g:message
					code="defaultVendorSublet.section.label" default="Nama Section" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="section-label">
						%{--<ba:editableValue
								bean="${defaultVendorSubletInstance}" field="section"
								url="${request.contextPath}/DefaultVendorSublet/updatefield" type="text"
								title="Enter section" onsuccess="reloadDefaultVendorSubletTable();" />--}%
							

                                <g:fieldValue bean="${defaultVendorSubletInstance?.section}" field="m051NamaSection"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${defaultVendorSubletInstance?.serial}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="serial-label" class="property-label"><g:message
					code="defaultVendorSublet.serial.label" default="Nama Serial" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="serial-label">
						%{--<ba:editableValue
								bean="${defaultVendorSubletInstance}" field="serial"
								url="${request.contextPath}/DefaultVendorSublet/updatefield" type="text"
								title="Enter serial" onsuccess="reloadDefaultVendorSubletTable();" />--}%
							

                                <g:fieldValue bean="${defaultVendorSubletInstance?.serial}" field="m052NamaSerial"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${defaultVendorSubletInstance?.operation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="operation-label" class="property-label"><g:message
					code="defaultVendorSublet.operation.label" default="Nama Repair" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="operation-label">
						%{--<ba:editableValue
								bean="${defaultVendorSubletInstance}" field="operation"
								url="${request.contextPath}/DefaultVendorSublet/updatefield" type="text"
								title="Enter operation" onsuccess="reloadDefaultVendorSubletTable();" />--}%
							

                            <g:fieldValue bean="${defaultVendorSubletInstance?.operation}" field="m053NamaOperation"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${defaultVendorSubletInstance?.vendor}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="vendor-label" class="property-label"><g:message
					code="defaultVendorSublet.vendor.label" default="Nama Vendor" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="vendor-label">
						%{--<ba:editableValue
								bean="${defaultVendorSubletInstance}" field="vendor"
								url="${request.contextPath}/DefaultVendorSublet/updatefield" type="text"
								title="Enter vendor" onsuccess="reloadDefaultVendorSubletTable();" />--}%
							

                        <g:fieldValue bean="${defaultVendorSubletInstance?.vendor}" field="m121Nama"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${defaultVendorSubletInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="defaultVendorSublet.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${defaultVendorSubletInstance}" field="lastUpdProcess"
								url="${request.contextPath}/DefaultVendorSublet/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadDefaultVendorSubletTable();" />--}%
							
								<g:fieldValue bean="${defaultVendorSubletInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${defaultVendorSubletInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="defaultVendorSublet.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${defaultVendorSubletInstance}" field="dateCreated"
								url="${request.contextPath}/DefaultVendorSublet/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadDefaultVendorSubletTable();" />--}%
							
								<g:formatDate date="${defaultVendorSubletInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${defaultVendorSubletInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="defaultVendorSublet.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${defaultVendorSubletInstance}" field="createdBy"
                                url="${request.contextPath}/DefaultVendorSublet/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadDefaultVendorSubletTable();" />--}%

                        <g:fieldValue bean="${defaultVendorSubletInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${defaultVendorSubletInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="defaultVendorSublet.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${defaultVendorSubletInstance}" field="lastUpdated"
								url="${request.contextPath}/DefaultVendorSublet/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadDefaultVendorSubletTable();" />--}%
							
								<g:formatDate date="${defaultVendorSubletInstance?.lastUpdated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${defaultVendorSubletInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="defaultVendorSublet.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${defaultVendorSubletInstance}" field="updatedBy"
                                url="${request.contextPath}/DefaultVendorSublet/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadDefaultVendorSubletTable();" />--}%

                        <g:fieldValue bean="${defaultVendorSubletInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${defaultVendorSubletInstance?.id}"
					update="[success:'defaultVendorSublet-form',failure:'defaultVendorSublet-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteDefaultVendorSublet('${defaultVendorSubletInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
