<%@ page import="com.kombos.parts.Vendor; com.kombos.parts.Vendor; com.kombos.administrasi.Operation; com.kombos.administrasi.Section; com.kombos.administrasi.Serial; com.kombos.administrasi.DefaultVendorSublet" %>



<div class="control-group fieldcontain ${hasErrors(bean: defaultVendorSubletInstance, field: 'section', 'error')} required">
	<label class="control-label" for="section">
		<g:message code="defaultVendorSublet.section.label" default="Nama Section" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="section" name="section.id" from="${Section.list()}" optionKey="id" required="" value="${defaultVendorSubletInstance?.section?.id}" class="many-to-one"/>
	</div>
</div>
<g:javascript>
    document.getElementById("section").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: defaultVendorSubletInstance, field: 'serial', 'error')} required">
	<label class="control-label" for="serial">
		<g:message code="defaultVendorSublet.serial.label" default="Nama Serial" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="serial" name="serial.id" from="${Serial.list()}" optionKey="id" required="" value="${defaultVendorSubletInstance?.serial?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: defaultVendorSubletInstance, field: 'operation', 'error')} ">
	<label class="control-label" for="operation">
		<g:message code="defaultVendorSublet.operation.label" default="Nama Repair" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="operation" name="operation.id" from="${Operation.list()}" optionKey="id" value="${defaultVendorSubletInstance?.operation?.id}" class="many-to-one" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: defaultVendorSubletInstance, field: 'vendor', 'error')} ">
	<label class="control-label" for="vendor">
		<g:message code="defaultVendorSublet.vendor.label" default="Nama Vendor" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="vendor" name="vendor.id" from="${com.kombos.parts.Vendor.list()}" optionKey="id" value="${defaultVendorSubletInstance?.vendor?.id}" class="many-to-one" />
	</div>
</div>

