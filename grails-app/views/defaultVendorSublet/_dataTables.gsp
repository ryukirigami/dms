
<%@ page import="com.kombos.administrasi.DefaultVendorSublet" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="defaultVendorSublet_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="defaultVendorSublet.section.label" default="Nama Section" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="defaultVendorSublet.serial.label" default="Nama Serial" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="defaultVendorSublet.operation.label" default="Nama Repair" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="defaultVendorSublet.vendor.label" default="Nama Vendor" /></div>
			</th>


		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_section" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_section" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_serial" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_serial" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_operation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_operation" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_vendor" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_vendor" class="search_init" />
				</div>
			</th>
	


		</tr>
	</thead>
</table>

<g:javascript>
var defaultVendorSubletTable;
var reloadDefaultVendorSubletTable;
$(function(){
	
	reloadDefaultVendorSubletTable = function() {
		defaultVendorSubletTable.fnDraw();
	}

	var recordsDefaultVendorSubletperpage = [];//new Array();
    var anDefaultVendorSubletSelected;
    var jmlRecDefaultVendorSubletPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	defaultVendorSubletTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	defaultVendorSubletTable = $('#defaultVendorSublet_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsDefaultVendorSublet = $("#defaultVendorSublet_datatables tbody .row-select");
            var jmlDefaultVendorSubletCek = 0;
            var nRow;
            var idRec;
            rsDefaultVendorSublet.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsDefaultVendorSubletperpage[idRec]=="1"){
                    jmlDefaultVendorSubletCek = jmlDefaultVendorSubletCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsDefaultVendorSubletperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecDefaultVendorSubletPerPage = rsDefaultVendorSublet.length;
            if(jmlDefaultVendorSubletCek==jmlRecDefaultVendorSubletPerPage && jmlRecDefaultVendorSubletPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "section",
	"mDataProp": "section",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "serial",
	"mDataProp": "serial",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "operation",
	"mDataProp": "operation",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "vendor",
	"mDataProp": "vendor",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var section = $('#filter_section input').val();
						if(section){
							aoData.push(
									{"name": 'sCriteria_section', "value": section}
							);
						}
	
						var serial = $('#filter_serial input').val();
						if(serial){
							aoData.push(
									{"name": 'sCriteria_serial', "value": serial}
							);
						}
	
						var operation = $('#filter_operation input').val();
						if(operation){
							aoData.push(
									{"name": 'sCriteria_operation', "value": operation}
							);
						}
	
						var vendor = $('#filter_vendor input').val();
						if(vendor){
							aoData.push(
									{"name": 'sCriteria_vendor', "value": vendor}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#defaultVendorSublet_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsDefaultVendorSubletperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsDefaultVendorSubletperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#defaultVendorSublet_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsDefaultVendorSubletperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anDefaultVendorSubletSelected = defaultVendorSubletTable.$('tr.row_selected');
            if(jmlRecDefaultVendorSubletPerPage == anDefaultVendorSubletSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsDefaultVendorSubletperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
