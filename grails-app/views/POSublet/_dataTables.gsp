
<%@ page import="com.kombos.reception.POSublet" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="POSublet_datatables_${idTable}" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
            <th rowspan="2" style="vertical-align: middle">
                <div> </div>
            </th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="POSublet.nomorWO.label" default="Nomor WO" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="POSublet.tanggalWO.label" default="Tanggal WO" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="POSublet.nomorPolisi.label" default="Nomor Polisi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="POSublet.baseModel.label" default="Base Model" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="POSublet.serviceAdvisor.label" default="SA" /></div>
			</th>

		
		</tr>
		<tr>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_nomorWO" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_tanggalWO" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                </div>
            </th>


			<th style="border-top: none;padding: 5px;">
				<div id="filter_nomorPolisi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_baseModel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
				</div>
			</th>



			<th style="border-top: none;padding: 5px;">
				<div id="filter_SA" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
				</div>
			</th>

		</tr>
	</thead>
</table>

<g:javascript>
var POSubletTable;
var reloadPOSubletTable;
$(function(){
	
	reloadPOSubletTable = function() {
		POSubletTable.fnDraw();
	}

	
	$('#search_tanggalWO').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalWO_day').val(newDate.getDate());
			$('#search_tanggalWO_month').val(newDate.getMonth()+1);
			$('#search_tanggalWO_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			POSubletTable.fnDraw();
	});



$('#search_noWo').bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
			staCari = "yes";
		 	POSubletTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

    var anOpen = [];
	$('#POSublet_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = POSubletTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/POSublet/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = POSubletTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			POSubletTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});


     $('#view').click(function(e){
        e.stopPropagation();
        staCari = "yes";
		POSubletTable.fnDraw();
//        reloadAuditTrailTable();
	});

	$('#clear').click(function(e){
        staCari = "no";
        $('#search_tglStart').val("");
        $('#search_noWo').val("");
        $('#search_tglEnd').val("");


        $("#poBelumKirim").prop("checked", false);
        $("#outstandingPO").prop("checked", false);
        $("#selesaiPO").prop("checked", false);
        $("#menungguApp").prop("checked", false);
        $("#approved").prop("checked", false);
        $("#unapproved").prop("checked", false);


        e.stopPropagation();
		POSubletTable.fnDraw();
	});


	POSubletTable = $('#POSublet_datatables_${idTable}').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
               "sName": "t401NoWO",
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
}
,

{
	"sName": "t401NoWO",
	"mDataProp": "nomorWO",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "t401TanggalWO",
	"mDataProp": "tanggalWO",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "historyCustomerVehicle",
	"mDataProp": "nomorPolisi",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "historyCustomerVehicle.fullModelCode",
	"mDataProp": "baseModel",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t401NamaSA",
	"mDataProp": "SA",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var nomorWO = $('#filter_nomorWO input').val();
						if(nomorWO){
							aoData.push(
									{"name": 'sCriteria_nomorWO', "value": nomorWO}
							);
						}

	
						var nomorPolisi = $('#filter_nomorPolisi input').val();
						if(nomorPolisi){
							aoData.push(
									{"name": 'sCriteria_reception', "value": nomorPolisi}
							);
						}
	
						var baseModel = $('#filter_baseModel input').val();
						if(baseModel){
							aoData.push(
									{"name": 'sCriteria_baseModel', "value": baseModel}
							);
						}
	

						var tanggalWO = $('#search_tanggalWO').val();
						var tanggalWODay = $('#search_tanggalWO_day').val();
						var tanggalWOMonth = $('#search_tanggalWO_month').val();
						var tanggalWOYear = $('#search_tanggalWO_year').val();
						
						if(tanggalWO){
							aoData.push(
									{"name": 'sCriteria_tanggalWO', "value": "date.struct"},
									{"name": 'sCriteria_tanggalWO_dp', "value": tanggalWO},
									{"name": 'sCriteria_tanggalWO_day', "value": tanggalWODay},
									{"name": 'sCriteria_tanggalWO_month', "value": tanggalWOMonth},
									{"name": 'sCriteria_tanggalWO_year', "value": tanggalWOYear}
							);
						}


						 var belumKirim = function(){
                                if($("#poBelumKirim").is(':checked')){
                                    return 1;
                                }else{
                                 return 0;
                                }
	                     };

	                     if(belumKirim){
							aoData.push(
									{"name": 'belumKirim', "value": belumKirim}
							);
						}

						var outstandingPO = function(){
                                if($("#outstandingPO").is(':checked')){
                                    return 1;
                                }else{
                                 return 0;
                                }
	                     };

	                     if(outstandingPO){
							aoData.push(
									{"name": 'outstandingPO', "value": outstandingPO}
							);
						}

						var selesaiPO = function(){
                                if($("#selesaiPO").is(':checked')){
                                    return 1;
                                }else{
                                 return 0;
                                }
	                     };

	                     if(selesaiPO){
							aoData.push(
									{"name": 'selesaiPO', "value": selesaiPO}
							);
						}

						var menungguApp = function(){
                                if($("#menungguApp").is(':checked')){
                                    return 1;
                                }else{
                                 return 0;
                                }
	                     };

	                     if(menungguApp){
							aoData.push(
									{"name": 'menungguApp', "value": menungguApp}
							);
						}

						var approved = function(){
                                if($("#approved").is(':checked')){
                                    return 1;
                                }else{
                                 return 0;
                                }
	                     };

	                     if(approved){
							aoData.push(
									{"name": 'approved', "value": approved}
							);
						}

						var unapproved = function(){
                                if($("#unapproved").is(':checked')){
                                    return 1;
                                }else{
                                 return 0;
                                }
	                     };

	                     if(unapproved){
							aoData.push(
									{"name": 'unapproved', "value": unapproved}
							);
						}

					    var search_noWo = $("#search_noWo").val();
					    var tglStart = $("#search_tglStart").val();
	                    var tglAkhir = $("#search_tglEnd").val();

	                    if(search_noWo){
	                        aoData.push(
									{"name": 'search_noWo', "value": search_noWo}
							);
	                    }
	                    if(tglStart){
	                        aoData.push(
									{"name": 'tglStart', "value": tglStart}
							);
	                    }


	                    if(tglAkhir){
	                        aoData.push(
									{"name": 'tglEnd', "value": tglAkhir}
							);
	                    }

                        aoData.push(
                                {"name": 'staCari', "value": staCari}
                        );
	
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
