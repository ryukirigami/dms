<%@ page import="com.kombos.reception.POSublet" %>


<g:javascript>
      function cariJobRcp(a){
          alert(a);
      }
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: POSubletInstance, field: 'reception', 'error')} ">
	<label class="control-label" for="reception">
		<g:message code="POSublet.reception.label" default="Nomor WO" />
		
	</label>
	<div class="controls">
	<g:select id="reception" name="reception.id" from="${com.kombos.reception.Reception.createCriteria().list {eq('staDel','0');eq('staSave','0');eq("companyDealer",session?.userCompanyDealer);}}" optionKey="id" required="" value="${POSubletInstance?.reception?.id}" class="many-to-one" onchange="cariJobRcp(this.value)"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POSubletInstance, field: 'operation', 'error')} ">
    <label class="control-label" for="operation">
        <g:message code="POSublet.operation.label" default="Job" />

    </label>
    <div class="controls">
        <g:select id="operation" name="operation.id" from="${com.kombos.administrasi.Operation.list()}" optionKey="id" required="" value="${POSubletInstance?.operation?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POSubletInstance, field: 't412NoPOSublet', 'error')} ">
	<label class="control-label" for="t412NoPOSublet">
		<g:message code="POSublet.t412NoPOSublet.label" default="No PO Sublet" />
		
	</label>
	<div class="controls">
	<g:textField name="t412NoPOSublet" value="${POSubletInstance?.t412NoPOSublet}" />
	<g:hiddenField name="staDel" value="0" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POSubletInstance, field: 't412Perihal', 'error')} ">
	<label class="control-label" for="t412Perihal">
		<g:message code="POSublet.t412Perihal.label" default="Perihal" />
		
	</label>
	<div class="controls">
	<g:textField name="t412Perihal" value="${POSubletInstance?.t412Perihal}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POSubletInstance, field: 't412TanggalPO', 'error')} ">
	<label class="control-label" for="t412TanggalPO">
		<g:message code="POSublet.t412TanggalPO.label" default="Tanggal PO" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="t412TanggalPO" precision="day" value="${POSubletInstance?.t412TanggalPO}" format="yyyy-MM-dd"/>
	</div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: POSubletInstance, field: 't412xKet', 'error')} ">
	<label class="control-label" for="t412xKet">
		<g:message code="POSublet.t412xKet.label" default="Ket" />
		
	</label>
	<div class="controls">
	<g:textField name="t412xKet" value="${POSubletInstance?.t412xKet}" />
	</div>
</div>
