<%@ page import="com.kombos.reception.POSublet" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title><g:message code="default.create.posublet.label" default="Input PO Sublet" /></title>
	</head>
	<body>
		<div id="create-POSublet" class="content scaffold-create" role="main">
			<legend><g:message code="default.create.posublet.label" default="Input PO Sublet" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${POSubletInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${POSubletInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
			<g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="reloadPOSubletTable();" update="POSublet-form"
              url="[controller: 'POSublet', action:'save']">	
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
					<a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
					<g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
