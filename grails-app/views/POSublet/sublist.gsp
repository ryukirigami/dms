<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var POSubletSubTable;
$(function(){ 
var anOpen = [];

    if(POSubletSubTable)
    	POSubletSubTable.dataTable().fnDestroy();

    POSubletSubTable = $('#PO_datatables_sub_${idTable}').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [

{
               "sName": "naon",
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"1.9%",
               "sDefaultContent": ''
},
{
               "sName": "naon",
               "mDataProp": null,
               "bSortable": false,

              "sWidth":"9px",
               "sDefaultContent": ''
},
{
	"sName": "kodeJob",
	"mDataProp": "kodeJob",
	"aTargets": [0],
    "mRender": function ( data, type, row ) {
                   return '<input type="hidden" value="'+row['id']+'">'+data+'&nbsp;&nbsp;<a class="pull-right cell-action" href="javascript:void(0);" onclick="loadPopup('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i></a>';
                },
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
},
{
	"sName": "namaJob",
	"mDataProp": "namaJob",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
},
{
	"sName": "vendor",
	"mDataProp": "vendor",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
},
{
	"sName": "hargaBeli",
	"mDataProp": "hargaBeli",
	"bSortable": false,
	"sWidth":"250px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"sName": "hargaJual",
	"mDataProp": "hargaJual",
	"bSortable": false,
	"sWidth":"250px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"sName": "statusApproval",
	"mDataProp": "statusApproval",
	"bSortable": false,
	"mRender": function ( data, type, row ) {
        if(data=='Approved'){
            return "Approved";
        }else if(data=='UnApproved'){
            return "Un Approved";
        }else if(data=='Waiting for approval'){
            return "Menunggu Approval";
        }else{
            return data;
        }
    },
    "sWidth":"250px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "alasanUnapproved",
	"mDataProp": "alasanUnApproved",
	"bSortable": false,
	"sWidth":"250px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "tanggalKirimSublet",
	"mDataProp": "tanggalKirimSublet",
	"bSortable": false,
	"sWidth":"250px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "tanggalSelesaiSublet",
	"mDataProp": "tanggalSelesaiSublet",
	"bSortable": false,
	"sWidth":"400px",
	"sDefaultContent": '',
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'tanggalRequest', "value": "${tanggalRequest}"}
						);

						aoData.push(
									{"name": 'idReception', "value": "${idReception}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
    </g:javascript>
</head>
<body>
<div class="innerDetails">
    <table id="PO_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover" style="width: 100%">
        <thead>
        <tr style="width: 100%">
            <th></th>
            <th></th>
            <th style="border-bottom: none; padding: 5px; width: 300px;">
                <div>Kode Job</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 300px;">
                <div>Nama Job</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 300px;">
                <div>Vendor</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 300px;">
                <div>Harga Beli</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 300px;">
                <div>Harga Jual</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 300px;">
                <div>Status Approval</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 300px;">
                <div>Alasan Unapproved</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 300px;">
                <div>Tanggal Kirim Sublet</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 300px;">
                <div>Tanggal Selesai Sublet</div>
            </th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
