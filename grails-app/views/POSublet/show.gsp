

<%@ page import="com.kombos.reception.POSublet" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
       value="${message(code: 'POSublet.label', default: 'Sublet Per WO')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePOSublet;

$(function(){ 
	deletePOSublet=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/POSublet/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPOSubletTable();
   				expandTableLayout('POSublet');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-POSublet" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="POSublet"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${POSubletInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="POSublet.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${POSubletInstance}" field="createdBy"
								url="${request.contextPath}/POSublet/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadPOSubletTable();" />--}%
							
								<g:fieldValue bean="${POSubletInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POSubletInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="POSublet.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${POSubletInstance}" field="dateCreated"
								url="${request.contextPath}/POSublet/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadPOSubletTable();" />--}%
							
								<g:formatDate date="${POSubletInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POSubletInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="POSublet.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${POSubletInstance}" field="lastUpdProcess"
								url="${request.contextPath}/POSublet/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadPOSubletTable();" />--}%
							
								<g:fieldValue bean="${POSubletInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POSubletInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="POSublet.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${POSubletInstance}" field="lastUpdated"
								url="${request.contextPath}/POSublet/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadPOSubletTable();" />--}%
							
								<g:formatDate date="${POSubletInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POSubletInstance?.operation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="operation-label" class="property-label"><g:message
					code="POSublet.operation.label" default="Operation" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="operation-label">
						%{--<ba:editableValue
								bean="${POSubletInstance}" field="operation"
								url="${request.contextPath}/POSublet/updatefield" type="text"
								title="Enter operation" onsuccess="reloadPOSubletTable();" />--}%
							
								<g:link controller="operation" action="show" id="${POSubletInstance?.operation?.id}">${POSubletInstance?.operation?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POSubletInstance?.reception}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="reception-label" class="property-label"><g:message
					code="POSublet.reception.label" default="Reception" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="reception-label">
						%{--<ba:editableValue
								bean="${POSubletInstance}" field="reception"
								url="${request.contextPath}/POSublet/updatefield" type="text"
								title="Enter reception" onsuccess="reloadPOSubletTable();" />--}%
							
								<g:link controller="reception" action="show" id="${POSubletInstance?.reception?.id}">${POSubletInstance?.reception?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POSubletInstance?.t412NoPOSublet}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t412NoPOSublet-label" class="property-label"><g:message
					code="POSublet.t412NoPOSublet.label" default="T412 No POS ublet" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t412NoPOSublet-label">
						%{--<ba:editableValue
								bean="${POSubletInstance}" field="t412NoPOSublet"
								url="${request.contextPath}/POSublet/updatefield" type="text"
								title="Enter t412NoPOSublet" onsuccess="reloadPOSubletTable();" />--}%
							
								<g:fieldValue bean="${POSubletInstance}" field="t412NoPOSublet"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POSubletInstance?.t412Perihal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t412Perihal-label" class="property-label"><g:message
					code="POSublet.t412Perihal.label" default="T412 Perihal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t412Perihal-label">
						%{--<ba:editableValue
								bean="${POSubletInstance}" field="t412Perihal"
								url="${request.contextPath}/POSublet/updatefield" type="text"
								title="Enter t412Perihal" onsuccess="reloadPOSubletTable();" />--}%
							
								<g:fieldValue bean="${POSubletInstance}" field="t412Perihal"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POSubletInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="POSublet.staDel.label" default="T412 Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${POSubletInstance}" field="staDel"
								url="${request.contextPath}/POSublet/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadPOSubletTable();" />--}%
							
								<g:fieldValue bean="${POSubletInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POSubletInstance?.t412TanggalPO}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t412TanggalPO-label" class="property-label"><g:message
					code="POSublet.t412TanggalPO.label" default="T412 Tanggal PO" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t412TanggalPO-label">
						%{--<ba:editableValue
								bean="${POSubletInstance}" field="t412TanggalPO"
								url="${request.contextPath}/POSublet/updatefield" type="text"
								title="Enter t412TanggalPO" onsuccess="reloadPOSubletTable();" />--}%
							
								<g:formatDate date="${POSubletInstance?.t412TanggalPO}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POSubletInstance?.t412xDivisi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t412xDivisi-label" class="property-label"><g:message
					code="POSublet.t412xDivisi.label" default="T412x Divisi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t412xDivisi-label">
						%{--<ba:editableValue
								bean="${POSubletInstance}" field="t412xDivisi"
								url="${request.contextPath}/POSublet/updatefield" type="text"
								title="Enter t412xDivisi" onsuccess="reloadPOSubletTable();" />--}%
							
								<g:fieldValue bean="${POSubletInstance}" field="t412xDivisi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POSubletInstance?.t412xKet}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t412xKet-label" class="property-label"><g:message
					code="POSublet.t412xKet.label" default="T412x Ket" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t412xKet-label">
						%{--<ba:editableValue
								bean="${POSubletInstance}" field="t412xKet"
								url="${request.contextPath}/POSublet/updatefield" type="text"
								title="Enter t412xKet" onsuccess="reloadPOSubletTable();" />--}%
							
								<g:fieldValue bean="${POSubletInstance}" field="t412xKet"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POSubletInstance?.t412xNamaUser}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t412xNamaUser-label" class="property-label"><g:message
					code="POSublet.t412xNamaUser.label" default="T412x Nama User" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t412xNamaUser-label">
						%{--<ba:editableValue
								bean="${POSubletInstance}" field="t412xNamaUser"
								url="${request.contextPath}/POSublet/updatefield" type="text"
								title="Enter t412xNamaUser" onsuccess="reloadPOSubletTable();" />--}%
							
								<g:fieldValue bean="${POSubletInstance}" field="t412xNamaUser"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POSubletInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="POSublet.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${POSubletInstance}" field="updatedBy"
								url="${request.contextPath}/POSublet/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadPOSubletTable();" />--}%
							
								<g:fieldValue bean="${POSubletInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('POSublet');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${POSubletInstance?.id}"
					update="[success:'POSublet-form',failure:'POSublet-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePOSublet('${POSubletInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
