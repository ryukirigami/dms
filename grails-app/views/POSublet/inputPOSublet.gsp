<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout, baseapplist,autoNumeric" />
</head>
<body>
<div class="navbar box-header no-border">
    Reception - Input PO Sublet
</div>
<table class="table table-bordered table-hover">
    <tbody>
    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                <g:message code="POSublet.nomorWO.label" default="Nomor WO" />

            </label>

        </td>

        <td class="span3">
            <input type="hidden" id="idJobRCP" name="idJobRCP" value="${idJobRCP}"/>
            <label class="control-label" for="namaVendor" >
                ${nomorWO}
            </label>
        </td>
    </tr>

    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label" for="po">
                <g:message code="POSublet.job.label" default="Job" />

            </label>
        </td>
        <td class="span3" >
            <label class="control-label" for="namaJob" >
                ${namaJob}
            </label>
        </td>
    </tr>

    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label" for="vendor">
                <g:message code="POSublet.vendor.label" default="Vendor" />

            </label>
        </td>
        <td class="span3">
            <g:select name="vendor" id="vendor" from="${com.kombos.parts.Vendor.createCriteria().list{eq("staDel","0"); order("m121Nama", "asc") }}" optionKey="id" value="${vendor}" />
        </td>
    </tr>

    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label" for="po">
                <g:message code="POSublet.job.label" default="Harga Beli" />
            </label>
        </td>
        <td class="span3" >
            <label class="control-label" for="namaJob" >
                <g:textField name="hargaBeli" class="nomor" id="hargaBeli"  value="${hargaBeli}"/>

            </label>
        </td>
    </tr>


    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                <g:message code="POSublet.jumlahSublet.label" default="Harga Jual Sublet" />

            </label>

        </td>

        <td class="span3">
            <label class="control-label" for="jumlahSublet" >
                <g:textField name="jumlahSublet" class="nomor" id="jumlahSublet" />
            </label>
        </td>
    </tr>
    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                <g:message code="POSublet.catatan.label" default="Catatan" />

            </label>

        </td>

        <td class="span3">
            <label class="control-label" for="catatan" >
                <g:textField name="catatan" id="catatan" />
            </label>
        </td>
    </tr>
    </div>
    </tr>
    </tbody>
</table>
<div class="modal-footer">
</div>

</body>
</html>
<script type="text/javascript">
    jQuery(function($) {
        $('.nomor').autoNumeric('init',{
            vMin:'0',
            aSep:'',
            mDec:'2'
        });
    });
</script>



