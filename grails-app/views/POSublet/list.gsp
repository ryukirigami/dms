
<%@ page import="com.kombos.reception.POSublet" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title><g:message code="POSublet.label" default="Reception - View Sublet Per WO" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var shrinkTableLayout;
			var expandTableLayout;
			var edit;
			var loadModal;
			var loadPopup;
			var aprvPOSublet;
			var staCari = "no";
			$(function(){

			loadModal = function(){
               checkJob =[];
               var id;
                $("table[id^=PO_datatables] tbody .row-select").each(function() {
                    if(this.checked){
                        id = $(this).next("input:hidden").val();
                        checkJob.push(id);
                    }
                });
                if(checkJob.length<1 || checkJob.length>1){
                    alert('Pilih Satu dan hanya boleh satu yang dipilih');
                    return;
                }
                else{
                    var checkJobMap = JSON.stringify(checkJob);

                    loadPopup(id);
                }

            checkJob = [];

          };

          loadPopup = function(idJobRCP){
                    $("#contentPOSublet").empty();
                    expandTableLayout();
                    $.ajax({type:'POST', url:'${request.contextPath}/POSublet/inputPOSublet',
                        success:function(data,textStatus){
                                $("#contentPOSublet").html(data);
                                $("#modalPOSublet").modal({
                                    "backdrop" : "false",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '600px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        data : {idJobRCP : idJobRCP},
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                };

                aprvPOSublet = function(){
                    var idJobRCP = $('#idJobRCP').val();
                    var jumlahSublet = $("#jumlahSublet").val();
                    var hargaBeli = $("#hargaBeli").val();
                    var vendor = $("#vendor").val();
                    var catatan = $("#catatan").val();

                    if(jumlahSublet == ""){
                        toastr.error("Jumlah Sublet harus diisi");
                        return;
                    }

                    if(catatan == ""){
                        toastr.error("Catatan harus diisi");
                        return;
                    }

                    $.ajax({type:'POST', url:'${request.contextPath}/POSublet/saveApproval',
                            success:function(){
                                    toastr.success("PO Sublet Sudah dikirim ke Approval");
                                    $("#contentPOSublet").html("");
                                    $("#modalPOSublet").modal('hide');
                                    $('#spinner').fadeOut();
                                    reloadPOSubletTable();

                            },
                            data : {idJobRCP : idJobRCP, vendor : vendor, jumlahSublet : jumlahSublet, catatan : catatan,hargaBeli:hargaBeli},
                            error:function(XMLHttpRequest,textStatus,errorThrown){},
                            complete:function(XMLHttpRequest,textStatus){
                                $('#spinner').fadeOut();
                            }
                        });

                }

			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout();
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('POSublet', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('POSublet', '${request.contextPath}/POSublet/massdelete', reloadPOSubletTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

                shrinkTableLayout = function(){
                    $("#POSublet-table").hide();
                    $("#POSublet-form").css("display","block");
                }

                expandTableLayout = function(){
                    try{
                        reloadPOSubletTable();
                    }catch(e){}
                    $("#POSublet-table").show();
                    $("#POSublet-form").css("display","none");
                }


				show = function(id) {
					showInstance('POSublet','${request.contextPath}/POSublet/show/'+id);
				};
				
				edit = function(id) {
					editInstance('POSublet','${request.contextPath}/POSublet/edit/'+id);
				};

				$("#modalPOSublet").on("show", function() {
                    $("#closeModal").on("click", function(e) {
                        $("#modalPOSublet").modal('hide');
                    });
                });

                $("#modalPOSublet").on("hide", function() {
                    $("#closeModal").off("click");
                });


});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="POSublet.label" default="Reception - View Sublet Per WO" /></span>
		<ul class="nav pull-right">
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-plus"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-remove"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="POSublet-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>

                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="tanggalWO">
                                <g:message code="reception.tanggalWO.label" default="Nomor WO" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="nomorWO" class="controls">
                                <input type="text" id="search_noWo" name="search_noWo">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="tanggalWO">
                                <g:message code="reception.tanggalWO.label" default="Tanggal WO" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="tanggal_invoice" class="controls">
                                <ba:datePicker id="search_tglStart" name="search_tglStart" precision="day" format="dd/mm/yyyy"  value="" />
                                &nbsp;-&nbsp;
                                <ba:datePicker id="search_tglEnd" name="search_tglEnd" precision="day" format="dd/mm/yyyy"  value="" />
                            </div>
                        </td>
                    </tr>
                </table>

                <div class="row-fluid">
                    <div class="span4 form-horizontal" >
                        <legend style="margin-bottom: 2px"> <label style="font-size: 12px">Kriteria PO </label></legend>
                        <table  class="table table-bordered table-dark table-hover">
                            <tbody>
                            <tr>
                                <td>
                                    <input type="checkbox" id="poBelumKirim" name="poBelumKirim" value="1">&nbsp;Belum Kirim PO Sublet&nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" id="outstandingPO" name="outStandingPO" value="1">&nbsp;Outstanding PO Sublet&nbsp;&nbsp;

                                </td>
                            </tr>
                            <tr>

                                <td >
                                    <input type="checkbox" id="selesaiPO" name="selesaiPO" value="1">&nbsp;Sudah Selesai PO Sublet&nbsp;&nbsp;
                                </td>

                            </tr>

                            </tbody>

                        </table>


                    </div>
                    <div class="span4 form-horizontal" >
                        <legend style="margin-bottom: 2px"><label style="font-size: 12px">Status Approval</label></legend>
                        <table  class="table table-bordered table-dark table-hover">
                            <tr>
                                <td>
                                    <input type="checkbox" id="menungguApp" name="menungguApp" value="1">&nbsp;Menunggu Approval&nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" id="approved" name="approved" value="1">&nbsp;Approved&nbsp;&nbsp;

                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <input type="checkbox" id="unapproved" name="unapproved" value="1">&nbsp;Un Approved
                                </td>

                            </tr>
                        </table>

                    </div>

                </div>

            <br/>
            <legend>Filter Sublet</legend>
            <table style="padding-right: 10px">
                <tr>
                    <td colspan="3" >
                        <div class="controls" style="right: 0">
                            <button class="btn btn-primary" name="view" id="view">Search</button>
                            <button class="btn cancel" name="clear" id="clear">Clear</button>
                        </div>
                    </td>
                </tr>
            </table>

            <br/>
            <legend>View Sublet Per WO</legend>
            <br/>
            <g:render template="dataTables" />

        </div>
		<div class="span7" id="POSublet-form" style="display: none;"></div>
	</div>
    <div id="modalPOSublet" class="modal fade">
        <div class="modal-dialog" style="width: 600px;">
            <div class="modal-content" style="width: 600px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 600px;">
                    <div id="contentPOSublet"/>
                    %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                    <div class="iu-content"></div>

                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    <g:field type="button" onclick="aprvPOSublet();" class="btn btn-primary create" name="approval" id="approval" value="${message(code: 'default.button.editJobRCP.label', default: 'Send To Approval')}" />
                    <button id='closeModal' type="button" class="btn btn-primary">Close</button>
                </div>
            </div>
        </div>
    </div>

    </body>
</html>
