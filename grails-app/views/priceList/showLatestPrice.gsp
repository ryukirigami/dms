<%--
  Created by IntelliJ IDEA.
  User: pwidodo
  Date: 1/18/14
  Time: 1:01 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="com.kombos.parts.Konversi; com.kombos.administrasi.CompanyDealer; java.text.SimpleDateFormat; com.kombos.administrasi.BaseModel; com.kombos.administrasi.PriceList" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
        <g:set var="entityName" value="${message(code: 'priceList.label', default: 'PriceList')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
        <r:require modules="baseapplayout, core, baseapptheme" />
        <title>Price List</title>
    </head>
    <body>
    <div class="container-fluid" style="margin-left: -100px; margin-top: -30px;">
        <div id="main-content" class="row-fluid">
            <div class="box-header no-border">
                <span class="pull-left">Price List Service Kendaraan Bermotor</span>
            </div>
            </br>
            <div class="box">
                <table class="display table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; width: 1355px; text-align: center;">
                <%
                    def priceLists
                    def conversi = new Konversi()
                    int no = 1
                    boolean staHead = true
                    PriceList priceList
                    def baseModels = BaseModel.findAllByStaDel('0')
                    BaseModel baseModel
                    Date tmt
                    CompanyDealer companyDealer = CompanyDealer.findById(new Long(params.companyDealer.id))
                    baseModels.each {
                        baseModel = it
                        priceLists = PriceList.findAllByBaseModelAndCompanyDealer(baseModel,companyDealer)
                        if(priceLists){
                            if(staHead){
                                out.println("<tr>")
                                out.println("<th></th>")
                                out.println("<th></th>")
                                priceLists.each {
                                    priceList = it
                                    out.println("<th colspan='2' align='center'>${priceList.mappingJobPanel.operation.m053NamaOperation}</th>")
                                }
                                out.println("</tr>")

                                out.println("<tr>")
                                out.println("<th>No</th>")
                                out.println("<th>Model</th>")
                                priceLists.each {
                                    priceList = it
                                    out.println("<th>Jasa</th>")
                                    out.println("<th>Parts</th>")
                                }
                                out.println("</tr>")
                            }

                            tmt = priceList.m095TMT
                            out.println("<tr>")
                            out.println("<td>${no}</td>")
                            out.println("<td>${baseModel.m102NamaBaseModel}</td>")
                            priceLists.each {
                                priceList = it
                                out.println("<td>${conversi.toRupiah(priceList.m095Jasa)}</td>")
                                out.println("<td>${conversi.toRupiah(priceList.m095Parts)}</td>")
                            }
                            out.println("</tr>")
                            no++
                            staHead = false
                        }
                    }
                %>
                </table>
                <div>
                    <%if(!staHead){%>
                    Berlaku Sejak <% out.println(new SimpleDateFormat("dd MMMMM yyyy").format(tmt)) %>
                    </br>
                    Harga Sudah Termasuk PPN
                    <%}else{%>
                    Please Update Price List Service
                    <%}%>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>