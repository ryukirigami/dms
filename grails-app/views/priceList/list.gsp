
<%@ page import="com.kombos.administrasi.PriceList" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'priceList.label', default: 'PriceList')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var update;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();" />
        break;
    case '_DELETE_' :
        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    update = function() {
    	$('#spinner').fadeIn(1);
    	$.ajax({
    	    type:'POST',
    	    url:'${request.contextPath}/priceList/update',
    	    success:function(data,textStatus){
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   				reloadPriceListTable();
   			}
    	});
    };

    loadForm = function(data, textStatus){
		$('#priceList-form').empty();
    	$('#priceList-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#priceList-table").hasClass("span12")){
   			$("#priceList-table").toggleClass("span12 span5");
        }
        $("#priceList-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#priceList-table").hasClass("span5")){
   			$("#priceList-table").toggleClass("span5 span12");
   		}
        $("#priceList-form").css("display","none");
   	}

});
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
</div>
<div class="box">
    <div class="span12" id="priceList-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <g:render template="dataTables" />

    </br>
        <input type="submit" class="btn btn-primary save" value="Update Price List" onclick="javascript:update();">
    </br>
    </br>
        <div>
            For user/customer view please refer to this page <a href="${request.contextPath}/priceList/showLatestPrice?companyDealer.id=${session.userCompanyDealerId}"><%out.println("${request.contextPath}/priceList/showLatestPrice?companyDealer.id=${session.userCompanyDealerId}")%></a>
        </div>
    </div>
    <div class="span7" id="priceList-form" style="display: none;"></div>
</div>
</body>
</html>
