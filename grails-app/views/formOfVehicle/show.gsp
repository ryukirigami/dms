

<%@ page import="com.kombos.administrasi.FormOfVehicle" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'formOfVehicle.label', default: 'Form Of Vehicle')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteFormOfVehicle;

$(function(){ 
	deleteFormOfVehicle=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/formOfVehicle/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadFormOfVehicleTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-formOfVehicle" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="formOfVehicle"
			class="table table-bordered table-hover">
			<tbody>

				 
			
				<g:if test="${formOfVehicleInstance?.m114KodeFoV}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m114KodeFoV-label" class="property-label"><g:message
					code="formOfVehicle.m114KodeFoV.label" default="Kode Form of Vehicle" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m114KodeFoV-label">
						%{--<ba:editableValue
								bean="${formOfVehicleInstance}" field="m114KodeFoV"
								url="${request.contextPath}/FormOfVehicle/updatefield" type="text"
								title="Enter m114KodeFoV" onsuccess="reloadFormOfVehicleTable();" />--}%
							
								<g:fieldValue bean="${formOfVehicleInstance}" field="m114KodeFoV"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${formOfVehicleInstance?.m114NamaFoV}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m114NamaFoV-label" class="property-label"><g:message
					code="formOfVehicle.m114NamaFoV.label" default="Nama Form of Vehicle" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m114NamaFoV-label">
						%{--<ba:editableValue
								bean="${formOfVehicleInstance}" field="m114NamaFoV"
								url="${request.contextPath}/FormOfVehicle/updatefield" type="text"
								title="Enter m114NamaFoV" onsuccess="reloadFormOfVehicleTable();" />--}%
							
								<g:fieldValue bean="${formOfVehicleInstance}" field="m114NamaFoV"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${formOfVehicleInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${formOfVehicleInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${formOfVehicleInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
				 	<g:if test="${formOfVehicleInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${formOfVehicleInstance}" field="dateCreated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${formOfVehicleInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${formOfVehicleInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="jenisStall.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${formOfVehicleInstance}" field="createdBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${formOfVehicleInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${formOfVehicleInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jenisStall.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${formOfVehicleInstance}" field="lastUpdated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${formOfVehicleInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${formOfVehicleInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="jenisStall.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${formOfVehicleInstance}" field="updatedBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${formOfVehicleInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${formOfVehicleInstance?.id}"
					update="[success:'formOfVehicle-form',failure:'formOfVehicle-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteFormOfVehicle('${formOfVehicleInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
