
<%@ page import="com.kombos.administrasi.FormOfVehicle" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="formOfVehicle_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

		 
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="formOfVehicle.m114KodeFoV.label" default="Kode Form of Vehicle" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="formOfVehicle.m114NamaFoV.label" default="Nama Form of Vehicle" /></div>
			</th>

 
		</tr>
		<tr>
		
	 
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m114KodeFoV" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m114KodeFoV" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m114NamaFoV" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m114NamaFoV" class="search_init" />
				</div>
			</th>
	
		 

		</tr>
	</thead>
</table>

<g:javascript>
var FormOfVehicleTable;
var reloadFormOfVehicleTable;
$(function(){
	
	reloadFormOfVehicleTable = function() {
		FormOfVehicleTable.fnDraw();
	}

    var recordsFormOfVehicleperpage = [];//new Array();
    var anFormOfVehicleSelected;
    var jmlRecFormOfVehiclePerPage=0;
    var id;
	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	FormOfVehicleTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	FormOfVehicleTable = $('#formOfVehicle_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsFormOfVehicle = $("#formOfVehicle_datatables tbody .row-select");
            var jmlFormOfVehicleCek = 0;
            var nRow;
            var idRec;
            rsFormOfVehicle.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsFormOfVehicleperpage[idRec]=="1"){
                    jmlFormOfVehicleCek = jmlFormOfVehicleCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsFormOfVehicleperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecFormOfVehiclePerPage = rsFormOfVehicle.length;
            if(jmlFormOfVehicleCek==jmlRecFormOfVehiclePerPage && jmlRecFormOfVehiclePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

 
{
	"sName": "m114KodeFoV",
	"mDataProp": "m114KodeFoV",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m114NamaFoV",
	"mDataProp": "m114NamaFoV",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
 
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m114ID = $('#filter_m114ID input').val();
						if(m114ID){
							aoData.push(
									{"name": 'sCriteria_m114ID', "value": m114ID}
							);
						}
	
						var m114KodeFoV = $('#filter_m114KodeFoV input').val();
						if(m114KodeFoV){
							aoData.push(
									{"name": 'sCriteria_m114KodeFoV', "value": m114KodeFoV}
							);
						}
	
						var m114NamaFoV = $('#filter_m114NamaFoV input').val();
						if(m114NamaFoV){
							aoData.push(
									{"name": 'sCriteria_m114NamaFoV', "value": m114NamaFoV}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	 $('.select-all').click(function(e) {

        $("#formOfVehicle_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsFormOfVehicleperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsFormOfVehicleperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#formOfVehicle_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsFormOfVehicleperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anFormOfVehicleSelected = FormOfVehicleTable.$('tr.row_selected');
            if(jmlRecFormOfVehiclePerPage == anFormOfVehicleSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsFormOfVehicleperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
