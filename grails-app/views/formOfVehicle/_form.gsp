<%@ page import="com.kombos.administrasi.FormOfVehicle" %>


 

<div class="control-group fieldcontain ${hasErrors(bean: formOfVehicleInstance, field: 'm114KodeFoV', 'error')} required">
	<label class="control-label" for="m114KodeFoV">
		<g:message code="formOfVehicle.m114KodeFoV.label" default="Kode Form of Vehicle" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m114KodeFoV" maxlength="2" required="" value="${formOfVehicleInstance?.m114KodeFoV}"/>
	</div>
</div>
<g:javascript>
    document.getElementById("m114KodeFoV").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: formOfVehicleInstance, field: 'm114NamaFoV', 'error')} required">
	<label class="control-label" for="m114NamaFoV">
		<g:message code="formOfVehicle.m114NamaFoV.label" default="Nama Form of Vehicle" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m114NamaFoV" maxlength="20" required="" value="${formOfVehicleInstance?.m114NamaFoV}"/>
	</div>
</div> 
