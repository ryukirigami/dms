<%@ page import="com.kombos.administrasi.BaseModel; com.kombos.customerprofile.Warna; org.apache.commons.codec.binary.Base64; com.kombos.customerprofile.Hobby; com.kombos.customerprofile.HistoryCustomer" %>


<script>
    $(function () {
        $("#jenisID").hide();
        $("#saveJenisID").hide();
        $("#cancelJenisID").hide();

        $("#agamaAdd").hide();
        $("#saveAgama").hide();
        $("#cancelAgama").hide();

        $("#nikahAdd").hide();
        $("#saveNikah").hide();
        $("#cancelNikah").hide();

        $("#hobbyAdd").hide();
        $("#saveHobby").hide();
        $("#cancelHobby").hide();

        $("#profile_tabs").tabs();
        $("#alamat_tabs").tabs();
        $("#account_web").tabs();
        $("#foto_profile_tabs").tabs();

        $("#kontak_profile_tabs").tabs();

        $("#identitas_profile_tabs").tabs();
        $("#lain_profile_tabs").tabs();

        $("#vehicle_tabs").tabs();

        $("#noHp_valid").tabs();

        changeJenisCustomer = function(){
            var jenisCustomer = $('input[name=jenisCustomer]:checked').val();
            if(jenisCustomer=='Corporate'){
                $('#company').prop("disabled", false);
            }else{
                $('#company').val('');
                $('#company').prop("disabled", true);
            }
        };


        $('#t182WebPassword1').change(function(){
            if ($(this).val() != $('#t182WebPassword').val()) {
                alert('Password konfirmasi tidak sama !');
                $(this).val("");
                $(this).focus();
            }
        });

        checkPassword = function(){
            var pass = $('#t182WebPassword').val();
            var konf = $('#t182WebPassword1').val();
            if(pass!=konf){
                alert('Password konfirmasi tidak sama !');
                return false;
            }
        };

        isNegative = function(n){
            if(n.value < 0){
                n.focus();
                n.value = '';
            }
        }

        $('#provinsi').typeahead({
            source: function (query, process) {
                provinsiList = [];
                map = {};
                return $.get('${request.contextPath}/company/getProvinsiList', { query: query }, function (data) {
                    $.each(data.options, function (i, provinsi) {
                        map[provinsi.nama] = provinsi;
                        provinsiList.push(provinsi.nama);
                    });
                    return process(provinsiList);
                });
            } ,
            updater: function (item) {
                $("#provinsi_id").val(map[item].id);
                $('#kabKota').val('');
                $("#kabKota_id").val('');
                $('#kecamatan').val('');
                $('#kecamatan_id').val('');
                $('#kelurahan').val('');
                $('#kelurahan_id').val('');
                return item;
            }

        });

        $('#kabKota').typeahead({
            source: function (query, process) {
                kabKotaList = [];
                map = {};
                return $.get('${request.contextPath}/company/getKabKotaList', { query: query, provinsi: $("#provinsi_id").val() }, function (data) {
                    $.each(data.options, function (i, kabKota) {
                        map[kabKota.nama] = kabKota;
                        kabKotaList.push(kabKota.nama);
                    });
                    return process(kabKotaList);
                });
            } ,
            updater: function (item) {
                $("#kabKota_id").val(map[item].id);
                $('#kecamatan').val('');
                $('#kecamatan_id').val('');
                $('#kelurahan').val('');
                $('#kelurahan_id').val('');
                return item;
            }

        });

        $('#kecamatan').typeahead({
            source: function (query, process) {
                kecamatanList = [];
                map = {};
                return $.get('${request.contextPath}/company/getKecamatanList', { query: query, kabKota: $("#kabKota_id").val() }, function (data) {
                    $.each(data.options, function (i, kecamatan) {
                        map[kecamatan.nama] = kecamatan;
                        kecamatanList.push(kecamatan.nama);
                    });
                    return process(kecamatanList);
                });
            } ,
            updater: function (item) {
                $("#kecamatan_id").val(map[item].id);
                $('#kelurahan').val('');
                $('#kelurahan_id').val('');
                return item;
            }

        });

        $('#kelurahan').typeahead({
            source: function (query, process) {
                kelurahanList = [];
                map = {};
                return $.get('${request.contextPath}/company/getKelurahanList', { query: query, kecamatan: $("#kecamatan_id").val() }, function (data) {
                    $.each(data.options, function (i, kelurahan) {
                        map[kelurahan.nama] = kelurahan;
                        kelurahanList.push(kelurahan.nama);
                    });
                    return process(kelurahanList);
                });
            } ,
            updater: function (item) {
                $("#kelurahan_id").val(map[item].id);
                return item;
            }

        });

        $('#provinsi2').typeahead({
            source: function (query, process) {
                provinsiList = [];
                map = {};
                return $.get('${request.contextPath}/company/getProvinsiList', { query: query }, function (data) {
                    $.each(data.options, function (i, provinsi) {
                        map[provinsi.nama] = provinsi;
                        provinsiList.push(provinsi.nama);
                    });
                    return process(provinsiList);
                });
            } ,
            updater: function (item) {
                $("#provinsi2_id").val(map[item].id);
                $('#kabKota2').val('');
                $("#kabKota2_id").val('');
                $('#kecamatan2').val('');
                $('#kecamatan2_id').val('');
                $('#kelurahan2').val('');
                $('#kelurahan2_id').val('');
                return item;
            }

        });

        $('#kabKota2').typeahead({
            source: function (query, process) {
                kabKotaList = [];
                map = {};
                return $.get('${request.contextPath}/company/getKabKotaList', { query: query, provinsi: $("#provinsi2_id").val() }, function (data) {
                    $.each(data.options, function (i, kabKota) {
                        map[kabKota.nama] = kabKota;
                        kabKotaList.push(kabKota.nama);
                    });
                    return process(kabKotaList);
                });
            } ,
            updater: function (item) {
                $("#kabKota2_id").val(map[item].id);
                $('#kecamatan2').val('');
                $('#kecamatan2_id').val('');
                $('#kelurahan2').val('');
                $('#kelurahan2_id').val('');
                return item;
            }

        });

        $('#kecamatan2').typeahead({
            source: function (query, process) {
                kecamatanList = [];
                map = {};
                return $.get('${request.contextPath}/company/getKecamatanList', { query: query, kabKota: $("#kabKota2_id").val() }, function (data) {
                    $.each(data.options, function (i, kecamatan) {
                        map[kecamatan.nama] = kecamatan;
                        kecamatanList.push(kecamatan.nama);
                    });
                    return process(kecamatanList);
                });
            } ,
            updater: function (item) {
                $("#kecamatan2_id").val(map[item].id);
                $('#kelurahan2').val('');
                $('#kelurahan2_id').val('');
                return item;
            }

        });

        $('#kelurahan2').typeahead({
            source: function (query, process) {
                kelurahanList = [];
                map = {};
                return $.get('${request.contextPath}/company/getKelurahanList', { query: query, kecamatan: $("#kecamatan2_id").val() }, function (data) {
                    $.each(data.options, function (i, kelurahan) {
                        map[kelurahan.nama] = kelurahan;
                        kelurahanList.push(kelurahan.nama);
                    });
                    return process(kelurahanList);
                });
            } ,
            updater: function (item) {
                $("#kelurahan2_id").val(map[item].id);
                return item;
            }

        });

        $("input[name='kategoriCust']").change(radioValueChanged);
//        console.log("$(input[name='kategoriCust']).val()=" + $("input[name='kategoriCust']").val());
//        if ($("input[name='kategoriCust']").val() == null) {
//            $("input[name='kategoriCust']").val("2");
//            $('#detailAsr').hide();
//        }
        $('#detailAsr').hide();
        function radioValueChanged()
        {
            console.log("$(this).val()=" + $(this).val());
            if($(this).is(":checked") && $(this).val() == "0")
            {
                $('#detailAsr').show();
            }
            else
            {
                $('#detailAsr').hide();
            }
        }


        createJenisIDCard = function(){
            $("#jenisIdCard").hide();
            $("#createJenisID").hide();
            $("#jenisID").show();
            $("#saveJenisID").show();
            $("#cancelJenisID").show();
        };

        saveJenisIDCard = function(){
            var isiJenisID = $('#jenisID').val();
            if(isiJenisID == ""){
                alert("Jenis ID Card Tidak boleh kosong");
                return;
            }

            $.ajax({type:'POST', url:'${request.contextPath}/historyCustomer/insertJenisID',
                data : {jenisID : isiJenisID},
                success:function(data,textStatus){
                    if(data){

                        if(data.error === "duplicate"){
                            alert("Data Duplicate, Silakan input yang belum ada");
                            $('#jenisID').val("");
                            $("#jenisID").focus();

                        }else{
                            $('#jenisID').val("");
                            $('#jenisIdCard').append("<option value='" + data.id + "'>" + data.jenisID + "</option>");
                            toastr.success("Jenis ID Card Berhasil disimpan");
                            cancelJenisIDCard();

                        }

                    }

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        };

        cancelJenisIDCard = function(){
            $("#jenisID").hide();
            $("#saveJenisID").hide();
            $("#cancelJenisID").hide();
            $("#jenisIdCard").show();
            $("#createJenisID").show();
        };

        createAgamaAdd = function(){
            $("#agama").hide();
            $("#createAgama").hide();
            $("#agamaAdd").show();
            $("#saveAgama").show();
            $("#cancelAgama").show();
        };

        saveAgamaAdd = function(){
            var agamaAdd = $('#agamaAdd').val();
            if(agamaAdd == ""){
                alert("Agama Tidak boleh kosong");
                return;
            }

            $.ajax({type:'POST', url:'${request.contextPath}/historyCustomer/insertAgama',
                data : {agamaAdd : agamaAdd},
                success:function(data,textStatus){
                    if(data){

                        if(data.error === "duplicate"){
                            alert("Data Duplicate, Silakan input yang belum ada");
                            $('#agamaAdd').val("");
                            $("#agamaAdd").focus();

                        }else{
                            $('#agamaAdd').val("");
                            $('#agama').append("<option value='" + data.id + "'>" + data.namaAgama + "</option>");
                            toastr.success("Agama Berhasil disimpan");
                            cancelAgamaAdd();

                        }

                    }

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        };

        cancelAgamaAdd = function(){
            $("#agamaAdd").hide();
            $("#saveAgama").hide();
            $("#cancelAgama").hide();
            $("#agama").show();
            $("#createAgama").show();
        };

        createNikahAdd = function(){
            $("#nikah").hide();
            $("#createNikah").hide();
            $("#nikahAdd").show();
            $("#saveNikah").show();
            $("#cancelNikah").show();
        };

        saveNikahAdd = function(){
            var nikahAdd = $('#nikahAdd').val();
            if(nikahAdd == ""){
                alert("Status Nikah Tidak boleh kosong");
                return;
            }

            $.ajax({type:'POST', url:'${request.contextPath}/historyCustomer/insertNikah',
                data : {nikahAdd : nikahAdd},
                success:function(data,textStatus){
                    if(data){

                        if(data.error === "duplicate"){
                            alert("Data Duplicate, Silakan input yang belum ada");
                            $('#nikahAdd').val("");
                            $("#nikahAdd").focus();

                        }else{
                            $('#nikahAdd').val("");
                            $('#nikah').append("<option value='" + data.id + "'>" + data.statusNikah + "</option>");
                            toastr.success("Status Nikah Berhasil disimpan");
                            cancelNikahAdd();

                        }

                    }

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        };

        cancelNikahAdd = function(){
            $("#nikahAdd").hide();
            $("#saveNikah").hide();
            $("#cancelNikah").hide();
            $("#nikah").show();
            $("#createNikah").show();
        };

        createHobbyAdd = function(){
            $("#hobbies").hide();
            $("#createHobby").hide();
            $("#hobbyAdd").show();
            $("#saveHobby").show();
            $("#cancelHobby").show();
        };

        saveHobbyAdd = function(){
            var hobbyAdd = $('#hobbyAdd').val();
            if(hobbyAdd == ""){
                alert("Hobby Tidak boleh kosong");
                return;
            }

            $.ajax({type:'POST', url:'${request.contextPath}/historyCustomer/insertHobby',
                data : {hobbyAdd : hobbyAdd},
                success:function(data,textStatus){
                    if(data){

                        if(data.error === "duplicate"){
                            alert("Data Duplicate, Silakan input yang belum ada");
                            $('#hobbyAdd').val("");
                            $("#hobbyAdd").focus();

                        }else{
                            $('#hobbyAdd').val("");
                            $('#hobbies').append("<option value='" + data.id + "'>" + data.hobby + "</option>");
                            toastr.success("Hobby Berhasil disimpan");
                            cancelHobbyAdd();

                        }

                    }

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        };

        cancelHobbyAdd = function(){
            $("#hobbyAdd").hide();
            $("#saveHobby").hide();
            $("#cancelHobby").hide();
            $("#hobbies").show();
            $("#createHobby").show();
        };

    });
</script>

<div class="navbar box-header no-border">
    <span class="pull-left"><legend style="font-size: large">Appointment Customer</legend></span>
</div>
<table style="width: 100%;padding-left: 5px;padding-right: 5px;">
<tr style="vertical-align: top;">
<td style="width: 33%;padding:5px;">
<div id="profile_tabs">
    <legend style="font-size: small">Profile</legend>
    <div id="profile_tabs-1">

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182ID', 'error')} ">
            <label class="control-label" for="t182ID">
                <g:message code="historyCustomer.t182ID.label" default="Kode Customer"/>
            </label>
            <div class="controls">
                <g:textField name="t182ID" maxlength="50" readonly="true" value="${historyCustomerInstance?.t182ID}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182GelarD', 'error')} ">
            <label class="control-label" for="t182GelarD">
                <g:message code="historyCustomer.t182GelarD.label" default="Gelar di depan"/>
            </label>
            <div class="controls">
                <g:textField name="t182GelarD" maxlength="50" value="${historyCustomerInstance?.t182GelarD}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NamaDepan', 'error')} ">
            <label class="control-label" for="t182NamaDepan">
                <g:message code="historyCustomer.t182NamaDepan.label" default="Nama Depan"/>
                <span class="required-indicator">*</span>
            </label>
            <div class="controls">
                <g:textField name="t182NamaDepan" maxlength="50" required="" value="${historyCustomerInstance?.t182NamaDepan}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NamaBelakang', 'error')} ">
            <label class="control-label" for="t182NamaBelakang">
                <g:message code="historyCustomer.t182NamaBelakang.label" default="Nama Belakang"/>
            </label>
            <div class="controls">
                <g:textField name="t182NamaBelakang" maxlength="50" value="${historyCustomerInstance?.t182NamaBelakang}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182GelarB', 'error')} ">
            <label class="control-label" for="t182GelarB">
                <g:message code="historyCustomer.t182GelarB.label" default="Gelar di belakang"/>
            </label>
            <div class="controls">
                <g:textField name="t182GelarB" maxlength="50" value="${historyCustomerInstance?.t182GelarB}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182JenisKelamin', 'error')} ">
            <label class="control-label" for="t182JenisKelamin">
                <g:message code="historyCustomer.t182JenisKelamin.label" default="Jenis Kelamin"/>
                <span class="required-indicator">*</span>
            </label>
            <div class="controls">
                <g:radioGroup name="t182JenisKelamin" values="['1','2']" value="${historyCustomerInstance?.t182JenisKelamin}" labels="['Laki-laki','Perempuan']" required="">
                    ${it.radio} <g:message code="${it.label}" />
                </g:radioGroup>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182TglLahir', 'error')} ">
            <label class="control-label" for="t182TglLahir">
                <g:message code="historyCustomer.t182TglLahir.label" default="Tgl. Lahir"/>
            </label>
            <div class="controls">
                <ba:datePicker name="t182TglLahir" precision="day" value="${historyCustomerInstance?.t182TglLahir}"
                               format="dd/mm/yyyy"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'jenisCustomer', 'error')} ">
            <label class="control-label" for="jenisCustomer">
                <g:message code="historyCustomer.jenisCustomer.label" default="Jenis Customer"/>
            </label>
            <div class="controls">
                <g:radioGroup values="${historyCustomerInstance.constraints.jenisCustomer.inList}" name="jenisCustomer"
                              value="${historyCustomerInstance?.jenisCustomer}" labels="['Personal','Corporate']" onchange="changeJenisCustomer()">
                    ${it.radio} <g:message code="${it.label}" />
                </g:radioGroup>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'company', 'error')} ">
            <label class="control-label" for="company">
                <g:message code="historyCustomer.company.label" default="Company"/>
            </label>
            <div class="controls">
                <g:select id="company" name="company.id" from="${com.kombos.maintable.Company.findAll('from Company c where c.staDel=0 order by c.namaPerusahaan')}"
                          optionKey="id" noSelection="['':'Silahkan Pilih']" disabled="true"
                          value="${historyCustomerInstance?.company?.id}" class="many-to-one"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'peranCustomer', 'error')} ">
            <label class="control-label" for="peranCustomer">
                <g:message code="historyCustomer.peranCustomer.label" default="Peran Customer"/>
            </label>
            <div class="controls">
                <g:select id="peranCustomer" name="peranCustomer.id" noSelection="['':'Silahkan Pilih']"
                          from="${com.kombos.maintable.PeranCustomer.findAll('from PeranCustomer p where p.staDel=0 order by p.m115NamaPeranCustomer')}"
                          optionKey="id" value="${historyCustomerInstance?.peranCustomer?.id}"
                          class="many-to-one"/>
            </div>
        </div>
    </div>
</div>

<br>

</td>
<td style="width: 33%;padding:5px;">

<div id="foto_profile_tabs">
    <legend style="font-size: small">Foto</legend>

    <div id="foto_profile_tabs-1">
        <script language="Javascript">
            function fileUpload(form, action_url, div_id) {
                // Create the iframe...
                var iframe = document.createElement("iframe");
                iframe.setAttribute("id", "upload_iframe");
                iframe.setAttribute("name", "upload_iframe");
                iframe.setAttribute("width", "0");
                iframe.setAttribute("height", "0");
                iframe.setAttribute("border", "0");
                iframe.setAttribute("style", "width: 0; height: 0; border: none;");

                // Add to document...
                form.parentNode.appendChild(iframe);
                window.frames['upload_iframe'].name = "upload_iframe";

                iframeId = document.getElementById("upload_iframe");

                // Add event...
                var eventHandler = function () {

                    if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
                    else iframeId.removeEventListener("load", eventHandler, false);

                    // Message from server...
                    if (iframeId.contentDocument) {
                        content = iframeId.contentDocument.body.innerHTML;
                    } else if (iframeId.contentWindow) {
                        content = iframeId.contentWindow.document.body.innerHTML;
                    } else if (iframeId.document) {
                        content = iframeId.document.body.innerHTML;
                    }

                    document.getElementById(div_id).innerHTML = content;

                    // Del the iframe...
                    setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
                }

                if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
                if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);

                // Set properties of form...
                form.setAttribute("target", "upload_iframe");
                form.setAttribute("action", action_url);
                form.setAttribute("method", "post");
                form.setAttribute("enctype", "multipart/form-data");
                form.setAttribute("encoding", "multipart/form-data");

                // Submit the form...
                form.submit();

                document.getElementById(div_id).innerHTML = "Uploading...";
            }
        </script>

        <g:form>
            <input type="file" name="myFile" accept="image/*"
                   onchange="fileUpload(this.form, '${g.createLink(controller: 'historyCustomer', action: 'uploadImage')}', 'upload');
                   return false;"/><br/><br/>
            <div id="upload">
                <g:if test="${historyCustomerInstance?.t182Foto}">
                    <img width="200px"
                         src="data:jpg;base64,${new String(new Base64().encode(historyCustomerInstance.t182Foto), "UTF-8")}"/>
                </g:if>
            </div>
        </g:form>

    </div>
</div>


<br>

<div id="kontak_profile_tabs">
    <legend style="font-size: small">Kontak</legend>

    <div id="kontak_profile_tabs-1">

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoTelpRumah', 'error')} ">
            <label class="control-label" for="t182NoTelpRumah">
                <g:message code="historyCustomer.t182NoTelpRumah.label" default="T182 No Telp Rumah"/>
                <span class="required-indicator">*</span>
            </label>
            <div class="controls">
                <g:textField onkeyup="checkNumber(this);" onblur="isNegative(this);" name="t182NoTelpRumah" maxlength="50" required=""
                             value="${historyCustomerInstance?.t182NoTelpRumah}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoTelpKantor', 'error')} ">
            <label class="control-label" for="t182NoTelpKantor">
                <g:message code="historyCustomer.t182NoTelpKantor.label" default="T182 No Telp Kantor"/>
            </label>
            <div class="controls">
                <g:textField onkeyup="checkNumber(this);" onblur="isNegative(this);" name="t182NoTelpKantor" maxlength="50"
                             value="${historyCustomerInstance?.t182NoTelpKantor}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoFax', 'error')} ">
            <label class="control-label" for="t182NoFax">
                <g:message code="historyCustomer.t182NoFax.label" default="T182 No Fax"/>
            </label>
            <div class="controls">
                <g:textField onkeyup="checkNumber(this);" onblur="isNegative(this);" name="t182NoFax" maxlength="50" value="${historyCustomerInstance?.t182NoFax}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoHp', 'error')} ">
            <label class="control-label" for="t182NoHp">
                <g:message code="historyCustomer.t182NoHp.label" default="T182 No Hp"/>
                <span class="required-indicator">*</span>
            </label>
            <div class="controls">
                <g:textField onkeyup="checkNumber(this);" onblur="isNegative(this);" name="t182NoHp" required="" maxlength="20" value="${historyCustomerInstance?.t182NoHp}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182Email', 'error')} ">
            <label class="control-label" for="t182Email">
                <g:message code="historyCustomer.t182Email.label" default="T182 Email"/>
            </label>
            <div class="controls">
                <g:field type="email" name="t182Email" maxlength="50" value="${historyCustomerInstance?.t182Email}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182StaTerimaMRS', 'error')} ">
            <label class="control-label" for="t182StaTerimaMRS">
                <g:message code="historyCustomer.t182StaTerimaMRS.label" default="T182 Sta Terima MRS"/>
            </label>
            <div class="controls">
                <g:radioGroup name="t182StaTerimaMRS" values="[1,0]" value="${historyCustomerInstance?.t182StaTerimaMRS}" labels="['Ya','Tidak']" required="">
                    ${it.radio} <g:message code="${it.label}" />
                </g:radioGroup>
            </div>
        </div>
    </div>
</div>

</td>
<td style="width: 33%;padding:5px;">

<div id="identitas_profile_tabs">
    <legend style="font-size: small">Identitas</legend>
    <div id="identitas_profile_tabs-1">
        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'jenisIdCard', 'error')} ">
            <label class="control-label" for="jenisIdCard">
                <g:message code="historyCustomer.jenisIdCard.label" default="Jenis Id Card"/>
            </label>
            <div class="controls">
                <g:select id="jenisIdCard" name="jenisIdCard.id" from="${com.kombos.customerprofile.JenisIdCard.findAll('from JenisIdCard j where j.staDel = 0 order by j.m060JenisIDCard')}"
                          optionKey="id" value="${historyCustomerInstance?.jenisIdCard?.id}" noSelection="['':'Silahkan Pilih']"
                          class="many-to-one" style="width: 175px;"/>
                <g:textField name="jenisID" id="jenisID"/>
                <g:field type="button" onclick="createJenisIDCard();" class="btn btn-primary create" name="createJenisID" id="createJenisID" value="${message(code: 'default.button.add.label', default: '+')}"/>
                <g:field type="button" onclick="saveJenisIDCard();" class="btn btn-primary create" name="saveJenisID" id="saveJenisID" value="${message(code: 'default.button.save.label', default: 'Simpan')}"/>
                <g:field type="button" onclick="cancelJenisIDCard();" class="btn btn-primary create" name="cancelJenisID" id="cancelJenisID" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NomorIDCard', 'error')} ">
            <label class="control-label" for="t182NomorIDCard">
                <g:message code="historyCustomer.t182NomorIDCard.label" default="T182 Nomor IDC ard"/>
            </label>
            <div class="controls">
                <g:textField name="t182NomorIDCard" maxlength="20"
                             value="${historyCustomerInstance?.t182NomorIDCard}"/>
            </div>
        </div>


        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NPWP', 'error')} ">
            <label class="control-label" for="t182NPWP">
                <g:message code="historyCustomer.t182NPWP.label" default="T182 NPWP"/>
            </label>
            <div class="controls">
                <g:textField name="t182NPWP" maxlength="20" value="${historyCustomerInstance?.t182NPWP}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoFakturPajakStd', 'error')} ">
            <label class="control-label" for="t182NoFakturPajakStd">
                <g:message code="historyCustomer.t182NoFakturPajakStd.label" default="T182 No Faktur Pajak Std"/>
            </label>
            <div class="controls">
                <g:textField name="t182NoFakturPajakStd" maxlength="20"
                             value="${historyCustomerInstance?.t182NoFakturPajakStd}"/>
            </div>
        </div>
    </div>
</div>


<br>

<div id="lain_profile_tabs">
    <legend style="font-size: small">Lain-lain</legend>
    <div id="lain_profile_tabs-1">
        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'agama', 'error')} ">
            <label class="control-label" for="agama">
                <g:message code="historyCustomer.agama.label" default="Agama"/>
            </label>
            <div class="controls">
                <g:select id="agama" name="agama.id" from="${com.kombos.customerprofile.Agama.findAll('from Agama a where a.staDel=0 order by a.m061NamaAgama')}" optionKey="id"
                          value="${historyCustomerInstance?.agama?.id}" class="many-to-one" noSelection="['':'Silahkan Pilih']" style="width: 175px;"/>
                <g:textField name="agamaAdd" id="agamaAdd"/>
                <g:field type="button" onclick="createAgamaAdd();" class="btn btn-primary create" name="createAgama" id="createAgama" value="${message(code: 'default.button.add.label', default: '+')}"/>
                <g:field type="button" onclick="saveAgamaAdd();" class="btn btn-primary create" name="saveAgama" id="saveAgama" value="${message(code: 'default.button.save.label', default: 'Simpan')}"/>
                <g:field type="button" onclick="cancelAgamaAdd();" class="btn btn-primary create" name="cancelAgama" id="cancelAgama" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'nikah', 'error')} ">
            <label class="control-label" for="nikah">
                <g:message code="historyCustomer.nikah.label" default="Nikah"/>
            </label>
            <div class="controls">
                <g:select id="nikah" name="nikah.id" from="${com.kombos.maintable.Nikah.findAll('from Nikah n where n.staDel = 0 order by n.m062StaNikah')}" optionKey="id"
                          value="${historyCustomerInstance?.nikah?.id}" class="many-to-one" noSelection="['':'Silahkan Pilih']" style="width: 175px;"/>
                <g:textField name="nikahAdd" id="nikahAdd"/>
                <g:field type="button" onclick="createNikahAdd();" class="btn btn-primary create" name="createNikah" id="createNikah" value="${message(code: 'default.button.add.label', default: '+')}"/>
                <g:field type="button" onclick="saveNikahAdd();" class="btn btn-primary create" name="saveNikah" id="saveNikah" value="${message(code: 'default.button.save.label', default: 'Simpan')}"/>
                <g:field type="button" onclick="cancelNikahAdd();" class="btn btn-primary create" name="cancelNikah" id="cancelNikah" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}"/>
            </div>
        </div>

        <div class="control-group ${hasErrors(bean: historyCustomerInstance, field: 'hobbies', 'error')} ">
            <label class="control-label" for="hobbies">
                <g:message code="historyCustomer.hobbies.label" default="Hobbies"/>
            </label>
            <div class="controls">
                <g:select name="hobbies" from="${Hobby.findAll('from Hobby h where h.staDel = 0 order by h.m063NamaHobby')}" multiple="multiple" optionKey="id" size="6"
                          value="${historyCustomerInstance?.hobbies*.id}" class="many-to-many" style="width: 175px;"/>
                <g:textField name="hobbyAdd" id="hobbyAdd"/>
                <g:field type="button" onclick="createHobbyAdd();" class="btn btn-primary create" name="createHobby" id="createHobby" value="${message(code: 'default.button.add.label', default: '+')}" style="vertical-align: top"/>
                <g:field type="button" onclick="saveHobbyAdd();" class="btn btn-primary create" name="saveHobby" id="saveHobby" value="${message(code: 'default.button.save.label', default: 'Simpan')}"/>
                <g:field type="button" onclick="cancelHobbyAdd();" class="btn btn-primary create" name="cancelHobby" id="cancelHobby" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}"/>
            </div>
        </div>
    </div>
</div>

</td>
</tr>
<tr>
    <td>

    <div id="alamat_tabs" class="tabbable control-group" style="width: 98%;">
        <ul class="nav nav-tabs">
            <li id="tab_ac_alamat_korespondensi" class="active"><a href="#ac_alamat_korespondensi" data-toggle="tab">Alamat Korespondensi</a></li>
            <li id="tab_ac_alamat_npwp" class=""><a href="#ac_alamat_npwp" data-toggle="tab">Alamat NPWP</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="ac_alamat_korespondensi">
                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182Alamat', 'error')} ">
                    <label class="control-label" for="t182Alamat">
                        <g:message code="historyCustomer.t182Alamat.label" default="T182 Alamat"/>
                        <span class="required-indicator">*</span>
                    </label>
                    <div class="controls">
                        <g:textArea name="t182Alamat" required=""
                                    value="${historyCustomerInstance?.t182Alamat}"/>
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182RT', 'error')} ">
                    <label class="control-label" for="t182RT">
                        <g:message code="historyCustomer.t182RT.label" default="T182 RT"/>
                    </label>
                    <div class="controls">
                        <g:textField name="t182RT" maxlength="3" value="${historyCustomerInstance?.t182RT}" onkeyup="checkNumber(this);" onblur="isNegative(this);" style="width:50px" />
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182RW', 'error')} ">
                    <label class="control-label" for="t182RW">
                        <g:message code="historyCustomer.t182RW.label" default="T182 RW"/>
                    </label>
                    <div class="controls">
                        <g:textField onkeyup="checkNumber(this);" onblur="isNegative(this);" name="t182RW" maxlength="3"  style="width:50px" value="${historyCustomerInstance?.t182RW}"/>
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'provinsi', 'error')} ">
                    <label class="control-label" for="provinsi">
                        <g:message code="historyCustomer.provinsi.label" default="Provinsi"/>
                    </label>
                    <div class="controls">
                        <g:textField name="__propinsi" id="provinsi"  value="${historyCustomerInstance?.provinsi?.m001NamaProvinsi}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                        <g:hiddenField name="provinsi.id" id="provinsi_id" value="${historyCustomerInstance?.provinsi?.id}" />
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kabKota', 'error')} ">
                    <label class="control-label" for="kabKota">
                        <g:message code="historyCustomer.kabKota.label" default="Kab Kota"/>
                    </label>
                    <div class="controls">
                        <g:textField name="__kabKota" id="kabKota"  value="${historyCustomerInstance?.kabKota?.m002NamaKabKota}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                        <g:hiddenField name="kabKota.id" id="kabKota_id" value="${historyCustomerInstance?.kabKota?.id}" />
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kecamatan', 'error')} ">
                    <label class="control-label" for="kecamatan">
                        <g:message code="historyCustomer.kecamatan.label" default="Kecamatan"/>
                    </label>
                    <div class="controls">
                        <g:textField name="__kecamatan" id="kecamatan"  value="${historyCustomerInstance?.kecamatan?.m003NamaKecamatan}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                        <g:hiddenField name="kecamatan.id" id="kecamatan_id" value="${historyCustomerInstance?.kecamatan?.id}" />
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kelurahan', 'error')} ">
                    <label class="control-label" for="kelurahan">
                        <g:message code="historyCustomer.kelurahan.label" default="Kelurahan"/>
                    </label>
                    <div class="controls">
                        <g:textField name="__kelurahan" id="kelurahan"  value="${historyCustomerInstance?.kelurahan?.m004NamaKelurahan}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                        <g:hiddenField name="kelurahan.id" id="kelurahan_id" value="${historyCustomerInstance?.kelurahan?.id}" />
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182KodePos', 'error')} ">
                    <label class="control-label" for="t182KodePos">
                        <g:message code="historyCustomer.t182KodePos.label" default="T182 Kode Pos"/>
                    </label>
                    <div class="controls">
                        <g:textField onkeyup="checkNumber(this);" onblur="isNegative(this);" name="t182KodePos" maxlength="5"
                                     value="${historyCustomerInstance?.t182KodePos}"/>
                    </div>
                </div>

            </div>

            <div class="tab-pane" id="ac_alamat_npwp">
                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182AlamatNPWP', 'error')} ">
                    <label class="control-label" for="t182AlamatNPWP">
                        <g:message code="historyCustomer.t182AlamatNPWP.label" default="T182 Alamat NPWP"/>
                        <span class="required-indicator">*</span>
                    </label>
                    <div class="controls">
                        <g:textArea name="t182AlamatNPWP" maxlength="50" required=""
                                    value="${historyCustomerInstance?.t182AlamatNPWP}"/>
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182RTNPWP', 'error')} ">
                    <label class="control-label" for="t182RTNPWP">
                        <g:message code="historyCustomer.t182RTNPWP.label" default="T182 RTNPWP"/>
                    </label>
                    <div class="controls">
                        <g:textField onkeyup="checkNumber(this);" onblur="isNegative(this);" style="width:50px" name="t182RTNPWP" maxlength="3" value="${historyCustomerInstance?.t182RTNPWP}"/>
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182RWNPWP', 'error')} ">
                    <label class="control-label" for="t182RWNPWP">
                        <g:message code="historyCustomer.t182RWNPWP.label" default="T182 RWNPWP"/>
                    </label>
                    <div class="controls">
                        <g:textField onkeyup="checkNumber(this);" onblur="isNegative(this);" style="width:50px" name="t182RWNPWP" maxlength="3" value="${historyCustomerInstance?.t182RWNPWP}"/>
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'provinsi2', 'error')} ">
                    <label class="control-label" for="provinsi2">
                        <g:message code="historyCustomer.provinsi2.label" default="Provinsi2"/>
                    </label>
                    <div class="controls">
                        <g:textField name="__propinsi2" id="provinsi2"  value="${historyCustomerInstance?.provinsi2?.m001NamaProvinsi}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                        <g:hiddenField name="provinsi2.id" id="provinsi2_id" value="${historyCustomerInstance?.provinsi2?.id}" />
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kabKota2', 'error')} ">
                    <label class="control-label" for="kabKota2">
                        <g:message code="historyCustomer.kabKota2.label" default="Kab Kota2"/>
                    </label>
                    <div class="controls">
                        <g:textField name="__kabKota2" id="kabKota2"  value="${historyCustomerInstance?.kabKota2?.m002NamaKabKota}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                        <g:hiddenField name="kabKota2.id" id="kabKota2_id" value="${historyCustomerInstance?.kabKota2?.id}" />
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kecamatan2', 'error')} ">
                    <label class="control-label" for="kecamatan2">
                        <g:message code="historyCustomer.kecamatan2.label" default="Kecamatan2"/>
                    </label>

                    <div class="controls">
                        <g:textField name="__kecamatan2" id="kecamatan2"  value="${historyCustomerInstance?.kecamatan2?.m003NamaKecamatan}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                        <g:hiddenField name="kecamatan2.id" id="kecamatan2_id" value="${historyCustomerInstance?.kecamatan2?.id}" />
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kelurahan2', 'error')} ">
                    <label class="control-label" for="kelurahan2">
                        <g:message code="historyCustomer.kelurahan2.label" default="Kelurahan2"/>
                    </label>
                    <div class="controls">
                        <g:textField name="__kelurahan2" id="kelurahan2"  value="${historyCustomerInstance?.kelurahan2?.m004NamaKelurahan}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                        <g:hiddenField name="kelurahan2.id" id="kelurahan2_id" value="${historyCustomerInstance?.kelurahan2?.id}" />
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182KodePosNPWP', 'error')} ">
                    <label class="control-label" for="t182KodePosNPWP">
                        <g:message code="historyCustomer.t182KodePosNPWP.label" default="T182 Kode Pos NPWP"/>
                    </label>
                    <div class="controls">
                        <g:textField onkeyup="checkNumber(this);" onblur="isNegative(this);" name="t182KodePosNPWP" maxlength="5"
                                     value="${historyCustomerInstance?.t182KodePosNPWP}"/>
                    </div>
                </div>

            </div>
        </div>
    </div>

     <br>

     <div id="account_web">
         <legend style="font-size: small">Account Web Hosting</legend>
         <div id="account_web_tabs-1">
             <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182WebUserName', 'error')} ">
                 <label class="control-label" for="t182WebUserName">
                     <g:message code="historyCustomer.t182WebUserName.label" default="T182 Web User Name"/>
                 </label>
                 <div class="controls">
                     <g:textField name="t182WebUserName" maxlength="50" autocomplete="false"
                                  value="${historyCustomerInstance?.t182WebUserName}"/>
                 </div>
             </div>

             <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182WebPassword', 'error')} ">
                 <label class="control-label" for="t182WebPassword">
                     <g:message code="historyCustomer.t182WebPassword.label" default="T182 Web Password"/>
                 </label>
                 <div class="controls">
                     <g:passwordField name="t182WebPassword" maxlength="50" autocomplete="false"
                                      value="${historyCustomerInstance?.t182WebPassword}"/>
                 </div>
             </div>

             <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182WebPassword', 'error')} ">
                 <label class="control-label" for="t182WebPassword1">
                     <g:message code="historyCustomer.t182WebPassword1.label" default="T182 Web Password"/>
                 </label>
                 <div class="controls">
                     <g:passwordField name="t182WebPassword1" maxlength="50" autocomplete="false"
                                      value="${historyCustomerInstance?.t182WebPassword}"/>
                 </div>
             </div>
         </div>
     </div>

    <br>
    <div id="noHp_valid">
         <span style="font-size: 16px;" ><b>Nomor HP Tidak Valid...</b></span>
         <a class="pull-right">
             &nbsp;&nbsp;
             <i>
                 <g:field type="button" style="padding: 5px;width: 80px" class="btn cancel"
                          name="more" id="more" value="${message(code: 'historyCustomer.more.label', default: '[More]')}" />
             </i>
             &nbsp;&nbsp;
         </a>
    </div>
    <br><br>
    </td>
    <td colspan="2" style="vertical-align: top;">
        <div id="vehicle_tabs">
            <legend style="font-size: small">&nbsp;Vehicle</legend>
            <div class="control-group">
                <label class="control-label" for="__noPolisi">
                    Nomor Polisi
                </label>

                <div class="controls">
                    <g:textField name="__noPolisi" value="${historyCustomerVehicleInstance?.kodeKotaNoPol?.m116ID + " " + historyCustomerVehicleInstance?.t183NoPolTengah + " " + historyCustomerVehicleInstance?.t183NoPolBelakang}" readonly="readonly"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="t109WMI">
                    VinCode
                </label>

                <div class="controls">
                    <g:textField style="width: 70px" name="t109WMI" id="t109WMI" value="${fullModelVinCode?.t109WMI}" maxlength="10"/>
                    <g:select style="width: 70px" name="t109VDS" id="t109VDS" from="${com.kombos.administrasi.FullModelVinCode.createCriteria().list {eq("staDel","0");order("t109VDS")}}" optionKey="t109VDS" optionValue="t109VDS" />
                    <g:textField style="width: 60px" name="t109CekDigit" id="t109CekDigit" value="${fullModelVinCode?.t109CekDigit}" maxlength="10"/>
                    <g:textField style="width: 70px" name="t109VIS" id="t109VIS" value="${fullModelVinCode?.t109VIS}" maxlength="10"/>
                    <br/><br/>
                    <g:field type="button" style="width: 70%" class="btn cancel" onclick="validateVinCode();" name="validate" id="validate" value="Validate VIN Code" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="baseModel">
                    <g:message code="historyCustomer.baseModel.label" default="Base Model"/>
                </label>

                <div class="controls">
                    <g:select style="width: 70%" id="baseModel" name="baseModel.id" from="${BaseModel.createCriteria().list {eq("staDel", "0");order("m102NamaBaseModel", "asc")}}"
                              optionKey="id" value="${historyCustomerVehicleInstance?.fullModelCode?.baseModel?.id}" class="many-to-one"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="grade">
                    Grade
                </label>

                <div class="controls">
                    <g:select style="width: 70%" id="grade" name="grade.id" from="${com.kombos.administrasi.Grade.createCriteria().list {eq("staDel", "0");order("m107NamaGrade", "asc")}}"
                              optionKey="id" value="${historyCustomerVehicleInstance?.fullModelCode?.grade?.id}" class="many-to-one"/>
                </div>
            </div>

            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 'engine', 'error')} ">
                <label class="control-label" for="engine">
                    Engine
                </label>

                <div class="controls">
                    <g:select style="width: 70%" id="engine" name="engine.id" from="${com.kombos.administrasi.Engine.createCriteria().list {eq("staDel", "0");order("m108NamaEngine", "asc")}}"
                              optionKey="id" value="${historyCustomerVehicleInstance?.fullModelCode?.engine?.id}" class="many-to-one"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="gear" >
                    Gear
                </label>

                <div class="controls">
                    <g:select style="width: 70%" id="gear" name="gear.id" from="${com.kombos.administrasi.Gear.createCriteria().list {eq("staDel", "0"); order("m106NamaGear", "asc")}}"
                              optionKey="id" value="${historyCustomerVehicleInstance?.fullModelCode?.gear?.id}" class="many-to-one"/>
                </div>
            </div>

            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 'fullModelCode', 'error')} ">
                <label class="control-label" for="fullModelCode" >
                    Full Model Code
                </label>

                <div class="controls">
                    <g:select style="width: 70%" id="fullModelCode" name="fullModelCode.id" from="${com.kombos.administrasi.FullModelCode.createCriteria().list {eq("staDel", "0"); order("t110FullModelCode", "asc")}}"
                              optionKey="id" value="${historyCustomerVehicleInstance?.fullModelCode?.id}" class="many-to-one"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="fullModelVinCodeBln" >
                    Thn Bln Pembuatan
                </label>

                <div class="controls">
                    <g:select name="fullModelVinCodeBln" id="fullModelVinCodeBln" from="${com.kombos.administrasi.FullModelVinCode.createCriteria().list {eq("staDel", "0"); order("t109ThnBlnPembuatan", "asc")}}"/>
                    <g:textField onkeypress="return isNumberKey(event);" name="fullModelVinCodeThn" id="fullModelVinCodeThn" maxlength="20" value=""/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="warna" >
                    Warna
                </label>

                <div class="controls">
                    <g:select  style="width: 70%" id="warna" name="warna.id" from="${Warna.createCriteria().list {eq("staDel","0");order("m092NamaWarna")}}" optionValue="m092NamaWarna" optionKey="id" value="${historyCustomerVehicleInstance?.warna?.id}" class="many-to-one"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="appointment">
                    Kategori Customer
                </label>

                <div class="controls">
                    <g:radioGroup name="kategoriCust" id="kategoriCust" values="['2','1','0']" labels="['Umum','Fleet','Asuransi']" value="${receptionInstance?.t401StaKategoriCustUmumSPKAsuransi}" >
                        ${it.radio} ${it.label}
                    </g:radioGroup>
            </div>

            <g:if test="${menu!="sales"}">
                <a class="pull-right">
                    &nbsp;&nbsp;
                    <i>
                        <g:field type="button" style="padding: 5px;width: 130px;" class="btn cancel" onclick="detailAsuransi();"
                                 name="detailAsr" id="detailAsr" value="${message(code: 'historyCustomer.detailAsuransi.label', default: 'Detail Asuransi')}" />
                    </i>
                    &nbsp;&nbsp;
                </a>
            </g:if>

            <br/><br/><br/>

            <div class="control-group">
                <label class="control-label" for="customerVehicle">
                    <g:message code="historyCustomer.upload.label" default="Upload Buku Service"/>
                </label>

                <div class="controls">
                    <input type="file" name="myDocument" id="myDocument"
                           onchange="uploadDokumen(this.form, '${g.createLink(controller: 'customer', action: 'uploadDokumen')}', 'uploadDokumen');
                           return false;"/><br/><br/>
                    <div id="uploadDokumen">
                    </div>
                    <br/>
                </div>
            </div>

        </div>
    </td>
</tr>
<tr>
    <td colspan="3">
        <g:render template="dataTablesPeranCust" />
    </td>
</tr>

</table>