<%@ page import="org.apache.commons.codec.binary.Base64; com.kombos.customerprofile.Hobby; com.kombos.customerprofile.HistoryCustomer" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'appointment.customer.label', default: 'Appointment Customer')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
		$(function(){ 
			

		});
		</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">Appointment Customer</span>
	</div>
	<div id="appointment" class="box">
		<div class="row-fluid">
			<div class="span5">
				<fieldset>
				<span class="box-label">
				Profile
				</span>
				<div class="box">
					<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182ID', 'error')} ">
						<label class="control-label" for="t182ID">
							<g:message code="historyCustomer.t182ID.label" default="Kode Customer"/>
						</label>

						<div class="controls">
							<g:textField name="t182ID" maxlength="50" readonly="true" value="${historyCustomerInstance?.t182ID}"/>
						</div>
					</div>

					<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182GelarD', 'error')} ">
						<label class="control-label" for="t182GelarD">
							<g:message code="historyCustomer.t182GelarD.label" default="Gelar di depan"/>

						</label>

						<div class="controls">
							<g:textField name="t182GelarD" maxlength="50"
										 value="${historyCustomerInstance?.t182GelarD}"/>
						</div>
					</div>


					<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NamaDepan', 'error')} ">
						<label class="control-label" for="t182NamaDepan">
							<g:message code="historyCustomer.t182NamaDepan.label" default="Nama Depan"/>
							<span class="required-indicator">*</span>
						</label>

						<div class="controls">
							<g:textField name="t182NamaDepan" maxlength="50" required=""
										 value="${historyCustomerInstance?.t182NamaDepan}"/>
						</div>
					</div>

					<div class="tabbable control-group">
						<ul class="nav nav-tabs">
							<li id="tab_ac_alamat_korespondensi" class="active"><a
								href="#ac_alamat_korespondensi" data-toggle="tab">Alamat
									Korespondensi</a></li>
							<li id="tab_ac_alamat_npwp" class=""><a href="#ac_alamat_npwp"
								data-toggle="tab">Alamat NPWP</a></li>
						</ul>

						<div class="tab-content">
							<div class="tab-pane active" id="ac_alamat_korespondensi">
								
							</div>
							<div class="tab-pane" id="ac_alamat_npwp">
								
							</div>
						</div>
					</div>
				</div>	
				</fieldset>
				<fieldset>
				<span class="box-label">Account Web Booking</span>
				<div class="box">
				</div>	
				</fieldset>
			</div>
			<div class="span7">
				<div class="row-fluid">
					<div class="span6">
						<fieldset>
						<span class="box-label">Foto</span>
						<div class="box">
						</div>	
						</fieldset>	
						<fieldset>
						<span class="box-label">Kontak</span>
						<div class="box">
						</div>	
						</fieldset>
					</div>
					<div class="span6">
						<fieldset>
						<span class="box-label">Identitas</span>
						<div class="box">
						</div>	
						</fieldset>	
						<fieldset>
						<span class="box-label">Lain-lain</span>
						<div class="box">
						</div>	
						</fieldset>
					</div>
				</div>
				<fieldset>
						<span class="box-label">Vehicle</span>
						<div class="box">
						</div>	
				</fieldset>	
				</div>
			</div>
		</div>
	</div>
</body>
</html>