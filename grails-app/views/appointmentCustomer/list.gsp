<%@ page import="com.kombos.customerprofile.HistoryCustomer" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title>Appointment Customer</title>
		<r:require modules="baseapplayout" />
		<g:javascript>
			$(function(){ 
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							//shrinkTableLayout('appointmentCustomer');
                            shrinkTableLayout();
							%{--<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormAppointmentCustomer(data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />--}%
                            $('#spinner').fadeIn(1);
                            $.ajax({type:'POST', url:'${request.contextPath}/appointmentCustomer/create/',
                                success:function(data,textStatus){
                                    loadForm(data, textStatus);
                                },
                                error:function(XMLHttpRequest,textStatus,errorThrown){},
                                complete:function(XMLHttpRequest,textStatus){
                                    $('#spinner').fadeOut();
                                }
                            });
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
//										massDelete('appointmentCustomer', '${request.contextPath}/appointmentCustomer/massdelete', reloadContohTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

			});
</g:javascript>
<g:javascript>
	var loadFormAppointmentCustomer;
	var loadForm;
	var uploadDokumen;
	var shrinkTableLayout;
	var expandTableLayout;
	var validateVinCode;
	var detailAsuransi;
	var fileUploadPopup;
	var validateExtension;

	$(function(){

	 detailAsuransi = function(){
        keyword = '';
        $("#detailAsuransiModal").modal({
            "backdrop" : "static",
            "keyboard" : true,
            "show" : true
        }).css({'width': '1400px','margin-left': function () {return -($(this).width() / 2);}});
    }
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	

    edit = function(id) {
        console.log("edit, id=" + id);
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
       	$.ajax({type:'POST', url:'${request.contextPath}/appointmentCustomer/edit/'+id,
   		    data: {id: id, noPolisiDepan: '${noPolisiDepan}', noPolisiTengah: '${noPolisiTengah}', noPolisiBelakang: '${noPolisiBelakang}'},
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    validateVinCode = function(){
            var t109WMI = $('#t109WMI').val();
            var t109VDS = $('#t109VDS').val();
            var t109CekDigit = $('#t109CekDigit').val();
            var t109VIS = $('#t109VIS').val();
            $('#spinner').fadeIn(1);
            $.ajax({
                url: '${request.contextPath}/customer/validate',
                data : {t109WMI : t109WMI,t109VDS : t109VDS,t109CekDigit : t109CekDigit, t109VIS : t109VIS },
                type: 'POST',
                success: function(data){
                    if(data.length>1){
                        $('#baseModel').html(data[0].data);
                        $('#grade').html(data[1].data);
                        $('#engine').html(data[2].data);
                        $('#gear').html(data[3].data);
                        $('#fullModelCode').html(data[4].data);
                        $('#fullModelVinCodeThn').val(data[5].data);
                        $('#fullModelVinCodeBln').html(data[6].data);
                    }else{
                        $('#baseModel').html(data[0].data);
                        $('#grade').html(data[0].data);
                        $('#engine').html(data[0].data);
                        $('#gear').html(data[0]).data;
                        $('#fullModelCode').html(data[0].data);
                        $('#fullModelVinCodeBln').html(data[0].data);
                        $('#fullModelVinCodeThn').val('');
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
    }

    fileUploadPopup = function(form, action_url, div_id) {
        // Create the iframe...
        var ext = $('#t195Foto').val();

        if(validateExtension(ext , 'foto') == false){
           alert("Anda hanya dapat meng-upload file berekstensi JPG, JPEG, PNG atau GIF");
           document.getElementById(div_id).innerHTML = "Upload Failed";
           document.getElementById("t195Foto").focus();
           return;
        }else{
            var iframe = document.createElement("iframe");
            iframe.setAttribute("id", "upload_iframe");
            iframe.setAttribute("name", "upload_iframe");
            iframe.setAttribute("width", "0");
            iframe.setAttribute("height", "0");
            iframe.setAttribute("border", "0");
            iframe.setAttribute("style", "width: 0; height: 0; border: none;");

            // Add to document...
            form.parentNode.appendChild(iframe);
            window.frames['upload_iframe'].name = "upload_iframe";

            iframeId = document.getElementById("upload_iframe");

            // Add event...
            var eventHandler = function () {

                if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
                else iframeId.removeEventListener("load", eventHandler, false);

                // Message from server...
                if (iframeId.contentDocument) {
                    content = iframeId.contentDocument.body.innerHTML;
                } else if (iframeId.contentWindow) {
                    content = iframeId.contentWindow.document.body.innerHTML;
                } else if (iframeId.document) {
                    content = iframeId.document.body.innerHTML;
                }

                document.getElementById(div_id).innerHTML = content;

                // Del the iframe...
                setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
            }

            if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
            if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);

            // Set properties of form...
            form.setAttribute("target", "upload_iframe");
            form.setAttribute("action", action_url);
            form.setAttribute("method", "post");
            form.setAttribute("enctype", "multipart/form-data");
            form.setAttribute("encoding", "multipart/form-data");

            // Submit the form...
            form.submit();

            document.getElementById(div_id).innerHTML = "Uploading...";
        }
    }

    validateExtension = function(v,from){
          var allowedImage = new Array("jpg","JPG","jpeg","JPEG","gif","GIF","png","PNG");
          var allowedDokumen = new Array("jpg","JPG","jpeg","JPEG","png","PNG","PDF","pdf","doc","DOC","DOCX","docx");

          if(from=='foto'){
              for(var ct=0;ct < allowedImage.length ; ct++ ){
                  sample = v.lastIndexOf(allowedImage[ct]);
                  if(sample != -1){return true;}
              }
          }else if(from=='document'){
              for(var ct=0;ct < allowedDokumen.length ; ct++ ){
                  sample = v.lastIndexOf(allowedDokumen[ct]);
                  if(sample != -1){return true;}
              }
          }
          return false;
    }

    uploadDokumen = function(form, action_url, div_id) {
        // Create the iframe...
        var ext = $('#myDocument').val();

        if(validateExtension(ext,'document') == false){
           alert("Anda hanya dapat meng-upload file berekstensi JPG, PNG, PDF atau file microsoft word");
           document.getElementById(div_id).innerHTML = "Upload Failed";
           document.getElementById("myDocument").focus();
           return;
        }else{
            var iframe = document.createElement("iframe");
            iframe.setAttribute("id", "upload_iframe");
            iframe.setAttribute("name", "upload_iframe");
            iframe.setAttribute("width", "0");
            iframe.setAttribute("height", "0");
            iframe.setAttribute("border", "0");
            iframe.setAttribute("style", "width: 0; height: 0; border: none;");

            // Add to document...
            form.parentNode.appendChild(iframe);
            window.frames['upload_iframe'].name = "upload_iframe";

            iframeId = document.getElementById("upload_iframe");

            // Add event...
            var eventHandler = function () {

                if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
                else iframeId.removeEventListener("load", eventHandler, false);

                // Message from server...
                if (iframeId.contentDocument) {
                    content = iframeId.contentDocument.body.innerHTML;
                } else if (iframeId.contentWindow) {
                    content = iframeId.contentWindow.document.body.innerHTML;
                } else if (iframeId.document) {
                    content = iframeId.document.body.innerHTML;
                }

                document.getElementById(div_id).innerHTML = content;

                // Del the iframe...
                setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
            }

            if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
            if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);

            // Set properties of form...
            form.setAttribute("target", "upload_iframe");
            form.setAttribute("action", action_url);
            form.setAttribute("method", "post");
            form.setAttribute("enctype", "multipart/form-data");
            form.setAttribute("encoding", "multipart/form-data");

            // Submit the form...
            form.submit();

            document.getElementById(div_id).innerHTML = "Uploading...";
        }
    }
    
    loadFormAppointmentCustomer = function(data, textStatus){
    	$('#appointmentCustomer-form').html(data);
   	}
   	loadForm = function(data, textStatus){
        $('#appointmentCustomer-form').empty();
        $('#appointmentCustomer-form').append(data);
    }
    shrinkTableLayout = function(){
		$('#appointmentCustomer-table').hide();
        $('#appointmentCustomer-form').css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
		var table = $('#appointmentCustomer-table');
   		table.show();
        $('#appointmentCustomer-form').css("display","none").empty();
   	}
   	
   	massDelete = function(url, callback) {
   		var recordsToDelete = [];
		$('#appointmentCustomer-table tbody .row-select').each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:url,      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		callback();
    		}
		});
		
   	}

});
</g:javascript>
	</head>
	<body>

	<div class="box">
		<div class="span12" id="appointmentCustomer-form" style="display: none;margin-left: 0;"></div>
		<div class="span12" id="appointmentCustomer-table" style="margin-left: 0;">
            %{--<div class="navbar box-header no-border">
                <span class="pull-left">Appointment Customer</span>
                <ul class="nav pull-right">
                    <li><a class="pull-right box-action" href="#"
                        style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
                            class="icon-plus"></i>&nbsp;&nbsp;
                    </a></li>
                    <li><a class="pull-right box-action" href="#"
                        style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                            class="icon-remove"></i>&nbsp;&nbsp;
                    </a></li>
                    <li class="separator"></li>
                </ul>
            </div>--}%
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		
	</div>
    <div id="detailAsuransiModal" class="modal hide" style="width: 1380px;">
        <div class="modal-dialog" style="width: 1380px;">
            <div class="modal-content" style="width: 1400px;">
                <div class="modal-body" style="max-height: 550px; width: 1350px;">
                    <div id="detailAsuransiContent">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <div class="iu-content">
                            <g:formRemote class="form-horizontal" name="createAsuransi" on404="alert('not found!');"
                                          onSuccess="alert('successs');" update="customerAsuransi-form"
                                          url="[controller: 'customer', action: 'saveAsuransi']"
                                          onFailure="toastr.error('Gagal disimpan');">
                                <fieldset class="form">
                                    <g:render template="formDetailAsuransi"/>
                                </fieldset>
                                <br/>

                                <fieldset class="buttons controls">
                                    <div class="pull-right">
                                        <a class="btn cancel" href="javascript:void(0);" onclick="closeModal('detailAsuransi');"><g:message
                                                code="default.button.close.label" default="Close"/></a>
                                        <g:submitButton class="btn btn-primary create" name="saveAsuransi"
                                                        value="${message(code: 'default.button.save.label', default: 'Save')}"/>
                                    </div>
                                </fieldset>

                                <br/>
                            </g:formRemote>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
</body>
</html>
