<table id="customerList_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
            <th style="border-bottom: none;padding: 5px;">
                <div>Nomor Polisi</div>
            </th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nama Customer</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Peran</div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var customerListTable;
var reloadcustomerListTable;
$(function(){
	customerListTable = $('#customerList_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "customerListDatatablesList")}",
		"aoColumns": [

{
	"sName": "nopol",
	"mDataProp": "nopol",
	"aTargets": [0],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "customer",
	"mDataProp": "customer",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "peran",
	"mDataProp": "peran",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
        				aoData.push(
									{"name": 'historyCustomerId', "value": ${(historyCustomerInstance?.id == null ? -1 : historyCustomerInstance?.id)}}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
