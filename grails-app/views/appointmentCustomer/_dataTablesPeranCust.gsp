<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="peranCust_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed" style="table-layout: fixed;">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div>Peran Customer</div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div>Nama Depan</div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div>Nama Belakang</div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div>Tanggal Transaksi</div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
    var addPeranCustTable;
    var reloadPeranCustTable;
$(function(){
    reloadPeranCustTable = function() {
   		addPeranCustTable.fnDraw();
   	}
   
   
   	addPeranCustTable = $('#peranCust_datatables').dataTable({
   		"sScrollX": "95%",
   		"bScrollCollapse": true,
   		"bAutoWidth" : false,
   		"bPaginate" : false,
   		"sInfo" : "",
   		"sInfoEmpty" : "",
   		bFilter: false,
   		"bStateSave": false,
   		'sPaginationType': 'bootstrap',
   		"fnInitComplete": function () {
   			this.fnAdjustColumnSizing(true);
           },
           "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
               return nRow;
           },
   		"bSort": true,
   		"bProcessing": true,
   		"bDestroy":true,
   		"iDisplayLength" : maxLineDisplay,
        "sAjaxSource": "${g.createLink(action: "datatablesPeranCustList")}",
   		"aoColumns": [
    {
        "sName": "peranCustomer",
        "mDataProp": "peranCustomer",
        "aTargets": [0],
        "bSearchable": true,
        "bSortable": false,
        "bVisible": true
    }
    ,
    {
        "sName": "namaDepan",
        "mDataProp": "namaDepan",
        "aTargets": [1],
        "bSearchable": true,
        "bSortable": false,
        "bVisible": true
    }
    ,
    {
        "sName": "namaBelakang",
        "mDataProp": "namaBelakang",
        "aTargets": [2],
        "bSearchable": true,
        "bSortable": false,
        "bVisible": true
    }
    ,
    {
        "sName": "tglTransaksi",
        "mDataProp": "tglTransaksi",
        "aTargets": [3],
        "bSearchable": true,
        "bSortable": false,
        "bVisible": true
    }
   ],
           "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
               aoData.push(
                    {"name": 'historyCustomerId', "value": ${(historyCustomerInstance?.id == null ? -1 : historyCustomerInstance?.id)}}
                );
               $.ajax({ "dataType": 'json',
                   "type": "POST",
                   "url": sSource,
                   "data": aoData ,
                   "success": function (json) {
                       fnCallback(json);
                      },
                   "complete": function () {
                      }
               });
   		}
   	});

});
</g:javascript>



