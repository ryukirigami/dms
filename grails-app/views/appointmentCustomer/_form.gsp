<%@ page import="org.apache.commons.codec.binary.Base64; com.kombos.customerprofile.Hobby; com.kombos.customerprofile.HistoryCustomer" %>


<script>
    $(function () {
        $("#profile_tabs").tabs();
        $("#alamat_tabs").tabs();
        $("#account_web").tabs();
        $("#foto_profile_tabs").tabs();
        $("#customer_cars_tabs").tabs();
        $("#kontak_profile_tabs").tabs();
        $("#identitas_profile_tabs").tabs();
        $("#lain_profile_tabs").tabs();
        $("#tpss_survey_tab").tabs();
        $("#history_retention_tabs").tabs();
        $("#history_kepemilikan_mobil_tabs").tabs();
        $("#customer_list_tabs").tabs();
    });
</script>

<table style="width: 100%;padding-left: 5px;padding-right: 5px;">
<tr style="vertical-align: top;">
<td style="width: 33%;padding:5px;">
<div id="profile_tabs">
    %{--<ul>--}%
    %{--<li><a href="#profile_tabs-1">Profile</a></li>--}%
    %{--</ul>--}%
    <legend style="font-size: small">Profile</legend>

    <div id="profile_tabs-1">

        <g:if test="${historyCustomerInstance?.customer}">
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'customer', 'error')} ">
                <label class="control-label">
                    <g:message code="historyCustomer.t182ID.label" default="Kode Customer"/>
                    %{--<span class="required-indicator">*</span>--}%
                </label>

                <div class="controls">
                    ${historyCustomerInstance?.customer}
                </div>
            </div>
        </g:if>


        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182ID', 'error')} ">
            <label class="control-label" for="t182ID">
                <g:message code="historyCustomer.t182ID.label" default="Kode Customer"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:textField name="t182ID" maxlength="50" readonly="true" value="${historyCustomerInstance?.t182ID}"/>
                %{--<g:field name="t182ID" type="number" onkeyup="checkNumber(this);" value="${historyCustomerInstance.t182ID}" />--}%
            </div>
        </div>
        <g:javascript>
            document.getElementById("t182ID").focus();
        </g:javascript>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182GelarD', 'error')} ">
            <label class="control-label" for="t182GelarD">
                <g:message code="historyCustomer.t182GelarD.label" default="Gelar di depan"/>

            </label>

            <div class="controls">
                <g:textField name="t182GelarD" maxlength="50"
                             value="${historyCustomerInstance?.t182GelarD}"/>
            </div>
        </div>


        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NamaDepan', 'error')} ">
            <label class="control-label" for="t182NamaDepan">
                <g:message code="historyCustomer.t182NamaDepan.label" default="Nama Depan"/>
                <span class="required-indicator">*</span>
            </label>

            <div class="controls">
                <g:textField name="t182NamaDepan" maxlength="50" required=""
                             value="${historyCustomerInstance?.t182NamaDepan}"/>
            </div>
        </div>


        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NamaBelakang', 'error')} ">
            <label class="control-label" for="t182NamaBelakang">
                <g:message code="historyCustomer.t182NamaBelakang.label" default="Nama Belakang"/>

            </label>

            <div class="controls">
                <g:textField name="t182NamaBelakang" maxlength="50"
                             value="${historyCustomerInstance?.t182NamaBelakang}"/>
            </div>
        </div>


        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182GelarB', 'error')} ">
            <label class="control-label" for="t182GelarB">
                <g:message code="historyCustomer.t182GelarB.label" default="Gelar di belakang"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:textField name="t182GelarB" maxlength="50"
                             value="${historyCustomerInstance?.t182GelarB}"/>
            </div>
        </div>


        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182JenisKelamin', 'error')} ">
            <label class="control-label" for="t182JenisKelamin">
                <g:message code="historyCustomer.t182JenisKelamin.label" default="Jenis Kelamin"/>
                <span class="required-indicator">*</span>
            </label>

            <div class="controls">
                <g:select name="t182JenisKelamin" required=""
                          from="${HistoryCustomer.JENIS_KELAMIN}"
                          optionKey="id"
                          optionValue="nama"
                          value="${historyCustomerInstance?.t182JenisKelamin}"
                          valueMessagePrefix="historyCustomer.t182JenisKelamin"/>
            </div>
        </div>


        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182TglLahir', 'error')} ">
            <label class="control-label" for="t182TglLahir">
                <g:message code="historyCustomer.t182TglLahir.label" default="Tgl. Lahir"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                %{--<div style="float: left;display: inline-block;width:300px;">--}%
                %{--<g:datePicker name="t182TglLahir" precision="day" value="${historyCustomerInstance?.t182TglLahir}"/>--}%
                %{--</div>--}%

                <ba:datePicker name="t182TglLahir" precision="day" value="${historyCustomerInstance?.t182TglLahir}"
                               format="dd/mm/yyyy"/>

                %{--<g:textField name="t182TglLahir" maxlength="50" --}%
                %{--value="${historyCustomerInstance?.t182TglLahir}"/>--}%
                %{--<script type="text/javascript">--}%
                %{--$("#t182TglLahir").datepicker({dateFormat: 'dd/mm/yy'});--}%
                %{--</script>--}%

            </div>
        </div>


        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'jenisCustomer', 'error')} ">
            <label class="control-label" for="jenisCustomer">
                <g:message code="historyCustomer.jenisCustomer.label" default="Jenis Customer"/>

            </label>

            <div class="controls">
                <g:select name="jenisCustomer" from="${historyCustomerInstance.constraints.jenisCustomer.inList}"
                          value="${historyCustomerInstance?.jenisCustomer}"
                          valueMessagePrefix="historyCustomer.jenisCustomer"/>
            </div>
        </div>


        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'company', 'error')} ">
            <label class="control-label" for="company">
                <g:message code="historyCustomer.company.label" default="Company"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:select id="company" name="company.id" from="${com.kombos.maintable.Company.list()}"
                          optionKey="id"
                          value="${historyCustomerInstance?.company?.id}" class="many-to-one"/>
            </div>
        </div>


        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'peranCustomer', 'error')} ">
            <label class="control-label" for="peranCustomer">
                <g:message code="historyCustomer.peranCustomer.label" default="Peran Customer"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:select id="peranCustomer" name="peranCustomer.id"
                          from="${com.kombos.maintable.PeranCustomer.list()}"
                          optionKey="id" value="${historyCustomerInstance?.peranCustomer?.id}"
                          class="many-to-one"/>
            </div>
        </div>

    </div>
</div>

<br>

<div id="alamat_tabs">
<ul>
    <li><a href="#alamat_tabs-1">Alamat Korespodensi</a></li>
    <li><a href="#alamat_tabs-2">Alamat NPWP</a></li>
</ul>

<div id="alamat_tabs-1">

    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182Alamat', 'error')} ">
        <label class="control-label" for="t182Alamat">
            <g:message code="historyCustomer.t182Alamat.label" default="T182 Alamat"/>
            <span class="required-indicator">*</span>
        </label>

        <div class="controls">
            <g:textArea name="t182Alamat" required=""
                        value="${historyCustomerInstance?.t182Alamat}"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182RT', 'error')} ">
        <label class="control-label" for="t182RT">
            <g:message code="historyCustomer.t182RT.label" default="T182 RT"/>
            %{--<span class="required-indicator">*</span>--}%
        </label>

        <div class="controls">
            <g:field type="number" onkeyup="checkNumber(this);" style="width:50px" name="t182RT" maxlength="3"  value="${historyCustomerInstance?.t182RT}"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182RW', 'error')} ">
        <label class="control-label" for="t182RW">
            <g:message code="historyCustomer.t182RW.label" default="T182 RW"/>
            %{--<span class="required-indicator">*</span>--}%
        </label>

        <div class="controls">
            <g:field type="number" onkeyup="checkNumber(this);" name="t182RW" maxlength="3"  style="width:50px" value="${historyCustomerInstance?.t182RW}"/>
        </div>
    </div>


    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'provinsi', 'error')} ">
        <label class="control-label" for="provinsi">
            <g:message code="historyCustomer.provinsi.label" default="Provinsi"/>
            %{--<span class="required-indicator">*</span>--}%
        </label>

        <div class="controls">
            <g:select id="provinsi" name="provinsi.id" from="${com.kombos.administrasi.Provinsi.list()}"
                      optionKey="id"
                      value="${historyCustomerInstance?.provinsi?.id}" class="many-to-one"/>
        </div>
    </div>


    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kabKota', 'error')} ">
        <label class="control-label" for="kabKota">
            <g:message code="historyCustomer.kabKota.label" default="Kab Kota"/>
            %{--<span class="required-indicator">*</span>--}%
        </label>

        <div class="controls">
            <g:select id="kabKota" name="kabKota.id" from="${com.kombos.administrasi.KabKota.list()}" optionKey="id"
                      value="${historyCustomerInstance?.kabKota?.id}" class="many-to-one"/>
        </div>
    </div>


    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kecamatan', 'error')} ">
        <label class="control-label" for="kecamatan">
            <g:message code="historyCustomer.kecamatan.label" default="Kecamatan"/>
            %{--<span class="required-indicator">*</span>--}%
        </label>

        <div class="controls">
            <g:select id="kecamatan" name="kecamatan.id" from="${com.kombos.administrasi.Kecamatan.list()}"
                      optionKey="id"
                      value="${historyCustomerInstance?.kecamatan?.id}" class="many-to-one"/>
        </div>
    </div>


    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kelurahan', 'error')} ">
        <label class="control-label" for="kelurahan">
            <g:message code="historyCustomer.kelurahan.label" default="Kelurahan"/>
            %{--<span class="required-indicator">*</span>--}%
        </label>

        <div class="controls">
            <g:select id="kelurahan" name="kelurahan.id" from="${com.kombos.administrasi.Kelurahan.list()}"
                      optionKey="id"
                      value="${historyCustomerInstance?.kelurahan?.id}" class="many-to-one"/>
        </div>
    </div>


    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182KodePos', 'error')} ">
        <label class="control-label" for="t182KodePos">
            <g:message code="historyCustomer.t182KodePos.label" default="T182 Kode Pos"/>
            %{--<span class="required-indicator">*</span>--}%
        </label>

        <div class="controls">
            <g:field type="number" onkeyup="checkNumber(this);" name="t182KodePos" maxlength="5"
                         value="${historyCustomerInstance?.t182KodePos}"/>
        </div>
    </div>

</div>


<div id="alamat_tabs-2">

    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182AlamatNPWP', 'error')} ">
        <label class="control-label" for="t182AlamatNPWP">
            <g:message code="historyCustomer.t182AlamatNPWP.label" default="T182 Alamat NPWP"/>
            %{--<span class="required-indicator">*</span>--}%
        </label>

        <div class="controls">
            <g:textArea name="t182AlamatNPWP" maxlength="50"
                        value="${historyCustomerInstance?.t182AlamatNPWP}"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182RTNPWP', 'error')} ">
        <label class="control-label" for="t182RTNPWP">
            <g:message code="historyCustomer.t182RTNPWP.label" default="T182 RTNPWP"/>
            %{--<span class="required-indicator">*</span>--}%
        </label>

        <div class="controls">
            <g:field type="number" onkeyup="checkNumber(this);" style="width:50px" name="t182RTNPWP" maxlength="3" value="${historyCustomerInstance?.t182RTNPWP}"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182RWNPWP', 'error')} ">
        <label class="control-label" for="t182RWNPWP">
            <g:message code="historyCustomer.t182RWNPWP.label" default="T182 RWNPWP"/>
            %{--<span class="required-indicator">*</span>--}%
        </label>

        <div class="controls">
            <g:field type="number" onkeyup="checkNumber(this);" style="width:50px" name="t182RWNPWP" maxlength="3" value="${historyCustomerInstance?.t182RWNPWP}"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'provinsi2', 'error')} ">
        <label class="control-label" for="provinsi2">
            <g:message code="historyCustomer.provinsi2.label" default="Provinsi2"/>
            %{--<span class="required-indicator">*</span>--}%
        </label>

        <div class="controls">
            <g:select id="provinsi2" name="provinsi2.id" from="${com.kombos.administrasi.Provinsi.list()}"
                      optionKey="id"
                      value="${historyCustomerInstance?.provinsi2?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kabKota2', 'error')} ">
        <label class="control-label" for="kabKota2">
            <g:message code="historyCustomer.kabKota2.label" default="Kab Kota2"/>
            %{--<span class="required-indicator">*</span>--}%
        </label>

        <div class="controls">
            <g:select id="kabKota2" name="kabKota2.id" from="${com.kombos.administrasi.KabKota.list()}" optionKey="id"
                      value="${historyCustomerInstance?.kabKota2?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kecamatan2', 'error')} ">
        <label class="control-label" for="kecamatan2">
            <g:message code="historyCustomer.kecamatan2.label" default="Kecamatan2"/>
            %{--<span class="required-indicator">*</span>--}%
        </label>

        <div class="controls">
            <g:select id="kecamatan2" name="kecamatan2.id" from="${com.kombos.administrasi.Kecamatan.list()}"
                      optionKey="id"
                      value="${historyCustomerInstance?.kecamatan2?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kelurahan2', 'error')} ">
        <label class="control-label" for="kelurahan2">
            <g:message code="historyCustomer.kelurahan2.label" default="Kelurahan2"/>
            %{--<span class="required-indicator">*</span>--}%
        </label>

        <div class="controls">
            <g:select id="kelurahan2" name="kelurahan2.id" from="${com.kombos.administrasi.Kelurahan.list()}"
                      optionKey="id"
                      value="${historyCustomerInstance?.kelurahan2?.id}" class="many-to-one"/>
        </div>
    </div>


    <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182KodePosNPWP', 'error')} ">
        <label class="control-label" for="t182KodePosNPWP">
            <g:message code="historyCustomer.t182KodePosNPWP.label" default="T182 Kode Pos NPWP"/>
            %{--<span class="required-indicator">*</span>--}%
        </label>

        <div class="controls">
            <g:field type="number" onkeyup="checkNumber(this);" name="t182KodePosNPWP" maxlength="5"
                         value="${historyCustomerInstance?.t182KodePosNPWP}"/>
        </div>
    </div>

</div>

</div>

<br>

<div id="account_web">
    %{--<ul>--}%
    %{--<li><a href="#account_web_tabs-1">Account Web Hosting</a></li>--}%
    %{--</ul>--}%
    <legend style="font-size: small">Account Web Hosting</legend>

    <div id="account_web_tabs-1">

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182WebUserName', 'error')} ">
            <label class="control-label" for="t182WebUserName">
                <g:message code="historyCustomer.t182WebUserName.label" default="T182 Web User Name"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:textField name="t182WebUserName" maxlength="50"
                             value="${historyCustomerInstance?.t182WebUserName}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182WebPassword', 'error')} ">
            <label class="control-label" for="t182WebPassword">
                <g:message code="historyCustomer.t182WebPassword.label" default="T182 Web Password"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:passwordField name="t182WebPassword" maxlength="50"
                                 value="${historyCustomerInstance?.t182WebPassword}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182WebPassword', 'error')} ">
            <label class="control-label" for="t182WebPassword1">
                <g:message code="historyCustomer.t182WebPassword1.label" default="T182 Web Password"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:passwordField name="t182WebPassword1" maxlength="50"
                                 value="${historyCustomerInstance?.t182WebPassword}"/>
            </div>
        </div>

    </div>

</div>

</td>
<td style="width: 33%;padding:5px;">

<div id="foto_profile_tabs">
    %{--<ul>--}%
    %{--<li><a href="#foto_profile_tabs-1">Foto</a></li>--}%
    %{--</ul>--}%
    <legend style="font-size: small">Foto</legend>

    <div id="foto_profile_tabs-1">
        %{--<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182Foto', 'error')} ">--}%
        %{--<label class="control-label" for="t182Foto">--}%
        %{--<g:message code="historyCustomer.t182Foto.label" default="Upload"/>--}%
        %{--<span class="required-indicator">*</span>--}%
        %{--</label>--}%

        %{--<div class="controls">--}%
        %{--<input type="file" id="t182Foto" name="t182Foto"/>--}%
        %{--</div>--}%
        %{--</div>--}%

        <script language="Javascript">
            function fileUpload(form, action_url, div_id) {
                // Create the iframe...
                var iframe = document.createElement("iframe");
                iframe.setAttribute("id", "upload_iframe");
                iframe.setAttribute("name", "upload_iframe");
                iframe.setAttribute("width", "0");
                iframe.setAttribute("height", "0");
                iframe.setAttribute("border", "0");
                iframe.setAttribute("style", "width: 0; height: 0; border: none;");

                // Add to document...
                form.parentNode.appendChild(iframe);
                window.frames['upload_iframe'].name = "upload_iframe";

                iframeId = document.getElementById("upload_iframe");

                // Add event...
                var eventHandler = function () {

                    if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
                    else iframeId.removeEventListener("load", eventHandler, false);

                    // Message from server...
                    if (iframeId.contentDocument) {
                        content = iframeId.contentDocument.body.innerHTML;
                    } else if (iframeId.contentWindow) {
                        content = iframeId.contentWindow.document.body.innerHTML;
                    } else if (iframeId.document) {
                        content = iframeId.document.body.innerHTML;
                    }

                    document.getElementById(div_id).innerHTML = content;

                    // Del the iframe...
                    setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
                }

                if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
                if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);

                // Set properties of form...
                form.setAttribute("target", "upload_iframe");
                form.setAttribute("action", action_url);
                form.setAttribute("method", "post");
                form.setAttribute("enctype", "multipart/form-data");
                form.setAttribute("encoding", "multipart/form-data");

                // Submit the form...
                form.submit();

                document.getElementById(div_id).innerHTML = "Uploading...";
            }
        </script>

        <g:form>
            <input type="file" name="myFile"
                   onchange="fileUpload(this.form, '${g.createLink(controller: 'historyCustomer', action: 'uploadImage')}', 'upload');
                   return false;"/><br/><br/>
        %{--<input type="button" value="upload" onClick="fileUpload(this.form, '${g.createLink(controller: 'historyCustomer', action: 'uploadImage')}', 'upload'); return false;">--}%
            <div id="upload">
                <g:if test="${historyCustomerInstance?.t182Foto}">
                    <img width="200px"
                         src="data:jpg;base64,${new String(new Base64().encode(historyCustomerInstance.t182Foto), "UTF-8")}"/>
                </g:if>
            </div>
        </g:form>

    </div>
</div>


<br>

<div id="kontak_profile_tabs">
    %{--<ul>--}%
    %{--<li><a href="#kontak_profile_tabs-1">Kontak</a></li>--}%
    %{--</ul>--}%
    <legend style="font-size: small">Kontak</legend>

    <div id="kontak_profile_tabs-1">

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoTelpRumah', 'error')} ">
            <label class="control-label" for="t182NoTelpRumah">
                <g:message code="historyCustomer.t182NoTelpRumah.label" default="T182 No Telp Rumah"/>
                <span class="required-indicator">*</span>
            </label>

            <div class="controls">
                <g:field type="number" onkeyup="checkNumber(this);" name="t182NoTelpRumah" maxlength="50" required=""
                             value="${historyCustomerInstance?.t182NoTelpRumah}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoTelpKantor', 'error')} ">
            <label class="control-label" for="t182NoTelpKantor">
                <g:message code="historyCustomer.t182NoTelpKantor.label" default="T182 No Telp Kantor"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:field type="number" onkeyup="checkNumber(this);" name="t182NoTelpKantor" maxlength="50"
                             value="${historyCustomerInstance?.t182NoTelpKantor}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoFax', 'error')} ">
            <label class="control-label" for="t182NoFax">
                <g:message code="historyCustomer.t182NoFax.label" default="T182 No Fax"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:field type="number" onkeyup="checkNumber(this);" name="t182NoFax" maxlength="50" value="${historyCustomerInstance?.t182NoFax}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoHp', 'error')} ">
            <label class="control-label" for="t182NoHp">
                <g:message code="historyCustomer.t182NoHp.label" default="T182 No Hp"/>
                <span class="required-indicator">*</span>
            </label>

            <div class="controls">
                <g:field type="number" onkeyup="checkNumber(this);" name="t182NoHp" required="" maxlength="20" value="${historyCustomerInstance?.t182NoHp}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182Email', 'error')} ">
            <label class="control-label" for="t182Email">
                <g:message code="historyCustomer.t182Email.label" default="T182 Email"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:field type="email" name="t182Email" maxlength="50" value="${historyCustomerInstance?.t182Email}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182StaTerimaMRS', 'error')} ">
            <label class="control-label" for="t182StaTerimaMRS">
                <g:message code="historyCustomer.t182StaTerimaMRS.label" default="T182 Sta Terima MRS"/>

            </label>

            <div class="controls">
                <g:checkBox name="t182StaTerimaMRS" value="${historyCustomerInstance?.t182StaTerimaMRS}"/>
            </div>
        </div>

    </div>
</div>


<br>

<div id="identitas_profile_tabs">
    %{--<ul>--}%
    %{--<li><a href="#identitas_profile_tabs-1">Identitas</a></li>--}%
    %{--</ul>--}%

    <legend style="font-size: small">Identitas</legend>

    <div id="identitas_profile_tabs-1">

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'jenisIdCard', 'error')} ">
            <label class="control-label" for="jenisIdCard">
                <g:message code="historyCustomer.jenisIdCard.label" default="Jenis Id Card"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:select id="jenisIdCard" name="jenisIdCard.id" from="${com.kombos.customerprofile.JenisIdCard.list()}"
                          optionKey="id" value="${historyCustomerInstance?.jenisIdCard?.id}"
                          class="many-to-one"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NomorIDCard', 'error')} ">
            <label class="control-label" for="t182NomorIDCard">
                <g:message code="historyCustomer.t182NomorIDCard.label" default="T182 Nomor IDC ard"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:textField name="t182NomorIDCard" maxlength="20"
                             value="${historyCustomerInstance?.t182NomorIDCard}"/>
            </div>
        </div>


        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NPWP', 'error')} ">
            <label class="control-label" for="t182NPWP">
                <g:message code="historyCustomer.t182NPWP.label" default="T182 NPWP"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:textField name="t182NPWP" maxlength="20" value="${historyCustomerInstance?.t182NPWP}"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoFakturPajakStd', 'error')} ">
            <label class="control-label" for="t182NoFakturPajakStd">
                <g:message code="historyCustomer.t182NoFakturPajakStd.label" default="T182 No Faktur Pajak Std"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:textField name="t182NoFakturPajakStd" maxlength="20"
                             value="${historyCustomerInstance?.t182NoFakturPajakStd}"/>
            </div>
        </div>

    </div>
</div>


<br>

<div id="lain_profile_tabs">
    %{--<ul>--}%
    %{--<li><a href="#lain_profile_tabs-1">Lain-lain</a></li>--}%
    %{--</ul>--}%
    <legend style="font-size: small">Lain-lain</legend>

    <div id="lain_profile_tabs-1">

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'agama', 'error')} ">
            <label class="control-label" for="agama">
                <g:message code="historyCustomer.agama.label" default="Agama"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:select id="agama" name="agama.id" from="${com.kombos.customerprofile.Agama.list()}" optionKey="id"
                          value="${historyCustomerInstance?.agama?.id}" class="many-to-one"/>
            </div>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'nikah', 'error')} ">
            <label class="control-label" for="nikah">
                <g:message code="historyCustomer.nikah.label" default="Nikah"/>
                %{--<span class="required-indicator">*</span>--}%
            </label>

            <div class="controls">
                <g:select id="nikah" name="nikah.id" from="${com.kombos.maintable.Nikah.list()}" optionKey="id"

                          value="${historyCustomerInstance?.nikah?.id}" class="many-to-one"/>
            </div>
        </div>


        <div class="control-group ${hasErrors(bean: historyCustomerInstance, field: 'hobbies', 'error')} ">
            <label class="control-label" for="hobbies">
                <g:message code="historyCustomer.hobbies.label" default="Hobbies"/>

            </label>

            <div class="controls">
                <g:select name="hobbies" from="${Hobby.list()}" multiple="multiple" optionKey="id" size="10"
                          value="${historyCustomerInstance?.hobbies*.id}" class="many-to-many"/>
            </div>
        </div>

    </div>
</div>

</td>
<td style="width: 33%;padding:5px;">
    <div id="customer_cars_tabs">
        %{--<ul>--}%
        %{--<li><a href="#customer_cars_tabs-1">Customer Cars</a></li>--}%
        %{--</ul>--}%

        <legend style="font-size: small">Customer Cars</legend>

        <div id="customer_cars_tabs-1">

            <g:render template="customerCarsDataTables"/>

        </div>
    </div>
    <br>

    <div id="customer_list_tabs">
        %{--<ul>--}%
        %{--<li><a href="#customer_list_tabs-1">Customer List</a></li>--}%
        %{--</ul>--}%

        <legend style="font-size: small">Customer List</legend>

        <div id="customer_list_tabs-1">

            <g:render template="customerListDataTables"/>

        </div>
    </div>
    <br>

    <div id="history_kepemilikan_mobil_tabs">
        %{--<ul>--}%
        %{--<li><a href="#history_kepemilikan_mobil_tabs-1">History Kepemilikan Mobil</a></li>--}%
        %{--</ul>--}%

        <legend style="font-size: small">History Kepemilikan Mobil</legend>

        <div id="history_kepemilikan_mobil_tabs-1">

            <g:render template="historyKepemilikanMobilDataTables"/>

        </div>
    </div>
    <br>

    <div id="history_retention_tabs">
        %{--<ul>--}%
        %{--<li><a href="#history_retention_tabs-1">History Retention</a></li>--}%
        %{--</ul>--}%

        <legend style="font-size: small">History Retention</legend>

        <div id="history_retention_tabs-1">

            <div id="tpss_survey_tab">
                <ul>
                    <li><a href="#tpss_survey_tab-1">TPSS Survey</a></li>
                    <li><a href="#tpss_survey_tab-2">MRS Birthday</a></li>
                    <li><a href="#tpss_survey_tab-3">MRS STNK</a></li>
                </ul>

                <div id="tpss_survey_tab-1">
                    <g:render template="surveyListDataTables"/>
                </div>

                <div id="tpss_survey_tab-2">

                </div>

                <div id="tpss_survey_tab-3">

                </div>
            </div>

        </div>
    </div>
</td>
</tr>
</table>






%{--<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'customer', 'error')} ">--}%
%{--<label class="control-label" for="customer">--}%
%{--<g:message code="historyCustomer.customer.label" default="Customer" />--}%

%{--</label>--}%
%{--<div class="controls">--}%
%{--<g:select id="customer" name="customer.id" from="${com.kombos.maintable.Customer.list()}" optionKey="id"  value="${historyCustomerInstance?.customer?.id}" class="many-to-one"/>--}%
%{--</div>--}%
%{--</div>--}%















%{--<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182StaCompany', 'error')} ">--}%
%{--<label class="control-label" for="t182StaCompany">--}%
%{--<g:message code="historyCustomer.t182StaCompany.label" default="T182 Sta Company"/>--}%

%{--</label>--}%

%{--<div class="controls">--}%
%{--<g:textField name="t182StaCompany" maxlength="1" --}%
%{--value="${historyCustomerInstance?.t182StaCompany}"/>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182HMinusKirimSMS', 'error')} ">--}%
%{--<label class="control-label" for="t182HMinusKirimSMS">--}%
%{--<g:message code="historyCustomer.t182HMinusKirimSMS.label" default="T182 HM inus Kirim SMS"/>--}%

%{--</label>--}%

%{--<div class="controls">--}%
%{--<g:field name="t182HMinusKirimSMS" type="number" onkeyup="checkNumber(this);" value="${historyCustomerInstance.t182HMinusKirimSMS}"--}%
%{--/>--}%
%{--</div>--}%
%{--</div>--}%


%{--<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182Ket', 'error')} ">--}%
%{--<label class="control-label" for="t182Ket">--}%
%{--<g:message code="historyCustomer.t182Ket.label" default="T182 Ket"/>--}%

%{--</label>--}%

%{--<div class="controls">--}%
%{--<g:textField name="t182Ket" maxlength="50"  value="${historyCustomerInstance?.t182Ket}"/>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182xNamaUser', 'error')} ">--}%
%{--<label class="control-label" for="t182xNamaUser">--}%
%{--<g:message code="historyCustomer.t182xNamaUser.label" default="T182x Nama User"/>--}%

%{--</label>--}%

%{--<div class="controls">--}%
%{--<g:textField name="t182xNamaUser" maxlength="20"  value="${historyCustomerInstance?.t182xNamaUser}"/>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182xNamaDivisi', 'error')} ">--}%
%{--<label class="control-label" for="t182xNamaDivisi">--}%
%{--<g:message code="historyCustomer.t182xNamaDivisi.label" default="T182x Nama Divisi"/>--}%

%{--</label>--}%

%{--<div class="controls">--}%
%{--<g:textField name="t182xNamaDivisi" maxlength="20" --}%
%{--value="${historyCustomerInstance?.t182xNamaDivisi}"/>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182TglTransaksi', 'error')} ">--}%
%{--<label class="control-label" for="t182TglTransaksi">--}%
%{--<g:message code="historyCustomer.t182TglTransaksi.label" default="T182 Tgl Transaksi"/>--}%

%{--</label>--}%

%{--<div class="controls">--}%
%{--<g:datePicker name="t182TglTransaksi" precision="day" value="${historyCustomerInstance?.t182TglTransaksi}"/>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'staDel', 'error')} ">--}%
%{--<label class="control-label" for="staDel">--}%
%{--<g:message code="historyCustomer.staDel.label" default="Sta Del"/>--}%

%{--</label>--}%

%{--<div class="controls">--}%
%{--<g:textField name="staDel" maxlength="1"  value="${historyCustomerInstance?.staDel}"/>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'createdBy', 'error')} ">--}%
%{--<label class="control-label" for="createdBy">--}%
%{--<g:message code="historyCustomer.createdBy.label" default="Created By"/>--}%

%{--</label>--}%

%{--<div class="controls">--}%
%{--<g:textField name="createdBy" value="${historyCustomerInstance?.createdBy}"/>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'updatedBy', 'error')} ">--}%
%{--<label class="control-label" for="updatedBy">--}%
%{--<g:message code="historyCustomer.updatedBy.label" default="Updated By"/>--}%

%{--</label>--}%

%{--<div class="controls">--}%
%{--<g:textField name="updatedBy" value="${historyCustomerInstance?.updatedBy}"/>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'lastUpdProcess', 'error')} ">--}%
%{--<label class="control-label" for="lastUpdProcess">--}%
%{--<g:message code="historyCustomer.lastUpdProcess.label" default="Last Upd Process"/>--}%

%{--</label>--}%

%{--<div class="controls">--}%
%{--<g:textField name="lastUpdProcess" value="${historyCustomerInstance?.lastUpdProcess}"/>--}%
%{--</div>--}%
%{--</div>--}%

