<%@ page import="org.apache.commons.codec.binary.Base64; com.kombos.customerprofile.HistoryCustomer" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="bootstrapeditable"/>
    <g:set var="entityName"
           value="${message(code: 'historyCustomer.label', default: 'HistoryCustomer')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <g:javascript disposition="head">
var deleteHistoryCustomer;

$(function(){ 
	deleteHistoryCustomer=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/historyCustomer/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadHistoryCustomerTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

    </g:javascript>
</head>

<body>

<div id="main_tabs">
%{--<ul>--}%
    %{--<li><a href="#main_tabs-1"><g:message code="default.create.label" args="[entityName]"/></a></li>--}%
%{--</ul>--}%

<div id="main_tabs-1">

<div id="show-historyCustomer" role="main">
<legend>
    <g:message code="default.show.label" args="[entityName]"/>
</legend>
<g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
</g:if>
<table id="historyCustomer"
       class="table table-bordered table-hover">
<tbody>

%{--<g:if test="${historyCustomerInstance?.customer}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="customer-label" class="property-label"><g:message--}%
%{--code="historyCustomer.customer.label" default="Customer" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="customer-label">--}%
%{--<ba:editableValue--}%
%{--bean="${historyCustomerInstance}" field="customer"--}%
%{--url="${request.contextPath}/HistoryCustomer/updatefield" type="text"--}%
%{--title="Enter customer" onsuccess="reloadHistoryCustomerTable();" />--}%
%{----}%
%{--<g:link controller="customer" action="show" id="${historyCustomerInstance?.customer?.id}">${historyCustomerInstance?.customer?.encodeAsHTML()}</g:link>--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%

<g:if test="${historyCustomerInstance?.t182ID}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182ID-label" class="property-label"><g:message
                    code="historyCustomer.t182ID.label" default="T182 ID"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182ID-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182ID"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182ID" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182ID"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.company}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="company-label" class="property-label"><g:message
                    code="historyCustomer.company.label" default="Company"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="company-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="company"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter company" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:link controller="company" action="show"
                    id="${historyCustomerInstance?.company?.id}">${historyCustomerInstance?.company?.encodeAsHTML()}</g:link>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.peranCustomer}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="peranCustomer-label" class="property-label"><g:message
                    code="historyCustomer.peranCustomer.label" default="Peran Customer"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="peranCustomer-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="peranCustomer"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter peranCustomer" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:link controller="peranCustomer" action="show"
                    id="${historyCustomerInstance?.peranCustomer?.id}">${historyCustomerInstance?.peranCustomer?.encodeAsHTML()}</g:link>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182GelarD}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182GelarD-label" class="property-label"><g:message
                    code="historyCustomer.t182GelarD.label" default="T182 Gelar D"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182GelarD-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182GelarD"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182GelarD" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182GelarD"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182NamaDepan}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182NamaDepan-label" class="property-label"><g:message
                    code="historyCustomer.t182NamaDepan.label" default="T182 Nama Depan"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182NamaDepan-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182NamaDepan"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182NamaDepan" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182NamaDepan"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182NamaBelakang}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182NamaBelakang-label" class="property-label"><g:message
                    code="historyCustomer.t182NamaBelakang.label" default="T182 Nama Belakang"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182NamaBelakang-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182NamaBelakang"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182NamaBelakang" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182NamaBelakang"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182GelarB}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182GelarB-label" class="property-label"><g:message
                    code="historyCustomer.t182GelarB.label" default="T182 Gelar B"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182GelarB-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182GelarB"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182GelarB" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182GelarB"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182JenisKelamin}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182JenisKelamin-label" class="property-label"><g:message
                    code="historyCustomer.t182JenisKelamin.label" default="T182 Jenis Kelamin"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182JenisKelamin-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182JenisKelamin"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182JenisKelamin" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182JenisKelamin"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182TglLahir}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182TglLahir-label" class="property-label"><g:message
                    code="historyCustomer.t182TglLahir.label" default="T182 Tgl Lahir"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182TglLahir-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182TglLahir"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182TglLahir" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:formatDate date="${historyCustomerInstance?.t182TglLahir}"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.jenisCustomer}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="jenisCustomer-label" class="property-label"><g:message
                    code="historyCustomer.jenisCustomer.label" default="Jenis Customer"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="jenisCustomer-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="jenisCustomer"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter jenisCustomer" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="jenisCustomer"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182NPWP}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182NPWP-label" class="property-label"><g:message
                    code="historyCustomer.t182NPWP.label" default="T182 NPWP"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182NPWP-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182NPWP"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182NPWP" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182NPWP"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182NoFakturPajakStd}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182NoFakturPajakStd-label" class="property-label"><g:message
                    code="historyCustomer.t182NoFakturPajakStd.label" default="T182 No Faktur Pajak Std"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182NoFakturPajakStd-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182NoFakturPajakStd"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182NoFakturPajakStd" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182NoFakturPajakStd"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.jenisIdCard}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="jenisIdCard-label" class="property-label"><g:message
                    code="historyCustomer.jenisIdCard.label" default="Jenis Id Card"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="jenisIdCard-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="jenisIdCard"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter jenisIdCard" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:link controller="jenisIdCard" action="show"
                    id="${historyCustomerInstance?.jenisIdCard?.id}">${historyCustomerInstance?.jenisIdCard?.encodeAsHTML()}</g:link>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182NomorIDCard}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182NomorIDCard-label" class="property-label"><g:message
                    code="historyCustomer.t182NomorIDCard.label" default="T182 Nomor IDC ard"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182NomorIDCard-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182NomorIDCard"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182NomorIDCard" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182NomorIDCard"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.agama}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="agama-label" class="property-label"><g:message
                    code="historyCustomer.agama.label" default="Agama"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="agama-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="agama"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter agama" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:link controller="agama" action="show"
                    id="${historyCustomerInstance?.agama?.id}">${historyCustomerInstance?.agama?.encodeAsHTML()}</g:link>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.nikah}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="nikah-label" class="property-label"><g:message
                    code="historyCustomer.nikah.label" default="Nikah"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="nikah-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="nikah"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter nikah" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:link controller="nikah" action="show"
                    id="${historyCustomerInstance?.nikah?.id}">${historyCustomerInstance?.nikah?.encodeAsHTML()}</g:link>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182NoTelpRumah}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182NoTelpRumah-label" class="property-label"><g:message
                    code="historyCustomer.t182NoTelpRumah.label" default="T182 No Telp Rumah"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182NoTelpRumah-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182NoTelpRumah"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182NoTelpRumah" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182NoTelpRumah"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182NoTelpKantor}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182NoTelpKantor-label" class="property-label"><g:message
                    code="historyCustomer.t182NoTelpKantor.label" default="T182 No Telp Kantor"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182NoTelpKantor-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182NoTelpKantor"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182NoTelpKantor" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182NoTelpKantor"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182NoFax}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182NoFax-label" class="property-label"><g:message
                    code="historyCustomer.t182NoFax.label" default="T182 No Fax"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182NoFax-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182NoFax"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182NoFax" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182NoFax"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182NoHp}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182NoHp-label" class="property-label"><g:message
                    code="historyCustomer.t182NoHp.label" default="T182 No Hp"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182NoHp-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182NoHp"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182NoHp" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182NoHp"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182Email}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182Email-label" class="property-label"><g:message
                    code="historyCustomer.t182Email.label" default="T182 Email"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182Email-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182Email"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182Email" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182Email"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182StaTerimaMRS}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182StaTerimaMRS-label" class="property-label"><g:message
                    code="historyCustomer.t182StaTerimaMRS.label" default="T182 Sta Terima MRS"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182StaTerimaMRS-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182StaTerimaMRS"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182StaTerimaMRS" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:formatBoolean boolean="${historyCustomerInstance?.t182StaTerimaMRS}"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182Alamat}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182Alamat-label" class="property-label"><g:message
                    code="historyCustomer.t182Alamat.label" default="T182 Alamat"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182Alamat-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182Alamat"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182Alamat" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182Alamat"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182RT}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182RT-label" class="property-label"><g:message
                    code="historyCustomer.t182RT.label" default="T182 RT"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182RT-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182RT"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182RT" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182RT"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182RW}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182RW-label" class="property-label"><g:message
                    code="historyCustomer.t182RW.label" default="T182 RW"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182RW-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182RW"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182RW" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182RW"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.kelurahan}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="kelurahan-label" class="property-label"><g:message
                    code="historyCustomer.kelurahan.label" default="Kelurahan"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="kelurahan-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="kelurahan"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter kelurahan" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:link controller="kelurahan" action="show"
                    id="${historyCustomerInstance?.kelurahan?.id}">${historyCustomerInstance?.kelurahan?.encodeAsHTML()}</g:link>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.kecamatan}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="kecamatan-label" class="property-label"><g:message
                    code="historyCustomer.kecamatan.label" default="Kecamatan"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="kecamatan-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="kecamatan"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter kecamatan" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:link controller="kecamatan" action="show"
                    id="${historyCustomerInstance?.kecamatan?.id}">${historyCustomerInstance?.kecamatan?.encodeAsHTML()}</g:link>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.kabKota}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="kabKota-label" class="property-label"><g:message
                    code="historyCustomer.kabKota.label" default="Kab Kota"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="kabKota-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="kabKota"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter kabKota" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:link controller="kabKota" action="show"
                    id="${historyCustomerInstance?.kabKota?.id}">${historyCustomerInstance?.kabKota?.encodeAsHTML()}</g:link>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.provinsi}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="provinsi-label" class="property-label"><g:message
                    code="historyCustomer.provinsi.label" default="Provinsi"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="provinsi-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="provinsi"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter provinsi" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:link controller="provinsi" action="show"
                    id="${historyCustomerInstance?.provinsi?.id}">${historyCustomerInstance?.provinsi?.encodeAsHTML()}</g:link>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182KodePos}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182KodePos-label" class="property-label"><g:message
                    code="historyCustomer.t182KodePos.label" default="T182 Kode Pos"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182KodePos-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182KodePos"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182KodePos" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182KodePos"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182Foto}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182Foto-label" class="property-label"><g:message
                    code="historyCustomer.t182Foto.label" default="T182 Foto"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182Foto-label">
            <img width="200px" src="data:jpg;base64,${new String(new Base64().encode(historyCustomerInstance.t182Foto), "UTF-8")}"/>
        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182AlamatNPWP}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182AlamatNPWP-label" class="property-label"><g:message
                    code="historyCustomer.t182AlamatNPWP.label" default="T182 Alamat NPWP"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182AlamatNPWP-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182AlamatNPWP"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182AlamatNPWP" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182AlamatNPWP"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182RTNPWP}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182RTNPWP-label" class="property-label"><g:message
                    code="historyCustomer.t182RTNPWP.label" default="T182 RTNPWP"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182RTNPWP-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182RTNPWP"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182RTNPWP" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182RTNPWP"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182RWNPWP}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182RWNPWP-label" class="property-label"><g:message
                    code="historyCustomer.t182RWNPWP.label" default="T182 RWNPWP"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182RWNPWP-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182RWNPWP"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182RWNPWP" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182RWNPWP"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.kelurahan2}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="kelurahan2-label" class="property-label"><g:message
                    code="historyCustomer.kelurahan2.label" default="Kelurahan2"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="kelurahan2-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="kelurahan2"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter kelurahan2" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:link controller="kelurahan" action="show"
                    id="${historyCustomerInstance?.kelurahan2?.id}">${historyCustomerInstance?.kelurahan2?.encodeAsHTML()}</g:link>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.kecamatan2}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="kecamatan2-label" class="property-label"><g:message
                    code="historyCustomer.kecamatan2.label" default="Kecamatan2"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="kecamatan2-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="kecamatan2"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter kecamatan2" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:link controller="kecamatan" action="show"
                    id="${historyCustomerInstance?.kecamatan2?.id}">${historyCustomerInstance?.kecamatan2?.encodeAsHTML()}</g:link>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.kabKota2}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="kabKota2-label" class="property-label"><g:message
                    code="historyCustomer.kabKota2.label" default="Kab Kota2"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="kabKota2-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="kabKota2"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter kabKota2" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:link controller="kabKota" action="show"
                    id="${historyCustomerInstance?.kabKota2?.id}">${historyCustomerInstance?.kabKota2?.encodeAsHTML()}</g:link>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.provinsi2}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="provinsi2-label" class="property-label"><g:message
                    code="historyCustomer.provinsi2.label" default="Provinsi2"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="provinsi2-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="provinsi2"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter provinsi2" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:link controller="provinsi" action="show"
                    id="${historyCustomerInstance?.provinsi2?.id}">${historyCustomerInstance?.provinsi2?.encodeAsHTML()}</g:link>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182KodePosNPWP}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182KodePosNPWP-label" class="property-label"><g:message
                    code="historyCustomer.t182KodePosNPWP.label" default="T182 Kode Pos NPWP"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182KodePosNPWP-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182KodePosNPWP"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182KodePosNPWP" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182KodePosNPWP"/>

        </span></td>

    </tr>
</g:if>

%{--<g:if test="${historyCustomerInstance?.t182StaCompany}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="t182StaCompany-label" class="property-label"><g:message--}%
%{--code="historyCustomer.t182StaCompany.label" default="T182 Sta Company" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="t182StaCompany-label">--}%
%{--<ba:editableValue--}%
%{--bean="${historyCustomerInstance}" field="t182StaCompany"--}%
%{--url="${request.contextPath}/HistoryCustomer/updatefield" type="text"--}%
%{--title="Enter t182StaCompany" onsuccess="reloadHistoryCustomerTable();" />--}%
%{----}%
%{--<g:fieldValue bean="${historyCustomerInstance}" field="t182StaCompany"/>--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%
%{----}%
%{--<g:if test="${historyCustomerInstance?.t182HMinusKirimSMS}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="t182HMinusKirimSMS-label" class="property-label"><g:message--}%
%{--code="historyCustomer.t182HMinusKirimSMS.label" default="T182 HM inus Kirim SMS" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="t182HMinusKirimSMS-label">--}%
%{--<ba:editableValue--}%
%{--bean="${historyCustomerInstance}" field="t182HMinusKirimSMS"--}%
%{--url="${request.contextPath}/HistoryCustomer/updatefield" type="text"--}%
%{--title="Enter t182HMinusKirimSMS" onsuccess="reloadHistoryCustomerTable();" />--}%
%{----}%
%{--<g:fieldValue bean="${historyCustomerInstance}" field="t182HMinusKirimSMS"/>--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%

<g:if test="${historyCustomerInstance?.t182WebUserName}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182WebUserName-label" class="property-label"><g:message
                    code="historyCustomer.t182WebUserName.label" default="T182 Web User Name"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182WebUserName-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182WebUserName"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182WebUserName" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182WebUserName"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${historyCustomerInstance?.t182WebPassword}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="t182WebPassword-label" class="property-label"><g:message
                    code="historyCustomer.t182WebPassword.label" default="T182 Web Password"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="t182WebPassword-label">
            %{--<ba:editableValue
                    bean="${historyCustomerInstance}" field="t182WebPassword"
                    url="${request.contextPath}/HistoryCustomer/updatefield" type="text"
                    title="Enter t182WebPassword" onsuccess="reloadHistoryCustomerTable();" />--}%

            <g:fieldValue bean="${historyCustomerInstance}" field="t182WebPassword"/>

        </span></td>

    </tr>
</g:if>

%{--<g:if test="${historyCustomerInstance?.t182Ket}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="t182Ket-label" class="property-label"><g:message--}%
%{--code="historyCustomer.t182Ket.label" default="T182 Ket" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="t182Ket-label">--}%
%{--<ba:editableValue--}%
%{--bean="${historyCustomerInstance}" field="t182Ket"--}%
%{--url="${request.contextPath}/HistoryCustomer/updatefield" type="text"--}%
%{--title="Enter t182Ket" onsuccess="reloadHistoryCustomerTable();" />--}%
%{----}%
%{--<g:fieldValue bean="${historyCustomerInstance}" field="t182Ket"/>--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%
%{----}%
%{--<g:if test="${historyCustomerInstance?.t182xNamaUser}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="t182xNamaUser-label" class="property-label"><g:message--}%
%{--code="historyCustomer.t182xNamaUser.label" default="T182x Nama User" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="t182xNamaUser-label">--}%
%{--<ba:editableValue--}%
%{--bean="${historyCustomerInstance}" field="t182xNamaUser"--}%
%{--url="${request.contextPath}/HistoryCustomer/updatefield" type="text"--}%
%{--title="Enter t182xNamaUser" onsuccess="reloadHistoryCustomerTable();" />--}%
%{----}%
%{--<g:fieldValue bean="${historyCustomerInstance}" field="t182xNamaUser"/>--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%
%{----}%
%{--<g:if test="${historyCustomerInstance?.t182xNamaDivisi}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="t182xNamaDivisi-label" class="property-label"><g:message--}%
%{--code="historyCustomer.t182xNamaDivisi.label" default="T182x Nama Divisi" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="t182xNamaDivisi-label">--}%
%{--<ba:editableValue--}%
%{--bean="${historyCustomerInstance}" field="t182xNamaDivisi"--}%
%{--url="${request.contextPath}/HistoryCustomer/updatefield" type="text"--}%
%{--title="Enter t182xNamaDivisi" onsuccess="reloadHistoryCustomerTable();" />--}%
%{----}%
%{--<g:fieldValue bean="${historyCustomerInstance}" field="t182xNamaDivisi"/>--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%
%{----}%
%{--<g:if test="${historyCustomerInstance?.t182TglTransaksi}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="t182TglTransaksi-label" class="property-label"><g:message--}%
%{--code="historyCustomer.t182TglTransaksi.label" default="T182 Tgl Transaksi" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="t182TglTransaksi-label">--}%
%{--<ba:editableValue--}%
%{--bean="${historyCustomerInstance}" field="t182TglTransaksi"--}%
%{--url="${request.contextPath}/HistoryCustomer/updatefield" type="text"--}%
%{--title="Enter t182TglTransaksi" onsuccess="reloadHistoryCustomerTable();" />--}%
%{----}%
%{--<g:formatDate date="${historyCustomerInstance?.t182TglTransaksi}" />--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%
%{----}%
%{--<g:if test="${historyCustomerInstance?.staDel}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="staDel-label" class="property-label"><g:message--}%
%{--code="historyCustomer.staDel.label" default="Sta Del" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="staDel-label">--}%
%{--<ba:editableValue--}%
%{--bean="${historyCustomerInstance}" field="staDel"--}%
%{--url="${request.contextPath}/HistoryCustomer/updatefield" type="text"--}%
%{--title="Enter staDel" onsuccess="reloadHistoryCustomerTable();" />--}%
%{----}%
%{--<g:fieldValue bean="${historyCustomerInstance}" field="staDel"/>--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%
%{----}%
%{--<g:if test="${historyCustomerInstance?.createdBy}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="createdBy-label" class="property-label"><g:message--}%
%{--code="historyCustomer.createdBy.label" default="Created By" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="createdBy-label">--}%
%{--<ba:editableValue--}%
%{--bean="${historyCustomerInstance}" field="createdBy"--}%
%{--url="${request.contextPath}/HistoryCustomer/updatefield" type="text"--}%
%{--title="Enter createdBy" onsuccess="reloadHistoryCustomerTable();" />--}%
%{----}%
%{--<g:fieldValue bean="${historyCustomerInstance}" field="createdBy"/>--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%
%{----}%
%{--<g:if test="${historyCustomerInstance?.updatedBy}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="updatedBy-label" class="property-label"><g:message--}%
%{--code="historyCustomer.updatedBy.label" default="Updated By" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="updatedBy-label">--}%
%{--<ba:editableValue--}%
%{--bean="${historyCustomerInstance}" field="updatedBy"--}%
%{--url="${request.contextPath}/HistoryCustomer/updatefield" type="text"--}%
%{--title="Enter updatedBy" onsuccess="reloadHistoryCustomerTable();" />--}%
%{----}%
%{--<g:fieldValue bean="${historyCustomerInstance}" field="updatedBy"/>--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%
%{----}%
%{--<g:if test="${historyCustomerInstance?.lastUpdProcess}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="lastUpdProcess-label" class="property-label"><g:message--}%
%{--code="historyCustomer.lastUpdProcess.label" default="Last Upd Process" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="lastUpdProcess-label">--}%
%{--<ba:editableValue--}%
%{--bean="${historyCustomerInstance}" field="lastUpdProcess"--}%
%{--url="${request.contextPath}/HistoryCustomer/updatefield" type="text"--}%
%{--title="Enter lastUpdProcess" onsuccess="reloadHistoryCustomerTable();" />--}%
%{----}%
%{--<g:fieldValue bean="${historyCustomerInstance}" field="lastUpdProcess"/>--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%
%{----}%
%{--<g:if test="${historyCustomerInstance?.dateCreated}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="dateCreated-label" class="property-label"><g:message--}%
%{--code="historyCustomer.dateCreated.label" default="Date Created" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="dateCreated-label">--}%
%{--<ba:editableValue--}%
%{--bean="${historyCustomerInstance}" field="dateCreated"--}%
%{--url="${request.contextPath}/HistoryCustomer/updatefield" type="text"--}%
%{--title="Enter dateCreated" onsuccess="reloadHistoryCustomerTable();" />--}%
%{----}%
%{--<g:formatDate date="${historyCustomerInstance?.dateCreated}" />--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%
%{----}%
%{--<g:if test="${historyCustomerInstance?.lastUpdated}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="lastUpdated-label" class="property-label"><g:message--}%
%{--code="historyCustomer.lastUpdated.label" default="Last Updated" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="lastUpdated-label">--}%
%{--<ba:editableValue--}%
%{--bean="${historyCustomerInstance}" field="lastUpdated"--}%
%{--url="${request.contextPath}/HistoryCustomer/updatefield" type="text"--}%
%{--title="Enter lastUpdated" onsuccess="reloadHistoryCustomerTable();" />--}%
%{----}%
%{--<g:formatDate date="${historyCustomerInstance?.lastUpdated}" />--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%

</tbody>
</table>
<g:form class="form-horizontal">
    <fieldset class="buttons controls">
        <a class="btn cancel" href="javascript:void(0);"
           onclick="expandTableLayout();"><g:message
                code="default.button.cancel.label" default="Cancel"/></a>
        <g:remoteLink class="btn btn-primary edit" action="edit"
                      id="${(historyCustomerInstance?.id == null ? -1 : historyCustomerInstance?.id)}"
                      update="[success: 'historyCustomer-form', failure: 'historyCustomer-form']"
                      on404="alert('not found');">
            <g:message code="default.button.edit.label" default="Edit"/>
        </g:remoteLink>
        <ba:confirm id="delete" class="btn cancel"
                    message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
                    onsuccess="deleteHistoryCustomer('${(historyCustomerInstance?.id == null ? -1 : historyCustomerInstance?.id)}')"
                    label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
    </fieldset>
    <br>
    <br>
    <br>
</g:form>
</div>

</div>
</div>

<script>
    $(function () {
        $("#main_tabs").tabs();
    });
</script>

</body>
</html>
