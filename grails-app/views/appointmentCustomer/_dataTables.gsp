
<%@ page import="com.kombos.customerprofile.HistoryCustomer" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="historyCustomer_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.t182NamaDepan.label" default="T182 Nama Depan" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.t182NamaBelakang.label" default="T182 Nama Belakang" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;width: 40%;">
            <div><g:message code="historyCustomer.t182Alamat.label" default="T182 Alamat" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.t182NoTelpRumah.label" default="T182 No Telp Rumah" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.t182NoTelpKantor.label" default="T182 No Telp Kantor" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.t182NoHp.label" default="T182 No Hp" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.company.label" default="Company" /></div>
        </th>





		</tr>
		<tr>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_t182NamaDepan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t182NamaDepan" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t182NamaBelakang" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t182NamaBelakang" class="search_init" />
                </div>
            </th>


            <th style="border-top: none;padding: 5px;width: 40%;">
                <div id="filter_t182Alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" width="95%" name="search_t182Alamat" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t182NoTelpRumah" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" onkeyup="checkNumber(this);" name="search_t182NoTelpRumah" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t182NoTelpKantor" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" onkeyup="checkNumber(this);" name="search_t182NoTelpKantor" class="search_init" />
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_t182NoHp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" onkeyup="checkNumber(this);" name="search_t182NoHp" class="search_init" />
                </div>
            </th>





                <th style="border-top: none;padding: 5px;">
                    <div id="filter_company" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                        <input type="text" name="search_company" class="search_init" />
                    </div>
                </th>



		</tr>
	</thead>
</table>

<g:javascript>
var historyCustomerTable;
var reloadHistoryCustomerTable;
$(function(){
		
	reloadHistoryCustomerTable = function() {
		historyCustomerTable.fnDraw();
	}

	var recordsHistoryCustomerPerPage = [];//new Array();
    var anHistoryCustomerSelected;
    var jmlRecHistoryCustomerPerPage=0;
    var id;

	
	$('#search_t182TglLahir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t182TglLahir_day').val(newDate.getDate());
			$('#search_t182TglLahir_month').val(newDate.getMonth()+1);
			$('#search_t182TglLahir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			historyCustomerTable.fnDraw();	
	});

	

	$('#search_t182TglTransaksi').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t182TglTransaksi_day').val(newDate.getDate());
			$('#search_t182TglTransaksi_month').val(newDate.getMonth()+1);
			$('#search_t182TglTransaksi_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			historyCustomerTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	historyCustomerTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	historyCustomerTable = $('#historyCustomer_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsHistoryCustomer = $("#historyCustomer_datatables tbody .row-select");
            var jmlHistoryCustomerCek = 0;
            var nRow;
            var idRec;
            rsHistoryCustomer.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsHistoryCustomerPerPage[idRec]=="1"){
                    jmlHistoryCustomerCek = jmlHistoryCustomerCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsHistoryCustomerPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecHistoryCustomerPerPage = rsHistoryCustomer.length;
            if(jmlHistoryCustomerCek==jmlRecHistoryCustomerPerPage && jmlRecHistoryCustomerPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [




{
	"sName": "t182NamaDepan",
	"mDataProp": "t182NamaDepan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		/*return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';*/
        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="javascript:void(0);" onclick="edit(' + row['id'] + ');">' + data + '</a>';

	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t182NamaBelakang",
	"mDataProp": "t182NamaBelakang",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "t182Alamat",
	"mDataProp": "t182Alamat",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "t182NoTelpRumah",
	"mDataProp": "t182NoTelpRumah",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t182NoTelpKantor",
	"mDataProp": "t182NoTelpKantor",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t182NoHp",
	"mDataProp": "t182NoHp",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "company",
	"mDataProp": "company",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var customer = $('#filter_customer input').val();
						if(customer){
							aoData.push(
									{"name": 'sCriteria_customer', "value": customer}
							);
						}
	
						var t182ID = $('#filter_t182ID input').val();
						if(t182ID){
							aoData.push(
									{"name": 'sCriteria_t182ID', "value": t182ID}
							);
						}
	
						var company = $('#filter_company input').val();
						if(company){
							aoData.push(
									{"name": 'sCriteria_company', "value": company}
							);
						}
	
						var peranCustomer = $('#filter_peranCustomer input').val();
						if(peranCustomer){
							aoData.push(
									{"name": 'sCriteria_peranCustomer', "value": peranCustomer}
							);
						}
	
						var t182GelarD = $('#filter_t182GelarD input').val();
						if(t182GelarD){
							aoData.push(
									{"name": 'sCriteria_t182GelarD', "value": t182GelarD}
							);
						}
	
						var t182NamaDepan = $('#filter_t182NamaDepan input').val();
						if(t182NamaDepan){
							aoData.push(
									{"name": 'sCriteria_t182NamaDepan', "value": t182NamaDepan}
							);
						}
	
						var t182NamaBelakang = $('#filter_t182NamaBelakang input').val();
						if(t182NamaBelakang){
							aoData.push(
									{"name": 'sCriteria_t182NamaBelakang', "value": t182NamaBelakang}
							);
						}
	
						var t182GelarB = $('#filter_t182GelarB input').val();
						if(t182GelarB){
							aoData.push(
									{"name": 'sCriteria_t182GelarB', "value": t182GelarB}
							);
						}
	
						var t182JenisKelamin = $('#filter_t182JenisKelamin input').val();
						if(t182JenisKelamin){
							aoData.push(
									{"name": 'sCriteria_t182JenisKelamin', "value": t182JenisKelamin}
							);
						}

						var t182TglLahir = $('#search_t182TglLahir').val();
						var t182TglLahirDay = $('#search_t182TglLahir_day').val();
						var t182TglLahirMonth = $('#search_t182TglLahir_month').val();
						var t182TglLahirYear = $('#search_t182TglLahir_year').val();
						
						if(t182TglLahir){
							aoData.push(
									{"name": 'sCriteria_t182TglLahir', "value": "date.struct"},
									{"name": 'sCriteria_t182TglLahir_dp', "value": t182TglLahir},
									{"name": 'sCriteria_t182TglLahir_day', "value": t182TglLahirDay},
									{"name": 'sCriteria_t182TglLahir_month', "value": t182TglLahirMonth},
									{"name": 'sCriteria_t182TglLahir_year', "value": t182TglLahirYear}
							);
						}
	
						var jenisCustomer = $('#filter_jenisCustomer input').val();
						if(jenisCustomer){
							aoData.push(
									{"name": 'sCriteria_jenisCustomer', "value": jenisCustomer}
							);
						}
	
						var t182NPWP = $('#filter_t182NPWP input').val();
						if(t182NPWP){
							aoData.push(
									{"name": 'sCriteria_t182NPWP', "value": t182NPWP}
							);
						}
	
						var t182NoFakturPajakStd = $('#filter_t182NoFakturPajakStd input').val();
						if(t182NoFakturPajakStd){
							aoData.push(
									{"name": 'sCriteria_t182NoFakturPajakStd', "value": t182NoFakturPajakStd}
							);
						}
	
						var jenisIdCard = $('#filter_jenisIdCard input').val();
						if(jenisIdCard){
							aoData.push(
									{"name": 'sCriteria_jenisIdCard', "value": jenisIdCard}
							);
						}
	
						var t182NomorIDCard = $('#filter_t182NomorIDCard input').val();
						if(t182NomorIDCard){
							aoData.push(
									{"name": 'sCriteria_t182NomorIDCard', "value": t182NomorIDCard}
							);
						}
	
						var agama = $('#filter_agama input').val();
						if(agama){
							aoData.push(
									{"name": 'sCriteria_agama', "value": agama}
							);
						}
	
						var nikah = $('#filter_nikah input').val();
						if(nikah){
							aoData.push(
									{"name": 'sCriteria_nikah', "value": nikah}
							);
						}
	
						var t182NoTelpRumah = $('#filter_t182NoTelpRumah input').val();
						if(t182NoTelpRumah){
							aoData.push(
									{"name": 'sCriteria_t182NoTelpRumah', "value": t182NoTelpRumah}
							);
						}
	
						var t182NoTelpKantor = $('#filter_t182NoTelpKantor input').val();
						if(t182NoTelpKantor){
							aoData.push(
									{"name": 'sCriteria_t182NoTelpKantor', "value": t182NoTelpKantor}
							);
						}
	
						var t182NoFax = $('#filter_t182NoFax input').val();
						if(t182NoFax){
							aoData.push(
									{"name": 'sCriteria_t182NoFax', "value": t182NoFax}
							);
						}
	
						var t182NoHp = $('#filter_t182NoHp input').val();
						if(t182NoHp){
							aoData.push(
									{"name": 'sCriteria_t182NoHp', "value": t182NoHp}
							);
						}
	
						var t182Email = $('#filter_t182Email input').val();
						if(t182Email){
							aoData.push(
									{"name": 'sCriteria_t182Email', "value": t182Email}
							);
						}
	
						var t182StaTerimaMRS = $('#filter_t182StaTerimaMRS input').val();
						if(t182StaTerimaMRS){
							aoData.push(
									{"name": 'sCriteria_t182StaTerimaMRS', "value": t182StaTerimaMRS}
							);
						}
	
						var t182Alamat = $('#filter_t182Alamat input').val();
						if(t182Alamat){
							aoData.push(
									{"name": 'sCriteria_t182Alamat', "value": t182Alamat}
							);
						}
	
						var t182RT = $('#filter_t182RT input').val();
						if(t182RT){
							aoData.push(
									{"name": 'sCriteria_t182RT', "value": t182RT}
							);
						}
	
						var t182RW = $('#filter_t182RW input').val();
						if(t182RW){
							aoData.push(
									{"name": 'sCriteria_t182RW', "value": t182RW}
							);
						}
	
						var kelurahan = $('#filter_kelurahan input').val();
						if(kelurahan){
							aoData.push(
									{"name": 'sCriteria_kelurahan', "value": kelurahan}
							);
						}
	
						var kecamatan = $('#filter_kecamatan input').val();
						if(kecamatan){
							aoData.push(
									{"name": 'sCriteria_kecamatan', "value": kecamatan}
							);
						}
	
						var kabKota = $('#filter_kabKota input').val();
						if(kabKota){
							aoData.push(
									{"name": 'sCriteria_kabKota', "value": kabKota}
							);
						}
	
						var provinsi = $('#filter_provinsi input').val();
						if(provinsi){
							aoData.push(
									{"name": 'sCriteria_provinsi', "value": provinsi}
							);
						}
	
						var t182KodePos = $('#filter_t182KodePos input').val();
						if(t182KodePos){
							aoData.push(
									{"name": 'sCriteria_t182KodePos', "value": t182KodePos}
							);
						}
	
						var t182Foto = $('#filter_t182Foto input').val();
						if(t182Foto){
							aoData.push(
									{"name": 'sCriteria_t182Foto', "value": t182Foto}
							);
						}
	
						var t182AlamatNPWP = $('#filter_t182AlamatNPWP input').val();
						if(t182AlamatNPWP){
							aoData.push(
									{"name": 'sCriteria_t182AlamatNPWP', "value": t182AlamatNPWP}
							);
						}
	
						var t182RTNPWP = $('#filter_t182RTNPWP input').val();
						if(t182RTNPWP){
							aoData.push(
									{"name": 'sCriteria_t182RTNPWP', "value": t182RTNPWP}
							);
						}
	
						var t182RWNPWP = $('#filter_t182RWNPWP input').val();
						if(t182RWNPWP){
							aoData.push(
									{"name": 'sCriteria_t182RWNPWP', "value": t182RWNPWP}
							);
						}
	
						var kelurahan2 = $('#filter_kelurahan2 input').val();
						if(kelurahan2){
							aoData.push(
									{"name": 'sCriteria_kelurahan2', "value": kelurahan2}
							);
						}
	
						var kecamatan2 = $('#filter_kecamatan2 input').val();
						if(kecamatan2){
							aoData.push(
									{"name": 'sCriteria_kecamatan2', "value": kecamatan2}
							);
						}
	
						var kabKota2 = $('#filter_kabKota2 input').val();
						if(kabKota2){
							aoData.push(
									{"name": 'sCriteria_kabKota2', "value": kabKota2}
							);
						}
	
						var provinsi2 = $('#filter_provinsi2 input').val();
						if(provinsi2){
							aoData.push(
									{"name": 'sCriteria_provinsi2', "value": provinsi2}
							);
						}
						
						<g:if test="${noPolisiDepan && noPolisiTengah && noPolisiBelakang}">
							aoData.push(
									{"name": 'sNoPolisiDepan', "value": "${noPolisiDepan}"}
							);
							aoData.push(
									{"name": 'sNoPolisiTengah', "value": "${noPolisiTengah}"}
							);
                            aoData.push(
									{"name": 'sNoPolisiBelakang', "value": "${noPolisiBelakang}"}
							);
						</g:if>
	
						var t182KodePosNPWP = $('#filter_t182KodePosNPWP input').val();
						if(t182KodePosNPWP){
							aoData.push(
									{"name": 'sCriteria_t182KodePosNPWP', "value": t182KodePosNPWP}
							);
						}

						var t182WebUserName = $('#filter_t182WebUserName input').val();
						if(t182WebUserName){
							aoData.push(
									{"name": 'sCriteria_t182WebUserName', "value": t182WebUserName}
							);
						}
	
						var t182WebPassword = $('#filter_t182WebPassword input').val();
						if(t182WebPassword){
							aoData.push(
									{"name": 'sCriteria_t182WebPassword', "value": t182WebPassword}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
								if(json.iTotalDisplayRecords > 0){
                                    var first = $('#historyCustomer_datatables tbody tr').first();
                                    if(first){
                                        first.toggleClass('row_selected');
                                        var id = first.find("input:hidden").val();
                                        //edit(id);
                                    }
								}
							   },
							"complete": function () {
							   }
						});
		}
	});
	$('.select-all').click(function(e) {

        $("#historyCustomer_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsHistoryCustomerPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsHistoryCustomerPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#historyCustomer_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsHistoryCustomerPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anHistoryCustomerSelected = historyCustomerTable.$('tr.row_selected');
            if(jmlRecHistoryCustomerPerPage == anHistoryCustomerSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsHistoryCustomerPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
