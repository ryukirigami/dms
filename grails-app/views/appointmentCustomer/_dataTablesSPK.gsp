
<%@ page import="com.kombos.customerprofile.SPKAsuransi" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="addSPK_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable fixed" style="table-layout: fixed; width: 550px">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="customer.t193NomorSPK.label" default="Nomor SPK Asuransi Tambahan" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="customer.t193TanggalSPKAdd.label" default="Tanggal SPK Asuransi Tambahan" /></div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var AddSPKTable;
var reloadAddSPKTable;
$(function(){
	reloadAddSPKTable = function() {
		AddSPKTable.fnDraw();
	}


	AddSPKTable = $('#addSPK_datatables').dataTable({
		"sScrollX": "95%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		bFilter: false,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            return nRow;
        },
		"bSort": true,
		"bProcessing": true,
		"bDestroy":true,
		"iDisplayLength" : maxLineDisplay,
		"aoColumns": [



{
	"sName": "",
	"mDataProp": "t193NomorSPKTambah",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"><input type="hidden" value="'+data+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"275px",
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "t193TanggalSPKTambah",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"275px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
		}
	});

});
</g:javascript>



