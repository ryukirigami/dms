<%@ page import="com.kombos.baseapp.Approval" %>



<div class="control-group fieldcontain ${hasErrors(bean: approvalInstance, field: 'approvalRemark', 'error')} ">
	<label class="control-label" for="approvalRemark">
		<g:message code="approval.approvalRemark.label" default="Approval Remark" />
		
	</label>
	<div class="controls">
	<g:textField name="approvalRemark" value="${approvalInstance?.approvalRemark}" />
	</div>
</div>
<g:javascript>
    document.getElementById("approvalRemark").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: approvalInstance, field: 'approvalStatus', 'error')} ">
	<label class="control-label" for="approvalStatus">
		<g:message code="approval.approvalStatus.label" default="Approval Status" />
		
	</label>
	<div class="controls">
	<g:select name="approvalStatus" from="${com.kombos.baseapp.ApprovalStatus?.values()}" keys="${com.kombos.baseapp.ApprovalStatus.values()*.name()}" required="" value="${approvalInstance?.approvalStatus?.name()}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: approvalInstance, field: 'approvedBy', 'error')} ">
	<label class="control-label" for="approvedBy">
		<g:message code="approval.approvedBy.label" default="Approved By" />
		
	</label>
	<div class="controls">
	<g:textField name="approvedBy" value="${approvalInstance?.approvedBy}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: approvalInstance, field: 'approvedOn', 'error')} ">
	<label class="control-label" for="approvedOn">
		<g:message code="approval.approvedOn.label" default="Approved On" />
		
	</label>
	<div class="controls">
	<g:datePicker name="approvedOn" precision="day" value="${approvalInstance?.approvedOn}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: approvalInstance, field: 'module', 'error')} ">
	<label class="control-label" for="module">
		<g:message code="approval.module.label" default="Module" />
		
	</label>
	<div class="controls">
	<g:textField name="module" value="${approvalInstance?.module}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: approvalInstance, field: 'params', 'error')} ">
	<label class="control-label" for="params">
		<g:message code="approval.params.label" default="Params" />
		
	</label>
	<div class="controls">
	<g:textField name="params" value="${approvalInstance?.params}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: approvalInstance, field: 'requestBy', 'error')} ">
	<label class="control-label" for="requestBy">
		<g:message code="approval.requestBy.label" default="Request By" />
		
	</label>
	<div class="controls">
	<g:textField name="requestBy" value="${approvalInstance?.requestBy}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: approvalInstance, field: 'requestOn', 'error')} ">
	<label class="control-label" for="requestOn">
		<g:message code="approval.requestOn.label" default="Request On" />
		
	</label>
	<div class="controls">
	<g:datePicker name="requestOn" precision="day" value="${approvalInstance?.requestOn}" />
	</div>
</div>

