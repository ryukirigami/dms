
<%@ page import="com.kombos.baseapp.Approval" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'approval.label', default: 'Approval')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/approval/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/approval/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#approval-form').empty();
    	$('#approval-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#approval-table").hasClass("span12")){
   			$("#approval-table").toggleClass("span12 span5");
        }
        $("#approval-form").css("display","block");
       	shrinkApprovalTable(); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#approval-table").hasClass("span5")){
   			$("#approval-table").toggleClass("span5 span12");
   		}
        $("#approval-form").css("display","none");
       	expandApprovalTable(); 
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#approval-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/approval/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadApprovalTable();
    		}
		});
		
   	}

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
<%--		<ul class="nav pull-right">--%>
<%--			<li><a class="pull-right box-action" href="#"--%>
<%--				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i--%>
<%--					class="icon-plus"></i>&nbsp;&nbsp;--%>
<%--			</a></li>--%>
<%--			<li><a class="pull-right box-action" href="#"--%>
<%--				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i--%>
<%--					class="icon-remove"></i>&nbsp;&nbsp;--%>
<%--			</a></li>--%>
<%--			<li class="separator"></li>--%>
<%--		</ul>--%>
	</div>
	<div class="box">
		<div class="span12" id="approval-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="approval-form" style="display: none;"></div>
	</div>
</body>
</html>
