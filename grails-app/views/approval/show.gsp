

<%@ page import="com.kombos.baseapp.Approval" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'approval.label', default: 'Approval')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteApproval;

$(function(){ 
	deleteApproval=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/approval/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadApprovalTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-approval" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="approval"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${approvalInstance?.approvalRemark}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="approvalRemark-label" class="property-label"><g:message
					code="approval.approvalRemark.label" default="Approval Remark" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="approvalRemark-label">
						%{--<ba:editableValue
								bean="${approvalInstance}" field="approvalRemark"
								url="${request.contextPath}/Approval/updatefield" type="text"
								title="Enter approvalRemark" onsuccess="reloadApprovalTable();" />--}%
							
								<g:fieldValue bean="${approvalInstance}" field="approvalRemark"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalInstance?.approvalStatus}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="approvalStatus-label" class="property-label"><g:message
					code="approval.approvalStatus.label" default="Approval Status" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="approvalStatus-label">
						%{--<ba:editableValue
								bean="${approvalInstance}" field="approvalStatus"
								url="${request.contextPath}/Approval/updatefield" type="text"
								title="Enter approvalStatus" onsuccess="reloadApprovalTable();" />--}%
							
								<g:fieldValue bean="${approvalInstance}" field="approvalStatus"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalInstance?.approvedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="approvedBy-label" class="property-label"><g:message
					code="approval.approvedBy.label" default="Approved By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="approvedBy-label">
						%{--<ba:editableValue
								bean="${approvalInstance}" field="approvedBy"
								url="${request.contextPath}/Approval/updatefield" type="text"
								title="Enter approvedBy" onsuccess="reloadApprovalTable();" />--}%
							
								<g:fieldValue bean="${approvalInstance}" field="approvedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalInstance?.approvedOn}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="approvedOn-label" class="property-label"><g:message
					code="approval.approvedOn.label" default="Approved On" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="approvedOn-label">
						%{--<ba:editableValue
								bean="${approvalInstance}" field="approvedOn"
								url="${request.contextPath}/Approval/updatefield" type="text"
								title="Enter approvedOn" onsuccess="reloadApprovalTable();" />--}%
							
								<g:formatDate date="${approvalInstance?.approvedOn}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalInstance?.module}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="module-label" class="property-label"><g:message
					code="approval.module.label" default="Module" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="module-label">
						%{--<ba:editableValue
								bean="${approvalInstance}" field="module"
								url="${request.contextPath}/Approval/updatefield" type="text"
								title="Enter module" onsuccess="reloadApprovalTable();" />--}%
							
								<g:fieldValue bean="${approvalInstance}" field="module"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalInstance?.params}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="params-label" class="property-label"><g:message
					code="approval.params.label" default="Params" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="params-label">
						%{--<ba:editableValue
								bean="${approvalInstance}" field="params"
								url="${request.contextPath}/Approval/updatefield" type="text"
								title="Enter params" onsuccess="reloadApprovalTable();" />--}%
							
								<g:fieldValue bean="${approvalInstance}" field="params"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalInstance?.requestBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="requestBy-label" class="property-label"><g:message
					code="approval.requestBy.label" default="Request By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="requestBy-label">
						%{--<ba:editableValue
								bean="${approvalInstance}" field="requestBy"
								url="${request.contextPath}/Approval/updatefield" type="text"
								title="Enter requestBy" onsuccess="reloadApprovalTable();" />--}%
							
								<g:fieldValue bean="${approvalInstance}" field="requestBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalInstance?.requestOn}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="requestOn-label" class="property-label"><g:message
					code="approval.requestOn.label" default="Request On" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="requestOn-label">
						%{--<ba:editableValue
								bean="${approvalInstance}" field="requestOn"
								url="${request.contextPath}/Approval/updatefield" type="text"
								title="Enter requestOn" onsuccess="reloadApprovalTable();" />--}%
							
								<g:formatDate date="${approvalInstance?.requestOn}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
			</fieldset>
		</g:form>
	</div>
</body>
</html>
