
<%@ page import="com.kombos.parts.StokOPName" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'stokOPNameFreeze.label', default: 'Hasil Stok Opname')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			var staStockOpname = "${stokOpnameStatus}";
			$(function(){ 
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('stokOPNameFreeze');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('stokOPNameFreeze', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('stokOPNameFreeze', '${request.contextPath}/stokOPNameFreeze/massdelete', reloadStokOPNameFreezeTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('stokOPNameFreeze','${request.contextPath}/stokOPNameFreeze/show/'+id);
				};
				
				edit = function(id) {
					editInstance('stokOPNameFreeze','${request.contextPath}/stokOPNameFreeze/edit/'+id);
				};

                if(staStockOpname=="0"){
                    $("#freeze").prop('disabled', true);
				    $("#unfreeze").prop('disabled', false);
                }
				$('#freeze').click(function(){
				    $("#freeze").prop('disabled', true);
				    $("#unfreeze").prop('disabled', false);
				    $('#timer').show();
				    CreateTimer("timer", 10);
				});

				$('#unfreeze').click(function(){
				    $("#freeze").prop('disabled', false);
				    $("#unfreeze").prop('disabled', true);
				     $.ajax({
                        url:'${request.getContextPath()}/stokOPNameFreeze/unfreeze',
                        type: "POST",
                        success : function(data){
                            alert('Sistem Sudah Kembali Normal');
                            location.reload();
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });
                    $('#timer').hide();
				});

});
    var Timer;
    var TotalSeconds;

    function CreateTimer(TimerID, Time) {
        Timer = document.getElementById(TimerID);
        TotalSeconds = Time;
        UpdateTimer()
        window.setTimeout("Tick()", 1000);
    }

    function Tick() {
        TotalSeconds -= 1;
        if(parseInt(TotalSeconds)>=0){
            UpdateTimer()
            window.setTimeout("Tick()", 1000);
        }
        else{
            $.ajax({
                url:'${request.getContextPath()}/stokOPNameFreeze/freeze',
                type: "POST",
                success : function(data){
                    alert('Sistem berhasil dibekukan');
                     location.reload()
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                }
            });
        }
    }

    function UpdateTimer() {
        Timer.innerHTML = "Dalam "+TotalSeconds+" detik, sistem yang berhubungan dengan stok akan dibekukan sementara";
    }

</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="stokOPName.confirm.label" default="Konfirmasi Stock Opname" /></span>
		<ul class="nav pull-right">
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="stokOPNameFreeze-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <div id='timer' style="font-size: large"/>
            <table>
                <tr style="align-content: center;width: 1200px">
                    <td style="padding: 5px;align-content: center">
                        <g:field type="button" style="width:200px;height: 50px;" class="btn cancel"
                                 name="freeze" id="freeze" value="${message(code: 'stokOPName.freeze.button.label', default: 'Freeze System')}" />
                    </td>
                    <td style="padding: 5px;align-content: center">
                        <g:field type="button" disabled="true" style="width:200px;height: 50px;" class="btn cancel"
                                 name="unfreeze" id="unfreeze" value="${message(code: 'stokOPName.unfreeze.button.label', default: 'unFreeze System')}" />
                    </td>
                </tr>
            </table>

            <div style="align:center">
                <table>
                    <tr>
                        <td style="padding: 5px">
                            <g:field type="button" onclick="window.location.replace('#/inputHasilStokOpname')" style="padding: 5px"
                            class="btn cancel pull-right box-action" name="inputHasilStokOpname" id="inputHasilStokOpname" value="${message(code: 'stokopname.button.inputHasilStokOpname.label', default: 'Input Hasil Stok Opname')}" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
		<div class="span7" id="stokOPNameFreeze-form" style="display: none;"></div>
	</div>
</body>
</html>
