<%@ page import="com.kombos.parts.StokOPName" %>



<div class="control-group fieldcontain ${hasErrors(bean: stokOPNameFreezeInstance, field: 'goods', 'error')} ">
	<label class="control-label" for="goods">
		<g:message code="stokOPNameFreeze.goods.label" default="Goods" />

	</label>
	<div class="controls">
	<g:select id="goods" name="goods.id" from="${com.kombos.parts.KlasifikasiGoods.list()}" optionKey="id" required="" value="${stokOPNameFreezeInstance?.goods?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stokOPNameFreezeInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="stokOPNameFreeze.lastUpdProcess.label" default="Last Upd Process" />

	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${stokOPNameFreezeInstance?.lastUpdProcess}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stokOPNameFreezeInstance, field: 'location', 'error')} ">
	<label class="control-label" for="location">
		<g:message code="stokOPNameFreeze.location.label" default="Location" />

	</label>
	<div class="controls">
	<g:select id="location" name="location.id" from="${com.kombos.parts.Location.list()}" optionKey="id" required="" value="${stokOPNameFreezeInstance?.location?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stokOPNameFreezeInstance, field: 'staData', 'error')} ">
	<label class="control-label" for="staData">
		<g:message code="stokOPNameFreeze.staData.label" default="Sta Data" />

	</label>
	<div class="controls">
	<g:textField name="staData" value="${stokOPNameFreezeInstance?.staData}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stokOPNameFreezeInstance, field: 't132ID', 'error')} ">
	<label class="control-label" for="t132ID">
		<g:message code="stokOPNameFreeze.t132ID.label" default="T132 ID" />

	</label>
	<div class="controls">
	<g:textField name="t132ID" value="${stokOPNameFreezeInstance?.t132ID}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stokOPNameFreezeInstance, field: 't132Jumlah11', 'error')} ">
	<label class="control-label" for="t132Jumlah11">
		<g:message code="stokOPNameFreeze.t132Jumlah11.label" default="T132 Jumlah11" />

	</label>
	<div class="controls">
	<g:field type="number" name="t132Jumlah11" value="${stokOPNameFreezeInstance.t132Jumlah11}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stokOPNameFreezeInstance, field: 't132Jumlah12', 'error')} ">
	<label class="control-label" for="t132Jumlah12">
		<g:message code="stokOPNameFreeze.t132Jumlah12.label" default="T132 Jumlah12" />

	</label>
	<div class="controls">
	<g:field type="number" name="t132Jumlah12" value="${stokOPNameFreezeInstance.t132Jumlah12}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stokOPNameFreezeInstance, field: 't132Jumlah13', 'error')} ">
	<label class="control-label" for="t132Jumlah13">
		<g:message code="stokOPNameFreeze.t132Jumlah13.label" default="T132 Jumlah13" />

	</label>
	<div class="controls">
	<g:field type="number" name="t132Jumlah13" value="${stokOPNameFreezeInstance.t132Jumlah13}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stokOPNameFreezeInstance, field: 't132Jumlah1Stock', 'error')} ">
	<label class="control-label" for="t132Jumlah1Stock">
		<g:message code="stokOPNameFreeze.t132Jumlah1Stock.label" default="T132 Jumlah1 Stock" />

	</label>
	<div class="controls">
	<g:field type="number" name="t132Jumlah1Stock" value="${stokOPNameFreezeInstance.t132Jumlah1Stock}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stokOPNameFreezeInstance, field: 't132Jumlah21', 'error')} ">
	<label class="control-label" for="t132Jumlah21">
		<g:message code="stokOPNameFreeze.t132Jumlah21.label" default="T132 Jumlah21" />

	</label>
	<div class="controls">
	<g:field type="number" name="t132Jumlah21" value="${stokOPNameFreezeInstance.t132Jumlah21}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stokOPNameFreezeInstance, field: 't132Jumlah22', 'error')} ">
	<label class="control-label" for="t132Jumlah22">
		<g:message code="stokOPNameFreeze.t132Jumlah22.label" default="T132 Jumlah22" />

	</label>
	<div class="controls">
	<g:field type="number" name="t132Jumlah22" value="${stokOPNameFreezeInstance.t132Jumlah22}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stokOPNameFreezeInstance, field: 't132Jumlah23', 'error')} ">
	<label class="control-label" for="t132Jumlah23">
		<g:message code="stokOPNameFreeze.t132Jumlah23.label" default="T132 Jumlah23" />

	</label>
	<div class="controls">
	<g:field type="number" name="t132Jumlah23" value="${stokOPNameFreezeInstance.t132Jumlah23}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stokOPNameFreezeInstance, field: 't132Jumlah2Stock', 'error')} ">
	<label class="control-label" for="t132Jumlah2Stock">
		<g:message code="stokOPNameFreeze.t132Jumlah2Stock.label" default="T132 Jumlah2 Stock" />

	</label>
	<div class="controls">
	<g:field type="number" name="t132Jumlah2Stock" value="${stokOPNameFreezeInstance.t132Jumlah2Stock}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stokOPNameFreezeInstance, field: 't132StaUpdateStock', 'error')} ">
	<label class="control-label" for="t132StaUpdateStock">
		<g:message code="stokOPNameFreeze.t132StaUpdateStock.label" default="T132 Sta Update Stock" />

	</label>
	<div class="controls">
	<g:textField name="t132StaUpdateStock" value="${stokOPNameFreezeInstance?.t132StaUpdateStock}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stokOPNameFreezeInstance, field: 't132Tanggal', 'error')} ">
	<label class="control-label" for="t132Tanggal">
		<g:message code="stokOPNameFreeze.t132Tanggal.label" default="T132 Tanggal" />

	</label>
	<div class="controls">
	<ba:datePicker name="t132Tanggal" precision="day" value="${stokOPNameFreezeInstance?.t132Tanggal}" format="yyyy-MM-dd"/>
	</div>
</div>

