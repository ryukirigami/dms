
<%@ page import="com.kombos.parts.StokOPName" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="stokOPNameFreeze_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover" style="table-layout: fixed;">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.goods.kode.label" default="Kode Part" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.goods.nama.label" default="Nama Part" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.qty1.kode.label" default="Qty 1" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.goods.satuan1.label" default="Satuan 1" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.qty2.kode.label" default="Qty 2" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.goods.satuan2.label" default="Satuan 2" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.icc.nama.label" default="ICC" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.goods.lokasirak.label" default="Lokasi/Rak" /></div>
            </th>

		
		</tr>
	</thead>
</table>

<g:javascript>
var stokOPNameFreezeTable;
var reloadStokOPNameFreezeTable;
$(function(){
	
	reloadStokOPNameFreezeTable = function() {
		stokOPNameFreezeTable.fnDraw();
	}

	



$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	stokOPNameFreezeTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	stokOPNameFreezeTable = $('#stokOPNameFreeze_datatables').dataTable({
		"sScrollX": "1350px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "goods",
	"mDataProp": "kodegoods",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';//<a href="#" onclick="show('+row['id']+');">'+data+'
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "namagoods",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "qty1",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "satuangoods1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "qty2",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "satuangoods2",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,


{
	"sName": "goods",
	"mDataProp": "icc",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "lokasigoods",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var location = $('#filter_location input').val();
						if(location){
							aoData.push(
									{"name": 'sCriteria_location', "value": location}
							);
						}
	
						var staData = $('#filter_staData input').val();
						if(staData){
							aoData.push(
									{"name": 'sCriteria_staData', "value": staData}
							);
						}
	
						var t132ID = $('#filter_t132ID input').val();
						if(t132ID){
							aoData.push(
									{"name": 'sCriteria_t132ID', "value": t132ID}
							);
						}
	
						var t132Jumlah11 = $('#filter_t132Jumlah11 input').val();
						if(t132Jumlah11){
							aoData.push(
									{"name": 'sCriteria_t132Jumlah11', "value": t132Jumlah11}
							);
						}
	
						var t132Jumlah12 = $('#filter_t132Jumlah12 input').val();
						if(t132Jumlah12){
							aoData.push(
									{"name": 'sCriteria_t132Jumlah12', "value": t132Jumlah12}
							);
						}
	
						var t132Jumlah13 = $('#filter_t132Jumlah13 input').val();
						if(t132Jumlah13){
							aoData.push(
									{"name": 'sCriteria_t132Jumlah13', "value": t132Jumlah13}
							);
						}
	
						var t132Jumlah1Stock = $('#filter_t132Jumlah1Stock input').val();
						if(t132Jumlah1Stock){
							aoData.push(
									{"name": 'sCriteria_t132Jumlah1Stock', "value": t132Jumlah1Stock}
							);
						}
	
						var t132Jumlah21 = $('#filter_t132Jumlah21 input').val();
						if(t132Jumlah21){
							aoData.push(
									{"name": 'sCriteria_t132Jumlah21', "value": t132Jumlah21}
							);
						}
	
						var t132Jumlah22 = $('#filter_t132Jumlah22 input').val();
						if(t132Jumlah22){
							aoData.push(
									{"name": 'sCriteria_t132Jumlah22', "value": t132Jumlah22}
							);
						}
	
						var t132Jumlah23 = $('#filter_t132Jumlah23 input').val();
						if(t132Jumlah23){
							aoData.push(
									{"name": 'sCriteria_t132Jumlah23', "value": t132Jumlah23}
							);
						}
	
						var t132Jumlah2Stock = $('#filter_t132Jumlah2Stock input').val();
						if(t132Jumlah2Stock){
							aoData.push(
									{"name": 'sCriteria_t132Jumlah2Stock', "value": t132Jumlah2Stock}
							);
						}
	
						var t132StaUpdateStock = $('#filter_t132StaUpdateStock input').val();
						if(t132StaUpdateStock){
							aoData.push(
									{"name": 'sCriteria_t132StaUpdateStock', "value": t132StaUpdateStock}
							);
						}

						var t132Tanggal = $('#search_t132Tanggal').val();
						var t132TanggalDay = $('#search_t132Tanggal_day').val();
						var t132TanggalMonth = $('#search_t132Tanggal_month').val();
						var t132TanggalYear = $('#search_t132Tanggal_year').val();
						
						if(t132Tanggal){
							aoData.push(
									{"name": 'sCriteria_t132Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_t132Tanggal_dp', "value": t132Tanggal},
									{"name": 'sCriteria_t132Tanggal_day', "value": t132TanggalDay},
									{"name": 'sCriteria_t132Tanggal_month', "value": t132TanggalMonth},
									{"name": 'sCriteria_t132Tanggal_year', "value": t132TanggalYear}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
