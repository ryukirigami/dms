

<%@ page import="com.kombos.parts.StokOPName" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'stokOPNameFreeze.label', default: 'StokOPName')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteStokOPName;

$(function(){ 
	deleteStokOPName=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/stokOPNameFreeze/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadStokOPNameFreezeTable();
   				expandTableLayout('stokOPNameFreeze');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-stokOPNameFreeze" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="stokOPNameFreeze"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${stokOPNameFreezeInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="stokOPNameFreeze.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="createdBy"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:fieldValue bean="${stokOPNameFreezeInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="stokOPNameFreeze.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="dateCreated"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:formatDate date="${stokOPNameFreezeInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="stokOPNameFreeze.goods.label" default="Goods" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="goods"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter goods" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:link controller="klasifikasiGoods" action="show" id="${stokOPNameFreezeInstance?.goods?.id}">${stokOPNameFreezeInstance?.goods?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="stokOPNameFreeze.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="lastUpdProcess"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:fieldValue bean="${stokOPNameFreezeInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="stokOPNameFreeze.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="lastUpdated"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:formatDate date="${stokOPNameFreezeInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.location}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="location-label" class="property-label"><g:message
					code="stokOPNameFreeze.location.label" default="Location" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="location-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="location"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter location" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:link controller="location" action="show" id="${stokOPNameFreezeInstance?.location?.id}">${stokOPNameFreezeInstance?.location?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.staData}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staData-label" class="property-label"><g:message
					code="stokOPNameFreeze.staData.label" default="Sta Data" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staData-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="staData"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter staData" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:fieldValue bean="${stokOPNameFreezeInstance}" field="staData"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.t132ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t132ID-label" class="property-label"><g:message
					code="stokOPNameFreeze.t132ID.label" default="T132 ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t132ID-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="t132ID"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter t132ID" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:fieldValue bean="${stokOPNameFreezeInstance}" field="t132ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.t132Jumlah11}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t132Jumlah11-label" class="property-label"><g:message
					code="stokOPNameFreeze.t132Jumlah11.label" default="T132 Jumlah11" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t132Jumlah11-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="t132Jumlah11"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter t132Jumlah11" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:fieldValue bean="${stokOPNameFreezeInstance}" field="t132Jumlah11"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.t132Jumlah12}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t132Jumlah12-label" class="property-label"><g:message
					code="stokOPNameFreeze.t132Jumlah12.label" default="T132 Jumlah12" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t132Jumlah12-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="t132Jumlah12"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter t132Jumlah12" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:fieldValue bean="${stokOPNameFreezeInstance}" field="t132Jumlah12"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.t132Jumlah13}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t132Jumlah13-label" class="property-label"><g:message
					code="stokOPNameFreeze.t132Jumlah13.label" default="T132 Jumlah13" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t132Jumlah13-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="t132Jumlah13"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter t132Jumlah13" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:fieldValue bean="${stokOPNameFreezeInstance}" field="t132Jumlah13"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.t132Jumlah1Stock}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t132Jumlah1Stock-label" class="property-label"><g:message
					code="stokOPNameFreeze.t132Jumlah1Stock.label" default="T132 Jumlah1 Stock" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t132Jumlah1Stock-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="t132Jumlah1Stock"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter t132Jumlah1Stock" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:fieldValue bean="${stokOPNameFreezeInstance}" field="t132Jumlah1Stock"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.t132Jumlah21}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t132Jumlah21-label" class="property-label"><g:message
					code="stokOPNameFreeze.t132Jumlah21.label" default="T132 Jumlah21" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t132Jumlah21-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="t132Jumlah21"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter t132Jumlah21" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:fieldValue bean="${stokOPNameFreezeInstance}" field="t132Jumlah21"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.t132Jumlah22}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t132Jumlah22-label" class="property-label"><g:message
					code="stokOPNameFreeze.t132Jumlah22.label" default="T132 Jumlah22" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t132Jumlah22-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="t132Jumlah22"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter t132Jumlah22" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:fieldValue bean="${stokOPNameFreezeInstance}" field="t132Jumlah22"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.t132Jumlah23}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t132Jumlah23-label" class="property-label"><g:message
					code="stokOPNameFreeze.t132Jumlah23.label" default="T132 Jumlah23" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t132Jumlah23-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="t132Jumlah23"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter t132Jumlah23" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:fieldValue bean="${stokOPNameFreezeInstance}" field="t132Jumlah23"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.t132Jumlah2Stock}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t132Jumlah2Stock-label" class="property-label"><g:message
					code="stokOPNameFreeze.t132Jumlah2Stock.label" default="T132 Jumlah2 Stock" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t132Jumlah2Stock-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="t132Jumlah2Stock"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter t132Jumlah2Stock" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:fieldValue bean="${stokOPNameFreezeInstance}" field="t132Jumlah2Stock"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.t132StaUpdateStock}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t132StaUpdateStock-label" class="property-label"><g:message
					code="stokOPNameFreeze.t132StaUpdateStock.label" default="T132 Sta Update Stock" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t132StaUpdateStock-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="t132StaUpdateStock"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter t132StaUpdateStock" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:fieldValue bean="${stokOPNameFreezeInstance}" field="t132StaUpdateStock"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.t132Tanggal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t132Tanggal-label" class="property-label"><g:message
					code="stokOPNameFreeze.t132Tanggal.label" default="T132 Tanggal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t132Tanggal-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="t132Tanggal"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter t132Tanggal" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:formatDate date="${stokOPNameFreezeInstance?.t132Tanggal}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stokOPNameFreezeInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="stokOPNameFreeze.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${stokOPNameFreezeInstance}" field="updatedBy"
								url="${request.contextPath}/StokOPName/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadStokOPNameTable();" />--}%
							
								<g:fieldValue bean="${stokOPNameFreezeInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('stokOPNameFreeze');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${stokOPNameFreezeInstance?.id}"
					update="[success:'stokOPNameFreeze-form',failure:'stokOPNameFreeze-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteStokOPName('${stokOPNameFreezeInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
