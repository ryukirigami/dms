
<%@ page import="com.kombos.administrasi.InformasiPenting" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="informasiPenting_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="informasiPenting.m032Judul.label" default="Judul" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="informasiPenting.m032Tanggal.label" default="Tanggal" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="informasiPenting.m032UserUpload.label" default="Dibuat Oleh" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="informasiPenting.m032Konten.label" default="Konten" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="informasiPenting.m032Image1.label" default="Attachment" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="informasiPenting.m032StaTampil.label" default="M032 Sta Tampil" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="informasiPenting.m032Image2.label" default="M032 Image2" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="informasiPenting.m032Image3.label" default="M032 Image3" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="informasiPenting.m032Image4.label" default="M032 Image4" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="informasiPenting.m032Image5.label" default="M032 Image5" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="informasiPenting.companyDealer.label" default="Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="informasiPenting.m032ID.label" default="M032 ID" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="informasiPenting.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="informasiPenting.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="informasiPenting.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
		<tr>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_m032Judul" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m032Judul" class="search_init" />
                </div>
            </th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m032Tanggal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m032Tanggal" value="date.struct">
					<input type="hidden" name="search_m032Tanggal_day" id="search_m032Tanggal_day" value="">
					<input type="hidden" name="search_m032Tanggal_month" id="search_m032Tanggal_month" value="">
					<input type="hidden" name="search_m032Tanggal_year" id="search_m032Tanggal_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m032Tanggal_dp" value="" id="search_m032Tanggal" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m032UserUpload" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m032UserUpload" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m032Konten" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m032Konten" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m032Image1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m032Image1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m032StaTampil" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m032StaTampil" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m032Image2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m032Image2" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m032Image3" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m032Image3" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m032Image4" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m032Image4" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m032Image5" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m032Image5" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m032ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m032ID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var informasiPentingTable;
var reloadInformasiPentingTable;
$(function(){
	
	reloadInformasiPentingTable = function() {
		informasiPentingTable.fnDraw();
	}

	var recordsInformasiPentingperpage = [];//new Array();
    var anInformasiPentingSelected;
    var jmlRecInformasiPentingPerPage=0;
    var id;

	
	$('#search_m032Tanggal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m032Tanggal_day').val(newDate.getDate());
			$('#search_m032Tanggal_month').val(newDate.getMonth()+1);
			$('#search_m032Tanggal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			informasiPentingTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	informasiPentingTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	informasiPentingTable = $('#informasiPenting_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsInformasiPenting = $("#informasiPenting_datatables tbody .row-select");
            var jmlInformasiPentingCek = 0;
            var nRow;
            var idRec;
            rsInformasiPenting.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsInformasiPentingperpage[idRec]=="1"){
                    jmlInformasiPentingCek = jmlInformasiPentingCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsInformasiPentingperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecInformasiPentingPerPage = rsInformasiPenting.length;
            if(jmlInformasiPentingCek==jmlRecInformasiPentingPerPage && jmlRecInformasiPentingPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m032Judul",
	"mDataProp": "m032Judul",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m032Tanggal",
	"mDataProp": "m032Tanggal",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "m032UserUpload",
	"mDataProp": "m032UserUpload",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "m032Konten",
	"mDataProp": "m032Konten",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m032Image1",
	"mDataProp": "m032Image1",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m032StaTampil",
	"mDataProp": "m032StaTampil",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m032Image2",
	"mDataProp": "m032Image2",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m032Image3",
	"mDataProp": "m032Image3",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m032Image4",
	"mDataProp": "m032Image4",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m032Image5",
	"mDataProp": "m032Image5",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m032ID",
	"mDataProp": "m032ID",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m032Tanggal = $('#search_m032Tanggal').val();
						var m032TanggalDay = $('#search_m032Tanggal_day').val();
						var m032TanggalMonth = $('#search_m032Tanggal_month').val();
						var m032TanggalYear = $('#search_m032Tanggal_year').val();
						
						if(m032Tanggal){
							aoData.push(
									{"name": 'sCriteria_m032Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_m032Tanggal_dp', "value": m032Tanggal},
									{"name": 'sCriteria_m032Tanggal_day', "value": m032TanggalDay},
									{"name": 'sCriteria_m032Tanggal_month', "value": m032TanggalMonth},
									{"name": 'sCriteria_m032Tanggal_year', "value": m032TanggalYear}
							);
						}
	
						var m032UserUpload = $('#filter_m032UserUpload input').val();
						if(m032UserUpload){
							aoData.push(
									{"name": 'sCriteria_m032UserUpload', "value": m032UserUpload}
							);
						}
	
						var m032Judul = $('#filter_m032Judul input').val();
						if(m032Judul){
							aoData.push(
									{"name": 'sCriteria_m032Judul', "value": m032Judul}
							);
						}
	
						var m032Konten = $('#filter_m032Konten input').val();
						if(m032Konten){
							aoData.push(
									{"name": 'sCriteria_m032Konten', "value": m032Konten}
							);
						}
	
						var m032Image1 = $('#filter_m032Image1 input').val();
						if(m032Image1){
							aoData.push(
									{"name": 'sCriteria_m032Image1', "value": m032Image1}
							);
						}
	
						var m032StaTampil = $('#filter_m032StaTampil input').val();
						if(m032StaTampil){
							aoData.push(
									{"name": 'sCriteria_m032StaTampil', "value": m032StaTampil}
							);
						}
	
						var m032Image2 = $('#filter_m032Image2 input').val();
						if(m032Image2){
							aoData.push(
									{"name": 'sCriteria_m032Image2', "value": m032Image2}
							);
						}
	
						var m032Image3 = $('#filter_m032Image3 input').val();
						if(m032Image3){
							aoData.push(
									{"name": 'sCriteria_m032Image3', "value": m032Image3}
							);
						}
	
						var m032Image4 = $('#filter_m032Image4 input').val();
						if(m032Image4){
							aoData.push(
									{"name": 'sCriteria_m032Image4', "value": m032Image4}
							);
						}
	
						var m032Image5 = $('#filter_m032Image5 input').val();
						if(m032Image5){
							aoData.push(
									{"name": 'sCriteria_m032Image5', "value": m032Image5}
							);
						}
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var m032ID = $('#filter_m032ID input').val();
						if(m032ID){
							aoData.push(
									{"name": 'sCriteria_m032ID', "value": m032ID}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#informasiPenting_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsInformasiPentingperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsInformasiPentingperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#informasiPenting_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsInformasiPentingperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anInformasiPentingSelected = InformasiPentingTable.$('tr.row_selected');
            if(jmlRecInformasiPentingPerPage == anInformasiPentingSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsInformasiPentingperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
