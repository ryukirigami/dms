

<%@ page import="com.kombos.administrasi.InformasiPenting" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'informasiPenting.label', default: 'Informasi Penting')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteInformasiPenting;

$(function(){ 
	deleteInformasiPenting=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/informasiPenting/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadInformasiPentingTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-informasiPenting" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="informasiPenting"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${informasiPentingInstance?.companyDealer}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="companyDealer-label" class="property-label"><g:message
                                code="informasiPenting.companyDealer.label" default="Company Dealer" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="companyDealer-label">
                        %{--<ba:editableValue--}%
                        %{--bean="${informasiPentingInstance}" field="companyDealer"--}%
                        %{--url="${request.contextPath}/InformasiPenting/updatefield" type="text"--}%
                        %{--title="Enter companyDealer" onsuccess="reloadInformasiPentingTable();" />--}%

                        ${informasiPentingInstance?.companyDealer?.encodeAsHTML()}

                    </span></td>

                </tr>
            </g:if>

				
				<g:if test="${informasiPentingInstance?.m032Tanggal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m032Tanggal-label" class="property-label"><g:message
					code="informasiPenting.m032Tanggal.label" default="Tanggal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m032Tanggal-label">
						%{--<ba:editableValue
								bean="${informasiPentingInstance}" field="m032Tanggal"
								url="${request.contextPath}/InformasiPenting/updatefield" type="text"
								title="Enter m032Tanggal" onsuccess="reloadInformasiPentingTable();" />--}%
							
								<g:formatDate date="${informasiPentingInstance?.m032Tanggal}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${informasiPentingInstance?.m032UserUpload}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m032UserUpload-label" class="property-label"><g:message
					code="informasiPenting.m032UserUpload.label" default="User Upload" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m032UserUpload-label">
						%{--<ba:editableValue
								bean="${informasiPentingInstance}" field="m032UserUpload"
								url="${request.contextPath}/InformasiPenting/updatefield" type="text"
								title="Enter m032UserUpload" onsuccess="reloadInformasiPentingTable();" />--}%
							
								<g:fieldValue bean="${informasiPentingInstance}" field="m032UserUpload"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${informasiPentingInstance?.m032Judul}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m032Judul-label" class="property-label"><g:message
					code="informasiPenting.m032Judul.label" default="Judul" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m032Judul-label">
						%{--<ba:editableValue
								bean="${informasiPentingInstance}" field="m032Judul"
								url="${request.contextPath}/InformasiPenting/updatefield" type="text"
								title="Enter m032Judul" onsuccess="reloadInformasiPentingTable();" />--}%
							
								<g:fieldValue bean="${informasiPentingInstance}" field="m032Judul"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${informasiPentingInstance?.m032Konten}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m032Konten-label" class="property-label"><g:message
					code="informasiPenting.m032Konten.label" default="Konten" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m032Konten-label">
						%{--<ba:editableValue
								bean="${informasiPentingInstance}" field="m032Konten"
								url="${request.contextPath}/InformasiPenting/updatefield" type="text"
								title="Enter m032Konten" onsuccess="reloadInformasiPentingTable();" />--}%
							
								<g:fieldValue bean="${informasiPentingInstance}" field="m032Konten"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${informasiPentingInstance?.m032Image1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m032Image1-label" class="property-label"><g:message
					code="informasiPenting.m032Image1.label" default="Image1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m032Image1-label">
						%{--<ba:editableValue
								bean="${informasiPentingInstance}" field="m032Image1"
								url="${request.contextPath}/InformasiPenting/updatefield" type="text"
								title="Enter m032Image1" onsuccess="reloadInformasiPentingTable();" />--}%
                            <img class="" src="${createLink(controller:'informasiPenting', action:'showM032Image1',params : [id : informasiPentingInstance.id, random : new Date().toTimestamp()])}" />
							
						</span></td>
					
				</tr>
				</g:if>

				%{--<g:if test="${informasiPentingInstance?.m032StaTampil}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m032StaTampil-label" class="property-label"><g:message--}%
					%{--code="informasiPenting.m032StaTampil.label" default="Sta Tampil" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m032StaTampil-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${informasiPentingInstance}" field="m032StaTampil"--}%
								%{--url="${request.contextPath}/InformasiPenting/updatefield" type="text"--}%
								%{--title="Enter m032StaTampil" onsuccess="reloadInformasiPentingTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${informasiPentingInstance}" field="m032StaTampil"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${informasiPentingInstance?.m032Image2}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m032Image2-label" class="property-label"><g:message--}%
					%{--code="informasiPenting.m032Image2.label" default="Image2" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m032Image2-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${informasiPentingInstance}" field="m032Image2"--}%
								%{--url="${request.contextPath}/InformasiPenting/updatefield" type="text"--}%
								%{--title="Enter m032Image2" onsuccess="reloadInformasiPentingTable();" />--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${informasiPentingInstance?.m032Image3}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m032Image3-label" class="property-label"><g:message--}%
					%{--code="informasiPenting.m032Image3.label" default="Image3" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m032Image3-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${informasiPentingInstance}" field="m032Image3"--}%
								%{--url="${request.contextPath}/InformasiPenting/updatefield" type="text"--}%
								%{--title="Enter m032Image3" onsuccess="reloadInformasiPentingTable();" />--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${informasiPentingInstance?.m032Image4}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m032Image4-label" class="property-label"><g:message--}%
					%{--code="informasiPenting.m032Image4.label" default="Image4" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m032Image4-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${informasiPentingInstance}" field="m032Image4"--}%
								%{--url="${request.contextPath}/InformasiPenting/updatefield" type="text"--}%
								%{--title="Enter m032Image4" onsuccess="reloadInformasiPentingTable();" />--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${informasiPentingInstance?.m032Image5}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m032Image5-label" class="property-label"><g:message--}%
					%{--code="informasiPenting.m032Image5.label" default="Image5" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m032Image5-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${informasiPentingInstance}" field="m032Image5"--}%
								%{--url="${request.contextPath}/InformasiPenting/updatefield" type="text"--}%
								%{--title="Enter m032Image5" onsuccess="reloadInformasiPentingTable();" />--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%

			%{----}%
				%{--<g:if test="${informasiPentingInstance?.m032ID}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m032ID-label" class="property-label"><g:message--}%
					%{--code="informasiPenting.m032ID.label" default="M032 ID" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m032ID-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${informasiPentingInstance}" field="m032ID"--}%
								%{--url="${request.contextPath}/InformasiPenting/updatefield" type="text"--}%
								%{--title="Enter m032ID" onsuccess="reloadInformasiPentingTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${informasiPentingInstance}" field="m032ID"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

            <g:if test="${informasiPentingInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="informasiPenting.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${informasiPentingInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/InformasiPenting/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadInformasiPentingTable();" />--}%

                        <g:fieldValue bean="${informasiPentingInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${informasiPentingInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="informasiPenting.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${informasiPentingInstance}" field="dateCreated"
								url="${request.contextPath}/InformasiPenting/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadInformasiPentingTable();" />--}%
							
								<g:formatDate date="${informasiPentingInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${informasiPentingInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="informasiPenting.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${informasiPentingInstance}" field="createdBy"
                                url="${request.contextPath}/InformasiPenting/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadInformasiPentingTable();" />--}%

                        <g:fieldValue bean="${informasiPentingInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${informasiPentingInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="informasiPenting.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${informasiPentingInstance}" field="lastUpdated"
								url="${request.contextPath}/InformasiPenting/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadInformasiPentingTable();" />--}%
							
								<g:formatDate date="${informasiPentingInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${informasiPentingInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="informasiPenting.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${informasiPentingInstance}" field="updatedBy"
                                url="${request.contextPath}/InformasiPenting/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadInformasiPentingTable();" />--}%

                        <g:fieldValue bean="${informasiPentingInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${informasiPentingInstance?.id}"
					update="[success:'informasiPenting-form',failure:'informasiPenting-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteInformasiPenting('${informasiPentingInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
