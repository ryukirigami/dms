<%@ page import="com.kombos.administrasi.InformasiPenting" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'informasiPenting.label', default: 'Informasi Penting')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="create-informasiPenting" class="content scaffold-create" role="main">
			<legend><g:message code="default.create.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${informasiPentingInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${informasiPentingInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save"  enctype="multipart/form-data">--}%
            <form id="informasiPenting-save" class="form-horizontal" action="${request.getContextPath()}/informasiPenting/save" method="post" onsubmit="submitForm();return false;">
                <fieldset class="form">
                    <g:render template="form"/>
                </fieldset>
                <fieldset class="buttons controls">
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                    <g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}"
                                    onclick="return confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}');"/>
                </fieldset>
            </form>
            %{--<g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="reloadInformasiPentingTable();" update="informasiPenting-form"--}%
              %{--url="[controller: 'informasiPenting', action:'save']">--}%
			%{--</g:formRemote>--}%
			%{--</g:form>--}%
            <g:javascript>
			var submitForm;
			$(function(){

			function progress(e){
		        if(e.lengthComputable){
		            //kalo mau pake progress bar
		            //$('progress').attr({value:e.loaded,max:e.total});
		        }
		    }

			submitForm = function() {

   			var form = new FormData($('#informasiPenting-save')[0]);

			$.ajax({
                url:'${request.getContextPath()}/informasiPenting/save',
                type: 'POST',
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',progress, false);
                    }
                    return myXhr;
                },
                success: function (res) {
     				$('#informasiPenting-form').empty();
					$('#informasiPenting-form').append(res);
					reloadInformasiPentingTable();
                },
                //add error handler for when a error occurs if you want!
                error: function (data, status, e){
					alert(e);
				},
                data: form,
                cache: false,
                contentType: false,
                processData: false
            });

				}
			});
            </g:javascript>
		</div>
	</body>
</html>
