<%@ page import="com.kombos.administrasi.InformasiPenting" %>

<g:javascript>
    $(document).ready(function() {
        if($('#m032Konten').val().length>0){
            $('#hitung').text(255 - $('#m032Konten').val().length);
        }

        $('#m032Konten').keyup(function() {
            var len = this.value.length;
            if (len >= 255) {
                this.value = this.value.substring(0, 255);
                len = 255;
            }
            $('#hitung').text(255 - len);
        });
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: informasiPentingInstance, field: 'm032Tanggal', 'error')} ">
    <label class="control-label" for="m032Tanggal">
        <g:message code="informasiPenting.m032Tanggal.label" default="Tanggal"/>

    </label>

    <div class="controls">
        %{--<g:datePicker name="m032Tanggal" precision="day" value="${informasiPentingInstance?.m032Tanggal}" default="none"
                      noSelection="['': '']"/>--}%
        <input type="hidden" name="id" id="id" value="${informasiPentingInstance?.id}"/>
        <input type="hidden" name="m032Tanggal" value="${new Date().format("dd/MM/yyyy")}" />
        <g:formatDate format="dd - MM - yyyy" date="${new Date()}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: informasiPentingInstance, field: 'm032UserUpload', 'error')} ">
    <label class="control-label" for="m032UserUpload">
        <g:message code="informasiPenting.m032UserUpload.label" default="Dibuat Oleh"/>

    </label>

    <div class="controls">
        %{--<g:textField name="m032UserUpload" value="${informasiPentingInstance?.m032UserUpload}"/>--}%
        <input type="hidden" name="m032UserUpload" value="${org.apache.shiro.SecurityUtils.subject.principal.toString()}" />
        ${org.apache.shiro.SecurityUtils.subject.principal.toString()}

    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: informasiPentingInstance, field: 'm032Judul', 'error')} ">
    <label class="control-label" for="m032Judul">
        <g:message code="informasiPenting.m032Judul.label" default="Judul"/>

    </label>

    <div class="controls">
        <g:textField maxlength="50" name="m032Judul" value="${informasiPentingInstance?.m032Judul}"/>
    </div>
</div>
<g:javascript>
    document.getElementById("m032Judul").focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: informasiPentingInstance, field: 'm032Konten', 'error')} ">
    <label class="control-label" for="m032Konten">
        <g:message code="informasiPenting.m032Konten.label" default="Konten"/>
    </label>

    <div class="controls">
        <g:textArea name="m032Konten" value="${informasiPentingInstance?.m032Konten}"/>
        <br/>
        <span id="hitung">255</span> Karakter Tersisa.
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: informasiPentingInstance, field: 'm032Image1', 'error')} ">
    <label class="control-label" for="m032Image1">
        <g:message code="informasiPenting.m032Image1.label" default="Attachment"/>

    </label>

    <div class="controls">
        <input type="file" id="m032Image1" name="m032Image1"/>
    </div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: informasiPentingInstance, field: 'm032StaTampil', 'error')} ">
    <label class="control-label" for="m032StaTampil">
        <g:message code="informasiPenting.m032StaTampil.label" default="M032 Sta Tampil"/>

    </label>

    <div class="controls">
        <g:textField name="m032StaTampil" value="${informasiPentingInstance?.m032StaTampil}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: informasiPentingInstance, field: 'm032Image2', 'error')} ">
    <label class="control-label" for="m032Image2">
        <g:message code="informasiPenting.m032Image2.label" default="M032 Image2"/>

    </label>

    <div class="controls">
        <input type="file" id="m032Image2" name="m032Image2"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: informasiPentingInstance, field: 'm032Image3', 'error')} ">
    <label class="control-label" for="m032Image3">
        <g:message code="informasiPenting.m032Image3.label" default="M032 Image3"/>

    </label>

    <div class="controls">
        <input type="file" id="m032Image3" name="m032Image3"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: informasiPentingInstance, field: 'm032Image4', 'error')} ">
    <label class="control-label" for="m032Image4">
        <g:message code="informasiPenting.m032Image4.label" default="M032 Image4"/>

    </label>

    <div class="controls">
        <input type="file" id="m032Image4" name="m032Image4"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: informasiPentingInstance, field: 'm032Image5', 'error')} ">
    <label class="control-label" for="m032Image5">
        <g:message code="informasiPenting.m032Image5.label" default="M032 Image5"/>

    </label>

    <div class="controls">
        <input type="file" id="m032Image5" name="m032Image5"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: informasiPentingInstance, field: 'companyDealer', 'error')} ">
    <label class="control-label" for="companyDealer">
        <g:message code="informasiPenting.companyDealer.label" default="Company Dealer"/>

    </label>

    <div class="controls">
        <g:select id="companyDealer" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.list()}"
                  optionKey="id" value="${informasiPentingInstance?.companyDealer?.id}" class="many-to-one"
                  noSelection="['null': '']"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: informasiPentingInstance, field: 'm032ID', 'error')} ">
    <label class="control-label" for="m032ID">
        <g:message code="informasiPenting.m032ID.label" default="M032 ID"/>

    </label>

    <div class="controls">
        <g:field name="m032ID" type="number" value="${informasiPentingInstance.m032ID}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: informasiPentingInstance, field: 'createdBy', 'error')} ">
    <label class="control-label" for="createdBy">
        <g:message code="informasiPenting.createdBy.label" default="Created By"/>

    </label>

    <div class="controls">
        <g:textField name="createdBy" value="${informasiPentingInstance?.createdBy}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: informasiPentingInstance, field: 'updatedBy', 'error')} ">
    <label class="control-label" for="updatedBy">
        <g:message code="informasiPenting.updatedBy.label" default="Updated By"/>

    </label>

    <div class="controls">
        <g:textField name="updatedBy" value="${informasiPentingInstance?.updatedBy}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: informasiPentingInstance, field: 'lastUpdProcess', 'error')} ">
    <label class="control-label" for="lastUpdProcess">
        <g:message code="informasiPenting.lastUpdProcess.label" default="Last Upd Process"/>

    </label>

    <div class="controls">
        <g:textField name="lastUpdProcess" value="${informasiPentingInstance?.lastUpdProcess}"/>
    </div>
</div>--}%

