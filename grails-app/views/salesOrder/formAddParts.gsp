<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 05/12/14
  Time: 16:31
--%>
<%@ page import="com.kombos.parts.Franc; com.kombos.parts.KlasifikasiGoods; com.kombos.parts.SalesOrder" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="salesOrder.formAddParts.label" default="${aksi} Sales Order" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
        var doSave;
        var cekStatus = "${salesOrderInstance?.id}";
        $('.auto').autoNumeric('init');
        $(function(){
            doSave = function(aksi){
                var checkGoods =[];
                var checkDelGoods =[];
                var aoData = [];
                var deliveryCost = $("#deliveryCost").val().replace(/,/g,"");
                $("#goods_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        checkGoods.push(id);
                    }else{
                        var idDel = $(this).next("input:hidden").val();
                        checkDelGoods.push(id);
                    }
                });

                if(checkGoods.length<1){
                    alert('Anda belum memilih data yang akan ditambahkan');
                }else{
                    for(var a=0 ; a < checkGoods.length;a++){
                        aoData.push(
                            {"name": 'qty_'+checkGoods[a]+'', "value": $('#qty_'+checkGoods[a]+'').val()},
                            {"name": 'discount_'+checkGoods[a]+'', "value": $('#discount_'+checkGoods[a]+'').val()},
                            {"name": 'harga_'+checkGoods[a]+'', "value": $('#harga_'+checkGoods[a]+'').val().replace(/,/g,"")}
                        );
                    }
                    aoData.push(
                        {"name": 'ids', "value": JSON.stringify(checkGoods)},
                        {"name": 'salesTypes', "value": $('#salesType').val()},
                        {"name": 'tradingFor', "value": $('#tradingFor').val()},
                        {"name": 'customerName', "value": $('#customerName').val()},
                        {"name": 'deliveryDates', "value": $('#deliveryDate').val()},
                        {"name": 'deliveryLocation', "value": $('#deliveryLocation').val()},
                        {"name": 'deliveryAddress', "value": $('#deliveryAddress').val()},
                        {"name": 'paymentType', "value": $('#paymentType').val()},
                        {"name": 'deliveryCost', "value": deliveryCost},
                        {"name": 'note', "value": $('#note').val()},
                        {"name": 'ppn', "value": $('#ppn').val()},
                        {"name": 'idSO', "value": $('#id').val()}
                    )
                    if(aksi=="update"){
                        aoData.push(
                            {"name": 'idsDel', "value": JSON.stringify(checkDelGoods)}
                        )
                    }
                    if(confirm("Anda yakin data akan disimpan?")){
                    $.ajax({
                        url:'${request.contextPath}/salesOrder/'+aksi,
                        type: "POST", // Always use POST when deleting data
                        data : aoData,
                        success : function(data){
                            toastr.success('<div>Data Berhasil disimpan.</div>');
                            loadPath('salesOrder/list');
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });
                    }
                }

                checkGoods = [];
            }

            $('#customerName').typeahead({
                source: function (query, process) {
                    return $.get('${request.contextPath}/salesOrder/listCustomer', { query: query }, function (data) {
                        return process(data.options);
                    });
                }
            });
        })

        function doAddParts(){
            $("#addPartsContent").empty();
		    $.ajax({
		        type:'POST',
		        url:'${request.contextPath}/salesOrder/addParts',
                success:function(data,textStatus){
                        $("#addPartsContent").html(data);
                        $("#addPartsModal").modal({
                            "backdrop" : "static",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
   		    });
        }

        function ubahJumlah(param){
            var total = 0;
            var valueParam = param;
            var valQty = $('#qty_'+valueParam).val();
            var valDisc = $('#discount_'+valueParam).val();
            var valHarga = $('#harga_'+valueParam).val().replace(/,/g,"");
            var val1 = valQty=="" ? valHarga : valQty*valHarga;
            var val2 = valDisc=="" ? (val1=="" ? valHarga : val1)  : ( val1=="" ? valHarga - valDisc/100*valHarga : val1 - valDisc/100*val1);
            $('#jumlah_'+valueParam+"").val(val2);
            $("#goods_datatables tbody .row-select").each(function() {
                var id = $(this).next("input:hidden").val();
                    total = parseInt(total) + parseInt($('#jumlah_'+id+"").val().replace(/,/g,""));
            });
            $("#txttotal").val(total);
        }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="salesOrder.formAddParts.label" default="${aksi} Sales Order" /></span>
</div>
<div class="box">
    <div class="span12" id="formAddParts-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
            <div class="row-fluid">
                <div id="kiri" class="span5">
                    <table>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="salesOrder.salesType.label" default="Tipe Penjualan" />
                            </td>
                            <td style="padding: 10px">
                                <g:hiddenField id="id" name="id" value="${salesOrderInstance?.id}"/>
                                <g:select id="salesType" name="salesType" from="${Franc.createCriteria().list{order("m117NamaFranc")}}" optionKey="id" required="" value="${salesOrderInstance?.salesType?.id}" class="many-to-one"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="salesOrder.tradingFor.label" default="Penjualan Untuk" />
                            </td>
                            <td style="padding: 10px">
                                <g:select id="tradingFor" name="tradingFor" from="${["Cabang","Umum"]}" required="" value="${salesOrderInstance?.tradingFor}"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="salesOrder.customerName.label" default="Nama Pelanggan" />
                            </td>
                            <td style="padding: 10px">
                                <g:textField name="customerName" id="customerName" value="${salesOrderInstance?.customerName}" class="typeahead" autocomplete="off"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="salesOrder.deliveryDate.label" default="Tanggal Pengiriman" />
                            </td>
                            <td style="padding: 10px">
                                <ba:datePicker name="deliveryDate" id="deliveryDate" precision="day" value="${salesOrderInstance?.deliveryDate}" format="dd-MM-yyyy"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="salesOrder.deliveryLocation.label" default="Lokasi Pengiriman" />
                            </td>
                            <td style="padding: 10px">
                                <g:textField name="deliveryLocation" id="deliveryLocation" value="${salesOrderInstance?.deliveryLocation}" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="salesOrder.deliveryAddress.label" default="Alamat Pengiriman" />
                            </td>
                            <td style="padding: 10px">
                                <g:textArea name="deliveryAddress" id="deliveryAddress" value="${salesOrderInstance?.deliveryAddress}" style="resize:none" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="kanan" class="span7">
                    <table>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="salesOrder.paymentType.label" default="Tipe Pembelian" />
                            </td>
                            <td style="padding: 10px">
                                <g:select id="paymentType" name="paymentType" from="${["Tunai","Kredit"]}" required="" value="${salesOrderInstance?.paymentType}"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="salesOrder.deliveryCost.label" default="Biaya Pengiriman" />
                            </td>
                            <td style="padding: 10px">
                                <g:textField name="deliveryCost" class="auto" id="deliveryCost" value="${salesOrderInstance?.deliveryCost ? Double.valueOf(salesOrderInstance?.deliveryCost).longValue() : '0'}" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="salesOrder.note.label" default="Catatan/No. Rangka" />
                            </td>
                            <td style="padding: 10px">
                                <g:textArea name="note" id="note" value="${salesOrderInstance?.note}" style="resize:none" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">
                                <g:message code="salesOrder.ppn.label" default="PPN (%)" />
                            </td>
                            <td style="padding: 10px">
                                <g:if test="${aksi=="Create"}">
                                    <g:textField name="ppn" id="ppn" value="${ppn}" />
                                </g:if>
                                <g:else>
                                    <g:textField name="ppn" id="ppn" value="${salesOrderInstance?.ppn}" />
                                </g:else>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <g:if test="${aksi && aksi!="Show"}">
                <button class="btn pull-left" onclick="doAddParts();">Add Parts</button>
            </g:if>
            <br/>
            <br/>
            <br/>
            <g:render template="dataTablesAdd" />
            <fieldset class="buttons controls">
                <a class="btn cancel" onclick="loadPath('salesOrder/index');">
                    <g:message code="default.button.cancel.label" default="Cancel" />
                </a>
                <g:if test="${aksi=="Create"}">
                    <button class="btn btn-primary" name="save" onclick="doSave('save');" >Create</button>
                </g:if>
                <g:elseif test="${aksi=="Update"}">
                    <button class="btn btn-primary" name="update" onclick="doSave('update');" >Update</button>
                </g:elseif>
            </fieldset>
        </div>
    <div class="span7" id="formAddParts-form" style="display: none;"></div>
</div>
<div id="addPartsModal" class="modal fade">
    <div class="modal-dialog" style="width: 1090px;">
        <div class="modal-content" style="width: 1090px;">
            <div class="modal-body" style="max-height: 800px;">
                <div id="addPartsContent"/>
                <div class="iu-content"></div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
