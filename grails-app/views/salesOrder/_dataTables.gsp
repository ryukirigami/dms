
<%@ page import="com.kombos.parts.SalesOrder" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="salesOrder_datatables" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover table-selectable fixed" style="table-layout: fixed; ">
    <col width="200px" />
    <col width="150px" />
    <col width="200px" />
    <col width="120px" />
    <col width="120px" />
    <col width="150px" />
    <col width="150px" />
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="salesOrder.sorNumber.label" default="No Sales Order" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="salesOrder.deliveryDate.label" default="Tgl Sales Order" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="salesOrder.customerName.label" default="Nama Pelanggan" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="salesOrder.salesType.label" default="Tipe Penjualan" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="salesOrder.paymentType.label" default="Tipe Pembelian" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="salesOrder.deliveryLocation.label" default="Lokasi Pengiriman" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="salesOrder.isApprove.label" default="Status Approval" /></div>
        </th>
    </tr>
   <tr>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_sorNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 230px;">
                <input type="text" name="search_sorNumber" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_deliveryDate" style="padding-top: 0px;position:relative; margin-top: 0px;width: 150px;" >
                <input type="hidden" name="search_tggldeliveryDate" value="date.struct">
                <input type="hidden" name="search_tggldeliveryDate_day" id="search_tggldeliveryDate_day" value="">
                <input type="hidden" name="search_tggldeliveryDate_month" id="search_tggldeliveryDate_month" value="">
                <input type="hidden" name="search_tggldeliveryDate_year" id="search_tggldeliveryDate_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_tggldeliveryDate_dp" value="" id="search_tggldeliveryDate" class="search_init">
            </div>
        </th>


        <th style="border-top: none;padding: 5px;">
            <div id="filter_customerName" style="padding-top: 0px;position:relative; margin-top: 0px;width: 200px;">
                <input type="text" name="search_customerName" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_deliveryLocation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 80px;">
                <input type="text" name="search_deliveryLocation" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_salesType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 80px;">
                <input type="text" name="search_salesType" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_paymentType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 80px;">
                <input type="text" name="search_paymentType" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_isApprove" style="padding-top: 0px;position:relative; margin-top: 0px;width: 80px;">
                <input type="text" name="search_isApprove" class="search_init" />
            </div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var salesOrderTable;
var reloadSalesOrderTable;
var updateData;
var show;
$(function(){

	reloadSalesOrderTable = function() {
		salesOrderTable.fnDraw();
	}

    updateData = function(data){
        loadPath("salesOrder/formAddParts?aksi=Update&id="+data);
    }

    show = function(data){
        loadPath("salesOrder/formAddParts?aksi=Show&id="+data);
    }

    $('#search_tggldeliveryDate').datepicker().on('changeDate', function(ev) {
    var newDate = new Date(ev.date);
    $('#search_tggldeliveryDate_day').val(newDate.getDate());
    $('#search_tggldeliveryDate_month').val(newDate.getMonth()+1);
    $('#search_tggldeliveryDate_year').val(newDate.getFullYear());
    $(this).datepicker('hide');
    salesOrderTable.fnDraw();
    })

    var anOpen = [];
	$('#salesOrder_datatables td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = salesOrderTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/salesOrder/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = salesOrderTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			salesOrderTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	salesOrderTable.fnDraw();
		}
	});

    salesOrderTable = $('#salesOrder_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"bDestroy": true,
		"aoColumns": [

{
	"sName": "sorNumber",
	"mDataProp": "sorNumber",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    if(row['isApprove']=='Wait For Approval'){
		    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="salesId" value="'+row['id']+'">&nbsp;&nbsp;<a href="javascript:void(0);" onclick="showDetail('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="updateData('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	    }else{
	        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="salesId" value="'+row['id']+'">&nbsp;&nbsp;<a href="javascript:void(0);" onclick="showDetail('+row['id']+');">'+data+'</a>';
	    }
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"400px",
	"bVisible": true
}

,

{
	"sName": "deliveryDate",
	"mDataProp": "deliveryDate",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}
,

{
	"sName": "customerName",
	"mDataProp": "customerName",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}
,

{
	"sName": "salesType",
	"mDataProp": "salesType",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}
,

{
	"sName": "paymentType",
	"mDataProp": "paymentType",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}
,

{
	"sName": "deliveryLocation",
	"mDataProp": "deliveryLocation",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "isApprove",
	"mDataProp": "isApprove",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}

],
    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
//                 aoData.push(
//                        {"name": 'salesOrder', "value": $("#salesOrder").val()}
//                 );

                 var sorNumber = $('#filter_sorNumber input').val();
				 if(sorNumber){
					aoData.push(
				    	{"name": 'sCriteria_sorNumber', "value": sorNumber}
					);
                 }

                var tggldeliveryDate = $('#search_tggldeliveryDate').val();
                var tggldeliveryDateDay = $('#search_tggldeliveryDate_day').val();
                var tggldeliveryDateMonth = $('#search_tggldeliveryDate_month').val();
                var tggldeliveryDateYear = $('#search_tggldeliveryDate_year').val();

                if(tggldeliveryDate){
                    aoData.push(
                            {"name": 'sCriteria_tggldeliveryDate', "value": "date.struct"},
                            {"name": 'sCriteria_tggldeliveryDate_dp', "value": tggldeliveryDate},
                            {"name": 'sCriteria_tggldeliveryDate_day', "value": tggldeliveryDateDay},
                            {"name": 'sCriteria_tggldeliveryDate_month', "value": tggldeliveryDateMonth},
                            {"name": 'sCriteria_tggldeliveryDate_year', "value": tggldeliveryDateYear}
                    );
                }


                 var customerName = $('#filter_customerName input').val();
				 if(customerName){
					aoData.push(
				    	{"name": 'sCriteria_customerName', "value": customerName}
					);
                 }

                 var deliveryLocation = $('#filter_deliveryLocation input').val();
				 if(deliveryLocation){
					aoData.push(
				    	{"name": 'sCriteria_deliveryLocation', "value": deliveryLocation}
					);
                 }

                 var salesType = $('#filter_salesType input').val();
				 if(salesType){
					aoData.push(
				    	{"name": 'sCriteria_salesType', "value": salesType}
					);
                 }

                 var paymentType = $('#filter_paymentType input').val();
				 if(paymentType){
					aoData.push(
				    	{"name": 'sCriteria_paymentType', "value": paymentType}
					);
                 }

                 var isApprove = $('#filter_isApprove input').val();
				 if(isApprove){
					aoData.push(
				    	{"name": 'sCriteria_isApprove', "value": isApprove}
					);
                 }

                $.ajax({ "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData ,
                    "success": function (json) {
                        fnCallback(json);
                       },
                    "complete": function () {
                       }
                });
}
});
});

</g:javascript>