
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="addGoods_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped fixed table-bordered table-hover" style="table-layout: fixed; width: 1010px">
    <col width="200px" />
    <col width="250px" />
    <col width="200px" />
    <col width="100px" />
    <col width="150px" />

    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.m111ID.label" default="Kode Goods" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.m111Nama.label" default="Nama Goods" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.satuan.label" default="Satuan" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.qtyStock.label" default="Qty Stok" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.harga.label" default="Harga Beli" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.harga.label" default="Harga Jual" /></div>
        </th>
    </tr>
    <tr>


        <th style="border-top: none;padding: 5px;">
            <div id="filter_m111ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_m111ID" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_m111Nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_m111Nama" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_satuan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_satuan" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;" />
        <th style="border-top: none;padding: 5px;" />
        <th style="border-top: none;padding: 5px;" />

    </tr>
    </thead>
</table>

<g:javascript>
var addGoodsTable;
var reloadGoodsTable;
$(function(){
    reloadGoodsTable = function() {
		addGoodsTable.fnDraw();
	}

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	addGoodsTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});
	addGoodsTable = $('#addGoods_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesPartsList")}",
		"aoColumns": [

{
	"sName": "m111ID",
	"mDataProp": "m111ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data+ " / " + row['franc'];
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m111Nama",
	"mDataProp": "m111Nama",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
}

,

{
	"sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "qty",
	"mDataProp": "qty",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "hargaBeli",
	"mDataProp": "hargaBeli",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}
,

{
	"sName": "harga",
	"mDataProp": "harga",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						aoData.push(
                                {"name": 'salesType', "value": $("#salesType").val()}
                        );

						var m111ID = $('#filter_m111ID input').val();
						if(m111ID){
							aoData.push(
									{"name": 'sCriteria_m111ID', "value": m111ID}
							);
                        }

						var m111Nama = $('#filter_m111Nama input').val();
						if(m111Nama){
							aoData.push(
									{"name": 'sCriteria_m111Nama', "value": m111Nama}
							);
						}

						var satuan = $('#filter_satuan input').val();
						if(satuan){
							aoData.push(
									{"name": 'sCriteria_satuan', "value": satuan}
							);
						}

                        var exist =[];
						var rows = $("#goods_datatables").dataTable().fnGetNodes();
                        for(var i=0;i<rows.length;i++)
                        {
							var id = $(rows[i]).find("#idPopUp").val();
							exist.push(id);
						}
						if(exist.length > 0){
							aoData.push(
										{"name": 'sCriteria_exist', "value": JSON.stringify(exist)}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>



