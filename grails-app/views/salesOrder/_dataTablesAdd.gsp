
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="goods_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped fixed table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.m111ID.label" default="Kode Goods" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.m111Nama.label" default="Nama Goods" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.qtyStock.label" default="Stock" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.qty.label" default="Kuantitas" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.hargasatuan.label" default="Harga Per Unit" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.satuan.label" default="Satuan" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.qtyStock.label" default="Pemotongan Harga (%)" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.satuan.label" default="Jumlah Material" /></div>
        </th>


    </tr>
    </thead>
    <tfoot>
    <tr>
        <td colspan="7"><span>Total (Total Jumlah Material - PPN)</span></td>
        <td ><input id="txttotal" type="text" style="width:90%;" value="0" readonly=""></td>
    </tr>
    </tfoot>
</table>

<g:javascript>
var goodsTable;
var reloadGoodsTable;
$(function(){

	reloadGoodsTable = function() {
		goodsTable.fnDraw();
	}

	goodsTable = $('#goods_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
//		"bProcessing": true,
//		"bServerSide": true,
//		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		%{--"sAjaxSource": "${g.createLink(action: "datatablesPartsList")}",--}%
		"aoColumns": [

{
	"sName": "m111IDAdd",
	"mDataProp": "m111IDAdd",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="idPopUp" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m111NamaAdd",
	"mDataProp": "m111NamaAdd",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"450px",
	"bVisible": true
}

,

{
	"sName": "stokAdd",
	"mDataProp": "stokAdd",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "qtyAdd",
	"mDataProp": "qtyAdd",
	"aTargets": [3],
	"mRender": function ( data, type, row ) {
    			return '<input id="qty_'+row['id']+'" onkeyup="ubahJumlah(\''+row['id']+'\');" class="inline-edit" type="text" style="width:70px;" value="'+data+'">';
    		},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "hargaAdd",
	"mDataProp": "hargaAdd",
	"aTargets": [4],
	"mRender": function ( data, type, row ) {
        return '<input id="harga_'+row['id']+'" class="inline-edit" readonly="" type="text" style="width:120px;" value="'+data+'">';
    },
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "satuanAdd",
	"mDataProp": "satuanAdd",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "discountAdd",
	"mDataProp": "discountAdd",
	"aTargets": [6],
	"mRender": function ( data, type, row ) {
        return '<input id="discount_'+row['id']+'" onkeyup="ubahJumlah(\''+row['id']+'\');" class="inline-edit" type="text" style="width:70px;" value="'+data+'">';
    },
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "jumlahAdd",
	"mDataProp": "jumlahAdd",
	"aTargets": [7],
	"mRender": function ( data, type, row ) {
        return '<input id="jumlah_'+row['id']+'" class="inline-edit" readonly="" type="text" style="width:140px;" value="'+data+'">';
    },
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});

	if(cekStatus!=""){
        $.ajax({
    		url:'${request.contextPath}/salesOrder/tableDetailData',
    		type: "POST", // Always use POST when deleting data
    		data: { id: cekStatus },
    		success : function(data){
    		    var total = 0;
    		    var jum;
    		    $.each(data,function(i,item){
                    goodsTable.fnAddData({
                        'id': item.id,
                        'm111IDAdd': item.m111IDAdd,
                        'm111NamaAdd': item.m111NamaAdd,
                        'stokAdd': item.stokAdd,
                        'qtyAdd': item.qtyAdd,
                        'hargaAdd': item.hargaAdd,
                        'satuanAdd': item.satuanAdd,
                        'discountAdd': item.discountAdd,
                        'jumlahAdd': item.jumlahAdd
                    });
                    //jum = item.jumlahAdd.replace(",","")
                    //total = parseInt(total) + parseInt(jum);
                    total = item.total;
                });
                  $('#txttotal').val(total);

            }
		});
    }
});
</g:javascript>