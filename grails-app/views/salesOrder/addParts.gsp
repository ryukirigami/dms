<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 05/12/14
  Time: 16:31
--%>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="salesOrder.addParts.label" default="Pilih Parts" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
        var addParts;
        var doSave;
        $(function(){
            addParts = function(){
                var checkGoods =[];
                $("#addGoods_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var nRow = $(this).parents('tr')[0];
                        checkGoods.push(nRow);
                    }
                });
                if(checkGoods.length<1){
                    alert('Anda belum memilih data yang akan ditambahkan');
                } else {
                    var total = 0;
                    for (var i=0;i < checkGoods.length;i++){
                        var aData = addGoodsTable.fnGetData(checkGoods[i]);
                        if(!aData['franc']){
                            toastr.warning("PARTS/ BAHAN BELUM DI KLASIFIKASI GOODS");
                        }
                        goodsTable.fnAddData({
                            'id': aData['id'],
                            'm111IDAdd': aData['m111ID'],
                            'm111NamaAdd': aData['m111Nama'],
                            'stokAdd': aData['qty'],
                            'hargaAdd': aData['harga'],
                            'satuanAdd': aData['satuan'],
                            'qtyAdd': 1,
                            'discountAdd': 0,
                            'jumlahAdd': aData['harga']

                        });
                        $('#qty_'+aData['id']).autoNumeric('init',{
                            vMin:'0',
                            vMax:aData['qty'],
                            mDec: 2,
                            aSep:''
                        });
                        $('#discount_'+aData['id']).autoNumeric('init',{
                            vMin:'0',
                            vMax:'100',
                            mDec: 2,
                            aSep:''
                        });
                        var hargaSat = aData['harga'].toString().replace(/,/g,"");
                        total = parseFloat(total) + parseFloat(hargaSat);
                    }
                    $("#txttotal").val(total);
                }
                reloadGoodsTable();
            }

        })

    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="salesOrder.addParts.label" default="Pilih Parts" /></span>
    %{--<button type="button" class="close pull-right" data-dismiss="modal">&times;</button>--}%
</div>
<div class="box">
    <div class="span12" id="addParts-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <g:render template="dataTablesParts" />
        <fieldset class="buttons controls pull-right">
            <button class="btn btn-primary" name="add" onclick="addParts();">Add Part</button>
            <button class="btn cancel" data-dismiss="modal" name="close" >Close</button>
        </fieldset>
    </div>
</div>
</body>
</html>
