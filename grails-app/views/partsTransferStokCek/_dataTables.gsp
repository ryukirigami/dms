
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay" />

<table id="partsTransferStok_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover table-selectable" style="table-layout: fixed; ">
    <thead>
    <tr>
        <th style="vertical-align: middle;">
            <div style="height: 10px"> </div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div id="dealer">Dealer Yang Membutuhkan</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div> Tanggal Permintaan</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div> Nomor PO</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div id="status"> Status</div>
        </th>

    </tr>

    </thead>
</table>

<g:javascript>
var partsTransferStokTable;
var reloadPartsTransferStokTable;
$(function(){
    function getCheckedRadioId(name) {
         var elements = document.getElementsByName(name);

         for (var i=0, len=elements.length; i<len; ++i){
                if (elements[i].checked)
                    return elements[i].value;
         }

    }

reloadPartsTransferStokTable = function() {
		partsTransferStokTable.fnDraw();
	}
 $('#clear').click(function(e){
        $('#search_t017Tanggal').val("");
        $('#search_t017Tanggal_day').val("");
        $('#search_t017Tanggal_month').val("");
        $('#search_t017Tanggal_year').val("");
        $('#search_t017Tanggal2').val("");
        $('#search_t017Tanggal2_day').val("");
        $('#search_t017Tanggal2_month').val("");
        $('#search_t017Tanggal2_year').val("");
        $("input[name=search_status][value=" + 0 + "]").attr('checked', 'checked');
        $('#dealer').html("Dealer Yg Membutuhkan")
        e.stopPropagation();
		partsTransferStokTable.fnDraw();
	});
    $('#view').click(function(e){
        if(getCheckedRadioId('search_status')=='2'){
            $('#dealer').html("Dealer Pengirim")
        }else{
            $('#dealer').html("Dealer Yg Membutuhkan")
        }
        if(getCheckedRadioId('search_status')=='1'){
            document.getElementById("printPartsTransferStokData").style.visibility="visible";
        }else{
            document.getElementById("printPartsTransferStokData").style.visibility="hidden";
        }
        e.stopPropagation();
		partsTransferStokTable.fnDraw();
	});
	var anOpen = [];
	$('#partsTransferStok_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = partsTransferStokTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/partsTransferStokCek/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = partsTransferStokTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			partsTransferStokTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});
	
    $('#partsTransferStok_datatables_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( partsTransferStokTable, nEditing );
            editRow( partsTransferStokTable, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( partsTransferStokTable, nEditing );
            nEditing = null;
        }
        else {
            editRow( partsTransferStokTable, nRow );
            nEditing = nRow;
        }
    } );
    
	reloadPartsTransferStokTable = function() {
		partsTransferStokTable.fnDraw();
	}

 	partsTransferStokTable = $('#partsTransferStok_datatables_${idTable}').dataTable({
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    if(row['sCriteria_status']=='0'){
		    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" id="nomorPO" value="'+row['nomorPO']+'">&nbsp;&nbsp;<a href="#/partsTransferStokCekInput" onclick="klik(\''+row['nomorPO']+'\');">'+data+'</a>';
	    }else{
		    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" id="nomorPO" value="'+row['nomorPO']+'">&nbsp;&nbsp;'+data;
	    }
	},
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "tglTransferStok",
	"mDataProp": "tglTransferStok",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"200px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"sName": "nomorPO",
	"mDataProp": "nomorPO",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"200px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"sName": "status",
	"mDataProp": "status",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"200px",
	"sDefaultContent": '',
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var t017Tanggal = $('#search_t017Tanggal').val();
						var t017TanggalDay = $('#search_t017Tanggal_day').val();
						var t017TanggalMonth = $('#search_t017Tanggal_month').val();
						var t017TanggalYear = $('#search_t017Tanggal_year').val();

						var t017Tanggal2 = $('#search_t017Tanggal2').val();
						var t017TanggalDay2 = $('#search_t017Tanggal2_day').val();
						var t017TanggalMonth2 = $('#search_t017Tanggal2_month').val();
						var t017TanggalYear2 = $('#search_t017Tanggal2_year').val();

						if(t017Tanggal){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal_dp', "value": t017Tanggal},
									{"name": 'sCriteria_t017Tanggal_day', "value": t017TanggalDay},
									{"name": 'sCriteria_t017Tanggal_month', "value": t017TanggalMonth},
									{"name": 'sCriteria_t017Tanggal_year', "value": t017TanggalYear}
							);
						}


						if(t017Tanggal2){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal2', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal2_dp', "value": t017Tanggal2},
									{"name": 'sCriteria_t017Tanggal2_day', "value": t017TanggalDay2},
									{"name": 'sCriteria_t017Tanggal2_month', "value": t017TanggalMonth2},
									{"name": 'sCriteria_t017Tanggal2_year', "value": t017TanggalYear2}
							);
						}
	                    var statusParts = getCheckedRadioId('search_status')

						if(statusParts){
							aoData.push(
									{"name": 'sCriteria_status', "value": statusParts}
							);
						}else{
						    aoData.push(
									{"name": 'sCriteria_status', "value": '0'}
							);
						}
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});



</g:javascript>



