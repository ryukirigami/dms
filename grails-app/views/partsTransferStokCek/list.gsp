
<%@ page import="com.kombos.parts.PartsTransferStok" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="title" value="Parts Transfer Stok - Permintaan dari Dealer Lain" />
    <title>${title}</title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var klik;
	var loadForm;
	var printPartsTransferStok;
	var printPartsTransferStokCont;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();" />
        break;
    case '_DELETE_' :
        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});

         		break;
       }
       return false;
	});


    editData = function(id) {
        $('#spinner').fadeIn(1);
        window.location.replace('#/editPartsTransferStok');
        $.ajax({url: '${request.contextPath}/partsTransferStokInput?noPartsTransferStok='+id,
            type:"GET",dataType: "html",
            complete : function (req, err) {
                $('#main-content').html(req.responseText);
                $('#spinner').fadeOut();
                loading = false;
            }
        });
    };

    loadForm = function(data, textStatus){
		$('#partsTransferStok-form').empty();
    	$('#partsTransferStok-form').append(data);
   	}

    shrinkTableLayout = function(){
    	$("#partsTransferStok-table").hide()
        $("#partsTransferStok-form").css("display","block");
   	}

   	expandTableLayout = function(){
        $("#partsTransferStok-table").show()
        $("#partsTransferStok-form").css("display","none");
   	}

   	massDelete = function() {
   		var recordsToDelete = [];
		$("#partsTransferStok-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}

		});

		var json = JSON.stringify(recordsToDelete);

		$.ajax({
    		url:'${request.contextPath}/partsTransferStok/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadPartsTransferStokTable();
    		}
		});
   	}

   	klik = function(data){
        $('#spinner').fadeIn(1);
        $.ajax({
            url: '${request.contextPath}/partsTransferStokCekInput?nomorPO='+data,
            type: "GET",dataType:"html",
            complete : function (req, err) {
                $('#main-content').html(req.responseText);
                $('#spinner').fadeOut();
            }
        });
    }

});
        var checkin = $('#search_t017Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
            checkin.hide();
            $('#search_t017Tanggal2')[0].focus();
        }).data('datepicker');

        var checkout = $('#search_t017Tanggal2').datepicker({
            onRender: function(date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');

        printPartsTransferStok = function(){
           checkPartsTransferStok =[];
            $("#partsTransferStok-table tbody .row-select").each(function() {
                if(this.checked){
                 var nRow = $(this).next("#nomorPO").val()?$(this).next("#nomorPO").val():"-"
                    if(nRow!="-"){
                        checkPartsTransferStok.push(nRow);
                    }
                }
            });
            if(checkPartsTransferStok.length<1 ){
                alert('Silahkan Pilih Salah Satu Dealer Untuk Dicetak');
                return;
            }
           var idPartsTransferStok =  JSON.stringify(checkPartsTransferStok);
           window.location = "${request.contextPath}/partsTransferStokCek/printPartsTransferStok?idPartsTransferStok="+idPartsTransferStok;
        }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left">${title}</span>
    <ul class="nav pull-right">
        %{--<li><a class="pull-right"href="#/partsTransferStokCekInput" onclick="klik()" style="display: block;" >&nbsp;&nbsp;<i class="icon-plus"></i>&nbsp;&nbsp;--}%
        %{--</a></li>--}%
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="partsTransferStok-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <fieldset>
            <table style="padding-right: 10px">
                <tr>
                    <td style="width: 130px">
                        <label class="control-label" for="t951Tanggal">
                            <g:message code="auditTrail.tanggal.label" default="Tanggal Permintaan" />&nbsp;
                        </label>&nbsp;&nbsp;
                    </td>
                    <td>
                        <div id="filter_m777Tgl" class="controls">
                            <ba:datePicker id="search_t017Tanggal" name="search_t017Tanggal" precision="day" format="dd/MM/yyyy"  value="" />
                            &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                            <ba:datePicker id="search_t017Tanggal2" name="search_t017Tanggal2" precision="day" format="dd/MM/yyyy"  value=""  />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" for="userRole">
                            <g:message code="auditTrail.role.label" default="Status" />&nbsp;
                        </label>&nbsp;&nbsp;
                    </td>
                    <td>
                        <div id="filter_status" class="controls">
                            <input type="radio" name="search_status" id="search_status1"  value="0" />Belum Ditindak Lanjuti
                            <input type="radio" name="search_status" id="search_status2" value="1" />Sudah Ditindak Lanjuti
                            %{--<input type="radio" name="search_status" id="search_status3" value="2" />Parts Yang akan Dijual--}%
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" >
                        <div class="controls" style="right: 0">
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="view" >View</button>
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel" name="clear" id="clear" >Clear</button>

                        </div>
                    </td>
                </tr>
            </table>

        </fieldset>
        <g:render template="dataTables" />
        <fieldset class="buttons controls" style="padding-top: 10px;">
            <button id='printPartsTransferStokData' onclick="printPartsTransferStok();" type="button" class="btn btn-primary" style="visibility: hidden">Print Parts PartsTransferStok</button>
        </fieldset>

    </div>
    <div class="span7" id="partsTransferStok-form" style="display: none;"></div>
</div>
</body>
</html>

<g:javascript>
    $("input[name=search_status][value=" + 0 + "]").attr('checked', 'checked');
</g:javascript>