
<%@ page import="com.kombos.administrasi.ManPowerSertifikat" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="manPowerSertifikat_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerSertifikat.namaManPower.label" default="Nama Man Power" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerSertifikat.sertifikat.label" default="Sertifikat" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerSertifikat.t016TglAwalSertifikat.label" default="Tgl Awal Sertifikat" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerSertifikat.t016TglAkhirSertifikat.label" default="Tgl Akhir Sertifikat" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerSertifikat.t016Ket.label" default="Ket" /></div>
			</th>


		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_namaManPower" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaManPower" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_sertifikat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_sertifikat" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t016TglAwalSertifikat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t016TglAwalSertifikat" value="date.struct">
					<input type="hidden" name="search_t016TglAwalSertifikat_day" id="search_t016TglAwalSertifikat_day" value="">
					<input type="hidden" name="search_t016TglAwalSertifikat_month" id="search_t016TglAwalSertifikat_month" value="">
					<input type="hidden" name="search_t016TglAwalSertifikat_year" id="search_t016TglAwalSertifikat_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t016TglAwalSertifikat_dp" value="" id="search_t016TglAwalSertifikat" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t016TglAkhirSertifikat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t016TglAkhirSertifikat" value="date.struct">
					<input type="hidden" name="search_t016TglAkhirSertifikat_day" id="search_t016TglAkhirSertifikat_day" value="">
					<input type="hidden" name="search_t016TglAkhirSertifikat_month" id="search_t016TglAkhirSertifikat_month" value="">
					<input type="hidden" name="search_t016TglAkhirSertifikat_year" id="search_t016TglAkhirSertifikat_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t016TglAkhirSertifikat_dp" value="" id="search_t016TglAkhirSertifikat" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t016Ket" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t016Ket" class="search_init" />
				</div>
			</th>
	
		

		</tr>
	</thead>
</table>

<g:javascript>
var ManPowerSertifikatTable;
var reloadManPowerSertifikatTable;
$(function(){
	
	reloadManPowerSertifikatTable = function() {
		ManPowerSertifikatTable.fnDraw();
	}

	var recordsManPowerSertifikatPerPage = [];
    var anManPowerSertifikatSelected;
    var jmlRecManPowerSertifikatPerPage=0;
    var id;
    
	$('#search_t016TglAwalSertifikat').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t016TglAwalSertifikat_day').val(newDate.getDate());
			$('#search_t016TglAwalSertifikat_month').val(newDate.getMonth()+1);
			$('#search_t016TglAwalSertifikat_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			ManPowerSertifikatTable.fnDraw();	
	});

	

	$('#search_t016TglAkhirSertifikat').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t016TglAkhirSertifikat_day').val(newDate.getDate());
			$('#search_t016TglAkhirSertifikat_month').val(newDate.getMonth()+1);
			$('#search_t016TglAkhirSertifikat_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			ManPowerSertifikatTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	ManPowerSertifikatTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	ManPowerSertifikatTable = $('#manPowerSertifikat_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		
		"fnDrawCallback": function () {
            var rsManPowerSertifikat = $("#manPowerSertifikat_datatables tbody .row-select");
            var jmlManPowerSertifikatCek = 0;
            var nRow;
            var idRec;
            rsManPowerSertifikat.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsManPowerSertifikatPerPage[idRec]=="1"){
                    jmlManPowerSertifikatCek = jmlManPowerSertifikatCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsManPowerSertifikatPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecManPowerSertifikatPerPage = rsManPowerSertifikat.length;
            if(jmlManPowerSertifikatCek==jmlRecManPowerSertifikatPerPage && jmlRecManPowerSertifikatPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "namaManPower",
	"mDataProp": "namaManPower",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "sertifikat",
	"mDataProp": "sertifikat",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t016TglAwalSertifikat",
	"mDataProp": "t016TglAwalSertifikat",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t016TglAkhirSertifikat",
	"mDataProp": "t016TglAkhirSertifikat",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t016Ket",
	"mDataProp": "t016Ket",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}



],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var namaManPower = $('#filter_namaManPower input').val();
						if(namaManPower){
							aoData.push(
									{"name": 'sCriteria_namaManPower', "value": namaManPower}
							);
						}
	
						var sertifikat = $('#filter_sertifikat input').val();
						if(sertifikat){
							aoData.push(
									{"name": 'sCriteria_sertifikat', "value": sertifikat}
							);
						}

						var t016TglAwalSertifikat = $('#search_t016TglAwalSertifikat').val();
						var t016TglAwalSertifikatDay = $('#search_t016TglAwalSertifikat_day').val();
						var t016TglAwalSertifikatMonth = $('#search_t016TglAwalSertifikat_month').val();
						var t016TglAwalSertifikatYear = $('#search_t016TglAwalSertifikat_year').val();
						
						if(t016TglAwalSertifikat){
							aoData.push(
									{"name": 'sCriteria_t016TglAwalSertifikat', "value": "date.struct"},
									{"name": 'sCriteria_t016TglAwalSertifikat_dp', "value": t016TglAwalSertifikat},
									{"name": 'sCriteria_t016TglAwalSertifikat_day', "value": t016TglAwalSertifikatDay},
									{"name": 'sCriteria_t016TglAwalSertifikat_month', "value": t016TglAwalSertifikatMonth},
									{"name": 'sCriteria_t016TglAwalSertifikat_year', "value": t016TglAwalSertifikatYear}
							);
						}

						var t016TglAkhirSertifikat = $('#search_t016TglAkhirSertifikat').val();
						var t016TglAkhirSertifikatDay = $('#search_t016TglAkhirSertifikat_day').val();
						var t016TglAkhirSertifikatMonth = $('#search_t016TglAkhirSertifikat_month').val();
						var t016TglAkhirSertifikatYear = $('#search_t016TglAkhirSertifikat_year').val();
						
						if(t016TglAkhirSertifikat){
							aoData.push(
									{"name": 'sCriteria_t016TglAkhirSertifikat', "value": "date.struct"},
									{"name": 'sCriteria_t016TglAkhirSertifikat_dp', "value": t016TglAkhirSertifikat},
									{"name": 'sCriteria_t016TglAkhirSertifikat_day', "value": t016TglAkhirSertifikatDay},
									{"name": 'sCriteria_t016TglAkhirSertifikat_month', "value": t016TglAkhirSertifikatMonth},
									{"name": 'sCriteria_t016TglAkhirSertifikat_year', "value": t016TglAkhirSertifikatYear}
							);
						}
	
						var t016Ket = $('#filter_t016Ket input').val();
						if(t016Ket){
							aoData.push(
									{"name": 'sCriteria_t016Ket', "value": t016Ket}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#manPowerSertifikat_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsManPowerSertifikatPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsManPowerSertifikatPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#manPowerSertifikat_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsManPowerSertifikatPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anManPowerSertifikatSelected = ManPowerSertifikatTable.$('tr.row_selected');
            if(jmlRecManPowerSertifikatPerPage == anManPowerSertifikatSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsManPowerSertifikatPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
