<%@ page import="com.kombos.administrasi.Sertifikat; com.kombos.administrasi.NamaManPower; com.kombos.administrasi.ManPowerSertifikat" %>

<g:javascript>
		$(function(){
			$('#namaManPower').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/manPowerStall/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
    var checkin = $('#tanggal1').datepicker({
        onRender: function(date) {
            return '';
        }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
            checkin.hide();
            $('#tanggal2')[0].focus();
        }).data('datepicker');

        var checkout = $('#tanggal2').datepicker({
            onRender: function(date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');
    
    $(document).ready(function() {
        if($('#t016Ket').val().length>0){
            $('#hitung').text(250 - $('#t016Ket').val().length);
        }
        $('#t016Ket').keyup(function() {
            var len = this.value.length;
            if (len >= 250) {
                this.value = this.value.substring(0, 250);
                len = 250;
            }
            $('#hitung').text(250 - len);
        });
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerSertifikatInstance, field: 'namaManPower', 'error')} required">
	<label class="control-label" for="namaManPower">
		<g:message code="manPowerSertifikat.namaManPower.label" default="Nama Man Power" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:select id="namaManPower" name="namaManPower.id" from="${NamaManPower.createCriteria().list{eq("staDel", "0");order("t015NamaLengkap","asc")}}" optionValue="${{it.t015NamaLengkap+" ("+it.t015NamaBoard+") " }}" optionKey="id" required="" value="${manPowerSertifikatInstance?.namaManPower?.id}" class="many-to-one"/>--}%
        <g:textField name="namaManPower" id="namaManPower" class="typeahead" maxlength="200" value="${manPowerSertifikatInstance?.namaManPower?.t015NamaLengkap}" autocomplete="off" required="true"/>
    </div>
</div>
<g:javascript>
    document.getElementById("namaManPower").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: manPowerSertifikatInstance, field: 'sertifikat', 'error')} required">
	<label class="control-label" for="sertifikat">
		<g:message code="manPowerSertifikat.sertifikat.label" default="Sertifikat" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="sertifikat" name="sertifikat.id" from="${Sertifikat.createCriteria().list{eq("staDel", "0");order("m016NamaSertifikat","asc")}}" optionKey="id" required="" value="${manPowerSertifikatInstance?.sertifikat?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerSertifikatInstance, field: 't016TglAwalSertifikat', 'error')} required">
	<label class="control-label" for="t016TglAwalSertifikat">
		<g:message code="manPowerSertifikat.t016TglAwalSertifikat.label" default="Tgl Awal Sertifikat" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="t016TglAwalSertifikat" id="tanggal1" precision="day"  value="${manPowerSertifikatInstance?.t016TglAwalSertifikat}" format="dd/mm/yyyy" required="true" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerSertifikatInstance, field: 't016TglAkhirSertifikat', 'error')} required">
	<label class="control-label" for="t016TglAkhirSertifikat">
		<g:message code="manPowerSertifikat.t016TglAkhirSertifikat.label" default="Tgl Akhir Sertifikat"  />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="t016TglAkhirSertifikat" precision="day" id="tanggal2"  value="${manPowerSertifikatInstance?.t016TglAkhirSertifikat}" format="dd/mm/yyyy" required="true" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerSertifikatInstance, field: 't016Ket', 'error')} ">
	<label class="control-label" for="t016Ket">
		<g:message code="manPowerSertifikat.t016Ket.label" default="Ket" />
		
	</label>
	<div class="controls">
	<g:textArea name="t016Ket" id="t016Ket" value="${manPowerSertifikatInstance?.t016Ket}"/>
        <br/>
        <span id="hitung">250</span> Karakter Tersisa.
	</div>
</div>
 
<g:javascript>
    document.getElementById("namaManPower").focus();
</g:javascript>