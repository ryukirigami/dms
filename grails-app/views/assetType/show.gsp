

<%@ page import="com.kombos.finance.AssetType" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'assetType.label', default: 'AssetType')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteAssetType;

$(function(){ 
	deleteAssetType=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/assetType/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadAssetTypeTable();
   				expandTableLayout('assetType');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-assetType" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="assetType"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${assetTypeInstance?.typeAsset}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="typeAsset-label" class="property-label"><g:message
					code="assetType.typeAsset.label" default="Type Asset" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="typeAsset-label">
						%{--<ba:editableValue
								bean="${assetTypeInstance}" field="typeAsset"
								url="${request.contextPath}/AssetType/updatefield" type="text"
								title="Enter typeAsset" onsuccess="reloadAssetTypeTable();" />--}%
							
								<g:fieldValue bean="${assetTypeInstance}" field="typeAsset"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetTypeInstance?.description}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="description-label" class="property-label"><g:message
					code="assetType.description.label" default="Description" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="description-label">
						%{--<ba:editableValue
								bean="${assetTypeInstance}" field="description"
								url="${request.contextPath}/AssetType/updatefield" type="text"
								title="Enter description" onsuccess="reloadAssetTypeTable();" />--}%
							
								<g:fieldValue bean="${assetTypeInstance}" field="description"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetTypeInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="assetType.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${assetTypeInstance}" field="staDel"
								url="${request.contextPath}/AssetType/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadAssetTypeTable();" />--}%
							
								<g:fieldValue bean="${assetTypeInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetTypeInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="assetType.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${assetTypeInstance}" field="createdBy"
								url="${request.contextPath}/AssetType/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadAssetTypeTable();" />--}%
							
								<g:fieldValue bean="${assetTypeInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetTypeInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="assetType.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${assetTypeInstance}" field="updatedBy"
								url="${request.contextPath}/AssetType/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadAssetTypeTable();" />--}%
							
								<g:fieldValue bean="${assetTypeInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetTypeInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="assetType.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${assetTypeInstance}" field="lastUpdProcess"
								url="${request.contextPath}/AssetType/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadAssetTypeTable();" />--}%
							
								<g:fieldValue bean="${assetTypeInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetTypeInstance?.asset}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="asset-label" class="property-label"><g:message
					code="assetType.asset.label" default="Asset" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="asset-label">
						%{--<ba:editableValue
								bean="${assetTypeInstance}" field="asset"
								url="${request.contextPath}/AssetType/updatefield" type="text"
								title="Enter asset" onsuccess="reloadAssetTypeTable();" />--}%
							
								<g:each in="${assetTypeInstance.asset}" var="a">
								<g:link controller="asset" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetTypeInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="assetType.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${assetTypeInstance}" field="dateCreated"
								url="${request.contextPath}/AssetType/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadAssetTypeTable();" />--}%
							
								<g:formatDate date="${assetTypeInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetTypeInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="assetType.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${assetTypeInstance}" field="lastUpdated"
								url="${request.contextPath}/AssetType/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadAssetTypeTable();" />--}%
							
								<g:formatDate date="${assetTypeInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('assetType');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${assetTypeInstance?.id}"
					update="[success:'assetType-form',failure:'assetType-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteAssetType('${assetTypeInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
