<%@ page import="com.kombos.finance.AssetType" %>



<div class="control-group fieldcontain ${hasErrors(bean: assetTypeInstance, field: 'typeAsset', 'error')} required">
	<label class="control-label" for="typeAsset">
		<g:message code="assetType.typeAsset.label" default="Type Asset" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="typeAsset" required="" value="${assetTypeInstance?.typeAsset}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: assetTypeInstance, field: 'description', 'error')} required">
	<label class="control-label" for="description">
		<g:message code="assetType.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
    	%{--<g:textField name="description" required="" value="${assetTypeInstance?.description}"/>--}%
    <g:textArea name="description" required="" value="${assetTypeInstance?.description}" maxlength="256" />
	</div>
</div>
%{--

<div class="control-group fieldcontain ${hasErrors(bean: assetTypeInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="assetType.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${assetTypeInstance?.lastUpdProcess}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: assetTypeInstance, field: 'asset', 'error')} ">
	<label class="control-label" for="asset">
		<g:message code="assetType.asset.label" default="Asset" />
		
	</label>
	<div class="controls">
	
<ul class="one-to-many">
<g:each in="${assetTypeInstance?.asset?}" var="a">
    <li><g:link controller="asset" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="asset" action="create" params="['assetType.id': assetTypeInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'asset.label', default: 'Asset')])}</g:link>
</li>
</ul>

	</div>
</div>

--}%
