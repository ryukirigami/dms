
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="historyCustomerPopup_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed" style="table-layout: fixed; ">
    <col width="250px" />
    <col width="250px" />
    <col width="250px" />
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px; width:239px;">
            <div><g:message code="historyCustomer.nopol.label" default="Nama Customer" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px; width:239px;">
            <div><g:message code="historyCustomer.nopol.label" default="Peran Customer" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px; width:239px;">
            <div><g:message code="historyCustomer.nopol.label" default="Alamat Customer" /></div>
        </th>
    </tr>
    <tr>


        <th style="border-top: none;padding: 5px; width:239px;">
            <div id="filter_nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_nama" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px; width:239px;">
            <div id="filter_peran" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_peran" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px; width:239px;">
            <div id="filter_alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_alamat" class="search_init" />
            </div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var historyCustomerPopupTable;
var reloadHistoryCustomerPopupTable;
$(function(){
	reloadHistoryCustomerPopupTable = function() {
		historyCustomerPopupTable.fnDraw();
        historyCustomerPopupTable.fnReloadAjax();
	}

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	historyCustomerPopupTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});
	historyCustomerPopupTable = $('#historyCustomerPopup_datatables').dataTable({
		"sScrollX": "500px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "mappingCustomerDataTables")}",
		"aoColumns": [

{
	"sName": "namaCustomer",
	"mDataProp": "namaCustomer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "peranCustomer",
	"mDataProp": "peranCustomer",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "alamatCustomer",
	"mDataProp": "alamatCustomer",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var historyCustomerVehicleId = $('#historyCustomerVehicleIdTemp').val();
                        if(historyCustomerVehicleId){
                            aoData.push(
									{"name": 'historyCustomerVehicleId', "value": historyCustomerVehicleId}
						    );
                        }

                        var namaCustomer = $('#filter_nama input').val();
						if(namaCustomer){
							aoData.push(
									{"name": 'sCriteria_namaCustomer', "value": namaCustomer}
							);
						}

						var peranCustomer = $('#filter_peran input').val();
						if(peranCustomer){
							aoData.push(
									{"name": 'sCriteria_peranCustomer', "value": peranCustomer}
							);
						}

						var alamatCustomer = $('#filter_alamat input').val();
						if(alamatCustomer){
							aoData.push(
									{"name": 'sCriteria_alamatCustomer', "value": alamatCustomer}
							);
						}


                        $.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>



