<table id="serviceDetails_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nomor WO</div>
			</th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Nomor Invoice</div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Nama Job</div>
            </th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Parts</div>
			</th>
            <th style="border-bottom: none;padding: 5px;">
                <div>History Request</div>
            </th>
		</tr>
	</thead>
</table>

<g:javascript>
var partsRCP;
var reloadPartsRCPTable;
$(function(){
    reloadPartsRCPTable = function() {
		partsRCP.fnDraw();
	}

	partsRCP = $('#serviceDetails_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "serviceDetailDatatablesList")}",
		"aoColumns": [

{
	"sName": "nomorWO",
	"mDataProp": "nomorWO",
	"aTargets": [0],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "nomorInvoice",
	"mDataProp": "nomorInvoice",
	"aTargets": [1],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "namaJob",
	"mDataProp": "namaJob",
	"aTargets": [2],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "parts",
	"mDataProp": "parts",
	"aTargets": [3],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "historyRequest",
	"mDataProp": "historyRequest",
	"aTargets": [4],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
        				aoData.push(
									{"name": 'historyCustomerVehicleId', "value": ${historyCustomerVehicleInstance?.id}}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});

</g:javascript>
