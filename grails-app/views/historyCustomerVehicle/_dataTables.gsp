
<%@ page import="com.kombos.customerprofile.HistoryCustomerVehicle" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="historyCustomerVehicle_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyCustomerVehicle.nopol.label" default="Nomor Polisi" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyCustomerVehicle.customerVehicle.label" default="Vin Code" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyCustomerVehicle.fullModelCode.label" default="Full Model Code" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyCustomerVehicle.baseModel.label" default="Model" /></div>
			</th>    
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="dpParts.t183TglDEC.label" default="Tanggal DEC" /></div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="dpParts.t183NoMesin.label" default="Nomor Mesin" /></div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="dpParts.warna.label" default="Warna" /></div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="dpParts.t183NoKunci.label" default="Nomor Kunci" /></div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="dpParts.t183TglSTNK.label" default="Tanggal STNK" /></div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="dpParts.asuransi.label" default="Asuransi" /></div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="dpParts.asuransi.label" default="Leasing" /></div>
            </th>
		</tr>

		<tr>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_nopol" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_nopol" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
				<div id="filter_customerVehicle" style="padding-top: 0px;position:relative; margin-top: 0px;width: 105px;">
					<input type="text" name="search_customerVehicle" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
				<div id="filter_fullModelCode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_fullModelCode" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_baseModel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 100px;">
					<input type="text" name="search_baseModel" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t183TglDEC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                     <input type="hidden" name="search_t183TglDEC" value="date.struct">
                     <input type="hidden" name="search_t183TglDEC_day" id="search_t183TglDEC_day" value="">
                     <input type="hidden" name="search_t183TglDEC_month" id="search_t183TglDEC_month" value="">
                     <input type="hidden" name="search_t183TglDEC_year" id="search_t183TglDEC_year" value="">
                     <input type="text" data-date-format="dd/mm/yyyy" name="search_t183TglDEC_dp" value="" id="search_t183TglDEC" class="search_init">
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t183NoMesin" style="padding-top: 0px;position:relative; margin-top: 0px;width: 100px;">
                    <input type="text" name="search_t183NoMesin" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_warna" style="padding-top: 0px;position:relative; margin-top: 0px;width: 100px;">
                    <input type="text" name="search_warna" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t183NoKunci" style="padding-top: 0px;position:relative; margin-top: 0px;width: 100px;">
                    <input type="text" name="search_t183NoKunci" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t183TglSTNK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                     <input type="hidden" name="search_t183TglSTNK" value="date.struct">
                     <input type="hidden" name="search_t183TglSTNK_day" id="search_t183TglSTNK_day" value="">
                     <input type="hidden" name="search_t183TglSTNK_month" id="search_t183TglSTNK_month" value="">
                     <input type="hidden" name="search_t183TglSTNK_year" id="search_t183TglSTNK_year" value="">
                     <input type="text" data-date-format="dd/mm/yyyy" name="search_t183TglSTNK_dp" value="" id="search_t183TglSTNK" class="search_init">
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_asuransi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 100px;">
                    <input type="text" name="search_asuransi" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_leasing" style="padding-top: 0px;position:relative; margin-top: 0px;width: 100px;">
                    <input type="text" name="search_leasing" class="search_init" />
                </div>
            </th>

		</tr>
	</thead>
</table>

<g:javascript>
var historyCustomerVehicleTable;
var reloadHistoryCustomerVehicleTable;
$(function(){
	
	reloadHistoryCustomerVehicleTable = function() {
		historyCustomerVehicleTable.fnDraw();
        historyCustomerVehicleTable.fnReloadAjax();
	}
                 
	$('#search_t183TglDEC').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t183TglDEC_day').val(newDate.getDate());
			$('#search_t183TglDEC_month').val(newDate.getMonth()+1);
			$('#search_t183TglDEC_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			historyCustomerVehicleTable.fnDraw();
	});
	$('#search_t183TglSTNK').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t183TglSTNK_day').val(newDate.getDate());
			$('#search_t183TglSTNK_month').val(newDate.getMonth()+1);
			$('#search_t183TglSTNK_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			historyCustomerVehicleTable.fnDraw();
	});
                 
    $("#historyCustomerVehicle_datatables th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	historyCustomerVehicleTable.fnDraw();
		}
	});
	$("#historyCustomerVehicle_datatables th div input").click(function (e) {
	 	e.stopPropagation();
	});
	historyCustomerVehicleTable = $('#historyCustomerVehicle_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
	"sName": "nopol",
	"mDataProp": "nopol",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="edit('+row['id']+');">'+data+'</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{<%-- vincode --%>
	"sName": "customerVehicle",
	"mDataProp": "customerVehicle",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"170px",
	"bVisible": true
},
{ <%-- fullmodel code --%>
	"sName": "fullModelCode",
	"mDataProp": "fullModelCode",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"170px",
	"bVisible": true
},
{
	"sName": "baseModel",
	"mDataProp": "baseModel",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "t183TglDEC",
	"mDataProp": "t183TglDEC",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "t183NoMesin",
	"mDataProp": "t183NoMesin",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "warna",
	"mDataProp": "warna",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "t183NoKunci",
	"mDataProp": "t183NoKunci",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "t183TglSTNK",
	"mDataProp": "t183TglSTNK",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
,
{
	"sName": "asuransi",
	"mDataProp": "asuransi",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
,
{
	"sName": "leasing",
	"mDataProp": "leasing",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var nopol = $('#filter_nopol input').val();
						if(nopol){
							aoData.push(
									{"name": 'sCriteria_nopol', "value": nopol}
							);
						}
	
						var customerVehicle = $('#filter_customerVehicle input').val();
						if(customerVehicle){
							aoData.push(
									{"name": 'sCriteria_customerVehicle', "value": customerVehicle}
							);
						}

						var fullModelCode=$('#filter_fullModelCode input').val();
						if(fullModelCode){
						    aoData.push(
						            {"name":'sCriteria_fullModelCode',"value": fullModelCode}
						    );
						}

						var baseModel = $('#filter_baseModel input').val();
						if(baseModel){
							aoData.push(
									{"name": 'sCriteria_baseModel', "value": baseModel}
							);
						}
	
						var t183TglDEC = $('#search_t183TglDEC').val();
						var t183TglDECDay = $('#search_t183TglDEC_day').val();
						var t183TglDECMonth = $('#search_t183TglDEC_month').val();
						var t183TglDECYear = $('#search_t183TglDEC_year').val();
						if(t183TglDEC){
							aoData.push(
									{"name": 'sCriteria_t183TglDEC', "value": "date.struct"},
									{"name": 'sCriteria_t183TglDEC_dp', "value": t183TglDEC},
									{"name": 'sCriteria_t183TglDEC_day', "value": t183TglDECDay},
									{"name": 'sCriteria_t183TglDEC_month', "value": t183TglDECMonth},
									{"name": 'sCriteria_t183TglDEC_year', "value": t183TglDECYear}
							);
						}

						var t183NoMesin = $('#filter_t183NoMesin input').val();
						if(t183NoMesin){
							aoData.push(
									{"name": 'sCriteria_t183NoMesin', "value": t183NoMesin}
							);
						}

						var warna = $('#filter_warna input').val();
						if(warna){
							aoData.push(
									{"name": 'sCriteria_t183Warna', "value": warna}
							);
						}

						var t183NoKunci = $('#filter_t183NoKunci input').val();
						if(t183NoKunci){
							aoData.push(
									{"name": 'sCriteria_t183NoKunci', "value": t183NoKunci}
							);
						}

						var t183TglSTNK = $('#search_t183TglSTNK').val();
						var t183TglSTNKDay = $('#search_t183TglSTNK_day').val();
						var t183TglSTNKMonth = $('#search_t183TglSTNK_month').val();
						var t183TglSTNKYear = $('#search_t183TglSTNK_year').val();
						if(t183TglSTNK){
							aoData.push(
									{"name": 'sCriteria_t183TglSTNK', "value": "date.struct"},
									{"name": 'sCriteria_t183TglSTNK_dp', "value": t183TglSTNK},
									{"name": 'sCriteria_t183TglSTNK_day', "value": t183TglSTNKDay},
									{"name": 'sCriteria_t183TglSTNK_month', "value": t183TglSTNKMonth},
									{"name": 'sCriteria_t183TglSTNK_year', "value": t183TglSTNKYear}
							);
						}

						var asuransi = $('#filter_asuransi input').val();
						if(asuransi){
							aoData.push(
									{"name": 'sCriteria_asuransi', "value": asuransi}
							);
						}

						var leasing = $('#filter_leasing input').val();
						if(leasing){
							aoData.push(
									{"name": 'sCriteria_leasing', "value": leasing}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>