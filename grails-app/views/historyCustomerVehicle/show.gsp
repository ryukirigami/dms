<%@ page import="com.kombos.customerprofile.HistoryCustomerVehicle" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'historyCustomerVehicle.label', default: 'Vehicle')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteHistoryCustomerVehicle;

$(function(){ 
	deleteHistoryCustomerVehicle=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/historyCustomerVehicle/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadHistoryCustomerVehicleTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-historyCustomerVehicle" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="historyCustomerVehicle"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${historyCustomerVehicleInstance?.alamatNPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="alamatNPWP-label" class="property-label"><g:message
					code="historyCustomerVehicle.alamatNPWP.label" default="Alamat NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="alamatNPWP-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="alamatNPWP"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter alamatNPWP" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="alamatNPWP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.alamatPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="alamatPIC-label" class="property-label"><g:message
					code="historyCustomerVehicle.alamatPIC.label" default="Alamat PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="alamatPIC-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="alamatPIC"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter alamatPIC" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="alamatPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.alamatPerusahaan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="alamatPerusahaan-label" class="property-label"><g:message
					code="historyCustomerVehicle.alamatPerusahaan.label" default="Alamat Perusahaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="alamatPerusahaan-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="alamatPerusahaan"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter alamatPerusahaan" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="alamatPerusahaan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.historyCustomerVehicleDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="historyCustomerVehicleDealer-label" class="property-label"><g:message
					code="historyCustomerVehicle.historyCustomerVehicleDealer.label" default="HistoryCustomerVehicle Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="historyCustomerVehicleDealer-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="historyCustomerVehicleDealer"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter historyCustomerVehicleDealer" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:link controller="historyCustomerVehicleDealer" action="show" id="${historyCustomerVehicleInstance?.historyCustomerVehicleDealer?.id}">${historyCustomerVehicleInstance?.historyCustomerVehicleDealer?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="historyCustomerVehicle.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="createdBy"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="historyCustomerVehicle.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="dateCreated"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:formatDate date="${historyCustomerVehicleInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.emailPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="emailPIC-label" class="property-label"><g:message
					code="historyCustomerVehicle.emailPIC.label" default="Email PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="emailPIC-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="emailPIC"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter emailPIC" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="emailPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.fax}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="fax-label" class="property-label"><g:message
					code="historyCustomerVehicle.fax.label" default="Fax" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="fax-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="fax"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter fax" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="fax"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.hpPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="hpPIC-label" class="property-label"><g:message
					code="historyCustomerVehicle.hpPIC.label" default="Hp PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="hpPIC-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="hpPIC"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter hpPIC" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="hpPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.jabatanPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jabatanPIC-label" class="property-label"><g:message
					code="historyCustomerVehicle.jabatanPIC.label" default="Jabatan PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jabatanPIC-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="jabatanPIC"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter jabatanPIC" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:link controller="namaJabatanPIC" action="show" id="${historyCustomerVehicleInstance?.jabatanPIC?.id}">${historyCustomerVehicleInstance?.jabatanPIC?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.jenisKelaminPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jenisKelaminPIC-label" class="property-label"><g:message
					code="historyCustomerVehicle.jenisKelaminPIC.label" default="Jenis Kelamin PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jenisKelaminPIC-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="jenisKelaminPIC"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter jenisKelaminPIC" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="jenisKelaminPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.jenisPerusahaan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jenisPerusahaan-label" class="property-label"><g:message
					code="historyCustomerVehicle.jenisPerusahaan.label" default="Jenis Perusahaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jenisPerusahaan-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="jenisPerusahaan"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter jenisPerusahaan" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="jenisPerusahaan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.kabKota}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kabKota-label" class="property-label"><g:message
					code="historyCustomerVehicle.kabKota.label" default="Kab Kota" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kabKota-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="kabKota"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter kabKota" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:link controller="kabKota" action="show" id="${historyCustomerVehicleInstance?.kabKota?.id}">${historyCustomerVehicleInstance?.kabKota?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.kabKotaNPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kabKotaNPWP-label" class="property-label"><g:message
					code="historyCustomerVehicle.kabKotaNPWP.label" default="Kab Kota NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kabKotaNPWP-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="kabKotaNPWP"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter kabKotaNPWP" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:link controller="kabKota" action="show" id="${historyCustomerVehicleInstance?.kabKotaNPWP?.id}">${historyCustomerVehicleInstance?.kabKotaNPWP?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.kabKotaPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kabKotaPIC-label" class="property-label"><g:message
					code="historyCustomerVehicle.kabKotaPIC.label" default="Kab Kota PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kabKotaPIC-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="kabKotaPIC"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter kabKotaPIC" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:link controller="kabKota" action="show" id="${historyCustomerVehicleInstance?.kabKotaPIC?.id}">${historyCustomerVehicleInstance?.kabKotaPIC?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.kecamatan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kecamatan-label" class="property-label"><g:message
					code="historyCustomerVehicle.kecamatan.label" default="Kecamatan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kecamatan-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="kecamatan"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter kecamatan" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:link controller="kecamatan" action="show" id="${historyCustomerVehicleInstance?.kecamatan?.id}">${historyCustomerVehicleInstance?.kecamatan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.kecamatanNPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kecamatanNPWP-label" class="property-label"><g:message
					code="historyCustomerVehicle.kecamatanNPWP.label" default="Kecamatan NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kecamatanNPWP-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="kecamatanNPWP"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter kecamatanNPWP" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:link controller="kecamatan" action="show" id="${historyCustomerVehicleInstance?.kecamatanNPWP?.id}">${historyCustomerVehicleInstance?.kecamatanNPWP?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.kecamatanPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kecamatanPIC-label" class="property-label"><g:message
					code="historyCustomerVehicle.kecamatanPIC.label" default="Kecamatan PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kecamatanPIC-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="kecamatanPIC"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter kecamatanPIC" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:link controller="kecamatan" action="show" id="${historyCustomerVehicleInstance?.kecamatanPIC?.id}">${historyCustomerVehicleInstance?.kecamatanPIC?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.kelurahan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kelurahan-label" class="property-label"><g:message
					code="historyCustomerVehicle.kelurahan.label" default="Kelurahan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kelurahan-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="kelurahan"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter kelurahan" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:link controller="kelurahan" action="show" id="${historyCustomerVehicleInstance?.kelurahan?.id}">${historyCustomerVehicleInstance?.kelurahan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.kelurahanNPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kelurahanNPWP-label" class="property-label"><g:message
					code="historyCustomerVehicle.kelurahanNPWP.label" default="Kelurahan NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kelurahanNPWP-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="kelurahanNPWP"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter kelurahanNPWP" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:link controller="kelurahan" action="show" id="${historyCustomerVehicleInstance?.kelurahanNPWP?.id}">${historyCustomerVehicleInstance?.kelurahanNPWP?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.kelurahanPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kelurahanPIC-label" class="property-label"><g:message
					code="historyCustomerVehicle.kelurahanPIC.label" default="Kelurahan PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kelurahanPIC-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="kelurahanPIC"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter kelurahanPIC" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:link controller="kelurahan" action="show" id="${historyCustomerVehicleInstance?.kelurahanPIC?.id}">${historyCustomerVehicleInstance?.kelurahanPIC?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.kodePerusahaan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kodePerusahaan-label" class="property-label"><g:message
					code="historyCustomerVehicle.kodePerusahaan.label" default="Kode Perusahaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kodePerusahaan-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="kodePerusahaan"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter kodePerusahaan" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="kodePerusahaan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.kodePos}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kodePos-label" class="property-label"><g:message
					code="historyCustomerVehicle.kodePos.label" default="Kode Pos" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kodePos-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="kodePos"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter kodePos" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="kodePos"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.kodePosNPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kodePosNPWP-label" class="property-label"><g:message
					code="historyCustomerVehicle.kodePosNPWP.label" default="Kode Pos NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kodePosNPWP-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="kodePosNPWP"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter kodePosNPWP" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="kodePosNPWP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.kodePosPic}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kodePosPic-label" class="property-label"><g:message
					code="historyCustomerVehicle.kodePosPic.label" default="Kode Pos Pic" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kodePosPic-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="kodePosPic"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter kodePosPic" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="kodePosPic"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="historyCustomerVehicle.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="lastUpdProcess"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="historyCustomerVehicle.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="lastUpdated"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:formatDate date="${historyCustomerVehicleInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.namaBelakangPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaBelakangPIC-label" class="property-label"><g:message
					code="historyCustomerVehicle.namaBelakangPIC.label" default="Nama Belakang PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaBelakangPIC-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="namaBelakangPIC"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter namaBelakangPIC" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="namaBelakangPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.namaDepanPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaDepanPIC-label" class="property-label"><g:message
					code="historyCustomerVehicle.namaDepanPIC.label" default="Nama Depan PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaDepanPIC-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="namaDepanPIC"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter namaDepanPIC" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="namaDepanPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.namaPerusahaan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaPerusahaan-label" class="property-label"><g:message
					code="historyCustomerVehicle.namaPerusahaan.label" default="Nama Perusahaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaPerusahaan-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="namaPerusahaan"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter namaPerusahaan" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="namaPerusahaan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.noNPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="noNPWP-label" class="property-label"><g:message
					code="historyCustomerVehicle.noNPWP.label" default="No NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="noNPWP-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="noNPWP"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter noNPWP" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="noNPWP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.provinsi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="provinsi-label" class="property-label"><g:message
					code="historyCustomerVehicle.provinsi.label" default="Provinsi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="provinsi-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="provinsi"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter provinsi" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:link controller="provinsi" action="show" id="${historyCustomerVehicleInstance?.provinsi?.id}">${historyCustomerVehicleInstance?.provinsi?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.provinsiNPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="provinsiNPWP-label" class="property-label"><g:message
					code="historyCustomerVehicle.provinsiNPWP.label" default="Provinsi NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="provinsiNPWP-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="provinsiNPWP"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter provinsiNPWP" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:link controller="provinsi" action="show" id="${historyCustomerVehicleInstance?.provinsiNPWP?.id}">${historyCustomerVehicleInstance?.provinsiNPWP?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.provinsiPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="provinsiPIC-label" class="property-label"><g:message
					code="historyCustomerVehicle.provinsiPIC.label" default="Provinsi PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="provinsiPIC-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="provinsiPIC"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter provinsiPIC" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:link controller="provinsi" action="show" id="${historyCustomerVehicleInstance?.provinsiPIC?.id}">${historyCustomerVehicleInstance?.provinsiPIC?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.rtPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="rtPIC-label" class="property-label"><g:message
					code="historyCustomerVehicle.rtPIC.label" default="Rt PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="rtPIC-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="rtPIC"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter rtPIC" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="rtPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.rwPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="rwPIC-label" class="property-label"><g:message
					code="historyCustomerVehicle.rwPIC.label" default="Rw PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="rwPIC-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="rwPIC"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter rwPIC" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="rwPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="historyCustomerVehicle.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="staDel"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.t101GelarB}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t101GelarB-label" class="property-label"><g:message
					code="historyCustomerVehicle.t101GelarB.label" default="T101 Gelar B" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t101GelarB-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="t101GelarB"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter t101GelarB" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="t101GelarB"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.t101GelarD}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t101GelarD-label" class="property-label"><g:message
					code="historyCustomerVehicle.t101GelarD.label" default="T101 Gelar D" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t101GelarD-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="t101GelarD"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter t101GelarD" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="t101GelarD"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.t101Ket}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t101Ket-label" class="property-label"><g:message
					code="historyCustomerVehicle.t101Ket.label" default="T101 Ket" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t101Ket-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="t101Ket"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter t101Ket" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="t101Ket"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.t101xNamaDivisi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t101xNamaDivisi-label" class="property-label"><g:message
					code="historyCustomerVehicle.t101xNamaDivisi.label" default="T101x Nama Divisi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t101xNamaDivisi-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="t101xNamaDivisi"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter t101xNamaDivisi" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="t101xNamaDivisi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.t101xNamaUser}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t101xNamaUser-label" class="property-label"><g:message
					code="historyCustomerVehicle.t101xNamaUser.label" default="T101x Nama User" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t101xNamaUser-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="t101xNamaUser"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter t101xNamaUser" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="t101xNamaUser"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.telp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="telp-label" class="property-label"><g:message
					code="historyCustomerVehicle.telp.label" default="Telp" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="telp-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="telp"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter telp" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="telp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.telpPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="telpPIC-label" class="property-label"><g:message
					code="historyCustomerVehicle.telpPIC.label" default="Telp PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="telpPIC-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="telpPIC"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter telpPIC" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="telpPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.tglLahirPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tglLahirPIC-label" class="property-label"><g:message
					code="historyCustomerVehicle.tglLahirPIC.label" default="Tgl Lahir PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tglLahirPIC-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="tglLahirPIC"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter tglLahirPIC" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:formatDate date="${historyCustomerVehicleInstance?.tglLahirPIC}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="historyCustomerVehicle.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="updatedBy"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyCustomerVehicleInstance?.website}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="website-label" class="property-label"><g:message
					code="historyCustomerVehicle.website.label" default="Website" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="website-label">
						%{--<ba:editableValue
								bean="${historyCustomerVehicleInstance}" field="website"
								url="${request.contextPath}/HistoryCustomerVehicle/updatefield" type="text"
								title="Enter website" onsuccess="reloadHistoryCustomerVehicleTable();" />--}%
							
								<g:fieldValue bean="${historyCustomerVehicleInstance}" field="website"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${historyCustomerVehicleInstance?.id}"
					update="[success:'historyCustomerVehicle-form',failure:'historyCustomerVehicle-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteHistoryCustomerVehicle('${historyCustomerVehicleInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
