<%@ page import="com.kombos.customerprofile.HistoryCustomerVehicle" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'historyCustomerVehicle.label', default: 'Customer Vehicle')}" />
		<title>Customer Vehicle</title>
		<r:require modules="baseapplayout" />
		<g:javascript>
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var loadCustomerModal;
	$(function(){
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/historyCustomerVehicle/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/historyCustomerVehicle/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#historyCustomerVehicle-form').empty();
    	$('#historyCustomerVehicle-form').append(data);
   	}
    
    shrinkTableLayout = function(){
        $("#historyCustomerVehicle-table").hide(); 
        $("#historyCustomerVehicle-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   	    var oTable = $('#historyCustomerVehicle_datatables').dataTable();
        oTable.fnReloadAjax();
   		$("#historyCustomerVehicle-table").show(); 
   		$("#historyCustomerVehicle-form").css("display","none");
   	}


    massDelete = function() {
   		var recordsToDelete = [];
		$("#historyCustomerVehicle-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/historyCustomerVehicle/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadHistoryCustomerVehicleTable();
    		}
		});
		
   	}

});
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
//    function isAlphabetKey(evt)
//    {
//        var charCode = (evt.which) ? evt.which : evt.keyCode
//        if (charCode > 31 && (charCode < 65 || charCode > 122))
//            return false;
//        return true;
//    }

    isAlphabetKey = function(e) {
        e = e || event;
        return /[a-zA-Z-]/i.test(
                String.fromCharCode(e.charCode || e.keyCode)
        ) || !e.charCode && e.keyCode  < 48;
    }

        loadCustomerModal = function(){
                    $("#customerModal").modal({
                        "backdrop" : "dynamic",
                        "keyboard" : true,
                        "show" : true
                    }).css({'width': '500px','margin-left': function () {return -($(this).width() / 2);}});
        };
        </g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">${message(code: 'historyCustomerVehicle.label', default: 'Customer Vehicle')}</span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="historyCustomerVehicle-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span12" id="historyCustomerVehicle-form" style="display: none;margin-left: 0;"></div>
	</div>

    <div id="customerModal" class="modal hide">
        <div class="modal-body" style="max-height: 500px;">
            <div id="customerContent">
                <g:render template="mappingCustomerDataTables" />
            </div>
            <div class="iu-content"></div>
        </div>
        <div class="modal-footer">
            <button id='selectCustomerDetail' type="button" class="btn btn-primary" onclick="selectData();">Select</button>
            <button id='closeCustomerDetail' type="button" class="btn btn-primary">Close</button>
        </div>
    </div>
    </body>
</html>
