<%@ page import="com.kombos.customerprofile.HistoryCustomerVehicle" %>
<script>

    var changeFullModel;
    $(function () {
        $("#btnSave").click(function() {
            var vincode = $('#vincode').val()
            var fullModelCode = $('#fullModelCode').val()
            var wmi = $('#wmi').val()
            var vds = $('#vds').val()
            var vis = $('#vis').val()
            var bulan = $('#bulan').val()
            var tahun = $('#tahun').val()
            var cekDigit = $('#cekDigit').val()
            $.ajax({
                url:'${request.contextPath}/historyCustomerVehicle/addVincode',
                type: "POST", // Always use POST when deleting data
                data : {vincode:vincode, fullModelCode:fullModelCode, wmi:wmi, vds:vds,vis:vis,cekDigit:cekDigit,
                    bulan:bulan, tahun:tahun},
                success : function(data){
                    if(data=="sukses"){
                        toastr.success("Sukses");
                    }else if(data=="update"){
                        alert("Vincode sudah ada, data di-update");
                    }else{
                        toastr.error("Gagal");
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                }
            });
            oFormService.fnHideVehicleDataTable();
        });
        $("#btnAddVehicle").click(function() {
            oFormService.fnShowVehicleDataTable();
        })
        oFormService = {
            fnShowVehicleDataTable: function() {
                $("#vehicleModal").modal("show");
                $("#vehicleModal").fadeIn();
            },
            fnHideVehicleDataTable: function() {
                $("#vehicleModal").modal("hide");
            }
        }
        $('#customerVehicles').typeahead({
            source: function (query, process) {
                var word = $('#customerVehicles').val();
                return $.get('${request.contextPath}/historyCustomerVehicle/getVincodes?word='+word, { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });
        $('#fullModelCode').typeahead({
            source: function (query, process) {
                var word = $('#fullModelCode').val();
                return $.get('${request.contextPath}/historyCustomerVehicle/getFullModelCodes?word='+word, { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });
        $("#customerModal").on("show", function() {
            $("#closeCustomerDetail").on("click", function(e) {
                $("#customerModal").modal('hide');
            });
        });
        $("#customerModal").on("hide", function() {
            $("#closeCustomerDetail").off("click");
        });
        selectData = function(){
            var row = 0;
            var rowSelect = 0;
            $("#historyCustomerPopup_datatables tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    doSelect(id);
                    rowSelect=rowSelect+1;
                }
                row=row+1;
            });
            if(row==0){
                alert('Data tidak ditemukan');
                return false;
            }else{
                if(rowSelect==0)alert('Silahkan pilih terlebih dahulu');
                return false;
            }
        };
        doSelect = function(a) {
            $.ajax({
                type: 'POST',
                url: '${request.contextPath}/historyCustomerVehicle/addNewCustomer',
                data: {customerId:a},
                success: function (data) {
                    $("#customerModal").modal('hide');
                    reloadHistoryCustomerTable();
                },
                error: function () {
                    alert('Save Failed');
                    return false;
                },
                complete: function () {
                    $('#spinner').fadeOut();
                }
            });
        };

    });

    function changeVincode(){
        var valVincode = $('#customerVehicles').val();
        jQuery.getJSON('${request.contextPath}/historyCustomerVehicle/changeVincode?vinCode='+valVincode, function (data) {
            if (data) {
                jQuery.each(data.res, function (index, value) {
                    $('#fullModelCodeId').val(value.id);
                    $('#fullModelCodeName').val(value.fullModelCode);
                });
                $('#statusImport').val(data.staImport);
                $('#model').val(data.baseModel);
                $("#mssgVincode").val(data.mssgVincode);
                $("#idVehicleUsed").val(data.idUsed);
                $("#staVinUsed").val(data.staVinUsed);
            }

        });
    }

    function disableSpace(event){
        var charCode = (event.which) ? event.which : event.keyCode
        if (charCode == 32) {
            return false;
        }
    }

    var nomorBelakang = $('#t183NoPolBelakang');
    nomorBelakang.bind('keypress', inputNoPolisi).keyup(function() {
        var val = $(this).val()
        $(this).val(val.toUpperCase())
    });
    function inputNoPolisi(e){
        var code = (e.keyCode ? e.keyCode : e.which);
    }

    var namaSTNK = $('#t183NamaSTNK');
    namaSTNK.bind('keypress', inputSTNK).keyup(function() {
        var val = $(this).val()
        $(this).val(val.toUpperCase())
    });
    function inputSTNK(e){
        var code = (e.keyCode ? e.keyCode : e.which);
    }

</script>
<div class="row-fluid">
	<div id="vehicleDetail" class="span4" style="width: 500px">
		<fieldset>
			<legend>Vehicle Detail</legend>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 't183NoPolTengah', 'error')} required">
                <label class="control-label" for="kodeKotaNoPol">
                    <span>Nomor Polisi</span>
                    <span class="required-indicator">*</span>
                </label>
                <div class="controls historyCustomerVehicle">
                    <g:select id="kodeKotaNoPol" name="kodeKotaNoPol.id" from="${com.kombos.administrasi.KodeKotaNoPol.list()}"
                      optionKey="id" optionValue="m116ID" required="" value="${historyCustomerVehicleInstance?.kodeKotaNoPol?.id}" class="many-to-one" style="width: 50px"  />
                    <g:textField name="t183NoPolTengah" value="${historyCustomerVehicleInstance?.t183NoPolTengah}" style="width: 95px" maxlength="5" onkeypress="return isNumberKey(event)" required="" />
                    <g:textField name="t183NoPolBelakang" value="${historyCustomerVehicleInstance?.t183NoPolBelakang}" maxlength="4" style="width: 50px"  onkeypress="return isAlphabetKey(event)"  />
                    <g:hiddenField name="mssgVincode" id="mssgVincode" value="Apakah anda yakin?" />
                    <g:hiddenField name="idVehicleUsed" id="idVehicleUsed" />
                    <g:hiddenField name="staVinUsed" id="staVinUsed" value="0" />
                </div>
            </div>
            <g:javascript>
                document.getElementById("kodeKotaNoPol").focus();
            </g:javascript>
			<div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 't183NamaSTNK', 'error')} required">
				<label class="control-label" for="t183NamaSTNK">
                    <span>Nama STNK</span>
                    <span class="required-indicator">*</span>
				</label>
				<div class="controls historyCustomerVehicle">
					<g:textField name="t183NamaSTNK" value="${historyCustomerVehicleInstance?.t183NamaSTNK}" maxlength="50" required="" />
				</div>
			</div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 't183AlamatSTNK', 'error')} required">
                <label class="control-label" for="t183AlamatSTNK">
                    <span>Alamat STNK</span>
                    <span class="required-indicator">*</span>
                </label>
                <div class="controls historyCustomerVehicle">
                    <g:textArea name="t183AlamatSTNK" value="${historyCustomerVehicleInstance?.t183AlamatSTNK}" rows="2" cols="50" maxlength="250" required="" />
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 'dealerPenjual', 'error')} required">
            	<label class="control-label" for="dealerPenjual">
                    <span>Main Dealer</span>
                    <span class="required-indicator">*</span>
            	</label>
            	<div class="controls">
            	    <g:select id="dealerPenjual" name="dealerPenjual.id" from="${com.kombos.administrasi.DealerPenjual.createCriteria().list {eq("staDel","0");order("m091NamaDealer");}}" optionKey="id" value="${historyCustomerVehicleInstance?.dealerPenjual?.id}" class="many-to-one"/>
            	</div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 't183TglDEC', 'error')}">
                <label class="control-label" for="t183TglDEC">
                    <span>Tanggal DEC</span>
                    <span class="required-indicator">*</span>
                </label>
                <div class="controls">
                    <ba:datePicker name="t183TglDEC" precision="day" value="${historyCustomerVehicleInstance?.t183TglDEC}" format="dd/MM/yyyy" />
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 'customerVehicle', 'error')} required">
                <label class="control-label" for="customerVehicles">
                    <span>Vin Code</span>
                    <span class="required-indicator">*</span>
                </label>
                <div class="controls">
                    <g:textField name="customerVehicles" onkeypress="return disableSpace(event);" id="customerVehicles" required="" onblur="changeVincode();" class="typeahead" value="${historyCustomerVehicleInstance?.customerVehicle?.t103VinCode}" autocomplete="off"/>
                    <g:field type="button" style="height: 20px;width: 20px" class="btn cancel" name="add" id="btnAddVehicle" value="${message(code: 'historyCustomer.add.label', default: '+')}" />
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 'fullModelCode', 'error')} required">
                <label class="control-label" for="fullModelCode">
                    <span>Full Model Code</span>
                    <span class="required-indicator">*</span>
                </label>
                <div class="controls">
                    <g:hiddenField id="fullModelCodeId" name="fullModelCodeId" value="${historyCustomerVehicleInstance?.fullModelCode?.id}" />
                    <g:textField id="fullModelCodeName" name="fullModelCodeNama" required="" readonly="" value="${historyCustomerVehicleInstance?.fullModelCode?.t110FullModelCode}"/>
                </div>
            </div>
            <div class="control-group fieldcontain">
                <label class="control-label" for="statusImport">
                    <span>Status Import</span>
                </label>
                <div class="controls historyCustomerVehicle">
                    <g:textField name="statusImport" id="statusImport" value="${historyCustomerVehicleInstance?.fullModelCode?.staImport?.m100NamaStaImport}" readonly="readonly"/>
                </div>
            </div>
            <div class="control-group fieldcontain">
                <label class="control-label" for="model">
                    <span>Model</span>
                </label>
                <div class="controls historyCustomerVehicle">
                    <g:textField name="model" id="model" value="${historyCustomerVehicleInstance?.fullModelCode?.baseModel?.m102NamaBaseModel}" readonly="readonly"/>
                </div>
            </div>
            %{--<div class="control-group fieldcontain">--}%
                %{--<label class="control-label" for="t183ThnBlnRakit_tahun">--}%
                    %{--<span>Tahun & Bulan Rakit</span>--}%
                %{--</label>--}%
                %{--<div class="controls historyCustomerVehicle">--}%
                    %{--<g:textField name="t183ThnBlnRakit_tahun" value="${historyCustomerVehicleInstance?.fullModelCode?.baseModel?.m102NamaBaseModel}" readonly="readonly" />--}%
                %{--</div>--}%
            %{--</div>--}%
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 't183NoMesin', 'error')} ">
                <label class="control-label" for="t183NoMesin">
                     <span>Nomor Mesin</span>
                </label>
                <div class="controls historyCustomerVehicle">
                    <g:textField name="t183NoMesin" value="${historyCustomerVehicleInstance?.t183NoMesin}" maxlength="20" />
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 't183NoKunci', 'error')}">
                <label class="control-label" for="t183NoKunci">
                    <span>Nomor Kunci</span>
                </label>
                <div class="controls historyCustomerVehicle">
                    <g:textField name="t183NoKunci" value="${historyCustomerVehicleInstance?.t183NoKunci}" maxlength="5" onkeypress="return isNumberKey(event)" />
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 'warna', 'error')} required">
            	<label class="control-label" for="warna">
                    <span>Warna || Kode Warna</span>
                    <span class="required-indicator">*</span>
            	</label>
            	<div class="controls">
            	    <g:select id="warna" name="warna.id" noSelection="['':'Pilih Warna Mobil']" from="${com.kombos.customerprofile.Warna.list()}" optionKey="id"  value="${historyCustomerVehicleInstance?.warna?.id}" class="many-to-one"/>
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 't183TglSTNK', 'error')} required">
                <label class="control-label" for="t183TglSTNK">
                    <span>Tanggal STNK</span>
                    <span class="required-indicator">*</span>
                </label>
                <div class="controls">
                    <ba:datePicker name="t183TglSTNK" precision="day" value="${historyCustomerVehicleInstance?.t183TglSTNK}" format="dd/MM/yyyy" required="true"/>
                </div>
            </div>

            <div class="control-group ${hasErrors(bean: historyCustomerVehicleInstance, field: 'vendorAsuransi', 'error')}">
                <label class="control-label" for="vendorAsuransi">
                    <g:message code="historyCustomer.vendorAsuransi.label" default="Vendor Asuransi"/>
                </label>
                <div class="controls">
                    <g:select id="vendorAsuransi" name="vendorAsuransi.id" from="${com.kombos.administrasi.VendorAsuransi.findAll('from VendorAsuransi a where a.staDel = 0 order by a.m193Nama')}" optionKey="id"
                              value="${historyCustomerVehicleInstance?.vendorAsuransi?.id}" class="many-to-one" noSelection="['':'Silahkan Pilih']" style="width: 175px;"/>
                </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 't183NamaSales', 'error')} ">
                <label class="control-label" for="t183NamaSales">
                    <span>Nomor Sales</span>
                </label>
                <div class="controls historyCustomerVehicle">
                    <g:textField name="t183NamaSales" value="${historyCustomerVehicleInstance?.t183NamaSales}" maxlength="20" />
                </div>
            </div>
            <div class="control-group ${hasErrors(bean: historyCustomerVehicleInstance, field: 'leasing', 'error')}">
                <label class="control-label" for="leasing">
                    <g:message code="historyCustomer.leasing.label" default="Vendor Leasing"/>
                </label>
                <div class="controls">
                    <g:select id="leasing" name="leasing.id" from="${com.kombos.administrasi.Leasing.findAll('from Leasing vl where vl.staDel = 0 order by vl.m1000NamaLeasing')}" optionKey="id" optionValue="m1000NamaLeasing"
                              value="${historyCustomerVehicleInstance?.leasing?.id}" class="many-to-one" noSelection="['':'Silahkan Pilih']" style="width: 175px;"/>
                </div>
            </div>

            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 't183StaGantiPemilik', 'error')} ">
            	<label class="control-label" for="t183StaGantiPemilik">
                    <span>Status Ganti Pemilik</span>
            	</label>
            	<div class="controls">
            	<g:radioGroup name="t183StaGantiPemilik" values="['1','0']" labels="['Ya','Tidak']" value="${historyCustomerVehicleInstance?.t183StaGantiPemilik}" >
                    ${it.radio} ${it.label}
                </g:radioGroup>
                   </div>
            </div>
            <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerVehicleInstance, field: 't183StaTaxi', 'error')} ">
            	<label class="control-label" for="t183StaTaxi">
                    <span>Apakah Taxi</span>
            	</label>
            	<div class="controls">
            	<g:radioGroup name="t183StaTaxi" values="['1','0']" labels="['Ya','Tidak']" value="${historyCustomerVehicleInstance?.t183StaTaxi}" >
                    ${it.radio} ${it.label}
                </g:radioGroup>
                   </div>
            </div>

		</fieldset>
	</div>
    <g:if test="${historyCustomerVehicleInstance?.id}">
	<div class="span8 row-fluid" style="width: 735px; margin-left: 20px">
		<div id="historyCustomerVehicleForm" >
			<fieldset>
				<legend>History Vehicle</legend>
			</fieldset>
            <g:render template="historyCustomerVehicleDataTables" />
		</div>
        <div id="historyServiceForm" >
            <fieldset>
                <legend>History Service</legend>
            </fieldset>
              <g:render template="historyServiceDataTables" />
        </div>
        <div id="serviceDetailForm">
            <fieldset>
                <legend>Service Detail</legend>
            </fieldset>
              <g:render template="serviceDetailDataTables" />
        </div>
	</div>
    </g:if>
</div>
<g:if test="${historyCustomerVehicleInstance?.id}">
<div class="row-fluid">
    <div id="historyCustomerForm" class="span4" style="width: 400px">
        <fieldset>
            <legend>History Customer</legend>
        </fieldset>
        <a class="btn btn-primary create" href="#" onclick="loadCustomerModal();">Add New Customer...</a>
        <g:render template="historyCustomerDataTables" />
    </div>
    <div id="historyRetentionForm" class="span8" style="width: 735px; margin-left: 20px">
        <fieldset>
            <legend>History Retention</legend>
        </fieldset>
          <g:render template="historyRetentionDataTables" />
    </div>
</div>

</g:if>
<div id="vehicleModal" class="modal hide">
    <div class="modal-dialog" style="width: 500px;">
        <div class="modal-content" style="width: 500px;">
            <div class="modal-header">
                <a class="close" href="javascript:void(0);" data-dismiss="modal">×</a>
                <h6>Tambah Data Vincode</h6>
            </div>
            <!-- dialog body -->
            <div class="modal-body" id="vehicleModal-body" style="max-height: 700px;">
                <div class="box">
                    <div class="span12">
                        <fieldset>
                            <table>
                                <tr style="display:table-row;">
                                    <td style="display:table-cell; padding:5px; width: 40px;">
                                        <label class="control-label" for="sCriteria_nama">Kode Vincode *</label>
                                    </td>
                                    <td style="display:table-cell; padding:5px;">
                                        <input type="text" id="vincode" style="width: 230px">
                                    </td>
                                </tr>
                                <tr style="display:table-row;">
                                    <td style="display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_nama">Full ModelCode *</label>
                                    </td>
                                    <td style="display:table-cell; padding:5px;">
                                        <input type="text" id="fullModelCode" style="width: 230px">
                                    </td>
                                </tr>
                                <tr style="display:table-row;">
                                    <td style="display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_nama">WMI *</label>
                                    </td>
                                    <td style="display:table-cell; padding:5px;">
                                        <input type="text" id="wmi" style="width: 230px">
                                    </td>
                                </tr>
                                <tr style="display:table-row;">
                                    <td style="display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_nama">VDS *</label>
                                    </td>
                                    <td style="display:table-cell; padding:5px;">
                                        <input type="text" id="vds" style="width: 230px">
                                    </td>
                                </tr>
                                <tr style="display:table-row;">
                                    <td style="display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_nama">Cek Digit *</label>
                                    </td>
                                    <td style="display:table-cell; padding:5px;">
                                        <input type="text" id="cekDigit" style="width: 230px">
                                    </td>
                                </tr>
                                <tr style="display:table-row;">
                                    <td style="display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_nama">VIS *</label>
                                    </td>
                                    <td style="display:table-cell; padding:5px;">
                                        <input type="text" id="vis" style="width: 230px">
                                    </td>
                                </tr>
                                <tr style="display:table-row;">
                                    <td style="display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_nama">Bulan & Tahun *</label>
                                    </td>
                                    <td style="display:table-cell; padding:5px;">
                                        <input type="text" id="bulan" style="width: 70px" maxlength="2">
                                        <input type="text" id="tahun" style="width: 95px" maxlength="4">
                                    </td>
                                </tr>

                            </table>
                        </fieldset>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn cancel" onclick="oFormService.fnHideVehicleDataTable();">
                    Tutup
                </a>
                <a href="javascript:void(0);" id="btnSave" class="btn btn-primary">
                   Save
                </a>
            </div>
        </div>
    </div>
</div>