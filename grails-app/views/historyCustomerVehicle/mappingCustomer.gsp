
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title>Add Customer</title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>

    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left">Add Customer</span>
    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
</div>
<div class="box">
    <div class="span12" id="customerDetail-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <g:render template="mappingCustomerDataTables"/>
        %{--<div class="pull-right">--}%
            %{--<g:field type="button" class="btn btn-primary" onclick="sendApproval();" name="send" id="send" value="Send" />--}%
            %{--<g:field type="button" class="btn cancel" name="close" id="close" value="Close" data-dismiss="modal" />--}%
        %{--</div>--}%
    </div>
    <div class="span7" id="customerDetail-form" style="display: none;"></div>
</div>
</body>
</html>
