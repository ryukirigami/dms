<%@ page import="com.kombos.customerprofile.HistoryCustomerVehicle" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'historyCustomerVehicle.label', default: 'Customer Vehicle')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
	<body>
		<div id="edit-historyCustomerVehicle" class="content scaffold-edit" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${historyCustomerVehicleInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${historyCustomerVehicleInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
            <g:javascript>
                var submitForm;
                $(function(){
                    submitForm = function(event){
                        var staUsed = $("#staVinUsed").val();
                        var idUsed = $("#idVehicleUsed").val();
                        var a = $("#t183NoPolTengah").val();
                        var b = $("#t183NoPolBelakang").val();
                        var c = $("#kodeKotaNoPol").val();

                        if(staUsed=="1" && idUsed!="${historyCustomerVehicleInstance?.id}"){%{--}else if(a == "${historyCustomerVehicleInstance?.t183NoPolTengah}" && b == "${historyCustomerVehicleInstance?.t183NoPolBelakang}" && b == "${historyCustomerVehicleInstance?.t183NoPolBelakang}"){--}%
                        %{----}%
                        %{--}--}%
                         }else if(c!="${historyCustomerVehicleInstance?.kodeKotaNoPol}" && a!="${historyCustomerVehicleInstance?.t183NoPolTengah}" && b!="${historyCustomerVehicleInstance?.t183NoPolBelakang}"){
                            alert("Nomor Polisi Sudah Ada")
                        }
                        else{
                            bootbox.confirm("Apakah anda yakin?", function(result) {
                            if(result){
                                event.stopPropagation();
                                $.ajax({
                                    type:'POST',
                                    data:jQuery('#formVehicleUpdate').serialize(),
                                    url:'${request.getContextPath()}/historyCustomerVehicle/update',
                                    success:function(data,textStatus){
                                        toastr.success("Data berhasil disimpan");
                                        jQuery('#historyCustomerVehicle-form').html(data);
                                        reloadHistoryCustomerVehicleTable();
                                    },
                                    error:function(XMLHttpRequest,textStatus,errorThrown){}
                                });
                            }});
                        }
                    }

                })
            </g:javascript>
            <form id="formVehicleUpdate" class="form-horizontal" action="${request.getContextPath()}/historyCustomerVehicle/update" method="post" onsubmit="submitForm(event);return false;">
                <g:hiddenField name="id" value="${historyCustomerVehicleInstance?.id}" />
                <g:hiddenField name="version" value="${historyCustomerVehicleInstance?.version}" />
                <fieldset class="form">
                    <g:render template="form"/>
                </fieldset>
                <fieldset class="buttons controls">
                    <g:actionSubmit class="btn btn-primary update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="loadPath('historyCustomerVehicle');"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                </fieldset>
            </form>

		</div>
	</body>
</html>