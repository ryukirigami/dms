
<div class="tabbable control-group">
	<ul class="nav nav-tabs">
		<li id="tab_cc_tpss_survey" class="active"><a href="#cc_tpss_survey" data-toggle="tab">TPSS Survey</a></li>
		<li id="tab_cc_jd_power_survey" class=""><a href="#cc_jd_power_survey" data-toggle="tab">JD Power Survey</a></li>
        <li id="tab_cc_field_action" class=""><a href="#cc_field_action" data-toggle="tab">Field Action</a></li>
        <li id="tab_cc_mrs" class=""><a href="#cc_mrs" data-toggle="tab">MRS</a></li>
	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="cc_tpss_survey">
            <table id="tpss_survey_datatables" cellpadding="0" cellspacing="0"
            	border="0"
            	class="display table table-striped table-bordered table-hover"
            	width="100%">
            	<thead>
            		<tr>
            			<th style="border-bottom: none;padding: 5px;">
            				<div>Tanggal</div>
            			</th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div>Feedback</div>
            			</th>
            		</tr>
            	</thead>
            </table>

            <g:javascript>
            var tpssSurveyTable;
            var reloadTpssSurveyTable;
            $(function(){
                reloadTpssSurveyTable = function() {
            		tpssSurveyTable.fnDraw();
            	}

            	tpssSurveyTable = $('#tpss_survey_datatables').dataTable({
            		"sScrollX": "100%",
            		"bScrollCollapse": true,
            		"bAutoWidth" : false,
            		"bPaginate" : true,
            		"sInfo" : "",
            		"sInfoEmpty" : "",
            		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
            		bFilter: true,
            		"bStateSave": false,
            		'sPaginationType': 'bootstrap',
            		"fnInitComplete": function () {
            			this.fnAdjustColumnSizing(true);
            		   },
            		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            			return nRow;
            		   },
            		"bSort": true,
            		"bProcessing": true,
            		"bServerSide": true,
            		"sServerMethod": "POST",
            		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
            		"sAjaxSource": "${g.createLink(action: "tpssSurveyDatatablesList")}",
            		"aoColumns": [

            {
            	"sName": "tanggal",
            	"mDataProp": "tanggal",
            	"aTargets": [0],
            	"bSortable": true,
            	"sWidth":"100px",
            	"bVisible": true
            },
            {
            	"sName": "feedback",
            	"mDataProp": "feedback",
            	"aTargets": [1],
            	"bSortable": true,
            	"sWidth":"100px",
            	"bVisible": true
            }
            ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    				aoData.push(
            									{"name": 'historyCustomerVehicleId', "value": ${historyCustomerVehicleInstance?.id}}
            						);
            						$.ajax({ "dataType": 'json',
            							"type": "POST",
            							"url": sSource,
            							"data": aoData ,
            							"success": function (json) {
            								fnCallback(json);
            							   },
            							"complete": function () {
            							   }
            						});
            		}
            	});
            });
            </g:javascript>
		</div>

        <div class="tab-pane" id="cc_jd_power_survey">
			<label>JD Power</label>
		</div>

        <div class="tab-pane" id="cc_field_action">
            <table id="cc_field_action_datatables" cellpadding="0" cellspacing="0"
                border="0"
                class="display table table-striped table-bordered table-hover"
                width="100%">
                <thead>
                    <tr>
                        <th style="border-bottom: none;padding: 5px;">
                            <div>Tanggal</div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div>Field Action</div>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>

        <div class="tab-pane" id="cc_mrs">
            <table id="mrs_datatables" cellpadding="0" cellspacing="0"
                border="0"
                class="display table table-striped table-bordered table-hover"
                width="100%">
                <thead>
                    <tr>
                        <th style="border-bottom: none;padding: 5px;">
                            <div>Tanggal MRS</div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div>MRS</div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div>Metode Invitation</div>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>

	</div>
</div>
