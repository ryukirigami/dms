
<%@ page import="com.kombos.maintable.PartStockTransaction" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'partStockTransaction.label', default: 'Part Stock Transaction')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			$(function(){
                $('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_UPLOAD_' :
							loadPath("partStockTransaction/uploadData");
							break;
                        case '_GENERATE_' :
							loadPath("partStockTransaction/generateData");
							break;
				   }
				   return false;
				});
                var checkin = $('#search_Tanggal').datepicker({
                    onRender: function(date) {
                        return '';
                    }
                }).on('changeDate', function(ev) {
                            var newDate = new Date(ev.date)
                            newDate.setDate(newDate.getDate());
                            checkout.setValue(newDate);
                            checkin.hide();
                            $('#search_Tanggal2')[0].focus();
                        }).data('datepicker');

                var checkout = $('#search_Tanggal2').datepicker({
                    onRender: function(date) {
                        return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
                    }
                }).on('changeDate', function(ev) {
                            checkout.hide();
                        }).data('datepicker');



                printPartStocktrx = function(){
                       checkPartStockTrx =[];
                        $("#partStockTransaction-table tbody .row-select").each(function() {
                              if(this.checked){
                                 var nRow = $(this).next("input:hidden").val();
                                    checkPartStockTrx.push(nRow);
                              }
                        });
                        if(checkPartStockTrx.length<1 ){
                            alert('Silahkan Pilih Salah satu Data Untuk Dicetak');
                            return;

                        }
                       var docNumber =  JSON.stringify(checkPartStockTrx);

                       window.location = "${request.contextPath}/partStockTransaction/printPartStockTrx?docNumber="+docNumber;

                }

});
</g:javascript>
	</head>
	<body>

        <div class="navbar box-header no-border">
            <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
            <ul class="nav pull-right">
                <li>
                    <g:if test="${!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                    <a class="pull-right box-action" href="#"
                       style="display: block;" target="_UPLOAD_">&nbsp;Upload&nbsp;</i>
                    </a>
                    </g:if>
                </li>
                <li class="separator"></li>
                <li>
                    <g:if test="${!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                        <a class="pull-right box-action" href="#"
                           style="display: block;" target="_GENERATE_">Generate</i>&nbsp;&nbsp;
                        </a>
                    </g:if>
                </li>
                    <li class="separator"></li>
            </ul>
        </div>
	<div class="box">
		<div class="span12" id="partStockTransaction-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="t951Tanggal">
                                <g:message code="auditTrail.wo.label" default="Company Dealer" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_wo" class="controls">
                                <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                    <g:select name="companyDealer" id="companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop"  noSelection="${['-':'Semua']}" />
                                </g:if>
                                <g:else>
                                    <g:select name="companyDealer" id="companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                </g:else>
                            </div>
                        </td>
                    </tr><tr>
                    <td style="width: 130px">
                        <label class="control-label" for="t951Tanggal">
                            <g:message code="auditTrail.tanggal.label" default="Tanggal Upload" />&nbsp;
                        </label>&nbsp;&nbsp;
                    </td>
                    <td>
                        <div id="filter_m777Tgl" class="controls">
                            <ba:datePicker id="search_Tanggal" name="search_Tanggal" precision="day" format="dd/MM/yyyy"  value="${new Date()}" />
                            &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                            <ba:datePicker id="search_Tanggal2" name="search_Tanggal2" precision="day" format="dd/MM/yyyy"  value="${new Date()}" />
                        </div>
                    </td>
                </tr>
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="view" id="view" >View</button>
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel clear" name="clear" id="clear" >Clear</button>
                            </div>
                        </td>
                    </tr>
                </table>

            </fieldset>
            <g:render template="dataTables" />
        </div>
        <button style="width: 130px;height: 30px; border-radius: 5px" class="btn btn-primary print" name="print" id="print" onclick="printPartStocktrx();" >Download *.txt</button>
        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
            <br><br>
            Ket : *Cabang yang belum kirim : *
            <div id="keterangan"></div>
        </g:if>

    </div>
    </body>
</html>
