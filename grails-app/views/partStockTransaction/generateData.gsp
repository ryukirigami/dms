
<%@ page import="com.kombos.parts.Goods" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'goods.label', default: 'Generate Part Stock Transaction')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
        <g:if test="${jsonData}">
            <g:javascript>
                <g:if test="${jmlhDataError && jmlhDataError>0}">
                    $('#save').attr("disabled", true);
                </g:if>
                var saveForm;
                $(function(){
                    $("#export").click(function(e) {
                        window.location = "${request.contextPath}/partStockTransaction/exportToExcel";
                    });


                    saveForm = function() {
                       var sendData = ${jsonData};
                       var tanggal = $("#tanggalUpload").val();
                       console.log(sendData);
                       var conf = "";
                       <g:if test="${jsonData}">
                            <g:if test="${jmlhDataError==0}">
                                conf = confirm("Apakah Anda Akan Menyimpan");
                            </g:if>
                            if(conf){
                                $.ajax({
                                    url:'${request.getContextPath()}/partStockTransaction/upload',
                                    type: 'POST',
                                    //add beforesend handler to validate or something
                                    //beforeSend: functionname,
                                    success: function (res) {
                                        $('#jobTable').empty();
                                        $('#jobTable').append(res);
                                        toastr.success("Save Succes");
                                        closeUpload();
                                    },
                                    //add error handler for when a error occurs if you want!
                                    error: function (data, status, e){
                                        alert(e);
                                    },
                                    complete: function(xhr, status) {

                                    },
                                    data:  {sendData : JSON.stringify(sendData), tanggal:tanggal},
                                    %{--contentType: "application/json; charset=utf-8",--}%
                                    traditional: true,
                                    cache: false
                                });
                            }
                        </g:if>
                        <g:else>
                            alert('No File Selected');
                        </g:else>
                    }
             });
            </g:javascript>
        </g:if>
	</head>
	<body>
    <div id="jobTable">
        <div class="navbar box-header no-border">
            <span class="pull-left">
                <g:message code="default.list.label" args="[entityName]" />
            </span>
            <ul class="nav pull-right">
                <li></li>
                <li></li>
                <li class="separator"></li>
            </ul>
        </div>
        <div class="box">
            <div class="span12" id="operation-table">
                <fieldset>
                     <table style="padding-right: 10px">
                            <tr>
                                <td style="width: 130px">
                                    <label class="control-label" for="t951Tanggal">
                                        <g:message code="auditTrail.wo.label" default="Tanggal Upload" />&nbsp;
                                    </label>&nbsp;&nbsp;
                                </td>
                                <td style="width: 230px">
                                    <div id="filter_wo" class="controls">
                                        <ba:datePicker id="tanggalUpload" name="tanggalUpload" precision="day" format="dd/MM/yyyy"  value="${tanggal}" />
                                    </div>
                                </td>
                                <td>
                                    <fieldset class="buttons controls">
                                        <g:field type="button" onclick="doGenerate();return false;" class="btn btn-primary create" name="doGenerate" id="doGenerate" value="${message(code: 'default.button.view.label', default: 'Generate')}" />
                                        <g:field type="button" onclick="saveForm();" class="btn btn-primary create" name="save" id="save" value="${message(code: 'default.button.upload.label', default: 'Simpan')}" />
                                        &nbsp;&nbsp;&nbsp;
                                        <g:if test="${flash.message}">
                                            ${flash.message}
                                        </g:if>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                    <g:javascript>
                        var doGenerate;
                        $(function(){
                            closeUpload = function(){
                                ${flash.message = ""}
                                loadPath("partStockTransaction/index");
                            }

                            doGenerate = function() {
                                var tanggal = $("#tanggalUpload").val();
                                $.ajax({
                                    url:'${request.getContextPath()}/partStockTransaction/viewGenerate',
                                    type: 'POST',
                                    success: function (res) {
                                        $('#jobTable').empty();
                                        $('#jobTable').append(res);

                                    },
                                    //add error handler for when a error occurs if you want!
                                    error: function (data, status, e){
                                        alert(e);
                                    },
                                    complete: function(xhr, status) {

                                    },
                                    data: {tanggal : tanggal},
                                    traditional: true,
                                    cache: false
                    });

                }
            });
                    </g:javascript>
                </fieldset>
                <br>

                    <table id="data" class="display table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; width: 1284px;">
                        <tr>
                            <th>
                                No.
                            </th>
                            <th>
                                Dealer Code
                            </th>
                            <th>
                                Area Code
                            </th>
                            <th>
                                Outlet Code
                            </th>

                            <th>
                                Part Nomor
                            </th>
                            <th>
                                Part Description
                            </th>
                            <th>
                                Stock
                            </th>
                            <th>
                                MAD Update
                            </th>


                            </tr>
                        <g:if test="${htmlData}">
                            ${htmlData}
                        </g:if>
                        <g:else>
                            <tr class="odd">
                                <td class="dataTables_empty" valign="top" colspan="16">No data available in table</td>
                            </tr>
                        </g:else>
                    </table>
                <g:field type="button" onclick="closeUpload();" class="btn btn-cancel delete" name="close" id="close" value="${message(code: 'default.button.view.label', default: 'Close')}" />
                %{--<g:field type="button" class="btn btn-primary create" name="export" id="export" value="${message(code: 'default.button.view.label', default: 'Export To Excel')}" />--}%
            </div>
        </div>
    </div>
</body>
</html>
