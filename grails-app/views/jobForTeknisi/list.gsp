
<%@ page import="com.kombos.administrasi.BahanBakar" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'jobForTeknisi.label', default: 'Job For Teknisi')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});


    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/jobForTeknisi/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#jobForTeknisi-form').empty();
    	$('#jobForTeknisi-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#jobForTeknisi-table").hasClass("span12")){
   			$("#jobForTeknisi-table").toggleClass("span12 span5");
        }
        $("#jobForTeknisi-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#jobForTeknisi-table").hasClass("span5")){
   			$("#jobForTeknisi-table").toggleClass("span5 span12");
   		}
        $("#jobForTeknisi-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#jobForTeknisi-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/jobForTeknisi/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadJobForTeknisiTable();
    		}
		});
		
   	}
    woDetail = function(){
           checkJobForTeknisi =[];
            var idCheckJobForTeknisi="";
                $("#jobForTeknisi-table tbody .row-select").each(function() {
                 if(this.checked){
                   var nRow = $(this).next("input:hidden").val();
					checkJobForTeknisi.push(nRow);
                }
            });
            if(checkJobForTeknisi.length<1 || checkJobForTeknisi.length>1 ){
                alert('Silahkan Pilih Salah satu Teknisi');
                return;
            }else{
                $("#jobForTeknisi-table tbody .row-select").each(function() {
                     if(this.checked){
                        var nRow = $(this).next("input:hidden").val();
                        checkJobForTeknisi.push(nRow);
                        idCheckJobForTeknisi =  JSON.stringify(checkJobForTeknisi);
                         $.ajax({
                                url:'${request.contextPath}/jobForTeknisi/jenisWo',
                                type: "POST",
                                data: { id: nRow },
                                success:function(data){
                                    if(data=="GR"){
                                         window.location.href='#/WoDetailGr';
                                        $.ajax({
                                            url:'${request.contextPath}/woDetailGr/list',
                                            type: "POST",
                                            data: { id: nRow , aksi : "jobForteknisi" },
                                            dataType: "html",
                                            complete : function (req, err) {
                                                $('#main-content').html(req.responseText);
                                                $('#spinner').fadeOut();
                                                loading = false;
                                            }
                                        });
                                    }
                                    else {
                                        window.location.href ='#/bpWODetail';
                                        $.ajax({
                                            url:'${request.contextPath}/BPWODetail/list',
                                            type: "POST",
                                            data: { id: nRow , aksi : "jobForteknisi"  },
                                            dataType: "html",
                                            complete : function (req, err) {
                                                $('#main-content').html(req.responseText);
                                                $('#spinner').fadeOut();
                                                loading = false;
                                            }
                                        });
                                    }
                                }
                            });
                    }
                });
            }

    }
    ok = function(){
           checkJobForTeknisi =[];

            var idCheckJobForTeknisi="";
            $("#jobForTeknisi-table tbody .row-select").each(function() {
                 if(this.checked){
                   var nRow = $(this).next("input:hidden").val();
					checkJobForTeknisi.push(nRow);
                }
            });
            if(checkJobForTeknisi.length<1 || checkJobForTeknisi.length>1 ){
                alert('Silahkan Pilih Salah satu Teknisi');
                return;
            }else{
                $("#jobForTeknisi-table tbody .row-select").each(function() {
                     if(this.checked){
                        var nRow = $(this).next("input:hidden").val();
                        checkJobForTeknisi.push(nRow);
                        idCheckJobForTeknisi =  JSON.stringify(checkJobForTeknisi);
                        $.ajax({
                            url:'${request.contextPath}/jobForTeknisi/jenisWo',
                            type: "POST",
                            data: { id: nRow },
                            success:function(data){
                                if(data=="GR"){
                                    window.location.replace('#/teknisiGr')
                                    $('#spinner').fadeIn(1);
                                    $.ajax({
                                        url: '${request.contextPath}/teknisiGr',
                                        type: "GET",dataType:"html",data: {id : nRow},
                                        complete : function (req, err) {
                                            $('#main-content').html(req.responseText);
                                            $('#spinner').fadeOut();
                                        }
                                    });
                                }else{
                                    window.location.replace('#/BPTeknisi')
                                    $('#spinner').fadeIn(1);
                                    $.ajax({
                                        url: '${request.contextPath}/BPTeknisi',
                                        type: "GET",dataType:"html",data: {id : nRow},
                                        complete : function (req, err) {
                                            $('#main-content').html(req.responseText);
                                            $('#spinner').fadeOut();
                                        }
                                    });
                                }
                            }
                        });
                      }
                });
            }
    }
});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
	</div>
	<div class="box">
		<div class="span12" id="jobForTeknisi-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
            <fieldset class="buttons controls" style="padding-top: 10px;">
                <button id='btn' onclick="woDetail();" type="button" class="btn btn-primary">WO Details</button>
                <button id='btn2' onclick="ok();" type="button" class="btn cancel">OK</button>
            </fieldset>
		</div>
		<div class="span7" id="jobForTeknisi-form" style="display: none;"></div>
	</div>
</body>
</html>
