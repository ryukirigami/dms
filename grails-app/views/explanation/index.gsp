<%--
  Created by IntelliJ IDEA.
  User: Mohammad Fauzi
  Date: 4/24/14
  Time: 11:57 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>

<head>
<meta name="layout" content="main">
<title>Job Order Completion</title>
<r:require modules="baseapplayout"/>

<g:javascript disposition="head">
        $(function () {

            jQuery("#buttonSave").click(function (event) {
                jQuery('#spinner').fadeIn(1);
                $(this).submit();
                event.preventDefault();
            });

            jQuery("#buttonPrint").click(function (event) {
                jQuery('#spinner').fadeIn(1);
                var noExplaination = '1234';
                alert('Print ' + noExplaination);
                event.preventDefault();
            });

            jQuery("#buttonClose").click(function (event) {
                event.preventDefault(); //to stop the default loading
            });

            jQuery("#buttonClose1").click(function (event) {
                event.preventDefault(); //to stop the default loading
            });

            jQuery("#buttonNext").click(function (event) {
                document.getElementById("halaman1").style.display = 'none';
                document.getElementById("halaman2").style.display = '';
                event.preventDefault(); //to stop the default loading
            });

            jQuery("#buttonBack").click(function (event) {
                document.getElementById("halaman1").style.display = '';
                document.getElementById("halaman2").style.display = 'none';
                event.preventDefault(); //to stop the default loading
            });


            jQuery("#loadData").click(function (event) {
                loadInformasiUmum();
                event.preventDefault(); //to stop the default loading
            });

            completeProcess = function () {

            }

            doResponse = function (data) {
                jQuery('#spinner').fadeOut();
                if (data.status == 'OK') {
                    alert('Save success');
                } else {
                    alert('Save fail\n' + data.error);
                }

            }


            checkNumber = function (n) {
                var number = !isNaN(parseFloat(n.value)) && isFinite(n.value);
                if (!number) {
                    // alert("tanggalFeedbackEnd !");
                    n.focus();
                    n.value = '';
                }
            }


            loadInformasiUmum = function(){
                var kategoriView = document.getElementById("kategoriView").value;
                var kataKunci = document.getElementById("kataKunci").value;
                $('#informasiUmum').empty();
                $('#DiagnoseCustomers').empty();
                $('#StatusPekerjaan').empty();
                $('#keluhanCustomer').empty();
                $('#InspectionDuringRepair').empty();
                $('#FinalInspection').empty();
                jQuery('#spinner').fadeIn(1);
                jQuery.getJSON('${request.contextPath}/explanation/loadDataSemua?kategoriView=' + encodeURIComponent(kategoriView)+'&kataKunci='+encodeURIComponent(kataKunci), function (data) {
                    jQuery('#spinner').fadeOut(1);
                    if(data.status == 'Error'){
                        alert(data.message);
                        return
                    }
                    var InformasiUmum = data.InformasiUmum;

                    var tabelInformasiUmum = "<tr>" +
                    "       <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>" +InformasiUmum.noWo + "</td>" +
                    "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +InformasiUmum.nopol + "</td>" +
                    "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +InformasiUmum.model + "</td>" +
                    "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +InformasiUmum.namaCustomer + "</td>" +
                    "</tr>";
                    $('#informasiUmum').append(tabelInformasiUmum);


                    var keluhans = data.keluhans;
                     jQuery.each(keluhans, function (index, value) {
                        var keluhan = "<tr>" +
                        "       <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>" +value.nama + "</td>" +
                        "</tr>";
                        $('#keluhanCustomer').append(keluhan);
                    });



                    var DiagnoseCustomers = data.DiagnoseCustomers;
                    jQuery.each(DiagnoseCustomers, function (index, value) {
                        var DiagnoseCustomer = "<tr>" +
                            "       <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>" +value.Masalah + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +value.Solusi + "</td>" +
                            "</tr>";
                        $('#DiagnoseCustomers').append(DiagnoseCustomer);
                    });


                    var StatusPekerjaans = data.StatusPekerjaans;
                                        jQuery.each(StatusPekerjaans, function (index, value) {
                                            var StatusPekerjaan = "<tr>" +
                        "       <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>" +value.nama + "</td>" +
                        "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +value.status + "</td>" +
                        "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +value.alasan + "</td>" +
                        "</tr>";
                        $('#StatusPekerjaan').append(StatusPekerjaan);
                    });

                    var InspectionDuringRepairs = data.InspectionDuringRepairs;
                                                           jQuery.each(InspectionDuringRepairs, function (index, value) {
                                            var InspectionDuringRepair = "<tr>" +
                        "       <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>" +value.status + "</td>" +
                        "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +value.alasan + "</td>" +
                        "</tr>";
                        $('#InspectionDuringRepair').append(InspectionDuringRepair);
                    });
                    
                    
                    var FinalInspections = data.FinalInspections;
                                                           jQuery.each(FinalInspections, function (index, value) {
                                            var FinalInspection = "<tr>" +
                        "       <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>" +value.alasan + "</td>" +
                        "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +value.hasil + "</td>" +
                        "</tr>";
                        $('#FinalInspection').append(FinalInspection);
                    });
                    
                    var receptionId = data.receptionId;
                    $('#receptionId').val(receptionId);
                });
            }





        });
</g:javascript>

<style>
th {
    border-style: solid;
    border-width: 1px;
    background-color: #f4ffff;
}

</style>
</head>

<body>
<g:formRemote name="form" id="form" on404="alert('not found!')" update="updateMe"
              method="post"
              onComplete="completeProcess()"
              onSuccess="doResponse(data)"
              url="[controller: 'explanation', action: 'doSave']">
<div id="halaman1">
    <div class="box">
        <legend>Delivery – Explanation</legend>
        <fieldset class="form">
            <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
                <tr>
                    <td style="width: 50%;vertical-align: top;">

                        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
                            <tr>
                                <td colspan="2">

                                    <div class="box">
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    Kategori View
                                                </td>
                                                <td>
                                                    <g:select name="kategoriView"
                                                              from="${["NOMOR WO", "NOMOR POLISI"]}"/>
                                                </td>
                                                <td>
                                                    Kata Kunci
                                                </td>
                                                <td>
                                                    <g:textField name="kataKunci"/>
                                                </td>
                                                <td>
                                                    <button id="loadData">OK</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </td>
                                %{----}%
                                %{--<td style="width: 20%;vertical-align: top;padding-left: 50px;padding-right: 50px;padding-top: 10px;padding-bottom: 20px;">--}%
                                    %{--<button style="height: 95%;width: 95%;font-size: large">START JOC</button>--}%
                                %{--</td>--}%
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="box">
                                        <legend style="font-size: medium">Informasi Umum</legend>

                                        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
                                            <tr>
                                                <td style="width: 80%;vertical-align: top;">

                                                    <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
                                                        <thead>
                                                        <tr>
                                                            <th>Nomor WO</th>
                                                            <th>Nomor Polisi</th>
                                                            <th>Model</th>
                                                            <th>Nama Cutomer</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="informasiUmum">

                                                        </tbody>
                                                    </table>

                                                </td>
                                            </tr>
                                        </table>

                                    </div>

                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">

                                    %{--<table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">--}%
                                        %{--<tr>--}%
                                            %{--<td style="padding-right: 20px;">--}%
                                                <div class="box">
                                                    <legend style="font-size: medium">Keluhan Customer</legend>

                                                    <table style="width: 100%;border: 1px;padding-left: 10px;padding-right: 10px;border-style: solid;">
                                                        <thead>
                                                        <tr>
                                                            <th>Keluhan</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="keluhanCustomer">

                                                        </tbody>
                                                    </table>

                                                </div>
                                            %{--</td>--}%
                                        %{--</tr>--}%
                                    %{--</table>--}%

                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <div class="box">
                                        <legend style="font-size: medium">Diagnose</legend>
                                        <table style="width: 100%;border: 1px;padding-left: 10px;padding-right: 10px;border-style: solid;">
                                            <thead>
                                            <tr>
                                                <th>Masalah</th>
                                                <th>Solusi</th>
                                            </tr>
                                            </thead>
                                            <tbody id="DiagnoseCustomers">

                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <div class="box">
                                        <legend style="font-size: medium">Status Pekerjaan</legend>
                                        <table style="width: 100%;border: 1px;padding-left: 10px;padding-right: 10px;border-style: solid;">
                                            <thead>
                                            <tr>
                                                <th>Order Pekerjaan</th>
                                                <th>Status</th>
                                                <th>Alasan</th>
                                            </tr>
                                            </thead>
                                            <tbody id="StatusPekerjaan">

                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>


                            <tr>
                                <td colspan="2">
                                    <div class="box">
                                        <legend style="font-size: medium">Inspection During Repair</legend>
                                        <table style="width: 100%;border: 1px;padding-left: 10px;padding-right: 10px;border-style: solid;">
                                            <thead>
                                            <tr>
                                                <th>Deskripsi Inspection During Repair</th>
                                                <th>Hasil Inspection During Repair</th>
                                            </tr>
                                            </thead>
                                            <tbody id="InspectionDuringRepair">

                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <div class="box">
                                        <legend style="font-size: medium">Final Inspection</legend>
                                        <table style="width: 100%;border: 1px;padding-left: 10px;padding-right: 10px;border-style: solid;">
                                            <thead>
                                            <tr>
                                                <th>Deskripsi Final Inspection</th>
                                                <th>Hasil Final Inspection</th>
                                            </tr>
                                            </thead>
                                            <tbody id="FinalInspection">

                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>


                            <tr>
                                <td colspan="2">
                                    <div class="box">
                                        <legend style="font-size: medium">Follow Up</legend>
                                        <table style="width: 100%;border: 1px;padding-left: 10px;padding-right: 10px;border-style: solid;">
                                            <thead>
                                            <tr>
                                                <th colspan="2">Metode dan Waktu Follow Up</th>
                                            </tr>
                                            </thead>
                                            <tbody id="FollowUp">
                                                <g:hiddenField name="receptionId" value=""/>
                                                <tr>
                                                    <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>Metode yang diinginkan untuk Follow Up</td>
                                                    <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>
                                                        <g:radioGroup name="metodeFollowUp" values="['1','2']" value="" labels="['SMS','Telp']" required="">
                                                            ${it.radio} <g:message code="${it.label}" />
                                                        </g:radioGroup>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>Tanggal Follow Up</td>
                                                    <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>
                                                        <ba:datePicker name="tanggalFollowUp" precision="day"
                                                                       value="" format="dd/MM/yyyy" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>Waktu Follow Up</td>
                                                    <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>
                                                        <g:select id="waktuFollowUp" name="waktuFollowUp.id" from="${com.kombos.maintable.KategoriWaktuFu.findAll('from KategoriWaktuFu j order by j.m804Id')}"
                                                                  optionKey="id" value="" noSelection="['':'Silahkan Pilih']"
                                                                  class="many-to-one" style="width: 130px;"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>Catatan</td>
                                                    <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>
                                                        <g:textField name="catatan" value="" required="" maxlength="50"/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                        </table>

                    </td>
                </tr>

            </table>
        </fieldset>
    </div>
    <fieldset class="buttons controls">
        <button class="btn btn-primary" id="buttonSave">Save</button>
        <button class="btn btn-primary" id="buttonPrint">Print</button>
        <button class="btn btn-primary" id="buttonClose" onclick="window.location.replace('#/home');">Close</button>
    </fieldset>
</div>

</g:formRemote>
</body>
</html>