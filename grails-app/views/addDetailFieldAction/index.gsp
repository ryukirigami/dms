<%--
  Created by IntelliJ IDEA.
  User: Mohammad Fauzi
  Date: 2/5/14
  Time: 12:25 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<head>
    <meta name="layout" content="main">
    <title>Upload Detail Field Action</title>
    <r:require modules="baseapplayout"/>
    <g:render template="../menu/maxLineDisplay"/>

    <g:javascript disposition="head">
        var addMore;
        var closeModal;
        var onSaveData;
        $(function () {
        addMore = function(){
            $("#addMoreContent").empty();
            $.ajax({type:'POST', url:'${request.contextPath}/FA?isFrom=AddDetail',
                success:function(data,textStatus){
                        $("#addMoreContent").html(data);
                        $("#addMoreModal").modal({
                            "backdrop" : "static",
                            "keyboard" : false,
                            "show" : true
                        }).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        }

        closeModal = function(modal){
            var root="${resource()}";
            var url = root+'/addDetailFieldAction/findList';
            jQuery('#fa').load(url);
            $('#addMoreModal').modal('hide');
        }
            onSelectAll = function(){
                $("#historyCustomerVehicle_datatables tbody .row-select").each(function() {
                            this.checked = true
                        });
            }

            onUnSelectAll = function(){
                $("#historyCustomerVehicle_datatables tbody .row-select").each(function() {
                            this.checked = false
                        });
            }

            onSaveData = function(){
                var hasil = confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}');
                if(!hasil){
                    return
                }
                try{
                    var checkJob =[];
                        $("#historyCustomerVehicle_datatables tbody .row-select").each(function() {
                            if(this.checked){
                                var id = $(this).next("input:hidden").val();
                                checkJob.push(id);
                            }
                        });
                        if(checkJob.length == 0){
                            alert('Anda belum memilih data yang akan disimpan');
                            return;
                        }
                        var checkBoxVendor = JSON.stringify(checkJob);
                        var fa = document.getElementById("fa").value;
                        $.ajax({
                            url:'${request.contextPath}/addDetailFieldAction/doSave?fa=' + fa,
                            type: "POST", // Always use POST when deleting data
                            data : { ids: checkBoxVendor },
                            success : function(data){
                                reloadHistoryCustomerVehicleTable();
                                toastr.success("Save Sukses");
                            },
                            error: function(xhr, textStatus, errorThrown) {
                                alert('Internal Server Error');
                            }
                        });

                    checkJob = [];
                }catch (e){
                    alert(e)
                }


             }

        });
    </g:javascript>




</head>

<body>

%{--<fieldset class="form">--}%

    <div class="box">
        <legend style="font-size: small">Vehice List</legend>

        <div id="partsDataTables_tabs-1">
            <br/>
            <button class="btn cancel" onclick="resetSearch();">Clear Search</button>
            <g:render template="vehicleListDataTables"/>
        </div>

        <legend style="font-size: small">Field Action Category</legend>

        <div class="controls">
            Deskripsi Field Action: <g:select id="fa" name="fa.id" style="width:85%" from="${com.kombos.customerprofile.FA.createCriteria().list{order("m185NamaFA")}}"
                                              optionKey="id" required=""
                                              class="many-to-one"/>
            <button class="btn cancel" onclick="addMore();"> ... </button>
        </div>
        <div id="addMoreModal" class="modal hide" style="width: 1200px;">
            <div class="modal-dialog" style="width: 1200px;">
                <div class="modal-content" style="width: 1200px; height: 550">
                    <div class="modal-body" style="max-height: 550px;">
                        <div id="addMoreContent">
                            <div class="iu-content"></div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button name="closeModal" type="button" onclick="closeModal();" class="btn cancel">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

%{--</fieldset>--}%

<fieldset class="buttons controls">
    <button class="btn btn-primary" onclick="onSaveData()">Save</button>
</fieldset>

</body>
</html>