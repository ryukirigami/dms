
<%@ page import="com.kombos.parts.StokOPName" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="viewSOpnameTdkSesuai_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover" style="table-layout: fixed;">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.goods.kode.label" default="Kode Part" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.goods.nama.label" default="Nama Part" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.qty1.kode.label" default="Qty 1" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.goods.satuan1.label" default="Satuan 1" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.qty2.kode.label" default="Qty 2" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.goods.satuan2.label" default="Satuan 2" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.icc.nama.label" default="ICC" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.goods.lokasirak.label" default="Lokasi/Rak" /></div>
            </th>

		
		</tr>
	</thead>
</table>

<g:javascript>
var viewSOpnameTdkSesuaiTable;
var reloadViewSOpnameTdkSesuaiTable;
$(function(){
	
	reloadViewSOpnameTdkSesuaiTable = function() {
		viewSOpnameTdkSesuaiTable.fnDraw();
	}

	



$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	viewSOpnameTdkSesuaiTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	$('#view').click(function(){
	    viewSOpnameTdkSesuaiTable.fnDraw();
	});

	$('#clear').click(function(e){
	    $('#search_t132ID').val("");
	    viewSOpnameTdkSesuaiTable.fnDraw();
	});

	viewSOpnameTdkSesuaiTable = $('#viewSOpnameTdkSesuai_datatables').dataTable({
		"sScrollX": "1350px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "goods",
	"mDataProp": "kodegoods",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';//<a href="#" onclick="show('+row['id']+');">'+data+'
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "namagoods",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t132Jumlah1Stock",
	"mDataProp": "t132Jumlah1Stock",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "satuangoods1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "t132Jumlah2Stock",
	"mDataProp": "t132Jumlah2Stock",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "satuangoods2",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,


{
	"sName": "goods",
	"mDataProp": "icc",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "lokasigoods",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	                    var t132ID = $('#search_t132ID').val();
						if(t132ID){
							aoData.push(
									{"name": 'sCriteria_t132ID', "value": t132ID}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
