
<%@ page import="com.kombos.parts.StokOPName" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'viewSOpnameTdkSesuai.label', default: 'Hasil Stok Opname')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="stokOPName.viewTdkSesuai.label" default="View Stok Opname yang Tidak Sesuai" /></span>
		<ul class="nav pull-right">
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="viewSOpnameTdkSesuai-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <table>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="stokOPName.t132ID.label" default="Nomor Stock Opname"/>
                    </td>
                    <td style="padding: 5px">
                        <g:select id="search_t132ID" name="search_t132ID" onchange="viewSOpnameTdkSesuaiTable.fnDraw();" optionValue="${{it}}" from="${StokOPName.executeQuery("SELECT t132ID FROM StokOPName where t132StaUpdateStock='0' and companyDealer = "+session?.userCompanyDealer?.id+" group by t132ID")}" value="" noSelection="['':'Pilih Nomor Stock Opname']" class="many-to-one"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a class="pull-right box-action" style="display: block;" >
                            &nbsp;&nbsp;
                            <i>
                                <g:field type="button" style="padding: 5px" class="btn cancel pull-right box-action"
                                         name="clear" id="clear" value="${message(code: 'default.clearsearch.label', default: 'Clear Search')}" />
                            </i>
                            &nbsp;&nbsp;
                        </a>
                    </td>
                </tr>
            </table>
			<g:render template="dataTables" />
            <a class="pull-right box-action" style="display: block;" >
                &nbsp;&nbsp;
                <i>
                    <g:field type="button" style="padding: 5px" class="btn cancel pull-right box-action"
                             name="onHandAdjust" id="onHandAdjust" value="${message(code: 'stokOPName.button.onHandAdjust.label', default: 'On Hand Adjustment')}" />
                </i>
                &nbsp;&nbsp;
            </a>
            <a class="pull-right box-action" style="display: block;" >
                &nbsp;&nbsp;
                <i>

                    <g:field type="button" onclick="window.location.replace('#/inputHasilStokOpname');" style="padding: 5px"
                     class="btn cancel pull-right box-action" name="reStokOpname" id="reStokOpname" value="${message(code: 'stokOPName.button.reStokOpname.label', default: 'Re-Entry Stock Opname')}" />
                </i>
                &nbsp;&nbsp;
            </a>
		</div>
		<div class="span7" id="viewSOpnameTdkSesuai-form" style="display: none;"></div>
	</div>
</body>
</html>
