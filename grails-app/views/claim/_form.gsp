<%@ page import="com.kombos.parts.Claim" %>



<div class="control-group fieldcontain ${hasErrors(bean: claimInstance, field: 'goods', 'error')} ">
	<label class="control-label" for="goods">
		<g:message code="claim.goods.label" default="Nama Goods" />
		
	</label>
	<div class="controls">
	%{--<g:select id="goods" name="goods.id" from="${com.kombos.parts.Goods.list()}" optionKey="id" required="" value="${claimInstance?.goods?.id}" class="many-to-one"/>--}%
    <g:textField name="t171ID" value="${claimInstance?.goods?.m111Nama}" readonly="" />
	</div>
</div>
<g:javascript>
    document.getElementById("goods").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: claimInstance, field: 'goodsReceive', 'error')} ">
	<label class="control-label" for="goodsReceive">
		<g:message code="claim.goodsReceive.label" default="No Reff" />
		
	</label>
	<div class="controls">
	%{--<g:select id="goodsReceive" name="goodsReceive.id" from="${com.kombos.parts.GoodsReceive.list()}" optionKey="id" required="" value="${claimInstance?.goodsReceive?.t167NoReff}" class="many-to-one"/>--}%
    <g:textField name="t171ID" value="${claimInstance?.goodsReceive?.t167NoReff}" readonly="" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: claimInstance, field: 'invoice', 'error')} ">
	<label class="control-label" for="invoice">
		<g:message code="claim.invoice.label" default="Invoice" />
		
	</label>
	<div class="controls">
	%{--<g:select id="invoice" name="invoice.id" from="${com.kombos.parts.Invoice.list()}" optionKey="id" required="" value="${claimInstance?.invoice?.id}" class="many-to-one"/>--}%
    <g:textField name="t171ID" value="${claimInstance?.invoice?.t166NoInv}" readonly="" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: claimInstance, field: 'vendor', 'error')} ">
    <label class="control-label" for="vendor">
        <g:message code="claim.vendor.label" default="Vendor" />

    </label>
    <div class="controls">
        %{--<g:select id="vendor" name="vendor.id" from="${com.kombos.parts.Vendor.list()}" optionKey="id" required="" value="${claimInstance?.vendor?.id}" class="many-to-one"/>--}%
        <g:textField name="t171ID" value="${claimInstance?.vendor?.m121Nama}" readonly="" />
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: claimInstance, field: 't171PetugasClaim', 'error')} ">
	<label class="control-label" for="t171PetugasClaim">
		<g:message code="claim.t171PetugasClaim.label" default="Petugas Claim" />
		
	</label>
	<div class="controls">
	<g:textField name="t171PetugasClaim" value="${claimInstance?.t171PetugasClaim}" readonly="" />
	</div>
</div>

<g:if test="${claimInstance?.t171ID}">
<div class="control-group fieldcontain ${hasErrors(bean: claimInstance, field: 't171ID', 'error')} ">
    <label class="control-label" for="t171ID">
        <g:message code="claim.t171ID.label" default="No Claim" />

    </label>
    <div class="controls">
        ${claimInstance?.t171ID}
    </div>
</div></g:if>

<div class="control-group fieldcontain ${hasErrors(bean: claimInstance, field: 't171Qty1Issued', 'error')} ">
	<label class="control-label" for="t171Qty1Rec">
		<g:message code="claim.t171Qty1Rec.label" default="Qty Rec" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t171Qty1Issued" value="${claimInstance.t171Qty1Issued}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: claimInstance, field: 't171Qty1Reff', 'error')} ">
	<label class="control-label" for="t171Qty1Reff">
		<g:message code="claim.t171Qty1Reff.label" default="Qty Reff" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t171Qty1Reff" value="${claimInstance.t171Qty1Reff}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: claimInstance, field: 't171Qty1Rusak', 'error')} ">
	<label class="control-label" for="t171Qty1Rusak">
		<g:message code="claim.t171Qty1Rusak.label" default="Qty Rusak" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t171Qty1Rusak" value="${claimInstance.t171Qty1Rusak}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: claimInstance, field: 't171Qty1Salah', 'error')} ">
	<label class="control-label" for="t171Qty1Salah">
		<g:message code="claim.t171Qty1Salah.label" default="Qty Salah" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t171Qty1Salah" value="${claimInstance.t171Qty1Salah}" />
	</div>
</div>
