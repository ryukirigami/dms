
<%@ page import="com.kombos.parts.SOQ" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="title" value="View Claim" />
		<title>${title}</title>
		<r:require modules="baseapplayout" />
        <g:javascript disposition="head">
	var show;
	var loadForm;
	var noUrut = 1;
	var printClaim;
	var printClaimCont;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){


	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();" />
            break;
        case '_DELETE_' :
            bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});

         		break;
       }
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/claim/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };


    loadForm = function(data, textStatus){
		$('#claim-form').empty();
    	$('#claim-form').append(data);
   	}

    shrinkTableLayout = function(){
    	$("#claim-table").hide()
//    	if($("#claim-table").hasClass("span12")){
//   			$("#claim-table").toggleClass("span12 span5");
//        }
        $("#claim-form").css("display","block");
   	}

   	expandTableLayout = function(){
//   		if($("#claim-table").hasClass("span5")){
//   			$("#claim-table").toggleClass("span5 span12");
//   		}
        $("#claim-table").show()
        $("#claim-form").css("display","none");
   	}

   	massDelete = function() {
   		var recordsToDelete = [];
		$("#claim-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		$("#claim-table tbody .row-select2").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		$("#claim-table tbody .row-select3").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});

		var json = JSON.stringify(recordsToDelete);

		$.ajax({
    		url:'${request.contextPath}/claim/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadClaimTable();
    		}
		});

   	}

});
     var checkin = $('#search_t017Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
        $('#search_t017Tanggal2')[0].focus();
    }).data('datepicker');

    var checkout = $('#search_t017Tanggal2').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    edit = function(id) {
        $('#spinner').fadeIn(1);
        window.location.replace('#/editClaim')
        $.ajax({url: '${request.contextPath}/claimInput?noClaim='+id,
            type:"GET",dataType: "html",
            complete : function (req, err) {
                $('#main-content').html(req.responseText);
                $('#spinner').fadeOut();
                loading = false;
            }
        });
    };
    klik = function(){

        $('#spinner').fadeIn(1);
        $.ajax({
            url: '${request.contextPath}/claimInput',
            type: "GET",dataType:"html",
            complete : function (req, err) {
                $('#main-content').html(req.responseText);
                $('#spinner').fadeOut();
            }
        });
    }

    printClaim = function(){
           checkClaim =[];
           var vendor,noClaim,tglClaim,petugasClaim, vendor = false;
            $("#claim-table tbody .row-select").each(function() {
                 if(this.checked){
                   var nRow = $(this).next("#noVendor").val()?$(this).next("#noVendor").val():"-"
                    if(nRow!="-"){
                        checkClaim.push(nRow);
                        vendor = true;
                    }
                }
            });
            if(checkClaim.length<1 ){
                alert('Silahkan Pilih Vendor Untuk Dicetak');
                return;
            }else if(vendor==false){
                alert('Silahkan Pilih tanda centang yang ada di Vendor');
                return;
            }
           var idClaim =  JSON.stringify(checkClaim);
           window.location = "${request.contextPath}/claim/printClaim?idClaim="+idClaim;
    }
 </g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">${title}</span>
        <ul class="nav pull-right">

            <li><a class="pull-right" href="#/claimInput" onclick="klik()" style="display: block;" >&nbsp;&nbsp;<i class="icon-plus"></i>&nbsp;&nbsp;
            <li><a class="pull-right box-action" href="#"
                   style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                        class="icon-remove"></i>&nbsp;&nbsp;
            </a></li>
            <li class="separator"></li>
        </ul>
	</div>
	<div class="box">
		<div class="span12" id="claim-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="t951Tanggal">
                                <g:message code="auditTrail.tanggal.label" default="Tanggal Claim" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_m777Tgl" class="controls">
                                <ba:datePicker id="search_t017Tanggal" name="search_t017Tanggal" precision="day" format="dd/MM/yyyy"  value="" />
                                &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                                <ba:datePicker id="search_t017Tanggal2" name="search_t017Tanggal2" precision="day" format="dd/MM/yyyy"  value=""  />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="view" >View</button>
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel" name="clear" id="clear" >Clear</button>
                            </div>
                        </td>
                    </tr>
                </table>

            </fieldset>
           	<g:render template="dataTables" />
            
           	<fieldset class="buttons controls" style="padding-top: 10px;">
                %{--<button id='ubahData' onclick="ubahClaim();" type="button" class="btn btn-primary">Ubah Data</button>--}%
                <button id='printClaimData' onclick="printClaim();" type="button" class="btn btn-primary">Print Claim</button>		<div class="span7" id="claim-form" style="display: none;"></div>

        </fieldset>
            
		</div>
	</div>
</body>
</html>
