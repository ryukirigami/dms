<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
var claimSubTable_${idTable};
$(function(){
var anOpen = [];
	$('#claim_datatables_sub_${idTable} td.subcontrol').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = claimSubTable_${idTable}.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/claim/subsublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = claimSubTable_${idTable}.fnOpen(nTr,data,'details');
    				$('div.innerinnerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerinnerDetails', $(nTr).next()[0]).slideUp( function () {
      			claimSubTable_${idTable}.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    if(claimSubTable_${idTable})
    	claimSubTable_${idTable}.dataTable().fnDestroy();
claimSubTable_${idTable} = $('#claim_datatables_sub_${idTable}').dataTable({
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": false,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "goodsReceive",
	"mDataProp": "t167NoReff",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" id="noReff" value="'+row['t171ID']+'.'+data+'">&nbsp;&nbsp; ' + data ;
    },
        "bSortable": false,
        "sWidth":"299px",
        "bVisible": true
    },
    {
        "sName": "goodsReceive",
        "mDataProp": "t167TglReff",
        "aTargets": [1],
        "bSortable": false,
        "sWidth":"180px",
        "bVisible": true
    },
    {
        "sName": "goodsReceive",
        "mDataProp": "t167TglJamReceive",
        "aTargets": [2],
        "bSortable": false,
        "sWidth":"250px",
        "bVisible": true
    },
    {
        "sName": "goodsReceive",
        "mDataProp": "t167PetugasReceive",
        "aTargets": [3],
        "bSortable": false,
        "sWidth":"250px",
        "bVisible": true
    }


        ],
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                                aoData.push(
                                            {"name": 'vendor', "value": "${vendor}"},
									{"name": 't171ID', "value": "${t171ID}"},
									{"name": 't171TglJamClaim', "value": "${t171TglJamClaim}"}

						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
    $('#claim_datatables_sub_${idTable} tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();

        var noClaim = $("#noVendor").val();
        console.log("id=" + id);
        if($(this).find('.row-select').is(":checked")){

        }
     });

});
		</g:javascript>
	</head>
	<body>
<div class="innerDetails">
<table id="claim_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover">
    <thead>
        <tr>
           <th></th>
           <th></th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nomor Reff</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Tanggal reff</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Tanggal dan Jam Receive</div>
			</th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Petugas Receive</div>
            </th>

        </tr>
			    </thead>
			    <tbody></tbody>
			</table>
</div>
</body>
</html>
