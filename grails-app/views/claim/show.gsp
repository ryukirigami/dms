

<%@ page import="com.kombos.parts.Claim" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'claim.label', default: 'Claim')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteClaim;

$(function(){ 
	deleteClaim=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/claim/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadClaimTable();
   				expandTableLayout('claim');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-claim" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="claim"
			class="table table-bordered table-hover">
			<tbody>

				

				<g:if test="${claimInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="claim.goods.label" default="Goods" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${claimInstance}" field="goods"
								url="${request.contextPath}/Claim/updatefield" type="text"
								title="Enter goods" onsuccess="reloadClaimTable();" />--}%
							
								%{--<g:link controller="goods" action="show" id="${claimInstance?.goods?.id}">${claimInstance?.goods?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${claimInstance?.goods}" field="m111Nama"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${claimInstance?.goodsReceive}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goodsReceive-label" class="property-label"><g:message
					code="claim.goodsReceive.label" default="Goods Receive" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goodsReceive-label">
						%{--<ba:editableValue
								bean="${claimInstance}" field="goodsReceive"
								url="${request.contextPath}/Claim/updatefield" type="text"
								title="Enter goodsReceive" onsuccess="reloadClaimTable();" />--}%
							
					%{--<g:link controller="goodsReceive" action="show" id="${claimInstance?.goodsReceive?.id}">${claimInstance?.goodsReceive?.encodeAsHTML()}</g:link>--}%
                        %{--<g:message default="${claimInstance?.goodsReceive?.t167ID}"  />--}%
                        <g:fieldValue bean="${claimInstance?.goodsReceive}" field="t167ID"/>
				    </span></td>

					
				</tr>
				</g:if>
			
				<g:if test="${claimInstance?.invoice}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="invoice-label" class="property-label"><g:message
					code="claim.invoice.label" default="Invoice" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="invoice-label">
						%{--<ba:editableValue
								bean="${claimInstance}" field="invoice"
								url="${request.contextPath}/Claim/updatefield" type="text"
								title="Enter invoice" onsuccess="reloadClaimTable();" />--}%
							
								%{--<g:link controller="invoice" action="show" id="${claimInstance?.invoice?.id}">${claimInstance?.invoice?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${claimInstance?.invoice}" field="t166NoInv"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${claimInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="claim.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${claimInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Claim/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadClaimTable();" />--}%
							
								<g:fieldValue bean="${claimInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${claimInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="claim.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${claimInstance}" field="lastUpdated"
								url="${request.contextPath}/Claim/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadClaimTable();" />--}%
							
								<g:formatDate date="${claimInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${claimInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="claim.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${claimInstance}" field="createdBy"
                                url="${request.contextPath}/Claim/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadClaimTable();" />--}%

                        <g:fieldValue bean="${claimInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${claimInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="claim.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${claimInstance}" field="dateCreated"
                                url="${request.contextPath}/Claim/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadClaimTable();" />--}%

                        <g:formatDate date="${claimInstance?.dateCreated}" />

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${claimInstance?.t171ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t171ID-label" class="property-label"><g:message
					code="claim.t171ID.label" default="T171 ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t171ID-label">
						%{--<ba:editableValue
								bean="${claimInstance}" field="t171ID"
								url="${request.contextPath}/Claim/updatefield" type="text"
								title="Enter t171ID" onsuccess="reloadClaimTable();" />--}%
							
								<g:fieldValue bean="${claimInstance}" field="t171ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${claimInstance?.t171PetugasClaim}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t171PetugasClaim-label" class="property-label"><g:message
					code="claim.t171PetugasClaim.label" default="T171 Petugas Claim" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t171PetugasClaim-label">
						%{--<ba:editableValue
								bean="${claimInstance}" field="t171PetugasClaim"
								url="${request.contextPath}/Claim/updatefield" type="text"
								title="Enter t171PetugasClaim" onsuccess="reloadClaimTable();" />--}%
							
								<g:fieldValue bean="${claimInstance}" field="t171PetugasClaim"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${claimInstance?.t171Qty1Issued}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t171Qty1Issued-label" class="property-label"><g:message
					code="claim.t171Qty1Issued.label" default="T171 Qty1 Rec" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t171Qty1Issued-label">
						%{--<ba:editableValue
								bean="${claimInstance}" field="t171Qty1Issued"
								url="${request.contextPath}/Claim/updatefield" type="text"
								title="Enter t171Qty1Issued" onsuccess="reloadClaimTable();" />--}%
							
								<g:fieldValue bean="${claimInstance}" field="t171Qty1Issued"/>
							
						</span></td>
					
				</tr>
				</g:if>

			
				<g:if test="${claimInstance?.t171Qty1Reff}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t171Qty1Reff-label" class="property-label"><g:message
					code="claim.t171Qty1Reff.label" default="T171 Qty1 Reff" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t171Qty1Reff-label">
						%{--<ba:editableValue
								bean="${claimInstance}" field="t171Qty1Reff"
								url="${request.contextPath}/Claim/updatefield" type="text"
								title="Enter t171Qty1Reff" onsuccess="reloadClaimTable();" />--}%
							
								<g:fieldValue bean="${claimInstance}" field="t171Qty1Reff"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${claimInstance?.t171Qty1Rusak}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t171Qty1Rusak-label" class="property-label"><g:message
					code="claim.t171Qty1Rusak.label" default="T171 Qty1 Rusak" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t171Qty1Rusak-label">
						%{--<ba:editableValue
								bean="${claimInstance}" field="t171Qty1Rusak"
								url="${request.contextPath}/Claim/updatefield" type="text"
								title="Enter t171Qty1Rusak" onsuccess="reloadClaimTable();" />--}%
							
								<g:fieldValue bean="${claimInstance}" field="t171Qty1Rusak"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${claimInstance?.t171Qty1Salah}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t171Qty1Salah-label" class="property-label"><g:message
					code="claim.t171Qty1Salah.label" default="T171 Qty1 Salah" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t171Qty1Salah-label">
						%{--<ba:editableValue
								bean="${claimInstance}" field="t171Qty1Salah"
								url="${request.contextPath}/Claim/updatefield" type="text"
								title="Enter t171Qty1Salah" onsuccess="reloadClaimTable();" />--}%
							
								<g:fieldValue bean="${claimInstance}" field="t171Qty1Salah"/>
							
						</span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${claimInstance?.t171TglJamClaim}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t171TglJamClaim-label" class="property-label"><g:message
					code="claim.t171TglJamClaim.label" default="T171 Tgl Jam Claim" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t171TglJamClaim-label">
						%{--<ba:editableValue
								bean="${claimInstance}" field="t171TglJamClaim"
								url="${request.contextPath}/Claim/updatefield" type="text"
								title="Enter t171TglJamClaim" onsuccess="reloadClaimTable();" />--}%
							
								<g:formatDate date="${claimInstance?.t171TglJamClaim}" />
							
						</span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${claimInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="claim.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${claimInstance}" field="updatedBy"
								url="${request.contextPath}/Claim/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadClaimTable();" />--}%
							
								<g:fieldValue bean="${claimInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${claimInstance?.vendor}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="vendor-label" class="property-label"><g:message
					code="claim.vendor.label" default="Vendor" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="vendor-label">
						%{--<ba:editableValue
								bean="${claimInstance}" field="vendor"
								url="${request.contextPath}/Claim/updatefield" type="text"
								title="Enter vendor" onsuccess="reloadClaimTable();" />--}%
							
								%{--<g:link controller="vendor" action="show" id="${claimInstance?.vendor?.id}">${claimInstance?.vendor?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${claimInstance?.vendor}" field="m121Nama"/>
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${claimInstance?.id}"
					update="[success:'claim-form',failure:'claim-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteClaim('${claimInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
