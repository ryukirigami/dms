
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay" />

<table id="claim_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover  table-selectable" style="table-layout: fixed; ">
	<thead>
		<tr>
            <th style="vertical-align: middle;">
                <div style="height: 10px"> </div>
            </th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Vendor</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nomor Claim</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Tanggal Claim</div>
			</th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Petugas Claim</div>
            </th>

        </tr>
		
	</thead>
</table>

<g:javascript>
var claimTable;
var reloadClaimTable;
$(function(){
  var recordspartsperpage = [];//new Array();
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;
	var anOpen = [];
reloadClaimTable = function() {
		claimTable.fnDraw();
	}
 $('#clear').click(function(e){
        $('#search_t017Tanggal').val("");
        $('#search_t017Tanggal_day').val("");
        $('#search_t017Tanggal_month').val("");
        $('#search_t017Tanggal_year').val("");
        $('#search_t017Tanggal2').val("");
        $('#search_t017Tanggal2_day').val("");
        $('#search_t017Tanggal2_month').val("");
        $('#search_t017Tanggal2_year').val("");
        e.stopPropagation();
        claimTable.fnDraw();
	});
    $('#view').click(function(e){
        e.stopPropagation();
		claimTable.fnDraw();
	});
	var anOpen = [];
	$('#claim_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = claimTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/claim/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = claimTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			claimTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});
	
    $('#claim_datatables_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( claimTable, nEditing );
            editRow( claimTable, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( claimTable, nEditing );
            nEditing = null;
        }
        else {
            editRow( claimTable, nRow );
            nEditing = nRow;
        }
    } );
    
	reloadClaimTable = function() {
		claimTable.fnDraw();
	}

 	claimTable = $('#claim_datatables_${idTable}').dataTable({

		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsParts = $("#claim_datatables_${idTable} tbody .row-select");
            var jmlPartsCek = 0;
            var nRow;
            var idRec;
            rsParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordspartsperpage[idRec]=="1"){
                    jmlPartsCek = jmlPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspartsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsPerPage = rsParts.length;
            if(jmlPartsCek == jmlRecPartsPerPage && jmlRecPartsPerPage > 0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "vendor",
	"mDataProp": "vendor",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox"  class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" id="noVendor" value="'+row['t171ID']+'">&nbsp;&nbsp;' + data+ ' <a class="pull-right cell-action" href="javascript:void(0);" onclick="edit(\''+row['t171ID']+'\');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": false,
	"sWidth":"325px",
	"bVisible": true
},
{
	"sName": "t171ID",
	"mDataProp": "t171ID",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"180px",
	"bVisible": true
},
{
	"sName": "t171TglJamClaim",
	"mDataProp": "t171TglJamClaim",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
},
{
	"sName": "t171PetugasClaim",
	"mDataProp": "t171PetugasClaim",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var t017Tanggal = $('#search_t017Tanggal').val();
						var t017TanggalDay = $('#search_t017Tanggal_day').val();
						var t017TanggalMonth = $('#search_t017Tanggal_month').val();
						var t017TanggalYear = $('#search_t017Tanggal_year').val();

						var t017Tanggal2 = $('#search_t017Tanggal2').val();
						var t017TanggalDay2 = $('#search_t017Tanggal2_day').val();
						var t017TanggalMonth2 = $('#search_t017Tanggal2_month').val();
						var t017TanggalYear2 = $('#search_t017Tanggal2_year').val();

						if(t017Tanggal){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal_dp', "value": t017Tanggal},
									{"name": 'sCriteria_t017Tanggal_day', "value": t017TanggalDay},
									{"name": 'sCriteria_t017Tanggal_month', "value": t017TanggalMonth},
									{"name": 'sCriteria_t017Tanggal_year', "value": t017TanggalYear}
							);
						}


						if(t017Tanggal2){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal2', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal2_dp', "value": t017Tanggal2},
									{"name": 'sCriteria_t017Tanggal2_day', "value": t017TanggalDay2},
									{"name": 'sCriteria_t017Tanggal2_month', "value": t017TanggalMonth2},
									{"name": 'sCriteria_t017Tanggal2_year', "value": t017TanggalYear2}
							);
						}
	
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
    %{--$('#claim_datatables_${idTable} tbody tr').live('click', function () {--}%
        %{--id = $(this).find('.row-select').next("input:hidden").val();--}%

        %{--var noClaim = $("#noVendor").val();--}%
        %{--console.log("id=" + id);--}%
        %{--if($(this).find('.row-select').is(":checked")){--}%
            %{--recordspartsperpage[id] = "1";--}%
            %{--$(this).find('.row-select').parent().parent().addClass('row_selected');--}%
            %{--anPartsSelected = claimTable.$('tr.row_selected');--}%
            %{--console.log("jmlRecPartsPerPage=" + jmlRecPartsPerPage + " anPartsSelected=" + anPartsSelected);--}%
            %{--if(jmlRecPartsPerPage == anPartsSelected.length){--}%
                %{--$('.select-all').attr('checked', true);--}%
            %{--}--}%

            %{--$('table[id^=claim_datatables_sub_] tbody .row-select2').each(function() {--}%
                %{--var noClaimInnerArr = $('#noReff').val().split('.');--}%
                %{--var noClaimInner = noClaimInnerArr[0]--}%
                %{--console.log("row-select2 noClaim=" + noClaim + " noClaimInner=" + noClaimInner);--}%
                %{--if (noClaim == noClaimInner) {--}%
                    %{--$(this).attr('checked', true);--}%
                    %{--var nRow = $(this).parent().parent();//.addClass('row_selected');--}%
                    %{--nRow.addClass('row_selected');--}%
                %{--}--}%
            %{--});--}%
            %{--$('table[id^=claim_datatables_sub_sub_] tbody .row-select3').each(function() {--}%
                %{--var noClaimInnerArr = $('#idPart').val().split('.');--}%
                %{--var noClaimInner = noClaimInnerArr[0]--}%
                %{--console.log("row-select2 noClaim=" + noClaim + " noClaimInner=" + noClaimInner);--}%
                %{--if (noClaim == noClaimInner) {--}%
                    %{--$(this).attr('checked', true);--}%
                    %{--var nRow = $(this).parent().parent();//.addClass('row_selected');--}%
                    %{--nRow.addClass('row_selected');--}%
                %{--}--}%
            %{--});--}%

        %{--} else {--}%
            %{--recordspartsperpage[id] = "0";--}%
            %{--$('.select-all').attr('checked', false);--}%
            %{--$(this).find('.row-select').parent().parent().removeClass('row_selected');--}%

            %{--$('table[id^=claim_datatables_sub_] tbody .row-select2').each(function() {--}%
                 %{--var noClaimInnerArr = $('#noReff').val().split('.');--}%
                %{--var noClaimInner = noClaimInnerArr[0]--}%

                %{--console.log("row-select2 noClaim=" + noClaim + " noClaimInner=" + noClaimInner);--}%
                %{--if (noClaim == noClaimInner) {--}%
                    %{--$(this).attr('checked', false);--}%

                    %{--var nRow = $(this).parent().parent();//.addClass('row_selected');--}%
                    %{--nRow.removeClass('row_selected');--}%
                %{--}--}%
            %{--});--}%
            %{--$('table[id^=claim_datatables_sub_sub_] tbody .row-select3').each(function() {--}%
                 %{--var noClaimInnerArr = $('#idPart').val().split('.');--}%
                %{--var noClaimInner = noClaimInnerArr[0]--}%

                %{--console.log("row-select2 noClaim=" + noClaim + " noClaimInner=" + noClaimInner);--}%
                %{--if (noClaim == noClaimInner) {--}%
                    %{--$(this).attr('checked', false);--}%

                    %{--var nRow = $(this).parent().parent();//.addClass('row_selected');--}%
                    %{--nRow.removeClass('row_selected');--}%
                %{--}--}%
            %{--});--}%
        %{--}--}%

    %{--});--}%
});



</g:javascript>



