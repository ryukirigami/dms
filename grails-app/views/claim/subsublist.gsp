<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
var claimSubSubTable_${idTable};
$(function(){ 
claimSubSubTable_${idTable} = $('#claim_datatables_sub_sub_${idTable}').dataTable({
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"18px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"15px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
	"sName": "kodePart",
	"mDataProp": "kodePart",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"> <input id="idPart" type="hidden" value="'+row['t171ID']+'.'+row['id']+'">&nbsp;&nbsp; ' + data ;
            %{--return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';--}%
	},
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "namaPart",
	"mDataProp": "namaPart",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"190px",
	"bVisible": true
},
{
	"sName": "invoice",
	"mDataProp": "noPO",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"108px",
	"bVisible": true
},
{
	"sName": "invoice",
	"mDataProp": "tanggalPO",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"107px",
	"bVisible": true
},
{
	"sName": "invoice",
	"mDataProp": "tipeOrder",
	"aTargets": [4],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "t171Qty1Reff",
	"mDataProp": "t171Qty1Reff",
	"aTargets": [5],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "t171Qty1Issued",
	"mDataProp": "t171Qty1Issued",
	"aTargets": [6],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "t171Qty1Rusak",
	"mDataProp": "t171Qty1Rusak",
	"aTargets": [7],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "t171Qty1Salah",
	"mDataProp": "t171Qty1Salah",
	"aTargets": [8],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "selisih",
	"mDataProp": "selisih",
	"aTargets": [9],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 't167NoReff', "value": "${t167NoReff}"},
									{"name": 't171ID', "value": "${t171ID}"},
									{"name": 't171TglJamClaim', "value": "${t171TglJamClaim}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});


});
		</g:javascript>
	</head>
	<body>
	<div class="innerinnerDetails">
<table id="claim_datatables_sub_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover">
    <thead>
        <tr>
           <th></th>
           <th></th>
           <th></th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Kode Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nama Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>No PO</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Tanggal PO</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Tipe Order</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Qty Reff</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Qty Receive</div>
			</th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Qty Rusak</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Qty Salah</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Qty Selisih</div>
            </th>
        </tr>
			    </thead>
			    <tbody></tbody>
			</table>
	</div>
</body>
</html>
