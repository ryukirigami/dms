
<%@ page import="com.kombos.parts.GoodsReceive" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="requestAdd_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed table-selectable" style="table-layout: fixed; ">
    <col width="150px" />
	<col width="200px" />
    <col width="200px" />
    <col width="200px" />
    <col width="200px" />
    <col width="145px" />
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px 10px 7px 7px; width: 150px;">
            <div><input type="checkbox" class="pull-left select-all sel-all2" aria-label="Select all" title="Select all"/>&nbsp;&nbsp;Nomor Urut</div>
        </th>

        <th style="border-bottom: none;padding: 5px 10px 7px 7px; width: 199px; text-align:center">
            <div class="center">Kode Part</div>
        </th>

        <th style="border-bottom: none;padding: 5px 10px 7px 7px; width: 199px; text-align:center">
            <div class="center">Nama Part</div>
        </th>
        <th style="border-bottom: none;padding: 5px 10px 7px 7px; width: 199px; text-align:center">
            <div class="center">Nomor PO</div>
        </th>
        <th style="border-bottom: none;padding: 5px 10px 7px 7px; width: 199px; text-align:center">
            <div class="center">Tanggal PO</div>
        </th>
        <th style="border-bottom: none;padding: 5px 10px 7px 7px; width: 143px; text-align:center">
            <div class="center">Tipe Order</div>
            <input type="hidden" name="input_vendor" id="input_vendor" value="${input_vendor}">
            <input type="hidden" name="input_companyDealer" id="input_companyDealer" value="${input_companyDealer}">
            <input type="hidden" name="pilihVendor" id="pilihVendor" value="${pilihVendor}">
        </th>
    </tr>
    <tr>
        <th style="border-top: none;padding: 0px 10px 7px 7px; ">
            <div id="filter_urut" style="padding-top: 0px;position:relative; margin-top: 0px;">
             &nbsp;
            </div>
        </th>
        <th style="border-top: none;padding: 0px 10px 7px 7px; ">
            <div id="filter_kodeParts" style="padding-top: 0px;position:relative; margin-top: 0px;">
                <input type="text" name="search_kodeParts" class="search_init" />
            </div>
        </th>
        <th style="border-top: none;padding: 0px 10px 7px 7px; ">
            <div id="filter_namaParts" style="padding-top: 0px;position:relative; margin-top: 0px;">
                <input type="text" name="search_namaParts" class="search_init" />
            </div>
        </th>
        <th style="border-top: none;padding: 0px 10px 7px 7px; ">
            <div id="filter_nomorPO" style="padding-top: 0px;position:relative; margin-top: 0px;">
                <input type="text" name="search_nomorPO" class="search_init" />
            </div>
        </th>
        <th style="border-top: none;padding: 0px 10px 7px 7px; ">
            <div id="filter_tglPO" class="controls" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >

                <input type="hidden" name="search_tglPO" value="date.struct">
                <input type="hidden" name="search_tglPO_day" id="search_tglPO_day" value="">
                <input type="hidden" name="search_tglPO_month" id="search_tglPO_month" value="">
                <input type="hidden" name="search_tglPO_year" id="search_tglPO_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_tglPO_dp" value="" id="search_tglPO" class="search_init">

            </div>
        </th>
        <th style="border-top: none;padding: 0px 10px 7px 7px; ">
            <div id="filter_tipeOrder" style="padding-top: 0px;position:relative; margin-top: 0px;">
                <input type="text" name="search_tipeOrder" class="search_init" />
            </div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var requestAddTable;
var reloadrequestAddTable;
$(function(){

	reloadrequestAddTable = function() {
		requestAddTable.fnDraw();
	}
    var recordspartsperpage = [];//new Array();
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;

	$('#search_tglPO').datepicker().on('changeDate', function(ev) {
	    console.log("inside search_tglPO datepicker");
        var newDate = new Date(ev.date);
        $('#search_tglPO_day').val(newDate.getDate());
        $('#search_tglPO_month').val(newDate.getMonth()+1);
        $('#search_tglPO_year').val(newDate.getFullYear());
        $(this).datepicker('hide');
    	//requestAddTable.fnDraw();
    	var oTable = $('#requestAdd_datatables').dataTable();
        oTable.fnReloadAjax();
        console.log("after oTable.fnReloadAjax()");
	});




$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	requestAddTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	requestAddTable = $('#requestAdd_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',

        "fnDrawCallback": function () {
            var rsParts = $("#requestAdd_datatables tbody .row-select");
            var jmlPartsCek = 0;
            var nRow;
            var idRec;
            rsParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordspartsperpage[idRec]=="1"){
                    jmlPartsCek = jmlPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspartsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsPerPage = rsParts.length;
            if(jmlPartsCek == jmlRecPartsPerPage && jmlRecPartsPerPage > 0){
                $('.sel-all2').attr('checked', true);
            } else {
                $('.sel-all2').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "addModalDatatablesList")}",
		"aoColumns": [

{
	"sName": "id",
	"mDataProp": "norut",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    if(row['qtyIssue']==0){
	        return data;
	    }else{
	        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	    }

	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}

,

{
	"sName": "kodePart",
	"mDataProp": "kodePart",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,

{
	"sName": "namaPart",
	"mDataProp": "namaPart",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,

{
	"sName": "nomorPO",
	"mDataProp": "nomorPO",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,

{
	"sName": "tglPO",
	"mDataProp": "tglPO",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,

{
	"sName": "tipeOrder",
	"mDataProp": "tipeOrder",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        var input_vendor = $('#input_vendor').val();
                        var input_companyDealer = $('#input_companyDealer').val();
                        var pilihVendor = $('#pilihVendor').val();
                        console.log("input_vendor=" + input_vendor);
                        aoData.push(
                            {"name": 'input_vendor', "value": input_vendor},
                            {"name": 'input_companyDealer', "value": input_companyDealer},
                            {"name": 'pilihVendor', "value": pilihVendor}
                        );
						var kodeParts = $('#filter_kodeParts input').val();
						if(kodeParts){
							aoData.push(
									{"name": 'sCriteria_kodeParts', "value": kodeParts}
							);
						}

						var namaParts = $('#filter_namaParts input').val();
						if(namaParts){
							aoData.push(
									{"name": 'sCriteria_namaParts', "value": namaParts}
							);
						}
						var nomorPO = $('#filter_nomorPO input').val();
						if(nomorPO){
							aoData.push(
									{"name": 'sCriteria_nomorPO', "value": nomorPO}
							);
						}

						var tipeOrder = $('#filter_tipeOrder input').val();
						if(tipeOrder){
							aoData.push(
									{"name": 'sCriteria_tipeOrder', "value": tipeOrder}
							);
						}

						var exist =[];
						var rows = $("#parts_datatables").dataTable().fnGetNodes();
                        for(var i=0;i<rows.length;i++)
                        {
							var id = $(rows[i]).find("#idPopUp").val();
							exist.push(id);
						}
						if(exist.length > 0){
							aoData.push(
										{"name": 'sCriteria_exist', "value": JSON.stringify(exist)}
							);
						}

						var tglPO = $('#search_tglPO').val();
						var tglPODay = $('#search_tglPO_day').val();
						var tglPOMonth = $('#search_tglPO_month').val();
						var tglPOYear = $('#search_tglPO_year').val();

						if(tglPO){

							aoData.push(
									{"name": 'sCriteria_tglPO', "value": "date.struct"},
									{"name": 'sCriteria_tglPO_dp', "value": tglPO},
									{"name": 'sCriteria_tglPO_day', "value": tglPODay},
									{"name": 'sCriteria_tglPO_month', "value": tglPOMonth},
									{"name": 'sCriteria_tglPO_year', "value": tglPOYear}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});


    $('.sel-all2').click(function(e) {

        $("#requestAdd_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordspartsperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordspartsperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#requestAdd_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordspartsperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPartsSelected = requestAddTable.$('tr.row_selected');
            if(jmlRecPartsPerPage == anPartsSelected.length){
                $('.sel-all2').attr('checked', true);
            }
        } else {
            recordspartsperpage[id] = "0";
            $('.sel-all2').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>



