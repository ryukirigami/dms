
<%@ page import="com.kombos.parts.GoodsReceive" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="Add Parts" />
    <title><g:message code="default.list.label" args="" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

   });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
</div>
<div class="box">
    <div class="span12" id="requestAdd-table">
        <g:render template="addModalDataTables" />
        <g:field type="button" onclick="tambahReq();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.upload.label', default: 'Add Selected')}" />
        <span style="margin-left: 930px"><button id='closeAddPart' type="button" class="btn cancel">Close</button></span>
    </div>

</div>
</body>
</html>
