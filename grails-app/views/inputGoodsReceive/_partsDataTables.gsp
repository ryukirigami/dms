<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="parts_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable fixed" style="table-layout: fixed; ">
           <col width="100px" />
       	   <col width="70px" />
           <col width="40px" />
           <col width="40px" />
           <col width="40px" />
           <col width="40px" />
           <col width="40px" />
    <thead>
    <tr>
        <th style="border-bottom: none; text-align:center">
            <div class="center"><input type="checkbox" style=" width: 13px;
                            height: 13px;
                            padding: 0;
                            margin:0;
                            vertical-align: bottom;
                            position: relative;
                            top: -1px;
                            *overflow: hidden;" class="pull-left select-all" />Kode Parts</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Nama Parts</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Nomor PO</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Tanggal PO</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Tipe Order</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Qty Reff</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Qty Receive</div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var partsTable;
var reloadPartsTable;
$(function(){

    reloadPartsTable = function() {
		partsTable.fnDraw();
	}
    var recordspartsperpage = [];//new Array();
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;

    $("th div input").bind('keypress', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) {
            e.stopPropagation();
            partsTable.fnDraw();
        }
    });
    $("th div input").click(function (e) {
        e.stopPropagation();
    });

	partsTable = $('#parts_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsParts = $("#parts_datatables tbody .row-select");
            var jmlPartsCek = 0;
            var nRow;
            var idRec;
            rsParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                recordsPartsAll.push(idRec);
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordspartsperpage[idRec]=="1"){
                    jmlPartsCek = jmlPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspartsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsPerPage = rsParts.length;
            if(jmlPartsCek == jmlRecPartsPerPage && jmlRecPartsPerPage > 0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
        "sAjaxSource": "${g.createLink(action: "inputGoodsReceiveDatatablesList")}",
		"aoColumns": [
{
	"sName": "partsCode",
	"mDataProp": "partsCode",
	"aTargets": [0],
    "mRender": function ( data, type, row ) {
        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="idPopUp" value="'+row['id']+'">&nbsp;&nbsp;'+data;
    },
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "partsName",
	"mDataProp": "partsName",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "poNumber",
	"mDataProp": "poNumber",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "poDate",
	"mDataProp": "poDate",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "orderType",
	"mDataProp": "orderType",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "qtyRef",
	"mDataProp": "qtyRef",
	"aTargets": [5],
	"mRender": function ( data, type, row ) {
			return '<input id="qty-'+row['id']+'" class="inline-edit" type="text" style="width:50px;" value="'+data+'">';
		},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "qtyRcv",
	"mDataProp": "qtyRcv",
	"aTargets": [6],
	"mRender": function ( data, type, row ) {
				return '<input id="qty2-'+row['id']+'" class="inline-edit" type="text" style="width:50px;" value="'+data+'">';
			},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            aoData.push(
                {"name": 'goodsReceiveId', "value": "${goodsReceiveInstance?.id}"}
            );
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});

    $('.select-all').click(function(e) {

        $("#parts_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordspartsperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordspartsperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#parts_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordspartsperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPartsSelected = partsTable.$('tr.row_selected');
            if(jmlRecPartsPerPage == anPartsSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordspartsperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>
