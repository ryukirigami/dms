
<%@ page import="com.kombos.administrasi.CompanyDealer" contentType="text/html;charset=UTF-8" %>
<html>
<head>
<head>
<meta name="layout" content="main">
<title>Input Goods Receive</title>
<r:require modules="baseapplayout"/>

<g:javascript disposition="head">
    var recordsPartsAll = [];   //untuk menyimpan dari modal
	var noUrut = 1;
	var pilVend = "1";
	var namaVendor = "";
	var namaCD = "";
	var checkOnValue;

        $(function () {

        $("#input_ppn").autoNumeric({
            vMin:'0',
            vMax:'999999999999999999'
        });

	$("#requestAddModal").on("show", function() {
		//$("#requestAddModal .btn").on("click", function(e) {
        $("#closeAddPart").on("click", function(e) {
			$("#requestAddModal").modal('hide');
		});
	});

	$("#requestAddModal").on("hide", function() {
		$("#requestAddModal a.btn").off("click");
	});

    loadRequestAddModal = function(){
         if(namaVendor != $('#input_vendor').val()){
           $("#goodsReceive_table tbody .row-select").each(function() {
                var id = $(this).next("input:hidden").val();
                var row = $(this).closest("tr").get(0);
                partsTable.fnDeleteRow(partsTable.fnGetPosition(row));
            });
         }
         if(namaCD != $('#input_companyDealer').val()){
           $("#goodsReceive_table tbody .row-select").each(function() {
                var id = $(this).next("input:hidden").val();
                var row = $(this).closest("tr").get(0);
                partsTable.fnDeleteRow(partsTable.fnGetPosition(row));
            });
         }
        namaVendor = $("#input_vendor").val();
        namaCD = $("#input_companyDealer").val();
		$("#requestAddContent").empty();
		$.ajax({type:'POST', url:'${request.contextPath}/inputGoodsReceive/addModal',
		    data : { input_vendor: namaVendor,input_companyDealer:namaCD,pilihVendor:pilVend},
   			success:function(data,textStatus){
					$("#requestAddContent").html(data);
				    $("#requestAddModal").modal({
						"backdrop" : "static",
						"keyboard" : true,
						"show" : true
					}).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
		}

       tambahReq = function(){
            var checkGoods =[];
            $("#requestAdd-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var nRow = $(this).parents('tr')[0];
                    var aData = requestAddTable.fnGetData(nRow);
                    var code = aData['kodePart'];
                    var isNotYet = true;
                    $('#parts_datatables tbody .row-select').each(function() {
                        var nRowExisting = $(this).parents('tr')[0];
                        var aDataExisting = partsTable.fnGetData(nRowExisting);
                        var codeExisting = aDataExisting['partsCode'];
                        if (code === codeExisting) {
                            isNotYet = false;
                        }
                    });
                    if (isNotYet) {
                        checkGoods.push(nRow);
                        recordsPartsAll.push(id);
                    }
                }
            });
            if(checkGoods.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                loadRequestAddModal();
            } else {
				for (var i=0;i < checkGoods.length;i++){
					var aData = requestAddTable.fnGetData(checkGoods[i]);
					partsTable.fnAddData({
					    'id': aData['id'],
                        'norut': noUrut,
                        'partsCode': aData['kodePart'],
                        'partsName': aData['namaPart'],
                        'poNumber': aData['nomorPO'],
                        'poDate': aData['tglPO'],
                        'orderType': aData['tipeOrder'],
                        'qtyRef': aData['qtyIssue'],
                        'qtyRcv': aData['qtyIssue']

						});
					noUrut++;
					$('#qty-'+aData['id']).autoNumeric('init',{
						vMin:'0',
						vMax:'9999999999999999',
						mDec: '2',
						aSep:''
					}).keypress(checkOnValue)
					.change(checkOnValue);

					$('#qty2-'+aData['id']).autoNumeric('init',{
                        vMin:'0',
                        vMax:'999999999999999999',
                        mDec: '2',
                        aSep:''
                    }).keypress(checkOnValue)
                    .change(checkOnValue);
				}
                var rowNum = checkGoods.pop();
                requestAddTable.fnDeleteRow(rowNum);
			}
      }

             onDeleteParts = function(){
                var checkedRows =[];
                $('#parts_datatables tbody .row-select').each(function() {
                    if(this.checked){
                        var row = $(this).closest('tr');
                        var nRow = row[0];
                        console.log("nRow=" + nRow);
                        checkedRows.push(nRow);
                    }
                });
                for (var i=checkedRows.length-1; i >= 0 ;i--){
                    var rowNum = checkedRows.pop();
                    partsTable.fnDeleteRow(rowNum);
                }
             }

             onSaveData = function(){
                var staVendor = '0'
                var x = document.getElementById('pilih0').checked
                if(x==true){
                    staVendor="1"
                }
                else{
                    staVendor="0"
                }
                 var input_vendor = $("#input_vendor").val();
                 var input_companyDealer = $("#input_companyDealer").val();
                 var pilihVendor = staVendor
                 var input_noReff = $("#input_noReff").val();
                 var input_reffDate = $("#input_reffDate").val();
                 var input_rcvDate = $("#input_rcvDate").val();
                 var input_rcvHour = $("#input_rcvHour").val();
                 var input_rcvMinuite = $("#input_rcvMinuite").val();
                 var goodsReceive_id = $("#goodsReceive_id").val();
                 var input_ppn = $("#input_ppn").val();
                 var input_tipeBayar = $("#input_tipeBayar").val();
                 console.log("goodsReceive_id=" + goodsReceive_id);
                 console.log("input_ppn=" + input_ppn + " input_tipeBayar=" + input_tipeBayar);
                 if ((input_vendor || input_companyDealer) && input_noReff && input_reffDate && input_rcvDate) {
                     if(confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}'))
                        {

                        try{
                            var checkJob =[];
                            var qty = "";
                            var qty2 = "";
                                //$(".row-select").each(function() {
                                $("#goodsReceive_table tbody .row-select").each(function() {
                                    if(this.checked){
                                        var id = $(this).next("input:hidden").val();
                                        checkJob.push(id);
                                        qty += "&qty-"+id+"="+document.getElementById("qty-"+id).value;
                                        qty2 += "&qty2-"+id+"="+document.getElementById("qty2-"+id).value;
                                        console.log("checkJob=" + checkJob + " id=" + id + " qty="+qty+" qty2="+qty2);
                                    }
                                });
                                console.log("input_vendor=" + input_vendor + ", input_noReff=" + input_noReff + ", input_reffDate=" + input_reffDate + ", input_rcvDate=" + input_rcvDate);

                                if(checkJob.length == 0){
                                    alert('Anda belum memilih data yang akan disimpan');
                                    return;
                                } else {
                                    var checkBoxVendor = JSON.stringify(checkJob);
                                    console.log("checkBoxVendor=" + checkBoxVendor);
                                    console.log("qty=" + qty +" qty2=" + qty2);
                                    $.ajax({
                                        url:'${request.contextPath}/inputGoodsReceive/save?1=1'+qty+qty2,
                                        type: "POST", // Always use POST when deleting data
                                        data : { ids: checkBoxVendor, input_vendor:input_vendor,input_companyDealer:input_companyDealer,pilihVendor:pilihVendor, input_noReff:input_noReff, input_reffDate:input_reffDate, input_rcvDate:input_rcvDate,
                                                                        input_rcvHour:input_rcvHour, input_rcvMinuite:input_rcvMinuite, goodsReceive_id: goodsReceive_id,
                                                                        qty:qty, qty2:qty2, input_tipeBayar: input_tipeBayar, input_ppn: input_ppn   },
                                        success : function(data){
                                            if(data.error){
                                                alert(data.error)
                                            }else{
                                                if (data === "isExist") {
                                                    alert("Parts Receive dengan No Referensi yang sama sudah ada");
                                                } else {
                                                    toastr.success('Save success');
                                                    $('#goodsReceive-form').empty();
                                                    $('#goodsReceive-form').append(data);
                                                }
                                            }
                                        },
                                        error: function(xhr, textStatus, errorThrown) {
                                            alert('Internal Server Error');
                                            partsTable.fnClearTable();
                                        }
                                    });

                                    checkJob = [];
                                }

                        }catch (e){
                            alert(e)
                        }
                        }
                 }
                 else {
                     //pake alert krn belum menemukan solusi jika tgl kosong
                     if (!input_noReff) {
                        alert("Silahkan mengisi Nomor Referensi");
                     } else if (!input_reffDate) {
                         alert("Silahkan memiih Tanggal Referensi");
                     } else if (!input_rcvDate) {
                         alert("Silahkan memiih Tanggal Receive");
                     }
                 }
             }


    shrinkTableLayout = function(){
        $("#goodsReceive-table").hide();
        console.log("afterHide");
        $("#goodsReceive-form").css("display","block");
   	}

    cancelling = function() {
        shrinkTableLayout();
        $('#spinner').fadeIn(1);
        $.ajax({type:'POST', url:'${request.contextPath}/goodsReceive/',
            success:function(data,textStatus){
                //loadForm(data, textStatus);
                $('#goodsReceive-form').empty();
                $('#goodsReceive-form').append(data);
            },
            error:function(XMLHttpRequest,textStatus,errorThrown){},
            complete:function(XMLHttpRequest,textStatus){
                $('#spinner').fadeOut();
            }
        });
    }
            document.getElementById('pilih0').checked = true
            $('#vendorData').prop('disabled',false)
            $('#input_companyDealer').prop('disabled',true)
            getRadio = function(a){
                var x = document.getElementById(a).checked
                if(a=='pilih0'){
                    if(x==true){
                        pilVend = '1'
                        $('#vendorData').prop('disabled',false)
                        $('#input_companyDealer').prop('disabled',true)
                    }
                }else{
                        pilVend = '0'
                        $('#vendorData').prop('disabled',true)
                        $('#input_companyDealer').prop('disabled',false)
                }
            }
        });

</g:javascript>
<g:javascript>
			$('#vendorData').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/inputInvoice/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});

            detailVendor = function(){
                    var vendorData = $('#vendorData').val();
                    $.ajax({
                        url:'${request.contextPath}/inputGoodsReceive/detailVendor?vendorData='+vendorData,
                        type: "POST", // Always use POST when deleting data
                        success : function(data){
                            $('#input_vendor').val(data);
                        }
                    })
            }
</g:javascript>
</head>

<body>
<g:formRemote class="form-horizontal" name="index" on404="alert('not found!');" onSuccess="reloadPartsTable();" update="inputGoodsReceive-form"
              url="[controller: 'inputGoodsReceive', action:'save']">
    <div class="box">
        <div class="span12" id="inputGoodsReceive-table">
        <form id="form-inputGoodsReceive" class="form-horizontal">
        <legend style="font-size: small">Input Parts Receive</legend>
        <fieldset>
        <g:hiddenField name="goodsReceive_id" id="goodsReceive_id" value="${goodsReceiveInstance?.id}" />
        <table>
            <tr style="display:table-row;">
                <td style="width: 130px; display:table-cell; padding:5px;">
                    <label class="control-label" for="input_vendor">
                        Vendor / Company Dealer
                        <span class="required-indicator">*</span>
                    </label>
                </td>
                <td>
                    <input type="radio" name="pilih" value="0" id="pilih0" onclick="getRadio('pilih0')">
                    <g:textField name="vendorData" id="vendorData" onblur="detailVendor()" />
                    <g:hiddenField name="input_vendor" id="input_vendor" />
                    <br><br>
                    <input type="radio" name="pilih" value="1" id="pilih1" onclick="getRadio('pilih1')">
                    <select id="input_companyDealer" name="input_companyDealer" style="width: 200px" required="" >
                        <g:each in="${com.kombos.administrasi.CompanyDealer.createCriteria().list { order("m011NamaWorkshop","asc")}}" status="o" var="vdr">
                                <option value="${vdr.id}">${vdr.m011NamaWorkshop}</option>
                        </g:each>
                    </select>
                </td>
                <td style="width: 130px; display:table-cell; padding:5px;">
                    <label class="control-label" for="input_reffDate">
                        Tanggal Referensi
                        <span class="required-indicator">*</span>
                    </label>
                </td>
                <td>
                    <ba:datePicker id="input_reffDate" name="input_reffDate" precision="day" format="dd-MM-yyyy"  value="${tglSkrg}" required="" />
                </td>
            </tr>
            <tr>
                <td style="width: 130px; display:table-cell; padding:5px;">
                    <label class="control-label" for="input_noReff">
                        No. Referensi
                        <span class="required-indicator">*</span>
                    </label>
                </td>
                <td>
                    <g:hiddenField name="input_id" value="${goodsReceiveInstance?.t167ID}" />
                    <g:textField name="input_noReff" id="input_noReff" maxlength="20" required="" value="" />
                </td>
                <td style="width: 130px; display:table-cell; padding:5px;">
                    <label class="control-label" for="input_rcvDate">
                        Tanggal Receive
                        <span class="required-indicator">*</span>
                    </label>
                </td>
                <td>
                    <ba:datePicker id="input_rcvDate" name="input_rcvDate" precision="day" format="dd-MM-yyyy" value="${tglSkrg}" required="" />
                    <select id="input_rcvHour" name="input_rcvHour" style="width: 60px" required="" >
                        <g:each in="${hourList}" status="o" var="hr">
                                <g:if test="${jamSkrg.equals(hr) || ("0" + jamSkrg).equals(hr)}">
                                    <option selected="selected" value="${hr}">${hr}</option>
                                </g:if>
                                <g:else>
                                    <option value="${hr}">${hr}</option>
                                </g:else>
                        </g:each>
                    </select> H :
                    <select id="input_rcvMinuite" name="input_rcvMinuite" style="width: 60px" required="" >
                        <g:each in="${minuiteList}" status="o" var="mnt">
                            <g:if test="${mntSkrg.equals(mnt) || ("0" + mntSkrg).equals(mnt)}">
                                <option selected="selected" value="${mnt}">${mnt}</option>
                            </g:if>
                            <g:else>
                                <option value="${mnt}">${mnt}</option>
                            </g:else>
                    </g:each>
                    </select>
                </td>
            </tr>
            <tr>
                <td style="width: 130px; display:table-cell; padding:5px;">
                    <label class="control-label" for="input_tipeBayar">
                        Tipe Bayar
                    </label>
                </td>
                <td>
                    %{--<select name="t167TipeBayar" id="input_tipeBayar" >
                        <option value="Kredit">Kredit</option>
                        <option value="Tunai">Tunai</option>
                    </select>--}%
                    <select id="input_tipeBayar" name="input_tipeBayar" style="width: 120px" required="" >
                        <g:each in="${tipeBayarList}" status="o" var="tipeBayar">
                                <g:if test="${goodsReceiveInstance?.t167TipeBayar.equals(tipeBayar)}">
                                    <option selected="selected" value="${tipeBayar}">${tipeBayar}</option>
                                </g:if>
                                <g:else>
                                    <option value="${tipeBayar}">${tipeBayar}</option>
                                </g:else>
                        </g:each>
                    </select>
                </td>
                <td>
                    <label class="control-label" for="input_ppn">
                        PPN
                    </label>
                </td>
                <td>
                    <div class="input-append">
                        <input id="input_ppn" name="t167Ppn" class="input-small" type="text" value="${goodsReceiveInstance?.t167Ppn?goodsReceiveInstance?.t167Ppn:'10'}">
                        <span class="add-on">%</span>
                    </div>

                </td>
            </tr>
            <tr>
                <td style="width: 130px; display:table-cell; padding:5px;">
                </td>
                <td colspan="3">
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="loadRequestAddModal();">
                        Add Parts
                    </a>
                </td>
            </tr>
        </table>
        </fieldset>
        </form>
        </div>
    </div>


    <div class="box">
        <legend style="font-size: small">Parts Receive</legend>

        <div id="goodsReceive_table">
            <g:render template="partsDataTables"/>
        </div>

            <a class="btn cancel" href="javascript:void(0);" onclick="onDeleteParts();">
                <g:message code="default.button.delete.label" default="Delete" />
            </a>

            <button class="btn btn-primary create" onclick="onSaveData();">Save</button>

            <button class="btn cancel" onclick="cancelling();">Cancel</button>
    </div>
</fieldset>

<div id="requestAddModal" class="modal fade">
    <div class="modal-dialog" style="width: 1200px;">
        <div class="modal-content" style="width: 1200px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 550px;">
                <div id="requestAddContent"/>
            </div>
            <!-- dialog buttons -->

        </div>
    </div>
</div>

</g:formRemote>

<div class="span12" id="goodsReceive-form" style="display: none;margin-left: 0;"></div>
</body>
</html>