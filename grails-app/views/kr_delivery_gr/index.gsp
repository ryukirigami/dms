<%@ page import="com.kombos.administrasi.CompanyDealer"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="baseapplayout" />
<g:javascript disappointmentGrition="head">
	$(function(){
		$('#kanan').show();				
		$('#filter_periode2').hide();
        $('#filter_periode3').hide();
        $('#filter_periode').show();
        $('#detail').attr('disabled',false);		
        $('#bulan1').change(function(){
            $('#bulan2').val($('#bulan1').val());
        });
        $('#bulan2').change(function(){
            if(parseInt($('#bulan1').val()) > parseInt($('#bulan2').val())){
                $('#bulan2').val($('#bulan1').val());
            }
        });
        $('#namaReport').change(function(){        
        	if($('#namaReport').val() == "01"){
        		$('#kanan').show();				
				$('#waiting').show();
				$('#sa').show();
				$('#filter_periode').show();
        		$('#filter_periode2').hide();
        		$('#filter_periode3').hide();
        		$('#detail').attr('disabled',false);				
        	} else 
			if($('#namaReport').val() == "02"){
				$('#kanan').show();				
				$('#waiting').show();
				$('#sa').show();
				$('#filter_periode').hide();
        		$('#filter_periode2').show();
        		$('#filter_periode3').hide();	
        		$('#detail').attr('disabled',false);
			} else
			if($('#namaReport').val() == "03"){
				$('#kanan').show();				
				$('#waiting').show();
				$('#sa').show();
				$('#filter_periode').hide();
        		$('#filter_periode2').hide();
        		$('#filter_periode3').show();	
        		$('#detail').attr('disabled',false);
			} else
			if($('#namaReport').val() == "04"){
				$('#kanan').show();				
				$('#waiting').hide();
				$('#sa').hide();
				$('#filter_periode').show();
        		$('#filter_periode2').hide();
        		$('#filter_periode3').hide();	
        		$('#detail').attr('disabled',false);
			} else
			if($('#namaReport').val() == "05"){
				$('#kanan').show();				
				$('#waiting').hide();
				$('#sa').hide();
				$('#filter_periode').hide();
        		$('#filter_periode2').show();
        		$('#filter_periode3').hide();	
        		$('#detail').attr('disabled',true);
			} else 
			if($('#namaReport').val() == "06"){
				$('#kanan').show();				
				$('#waiting').hide();
				$('#sa').hide();
				$('#filter_periode').hide();
        		$('#filter_periode2').hide();
        		$('#filter_periode3').show();
        		$('#detail').attr('disabled',true);
			} else
			if($('#namaReport').val() == "07"){
				$('#kanan').show();				
				$('#waiting').hide();
				$('#sa').show();
				$('#filter_periode').show();
        		$('#filter_periode2').hide();
        		$('#filter_periode3').hide();	
				$('#detail').removeAttr('checked');
        		$('#detail').attr('disabled',false);
			} else
			if($('#namaReport').val() == "08"){
				$('#kanan').show();				
				$('#waiting').hide();
				$('#sa').show();
				$('#filter_periode').hide();
        		$('#filter_periode2').show();
        		$('#filter_periode3').hide();	
				$('#detail').removeAttr('checked');
        		$('#detail').attr('disabled',true);
			}
        });


	    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    });
    var checkin = $('#search_tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
                checkin.hide();
                $('#search_tanggal2')[0].focus();
            }).data('datepicker');

    var checkout = $('#search_tanggal2').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout.hide();
    }).data('datepicker');

    previewData = function(){
        var namaReport = $('#namaReport').val();
        var workshop = $('#workshop').val();
        var search_tanggal = $('#search_tanggal').val();
        var search_tanggal2 = $('#search_tanggal2').val();
        var bulan1 = $('#bulan1').val();
        var bulan2= $('#bulan2').val();
        var tahun = $('#tahun').val();
		var ceklis = "";
		var sa = "";
		var jenisPekerjaan = "";
		var waitingStatus = "";
		var format = $('input[name="formatReport"]:checked').val();
		if(document.getElementById("detail").checked){ceklis = "1"}else{ceklis = "0"}
        if(namaReport == "" || namaReport == null){
            alert('Harap Isi Data Dengan Lengkap');
            return;
        }
		
		$("input[name='jenisPekerjaan']").each(function (){
			if ($(this).attr("checked")) {
				jenisPekerjaan += $(this).val() + ',';
			}			
		});
		jenisPekerjaan = jenisPekerjaan.substr(0, jenisPekerjaan.length - 1);		
		
		$("input[name='sa']").each(function (){
			if ($(this).attr("checked")) {
				sa += $(this).val() + ',';
			}			
		});
		sa = sa.substr(0, sa.length - 1);		
		
		$("input[name='waitingStatus']").each(function (){
			if ($(this).attr("checked")) {
				waitingStatus += $(this).val() + ',';
			}			
		});
		waitingStatus = waitingStatus.substr(0, waitingStatus.length - 1);	
		window.location = "${request.contextPath}/kr_delivery_gr/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&jenisPekerjaan="+jenisPekerjaan+"&waitingStatus="+waitingStatus+"&sa="+sa+"&detail="+ceklis+"&format="+format;
    }


</g:javascript>

</head>
<body>
	<div class="navbar box-header no-border">Report – Delivery (GR)
	</div>
	<br>
	<div class="box">
		<div class="span12" id="appointmentGr-table">
			<div class="row-fluid form-horizontal">
				<div id="kiri" class="span6">
                    <div class="control-group">
                        <label class="control-label" for="filter_periode" style="text-align: left;">
                            Format Report
                        </label>
                        <div id="filter_format" class="controls">
                            <g:radioGroup name="formatReport" id="formatReport" values="['x','p']" labels="['Excel','PDF']" value="x" required="" >
                                ${it.radio} ${it.label}
                            </g:radioGroup>
                        </div>
                    </div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Workshop </label>
						<div id="filter_wo" class="controls">
                            <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                <g:select name="workshop" id="workshop" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" onchange="reloadSaTable();" />
                            </g:if>
                            <g:else>
                                <g:select name="workshop" id="workshop" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                            </g:else>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Kategori Workshop </label>
						<div id="filter_workshop" class="controls">
							&nbsp; <b>General Repair</b>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Nama Report </label>
						<div id="filter_nama" class="controls">
							<select name="namaReport" id="namaReport" size="8"
								style="width: 100%; height: 150px; font-size: 12px;">
								<option value="01">01. Delivery GR - Delivery Lead Time (Daily)</option>				
								<option value="02">02. Delivery GR - Delivery Lead Time (Monthly)</option>
								<option value="03">03. Delivery GR - Delivery Lead Time (Yearly)</option>
								<option value="04">04. Delivery GR - SA Delivery (Daily)</option>
								<option value="05">05. Delivery GR - SA Delivery (Monthly)</option>
								<option value="06">06. Delivery GR - SA Delivery (Yearly)</option>
								<option value="07">07. Delivery GR - OTD Rate (Daily)</option>
								<option value="08">08. Delivery GR - OTD Rate (Monthly)</option>								
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Periode </label>
						<div id="filter_periode" class="controls">
							<ba:datePicker id="search_tanggal" name="search_tanggal"
								precision="day" format="dd-MM-yyyy" value="${new Date()}" />
							&nbsp;&nbsp;s.d.&nbsp;&nbsp;
							<ba:datePicker id="search_tanggal2" name="search_tanggal2"
								precision="day" format="dd-MM-yyyy" value="${new Date()+1}" />
						</div>
						<div id="filter_periode2" class="controls">
							%{ def listBulan =
							['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
							}% <select name="bulan1" id="bulan1" style="width: 170px">
								%{ for(int i=0;i<12;i++){ out.print('
								<option value="'+(i+1)+'">'+listBulan.get(i)+'</option>'); } }%
							</select> &nbsp; - &nbsp; <select name="bulan2" id="bulan2"
								style="width: 170px"> %{ for(int i=0;i<12;i++){
								out.print('
								<option value="'+(i+1)+'">'+listBulan.get(i)+'</option>'); } }%
							</select> &nbsp; <select name="tahun" id="tahun" style="width: 106px">
								%{ for(def a = (new Date().format("yyyy")).toInteger();a >= 2000
								;a--){ out.print('
								<option value="'+a+'">'+a+'</option>'); } }%

							</select>
						</div>
						<div id="filter_periode3" class="controls">
							<select name="tahun" id="tahun" style="width: 170px"> %{
								for(def a = (new Date().format("yyyy")).toInteger(); a >= 2015;
								a--) { out.print('
								<option value="'+a+'">'+a+'</option>'); } }%
							</select>
						</div>
						<div class="control-group" id="reportDetail">
							<label class="control-label"
								style="text-align: left;"> Report Detail </label>
							<div id="filter_detail" class="controls">
								<input type="checkbox" name="detail" id="detail" value="1" /> Ya
							</div>
						</div>						
					</div>					
				</div>
				<div id="kanan" class="span6">										
					<div class="control-group" id="waiting">	
						<label class="control-label" style="text-align:left;">
						Waiting Status
						</label>
						<div class="controls">
							<g:render template="tableWaiting"></g:render>
						</div>
					</div>
					<div class="control-group" id="sa">
						<label class="control-label" for="workshop"
							style="text-align: left;"> Service Advisor </label>
						<div class="controls">
							<g:render template="tableSA" />
						</div>						
					</div>
					<div class="control-group">	
						<label class="control-label" style="text-align:left;">
						Jenis Pekerjaan
						</label>
						<div class="controls">
							<g:render template="tableJenisPekerjaan"></g:render>
						</div>
					</div>
				</div>				
			</div>
			<g:field type="button" onclick="previewData()"
				class="btn btn-primary create" name="preview"
				value="${message(code: 'default.button.upload.label', default: 'Preview')}" />
			<g:field type="button" onclick="window.location.replace('#/home')"
				class="btn btn-cancel cancel" name="cancel"
				value="${message(code: 'default.button.upload.label', default: 'Close')}" />
		</div>
	</div>
</body>
</html>
