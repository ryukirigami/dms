<table cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <tr>
    	<td>
    		<input type="checkbox" id="selectwaiting" onclick="selectAll(this);"/> <b>Waiting Status</b>
    	</td>
    </tr>
    <tr>
    	<td>
    		<input type="checkbox" name="waitingStatus" value="yes"/>&nbsp;YES
    	</td>
    </tr>
    <tr>
    	<td>
    		<input type="checkbox" name="waitingStatus" value="no"/>&nbsp;NO
    	</td>
    </tr>
</table>
<g:javascript>
function selectAll(e){
	if(e.checked){
		$("input[name='waitingStatus']").each(function (){
			$(this).attr("checked", "checked");
		});
	} else {
		$("input[name='waitingStatus']").each(function (){
			$(this).removeAttr("checked");
		});
	}	
}
</g:javascript>