<%@ page import="com.kombos.administrasi.MappingStallTipeBerat" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'mappingStallTipeBerat.label', default: 'Mapping Stall - Tipe Berat')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-mappingStallTipeBerat" class="content scaffold-edit" role="main">
			<legend><g:message code="default.edit.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

			%{--<g:form method="post" >--}%
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="reloadMappingStallTipeBeratTable();" update="mappingStallTipeBerat-form"
				url="[controller: 'mappingStallTipeBerat', action:'update']">
				<g:hiddenField name="id" value="${mappingStallTipeBeratInstance?.id}" />
				<g:hiddenField name="version" value="${mappingStallTipeBeratInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
                <fieldset class="buttons controls">
                    <g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}"
                                    onclick="return confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}');"/>
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                </fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
