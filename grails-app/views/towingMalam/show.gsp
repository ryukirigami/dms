

<%@ page import="com.kombos.reception.TowingMalam" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'towingMalam.label', default: 'TowingMalam')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteTowingMalam;

$(function(){ 
	deleteTowingMalam=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/towingMalam/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadTowingMalamTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-towingMalam" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="towingMalam"
			class="table table-bordered table-hover">
			<tbody>

				
				%{--<g:if test="${towingMalamInstance?.t421ID}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="t421ID-label" class="property-label"><g:message--}%
					%{--code="towingMalam.t421ID.label" default="ID" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="t421ID-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${towingMalamInstance}" field="t421ID"--}%
								%{--url="${request.contextPath}/TowingMalam/updatefield" type="text"--}%
								%{--title="Enter t421ID" onsuccess="reloadTowingMalamTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${towingMalamInstance}" field="t421ID"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${towingMalamInstance?.t421TglJamDatang}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t421TglJamDatang-label" class="property-label"><g:message
					code="towingMalam.t421TglJamDatang.label" default="Tgl Jam Datang" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t421TglJamDatang-label">
						%{--<ba:editableValue
								bean="${towingMalamInstance}" field="t421TglJamDatang"
								url="${request.contextPath}/TowingMalam/updatefield" type="text"
								title="Enter t421TglJamDatang" onsuccess="reloadTowingMalamTable();" />--}%
							
								<g:formatDate format="dd/MM/yyyy HH:mm" date="${towingMalamInstance?.t421TglJamDatang}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${towingMalamInstance?.customerVehicle}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="customerVehicle-label" class="property-label"><g:message
					code="towingMalam.customerVehicle.label" default="Customer Vehicle" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="customerVehicle-label">
						%{--<ba:editableValue
								bean="${towingMalamInstance}" field="customerVehicle"
								url="${request.contextPath}/TowingMalam/updatefield" type="text"
								title="Enter customerVehicle" onsuccess="reloadTowingMalamTable();" />--}%
							
								<g:link controller="customerVehicle" action="show" id="${towingMalamInstance?.customerVehicle?.id}">${towingMalamInstance?.customerVehicle?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${towingMalamInstance?.t421Mobil}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t421Mobil-label" class="property-label"><g:message
					code="towingMalam.t421Mobil.label" default="Mobil" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t421Mobil-label">
						%{--<ba:editableValue
								bean="${towingMalamInstance}" field="t421Mobil"
								url="${request.contextPath}/TowingMalam/updatefield" type="text"
								title="Enter t421Mobil" onsuccess="reloadTowingMalamTable();" />--}%
							
								<g:fieldValue bean="${towingMalamInstance}" field="t421Mobil"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${towingMalamInstance?.t421Tahun}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t421Tahun-label" class="property-label"><g:message
					code="towingMalam.t421Tahun.label" default="Tahun" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t421Tahun-label">
						%{--<ba:editableValue
								bean="${towingMalamInstance}" field="t421Tahun"
								url="${request.contextPath}/TowingMalam/updatefield" type="text"
								title="Enter t421Tahun" onsuccess="reloadTowingMalamTable();" />--}%
							
								<g:fieldValue bean="${towingMalamInstance}" field="t421Tahun"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${towingMalamInstance?.t421Warna}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t421Warna-label" class="property-label"><g:message
					code="towingMalam.t421Warna.label" default="Warna" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t421Warna-label">
						%{--<ba:editableValue
								bean="${towingMalamInstance}" field="t421Warna"
								url="${request.contextPath}/TowingMalam/updatefield" type="text"
								title="Enter t421Warna" onsuccess="reloadTowingMalamTable();" />--}%
							
								<g:fieldValue bean="${towingMalamInstance}" field="t421Warna"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${towingMalamInstance?.t421NamaPemilik}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t421NamaPemilik-label" class="property-label"><g:message
					code="towingMalam.t421NamaPemilik.label" default="Nama Pemilik" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t421NamaPemilik-label">
						%{--<ba:editableValue
								bean="${towingMalamInstance}" field="t421NamaPemilik"
								url="${request.contextPath}/TowingMalam/updatefield" type="text"
								title="Enter t421NamaPemilik" onsuccess="reloadTowingMalamTable();" />--}%
							
								<g:fieldValue bean="${towingMalamInstance}" field="t421NamaPemilik"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${towingMalamInstance?.t421NoTelp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t421NoTelp-label" class="property-label"><g:message
					code="towingMalam.t421NoTelp.label" default="No Telp" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t421NoTelp-label">
						%{--<ba:editableValue
								bean="${towingMalamInstance}" field="t421NoTelp"
								url="${request.contextPath}/TowingMalam/updatefield" type="text"
								title="Enter t421NoTelp" onsuccess="reloadTowingMalamTable();" />--}%
							
								<g:fieldValue bean="${towingMalamInstance}" field="t421NoTelp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${towingMalamInstance?.t421NamaPenerima}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t421NamaPenerima-label" class="property-label"><g:message
					code="towingMalam.t421NamaPenerima.label" default="Nama Penerima" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t421NamaPenerima-label">
						%{--<ba:editableValue
								bean="${towingMalamInstance}" field="t421NamaPenerima"
								url="${request.contextPath}/TowingMalam/updatefield" type="text"
								title="Enter t421NamaPenerima" onsuccess="reloadTowingMalamTable();" />--}%
							
								<g:fieldValue bean="${towingMalamInstance}" field="t421NamaPenerima"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${towingMalamInstance?.t421StaMetodeFU}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t421StaMetodeFU-label" class="property-label"><g:message
					code="towingMalam.t421StaMetodeFU.label" default="Metode Follow Up" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t421StaMetodeFU-label">
						%{--<ba:editableValue
								bean="${towingMalamInstance}" field="t421StaMetodeFU"
								url="${request.contextPath}/TowingMalam/updatefield" type="text"
								title="Enter t421StaMetodeFU" onsuccess="reloadTowingMalamTable();" />--}%
							
								%{--<g:fieldValue bean="${towingMalamInstance}" field="t421StaMetodeFU"/>--}%
                                %{
                                    out.println(towingMalamInstance.t421StaMetodeFU==null?"":(towingMalamInstance.t421StaMetodeFU=='T'?"Telp":"E-mail"))
                                }%
                        </span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${towingMalamInstance?.t421StaFU}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t421StaFU-label" class="property-label"><g:message
					code="towingMalam.t421StaFU.label" default="Status Follow Up" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t421StaFU-label">
						%{--<ba:editableValue
								bean="${towingMalamInstance}" field="t421StaFU"
								url="${request.contextPath}/TowingMalam/updatefield" type="text"
								title="Enter t421StaFU" onsuccess="reloadTowingMalamTable();" />--}%
							
								%{--<g:fieldValue bean="${towingMalamInstance}" field="t421StaFU"/>--}%
							    %{
							        out.println(towingMalamInstance.t421StaFU==null?"":(towingMalamInstance.t421StaFU=='1'?"Berhasil":"Tidak Berhasil"))
							    }%
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${towingMalamInstance?.t421TglJamFU}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t421TglJamFU-label" class="property-label"><g:message
					code="towingMalam.t421TglJamFU.label" default="Tanggal Follow Up" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t421TglJamFU-label">
						%{--<ba:editableValue
								bean="${towingMalamInstance}" field="t421TglJamFU"
								url="${request.contextPath}/TowingMalam/updatefield" type="text"
								title="Enter t421TglJamFU" onsuccess="reloadTowingMalamTable();" />--}%
							
								<g:formatDate date="${towingMalamInstance?.t421TglJamFU}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${towingMalamInstance?.t421NamaFU}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t421NamaFU-label" class="property-label"><g:message
					code="towingMalam.t421NamaFU.label" default="Nama User Follow Up" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t421NamaFU-label">
						%{--<ba:editableValue
								bean="${towingMalamInstance}" field="t421NamaFU"
								url="${request.contextPath}/TowingMalam/updatefield" type="text"
								title="Enter t421NamaFU" onsuccess="reloadTowingMalamTable();" />--}%
							
								<g:fieldValue bean="${towingMalamInstance}" field="t421NamaFU"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${towingMalamInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="towingMalam.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${towingMalamInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/TowingMalam/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadTowingMalamTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${towingMalamInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${towingMalamInstance?.createdBy}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="createdBy-label" class="property-label"><g:message--}%
					%{--code="towingMalam.createdBy.label" default="Created By" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="createdBy-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${towingMalamInstance}" field="createdBy"--}%
								%{--url="${request.contextPath}/TowingMalam/updatefield" type="text"--}%
								%{--title="Enter createdBy" onsuccess="reloadTowingMalamTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${towingMalamInstance}" field="createdBy"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${towingMalamInstance?.updatedBy}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="updatedBy-label" class="property-label"><g:message--}%
					%{--code="towingMalam.updatedBy.label" default="Updated By" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="updatedBy-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${towingMalamInstance}" field="updatedBy"--}%
								%{--url="${request.contextPath}/TowingMalam/updatefield" type="text"--}%
								%{--title="Enter updatedBy" onsuccess="reloadTowingMalamTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${towingMalamInstance}" field="updatedBy"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${towingMalamInstance?.lastUpdProcess}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="lastUpdProcess-label" class="property-label"><g:message--}%
					%{--code="towingMalam.lastUpdProcess.label" default="Last Upd Process" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="lastUpdProcess-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${towingMalamInstance}" field="lastUpdProcess"--}%
								%{--url="${request.contextPath}/TowingMalam/updatefield" type="text"--}%
								%{--title="Enter lastUpdProcess" onsuccess="reloadTowingMalamTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${towingMalamInstance}" field="lastUpdProcess"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${towingMalamInstance?.dateCreated}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="dateCreated-label" class="property-label"><g:message--}%
					%{--code="towingMalam.dateCreated.label" default="Date Created" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="dateCreated-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${towingMalamInstance}" field="dateCreated"--}%
								%{--url="${request.contextPath}/TowingMalam/updatefield" type="text"--}%
								%{--title="Enter dateCreated" onsuccess="reloadTowingMalamTable();" />--}%
							%{----}%
								%{--<g:formatDate date="${towingMalamInstance?.dateCreated}" />--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${towingMalamInstance?.lastUpdated}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="lastUpdated-label" class="property-label"><g:message--}%
					%{--code="towingMalam.lastUpdated.label" default="Last Updated" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="lastUpdated-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${towingMalamInstance}" field="lastUpdated"--}%
								%{--url="${request.contextPath}/TowingMalam/updatefield" type="text"--}%
								%{--title="Enter lastUpdated" onsuccess="reloadTowingMalamTable();" />--}%
							%{----}%
								%{--<g:formatDate date="${towingMalamInstance?.lastUpdated}" />--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${towingMalamInstance?.id}"
					update="[success:'towingMalam-form',failure:'towingMalam-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				%{--<ba:confirm id="delete" class="btn cancel"--}%
					%{--message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"--}%
					%{--onsuccess="deleteTowingMalam('${towingMalamInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>--}%
			</fieldset>
		</g:form>
	</div>
</body>
</html>
