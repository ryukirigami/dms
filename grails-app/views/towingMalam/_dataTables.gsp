
<%@ page import="com.kombos.reception.TowingMalam" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="towingMalam_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="towingMalam.t421ID.label" default="ID" /></div>--}%
			%{--</th>--}%


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="towingMalam.t421TglJamDatang.label" default="Tgl Jam Datang" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="towingMalam.customerVehicle.label" default="Customer Vehicle" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="towingMalam.t421Mobil.label" default="Mobil" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="towingMalam.t421Tahun.label" default="Tahun" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="towingMalam.t421Warna.label" default="Warna" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="towingMalam.t421NamaPemilik.label" default="Nama Pemilik" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="towingMalam.t421NoTelp.label" default="No Telp" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="towingMalam.t421NamaPenerima.label" default="Nama Penerima" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="towingMalam.t421StaMetodeFU.label" default="Metode Follow Up" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="towingMalam.t421StaFU.label" default="Status Follow Up" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="towingMalam.t421TglJamFU.label" default="Tanggal Follow Up" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="towingMalam.t421NamaFU.label" default="Nama User Follow Up" /></div>
			</th>


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="towingMalam.staDel.label" default="Sta Del" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="towingMalam.createdBy.label" default="Created By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="towingMalam.updatedBy.label" default="Updated By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="towingMalam.lastUpdProcess.label" default="Last Upd Process" /></div>--}%
			%{--</th>--}%

		
		</tr>
		<tr>
		
	
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_t421ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_t421ID" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t421TglJamDatang" style="padding-top: 0px;position:relative; margin-top: 0px;width: 300px;" >
					<input type="hidden" name="search_t421TglJamDatang" value="date.struct">
					<input type="hidden" name="search_t421TglJamDatang_day" id="search_t421TglJamDatang_day" value="">
					<input type="hidden" name="search_t421TglJamDatang_month" id="search_t421TglJamDatang_month" value="">
					<input type="hidden" name="search_t421TglJamDatang_year" id="search_t421TglJamDatang_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t421TglJamDatang_dp" value="" id="search_t421TglJamDatang" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_customerVehicle" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_customerVehicle" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t421Mobil" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t421Mobil" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t421Tahun" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t421Tahun" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t421Warna" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t421Warna" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t421NamaPemilik" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t421NamaPemilik" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t421NoTelp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t421NoTelp" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t421NamaPenerima" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t421NamaPenerima" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t421StaMetodeFU" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t421StaMetodeFU" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t421StaFU" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t421StaFU" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t421TglJamFU" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t421TglJamFU" value="date.struct">
					<input type="hidden" name="search_t421TglJamFU_day" id="search_t421TglJamFU_day" value="">
					<input type="hidden" name="search_t421TglJamFU_month" id="search_t421TglJamFU_month" value="">
					<input type="hidden" name="search_t421TglJamFU_year" id="search_t421TglJamFU_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t421TglJamFU_dp" value="" id="search_t421TglJamFU" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t421NamaFU" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t421NamaFU" class="search_init" />
				</div>
			</th>
	
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_staDel" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_createdBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_updatedBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_lastUpdProcess" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%


		</tr>
	</thead>
</table>

<g:javascript>
var towingMalamTable;
var reloadTowingMalamTable;
$(function(){
	
	reloadTowingMalamTable = function() {
		towingMalamTable.fnDraw();
	}

	
	$('#search_t421TglJamDatang').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t421TglJamDatang_day').val(newDate.getDate());
			$('#search_t421TglJamDatang_month').val(newDate.getMonth()+1);
			$('#search_t421TglJamDatang_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			towingMalamTable.fnDraw();	
	});

	

	$('#search_t421TglJamFU').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t421TglJamFU_day').val(newDate.getDate());
			$('#search_t421TglJamFU_month').val(newDate.getMonth()+1);
			$('#search_t421TglJamFU_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			towingMalamTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	towingMalamTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	towingMalamTable = $('#towingMalam_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

//{
//	"sName": "t421ID",
//	"mDataProp": "t421ID",
//	"aTargets": [0],
//	"mRender": function ( data, type, row ) {
//		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
//	},
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"300px",
//	"bVisible": true
//}
//
//,

{
	"sName": "t421TglJamDatang",
	"mDataProp": "t421TglJamDatang",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;Follow Up&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "customerVehicle",
	"mDataProp": "customerVehicle",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t421Mobil",
	"mDataProp": "t421Mobil",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t421Tahun",
	"mDataProp": "t421Tahun",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t421Warna",
	"mDataProp": "t421Warna",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t421NamaPemilik",
	"mDataProp": "t421NamaPemilik",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t421NoTelp",
	"mDataProp": "t421NoTelp",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t421NamaPenerima",
	"mDataProp": "t421NamaPenerima",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t421StaMetodeFU",
	"mDataProp": "t421StaMetodeFU",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t421StaFU",
	"mDataProp": "t421StaFU",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t421TglJamFU",
	"mDataProp": "t421TglJamFU",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t421NamaFU",
	"mDataProp": "t421NamaFU",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
//
//,
//
//{
//	"sName": "staDel",
//	"mDataProp": "staDel",
//	"aTargets": [13],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "createdBy",
//	"mDataProp": "createdBy",
//	"aTargets": [14],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "updatedBy",
//	"mDataProp": "updatedBy",
//	"aTargets": [15],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "lastUpdProcess",
//	"mDataProp": "lastUpdProcess",
//	"aTargets": [16],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var t421ID = $('#filter_t421ID input').val();
						if(t421ID){
							aoData.push(
									{"name": 'sCriteria_t421ID', "value": t421ID}
							);
						}

						var t421TglJamDatang = $('#search_t421TglJamDatang').val();
						var t421TglJamDatangDay = $('#search_t421TglJamDatang_day').val();
						var t421TglJamDatangMonth = $('#search_t421TglJamDatang_month').val();
						var t421TglJamDatangYear = $('#search_t421TglJamDatang_year').val();
						
						if(t421TglJamDatang){
							aoData.push(
									{"name": 'sCriteria_t421TglJamDatang', "value": "date.struct"},
									{"name": 'sCriteria_t421TglJamDatang_dp', "value": t421TglJamDatang},
									{"name": 'sCriteria_t421TglJamDatang_day', "value": t421TglJamDatangDay},
									{"name": 'sCriteria_t421TglJamDatang_month', "value": t421TglJamDatangMonth},
									{"name": 'sCriteria_t421TglJamDatang_year', "value": t421TglJamDatangYear}
							);
						}
	
						var customerVehicle = $('#filter_customerVehicle input').val();
						if(customerVehicle){
							aoData.push(
									{"name": 'sCriteria_customerVehicle', "value": customerVehicle}
							);
						}
	
						var t421Mobil = $('#filter_t421Mobil input').val();
						if(t421Mobil){
							aoData.push(
									{"name": 'sCriteria_t421Mobil', "value": t421Mobil}
							);
						}
	
						var t421Tahun = $('#filter_t421Tahun input').val();
						if(t421Tahun){
							aoData.push(
									{"name": 'sCriteria_t421Tahun', "value": t421Tahun}
							);
						}
	
						var t421Warna = $('#filter_t421Warna input').val();
						if(t421Warna){
							aoData.push(
									{"name": 'sCriteria_t421Warna', "value": t421Warna}
							);
						}
	
						var t421NamaPemilik = $('#filter_t421NamaPemilik input').val();
						if(t421NamaPemilik){
							aoData.push(
									{"name": 'sCriteria_t421NamaPemilik', "value": t421NamaPemilik}
							);
						}
	
						var t421NoTelp = $('#filter_t421NoTelp input').val();
						if(t421NoTelp){
							aoData.push(
									{"name": 'sCriteria_t421NoTelp', "value": t421NoTelp}
							);
						}
	
						var t421NamaPenerima = $('#filter_t421NamaPenerima input').val();
						if(t421NamaPenerima){
							aoData.push(
									{"name": 'sCriteria_t421NamaPenerima', "value": t421NamaPenerima}
							);
						}
	
						var t421StaMetodeFU = $('#filter_t421StaMetodeFU input').val();
						if(t421StaMetodeFU){
							aoData.push(
									{"name": 'sCriteria_t421StaMetodeFU', "value": t421StaMetodeFU}
							);
						}
	
						var t421StaFU = $('#filter_t421StaFU input').val();
						if(t421StaFU){
							aoData.push(
									{"name": 'sCriteria_t421StaFU', "value": t421StaFU}
							);
						}

						var t421TglJamFU = $('#search_t421TglJamFU').val();
						var t421TglJamFUDay = $('#search_t421TglJamFU_day').val();
						var t421TglJamFUMonth = $('#search_t421TglJamFU_month').val();
						var t421TglJamFUYear = $('#search_t421TglJamFU_year').val();
						
						if(t421TglJamFU){
							aoData.push(
									{"name": 'sCriteria_t421TglJamFU', "value": "date.struct"},
									{"name": 'sCriteria_t421TglJamFU_dp', "value": t421TglJamFU},
									{"name": 'sCriteria_t421TglJamFU_day', "value": t421TglJamFUDay},
									{"name": 'sCriteria_t421TglJamFU_month', "value": t421TglJamFUMonth},
									{"name": 'sCriteria_t421TglJamFU_year', "value": t421TglJamFUYear}
							);
						}
	
						var t421NamaFU = $('#filter_t421NamaFU input').val();
						if(t421NamaFU){
							aoData.push(
									{"name": 'sCriteria_t421NamaFU', "value": t421NamaFU}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
