<%@ page import="com.kombos.reception.TowingMalam" %>

<g:javascript>
    $('#findDataCustomer').click(function(){
       if($('#t183NoPolTengah').val()=="" || $('#t183NoPolTengah').val()==null
               || $('#t183NoPolBelakang').val()=="" || $('#t183NoPolBelakang').val()==null){
           alert('Data No Polisi Masih Ada yang Kosong');
           return false
       }
       var kodeKota = $('#noPol').val();
       var noTengah = $('#t183NoPolTengah').val();
       var noBelakang = $('#t183NoPolBelakang').val();
       setNoPol(kodeKota,noTengah,noBelakang);

    });

    function setNoPol(kodeKota,noTengah,noBelakang){
        $.ajax({
    		url:'${request.contextPath}/towingMalam/findNoPol',
    		type: "POST", // Always use POST when deleting data
    		data: {kode : kodeKota, tengah : noTengah, belakang : noBelakang},
    		dataType: 'json',
    		success: function(data,textStatus,xhr){
    		    if(data){
                    $('#t421NamaPemilik').attr('value',data.namaDepan);
                    $('#t421Mobil').attr('value',data.fullModelCodeBaseModel);
//                    $('#fullModelCodeBaseModel').attr('value',data[0].fullModelCodeBaseModel);
//                    $('#fullModelCodeGear').attr('value',data[0].fullModelCodeGear);
                    $('#t421Warna').attr('value',data.warna);
                    $('#t421Tahun').attr('value',data.t183ThnBlnRakit);
                    $('#t421NamaPemilik').attr('value',data.namaDepan);
    		    }else{
                    $('#t421NamaPemilik').attr('value',data.namaDepan);
                    $('#t421Mobil').attr('value',data.fullModelCodeBaseModel);
//                    $('#fullModelCodeBaseModel').attr('value',data[0].fullModelCodeBaseModel);
//                    $('#fullModelCodeGear').attr('value',data[0].fullModelCodeGear);
                    $('#t421Warna').attr('value',data.warna);
                    $('#t421Tahun').attr('value',data.t183ThnBlnRakit);
    		        $('#t421NamaPemilik').attr('value',data.namaDepan);
    		    }
    		},
    		error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(data,textStatus){
   			}
		});
    }
</g:javascript>

%{
    if(request.getRequestURI().contains("edit") || request.getRequestURI().contains("update")){          //follow up
}%
<g:if test="${towingMalamInstance?.t421TglJamDatang}">
<table id="towingMalam"
            class="table table-bordered table-hover">
            <tbody>
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t421TglJamDatang-label" class="property-label"><g:message
                            code="towingMalam.t421TglJamDatang.label" default="Tgl Jam Datang" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="t421TglJamDatang-label">
                    %{--<ba:editableValue
                            bean="${towingMalamInstance}" field="t421TglJamDatang"
                            url="${request.contextPath}/TowingMalam/updatefield" type="text"
                            title="Enter t421TglJamDatang" onsuccess="reloadTowingMalamTable();" />--}%

                    <g:formatDate format="dd/MM/yyyy HH:mm" date="${towingMalamInstance?.t421TglJamDatang}" />

                </span></td>

            </tr>
        </g:if>

        <g:if test="${towingMalamInstance?.customerVehicle}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="customerVehicle-label" class="property-label"><g:message
                            code="towingMalam.customerVehicle.label" default="Customer Vehicle" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="customerVehicle-label">
                    %{--<ba:editableValue
                            bean="${towingMalamInstance}" field="customerVehicle"
                            url="${request.contextPath}/TowingMalam/updatefield" type="text"
                            title="Enter customerVehicle" onsuccess="reloadTowingMalamTable();" />--}%

                    <g:link controller="customerVehicle" action="show" id="${towingMalamInstance?.customerVehicle?.id}">${towingMalamInstance?.customerVehicle?.encodeAsHTML()}</g:link>

                </span></td>

            </tr>
        </g:if>

        <g:if test="${towingMalamInstance?.t421Mobil}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="t421Mobil-label" class="property-label"><g:message
                            code="towingMalam.t421Mobil.label" default="Mobil" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="t421Mobil-label">
                    %{--<ba:editableValue
                            bean="${towingMalamInstance}" field="t421Mobil"
                            url="${request.contextPath}/TowingMalam/updatefield" type="text"
                            title="Enter t421Mobil" onsuccess="reloadTowingMalamTable();" />--}%

                    <g:fieldValue bean="${towingMalamInstance}" field="t421Mobil"/>

                </span></td>

            </tr>
        </g:if>

        <g:if test="${towingMalamInstance?.t421Tahun}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="t421Tahun-label" class="property-label"><g:message
                            code="towingMalam.t421Tahun.label" default="Tahun" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="t421Tahun-label">
                    %{--<ba:editableValue
                            bean="${towingMalamInstance}" field="t421Tahun"
                            url="${request.contextPath}/TowingMalam/updatefield" type="text"
                            title="Enter t421Tahun" onsuccess="reloadTowingMalamTable();" />--}%

                    <g:fieldValue bean="${towingMalamInstance}" field="t421Tahun"/>

                </span></td>

            </tr>
        </g:if>

        <g:if test="${towingMalamInstance?.t421Warna}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="t421Warna-label" class="property-label"><g:message
                            code="towingMalam.t421Warna.label" default="Warna" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="t421Warna-label">
                    %{--<ba:editableValue
                            bean="${towingMalamInstance}" field="t421Warna"
                            url="${request.contextPath}/TowingMalam/updatefield" type="text"
                            title="Enter t421Warna" onsuccess="reloadTowingMalamTable();" />--}%

                    <g:fieldValue bean="${towingMalamInstance}" field="t421Warna"/>

                </span></td>

            </tr>
        </g:if>

        <g:if test="${towingMalamInstance?.t421NamaPemilik}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="t421NamaPemilik-label" class="property-label"><g:message
                            code="towingMalam.t421NamaPemilik.label" default="Nama Pemilik" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="t421NamaPemilik-label">
                    %{--<ba:editableValue
                            bean="${towingMalamInstance}" field="t421NamaPemilik"
                            url="${request.contextPath}/TowingMalam/updatefield" type="text"
                            title="Enter t421NamaPemilik" onsuccess="reloadTowingMalamTable();" />--}%

                    <g:fieldValue bean="${towingMalamInstance}" field="t421NamaPemilik"/>

                </span></td>

            </tr>
        </g:if>

        <g:if test="${towingMalamInstance?.t421NoTelp}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="t421NoTelp-label" class="property-label"><g:message
                            code="towingMalam.t421NoTelp.label" default="No Telp" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="t421NoTelp-label">
                    %{--<ba:editableValue
                            bean="${towingMalamInstance}" field="t421NoTelp"
                            url="${request.contextPath}/TowingMalam/updatefield" type="text"
                            title="Enter t421NoTelp" onsuccess="reloadTowingMalamTable();" />--}%

                    <g:fieldValue bean="${towingMalamInstance}" field="t421NoTelp"/>

                </span></td>

            </tr>
        </g:if>
    </tbody>
</table>

<div class="control-group fieldcontain ${hasErrors(bean: towingMalamInstance, field: 't421StaMetodeFU', 'error')} required">
    <label class="control-label" for="t421StaMetodeFU">
        <g:message code="towingMalam.t421StaMetodeFU.label" default="Metode Follow Up" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select from="${['Telp', 'E-mail']}" keys="${['T','E']}" name="t421StaMetodeFU" maxlength="1" required="" value="${towingMalamInstance?.t421StaMetodeFU}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: towingMalamInstance, field: 't421StaFU', 'error')} required">
    <label class="control-label" for="t421StaFU">
        <g:message code="towingMalam.t421StaFU.label" default="Status Follow Up" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select from="${['Berhasil', 'Tidak Berhasil']}" keys="${['1','0']}" name="t421StaFU" maxlength="1" required="" value="${towingMalamInstance?.t421StaFU}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: towingMalamInstance, field: 't421TglJamFU', 'error')} required">
    <label class="control-label" for="t421TglJamFU">
        <g:message code="towingMalam.t421TglJamFU.label" default="Tanggal Follow Up" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <ba:datePicker name="t421TglJamFU" precision="day" format="dd/MM/yyyy" value="${towingMalamInstance?.t421TglJamFU}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: towingMalamInstance, field: 't421NamaFU', 'error')} required">
    <label class="control-label" for="t421NamaFU">
        <g:message code="towingMalam.t421NamaFU.label" default="Nama User" />
    </label>
    <div class="controls">
        %{
        out.print( org.apache.shiro.SecurityUtils.subject.principal.toString())
        }%
        <input type="hidden" name="t421NamaFU" value="${org.apache.shiro.SecurityUtils.subject.principal.toString()}" />
    </div>
</div>

%{
    }else{
}%

<div style="margin-left: 2px" class="control-group fieldcontain ${hasErrors(bean: towingMalamInstance, field: 't921StaMediaKeluhan', 'error')} required">
    <label class="control-label">
        <g:message code="complaint.t921StaMediaKeluhan.label" default="No Polisi" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select style="width:70px" id="noPol" name="noPol.id" from="${com.kombos.administrasi.KodeKotaNoPol.createCriteria().list(){eq("staDel","0");order("m116ID")}}" optionValue="${{it.m116ID}}" optionKey="id" required="" value="${(!request.getRequestURI().contains("edit"))?"":towingMalamInstance?.customerVehicle.currentCondition.kodeKotaNoPol}" class="many-to-one"/>
        &nbsp;
        <g:textField style="width: 95px" maxlength="5" onkeypress="return isNumberKey(event);" name="t183NoPolTengah" id="t183NoPolTengah" required="" value="${(!request.getRequestURI().contains("edit"))?"":towingMalamInstance?.customerVehicle.currentCondition.t183NoPolTengah}"/>
        &nbsp;
        <g:textField style="width: 50px" maxlength="3" name="t183NoPolBelakang" id="t183NoPolBelakang" required="" value="${(!request.getRequestURI().contains("edit"))?"":towingMalamInstance?.customerVehicle.currentCondition.t183NoPolBelakang}"/>
        &nbsp;
        <g:field type="button" name="findDataCustomer" id="findDataCustomer" class="btn cancel" value="${message(code: 'default.button.complaint.dataCustomer.label', default: 'Data Customer')}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: towingMalamInstance, field: 't421Mobil', 'error')} required">
    <label class="control-label" for="t421Mobil">
        <g:message code="towingMalam.t421Mobil.label" default="Mobil" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t421Mobil" id="t421Mobil" maxlength="50" required="" value="${towingMalamInstance?.t421Mobil}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: towingMalamInstance, field: 't421Tahun', 'error')} required">
    <label class="control-label" for="t421Tahun">
        <g:message code="towingMalam.t421Tahun.label" default="Tahun" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t421Tahun" id="t421Tahun" maxlength="4" required="" value="${towingMalamInstance?.t421Tahun}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: towingMalamInstance, field: 't421Warna', 'error')} required">
    <label class="control-label" for="t421Warna">
        <g:message code="towingMalam.t421Warna.label" default="Warna" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t421Warna" id="t421Warna" maxlength="50" required="" value="${towingMalamInstance?.t421Warna}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: towingMalamInstance, field: 't421NamaPemilik', 'error')} required">
    <label class="control-label" for="t421NamaPemilik">
        <g:message code="towingMalam.t421NamaPemilik.label" default="Nama Pemilik" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t421NamaPemilik" id="t421NamaPemilik" maxlength="50" required="" value="${towingMalamInstance?.t421NamaPemilik}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: towingMalamInstance, field: 't421NoTelp', 'error')} required">
    <label class="control-label" for="t421NoTelp">
        <g:message code="towingMalam.t421NoTelp.label" default="No Telp" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t421NoTelp" id="t421NoTelp" maxlength="50" required="" value="${towingMalamInstance?.t421NoTelp}"/>
    </div>
</div>

%{
    }
}%