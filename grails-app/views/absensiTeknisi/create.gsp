<%@ page import="com.kombos.administrasi.ManPowerAbsensi" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'absensiTeknisi.label', default: 'ManPower Rencana Hadir')}" />
		<title>Absensi Data</title>
	</head>
	<body>
		<div id="create-absensiTeknisi" class="content scaffold-create" role="main">

            <g:if test="${gagal}">
                <div class="message" style="margin-left: 520px; color: red">${gagal}</div>
            </g:if>
			<g:hasErrors bean="${absensiTeknisiInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${absensiTeknisiInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
			<g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" update="absensiTeknisi-form"
              url="[controller: 'absensiTeknisi', action:'save']">
				<fieldset class="form" style="margin-left: 320px">
                    <g:hiddenField name="statusAbsen" value="${statusAbsen}" />
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls" style="margin-left: 500px">
					<a class="btn cancel" href="javascript:void(0);"
                       onclick="closeAbsensi();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
					<g:submitButton class="btn btn-primary create" name="save" value="Masuk" />
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
