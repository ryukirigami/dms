<%@ page import="org.codenarc.rule.logging.PrintlnAstVisitor; com.kombos.administrasi.NamaManPower; com.kombos.administrasi.ManPowerAbsensi" %>

<g:javascript>
    $(function(){
        $('#t015IdManPower').typeahead({
            source: function (query, process) {
                return $.get('${request.contextPath}/absensiTeknisi/getTeknisi', { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });
    });

    $('#namaManPower').change(function(){
                    var namaManPower=$('#namaManPower').val();

                    $.ajax({
                        url:'${request.contextPath}/absensiTeknisi/getIdTeknisi',
                        type:'POST',
                        data:'id=' + namaManPower,
                        dataType: 'json',
    		            success: function(data,textStatus,xhr){
//    		                if(data.length>0){
    		                    $('#t015IdManPower').val(data.t015IdManPower);

//    		                }else{
//                                $('#t015IdManPower').attr('value','');
//    		                }

    		            },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(data,textStatus){
                        }
                   });
                  });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: absensiTeknisiInstace, field: 't017Tanggal', 'error')} required">
	<label class="control-label" for="t017Tanggal">
		<g:message code="absensiTeknisi.t017Tanggal.label" default="Hari & Tanggal" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField name="tanggal" id="tanggal" maxlength="20" value="${tanggal}" readonly="readonly"  />
	</div>
</div>
<g:javascript>
    document.getElementById("password").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: absensiTeknisiInstace, field: 't017Tanggal', 'error')} required">
    <label class="control-label" for="t017Tanggal">
        <g:message code="absensiTeknisi.t017Tanggal.label" default="Pukul" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="pukul" id="pukul" maxlength="20" value="${pukul}" readonly="readonly"  />
    </div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: absensiTeknisiInstace, field: 't017Tanggal', 'error')} required">
    <label class="control-label" for="t017Tanggal">
        <g:message code="absensiTeknisi.t017Tanggal.label" default="Inisial Teknisi" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="namaManPower" noSelection="['':'Pilih Nama Teknisi']" name="namaManPower.id" from="${NamaManPower.createCriteria().list{eq("staDel","0");
            manPowerDetail{
                manPower{
                    or{
                        eq("m014Inisial","TKBD")
                        eq("m014Inisial","TKGR")
                        eq("m014Inisial","TKPT")
                    }
                }
            }
            eq("companyDealer", session.userCompanyDealer)
            order("t015NamaBoard")
        }
        }" optionKey="id" value="${absensiTeknisiInstace?.namaManPower?.id}" class="many-to-one"/>
    </div>
</div>
%{--===sebelum ====--}%
%{--<div class="control-group fieldcontain ${hasErrors(bean: absensiTeknisiInstace, field: 't015IdManPower', 'error')} required">--}%
    %{--<label class="control-label" for="t017Tanggal">--}%
        %{--<g:message code="absensiTeknisi.t015IdManPower.label" default="ID Teknisi" />--}%
        %{--<span class="required-indicator">*</span>--}%
    %{--</label>--}%
    %{--<div class="controls">--}%
        %{--<input type="text" name="t015IdManPower" id="t015IdManPower" class="typeahead" />--}%
        %{--autocomplete="off"--}%
    %{--</div>--}%
%{--</div>--}%
%{--====== sebeluma =====--}%
%{--ian--}%
<div class="control-group">
    <label class="control-label" for="t017Tanggal">
        <g:message code="absensiTeknisi.t015IdManPower.label" default="ID Teknisi" />
    </label>
    <div class="controls">
        <g:textField name="t015IdManPower" id="t015IdManPower" value="${absensiTeknisiInstace?.t015IdManPower?.id }" readonly="" />
    </div>
</div>
%{--Dita--}%

<div class="control-group fieldcontain ${hasErrors(bean: absensiTeknisiInstace, field: 't017Tanggal', 'error')} required">
    <label class="control-label" for="t017Tanggal">
        <g:message code="absensiTeknisi.t017Tanggal.label" default="Password" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        %{--<g:passwordField name="password" id="password" maxlength="20" value="${password}"   />--}%
        <g:passwordField name="password" id="password" maxlength="20"  />
    </div>
</div>