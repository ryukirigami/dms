
<%@ page import="com.kombos.administrasi.ManPowerAbsensi" %>
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'absensiTeknisi.label', default: 'ManPower Rencana Hadir')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var closeAbsensi;
	$(function(){
    closeAbsensi = function(){
        $("#absensiTeknisi-form").css("display","none");
        $("#absensiTeknisi-table").show();
   	}

	closeAbsensi();

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();" />
                closeAbsensi();
        break;

}
return false;
});


absensi = function(id) {
shrinkTableLayout();
  $('#spinner').fadeIn(1);
  $.ajax({type:'POST', url:'${request.contextPath}/absensiTeknisi/create/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    loadForm = function(data, textStatus){
		$('#absensiTeknisi-form').empty();
    	$('#absensiTeknisi-form').append(data);
   	}

    shrinkTableLayout = function(){
    	$("#absensiTeknisi-table").hide()
        $("#absensiTeknisi-form").css("display","block");
   	}
});

    </g:javascript>


</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left">Absensi Teknisi</span>
    <ul class="nav pull-right">

    </ul>
</div>
<div class="box">
    <div class="span12" id="absensiTeknisi-table">
         <fieldset>
            <table style="padding-right: 10px; align-content: center;">
                <tr>
                    <td colspan="3" >
                        <div class="controls" style="margin-left:370px;">
                            <button style="width: 200px;height: 70px; border-radius: 5px" class="btn delete" onclick="absensi(0)" name="masuk" id="masuk" >Masuk / Datang</button>
                            <button style="width: 200px;height: 70px; border-radius: 5px" class="btn delete" onclick="absensi(1)" name="pulang" id="pulang" >Keluar / Pulang</button>
                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>
        %{--ian--}%
        <br/>
        <br/>
        <br/>

    <table id="absensi_datatables" cellpadding="0" cellspacing="0"
           border="0"
           class="display table table-striped table-bordered table-hover"
           width="100%">
        <thead>
        <tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="goods.m111ID.label" default="No" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="goods.m111Nama.label" default="Nama Teknisi" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="goods.satuan.label" default="Jam Masuk" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="goods.satuan.label" default="Jam Pulang" /></div>
            </th>
        </tr>
        </thead>
    </table>

    <g:javascript>
var absensiTable;
var reloadAbsensiTable;
$(function(){
	reloadAbsensiTable = function() {
		absensiTable.fnDraw();
	}
	var recordsAbsensiPerPage = [];
    var anAbsensiSelected;
    var jmlRecAbsensiPerPage=0;
    var id;
if(absensiTable)
    	absensiTable.dataTable().fnDestroy();
	absensiTable = $('#absensi_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsAbsensi = $("#absensi_datatables tbody .row-select");
            var jmlAbsensiCek = 0;
            var nRow;
            var idRec;
            rsAbsensi.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsAbsensiPerPage[idRec]=="1"){
                    jmlAbsensiCek = jmlAbsensiCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsAbsensiPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecAbsensiPerPage = rsAbsensi.length;
            if(jmlAbsensiCek==jmlRecAbsensiPerPage && jmlRecAbsensiPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 10, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "nomor",
	"mDataProp": "nomor",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "teknisi",
	"mDataProp": "teknisi",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "jamMasuk",
	"mDataProp": "jamMasuk",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},

{
	"sName": "jamKeluar",
	"mDataProp": "jamKeluar",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {



						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});

});
            </g:javascript>
    </div>


    <div class="span7" id="absensiTeknisi-form" style="display: none;"></div>
    <g:if test="${sukses}">
        <g:javascript>
            toastr.success("Sukses Absesn");
        </g:javascript>
    </g:if>
</div>
</body>
</html>
