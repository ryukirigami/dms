
<%@ page import="com.kombos.parts.PickingSlipDetail" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="pickingSlip_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover fixed"
    style="table-layout: fixed; width: 550px">
    <col width="200px" />
    <col width="350px" />
 	<thead>
		<tr>

            <th></th>
            <th></th>
            <th></th>
            <th></th>

            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="pickingSlipDetail.pickingSlip.goods.m111ID.label" default="Kode Goods" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="pickingSlipDetail.pickingSlip.goods.m111Nama.label" default="Nama Goods" /></div>
			</th>


		</tr>
		<tr>

            <th></th>
            <th></th>
            <th></th>
            <th></th>

            <th style="border-top: none;padding: 5px;">
				<div id="filter_m111ID_modal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m111ID_modal" id="search_m111ID_modal" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m111Nama_modal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m111Nama_modal" id="search_m111Nama_modal" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var pickingSlipTable;
var reloadPickingSlipTable;
var idRecept = "${idRecept}";
$(function(){

    var recordsClaimPerPage = [];
    var anClaimSelected;
    var jmlRecClaimPerPage=0;
    var id;

	reloadPickingSlipTable = function() {
		pickingSlipTable.fnDraw();
	}

	if(pickingSlipTable!=null){
	    console.log("ada ini");
	    pickingSlipTable.fnDestroy();
	}



$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		//	alert("hallo");
		 	pickingSlipTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	pickingSlipTable = $('#pickingSlip_datatables').dataTable({
		"sScrollX": "550px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
			"fnDrawCallback": function () {

            var rsClaim = $("#pickingSlip_datatables tbody .row-select");
            var jmlClaimCek = 0;
            var nRow;
            var idRec;
            rsClaim.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsClaimPerPage[idRec]=="1"){
                    jmlClaimCek = jmlClaimCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsClaimPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecClaimPerPage = rsClaim.length;
            if(jmlClaimCek==jmlRecClaimPerPage && jmlRecClaimPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
	"sName": "id",
	"mDataProp": "id",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,

	"bVisible": false
}
,
{
	"sName": "id",
	"mDataProp": "satuan",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,

	"bVisible": false
}
,
{
	"sName": "id",
	"mDataProp": "noUrut",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,

	"bVisible": false
},

{
	"sName": "id",
	"mDataProp": "qtyPicking",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": false
}
,
{
	"sName": "goods",
	"mDataProp": "m111ID_modal",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['noUrut']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t142StaPrePicking",
	"mDataProp": "m111Nama_modal",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"350px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m111ID = $('#filter_m111ID_modal input').val();
						if(m111ID){
							aoData.push(
									{"name": 'sCriteria_m111ID_modal', "value": m111ID}
							);
						}

						var m111Nama = $('#filter_m111Nama_modal input').val();
						if(m111Nama){
							aoData.push(
									{"name": 'sCriteria_m111Nama_modal', "value": m111Nama}
							);
						}

                        if(idRecept){
						    aoData.push(
									{"name": 'idRecept', "value": "${idRecept}"}
							);
						}

                        aoData.push(
									{"name": 'type', "value": "${type}"}
						);


                        var exist = [];
						$("#blockStokDetail_datatables tbody .row-select").each(function() {
							var id = $(this).next("input:hidden").val();
							exist.push(id);
						});
						if(exist.length > 0){
							aoData.push(
										{"name": 'sCriteria_exist', "value": JSON.stringify(exist)}
							);
						}

                        var existEdit = [];
						$("#blockStokDetailEdit_datatables tbody .row-select").each(function() {
							var id = $(this).next("input:hidden").val();
							existEdit.push(id);
						});
						if(existEdit.length > 0){
							aoData.push(
										{"name": 'sCriteria_existEdit', "value": JSON.stringify(existEdit)}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

		$('.select-all').click(function(e) {

        $("#pickingSlip_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsClaimPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsClaimPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#pickingSlip_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsClaimPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anClaimSelected = pickingSlipTable.$('tr.row_selected');

            if(jmlRecClaimPerPage == anClaimSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsClaimPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });


});
</g:javascript>


			
