
<%@ page import="com.kombos.parts.Goods" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'blockStokInput.label', default: 'Input Block Stock')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var showGoods;
	var loadFormGoods;
	var shrinkTableLayout;
	var expandTableLayout;
	var addPickingSlip2;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadFormGoods(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    showGods = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/goods/show/'+id,
   			success:function(data,textStatus){
   				loadFormGoods(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    loadFormGoods = function(data, textStatus){
		$('#blokStokDetail-form').empty();
    	$('#blokStokDetail-form').append(data);
   	}

    expandTableLayout = function(){
   		if($("#blokStokDetail-table").hasClass("span5")){
   			$("#blokStokDetail-table").toggleClass("span5 span12");
   		}
        $("#blokStokDetail-form").css("display","none");
   	    }

addPickingSlip2 = function(){
           checkGoods =[];
            $("#pickingSlip_datatables .row-select").each(function() {
                if(this.checked){
                  var nRow = $(this).parents('tr')[0];
					checkGoods.push(nRow);

                }
            });
            if(checkGoods.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                return;
             //   loadPilihJobModal();
            }else{
              //  alert("lenght : "+checkGoods.length);
                for (var i=0;i<checkGoods.length;i++){
             //   alert("var i "+i);
					var aData = pickingSlipTable.fnGetData(checkGoods[i]);
					blockStokDetailTable.fnAddData({
					    'id' : aData['id'],
						'kodeParts': aData['m111ID_modal'],
						'namaParts': aData['m111Nama_modal'],
						'qtyPicking': aData['qtyPicking'],
						'satuan1': aData['satuan'],
						'qtyBlockStock': '0',
						'satuan2':  aData['satuan'],
						'location':'0'});
                        $('#qtyBlockStok'+aData['id']).autoNumeric('init',{
                                                vMin:'0',
                                                vMax:'999999999999999999',
                                                mDec: null,
                                                aSep:''
                                            });

				}
            }

        checkGoods = [];

    }
});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
	</div>
	<div class="box">
		<div class="span12" id="blokStokDetail-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="blokStokDetail-form" style="display: none;"></div>
	</div>
</body>
</html>
