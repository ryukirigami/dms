<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="parts_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable fixed" style="table-layout: fixed; ">
    <col width="100px" />
    <col width="100px" />
    <col width="60px" />
    <col width="60px" />
    <col width="60px" />
    <col width="60px" />
    <col width="60px" />
    <col width="40px" />
    <col width="40px" />
    <col width="40px" />
    <col width="40px" />
    <thead>
    <tr>
        <th style="border-bottom: none; text-align:center" rowspan="2">
            <div><input type="checkbox" style=" width: 13px;
                height: 13px;
                padding: 0;
                margin:0;
                vertical-align: bottom;
                position: relative;
                top: -1px;
                *overflow: hidden;" class="pull-left select-all" />Kode Parts</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center" rowspan="2">
            <div class="center">Nama Parts</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center" rowspan="2">
            <div class="center">Tanggal Dan Jam Receive</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center" rowspan="2">
            <div class="center">Nomor PO</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center" rowspan="2">
            <div class="center">Tanggal PO</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center" rowspan="2">
            <div class="center">Nomor Invoice</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center" rowspan="2">
            <div class="center">Tanggal Invoice</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center" colspan="4">
            <div class="center">Qty</div>
        </th>
    </tr>
    <tr>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Invoice</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Receive</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Rusak</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Salah</div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var partsTable;
var reloadpartsTable;
$(function(){

    reloadpartsTable = function() {
		partsTable.fnDraw();
	}
    var recordspartsperpage = [];//new Array();
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;

	partsTable = $('#parts_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsParts = $("#parts_datatables tbody .row-select");
            var jmlPartsCek = 0;
            var nRow;
            var idRec;
            rsParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                recordsPartsAll.push(idRec);
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordspartsperpage[idRec]=="1"){
                    jmlPartsCek = jmlPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspartsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsPerPage = rsParts.length;
            if(jmlPartsCek == jmlRecPartsPerPage && jmlRecPartsPerPage > 0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "inputBinningDatatablesList")}",
		"aoColumns": [

{
	"sName": "kodePart",
	"mDataProp": "kodePart",
	"aTargets": [0],
    "mRender": function ( data, type, row ) {
        return '<input type="checkbox" style="width: 13px; height: 13px; padding: 0; margin:0; vertical-align: bottom; position: relative; top: -1px; *overflow: hidden;" ' +
                'class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
    },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "namaPart",
	"mDataProp": "namaPart",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tglJamReceive",
	"mDataProp": "tglJamReceive",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
,

{
	"sName": "nomorPO",
	"mDataProp": "nomorPO",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "tglPO",
	"mDataProp": "tglPO",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
,


{
	"sName": "nomorInvoice",
	"mDataProp": "nomorInvoice",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
,

{
	"sName": "tglInvoice",
	"mDataProp": "tglInvoice",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
,
{
	"sName": "qInvoice",
	"mDataProp": "qInvoice",
	"aTargets": [7],
    "mRender": function ( data, type, row ) {
    			return '<input id="qInvoice_'+row['id']+'" class="inline-edit" type="text" style="width:70px;" value="'+data+'">';
    		},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"80px",
	"bVisible": true
}
,

{
	"sName": "qReceive",
	"mDataProp": "qReceive",
	"aTargets": [8],
    "mRender": function ( data, type, row ) {
    			return '<input id="qReceive_'+row['id']+'" class="inline-edit" type="text" style="width:70px;" value="'+data+'">';
    		},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"80px",
	"bVisible": true
}

,

{
	"sName": "qRusak",
	"mDataProp": "qRusak",
	"aTargets": [9],
    "mRender": function ( data, type, row ) {
    			return '<input id="qRusak_'+row['id']+'" class="inline-edit" type="text" style="width:70px;" value="'+data+'">';
    		},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"80px",
	"bVisible": true
}
,

{
	"sName": "qSalah",
	"mDataProp": "qSalah",
	"aTargets": [10],
    "mRender": function ( data, type, row ) {
    			return '<input id="qSalah_'+row['id']+'" class="inline-edit" type="text" style="width:70px;" value="'+data+'">';
    		},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"80px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            aoData.push(
                {"name": 'binningId', "value": "${binningInstance?.id}"}
            );
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});

    $('.select-all').click(function(e) {

        $("#parts_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordspartsperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordspartsperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#parts_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordspartsperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPartsSelected = partsTable.$('tr.row_selected');
            if(jmlRecPartsPerPage == anPartsSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordspartsperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>

