
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<head>
<meta name="layout" content="main">
<title>Input Binning</title>
<r:require modules="baseapplayout"/>

<g:javascript disposition="head">
    var recordsPartsAll = [];   //untuk menyimpan dari modal
    var noUrut = 1;
	var checkOnValue;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
        $(function () {

	$("#requestAddModal").on("show", function() {
		//$("#requestAddModal .btn").on("click", function(e) {
		$("#closeAddPart").on("click", function(e) {
			$("#requestAddModal").modal('hide');
		});
	});

	$("#requestAddModal").on("hide", function() {
		$("#requestAddModal a.btn").off("click");
	});

    loadRequestAddModal = function(){
		$("#requestAddContent").empty();
		$.ajax({type:'POST', url:'${request.contextPath}/inputBinning/addModal',
   			success:function(data,textStatus){
					$("#requestAddContent").html(data);
				    $("#requestAddModal").modal({
						"backdrop" : "static",
						"keyboard" : true,
						"show" : true
					}).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
		}

       tambahReq = function(){
            var checkGoods =[];
            $("#requestAdd-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    console.log("id=" + id);

                    var nRow = $(this).parents('tr')[0];
                    var aData = requestAddTable.fnGetData(nRow);

                    var code = aData['kodePart'];
                    var isNotYet = true;
                    $('#parts_datatables tbody .row-select').each(function() {
                        var nRowExisting = $(this).parents('tr')[0];
                        var aDataExisting = partsTable.fnGetData(nRowExisting);
                        var codeExisting = aDataExisting['partsCode'];
                        if (code === codeExisting) {
                            isNotYet = false;
                        }
                    });
                    if (isNotYet) {
                        checkGoods.push(nRow);
                        recordsPartsAll.push(id);
                    }
                }
            });
            if(checkGoods.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                loadRequestAddModal();
            }
            else {
				for (var i=0;i < checkGoods.length;i++){

					var aData = requestAddTable.fnGetData(checkGoods[i]);
//					alert("aData=" + aData['id'] + ", " + aData['kodePart'] + ", " + aData['namaPart'] + ", " +
//					    aData['nomorPO'] + ", " + aData['tglPO']  + ", " + aData['tipeOrder'] );
                    //alert("partsTable=" + partsTable);
					partsTable.fnAddData({
					    'id': aData['id'],
                        'norut': noUrut,
                        'kodePart': aData['kodePart'],
                        'namaPart': aData['namaPart'],
                        'tglJamReceive': aData['tglJamReceive'],
                        'nomorPO': aData['nomorPO'],
                        'tglPO': aData['tglPO'],
                        'nomorInvoice': aData['nomorInvoice'],
                        'tglInvoice': aData['tglInvoice'],
                        'qInvoice': aData['qtyInvoice'],
                        'qReceive': aData['qtyRece'],
                        'qRusak': '0',
                        'qSalah': '0'
						});
					noUrut++;
					$('#qInvoice_'+aData['id']).autoNumeric('init',{
						vMin:'0',
						vMax:'999999999999999999',
						mDec: '2',
						aSep:''
					}).keypress(checkOnValue)
					.change(checkOnValue);

					$('#qReceive_'+aData['id']).autoNumeric('init',{
                        vMin:'0',
                        vMax:'999999999999999999',
                        mDec: '2',
                        aSep:''
                    }).keypress(checkOnValue)
                    .change(checkOnValue);

					$('#qRusak_'+aData['id']).autoNumeric('init',{
                        vMin:'0',
                        vMax:'999999999999999999',
                        mDec: '2',
                        aSep:''
                    }).keypress(checkOnValue)
                    .change(checkOnValue);

					$('#qSalah_'+aData['id']).autoNumeric('init',{
                        vMin:'0',
                        vMax:'999999999999999999',
                        mDec: '2',
                        aSep:''
                    }).keypress(checkOnValue)
                    .change(checkOnValue);
				}
                var rowNum = checkGoods.pop();
                requestAddTable.fnDeleteRow(rowNum);
			}
      }

	checkOnValue = function() {
		var $this   = $(this);
		var value = $this.val();
		var nRow = $this.parents('tr')[0];
		var checkInput = $('input[type="checkbox"]', nRow);
		if(value > 0){
			checkInput.attr("checked","checked");
		} else {
			checkInput.removeAttr("checked");
		}
	}

             onSaveData = function(){
                var input_rcvDate = $("#input_rcvDate").val();
                var input_rcvHour = $("#input_rcvHour").val();
                var input_rcvMinuite = $("#input_rcvMinuite").val();
                console.log("input_rcvDate=" + input_rcvDate + " input_rcvHour=" + input_rcvHour + " input_rcvMinuite=" + input_rcvMinuite);
                var binning_id = $("#binning_id").val();
                console.log("binning_id=" + binning_id);

                if (input_rcvDate) {
                    if (confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}'))
                    {
                        try{
                            var checkJob =[];
                            var qInvoice = "";
                            var qReceive = "";
                            var qRusak = "";
                            var qSalah = "";
                            var isQtyOk = true;
                                $("#parts_datatables tbody .row-select").each(function() {
                                    if(this.checked){
                                        var id = $(this).next("input:hidden").val();
                                        checkJob.push(id);
                                        qInvoice += "&qInvoice_"+id+"="+document.getElementById("qInvoice_"+id).value;
                                        qReceive += "&qReceive_"+id+"="+document.getElementById("qReceive_"+id).value;
                                        qRusak += "&qRusak_"+id+"="+document.getElementById("qRusak_"+id).value;
                                        qSalah += "&qSalah_"+id+"="+document.getElementById("qSalah_"+id).value;
                                        console.log("id=" + id + " qInvoice=" + qInvoice + " qReceive=" + qReceive
                                        + " qRusak=" + qRusak + " qSalah=" + qSalah);

                                        var iqSalah = parseInt(document.getElementById("qSalah_"+id).value);
                                        var iqRusak = parseInt(document.getElementById("qRusak_"+id).value);
                                        var iqReceive = parseInt(document.getElementById("qReceive_"+id).value);
                                        console.log("iqSalah=" + iqSalah + " iqRusak=" + iqRusak + "iqReceive=" + iqReceive);
                                        if ((iqSalah + iqRusak) > iqReceive) {
                                            isQtyOk = false;
                                        }
                                    }
                                });
                                console.log("checkJob.length=" + checkJob.length + " checkJob=" + checkJob);

                                if(checkJob.length == 0){
                                    alert('Anda belum memilih data yang akan disimpan');
                                    return;
                                } else if (!isQtyOk) {
                                    alert('Jumlah Salah dan Rusak tidak boleh melebihi jumlah Receive');
                                    return;
                                }

                                var checkBoxVendor = JSON.stringify(checkJob);
                                console.log("checkBoxVendor=" + checkBoxVendor);
                                $.ajax({
                                    url:'${request.contextPath}/inputBinning/save?1=1'+qInvoice+qReceive+qRusak+qSalah,
                                    type: "POST", // Always use POST when deleting data
                                    data : { ids: checkBoxVendor, input_rcvDate:input_rcvDate, input_rcvHour:input_rcvHour, input_rcvMinuite:input_rcvMinuite,
                                       binning_id: binning_id,
                                       qInvoice:qInvoice, qReceive: qReceive, qRusak: qRusak, qSalah: qSalah},
                                    success : function(data){
                                        toastr.success('Save success');
                                        //cancelling();
                                        shrinkTableLayout();
                                        console.log("after shrinkTableLayout()");
                                        $('#spinner').fadeIn(1);
                                        console.log("after spinner fadeIn");

                                        $.ajax({type:'POST', url:'${request.contextPath}/binning/',
                                            success:function(data,textStatus){
                                                //loadForm(data, textStatus);
                                                $('#binning-form').empty();
                                                $('#binning-form').append(data);
                                            },
                                            error:function(XMLHttpRequest,textStatus,errorThrown){
                                                console.log("on error save, textStatus=" + textStatus + ", errorThrown=" + errorThrown);
                                            },
                                            complete:function(XMLHttpRequest,textStatus){
                                                $('#spinner').fadeOut();
                                            }
                                        });
                                    },
                                    error: function(xhr, textStatus, errorThrown) {
                                        alert('Internal Server Error');
                                    }
                                });

                            checkJob = [];
                        }catch (e){
                            alert(e)
                        }
                    }
                } else {
                    if (!input_rcvDate) {
                        alert("Silahkan memilih Tanggal Binning");
                     }
                }
             }

             onDeleteParts = function(){
                var checkedRows =[];
                $('#parts_datatables tbody .row-select').each(function() {
                    if(this.checked){
                        var row = $(this).closest('tr');
                        var nRow = row[0];
                        console.log("nRow=" + nRow);
                        checkedRows.push(nRow);
                    }
                });
                for (var i=checkedRows.length-1; i >= 0 ;i--){
                    var rowNum = checkedRows.pop();
                    partsTable.fnDeleteRow(rowNum);
                }
             }


    shrinkTableLayout = function(){
        $("#input-binning-table").hide();
        console.log("afterHide");
        $("#input-binning-form").css("display","block");
   	}

    loadForm = function(data, textStatus){
        console.log($('inside loadForm'));
        console.log($('#binning-form'));
        console.log($('#binning-form').html());
		$('#binning-form').empty();
		console.log("loadForm data=" + data);
		console.log("loadForm textStatus=" + textStatus);
    	$('#binning-form').append(data);
    	console.log("after loadForm");
   	}

    loadFormAfterInsert = function(data, textStatus){
        $("#binning-table").show();
        $("#binning-form").css("display","none");
    }

    cancelling = function() {
        shrinkTableLayout();
        $('#spinner').fadeIn(1);
        $.ajax({type:'POST', url:'${request.contextPath}/binning/',
            success:function(data,textStatus){
                //loadForm(data, textStatus);
                $('#binning-form').empty();
                $('#binning-form').append(data);
            },
            error:function(XMLHttpRequest,textStatus,errorThrown){},
            complete:function(XMLHttpRequest,textStatus){
                $('#spinner').fadeOut();
            }
        });
    }

});
</g:javascript>

</head>

<body>
<div class="span12" id="input-binning-table">
<fieldset class="form">

    <div class="box">
        <form id="form-request" class="form-horizontal">
            <legend style="font-size: small">Input Binning</legend>
            <fieldset>
            %{--<input type="hidden" name="input_id" value="${binningInstance?.t168ID}">--}%
            %{--<div class="control-group">
                <label class="control-label" for="input_id" style="text-align: left;">Nomor Binning <span class="required-indicator">*</span>
                </label>
                <div id="filter_nomor" class="controls">
                    <input type="text" name="input_id" id="input_id" value="${binningInstance?.t168ID}" readonly="readonly" >
                </div>
            </div>--}%
            <g:hiddenField name="binning_id" id="binning_id" value="${binningInstance?.id}" />
            <div class="control-group">
                <label class="control-label" for="input_rcvDate" style="text-align: left;">
                    Tanggal Binning <span class="required-indicator">*</span>
                </label>
                <div id="tglBinning" class="controls">
                    <ba:datePicker id="input_rcvDate" name="input_rcvDate" precision="day" format="dd-MM-yyyy"  value="${tglSkrg}"  />
                    <select id="input_rcvHour" name="input_rcvHour" style="width: 60px" required="" >
                        <g:each in="${hourList}" status="o" var="hr">
                                <g:if test="${jamSkrg.equals(hr) || ("0" + jamSkrg).equals(hr)}">
                                    <option selected="selected" value="${hr}">${hr}</option>
                                </g:if>
                                <g:else>
                                    <option value="${hr}">${hr}</option>
                                </g:else>
                        </g:each>
                    </select> H :
                    <select id="input_rcvMinuite" name="input_rcvMinuite" style="width: 60px" required="" >
                        <g:each in="${minuiteList}" status="o" var="mnt">
                            <g:if test="${mntSkrg.equals(mnt) || ("0" + mntSkrg).equals(mnt)}">
                                <option selected="selected" value="${mnt}">${mnt}</option>
                            </g:if>
                            <g:else>
                                <option value="${mnt}">${mnt}</option>
                            </g:else>
                        </g:each>
                    </select>
                </div>
            </div>
            </fieldset>
        </form>
        <fieldset class="buttons controls">
            <button class="btn btn-primary" onclick="loadRequestAddModal();">Add Parts</button>
        </fieldset>
    </div>


    <div class="box">
        <legend style="font-size: small">Parts Binning</legend>
        <div id="binning_table">
            <g:render template="partsDataTables"/>
        </div>
            <a class="btn cancel" href="javascript:void(0);" onclick="onDeleteParts();">
                <g:message code="default.button.delete.label" default="Delete" />
            </a>

            <button class="btn btn-primary create" onclick="onSaveData();">Save</button>

            <button class="btn cancel" onclick="cancelling();">Cancel</button>
    </div>
</fieldset>

<div id="requestAddModal" class="modal fade">
    <div class="modal-dialog" style="width: 1200px;">
        <div class="modal-content" style="width: 1200px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 550px;">
                <div id="requestAddContent"/>
            </div>
            <!-- dialog buttons -->
        </div>
    </div>
</div>
</div>

<div class="span12" id="input-binning-form" style="display: none;margin-left: 0;"></div>
</body>
</html>