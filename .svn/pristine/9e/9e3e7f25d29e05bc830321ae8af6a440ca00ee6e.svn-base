package com.kombos.baseapp.utils

import com.kombos.baseapp.AuditTrailDetail
import com.kombos.baseapp.AuditTrailHeader
import com.kombos.baseapp.AuditTrailLog
import org.codehaus.groovy.grails.commons.DefaultGrailsDomainClass

class AuditTrailLogUtilService {
    def grailsApplication

    def setParamRequest(def params, def request, def menuCode){
        params.remoteUser = request.getRemoteUser()
        params.remoteAddr = request.getRemoteAddr()
        params.menuCode = menuCode
        return params
    }

    def auditTrailDomainInsert(def object, def params, def request, def menuCode) {
		 try{
            StringBuilder auditString = this.readObjectDomain(object)

            this.saveToAuditTrail(
                    AuditTrailLog.INSERT
                    ,getValueOfId(object, getPkIdColumn(object))
                    ,''
                    ,auditString.toString()
                    ,setParamRequest(params, request, menuCode)
            )

            this.saveAuditTrailDetail(
                    AuditTrailLog.INSERT
                    ,getValueOfId(object, getPkIdColumn(object))
                    ,null
                    ,object
                    ,setParamRequest(params, request, menuCode)
            )
        }catch(Exception e){
            e.printStackTrace()
        }
    }

    def auditTrailDomainUpdate(def oldObject, def newObject, def params, def request, def menuCode) {
		def props = new DefaultGrailsDomainClass(newObject.class).persistentProperties.toList()
        try{
            StringBuilder auditStringNew = this.readObjectDomain(newObject, props)
            StringBuilder auditStringOld = this.readObjectDomain(oldObject, props)

            this.saveToAuditTrail(
                    AuditTrailLog.UPDATE
                    ,getValueOfId(oldObject, getPkIdColumn(oldObject))
                    ,auditStringOld.toString()
                    ,auditStringNew.toString()
                    ,setParamRequest(params, request, menuCode)
            )

            this.saveAuditTrailDetail(
                    AuditTrailLog.UPDATE
                    ,getValueOfId(oldObject, getPkIdColumn(oldObject))
                    ,oldObject
                    ,newObject
                    ,setParamRequest(params, request, menuCode)
            )
        }catch(Exception e){
            e.printStackTrace()
        }
    }

    def auditTrailDomainDelete(def oldObject, def params, def request, def menuCode) {
        try{
            StringBuilder auditStringOld = this.readObjectDomain(oldObject)

            this.saveToAuditTrail(
                    AuditTrailLog.DELETE
                    ,getValueOfId(oldObject, getPkIdColumn(oldObject))
                    ,auditStringOld.toString()
                    ,''
                    ,setParamRequest(params, request, menuCode)
            )

//            this.saveAuditTrailDetail(
//                    AuditTrailLog.DELETE
//                    ,getValueOfId(oldObject, getPkIdColumn(oldObject))
//                    ,oldObject
//                    ,null
//                    ,setParamRequest(params, request, menuCode)
//            )
        }catch(Exception e){
            e.printStackTrace()
        }
    }

    //get pk id column
    def getPkIdColumn(def object){
        def domainClass = grailsApplication.getDomainClass(object.getClass().getName())

        def idPropertyName = null
        def dcMapping = org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsDomainBinder.getMapping(domainClass.getClazz())
        if (dcMapping) {
            def metaClass =  dcMapping.identity.metaClass

            if (metaClass.hasProperty(dcMapping.identity, "name") && dcMapping.identity.name) {
                idPropertyName = dcMapping.identity.name
            }
            else if (metaClass.hasProperty(dcMapping.identity, "propertyNames") && dcMapping.identity.propertyNames) {
                idPropertyName = dcMapping.identity.propertyNames
            }
            else if (metaClass.hasProperty(dcMapping.identity, "natural") && dcMapping.identity.natural) {
                idPropertyName = dcMapping.identity.natural.propertyNames
            }
        }
        return idPropertyName
    }

    //read object domain class
    def readObjectDomain(def object){
        StringBuilder auditString = new StringBuilder()
        new DefaultGrailsDomainClass(object.class).persistentProperties.toList().each { property ->
            auditString.append(property.name+"="+object[property.name]+"| ")
        }
        return auditString
    }
	
	def readObjectDomain(def object, def properties){
		StringBuilder auditString = new StringBuilder()
		properties.each { property ->
			auditString.append(property.name+"="+object[property.name]+"| ")
		}
		return auditString
	}

    //read column
    def readColumnDomain(def object){
        String auditString = ""
        new DefaultGrailsDomainClass(object.class).persistentProperties.toList().each { property ->
            auditString = auditString + (property.name+",")
        }
        return auditString.substring(0,auditString.length()-1)
    }

    //set object value from sql
    def createOldObject(Class domainClass, def columnTable, def oldObjectSql){
        def oldObject = domainClass.newInstance()
        int i = 0
        columnTable.split(',').each {
            oldObject[it] = oldObjectSql[i]
            i++
        }
        return oldObject
    }

    //get old object
//    def getOldObject(def object){
//        Class domainClass = grailsApplication.getClassForName(object.getClass().getName())
//        String idColumn = this.getPkIdColumn(object)
//        StringBuilder auditString = new StringBuilder()
//        def idValue = getValueOfId(object, idColumn)
//        def columnTable = this.readColumnDomain(object)
//        def query = new StringBuilder("select "+columnTable+" from ${domainClass.getSimpleName()} where ${idColumn?idColumn:'id'} = '${idValue}'")
//        def oldObjectSql = domainClass.executeQuery(query.toString())
//        def oldObject = createOldObject(domainClass, columnTable, oldObjectSql[0])
//        return oldObject
//    }

    //insert into auditTrailLog
    def saveToAuditTrail(
        String actionType
        ,def pkId
        ,String oldValue
        ,String newValue
        ,def paramsRequest
    ) {
        def auditTrailLog = new AuditTrailLog(
                actionType : actionType
                ,userName : paramsRequest.remoteUser
                ,menuCode : paramsRequest.menuCode
                ,pkId : pkId
                ,oldValue : oldValue
                ,newValue : newValue
                ,createdBy : paramsRequest.remoteUser
                ,createdDate : new Date()
                ,createdHost : "${paramsRequest.remoteUser}:${paramsRequest.remoteAddr}"
                ,lastEditBy : paramsRequest.remoteUser
                ,lastEditDate : new Date()
                ,lastEditHost : "${paramsRequest.remoteUser}:${paramsRequest.remoteAddr}"
                ,createdHostName : "${paramsRequest.remoteUser}:${paramsRequest.remoteAddr}"
        ).save(failOnError: true)

        auditTrailLog.errors.each {
            println it
        }
    }

    //get value of id
    def getValueOfId(def object, def idColumn){
        if(idColumn){
            return object[idColumn]
        }else{
            return object.id
        }
    }

    def saveAuditTrailDetail(String actionType, def pkId, def oldObject, def newObject, def paramsRequest){
        AuditTrailHeader auditTrailHeader = new AuditTrailHeader(
                actionType : actionType
                ,tableName : paramsRequest.menuCode
                ,tablePkIdValue : pkId
                ,createdBy : paramsRequest.remoteUser
                ,createdDate : new Date()
                ,createdHost : "${paramsRequest.remoteUser}:${paramsRequest.remoteAddr}"
                ,updatedBy : paramsRequest.remoteUser
                ,updatedDate : new Date()
                ,updatedHost : "${paramsRequest.remoteUser}:${paramsRequest.remoteAddr}"
        ).save(failOnError: true)

        auditTrailHeader.errors.each {
            println it
        }

        if(auditTrailHeader.pkId){
            if(AuditTrailLog.INSERT.equalsIgnoreCase(actionType)){
                this.saveAuditTrailDetailInsert(auditTrailHeader.pkId,oldObject, newObject, paramsRequest)
            }else if(AuditTrailLog.UPDATE.equalsIgnoreCase(actionType)){
                this.saveAuditTrailDetailUpdate(auditTrailHeader.pkId,oldObject, newObject, paramsRequest)
            }else if(AuditTrailLog.DELETE.equalsIgnoreCase(actionType)){
                this.saveAuditTrailDetailDelete(auditTrailHeader.pkId,oldObject, newObject, paramsRequest)
            }
        }
    }

    def saveAuditTrailDetailInsert(def headerId, def oldObject, def newObject, def paramsRequest){
        new DefaultGrailsDomainClass(newObject.class).persistentProperties.toList().each { property ->
            new AuditTrailDetail(
                    headerId : headerId
                    ,fieldName : property.name
                    ,oldValue : ''
                    ,newValue : newObject[property.name]
                    ,createdBy : paramsRequest.remoteUser
                    ,createdDate : new Date()
                    ,createdHost : "${paramsRequest.remoteUser}:${paramsRequest.remoteAddr}"
                    ,updatedBy : paramsRequest.remoteUser
                    ,updatedDate : new Date()
                    ,updatedHost : "${paramsRequest.remoteUser}:${paramsRequest.remoteAddr}"
            ).save(flush: true)
        }
    }

    def saveAuditTrailDetailUpdate(def headerId, def oldObject, def newObject, def paramsRequest){
        new DefaultGrailsDomainClass(oldObject.class).persistentProperties.toList().each { property ->
            new AuditTrailDetail(
                    headerId : headerId
                    ,fieldName : property.name
                    ,oldValue : oldObject[property.name]
                    ,newValue : newObject[property.name]!=null?newObject[property.name]:""
                    ,createdBy : paramsRequest.remoteUser
                    ,createdDate : new Date()
                    ,createdHost : "${paramsRequest.remoteUser}:${paramsRequest.remoteAddr}"
                    ,updatedBy : paramsRequest.remoteUser
                    ,updatedDate : new Date()
                    ,updatedHost : "${paramsRequest.remoteUser}:${paramsRequest.remoteAddr}"
            ).save(flush: true)
        }
    }

    def saveAuditTrailDetailDelete(def headerId, def oldObject, def newObject, def paramsRequest){
        new DefaultGrailsDomainClass(oldObject.class).persistentProperties.toList().each { property ->
            new AuditTrailDetail(
                    headerId : headerId
                    ,fieldName : property.name
                    ,oldValue : oldObject[property.name]
                    ,newValue : ''
                    ,createdBy : paramsRequest.remoteUser
                    ,createdDate : new Date()
                    ,createdHost : "${paramsRequest.remoteUser}:${paramsRequest.remoteAddr}"
                    ,updatedBy : paramsRequest.remoteUser
                    ,updatedDate : new Date()
                    ,updatedHost : "${paramsRequest.remoteUser}:${paramsRequest.remoteAddr}"
            ).save(flush: true)
        }
    }
}
