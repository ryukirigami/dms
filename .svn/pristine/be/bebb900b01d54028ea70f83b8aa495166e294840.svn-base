package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class ClaimService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Claim.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            if (params."sCriteria_t017Tanggal" && params."sCriteria_t017Tanggal2") {
                ge("t171TglJamClaim", params."sCriteria_t017Tanggal")
                lt("t171TglJamClaim", params."sCriteria_t017Tanggal2" + 1)
            }

//            ilike("status","1")
            eq("staDel",'0')
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("vendor", "vendor")
                groupProperty("t171ID", "t171ID")
                groupProperty("t171TglJamClaim", "t171TglJamClaim")
                groupProperty("t171PetugasClaim", "t171PetugasClaim")
            }

            and{
                order("vendor","asc")
                order("t171ID","asc")
            }
        }
        def rows = []

        results.each {
            rows << [
                    t171TglJamClaim: it.t171TglJamClaim?.format(dateFormat),
                    vendor: it.vendor.m121Nama,
                    t171ID: it.t171ID,
                    t171PetugasClaim: it.t171PetugasClaim,
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def datatablesSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def x = 0
        def c = Claim.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            vendor{
                ilike("m121Nama","%" + params.vendor+ "%")
            }
            ilike("t171ID","%" + params.t171ID  + "%")
            Date date41 = df.parse(params.t171TglJamClaim)

            if (params.t171TglJamClaim) {
                ge("t171TglJamClaim", date41)
                lt("t171TglJamClaim", date41 + 1)
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("goodsReceive", "goodsReceive")
            }
        }

        def rows = []

        results.each {
            rows << [
                    t167NoReff: it.goodsReceive.t167NoReff,
                    t167TglJamReceive: it.goodsReceive.t167TglJamReceive?.format("d/MM/yyyy HH:mm:ss"),
                    t167TglReff: it.goodsReceive.t167TglReff?.format(dateFormat),
                    t167PetugasReceive: it.goodsReceive.t167PetugasReceive,
                    t171TglJamClaim: params.t171TglJamClaim,
                    t171ID: params.t171ID,

            ]

        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSubSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = Claim.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            goodsReceive{
                ilike("t167NoReff", params.t167NoReff )
            }
            ilike("t171ID", params.t171ID)
            Date date41 = df.parse(params.t171TglJamClaim)

            if (params.t171TglJamClaim) {
                ge("t171TglJamClaim", date41)
                lt("t171TglJamClaim", date41 + 1)
            }
        }

        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    kodePart: it.goods?.m111ID,
                    namaPart: it.goods?.m111Nama,
                    noPO: GoodsReceiveDetail.findByGoodsAndInvoice(it.goods,it.invoice)?.poDetail.po?.t164NoPO,
                    tanggalPO: it.invoice?.po?.t164TglPO?.format(dateFormat),
                    tipeOrder: PODetail.findByPo(it.invoice?.po)?.validasiOrder?.partTipeOrder?.m112TipeOrder,
                    t171Qty1Reff: it.t171Qty1Reff,
                    t171Qty1Issued: it.t171Qty1Issued,
                    t171Qty1Rusak: it.t171Qty1Rusak,
                    t171Qty1Salah: it.t171Qty1Salah,
                    selisih: it.t171Qty1Reff - it.t171Qty1Issued,
                    t171ID : params.t171ID


            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Claim", params.id]]
            return result
        }

        result.claimInstance = Claim.get(params.id)

        if (!result.claimInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def id = Claim.findByT171ID(params.id)
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Claim", id.id]]
            return result
        }

        result.claimInstance = Claim.get(id.id)

        if (!result.claimInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Claim", params.id]]
            return result
        }

        result.claimInstance = Claim.get(params.id)

        if (!result.claimInstance)
            return fail(code: "default.not.found.message")

        try {
            result.claimInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        Claim.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.claimInstance && m.field)
                    result.claimInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["Claim", params.id]]
                return result
            }

            result.claimInstance = Claim.get(params.id)

            if (!result.claimInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.claimInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            //result.claimInstance.properties = params
            result.claimInstance?.t171Qty1Issued  = Double.parseDouble(params.t171Qty1Issued)
            result.claimInstance?.t171Qty1Reff  = Double.parseDouble(params.t171Qty1Reff)
            result.claimInstance?.t171Qty1Rusak  = Double.parseDouble(params.t171Qty1Rusak)
            result.claimInstance?.t171Qty1Salah  = Double.parseDouble(params.t171Qty1Salah)
            result.claimInstance?.datatablesUtilService  = datatablesUtilService.syncTime()

            if (result.claimInstance.hasErrors() || !result.claimInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def massDelete(params){
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            String data = it
            def dt = null
            def gr = null
            def claim = null
            if(data.contains('.')){
                dt = data.split("\\.")
                try{
                    claim = Claim.findById(dt[1].trim())
                }catch(Exception a){
                    gr = GoodsReceive.findByT167NoReff(dt[1].trim())
                }
            }



            if(gr){
                Claim.findAllByGoodsReceiveAndT171ID(gr,dt[0].trim()).each{
                    it?.delete(flush: true)
                }
            }else if(claim){
                Claim?.get(dt[1].trim())?.delete(flush: true)
            }else{
                try{
                    Claim.findAllByT171ID(it).each{
                        it?.delete(flush: true)
                    }
                }catch(Exception a){

                }

            }
        }

    }
    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Claim", params.id]]
            return result
        }

        result.claimInstance = new Claim()
        result.claimInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.claimInstance && m.field)
                result.claimInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["Claim", params.id]]
            return result
        }

        result.claimInstance = new Claim(params)

        if (result.claimInstance.hasErrors() || !result.claimInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def claim = Claim.findById(params.id)
        if (claim) {
            claim."${params.name}" = params.value
            claim.save()
            if (claim.hasErrors()) {
                throw new Exception("${claim.errors}")
            }
        } else {
            throw new Exception("Claim not found")
        }
    }
}