package com.kombos.reception

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.maintable.Settlement
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

import java.text.DateFormat
import java.text.SimpleDateFormat

class GatePassT800Controller {

	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	def gatePassT800Service

    def jasperService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	static viewPermissions = ['index', 'list', 'datatablesList']

	static addPermissions = ['create', 'save']

	static editPermissions = ['edit', 'update']

	static deletePermissions = ['delete']

	def index() {
		redirect(action: "list", params: params)
        params.companyDealer = session.userCompanyDealer
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params
        params.companyDealer = session?.userCompanyDealer
		render gatePassT800Service.datatablesList(params) as JSON
	}

	def create() {
		def result = gatePassT800Service.create(params)

        if(!result.error)
            return [gatePassT800Instance: result.gatePassT800Instance]
        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
		def result = gatePassT800Service.save(params)
        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["GatePassT800", result.gatePassT800Instance.id])
            redirect(action:'show', id: result.gatePassT800Instance.id)
            return
        }

        render(view:'create', model:[gatePassT800Instance: result.gatePassT800Instance])
	}

	def show(Long id) {
		def result = gatePassT800Service.show(params)


        if(!result.error)
			return [ gatePassT800Instance: result.gatePassT800Instance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = gatePassT800Service.show(params)

        if(!result.error)
			return [ gatePassT800Instance: result.gatePassT800Instance]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = gatePassT800Service.update(params)
        if(result.ada=="ada"){
            flash.message = "Data Gatepass untuk nomor wo dengan nomor polisi tersebut sudah ada"
            render(view:'create', model:[gatePassT800Instance: result.instance])
            return
        }

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["GatePassT800", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[gatePassT800Instance: result.gatePassT800Instance.attach()])
	}

	def delete() {
		def result = gatePassT800Service.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["GatePassT800", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(GatePassT800, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def cekData(){
        params.companyDealer = session?.userCompanyDealer
        def result = gatePassT800Service.findNoPol(params);
        if(result!=null){
            render result as JSON
        }

    }

    def printGatePass(){
        def jsonArray = JSON.parse(params.idGatepass)
        def gatePassList = []
        def params = [:]

        params.gambar = grailsApplication.mainContext.servletContext.getRealPath("/images/Hasjrat_logo.png")

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            gatePassList << it
        }

        gatePassList.each {

            def reportData = calculateReportData(it)

            def reportDef = new JasperReportDef(name:'gatePassT800a.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData,parameters: params
            )

            reportDefList.add(reportDef)
        }

        def file = File.createTempFile("GatePass_",".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }


    def calculateReportData(def id){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();

        def results = GatePassT800.findAllById(id)
        results.sort {
            it.reception.t401NoWO
        }

        int count = 0
        double sumQty = 0
        def warna = "-"
        results.each {
            def data = [:]
  
            count = count + 1
            data.put("dealer",it?.companyDealer?.m011NamaWorkshop)
            data.put("nomor",": " + it?.t800ID)
            data.put("tanggal",it?.t800TglJamGatePass ? ": " + it?.t800TglJamGatePass?.format("dd-MM-yyyy | HH:mm") :"")
            data.put("nama",": " + it?.reception?.historyCustomer?.fullNama)
            data.put("nopol",": " + it?.reception?.historyCustomerVehicle?.fullNoPol)
            data.put("model",": " + it?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel)
            try {
                warna = it?.reception?.historyCustomerVehicle?.warna?.m092NamaWarna
            }catch (Exception e){

            }
            data.put("warna",": " + warna)
            data.put("tglMasuk",": " +it?.reception?.customerIn?.t400TglCetakNoAntrian?.format("dd-MM-yyyy | HH:mm"))
            data.put("catatan",it?.t800xKet ?  ": " + it?.t800xKet : ": -" )
            data.put("tglKeluar",": "+ it?.t800TglJamGatePass ? it?.t800TglJamGatePass?.format("dd-MM-yyyy | HH:mm"):"")
            data.put("judul",it?.gatePass?.m800NamaGatePass)
            if(it?.t800xNamaUser==null){
                def settle = Settlement?.findByReceptionAndStaDel(it?.reception,"0")
                data.put("sa",it?.reception?.t401NamaSA ? User?.findByUsernameAndStaDel(it?.reception?.t401NamaSA,"0")?.fullname : "-")
                data.put("pelanggan",it?.reception?.historyCustomer?.fullNama)
                data.put("security",it?.reception?.customerIn?.createdBy ? User?.findByUsernameAndStaDel(it?.reception?.customerIn?.createdBy,"0")?.fullname : "-")
                data.put("kasir",settle ? User.findByUsernameAndStaDel(settle?.createdBy,"0")?.fullname : "-")

                data.put("judul",it?.gatePass?.m800NamaGatePass)
                data.put("jSa","Service Advisor")
                data.put("jPelanggan","Pelanggan")
                data.put("jSecurity","Security")
                data.put("jKasir","Kasir")
            }else{
                data.put("sa","")
                data.put("pelanggan","")
                data.put("security","")
                data.put("kasir","")

                data.put("judul",it?.gatePass?.m800NamaGatePass)
                data.put("jSa","")
                data.put("jPelanggan","")
                data.put("jSecurity","")
                data.put("jKasir","")
            }


            reportData.add(data)
        }

        return reportData
    }
}
