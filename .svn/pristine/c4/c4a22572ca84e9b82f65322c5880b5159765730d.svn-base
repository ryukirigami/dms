package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class TransactionTypeController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def transactionTypeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render transactionTypeService.datatablesList(params) as JSON
    }

    def create() {
        def result = transactionTypeService.create(params)

        if (!result.error)
            return [transactionTypeInstance: result.transactionTypeInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }
    def save() {
        params.staDel = "0"
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = transactionTypeService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["TransactionType", result.transactionTypeInstance.id])
            redirect(action: 'show', id: result.transactionTypeInstance.id)
            return
        }

        render(view: 'create', model: [transactionTypeInstance: result.transactionTypeInstance])
    }

    def show(Long id) {
        def result = transactionTypeService.show(params)

        if (!result.error)
            return [transactionTypeInstance: result.transactionTypeInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = transactionTypeService.show(params)

        if (!result.error)
            return [transactionTypeInstance: result.transactionTypeInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdProcess = "UPDATE"
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = transactionTypeService.update(params)
        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["TransactionType", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [transactionTypeInstance: result.transactionTypeInstance.attach()])
    }

    def delete() {
        def result = transactionTypeService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["TransactionType", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(TransactionType, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
