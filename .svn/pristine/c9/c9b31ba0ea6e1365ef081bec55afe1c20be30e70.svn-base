package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class KegiatanApprovalController {

	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

    def generateCodeService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	static viewPermissions = ['index', 'list', 'datatablesList']

	static addPermissions = ['create', 'save']

	static editPermissions = ['edit', 'update']

	static deletePermissions = ['delete']

	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
        def staRole = "bukan"
        def user = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
        def c= User.createCriteria()
        def result = c.list {
            eq("username",org.apache.shiro.SecurityUtils.subject.principal.toString(),[ignoreCase: true])
            roles{
                ilike("authority","%"+"admin"+"%")
            }
        }
        if(result){
            staRole = "admin"
        }
        [staRole : staRole]
	}

	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		session.exportParams=params

		def c = KegiatanApproval.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
			if(params."sCriteria_m770IdApproval"){
                ilike("m770IdApproval","%" + (params."sCriteria_m770IdApproval" as String) + "%")
			}

			if(params."sCriteria_m770KegiatanApproval"){
				ilike("m770KegiatanApproval","%" + (params."sCriteria_m770KegiatanApproval" as String) + "%")
			}


			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}

		def rows = []

		results.each {
			rows << [

						id: it.id,

						m770IdApproval: it.m770IdApproval,

						m770KegiatanApproval: it.m770KegiatanApproval,

						m770NamaTabel: it.m770NamaTabel,

						m770NamaFk: it.m770NamaFk,

						m770NamaFieldDiupdate: it.m770NamaFieldDiupdate,

						m770NamaFormDetail: it.m770NamaFormDetail,

						m770StaButuhNilai: it.m770StaButuhNilai,

						staDel: it.staDel,

						createdBy: it.createdBy,

						updatedBy: it.updatedBy,

						lastUpdProcess: it.lastUpdProcess,

			]
		}

		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

		render ret as JSON
	}

	def create() {
		[kegiatanApprovalInstance: new KegiatanApproval(params)]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def kegiatanApprovalInstance = new KegiatanApproval(params)
        kegiatanApprovalInstance?.setM770IdApproval(generateCodeService.codeGenerateSequence("M770_IDApproval",null))
		kegiatanApprovalInstance?.setStaDel("0")
        kegiatanApprovalInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        kegiatanApprovalInstance?.setLastUpdProcess("INSERT")
        def cek = KegiatanApproval.createCriteria().list {
            eq("staDel","0")
            eq("m770KegiatanApproval",params.m770KegiatanApproval.trim(), [ignoreCase: true])
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [kegiatanApprovalInstance: kegiatanApprovalInstance])
            return
        }
        if (!kegiatanApprovalInstance.save(flush: true)) {
			render(view: "create", model: [kegiatanApprovalInstance: kegiatanApprovalInstance])
			return
		}
        //menghapus code pada tabel urutbelumterpakai
        //generateCodeService.hapusCodeBelumTerpakai("M770_IDApproval",null,kegiatanApprovalInstance.m770IdApproval)

		flash.message = message(code: 'default.created.message', args: [message(code: 'kegiatanApproval.label', default: 'Kegiatan Approval'), ""])
		redirect(action: "show", id: kegiatanApprovalInstance.id)
	}

	def show(Long id) {
		def kegiatanApprovalInstance = KegiatanApproval.get(id)
		if (!kegiatanApprovalInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kegiatanApproval.label', default: 'Kegiatan Approval'), id])
			redirect(action: "list")
			return
		}

		[kegiatanApprovalInstance: kegiatanApprovalInstance]
	}

	def edit(Long id) {
		def kegiatanApprovalInstance = KegiatanApproval.get(id)
        if (!kegiatanApprovalInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kegiatanApproval.label', default: 'KegiatanApproval'), id])
			redirect(action: "list")
			return
		}

		[kegiatanApprovalInstance: kegiatanApprovalInstance]
	}

	def update(Long id, Long version) {
		def kegiatanApprovalInstance = KegiatanApproval.get(id)
        if (!kegiatanApprovalInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kegiatanApproval.label', default: 'KegiatanApproval'), id])
			redirect(action: "list")
			return
		}

        if (version != null) {
			if (kegiatanApprovalInstance.version > version) {

				kegiatanApprovalInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'kegiatanApproval.label', default: 'KegiatanApproval')] as Object[],
				"Another user has updated this KegiatanApproval while you were editing")
				render(view: "edit", model: [kegiatanApprovalInstance: kegiatanApprovalInstance])
				return
			}
		}
        def kegiatan = KegiatanApproval.createCriteria().list {
            eq("staDel","0")
            eq("m770KegiatanApproval",params.m770KegiatanApproval.trim(), [ignoreCase: true])
        }
        if(kegiatan){
            for(find in kegiatan){
                if(id!=find.id){
                    flash.message = "Data sudah ada"
                    render(view: "edit", model: [kegiatanApprovalInstance: kegiatanApprovalInstance])
                    return
                    break
                }
            }
        }
        kegiatanApprovalInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        kegiatanApprovalInstance?.setLastUpdProcess("UPDATE")
        params?.lastUpdated = datatablesUtilService?.syncTime()
        kegiatanApprovalInstance.properties = params

		if (!kegiatanApprovalInstance.save(flush: true)) {
			render(view: "edit", model: [kegiatanApprovalInstance: kegiatanApprovalInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'kegiatanApproval.label', default: 'KegiatanApproval'), kegiatanApprovalInstance.id])
		redirect(action: "show", id: kegiatanApprovalInstance.id)
	}

	def delete(Long id) {
		def kegiatanApprovalInstance = KegiatanApproval.get(id)
		if (!kegiatanApprovalInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kegiatanApproval.label', default: 'KegiatanApproval'), id])
			redirect(action: "list")
			return
		}

		try {
            kegiatanApprovalInstance?.setStaDel("1")
            kegiatanApprovalInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            kegiatanApprovalInstance?.setLastUpdProcess("DELETE")
            kegiatanApprovalInstance?.lastUpdated = datatablesUtilService?.syncTime()
            kegiatanApprovalInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'kegiatanApproval.label', default: 'KegiatanApproval'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'kegiatanApproval.label', default: 'KegiatanApproval'), id])
			redirect(action: "show", id: id)
		}
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(KegiatanApproval, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(KegiatanApproval, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}


}
