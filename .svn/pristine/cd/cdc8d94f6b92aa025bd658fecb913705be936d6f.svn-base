package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam

class JobSuggestionService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = JobSuggestion.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_customerVehicle") {
                eq("customerVehicle", params."sCriteria_customerVehicle")
            }

            if (params."sCriteria_t503ID") {
                eq("t503ID", params."sCriteria_t503ID")
            }

            if (params."sCriteria_t503KetJobSuggest") {
                ilike("t503KetJobSuggest", "%" + (params."sCriteria_t503KetJobSuggest" as String) + "%")
            }

            if (params."sCriteria_t503TglJobSuggest") {
                ge("t503TglJobSuggest", params."sCriteria_t503TglJobSuggest")
                lt("t503TglJobSuggest", params."sCriteria_t503TglJobSuggest" + 1)
            }

            if (params."sCriteria_t503staJob") {
                ilike("t503staJob", "%" + (params."sCriteria_t503staJob" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {

        def TempNamaCust=MappingCustVehicle.findByCustomerVehicle(it.customerVehicle, [sort : 'dateCreated',order : 'desc']);

            rows << [

                    id: it.id,

                    t503ID: it.t503ID,

                    customerVehicle: it.customerVehicle,

                    noPol: it.customerVehicle.getCurrentCondition()?it.customerVehicle.getCurrentCondition().fullNoPol:"",

                    t503KetJobSuggest: it.t503KetJobSuggest,

                    t503TglJobSuggest: it.t503TglJobSuggest ? it.t503TglJobSuggest.format(dateFormat) : "",

                    t503staJob: it.t503staJob,

                    namaCustomer: TempNamaCust.customer.histories.size()>0?TempNamaCust.customer.histories.last().fullNama:""

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["JobSuggestion", params.id]]
            return result
        }

        result.jobSuggestionInstance = JobSuggestion.get(params.id)

        if (!result.jobSuggestionInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["JobSuggestion", params.id]]
            return result
        }

        result.jobSuggestionInstance = JobSuggestion.get(params.id)

        if (!result.jobSuggestionInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["JobSuggestion", params.id]]
            return result
        }

        result.jobSuggestionInstance = JobSuggestion.get(params.id)

        if (!result.jobSuggestionInstance)
            return fail(code: "default.not.found.message")

        try {
            result.jobSuggestionInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        JobSuggestion.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.jobSuggestionInstance && m.field)
                    result.jobSuggestionInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["JobSuggestion", params.id]]
                return result
            }

            result.jobSuggestionInstance = JobSuggestion.get(params.id)

            if (!result.jobSuggestionInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.jobSuggestionInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.jobSuggestionInstance.properties = params

            if (result.jobSuggestionInstance.hasErrors() || !result.jobSuggestionInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["JobSuggestion", params.id]]
            return result
        }

        result.jobSuggestionInstance = new JobSuggestion()
        result.jobSuggestionInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.jobSuggestionInstance && m.field)
                result.jobSuggestionInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["JobSuggestion", params.id]]
            return result
        }

        result.jobSuggestionInstance = new JobSuggestion(params)
        result.jobSuggestionInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        result.jobSuggestionInstance.setLastUpdProcess("INSERT")
        result.jobSuggestionInstance.setStaDel("0")
        if (result.jobSuggestionInstance.hasErrors() || !result.jobSuggestionInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def jobSuggestion = JobSuggestion.findById(params.id)
        if (jobSuggestion) {
            jobSuggestion."${params.name}" = params.value
            jobSuggestion.save()
            if (jobSuggestion.hasErrors()) {
                throw new Exception("${jobSuggestion.errors}")
            }
        } else {
            throw new Exception("JobSuggestion not found")
        }
    }

}