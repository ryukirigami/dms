    package com.kombos.parts

import com.kombos.administrasi.GeneralParameter
import com.kombos.administrasi.KegiatanApproval
import com.kombos.administrasi.MappingCompanyRegion
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.ApprovalT770
import grails.converters.JSON
    import org.apache.commons.io.FileUtils
    import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
    import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

    import java.text.DateFormat
import java.text.SimpleDateFormat

class SupplySlipOwnController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def supplySlipOwnService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable : new Date().format("ddMMyyyymmss")]
    }

    def addParts() {

    }

    def formAddParts() {
        def aksi = params.aksi
        def supllySlip = new SupplySlipOwn()
        def ppn = 10
        def generalParam = GeneralParameter.findByCompanyDealerAndM000StaDel(session.userCompanyDealer,"0")
        if(generalParam?.m000Tax){
            ppn = generalParam.m000Tax
        }
        if(params.id){
            supllySlip = SupplySlipOwn.get(params.id.toLong())
        }
        [aksi : aksi , supplySlipOwnInstance: supllySlip , ppn : ppn]
    }

    def sublist() {
        [idTable : new Date().format("ddMMyyyymmss"),salesRetur : params.id]
    }

    def datatablesList() {
        session.exportParams = params
        params?.companyDealer = session?.userCompanyDealer
        render supplySlipOwnService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        session.exportParams = params
        params?.companyDealer = session?.userCompanyDealer
        render supplySlipOwnService.datatablesSubList(params) as JSON
    }

    def datatablesPartsList() {
        session.exportParams = params
        params?.companyDealer = session?.userCompanyDealer
        render supplySlipOwnService.datatablesPartList(params) as JSON
    }
    def generateNomorSO(){
        def result = [:]
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def currentDate = new Date().format("dd-MM-yyyy")
        def c = SupplySlipOwn.createCriteria()
        def results = c.list {
            ge("dateCreated",df.parse(currentDate))
            lt("dateCreated",df.parse(currentDate) + 1)
        }

        def m = results.size()?:0
        def reg = MappingCompanyRegion.findByCompanyDealer(session.userCompanyDealer).companyDealer.m011ID
        def data = reg+"."+new Date().format("yyMMdd").toString() + "." + String.format('SS%02d',m+1)
        result.value = data
        return result
    }
    def save() {
        def jsonArray = JSON.parse(params.ids)
        CategorySlip cs = CategorySlip.get(params?.categorySlips?.toLong())
        def supplySlip = new SupplySlipOwn(params)
        supplySlip?.ssNumber = generateNomorSO().value
        supplySlip?.companyDealer = session.userCompanyDealer
        supplySlip.categorySlip = cs
        supplySlip?.staApprove = "0"
        supplySlip?.ssDate = new Date().clearTime()
        supplySlip?.staDel = "0"
        supplySlip.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
        supplySlip.lastUpdProcess="INSERT"
        supplySlip?.dateCreated = datatablesUtilService?.syncTime()
        supplySlip?.lastUpdated = datatablesUtilService?.syncTime()
        supplySlip?.save(flush: true)
        supplySlip?.errors?.each {println it}

        int angka = 0;
        jsonArray.each {
            def cQty = (params."qty_${it}" as String).replace(',','.')
            angka++;
            def harga = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(Goods?.get(it as Long),session.userCompanyDealer,"0",[sort: "dateCreated", order: "desc"])
            def soDetail = new SupplySlipOwnDetail()
            soDetail?.supplySlipOwn = supplySlip
            soDetail?.goods = Goods?.get(it as Long)
            soDetail?.qty = cQty as Double
            soDetail?.hargaSatuan = harga ? harga?.t150Harga : 0
            soDetail?.staDel = "0"
            soDetail.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
            soDetail.lastUpdProcess="INSERT"
            soDetail?.dateCreated = datatablesUtilService?.syncTime()
            soDetail?.lastUpdated = datatablesUtilService?.syncTime()
            soDetail?.save(flush: true)
        }

        def approval = new ApprovalT770(
                t770FK:supplySlip.id,
                companyDealer: session?.userCompanyDealer,
                kegiatanApproval: KegiatanApproval.findByM770KegiatanApprovalAndStaDel(KegiatanApproval.PARTS_USED_OWN,"0"),
                t770NoDokumen: supplySlip?.ssNumber,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.keterangan,
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        )
        approval.save(flush: true)

        render "OK"
    }

    def update() {
        def jsonArray = JSON.parse(params.ids)
        def jsonArrayDel = JSON.parse(params.idsDel)

        def supplySlip = SupplySlipOwn.get(params.idSO.toLong())
        supplySlip.properties = params
        supplySlip?.categorySlip = CategorySlip.get(params?.categorySlips?.toLong())
        supplySlip.updatedBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
        supplySlip.lastUpdProcess="UPDATE"
        supplySlip?.lastUpdated = datatablesUtilService?.syncTime()
        supplySlip?.save(flush: true)


        jsonArrayDel.each {
            def soDetail = SupplySlipOwnDetail.findBySupplySlipOwnAndGoodsAndStaDel(supplySlip,Goods?.get(it.toLong()),"0")
            if(soDetail){
                soDetail.delete()
            }
        }

        jsonArray.each {
            def cQty = (params."qty_${it}" as String).replace(',','.')
            def soDetail = new SupplySlipOwnDetail()
            def fSoDetail = SupplySlipOwnDetail.findBySupplySlipOwnAndGoodsAndStaDel(supplySlip,Goods?.get(it.toLong()),"0" )
            if(fSoDetail){
                soDetail = fSoDetail
            }else{
                soDetail.dateCreated = datatablesUtilService?.syncTime()
            }
            soDetail.lastUpdated = datatablesUtilService?.syncTime()
            soDetail?.supplySlipOwn = supplySlip
            soDetail?.goods = Goods?.get(it.toLong())
            soDetail?.qty = cQty as Double
            soDetail.updatedBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
            soDetail.lastUpdProcess="UPDATE"
            soDetail?.save(flush: true)
        }

        render "OK"
    }

    def massdelete() {
        params.companyDealer = session?.userCompanyDealer
        def hasil = supplySlipOwnService.massDelete(params)
        render hasil
    }

    def listCustomer(){
        def res = [:]
        def opts = []
        def customers = CustomerSales.findAllByStaDel("0")
        customers.each {
            opts<<it.nama
        }
        res."options" = opts
        render res as JSON
    }
    def tableDetailData(){
        def result = []
        def hasil = SupplySlipOwnDetail.createCriteria().list() {
            eq("staDel","0")
            supplySlipOwn{
                eq("id",params.id.toLong())
            }
        }
        def total = 0
        hasil.each {
            result << [
                    id : it?.goods?.id,
                    m111IDAdd : it?.goods?.m111ID,
                    m111NamaAdd : it?.goods?.m111Nama,
                    qtyAdd : it?.qty,
                    satuanAdd : it?.goods?.satuan.m118Satuan1,
            ]
        }
        render result as JSON
    }

    def printSupplySlipOwn(){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def jsonArray = JSON.parse(params.idSupplySlipOwn)
        def poList = []

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            println it
            poList << SupplySlipOwn.findByIdAndStaDel(it,"0")
        }

        poList.each {
            def reportData = SupplySlipReportData(it?.id)
            def reportDef = new JasperReportDef(name: 'supplySlip2.jasper',
                fileFormat: JasperExportFormat.PDF_FORMAT,
                reportData: reportData
            )
        reportDefList.add(reportDef)
        }

        def pdf = File.createTempFile("SUPPLYSLIP_",".pdf")

        pdf.deleteOnExit()
        FileUtils.writeByteArrayToFile(pdf, jasperService.generateReport(reportDefList).toByteArray())
//        JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}")
        response.outputStream << pdf.newInputStream()
    }

    def SupplySlipReportData(def id){
//        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def results = SupplySlipOwn.createCriteria().list {
            eq("staDel","0")
            eq("id", id)
        }
        def count = 0
        def total = 0
        def grandtotal = 0
        def partsItem = []
        def supplyDetail = SupplySlipOwnDetail.findAllBySupplySlipOwnAndStaDel(results[0], '0')

//        def supplyDetail = SupplySlipOwnDetail.createCriteria().list{
//            eq("staDel", "0")
//            eq("supplySlipOwn", id)
//        }
        supplyDetail.each {
            count++
            total = ((it?.hargaSatuan ? it?.hargaSatuan : 0)*(it?.qty ? it?.qty : 0))-(((it?.hargaSatuan ? it?.hargaSatuan : 0)*(it?.qty ? it?.qty : 0))*((it?.discount ? it?.discount : 0)/100))
            grandtotal += total
            partsItem << [
                    no : count,
                    partsNo : it?.goods?.m111ID,
                    partsName: it?.goods?.m111Nama,
                    partsQty : it?.qty ? it?.qty : 0,
                    partsUnitPrice : it?.hargaSatuan ? it?.hargaSatuan : 0,
                    partsDisc : it?.discount ? it?.discount : 0,
                    partsExtdAmount : total,
                    partsRemark : "-"
//                    partsRemark : it?.t703Remark ? it?.t703Remark : ""
            ]
        }

        if(results.size()>0){
            String tek = ""
            results.each { it ->
                def data = [:]

                data.put("slipNo", it?.ssNumber)
                data.put("slipTime", it?.ssDate.format("hh:mm"))
                data.put("slipDate", it?.ssDate.format("dd-MM-yyyy"))
                data.put("noWO", "")
                data.put("customerAddress", "")
                data.put("noPolisi", "")
                data.put("customerName", "")
                data.put("partsItem", partsItem)
                data.put("grandTot", grandtotal)
                reportData.add(data)
            }
        }else{
            def data = [:]
            data.put("noWO", "")
            reportData.add(data)
        }
        return reportData
    }
}