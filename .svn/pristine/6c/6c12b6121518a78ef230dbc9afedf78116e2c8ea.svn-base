package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class KelompokNPBController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def kelompokNPBService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render kelompokNPBService.datatablesList(params) as JSON
	}

	def create() {
		def result = kelompokNPBService.create(params)

        if(!result.error)
            return [kelompokNPBInstance: result.kelompokNPBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def result = kelompokNPBService.save(params)

        if(result.equals('Data has been used.')){
            result = [:]
            result.kelompokNPBInstance = new KelompokNPB(params)
            flash.message = message(code: 'default.created.kelompokNPB.error.message', default: 'Data has been used.')
        } else if(!result?.error) {
            flash.message = g.message(code: "default.created.message", args: ["KelompokNPB", result.kelompokNPBInstance.id])
            redirect(action:'show', id: result.kelompokNPBInstance.id)
            return
        }



        render(view:'create', model:[kelompokNPBInstance: result.kelompokNPBInstance])
	}

	def show(Long id) {
		def result = kelompokNPBService.show(params)

		if(!result.error)
			return [ kelompokNPBInstance: result.kelompokNPBInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = kelompokNPBService.show(params)

		if(!result.error)
			return [ kelompokNPBInstance: result.kelompokNPBInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def result = kelompokNPBService.update(params)

        if(result.equals('Data has been used.')){
            result = [:]
            result.kelompokNPBInstance = KelompokNPB.get(id)
            flash.message = message(code: 'default.created.kelompokNPB.error.message', default: 'Data has been used.')
        } else if(!result?.error) {
            flash.message = g.message(code: "default.updated.message", args: ["KelompokNPB", params.id])
            redirect(action:'show', id: params.id)
            return
        } else if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[kelompokNPBInstance: result.kelompokNPBInstance.attach()])
	}

	def delete() {
		def result = kelompokNPBService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["KelompokNPB", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(KelompokNPB, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
