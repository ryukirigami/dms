package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Vendor
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class DefaultVendorSubletController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = DefaultVendorSublet.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_section") {

                eq("section",Section.findByM051NamaSectionIlike("%"+params."sCriteria_section"+"%"))
            }

            if (params."sCriteria_serial") {

                eq("serial",Serial.findByM052NamaSerialIlike("%"+params."sCriteria_serial"+"%"))
            }

            if (params."sCriteria_operation") {

                eq("operation",Operation.findByM053NamaOperationIlike("%"+params."sCriteria_operation"+"%"))
            }

            if (params."sCriteria_vendor") {
                //eq("vendor", params."sCriteria_vendor")
                eq("vendor",Vendor.findByM121NamaIlike("%"+params."sCriteria_vendor"+"%"))
            }



            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    section: it.section.m051NamaSection,

                    serial: it.serial.m052NamaSerial,

                    operation: it.operation.m053NamaOperation,

                    vendor: it.vendor.m121Nama,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [defaultVendorSubletInstance: new DefaultVendorSublet(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def defaultVendorSubletInstance = new DefaultVendorSublet(params)
        defaultVendorSubletInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        defaultVendorSubletInstance?.lastUpdProcess = "INSERT"
        def cek = DefaultVendorSublet.createCriteria().list() {
            and{
                eq("section",defaultVendorSubletInstance.section)
                eq("serial",defaultVendorSubletInstance.serial)
                eq("operation",defaultVendorSubletInstance.operation)
                eq("vendor",defaultVendorSubletInstance.vendor)

            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [defaultVendorSubletInstance: defaultVendorSubletInstance])
            return
        }
        if (!defaultVendorSubletInstance.save(flush: true)) {
            render(view: "create", model: [defaultVendorSubletInstance: defaultVendorSubletInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'defaultVendorSublet.label', default: 'Default Vendor Job Sublet'), defaultVendorSubletInstance.id])
        redirect(action: "show", id: defaultVendorSubletInstance.id)
    }

    def show(Long id) {
        def defaultVendorSubletInstance = DefaultVendorSublet.get(id)
        if (!defaultVendorSubletInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'defaultVendorSublet.label', default: 'DefaultVendorSublet'), id])
            redirect(action: "list")
            return
        }

        [defaultVendorSubletInstance: defaultVendorSubletInstance]
    }

    def edit(Long id) {
        def defaultVendorSubletInstance = DefaultVendorSublet.get(id)
        if (!defaultVendorSubletInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'defaultVendorSublet.label', default: 'DefaultVendorSublet'), id])
            redirect(action: "list")
            return
        }

        [defaultVendorSubletInstance: defaultVendorSubletInstance]
    }

    def update(Long id, Long version) {
        def defaultVendorSubletInstance = DefaultVendorSublet.get(id)
        if (!defaultVendorSubletInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'defaultVendorSublet.label', default: 'DefaultVendorSublet'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (defaultVendorSubletInstance.version > version) {

                defaultVendorSubletInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'defaultVendorSublet.label', default: 'DefaultVendorSublet')] as Object[],
                        "Another user has updated this DefaultVendorSublet while you were editing")
                render(view: "edit", model: [defaultVendorSubletInstance: defaultVendorSubletInstance])
                return
            }
        }

        def cek = DefaultVendorSublet.createCriteria()
        def result = cek.list() {
            and{
                eq("section",Section.findById(params.section?.id))
                eq("serial",Serial.findById(params.serial?.id))
                eq("operation",Operation.findById(params.operation?.id))
                eq("vendor",Vendor.findById(params.vendor?.id))
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [defaultVendorSubletInstance: defaultVendorSubletInstance])
            return
        }

        else{
            params?.lastUpdated = datatablesUtilService?.syncTime()
            defaultVendorSubletInstance.properties = params
            defaultVendorSubletInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            defaultVendorSubletInstance?.lastUpdProcess = "UPDATE"
            if (!defaultVendorSubletInstance.save(flush: true)) {
                render(view: "edit", model: [defaultVendorSubletInstance: defaultVendorSubletInstance])
                return
            }
        }


        flash.message = message(code: 'default.updated.message', args: [message(code: 'defaultVendorSublet.label', default: 'Default Vendor Job Sublet'), defaultVendorSubletInstance.id])
        redirect(action: "show", id: defaultVendorSubletInstance.id)
    }

    def delete(Long id) {
        def defaultVendorSubletInstance = DefaultVendorSublet.get(id)
        if (!defaultVendorSubletInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'defaultVendorSublet.label', default: 'DefaultVendorSublet'), id])
            redirect(action: "list")
            return
        }

        try {
            defaultVendorSubletInstance.delete(flush: true)
            defaultVendorSubletInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            defaultVendorSubletInstance?.lastUpdProcess = "UPDATE"
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'defaultVendorSublet.label', default: 'DefaultVendorSublet'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'defaultVendorSublet.label', default: 'DefaultVendorSublet'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(DefaultVendorSublet, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(DefaultVendorSublet, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
