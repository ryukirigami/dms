package com.kombos.finance

import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import com.kombos.maintable.InvoiceT701
import com.kombos.maintable.InvoicingGenerateInvoicesController
import com.kombos.parts.Konversi
import com.kombos.parts.StatusApproval
import grails.converters.JSON
import org.springframework.web.context.request.RequestContextHolder

class CashBalanceService implements AfterApprovalInterface {
    boolean transactional = false

    def datatablesUtilService
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    @Override
    def afterApproval(String fks, StatusApproval staApproval, Date tglApproveUnApprove, String alasanUnApprove, String namaUserApproveUnApprove) {
        if (staApproval == StatusApproval.APPROVED) {
            def invoice = InvoiceT701.get(fks?.toLong())
            if(invoice){
                invoice.t701JenisInv = "K"
                invoice.t701PrintKe = 1
                invoice.t701StaApproveMigrate = "0"
                invoice.save(flush: true)
                def jurnal = new InvoicingGenerateInvoicesController()
                jurnal.doJournalIvoiceKredit(invoice)
            }
        }else {
            def invoice = InvoiceT701.get(fks?.toLong())
            if(invoice){
                invoice?.t701StaApproveMigrate = "1"
                invoice?.save(flush: true)
            }
        }
    }

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = CashBalance.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            if (params."sCriteria_requiredDate") {
                ge("requiredDate", params."sCriteria_requiredDate")
                lt("requiredDate", params."sCriteria_requiredDate" + 1)
            }

            order("requiredDate","desc");
        }

        def rows = []
        def konversi = new Konversi()
        results.each {
            rows << [

                    id: it.id,
                    requiredDate: it.requiredDate ? it.requiredDate.format(dateFormat) : "",
                    cashBalance: konversi.toRupiah(it.cashBalance)

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesListInvoice(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        Date tgglCari = new Date().parse("dd/MM/yyyy",params?.tgglCari?.toString())
        def c = InvoiceT701.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("t701StaDel","0")
            eq("companyDealer",params?.companyDealer)
            eq("t701JenisInv","T",[ignoreCase:true])
            isNull("t701NoInv_Reff");
            or{
                isNull("t701StaApprovedReversal")
                eq("t701StaApprovedReversal","1")
            }
            gt("t701TotalBayarRp",0.toDouble())
            eq("t701StaSettlement","0")
//            ge("t701TglJamInvoice" , tgglCari.clearTime())
//            lt("t701TglJamInvoice" , tgglCari.clearTime()+1)
        }

        def rows = []
        def konversi = new Konversi()
        results.each {
            rows << [

                    id: it.id,
                    t701NoInv: it?.t701NoInv,
                    tglInvoice: it?.t701TglJamInvoice ? it?.t701TglJamInvoice?.format("dd/MM/yyyy | HH:mm") : "",
                    t401NoWO: it?.reception?.t401NoWO,
                    t401NamaSA: it?.reception?.t401NamaSA,
                    t701Customer: it?.t701Customer,
                    t701StaApproveMigrate: it?.t701StaApproveMigrate ? it?.t701StaApproveMigrate : "0",
                    t701TotalBayarRp: konversi?.toRupiah(it?.t701TotalBayarRp)
            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CashBalance", params.id]]
            return result
        }

        result.cashBalanceInstance = CashBalance.get(params.id)

        if (!result.cashBalanceInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CashBalance", params.id]]
            return result
        }

        result.cashBalanceInstance = CashBalance.get(params.id)

        if (!result.cashBalanceInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CashBalance", params.id]]
            return result
        }

        result.cashBalanceInstance = CashBalance.get(params.id)

        if (!result.cashBalanceInstance)
            return fail(code: "default.not.found.message")

        try {
            result.cashBalanceInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        CashBalance.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.cashBalanceInstance && m.field)
                    result.cashBalanceInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["CashBalance", params.id]]
                return result
            }

            result.cashBalanceInstance = CashBalance.get(params.id)

            if (!result.cashBalanceInstance){
                return fail(code: "default.not.found.message")
            }

            // Optimistic locking check.
            if (params.version) {
                if (result.cashBalanceInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }


            def cashBalance = CashBalance.get(params.id)
            cashBalance.cashBalance    = Float.parseFloat(params.cashBalance as String)
            cashBalance.createdBy      = org.apache.shiro.SecurityUtils.subject.principal.toString()
            cashBalance.lastUpdProcess = "INSERT"
            cashBalance.staDel         = "0"
            cashBalance.save(flush: true, failOnError: true)

            if (!cashBalance.hasErrors()) {
                def details = CashBalanceDetail.findAllByCashBalance(cashBalance);
                if(details.size()>0){
                    details*.delete()
                }
                def jsonArray = JSON.parse(params.details)
                jsonArray.each {
                    def pecahanUang = null
                    String angka = it.pecahanUang.toString().indexOf(",")>-1 ?it.pecahanUang.toString().replaceAll(",",""):it.pecahanUang.toString()
                    angka = angka.substring(0,angka.indexOf("."))
                    if(it.tipe.toString().equalsIgnoreCase("Kertas")){
                        pecahanUang = PecahanUang.findByNilaiAndTypeIlike(angka.toInteger(),"%Kertas%")
                    }else{
                        pecahanUang = PecahanUang.findByNilaiAndTypeIlike(angka.toInteger(),"%Logam%")
                    }
                    if (pecahanUang) {
                        def cekCBDetail = CashBalanceDetail.findByCashBalanceAndPecahanUangAndStaDel(cashBalance,pecahanUang,"0");
                        if(cekCBDetail){
                            cekCBDetail.lastUpdated = datatablesUtilService?.syncTime();
                            cekCBDetail.quantity = Integer.parseInt(it.quantity?it.quantity.toString():0.toString())
                            cekCBDetail.lastUpdProcess = "UPDATE"
                            cekCBDetail.save(flush: true,failOnError: true)
                            if (cekCBDetail.hasErrors()) {
                                result.result = cekCBDetail.errors
                            } else {
                                result.result = "SUCCESS"
                            }
                        }else {
                            def cbDetail = new CashBalanceDetail(
                                    pecahanUang: pecahanUang,
                                    cashBalance: cashBalance,
                                    quantity: Integer.parseInt(it.quantity?it.quantity.toString():0.toString()),
                                    createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                                    lastUpdProcess: "INSERT",
                                    staDel:"0",
                                    dateCreated: datatablesUtilService?.syncTime(),
                                    lastUpdated: datatablesUtilService?.syncTime()
                            ).save(flush: true, failOnError: true)
                            if (cbDetail.hasErrors()) {
                                result.result = cbDetail.errors
                            } else {
                                result.result = "SUCCESS"
                            }

                        }
                    }
                }
            } else {
                result.result = "error"
            }

            result.cashBalanceInstance = cashBalance

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CashBalance", params.id]]
            return result
        }

        result.cashBalanceInstance = new CashBalance()
        result.cashBalanceInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.cashBalanceInstance && m.field)
                result.cashBalanceInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["CashBalance", params.id]]
            return result
        }
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        Date dateSave = Date.parse("dd/MM/yyyy", params.requiredDate as String)

        def cashBalance = new CashBalance()
        cashBalance.companyDealer  = session.userCompanyDealer
        cashBalance.cashBalance    = Float.parseFloat(params.cashBalance as String)
        cashBalance.requiredDate   = dateSave
        cashBalance.createdBy      = org.apache.shiro.SecurityUtils.subject.principal.toString()
        cashBalance.lastUpdProcess = "INSERT"
        cashBalance.staDel         = "0"
        cashBalance?.dateCreated   = datatablesUtilService?.syncTime()
        cashBalance?.lastUpdated   = datatablesUtilService?.syncTime()
        cashBalance.save(flush: true, failOnError: true)
        if (!cashBalance.hasErrors()) {
            def jsonArray = JSON.parse(params.details)
            jsonArray.each {
                def pecahanUang = null
                String angka = it.pecahanUang.toString().indexOf(",")>-1 ?it.pecahanUang.toString().replaceAll(",",""):it.pecahanUang.toString()
                angka = angka.substring(0,angka.indexOf("."))
                if(it.tipe.toString().equalsIgnoreCase("Kertas")){
                    pecahanUang = PecahanUang.findByNilaiAndTypeIlike(angka.toInteger(),"%Kertas%")
                }else{
                    pecahanUang = PecahanUang.findByNilaiAndTypeIlike(angka.toInteger(),"%Logam%")
                }
                if (pecahanUang) {
                    def cbDetail = new CashBalanceDetail(
                        pecahanUang: pecahanUang,
                        cashBalance: cashBalance,
                        quantity: Integer.parseInt(it.quantity?it.quantity.toString():0.toString()),
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        dateCreated: datatablesUtilService?.syncTime(),
                        lastUpdated: datatablesUtilService?.syncTime(),
                        lastUpdProcess: "INSERT",
                        staDel:"0"
                    ).save(flush: true, failOnError: true)
                    if (cbDetail.hasErrors()) {
                        result.result = cbDetail.errors
                    } else {
                        result.result = "SUCCESS"
                    }
                }
            }
        } else {
            result.result = "error"
        }
        result.cashBalanceInstance = cashBalance

        // success
        return result
    }

    def updateField(def params) {
        def cashBalance = CashBalance.findById(params.id)
        if (cashBalance) {
            cashBalance."${params.name}" = params.value
            cashBalance.save()
            if (cashBalance.hasErrors()) {
                throw new Exception("${cashBalance.errors}")
            }
        } else {
            throw new Exception("CashBalance not found")
        }
    }

}