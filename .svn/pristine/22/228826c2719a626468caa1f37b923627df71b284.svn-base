package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.GeneralParameter
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.reception.Reception
import org.activiti.engine.impl.pvm.delegate.ActivityExecution

class BatchProcessMenginsertDataReminderService {

    def invitationService
    def sessionFactory
    boolean gormReady = false
    boolean locked = false

    def currentGeneralParameter(CompanyDealer companyDealer) {
        def generalParameter = GeneralParameter.findByCompanyDealer(companyDealer)
        if (!generalParameter) {
            generalParameter = new GeneralParameter()
            generalParameter.companyDealer = companyDealer
        }
        if (generalParameter?.m000FormatSMSService == null || generalParameter?.m000FormatSMSService?.trim()?.empty) {
            generalParameter?.m000FormatSMSService = GeneralParameter.defaultm000FormatSMSService
        }
        if (generalParameter?.m000FormatEmail == null || generalParameter?.m000FormatEmail?.trim()?.empty) {
            generalParameter?.m000FormatEmail = GeneralParameter.defaultm000FormatEmailService
        }
        return generalParameter
    }

    def berdasarkanTipeCustomerYangSudahPernahService() {
        def sbe = JenisReminder.findByM201NamaJenisReminder("SBE")
        def accurate = StatusReminder.findByM202NamaStatusReminder("ACCURATE")

        def chcv = HistoryCustomerVehicle.createCriteria()
        def results = chcv.list {
//            ne("countReception", 0)
        }

        results.each { HistoryCustomerVehicle hcv ->

            def reminder = Reminder.findByCustomerVehicle(hcv.customerVehicle)
            if (!reminder) {
                reminder = new Reminder()
                reminder.companyDealer = hcv.customerVehicle?.companyDealer
            }
            def generalParameter =  currentGeneralParameter(hcv.customerVehicle?.companyDealer)
            reminder.customerVehicle = hcv.customerVehicle
            reminder.m201ID = sbe
            reminder.m202ID = accurate
            def dataService = Reception.findAllByHistoryCustomerVehicleAndStaDelAndStaSave(hcv,"0","0")
            dataService.sort {
                it.dateCreated
            }
            int lastKm = 0
            Date lastDate = null
            def companyD = null
            if(dataService.size()>0){
                lastKm = dataService?.last()?.t401KmSaatIni
                lastDate = dataService?.last()?.t401TanggalWO
                companyD = dataService?.last()?.companyDealer
            }
            reminder.t201LastKM = lastKm
            reminder.t201LastServiceDate = lastDate
            reminder.companyDealer = companyD
            reminder.staDel = "0"

            //jatuh tempo sementara
            reminder.t201TglJatuhTempo = new Date()
            if (generalParameter?.m000StaAktifDM?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglDM = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusTglReminderDM?.toDouble()?.intValue()
            }
            if (generalParameter?.m000StaAktifEmail?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglEmail = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusReminderEmail?.toDouble()?.intValue()
            }
            if (generalParameter?.m000StaAktifSMS?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglSMS = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusReminderSMS?.toDouble()?.intValue()
            }
            if (generalParameter?.m000StaAktifCall?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglCall = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusReminderCall?.toDouble()?.intValue()
            }


            reminder.save(flush: true)

            def jenisInvitation = JenisInvitation.findByM204JenisInvitation("SMS")
            def invitation = Invitation.findByReminderAndJenisInvitation(reminder, jenisInvitation);
            if (!invitation) {
                invitation = new Invitation()
                invitation.reminder = reminder
                invitation.companyDealer = reminder?.companyDealer
            }

            invitation.jenisInvitation = jenisInvitation
            invitation.customerVehicle = hcv.customerVehicle
            invitation.t202TglNextReContact = new Date()
            invitation.t202KetReContact = "-"

            invitation.save(flush: true)

        }
    }


    def berdasarkanTipeCustomerServiceYangMemilikiSaranPerbaikan() {

        def gr = JenisReminder.findByM201NamaJenisReminder("GR")
        def accurate = StatusReminder.findByM202NamaStatusReminder("ACCURATE")

        def cjs = JobSuggestion.createCriteria()
        def results = cjs.list {
            eq("staDel", "0")
        }

        results.each { JobSuggestion js ->

            def reminder = Reminder.findByCustomerVehicle(js.customerVehicle)
            if (!reminder) {
                reminder = new Reminder()
//                reminder.companyDealer = js.customerVehicle?.companyDealer
            }
            def generalParameter =  currentGeneralParameter(js.customerVehicle?.companyDealer)
            reminder.customerVehicle = js.customerVehicle
            reminder.m201ID = gr
            reminder.m202ID = accurate

            def dataService = Reception.findAllByHistoryCustomerVehicleAndStaDelAndStaSave(js?.customerVehicle?.getCurrentCondition(),"0","0")
            dataService.sort {
                it.dateCreated
            }
            int lastKm = 0
            Date lastDate = null
            def companyD = null
            if(dataService.size()>0){
                lastKm = dataService?.last()?.t401KmSaatIni
                lastDate = dataService?.last()?.t401TanggalWO
                companyD = dataService?.last()?.companyDealer
            }
            reminder.companyDealer = CompanyDealer.findByIdAndStaDel(companyD as long, "0")
            reminder.t201LastKM = lastKm
            reminder.t201LastServiceDate = lastDate

            //jatuh tempo sementara
            reminder.t201TglJatuhTempo = new Date()
            if (generalParameter?.m000StaAktifDM?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglDM = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusTglReminderDM?.toDouble()?.intValue()
            }
            if (generalParameter?.m000StaAktifEmail?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglEmail = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusReminderEmail?.toDouble()?.intValue()
            }
            if (generalParameter?.m000StaAktifSMS?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglSMS = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusReminderSMS?.toDouble()?.intValue()
            }
            if (generalParameter?.m000StaAktifCall?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglCall = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusReminderCall?.toDouble()?.intValue()
            }


            reminder.save(flush: true)

            def jenisInvitation = JenisInvitation.findByM204JenisInvitation("SMS")
            def invitation = Invitation.findByReminderAndJenisInvitation(reminder, jenisInvitation);
            if (!invitation) {
                invitation = new Invitation()
                invitation.reminder = reminder
                invitation.companyDealer = reminder?.companyDealer
            }

            invitation.jenisInvitation = jenisInvitation
            invitation.customerVehicle = js.customerVehicle
            invitation.t202TglNextReContact = new Date()
            invitation.t202KetReContact = "-"

            invitation.save(flush: true)

        }
    }

    def berdasarkanTipeCustomerYangMengalamiFieldAction() {
        def jr = JenisReminder.findByM201NamaJenisReminder("FA")
        def accurate = StatusReminder.findByM202NamaStatusReminder("ACCURATE")

        def cfa = VehicleFA.createCriteria()
        def results = cfa.list {
//            eq("sta", 0)
        }


        results.each { VehicleFA fa ->

            def reminder = Reminder.findByCustomerVehicle(fa.customerVehicle)
            if (!reminder) {
                reminder = new Reminder()
                reminder.companyDealer = fa.companyDealer
            }
            def generalParameter =  currentGeneralParameter(fa.customerVehicle?.companyDealer)
            reminder.customerVehicle = fa.customerVehicle
            reminder.m201ID = jr
            reminder.m202ID = accurate

            def dataService = Reception.findAllByHistoryCustomerVehicleAndStaDelAndStaSave(fa?.historyCustomerVehicle,"0","0")
            dataService.sort {
                it.dateCreated
            }
            int lastKm = 0
            Date lastDate = null
            if(dataService.size()>0){
                lastKm = dataService?.last()?.t401KmSaatIni
                lastDate = dataService?.last()?.t401TanggalWO
            }
            reminder.t201LastKM = lastKm
            reminder.t201LastServiceDate = lastDate
            //jatuh tempo sementara
            reminder.t201TglJatuhTempo = new Date()
            if (generalParameter?.m000StaAktifDM?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglDM = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusTglReminderDM?.toDouble()?.intValue()
            }
            if (generalParameter?.m000StaAktifEmail?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglEmail = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusReminderEmail?.toDouble()?.intValue()
            }
            if (generalParameter?.m000StaAktifSMS?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglSMS = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusReminderSMS?.toDouble()?.intValue()
            }
            if (generalParameter?.m000StaAktifCall?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglCall = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusReminderCall?.toDouble()?.intValue()
            }


            reminder.save(flush: true)

            def jenisInvitation = JenisInvitation.findByM204JenisInvitation("SMS")
            def invitation = Invitation.findByReminderAndJenisInvitation(reminder, jenisInvitation);
            if (!invitation) {
                invitation = new Invitation()
                invitation.reminder = reminder
                invitation.companyDealer = reminder?.companyDealer
            }

            invitation.jenisInvitation = jenisInvitation
            invitation.customerVehicle = fa.customerVehicle
            invitation.t202TglNextReContact = new Date()
            invitation.t202KetReContact = "-"

            invitation.save(flush: true)

        }
    }

    def berdasarkanTipeCustomerBaru() {
        def sbi = JenisReminder.findByM201NamaJenisReminder("SBI")
        def accurate = StatusReminder.findByM202NamaStatusReminder("ACCURATE")

        def chcv = HistoryCustomerVehicle.createCriteria()
        def results = chcv.list {
            eq("countReception", 0)
        }


        results.each { HistoryCustomerVehicle hcv ->

            def reminder = Reminder.findByCustomerVehicle(hcv.customerVehicle)
            if (!reminder) {
                reminder = new Reminder()
                reminder.companyDealer = hcv.customerVehicle?.companyDealer
            }
            def generalParameter =  currentGeneralParameter(hcv.customerVehicle?.companyDealer)
            reminder.customerVehicle = hcv.customerVehicle
            reminder.m201ID = sbi
            reminder.m202ID = accurate

            //jatuh tempo sementara
            reminder.t201TglJatuhTempo = new Date()
            if (generalParameter?.m000StaAktifDM?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglDM = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusTglReminderDM?.toDouble()?.intValue()
            }
            if (generalParameter?.m000StaAktifEmail?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglEmail = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusReminderEmail?.toDouble()?.intValue()
            }
            if (generalParameter?.m000StaAktifSMS?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglSMS = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusReminderSMS?.toDouble()?.intValue()
            }
            if (generalParameter?.m000StaAktifCall?.trim()?.equalsIgnoreCase("1")) {
                reminder.t201TglCall = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusReminderCall?.toDouble()?.intValue()
            }


            reminder.save(flush: true)

            hcv.t183StaReminderSBI = "1"
            hcv.save(flush: true)

            def jenisInvitation = JenisInvitation.findByM204JenisInvitation("SMS")
            def invitation = Invitation.findByReminderAndJenisInvitation(reminder, jenisInvitation);
            if (!invitation) {
                invitation = new Invitation()
                invitation.reminder = reminder
                invitation.companyDealer = reminder?.companyDealer
            }

            invitation.jenisInvitation = jenisInvitation
            invitation.customerVehicle = hcv.customerVehicle
            invitation.t202TglNextReContact = new Date()
            invitation.t202KetReContact = "-"

            invitation.save(flush: true)
            invitation.errors.each {
                //println "INVITATION 4 "+it
            }

        }
    }

    def berdasarkanTipeCustomerYangTidakMelakukanServiceDalam12BulanTerakhir() {

        Calendar calendar = Calendar.getInstance()
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 12)
        Date end = calendar.getTime()
        def query = sessionFactory.currentSession.createSQLQuery("select max(id) from T401_RECEPTION where T401_TanggalWO <= :end and T401_STADEL = '0' and T401_STASAVE='0' group by T401_T183_ID");
        query.setDate("end", end);
        def ids = query.list();
        def history = []
        if (!ids.empty) {
            def idsLong = []
            ids.each {
                idsLong << it?.toString()?.toLong()
            }
            history = Reception.findAllByT401TanggalWOLessThanEqualsAndIdInListAndFlagInvitationIsNull(end, idsLong)
            def cp = JenisReminder.findByM201NamaJenisReminder("CP")
            def aktif = StatusReminder.findByM202NamaStatusReminder("ACCURATE")
            history.each { reception ->
                def generalParameter = invitationService.currentGeneralParameter(reception?.historyCustomerVehicle?.companyDealer)
                if (generalParameter?.m000StaAktifEmail?.trim()?.equalsIgnoreCase("1")) {
                    invitationService.editOrAddReminder(
                            JenisInvitation.findByM204JenisInvitation("Email"),
                            reception,
                            cp,
                            aktif,
                            generalParameter
                    )
                }
                if (generalParameter?.m000StaAktifSMS?.trim()?.equalsIgnoreCase("1")) {
                    invitationService.editOrAddReminder(
                            JenisInvitation.findByM204JenisInvitation("SMS"),
                            reception,
                            cp,
                            aktif,
                            generalParameter
                    )
                }
                if (generalParameter?.m000StaAktifDM?.trim()?.equalsIgnoreCase("1")) {
                    invitationService.editOrAddReminder(
                            JenisInvitation.findByM204JenisInvitation("Mail"),
                            reception,
                            cp,
                            aktif,
                            generalParameter
                    )
                }
                if (generalParameter?.m000StaAktifCall?.trim()?.equalsIgnoreCase("1")) {
                    invitationService.editOrAddReminder(
                            JenisInvitation.findByM204JenisInvitation("Phone Call"),
                            reception,
                            cp,
                            aktif,
                            generalParameter
                    )
                }
            }

        }
    }


    def invitationScheduler(ActivityExecution execution) throws Exception {
        if(gormReady && !locked) {
            locked = true
            def variables = execution.getVariables()
            log.info("invitationScheduler " + execution.getVariables())
            berdasarkanTipeCustomerBaru()
            berdasarkanTipeCustomerYangSudahPernahService()
            berdasarkanTipeCustomerServiceYangMemilikiSaranPerbaikan()
            berdasarkanTipeCustomerYangMengalamiFieldAction()
            berdasarkanTipeCustomerYangTidakMelakukanServiceDalam12BulanTerakhir()
            locked = false
        }
    }

}
