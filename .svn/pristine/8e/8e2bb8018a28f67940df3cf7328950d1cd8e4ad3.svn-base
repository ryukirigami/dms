package com.kombos.delivery

import com.kombos.administrasi.ManPower
import com.kombos.administrasi.MesinEdc
import com.kombos.baseapp.sec.shiro.User
import com.kombos.finance.*
import com.kombos.hrd.Jabatan
import com.kombos.hrd.Karyawan
import com.kombos.maintable.InvoiceT701
import com.kombos.maintable.MetodeBayar
import com.kombos.maintable.PartInv
import com.kombos.maintable.Settlement
import com.kombos.parts.Konversi
import com.kombos.reception.Reception
import com.kombos.utils.MoneyUtil
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class SettlementController {

    def deliveryService

    def invoiceTunaiJournalService

    def pemakaianPartsJournalService

    def HPPSubletJournalService

    def datatablesUtilService

    static allowedMethods = [save: "POST"]

    static addPermissions = ['save']

    static viewPermissions = ['index', 'settlementDataTablesList', 'searchData', 'getRate']

    def jasperService

    def index() {
        def bank = deliveryService.getBankList()
        def metodeBayar = deliveryService.getMetodeBayarList()
        def mesinEDC = deliveryService.getMesinEDCList()
        def konversi = new Konversi()
        [metodeBayar:metodeBayar, mesinEDC:mesinEDC,konversi : konversi]
    }

    def getRate() {
        def rate = deliveryService.getRate(params)
        render rate as JSON
    }

    def settlementDataTablesList() {
        def settlementList = Settlement.createCriteria().list {
            eq("staDel","0")
            invoice{
                if(params?.idInv){
                    eq("id",params?.idInv?.toLong())
                }else{
                    eq("id",-1000.toLong())
                }
            }
        }

        def dataToRender = [:]
        dataToRender.sEcho = params.sEcho
        dataToRender.aaData = []
        dataToRender.iTotalRecords = 0
        dataToRender.iTotalDisplayRecords = 0

        def rows = []
        def totalBayar = 0;
        def konversi = new Konversi()
        settlementList?.each {
            rows << [
                    tanggalBayar:it?.t704TglJamSettlement?.format("dd/MM/yyyy HH:mm"),
                    noInvoice:it?.invoice?.t701NoInv,
                    tipe:it?.invoice?.t701IntExt,
                    nomorWO:it?.reception?.t401NoWO,
                    customer:it?.reception?.historyCustomer?.fullNama,
                    metodeBayar:it?.metodeBayar?.m701MetodeBayar,
                    noWBS:it?.t704NoWBS,
                    namaBank:it?.bank?.m702NamaBank,
                    noKartu:it?.t704NoKartu,
                    bunga:it?.rateBank?.m703RateRp,
                    jumlah:konversi?.toRupiah(it?.t704JmlBayar),
                    totalBayar : totalBayar
            ]
        }
        dataToRender.aaData = rows
        dataToRender.iTotalRecords = rows.size()
        dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords

        render dataToRender as JSON
    }

    def searchData() {
        params?.company = session?.userCompanyDealer
        def result = deliveryService.searchSettlement(params)
        render result as JSON
    }

    def save(){
        def inv = new InvoiceT701()
        def recept = new Reception()
        if(params.metodeCari=="invoice"){
            inv = InvoiceT701.get(params?.idCari.toLong())
            recept = inv?.reception
        }else{
            recept = Reception.get(params?.idCari?.toLong())
            inv = InvoiceT701.findByReceptionAndT701StaDelAndT701JenisInvIlike(recept,"0","%T%")
        }

        try {
            def not = Reception.get(recept?.id)
            not?.isPending = "1"
            not?.save(flush: true)
        }catch (Exception e){

        }
        def cariSettlement = Settlement.findByReceptionAndInvoiceAndMetodeBayarAndStaDel(recept, inv, MetodeBayar.get(params.metodebayar.toLong()), "0")

            def settlement = new Settlement()
            settlement.companyDealer = session.userCompanyDealer
            settlement.metodeBayar = MetodeBayar.get(params.metodebayar.toLong())
//        settlement.t704TglJamSettlement = new Date()
            settlement.t704TglJamSettlement = datatablesUtilService?.syncTime()
            if(params.wbs){
                settlement.t704NoWBS = params.wbs
            }
            if(params.nokartu){
                settlement.t704NoKartu = params.nokartu
            }
//        settlement.t704NoReff
            if(params.bank){
                settlement.bank = BankAccountNumber?.get(params.bank.toLong())?.bank
            }
            if(params.mesin){
                settlement.mesinEdc = MesinEdc.get(params.mesin.toLong())
            }
//        settlement.rateBank
            if(params.stapph){
                settlement.t704StaPPh23 = "1"
            }
            if(params.pph){
                settlement.t704JmlPPh23=params.pph.toDouble()
            }
            if(params.stappn){
                settlement.t704StaPPN = "1"
            }
            if(params.ppn){
                settlement.t704JmlPPN = params.ppn.toDouble()
            }
            Double bayar = params.uang.toDouble()
            if(params?.jumKembali){
                bayar = bayar - params?.jumKembali?.toDouble()
            }
            settlement.t704JmlBayar = bayar
            if(params.buktitrf){
                settlement.t704NoBuktiTransfer = params.buktitrf
            }
            settlement.staDel = "0"
            settlement.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            settlement.t704xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
            settlement.lastUpdProcess = "INSERT"
            settlement?.setDateCreated(datatablesUtilService?.syncTime())
            settlement?.setLastUpdated(datatablesUtilService?.syncTime())
            settlement.reception = recept
            settlement.invoice = inv

        if((cariSettlement?.invoice?.id != inv?.id && cariSettlement?.reception?.id != recept?.id) && cariSettlement?.t704JmlBayar != params.uang.toDouble()){
            settlement.save(flush: true)
            render "ok"
        }else if((cariSettlement?.invoice?.id == inv?.id && cariSettlement?.reception?.id == recept?.id) && cariSettlement?.t704JmlBayar != inv?.t701TotalBayarRp){
            settlement.save(flush: true)
            render "ok"
        }else {
            render "nok"
        }
    }

    def jumlahSudahBayar(){
        def invoiceInstance = InvoiceT701.get(params.idInv.toLong());
        def sudahBayar = 0
        def settlements = Settlement.createCriteria().list {
            eq("staDel","0");
            eq("invoice",invoiceInstance);
        }
        settlements.each {
            sudahBayar+=it.t704JmlBayar
        }
        def konversi = new Konversi()
        def sSudahBayar = konversi.toRupiah(sudahBayar)
        render sSudahBayar
    }

    def printKW(){
        def konversi = new Konversi()
        def reportData =[]
        def invoice = InvoiceT701.get(params.id);

        if(!invoice.t701StaSettlement || (invoice?.t701StaSettlement && invoice?.t701StaSettlement!="1")){
            invoiceTunaiJournalService.createJournal(params)
            invoice?.t701StaSettlement = "1"
            invoice?.save(flush: true)
            def cekPart = PartInv.findAllByInvoiceAndT703StaDel(invoice,"0");
            params?.noWo = invoice?.reception?.t401NoWO
            params?.noInv = invoice?.t701NoInv
            params?.companyDealer = session?.userCompanyDealer
            if(cekPart?.size()>0){
                pemakaianPartsJournalService.createJournal(params)
            }
            if(invoice?.t701SubletRp && invoice?.t701SubletRp>0){
                params.totalBiaya = invoice?.t701SubletRp
                params.docNumber = invoice?.reception?.t401NoWO
                HPPSubletJournalService.createJournal(params)
            }
        }
        def user = User.findByUsernameAndStaDel(org?.apache?.shiro?.SecurityUtils?.subject?.principal?.toString(),"0")
        def moneyUtil = new MoneyUtil()
        def kabeng = Karyawan?.findByJabatanAndBranchAndStaDel(ManPower?.findByM014JabatanManPowerIlikeAndStaDel("%"+"KEPALA BENGKEL"+"%","0"),session?.userCompanyDealer, "0")
        reportData <<[
                lblTambahan : "Nomor WO :",
                noTambahan  : invoice?.reception?.t401NoWO,
                jenis       : "(SERVICE)",
                no          : "",
                pembayar    : invoice?.t701Customer,
                terbilang   : moneyUtil.convertMoneyToWords(invoice?.t701TotalBayarRp ? invoice?.t701TotalBayarRp: 0.toDouble()),
                tujuan      : "Pembayaran biaya service / "+invoice?.reception?.historyCustomerVehicle?.fullNoPol,
                jumlah      : konversi.toRupiah(invoice?.t701TotalBayarRp),
                tggl        : invoice?.companyDealer?.kabKota?.m002NamaKabKota+" , "+new Date().format("dd MMMM yyyy"),
                tgglPrint   : datatablesUtilService?.syncTime()?.format("dd/MM/yyyy HH:mm:ss"),
                kasir       : user?.fullname,
                kabeng      : kabeng ? kabeng?.nama : "-"
        ]
        List<JasperReportDef> reportDefList = []
        def reportDef = new JasperReportDef(name:'Kwitansi.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData
        )

        reportDefList.add(reportDef);

        def file = File.createTempFile("Kwitansi_",".pdf")

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setContentType("application/octet-stream")
        response.setHeader("Content-disposition", "attachment;filename=${file.getName()}")

        response.outputStream << file.newInputStream()
    }
}
