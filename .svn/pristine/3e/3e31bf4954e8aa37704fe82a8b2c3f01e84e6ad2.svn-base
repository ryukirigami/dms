app.client.website=http://www.kombos.com/
app.copyright=Copyright &copy; 2013. CV Kombos
app.title=Dealer Management System
app.version=Version 1.0
app.banner.title=Dealer Management System
app.banner.logged.in.as=Logged in as
app.banner.logout=logout
app.banner.logged.session.expired=Your session has expired or You already logged out from another browser tab.
app.banner.password_expiry=Your password will expire in {0} day(s)
app.banner.today_password_expiry=Your password will expire today, please change it

app.login.title=Login
app.login.username.label=Username
app.login.password.label=Password
app.login.oldpassword.label=Old Password
app.login.newpassword.label=New Password
app.login.confirmnewpassword.label=Confirm New Password
app.login.rememberme.label=Remember me
app.login.button.label=Login
app.login.button.changepassword.label=Change Password
app.login.default.invalid.message=Your authentication is no longer valid!
app.login.language=Language
app.login.language.en=English
app.login.language.in=Indonesian

app.menu.home=Home
app.menu.logout=Logout

app.settings.label=Application Settings
app.administrations.label=Administrations

login.failed=Authentication failed
login.failed.minimum_password_length=Minimum password length is {0}
login.failed.password_cycles=Your password has expired, please contact administrator
login.failed.password_history=You already use this password before, please choose new one
login.failed.confirm_new_password.not_match=New Password does not match the Confirm New Password
login.failed.old_password.incorrect=Incorrect Old Password
login.failed.user_locked=Your account is locked, please contact administrator
login.failed.exceed_maximum_inactivity_allowed=You exceed maximum days allowed for inactivity ({0} days)

security.denied.notfound=User not found
security.denied.title=Denied
security.denied.message=Sorry, you're not authorized to access this page.
security.errors.login.expired="Sorry, your account has expired."
security.errors.login.passwordExpired="Sorry, your password has expired."
security.errors.login.disabled="Sorry, your account is disabled."
security.errors.login.locked="Sorry, your account is locked."
security.errors.login.fail="Sorry, we were not able to find a user with that username and password."

spinner.alt=Loading&hellip;

default.doesnt.match.message=Property [{0}] of class [{1}] with value [{2}] does not match the required pattern [{3}]
default.invalid.url.message=Property [{0}] of class [{1}] with value [{2}] is not a valid URL
default.invalid.creditCard.message=Property [{0}] of class [{1}] with value [{2}] is not a valid credit card number
#default.invalid.email.message=Property [{0}] of class [{1}] with value [{2}] is not a valid e-mail address
default.invalid.email.message=Silahkan isi {0} dengan format e-mail yang benar
default.invalid.range.message=Property [{0}] of class [{1}] with value [{2}] does not fall within the valid range from [{3}] to [{4}]
default.invalid.size.message=Property [{0}] of class [{1}] with value [{2}] does not fall within the valid size range from [{3}] to [{4}]
default.invalid.max.message=Property [{0}] of class [{1}] with value [{2}] exceeds maximum value [{3}]
default.invalid.min.message=Property [{0}] of class [{1}] with value [{2}] is less than minimum value [{3}]
#default.invalid.max.size.message=Property [{0}] of class [{1}] with value [{2}] exceeds the maximum size of [{3}]
default.invalid.max.size.message=Inputan yang berwarna merah melebihi batas maksimal inputan.
default.invalid.min.size.message=Property [{0}] of class [{1}] with value [{2}] is less than the minimum size of [{3}]
default.invalid.validator.message=Property [{0}] of class [{1}] with value [{2}] does not pass custom validation
default.not.inlist.message=Property [{0}] of class [{1}] with value [{2}] is not contained within the list [{3}]
default.blank.message=Property [{0}] of class [{1}] cannot be blank
default.not.equal.message=Property [{0}] of class [{1}] with value [{2}] cannot equal [{3}]
#default.null.message=Property [{0}] of class [{1}] cannot be null
default.null.message=Silahkan isi {0} terlebih dahulu ya
default.not.unique.message=Property [{0}] of class [{1}] with value [{2}] must be unique

default.paginate.prev=Previous
default.paginate.next=Next
default.boolean.true=True
default.boolean.false=False
default.date.format=EEEEEEEE MMMMMMMM, dd yyyy
default.number.format=0

default.created.failed.message={0} with {1} and {2} cannot created
default.created.message={0} {1} created
user.created.message={0} {1} created with password : {2}
default.updated.message={0} {1} updated
default.deleted.message={0} {1} deleted
default.not.deleted.message={0} {1} could not be deleted
default.not.found.message={0} not found with id {1}
default.duplicate.primary.message=* Data Sudah Ada
default.optimistic.locking.failure=Another user has updated this {0} while you were editing

default.home.label=Home
default.list.label={0} List
default.add.label=Add {0}
default.new.label=New {0}
default.create.label=Create {0}
default.show.label=Show {0}
default.edit.label=Edit {0}

default.ok.label=OK
default.print.label=Print
default.preview.label=Preview
default.close.label=Close

default.button.create.label=Create
default.button.edit.label=Edit
default.button.update.label=Update
default.button.delete.label=Delete
default.button.delete.confirm.message=Anda yakin data akan dihapus?
default.button.save.confirm.message=Anda yakin data akan disimpan?
default.button.edit.confirm.message=Anda yakin data akan diubah?

# Data binding errors. Use "typeMismatch.$className.$propertyName to customize (eg typeMismatch.Book.author)
typeMismatch.java.net.URL=Property {0} must be a valid URL
typeMismatch.java.net.URI=Property {0} must be a valid URI
typeMismatch.java.util.Date=Property {0} must be a valid Date
typeMismatch.java.lang.Double=Property {0} must be a valid number
typeMismatch.java.lang.Integer=Property {0} must be a valid number
typeMismatch.java.lang.Long=Property {0} must be a valid number
typeMismatch.java.lang.Short=Property {0} must be a valid number
typeMismatch.java.math.BigDecimal=Property {0} must be a valid number
typeMismatch.java.math.BigInteger=Property {0} must be a valid number

#domain class label
app.user.label=User

# Welcome page
app.previous_successfull_logon.label = Your Previous Successfull Logon on {0}
app.previous_unsuccessfull_logon.label = Your Previous Unsuccessfull Logon on {0}

# Domain specific
app.commonCode.key.label=Key
app.commonCode.value.label=Value

app.today.label=Today is
app.yourmoney.label=Your money is

role.label=Role
app.role.name.label=Name
app.role.permissions.label=Permission
app.role.users.label=Users

app.userAdditionalColumnInfo.columnName.label=Column Name
app.userAdditionalColumnInfo.columnType.label=Column Type
app.userAdditionalColumnInfo.label.label=Label

#label for change password
app.changePassword.label=Change Password


#label for application setting parameter and security checklist
app.appSettingParam.label=Application Setting Parameter

app.appSettingParam.application_title.label=Application Title
app.appSettingParam.welcome_text.label=Welcome Text
app.appSettingParam.attachment_max_size.label=Attachment Maximum Size
app.appSettingParam.max_number_lines_displayed.label=Maximum Lines Displayed
app.appSettingParam.theme.label=Theme
app.appSettingParam.default_language.label=Default Language
app.appSettingParam.date_format.label=Date Format
app.appSettingParam.time_format.label=Time Format
app.appSettingParam.users_display_format.label=User Display Format

app.securityChecklist.minimum_password_length.label=Password Length
app.securityChecklist.password_history.label=Password History
app.securityChecklist.password_cycle.label=Password Cycle

app.securityChecklist.preventing_guessable_password.label=Preventing guessable password
app.securityChecklist.no_whitespace.label=No Whitespace
app.securityChecklist.no_alphabetical_sequence.label=No Alphabetical Sequence
app.securityChecklist.no_qwerty_sequence.label=No QWERTY Sequence
app.securityChecklist.numerical_sequence_rule.label=Numerical Character Sequence
app.securityChecklist.repeat_character_rule.label=Repeat Character
app.securityChecklist.digit_character_rule.label=Digit Character
app.securityChecklist.non_alphanumeric_character_rule.label=Non Alphanumeric Character
app.securityChecklist.upper_case_character_rule.label=Upper Case Character
app.securityChecklist.lower_case_character_rule.label=Lower Case Character

app.securityChecklist.disable_account_after_failed_login.label=Disable Account After Failed Log In
app.securityChecklist.relogin_after_change_password.label=Relogin After Change Password
app.securityChecklist.previous_successful_log_on.label=Previous Successful Log On
app.securityChecklist.previous_unsuccessful_log_on.label=Previous Unsuccessful Log On
app.securityChecklist.automatic_log_out.label=Automatic Log Out
app.securityChecklist.automatic_disable_user_profile_after_inactive.label=Automatic Disable User Profile

#description for application setting parameter and security checklist
app.appSettingParam.application_title.description=Application title will be shown at the browser's tab.
app.appSettingParam.welcome_text.description=Welcome text will be shown at the home page, once user has logged in.
app.appSettingParam.attachment_max_size.description=Attachment maximum size in Kilo Bytes (KB)
app.appSettingParam.max_number_lines_displayed.description=Maximum lines displayed in each grid of the function
app.appSettingParam.theme.description=Theme of the site layout
app.appSettingParam.default_language.description=Default language for site
app.appSettingParam.date_format.description=Date format will decide how date will be displayed
app.appSettingParam.time_format.description=Time format will decide how time will be displayed
app.appSettingParam.users_display_format.description=User display format will decide how user profile name will be displayed

app.securityChecklist.minimum_password_length.description=Minimum password length allowed
app.securityChecklist.password_history.description=Maximum number of password history stored after changing password
app.securityChecklist.password_cycle.description=Maximum number of days allowed for new password used by user

app.securityChecklist.preventing_guessable_password.description=Prevent user's password be guessable
app.securityChecklist.no_whitespace.description=Not allowing whitespace
app.securityChecklist.no_alphabetical_sequence.description=Not allowing alphabetical sequence
app.securityChecklist.no_qwerty_sequence.description=Not allowing QWERTY character sequence
app.securityChecklist.numerical_sequence_rule.description=Maximum number of numerical character sequence allowed(min: 3)
app.securityChecklist.repeat_character_rule.description=Maximum number of repeat character allowed(min: 3)
app.securityChecklist.digit_character_rule.description=Minimum digit character occurrence
app.securityChecklist.non_alphanumeric_character_rule.description=Minimum non alphanumeric character occurrence
app.securityChecklist.upper_case_character_rule.description=Minimum upper case character occurrence
app.securityChecklist.lower_case_character_rule.description=Minimum lowe case character occurrence

app.securityChecklist.disable_account_after_failed_login.description=Maximum number of failed log in allowed before disabling user
app.securityChecklist.relogin_after_change_password.description=System fill force user to re-login to use the new password
app.securityChecklist.previous_successful_log_on.description=System will show the successful log on date at the welcome page
app.securityChecklist.previous_unsuccessful_log_on.description=System will show the unsuccessful log on date at the welcome page
app.securityChecklist.automatic_log_out.description=Maximum number of minutes allowed for user inactivity before system automatically log out
app.securityChecklist.automatic_disable_user_profile_after_inactive.description=Maximum number of days allowed for user inactivity before system disabling the user

#user
app.user.username.label=Username
app.user.newpassword.label=New Password
app.user.remain_old_password.label=Leave this blank, the old password will remain
app.user.passwordHash.label=Password
app.user.roles.label=Role
app.user.status.label=Status
app.user.passwordExpiredOn.label=Password&nbsp;Expired&nbsp;On
app.user.lastLoggedIn.label=Last&nbsp;Logged&nbsp;In&nbsp;On
app.user.loggedInFrom.label=Last&nbsp;Logged&nbsp;In&nbsp;From
app.user.invalid_date.error=Invalid date for {0}

app.user.staff_no.label=Staff&nbsp;No
app.user.first_name.label=First&nbsp;Name
app.user.last_name.label=Last&nbsp;Name
app.user.effective_start_date.label=Effective&nbsp;Start&nbsp;Date
app.user.effective_end_date.label=Effective&nbsp;End&nbsp;Date
app.user.group.label=Group
app.user.group_local_branch.label=Group&nbsp;Local&nbsp;Branch
app.user.group_form.label=Group&nbsp;Form
app.user.report_to.label=Report&nbsp;To
app.user.last_login.label=Last&nbsp;Login
app.user.password_can_expire.label=Password&nbsp;can&nbsp;expire
app.user.account_locked.label=Lock&nbsp;Out
app.user.account_expired.label=Expired
app.user.is_active.label=Active
app.user.country.label=Country
app.user.password_expired.label=Password&nbsp;Expired

#button
default.button.cancel.label=Cancel
default.button.close.label=Close

#GL BI Code Mapping
app.GLBICodeMapping.glAcctNo.label=COA
app.GLBICodeMapping.form.label=Form
app.GLBICodeMapping.glDesc.label=COADesc
app.GLBICodeMapping.prodCd.label=Product&nbsp;Code
app.GLBICodeMapping.mapPosGroup.label=BICode&nbsp;Positive&nbsp;Group
app.GLBICodeMapping.mapPos.label=BICode&nbsp;Positive
app.GLBICodeMapping.mapNegGroup.label=BICode&nbsp;Negative&nbsp;Group
app.GLBICodeMapping.mapNeg.label=BICode&nbsp;Negative
app.GLBICodeMapping.rsdntTp.label=Resident&nbsp;Type
app.GLBICodeMapping.accType.label=Account&nbsp;Type
app.GLBICodeMapping.ccy.label=Currency&nbsp;Type
app.GLBICodeMapping.nettOffFlg.label=Net&nbsp;of&nbsp;FLAG
app.GLBICodeMapping.netOffType.label=Net&nbsp;Off&nbsp;Type
app.GLBICodeMapping.custClass.label=Customer&nbsp;Class
app.GLBICodeMapping.biCodeFormPos.label=BICode&nbsp;Form&nbsp;Positive
app.GLBICodeMapping.biCodeFormNeg.label=BICode&nbsp;Form&nbsp;Negative

#Branch
app.branch.regCd.label=Region
app.branch.mbrCd.label=Main Branch
app.branch.brCd.label=Branch Code
app.branch.brNm.label=Branch Name
app.branch.biBranch.label=BI Branch
app.branch.locationCode.label=Location Code
app.branch.branchFlag.label=Branch Flag
branch.id.label=PKId

#Rules Header
app.parameterizedValue.id.label=PKId
app.parameterizedValue.formcode.label=Form Code
app.parameterizedValue.columnname.label=Column Name
app.parameterizedValue.othersvalue.label=Others Value
app.parameterizedValue.nullflag.label=Null Flag
app.parameterizedValue.sequence.label=Sequence

#Rules Value
app.rulesValue.id.label=PKId
app.rulesValue.parentid.label=Parent Id
app.rulesValue.valuepercondition.label=Value per Condition
app.rulesValue.sequence.label=Sequence

#Rules Detail
app.rulesDetail.id.label=PKId
app.rulesDetail.parentvalueid.label=Parent Value Id
app.rulesDetail.columnname.label=Column Name
app.rulesDetail.operator.label=Operator
app.rulesDetail.value.label=Value
app.rulesDetail.clauseflag.label=Clause Flag
app.rulesDetail.sequence.label=Sequence

#Mapping Rules Header
app.mappingRules.mappingName.label=Mapping Name
app.mappingRules.mappingDesc.label=Mapping Desc
app.mappingRules.tableDestination.label=Table Destination
app.mappingRules.mappingMode.label=Mapping Mode

#Mapping Rules Detail
app.mappingRulesDetail.column_source.label=Column&nbsp;Source
app.mappingRulesDetail.column_destination.label=Column&nbsp;Destination
app.mappingRulesDetail.column_destination_datatype.label=Column&nbsp;Destination&nbsp;Data&nbsp;Type
app.mappingRulesDetail.column_destination_allow_null.label=Allow&nbsp;Null
app.mappingRulesDetail.primary_key.label=Primary&nbsp;Key

#System Parameter
app.systemParameter.paramDate.label=Param Date

#Form Management Setting
app.form.formcode.label=Form Code
app.form.formname.label=Form Name
app.form.tablename.label=Table Name
app.form.tablesource.label=Table Source

#Form Management Detail
app.formManagement.formcode.label=Form&nbsp;Code
app.formManagement.reportitem.label=Report&nbsp;Item
app.formManagement.reportfield.label=Report&nbsp;Field
app.formManagement.sourcetype.label=Source&nbsp;Type
app.formManagement.sourcefield.label=Source&nbsp;Field
app.formManagement.sourcefieldsybase.label=Source&nbsp;Field&nbsp;Sybase
app.formManagement.commoncode.label=Common&nbsp;Code
app.formManagement.condition.label=Condition
app.formManagement.commoncode2.label=Common&nbsp;Code2
app.formManagement.condition2.label=Condition2
app.formManagement.groupingtype.label=Grouping&nbsp;Type
app.formManagement.sequence.label=Sequence
app.formManagement.status.label=Status
app.formManagement.showtoformui.label=Show&nbsp;to&nbsp;Form&nbsp;UI
app.formManagement.showtodetailui.label=Show&nbsp;to&nbsp;Detail&nbsp;UI
app.formManagement.showformeditui.label=Show&nbsp;Form&nbsp;Edit&nbsp;UI
app.formManagement.allowformupdate.label=Allow&nbsp;Form&nbsp;Update
app.formManagement.allowdetailupdate.label=Allow&nbsp;Detail&nbsp;Update
app.formManagement.isgroupsource.label=Is&nbsp;Group&nbsp;Source
app.formManagement.issummarized.label=Is&nbsp;Summarized
app.formManagement.detailformdefaultvalue.label=Detail&nbsp;Form&nbsp;Default&nbsp;Value
app.formManagement.datatype.label=Data&nbsp;Type
app.formManagement.sourcereftable.label=Source&nbsp;Ref&nbsp;Table
app.formManagement.reftablefield.label=Ref&nbsp;Table&nbsp;Field
app.formManagement.sourcefieldaspkid.label=Source&nbsp;Field&nbsp;as&nbsp;PKId
app.formManagement.showdetaileditui.label=Show&nbsp;Detail&nbsp;Edit&nbsp;UI
app.formManagement.isvalidateonformlevel.label=Mandatory&nbsp;Form
app.formManagement.isvalidateondetaillevel.label=Mandatory&nbsp;Detail
app.formManagement.tf_absolute.label=TF_Absolute
app.formManagement.tf_textfile.label=TF_Textfile
app.formManagement.tf_fieldlen.label=TF_Fieldlen
app.formManagement.tf_alignment.label=TF_Alignment
app.formManagement.tf_spacechar.label=TF_Spacechar

#Validation Rules
app.validationRules.id.label=Rule Id
app.validationRules.ruletype.label=Rule Type
app.validationRules.ruledesc.label=Rule Description
app.validationRules.operator.label=Operator

#Validation Columns
app.validationColumns.ruleid.label=Rule&nbsp;Id
app.validationColumns.fieldname.label=Field&nbsp;Name
app.validationColumns.formcode.label=Form&nbsp;Code
app.validationColumns.validation.label=Validation
app.validationColumns.fieldvalue.label=Field&nbsp;Value
app.validationColumns.otherfield.label=Other&nbsp;Field
app.validationColumns.ruleparam.label=Rule&nbsp;Param
app.validationColumns.msg.label=Message
app.validationColumns.validationlevel.label=Validation&nbsp;Level
app.validationColumns.sequence.label=Sequence

#Compare Source VS Form
app.compare.bibranch.label=BI Branch
app.compare.bibranchname.label=BI Branch Name
app.compare.button.process.label=Process

#Automatic Rounding
app.automaticRounding.bibranch.label=BI Branch
app.automaticRounding.bibranchname.label=BI Branch Name
app.automaticRounding.button.process.label=Process

#Automatic Rounding Approval
app.automaticRoundingApproval.refid.label=Ref&nbsp;Id
app.automaticRoundingApproval.action.label=Action
app.automaticRoundingApproval.jenisform1.label=Jenis&nbsp;Form&nbsp;1
app.automaticRoundingApproval.sandisap.label=Sandi&nbsp;SAP
app.automaticRoundingApproval.form.label=Form
app.automaticRoundingApproval.fieldname.label=Field&nbsp;Name
app.automaticRoundingApproval.cabangbi.label=Cabang&nbsp;BI
app.automaticRoundingApproval.currency.label=Currency
app.automaticRoundingApproval.roundingvalue.label=Rounding&nbsp;Value

#Form 1 VS Other
app.form1VSOther.bibranch.label=BI Branch
app.form1VSOther.bibranchname.label=BI Branch Name
app.form1VSOther.button.process.label=Process
app.form1VSOther.button.result.label=Go To Result

#group form
app.groupForm.groupname.label=Group Name
app.groupForm.pkid.label=PKID

#group local branch
app.groupLocalBranch.groupname.label=Group Name
app.groupLocalBranch.pkid.label=PKID

#Partial Upload Monitoring
partialUploadMonitoring.label = Partial Upload Monitoring
app.partialUploadMonitoring.jobid.label = Job&nbsp;ID
app.partialUploadMonitoring.mappingrules.label = Mapping&nbsp;Rules
app.partialUploadMonitoring.rowaffected.label = Row&nbsp;Affected
app.partialUploadMonitoring.status.label = Status
app.partialUploadMonitoring.filename.label = File&nbsp;Name
app.partialUploadMonitoring.description.label = Description
app.partialUploadMonitoring.createddate.label = Created&nbsp;Date
app.partialUploadMonitoring.lastupdateddate.label = Last&nbsp;Updated
app.partialUploadMonitoring.errormessage.label = Error&nbsp;Message
app.partialUploadMonitoring.detailmessages.label = Detail&nbsp;Messages

#home
home.mytask.label = My Task
home.iu.label = Informasi Umum
home.summary.label = Today Summary
home.approval.label = Approval & Alert
historyCustomer.label=Customer Form
historyCustomer.company.label=Perusahaan
historyCustomer.t182TglLahir.label=Tanggal Lahir
historyCustomer.t182GelarB.label=Gelar di belakang
historyCustomer.t182JenisKelamin.label=Jenis Kelamin
historyCustomer.t182NamaBelakang.label=Nama Belakang
historyCustomer.t182NamaDepan.label=Nama Depan
historyCustomer.fullNama.label=Nama Lengkap Customer
historyCustomer.t182GelarD.label=Gelar di depan
historyCustomer.t182ID.label=Kode Customer
historyCustomer.jenisCustomer.label=Jenis Customer
historyCustomer.peranCustomer.label=Peran Customer
historyCustomer.t182Alamat.label=Alamat Korespodensi Customer
historyCustomer.t182RT.label=RT
historyCustomer.t182RW.label=RW
historyCustomer.t182KodePos.label=Kode Pos
historyCustomer.t182AlamatNPWP.label=Alamat NPWP Customer
historyCustomer.t182RTNPWP.label=RT
historyCustomer.t182RWNPWP.label=RW
historyCustomer.provinsi2.label=Provinsi
historyCustomer.kabKota2.label=Kab./Kota
historyCustomer.kecamatan2.label=Kecamatan
historyCustomer.kelurahan2.label=Kelurahan
historyCustomer.t182KodePosNPWP.label=Kode Pos NPWP
historyCustomer.provinsi.label=Provinsi
historyCustomer.kabKota.label=Kab./Kota
historyCustomer.kecamatan.label=Kecamatan
historyCustomer.kelurahan.label=Kelurahan
historyCustomer.t182WebUserName.label=Username
historyCustomer.t182WebPassword.label=Password
historyCustomer.t182WebPassword1.label=Konf. Password
historyCustomer.t182Foto.label=Foto
historyCustomer.t182NoTelpRumah.label=Telp. Rumah
historyCustomer.t182NoTelpKantor.label=Telp. Kantor
historyCustomer.t182NoFax.label=Faximile
historyCustomer.t182NoHp.label=Telp. Seluler
historyCustomer.t182Email.label=Email
historyCustomer.t182StaTerimaMRS.label=Terima MRS?
historyCustomer.jenisIdCard.label=Jenis ID Card
historyCustomer.t182NomorIDCard.label=No.ID Card
historyCustomer.t182NPWP.label=No NPWP
historyCustomer.t182NamaSesuaiNPWP.label=Nama Sesuai NPWP
historyCustomer.t182NoFakturPajakStd.label=NFPS
historyCustomer.agama.label=Agama
historyCustomer.nikah.label=Status
historyCustomer.hobbies.label=Hobby
FA.m185TanggalFA.label=Tanggal Surat Field Action
FA.jenisFA.label=Jenis Field Action
FA.m185NomorSurat.label=Nomor Surat Field Action
FA.m185NamaFA.label=Nama Field Action
FA.m185ThnBlnRakit1.label=Tahun & Bulan Rakit Awal
FA.m185ThnBlnRakit2.label=Tahun & Bulan Rakit Akhir
FA.label=Master Field Action
vehicleFA.label=Detail Field Action
partsAdjust.label=On Hand Adjustment
partsAdjust.t145ID.label=Nomor Adjusment
partsAdjust.t145TglAdj.label=Tanggal Adjusment

#Stock Opname
stockOPName.input.label =Input Stock Opname
stokOPName.t132Tanggal.label = Tanggal Stock Opname
stokOPName.t132ID.label = Nomor Stock Opname
stockOPName.button.cariKlasifikasiGoods.label = Search Part
stokOPName.label = Stock Opname
stokOPName.goods.kode.label = Kode Part
stokOPName.goods.nama.label = Nama Part
stokOPName.goods.lokasi.label = Location
stokOPName.viewTdkSesuai.label = View Stok Opname yang Tidak Sesuai
stockOPName.view.label = View Stok Opname
stokOPName.clearsearch.label = Clear Search
stokOPName.search.label = Search
stokOPName.button.printBAstok.label = Print BA Stock Taking + Lampiran
stokopname.button.inputHasilStokOpname.label = Input Hasil Stok Opname
stokOPName.qty1.kode.label = Qty 1
stokOPName.qty2.kode.label = Qty 2
stokOPName.goods.satuan1.label = Satuan 1
stokOPName.goods.satuan2.label = Satuan 2
stokOPName.icc.nama.label = ICC
stokOPName.goods.lokasirak.label = Lokasi/Rak
stokopname.button.printstok.label = Print Kartu Stok
stokOPName.confirm.label = Konfirmasi Stok Opname
stokOPName.freeze.button.label = Freeze System
stokOPName.unfreeze.button.label = Unfreeze System

partsDisposal.label=Disposal
partsDisposal.t147ID.label=Nomor Disposal
partsDisposal.t147TglDisp.label=Tanggal Disposal
partsDisposal.t147xNamaUser.label=Petugas Disposal
retention.label=Master Template SMS Retention
retention.m207NamaRetention.label=Nama Template
retention.m207FormatSms.label=Format SMS Retention

goodsHargaJual.t151TMT.label=Tanggal Berlaku
colorMatching.m046TglBerlaku.label=Tanggal Berlaku
#vendor.m121staSPLD.label=Status SPLD


#administratin/company dealer/ contact person
contactPerson.label=Contact Person
contactPerson.companyDealer.label = Company Dealer
contactPerson.m013Tanggal.label = Tanggal Berlaku
contactPerson.m013NamaCP.label = Nama Contact Person
contactPerson.m013JabatanCP.label = Jabatan Contact Person
contactPerson.m013Telp1.label = Nomor Telp 1
contactPerson.m013Telp2.label = Nomor Telp 2
contactPerson.gagalSimpan.label = Data Tidak Bisa disimpan
contactPerson.companyDealerUsed.label = Company Dealer Telah digunakanretention.m207StaOtomatis.label=Metode Pengiriman SMS

historyCustomerVehicle.label=Customer Vehicle
reminder.label=Reminder
reminder.m201ID.label=Jenis Reminder
reminder.t201LastKM.label=KM
reminder.t201TglJatuhTempo.label=Est. Jatuh Tempo
reminder.t201TglDM.label=Tgl. Kirim DM
reminder.t201TglSMS.label=Tgl. Kirim SMS
reminder.t201TglEmail.label=Tgl. Kirim Email
reminder.t201LastServiceDate.label=Last Service Date
reminder.operation.label=Operation


#WOInformation
woInformation.label = WO Information
wo.category.search.label = Kategori Search
wo.key.label = Key
wo.t401NoWo.label = Nomor WO
wo.noPolisi.label = Nomor Polisi
wo.modelKendaraan.label = Model Kendaraan
wo.tanggalWO.label = Tanggal WO
wo.tanggalPenyerahan.label = Waktu Penyerahan
wo.teleponCustomer.label = Telepon Customer
wo.alamatCustomer.label = Alamat Customer
wo.namaCustomer.label = Nama Customer
wo.keluhan.label = Keluhan
wo.stallParkir.label = Stall Parkir
wo.permintaan.label = Permintaan
wo.fir.label = FIR (Fix It Right)
wo.biaya.label = Biaya
wo.rateTeknisi.label = Rate Teknisi
wo.statusKendaraan.label = Status Kendaraan

#problemFinding
PF.category.search.label = Kategori Search
PF.key.label = Key
PF.t401NoWo.label = Nomor WO
PF.noPolisi.label = Nomor Polisi
PF.model.label = Model Kendaraan
PF.stall.label = Nama Stall
PF.teknisi.label = Teknisi
PF.foreman.label = Foreman
PF.job.label = Job
PF.deskripsi.label = Deskripsi Problem
PF.tanggal.label = Tanggal Problem
PF.jenis.label = Jenis Problem
PF.status.label = Status Kendaraan
PF.catatan.label = Catatan Tambahan
antriCuci.t600Tanggal.label=Tanggal Request
antriCuci.t600xKet.label=Alasan
antriCuci.reception.label=Nomor WO
antriCuci.t600NomorAntrian.label=Nomor Antrian
antriCuci.alasanUbahUrutan.label=Alasan

namaDokumen.m007NamaDokumen.unique=Nama dokumen sudah ada

materai.m705TglBerlaku.unique=Data materai dengan tanggal berlaku, jumlah tagihan awal, dan jumlah tagihan akhir sudah digunakan

alasanRefund.m071ID.unique=Id Alasan Refund sudah digunakan
alasanRefund.m071AlasanRefund.unique=Alasan refund sudah digunakan
staticDisc.m019TMT.unique=Tanggal sudah digunakan
dinamicDisc.m020TMT.unique=Tanggal sudah digunakan

flatRate.operation.unique = Data Operation Sudah Digunakan
flatRate.baseModel.unique = Data Base Model Sudah Digunakan
flatRate.t113TMT.blank = Silahkan Isi Tanggal Mulai Berlaku Terlebih Dahulu

loket.m404NamaLoket.unique = Data duplikat (Sudah digunakan)
loket.m404NamaLoket.validator.invalid = Data telah digunakan

XPartsMappingMinorChange.t111xStaIO.blank = Status Perubahan harus diisi
XPartsMappingMinorChange.goods.validator.invalid = Data telah digunakan

tarifCBUNTAM.t113bTMT.validator.invalid = Data Kategori dan Tanggal Berlaku Sudah digunakan (Duplikat)

tarifPerJam.t152TMT.validator.invalid = Data Kategori dan Tanggal Berlaku Sudah digunakan (Duplikat)

partsMapping.goods.validator.invalid = Data Sudah digunakan (Duplikat)
partsMapping.fullModelCode.validator.invalid = Data Sudah digunakan (Duplikat)

SPKDetail.customerVehicle.unique = Data telah digunakan
SPK.t191NoSPK.unique = No SPK sudah digunakan (Duplikat)

flatRate.t113FlatRate.validator.invalid = Data telah digunakan
flatRate.t113ProductionRate.validator.invalid = Data telah digunakan

baseModel.m102FotoWAC.validator.invalid = Data telah digunakan
baseModel.m102KodeBaseModel.unique = Data telah digunakan
baseModel.m102KodeBaseModel.validator.invalid = Data telah digunakan

material.m047Qty1.validator.invalid = Data telah digunakan

#delivery settlement
delivery.settlement.label=Delivery - Settlement
delivery.settlement.left.label=Data Invoice
delivery.settlement.right.top.label=Invoice Settlement

#delivery kwitansi
delivery.kwitansi.label=Delivery - Kwitansi
delivery.kwitansi.left.label=Data Work Order / Appoinment
delivery.kwitansi.right.top.label=Pembayaran Kwitansi

#delivery refund
default.refund.label=Refund
default.save.label=Save Settlement
default.gate.label=Gate Pass
default.bayar.label=Save Pembayaran
default.save.kuitansi.label=Save Kuitansi
default.print.kuitansi.label=Print Kuitansi
default.preview.kuitansi.label=Preview Kuitansi
default.search.label=Search
default.clear.label=Clear

delivery.refund.label=Delivery - Refund
delivery.refund.left.label=Data Work Order
delivery.refund.right.top.label=Refund
delivery.refund.right.bottom.label=Cek Data Kuitansi

delivery.slipList.label=Delivery - Slip
delivery.slipList.kriteria.search.refund.label=Kriteria Search Refund Form
delivery.slipList.tanggal.refund.label=Tanggal Refund
delivery.slipList.until.label=s/d
delivery.slipList.jenis.refund.label=Jenis Refund
delivery.slipList.metode.refund.label=Metode Refund
delivery.slipList.nomor.refund.label=Nomor Refund Form
delivery.slipList.refund.list.label=Refund Form List

delivery.slipList.kriteria.search.kuitansi.label=Kriteria Search Kuitansi
delivery.slipList.tanggal.kuitansi.label=Tanggal Kuitansi
delivery.slipList.jenis.bayar.label=Jenis Bayar
delivery.slipList.metode.bayar.label=Metode Bayar
delivery.slipList.nomor.kuitansi.label=Nomor Kuitansi
delivery.slipList.nama.kasir.label=Nama Kasir
delivery.slipList.kuitansi.list.label=Kuitansi List

delivery.slipList.kriteria.search.nota.retur.label=Kriteria Search Nota Retur
delivery.slipList.tanggal.nota.label=Tanggal Nota Retur
delivery.slipList.kategori.label=Kategori
delivery.slipList.kata.kunci.label=Kata Kunci
delivery.slipList.nota.list.label=Nota Retur List

delivery.slipList.kategori.search.faktur.label=Kategori Search Faktur Pajak
delivery.slipList.tanggal.faktur.label=Tanggal Faktur
delivery.slipList.faktur.list.label=Faktur Pajak List

delivery.slipList.kriteria.search.parts.label=Kriteria Search Parts Slip
delivery.slipList.tanggal.parts.label=Tanggal Parts Slip
delivery.slipList.nomor.parts.label=Nomor Parts Slip
delivery.slipList.nomor.wo.label=Nomor WO
delivery.slipList.nomor.polisi.label=Nomor Polisi
delivery.slipList.parts.list.label=Parts Slip List

delivery.slipList.kriteria.search.label=Kriteria Search Invoice
delivery.slipList.tanggal.invoice.label=Tanggal Invoice
delivery.slipList.tanggal.settlement.label=Tanggal Settlement
delivery.slipList.status.invoice.label=Status Invoice
delivery.slipList.customer.spk.label=Customer SPK ?
delivery.slipList.nomor.invoice.label=Nomor Invoice
delivery.slipList.tipe.invoice.label=Tipe Invoice
delivery.slipList.customer.name.label=Nama Customer
delivery.slipList.aging.label=Aging
delivery.slipList.invoice.list.label=Invoice List
SOQ.t156Tanggal.label=Id
SOQ.goods.label=Kode Parts
SOQ.goods2.label=Nama Parts
SOQ.t156Qty1.label=Jumlah
SOQ.t156Satuan.label=Satuan

trainingInstructor.namaInstruktur.blank=Mohon isi Nama Instruktur
trainingInstructor.namaInstruktur.nullable=Mohon isi Nama Instruktur

certificationType.namaSertifikasi.blank=Mohon isi Nama Sertifikikasi
certificationType.namaSertifikasi.nullable=Mohon isi Nama Sertifikikasi
certificationType.tipeSertifikasi.blank=Mohon isi Tipe Sertifikasi
certificationType.tipeSertifikasi.nullable=Mohon isi Tipe Sertifikasi

trainingType.namaTraining.blank=Mohon isi Nama Training
trainingType.namaTraining.nullable=Mohon isi Nama Training
trainingType.tipeTraining.blank=Mohon isi Tipe Training
trainingType.tipeTraining.nullable=Mohon isi Tipe Training
