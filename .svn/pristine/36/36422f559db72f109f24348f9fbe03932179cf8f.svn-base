package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Goods
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class MaterialController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params

        def c = Material.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_operation"){
                operation{
                    ilike("m053NamaOperation","%"+params."sCriteria_operation"+"%")
                }
			//	eq("operation",Operation.findByM053NamaOperationIlike("%"+params."sCriteria_operation"+"%"))
			}

            if(params."sCriteria_tipeDifficulty"){
                panelRepair{
                    repairDifficulty{
                        ilike("m042Tipe", "%"+params."sCriteria_tipeDifficulty"+"%")
                    }
                }
            }


			if(params."sCriteria_goods"){
				eq("goods",Goods.findByM111NamaIlike("%"+params."sCriteria_goods"+"%"))
			}

			if(params."sCriteria_m047Qty1"){
				eq("m047Qty1",Double.parseDouble(params."sCriteria_m047Qty1"))
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			if(params."sCriteria_m047Qty2"){
				eq("m047Qty2",params."sCriteria_m047Qty2")
			}

			if(params."sCriteria_satuan"){
                satuan{
                    ilike("m118KodeSatuan", "%"+params."sCriteria_satuan"+"%")
                }
			//	eq("satuan",Satuan.findByM118KonversiIlike("%"+params."sCriteria_satuan"+"%"))
			}

			if(params."sCriteria_panelRepair"){
			    String kriteria = params.sCriteria_panelRepair

                if(kriteria.contains("-")){


                    def data = kriteria.split("-")
                    def krit1 = data[0]
                    def krit2 = "0"

                    try{
                        krit2 = data[1]

                    }catch(ArrayIndexOutOfBoundsException ex){

                    }

                    panelRepair{
                        eq("m043Area1", Double.parseDouble(krit1))
                        eq("m043Area2", Double.parseDouble(krit2))
                    }

                }else{
                    panelRepair{
                        //  eq("m043Area1", Double.parseDouble(params.sCriteria_panelRepair))
                        or{
                            eq("m043Area1",Double.parseDouble(params.sCriteria_panelRepair))
                            eq("m043Area2",Double.parseDouble(params.sCriteria_panelRepair))
                        }
                    }
                }
            }
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}

            ilike("staDel", "0")
			
			switch(sortProperty){
				case "panelRepair" :
                    panelRepair{
                        order("m043Area1", sortDir)
                    }
                    break;
                default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						operation: it.operation?.m053NamaOperation,
			
						panelRepair: it.panelRepair?.repairDifficulty?.m042Tipe, //Tipe Difficult
			
						goods: it.goods?.m111Nama,
			
						m047Qty1: it.m047Qty1,
			
						createdBy: it.createdBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
						m047Qty2: it.m047Qty2,
			
						satuan: it.satuan?.toString(),
			
						staDel: it.panelRepair?.m043Area1 + " s.d " + it.panelRepair?.m043Area2, //Area
			
						updatedBy: it.updatedBy,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[materialInstance: new Material(params)]
	}

	def save() {
		def materialInstance = new Material(params)
        def area = params.area
        def panelRepair = PanelRepair.get(Double.parseDouble(area))

        if(materialInstance.m047Qty1 == 0){
            flash.message = "Quantity tidak boleh 0"
            render(view: "create", model: [materialInstance: materialInstance])
            return
        }


        materialInstance.panelRepair = panelRepair
        materialInstance.staDel = '0'
        materialInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
     //   materialInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        materialInstance?.lastUpdProcess = "INSERT"
        materialInstance?.companyDealer = session.userCompanyDealer


        if (!materialInstance.save(flush: true)) {
			render(view: "create", model: [materialInstance: materialInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'material.label', default: 'Material'), materialInstance.id])
		redirect(action: "show", id: materialInstance.id)
	}

	def show(Long id) {
		def materialInstance = Material.get(id)
        String difficultType =  materialInstance?.panelRepair?.repairDifficulty?.m042Tipe
        String area = materialInstance?.panelRepair?.m043Area1 + " - " + materialInstance?.panelRepair?.m043Area2
		if (!materialInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'material.label', default: 'Material'), id])
			redirect(action: "list")
			return
		}

		[materialInstance: materialInstance, difficultType : difficultType, area : area]
	}

	def edit(Long id) {
		def materialInstance = Material.get(id)


		if (!materialInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'material.label', default: 'Material'), id])
			redirect(action: "list")
			return
		}

		[materialInstance: materialInstance]
	}

	def update(Long id, Long version) {
		def materialInstance = Material.get(id)

        materialInstance.properties = params

        if (!materialInstance.m047Qty1 == "0") {
            flash.message = "Quantity tidak boleh 0"
            render(view: "edit", model: [materialInstance: materialInstance])
            return
        }


        materialInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        materialInstance?.lastUpdProcess = "UPDATE"


        if (!materialInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'material.label', default: 'Material'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (materialInstance.version > version) {
				
				materialInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'material.label', default: 'Material')] as Object[],
				"Another user has updated this Material while you were editing")
				render(view: "edit", model: [materialInstance: materialInstance])
				return
			}
		}

		materialInstance.properties = params

		if (!materialInstance.save(flush: true)) {
			render(view: "edit", model: [materialInstance: materialInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'material.label', default: 'Material'), materialInstance.id])
		redirect(action: "show", id: materialInstance.id)
	}

	def delete(Long id) {
		def materialInstance = Material.get(id)
		if (!materialInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'material.label', default: 'Material'), id])
			redirect(action: "list")
			return
		}

        materialInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        materialInstance?.lastUpdProcess = "DELETE"
        materialInstance.staDel = '1'

        try {
			materialInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'material.label', default: 'Material'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'material.label', default: 'Material'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDel(Material, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(Material, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def getAreas(){
        def tipeDifficulty = params.tipeDifficulty
        def panelRepairList = PanelRepair.findAllByRepairDifficulty(RepairDifficulty.findById(Double.parseDouble(tipeDifficulty)))
        def res = []
        panelRepairList.each {
            res << [
                    id : it.id,
                    area1 : it.m043Area1,
                    area2 : it.m043Area2
            ]
        }
        render res as JSON

    }
	
	
}
