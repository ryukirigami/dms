package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.StaHadir
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.sql.Time
import java.text.DateFormat
import java.text.SimpleDateFormat

class ManPowerAbsensiController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		session.exportParams=params
        DateFormat df = new SimpleDateFormat("HH:mm")
		def c = ManPowerAbsensi.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_namaManPower"){
                String data = params.sCriteria_namaManPower
                def namaX = data.split("\\(")
				eq("namaManPower", NamaManPower.findByT015NamaLengkapIlike("%"+namaX[0].trim()+"%"))
			}


            if (params."sCriteria_t017Tanggal" && params."sCriteria_t017Tanggal2") {
                ge("t017Tanggal", params."sCriteria_t017Tanggal")
                lt("t017Tanggal", params."sCriteria_t017Tanggal2" + 1)
            }

			
			if(params."sCriteria_staHadir"){
				//eq("staHadir",params."sCriteria_staHadir")
				eq("staHadir", StaHadir.findByM017IDIlike("%"+params."sCriteria_staHadir"+"%"))
			}

			if(params."sCriteria_t017JamPulang"){
                Date date1 = df.parse(params."sCriteria_t017JamPulang")
                if(params."sCriteria_t017JamPulang" != "0:0"){
                    eq("t017JamPulang", date1)
                }
			}

			if(params."sCriteria_t017JamDatang"){
                Date date2 = df.parse(params."sCriteria_t017JamDatang")
                if(params."sCriteria_t017JamDatang" != "0:0"){
                    eq("t017JamDatang", date2)
                }
			}
	
			if(params."sCriteria_t017Ket"){
				ilike("t017Ket","%" + (params."sCriteria_t017Ket" as String) + "%")
			}
	
			 
				ilike("staDel","0")


            namaManPower{
                order("t015NamaLengkap",sortDir)
            }
            order("t017Tanggal","asc")

//			switch(sortProperty){
//                case "namaManPower":
//                    namaManPower{
//                        order("t015NamaLengkap",sortDir)
//                    }
//                break
//				default:
//					order(sortProperty,sortDir)
//					break;
//			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						namaManPower: it.namaManPower?.t015NamaLengkap,
			
						t017Tanggal: it.t017Tanggal?it.t017Tanggal.format(dateFormat):"",
			
						staHadir: it.staHadir?.m017NamaStaHadir,
			 
						t017JamDatang: it.t017JamDatang == null ? "":new SimpleDateFormat("HH:mm").format(new Date(it.t017JamDatang?.getTime())),
						
						t017JamPulang: it.t017JamPulang == null ? "": new SimpleDateFormat("HH:mm").format(new Date(it.t017JamPulang?.getTime())),
						
						t017Ket: it.t017Ket,
			
						staDel: it.staDel,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[manPowerAbsensiInstance: new ManPowerAbsensi(params)]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def manPowerAbsensiInstance = new ManPowerAbsensi()
        String data = params.namaManPower
        def namaX = data.split("\\(")
        def nama = NamaManPower.findByT015NamaLengkap(namaX[0].trim())

        if(!nama){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Nama Manpower Tidak  Ada')
            render(view: "create", model: [manPowerAbsensiInstance: manPowerAbsensiInstance])
            return
        }
        if(params.t017Tanggal > params.t017Tanggal2){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Tanggal Awal Harus Lebih Besar')
            render(view: "create", model: [manPowerAbsensiInstance: manPowerAbsensiInstance])
            return
        }

        Date tanggal = params.t017Tanggal12;
        int jmlhHari = (params.t017Tanggal2 - params.t017Tanggal)
        NamaManPower namaManPower = NamaManPower.findByT015NamaLengkap(namaX[0].trim())
        (0..jmlhHari).each {
            manPowerAbsensiInstance = new ManPowerAbsensi()
            manPowerAbsensiInstance?.setNamaManPower(namaManPower)
            manPowerAbsensiInstance?.setCompanyDealer(namaManPower.companyDealer)
            manPowerAbsensiInstance?.setT017Tanggal(params.t017Tanggal + it)
            manPowerAbsensiInstance?.setStaHadir(StaHadir.findById(params.staHadir.id))
            manPowerAbsensiInstance?.setT017Ket(params.t017Ket)
            manPowerAbsensiInstance?.setStaDel('0')
            manPowerAbsensiInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            manPowerAbsensiInstance?.setLastUpdProcess("INSERT")
            if(params.staHadir.id=="1" || params.staHadir.id=="2"){
                Time timeP = new Time(Integer?.parseInt(params?.t017JamPulang_hour), Integer?.parseInt(params?.t017JamPulang_minute), 0)
                Time timeD = new Time(Integer?.parseInt(params?.t017JamDatang_hour), Integer?.parseInt(params?.t017JamDatang_minute), 0)
                manPowerAbsensiInstance?.setT017JamDatang(timeD)
                manPowerAbsensiInstance?.setT017JamPulang(timeP)
            }
            manPowerAbsensiInstance.save(flush: true)
            manPowerAbsensiInstance.errors.each{ println it }
        }


		flash.message = message(code: 'default.created.message', args: [message(code: 'manPowerAbsensi.label', default: 'Rencana Hadir Man Power'), manPowerAbsensiInstance.id])
		redirect(action: "show", id: manPowerAbsensiInstance.id)
	}

	def show(Long id) {
		def manPowerAbsensiInstance = ManPowerAbsensi.get(id)
		if (!manPowerAbsensiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerAbsensi.label', default: 'ManPowerAbsensi'), id])
			redirect(action: "list")
			return
		}

		[manPowerAbsensiInstance: manPowerAbsensiInstance]
	}

	def edit(Long id) {
		def manPowerAbsensiInstance = ManPowerAbsensi.get(id)
		if (!manPowerAbsensiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerAbsensi.label', default: 'ManPowerAbsensi'), id])
			redirect(action: "list")
			return
		}

		[manPowerAbsensiInstance: manPowerAbsensiInstance]
	}

	def update(Long id, Long version) {
		def manPowerAbsensiInstance = ManPowerAbsensi.get(id)
		if (!manPowerAbsensiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerAbsensi.label', default: 'ManPowerAbsensi'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (manPowerAbsensiInstance.version > version) {
				
				manPowerAbsensiInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'manPowerAbsensi.label', default: 'ManPowerAbsensi')] as Object[],
				"Another user has updated this ManPowerAbsensi while you were editing")
				render(view: "edit", model: [manPowerAbsensiInstance: manPowerAbsensiInstance])
				return
			}
		}

		//manPowerAbsensiInstance.properties = params
        String data = params.namaManPower
        def namaX = data.split("\\(")
        def nama = NamaManPower.findByT015NamaLengkap(namaX[0].trim())
        if(!nama){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Nama Manpower Tidak')
            render(view: "edit", model: [manPowerAbsensiInstance: manPowerAbsensiInstance])
            return
        }
            manPowerAbsensiInstance?.setNamaManPower(NamaManPower.findByT015NamaLengkap(namaX[0].trim()))
            manPowerAbsensiInstance?.setT017Tanggal(params.t017Tanggal)
            manPowerAbsensiInstance?.setStaHadir(StaHadir.findById(params.staHadir.id))
            manPowerAbsensiInstance?.setT017Ket(params.t017Ket)
            manPowerAbsensiInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            manPowerAbsensiInstance?.setLastUpdProcess("UPDATE")
            manPowerAbsensiInstance?.lastUpdated = datatablesUtilService?.syncTime()

            if(params.staHadir.id=="1" || params.staHadir.id=="2"){

                Time timeP = new Time(Integer?.parseInt(params?.t017JamPulang_hour), Integer?.parseInt(params?.t017JamPulang_minute), 0)
                Time timeD = new Time(Integer?.parseInt(params?.t017JamDatang_hour), Integer?.parseInt(params?.t017JamDatang_minute), 0)
                manPowerAbsensiInstance?.setT017JamDatang(timeD)
                manPowerAbsensiInstance?.setT017JamPulang(timeP)
            }else{
//                Time timeP = new Time(Integer?.parseInt('00'), Integer?.parseInt('00'), 0)
//                Time timeD = new Time(Integer?.parseInt('00'), Integer?.parseInt('00'), 0)
//                manPowerAbsensiInstance?.setT017JamDatang(timeD)
//                manPowerAbsensiInstance?.setT017JamPulang(timeP)
            }


		 
		if (!manPowerAbsensiInstance.save(flush: true)) {
			render(view: "edit", model: [manPowerAbsensiInstance: manPowerAbsensiInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'manPowerAbsensi.label', default: 'Rencana Hadir Man Power'), manPowerAbsensiInstance.id])
		redirect(action: "show", id: manPowerAbsensiInstance.id)
	}

	def delete(Long id) {
		def manPowerAbsensiInstance = ManPowerAbsensi.get(id)
		if (!manPowerAbsensiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerAbsensi.label', default: 'ManPowerAbsensi'), id])
			redirect(action: "list")
			return
		}

		try {
			//manPowerAbsensiInstance.delete(flush: true)
			manPowerAbsensiInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
			manPowerAbsensiInstance?.setLastUpdProcess("DELETE")
			manPowerAbsensiInstance?.setStaDel('1')
            manPowerAbsensiInstance?.lastUpdated = datatablesUtilService?.syncTime()
            manPowerAbsensiInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'manPowerAbsensi.label', default: 'ManPowerAbsensi'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'manPowerAbsensi.label', default: 'ManPowerAbsensi'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(ManPowerAbsensi, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(ManPowerAbsensi, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
    def kodeList() {
        def res = [:]

        def result = NamaManPower.findAllWhere(staDel : '0')
        def opts = []
        result.each {
            opts << it?.t015NamaLengkap +(it?.manPowerDetail?" (" + (it?.manPowerDetail?.m015LevelManPower) + ")":"")
        }

        res."options" = opts
        render res as JSON
    }
	
}
