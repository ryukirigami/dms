package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.Customer
import com.kombos.maintable.MappingCustVehicle
import com.kombos.maintable.Nikah
import com.kombos.maintable.PeranCustomer
import grails.converters.JSON
import org.apache.commons.codec.binary.Base64
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.multipart.commons.CommonsMultipartFile

class HistoryCustomerController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def addNewCars() {
        def currentRequest = RequestContextHolder.requestAttributes
        CustomerVehicle customerVehicle = CustomerVehicle.get(params?.vehicleId)

        HistoryCustomerVehicle historyCustomerVehicle = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(customerVehicle,'0')
        def rows = currentRequest?.session.detailCustomerCars
        if(rows){
            rows << [
                    id:customerVehicle?.id,
                    pemilik: historyCustomerVehicle?.t183NamaSTNK,
                    nopol: historyCustomerVehicle?.kodeKotaNoPol?.m116ID + ' ' + historyCustomerVehicle?.t183NoPolTengah + ' ' + historyCustomerVehicle?.t183NoPolBelakang,
                    model:historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
            ]
        }else{
            rows = []
            rows << [
                    id:customerVehicle?.id,
                    pemilik: historyCustomerVehicle?.t183NamaSTNK,
                    nopol: historyCustomerVehicle?.kodeKotaNoPol?.m116ID + ' ' + historyCustomerVehicle?.t183NoPolTengah + ' ' + historyCustomerVehicle?.t183NoPolBelakang,
                    model:historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
            ]
        }


        currentRequest.session['detailCustomerCars'] = rows
        def result = [detail : rows]
        render result as JSON
    }

    def uploadImage() {
        if (params.myFile) {
            def fileName
            def inputStream
            if (params.myFile instanceof CommonsMultipartFile) {
                fileName = params.myFile?.originalFilename
                inputStream = params.myFile.getInputStream()
            } else {
                fileName = params.myFile
                inputStream = request.getInputStream()
            }

            fileName = fileName.replaceAll(" ", "_")

            File storedFile = new File("/opt/${fileName}")
            storedFile.parentFile.mkdirs()
            storedFile.append(inputStream)

            session.customerFoto = storedFile?.absolutePath

            render '<img width=\"200px\" src="data:' + 'jpg' + ';base64,' + new String(new Base64().encode(storedFile.bytes), "UTF-8") + '" ' + ' />'
        } else {
            render "No Image"
        }
    }

    def lihatGambar() {
        def historyCustomer = HistoryCustomer.read(params.id)
        if (params.id && historyCustomer) {
            render '<img width=\"200px\" src="data:' + 'jpg' + ';base64,' + new String(new Base64().encode(historyCustomer.t182Foto), "UTF-8") + '" ' + ' />'
        } else {
            render "No Image"
        }
    }

    def surveyListDatatablesList() {
        def historyCustomer = HistoryCustomer.read(params.historyCustomerId)
        def ret

        def customerVehicles = MappingCustVehicle.withCriteria {
            projections {
                groupProperty("customerVehicle")
            }
            eq("customer", historyCustomer?.customer)
        }

        def c = CustomerSurveyDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            projections {
                groupProperty("customerSurvey")
            }
            if (customerVehicles.size() > 0) {
                inList("customerVehicle", customerVehicles)
            } else {
                sqlRestriction("1!=1")
            }
        }

        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    tanggal: it?.t104TglAwal?.format("dd-MM-yyyy"),
                    feedback: it?.t104Text
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def customerCarsDatatablesList() {

        def output
        if('-1'.equals(params.historyCustomerId)){
            def currentRequest = RequestContextHolder.requestAttributes
            def rows = currentRequest?.session.detailCustomerCars
            def size = (rows?.size()>0)?rows?.size():0
            def data = (rows?.size()>0)?rows:[]
            output = [sEcho: params.sEcho, iTotalRecords: size, iTotalDisplayRecords: size, aaData: data]
        }else{
            def customer = HistoryCustomer.get(params.historyCustomerId).customer
            def mapping = MappingCustVehicle.findAllByCustomer(customer)
            def rows = []
            mapping.each {
                def historyCustomerVehicle = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(it.customerVehicle,'0')
                if(historyCustomerVehicle) {
                    rows << [
                            id     : historyCustomerVehicle?.id,
                            pemilik: historyCustomerVehicle?.t183NamaSTNK ? historyCustomerVehicle?.t183NamaSTNK:"-",
                            nopol  : historyCustomerVehicle?.kodeKotaNoPol?.m116ID + ' ' + historyCustomerVehicle?.t183NoPolTengah + ' ' + historyCustomerVehicle?.t183NoPolBelakang,
                            model  : historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel ? historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel:"-"
                    ]
                }
            }

            def currentRequest = RequestContextHolder.requestAttributes
            def sessRows = currentRequest?.session.detailCustomerCars
            sessRows?.each{
                rows << it
            }
            def size = (rows?.size()>0)?rows?.size():0
            def data = (rows?.size()>0)?rows:[]
            output = [sEcho: params.sEcho, iTotalRecords: size, iTotalDisplayRecords: size, aaData: data]
        }

        render output as JSON
    }

    def customerListDatatablesList() {

        def historyCustomer = HistoryCustomer.read(params.historyCustomerId)

        def ret

        def c = MappingCustVehicle.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("customer", historyCustomer?.customer)
        }

        def rows = []

        results.each { mapping ->
            def historyCustomerVehicle = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(mapping.customerVehicle,'0')
            def historyCustomers = HistoryCustomer.findAllByCustomerAndStaDel(mapping.customer,'0')

            historyCustomers.each { customer ->
                rows << [
                        id: mapping.id + customer.id,
                        nopol: ((historyCustomerVehicle?.kodeKotaNoPol?.m116ID) + " " + (historyCustomerVehicle?.t183NoPolTengah) + " " + (historyCustomerVehicle?.t183NoPolBelakang)),
                        customer: ((customer?.t182NamaDepan) + " " + (customer?.t182NamaBelakang)),
                        peran: (customer?.peranCustomer?.m115NamaPeranCustomer)
                ]
            }

        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def historyKepemilikanMobilDatatablesList() {
        def historyCustomer = HistoryCustomer.read(params.historyCustomerId)

        def customerVehicles = MappingCustVehicle.withCriteria {
            projections {
                groupProperty("customerVehicle")
            }
            eq("customer", historyCustomer?.customer)
        }

        def ret

        def c = MappingCustVehicle.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            customerVehicles.size() == 0 ? sqlRestriction("1!=1") : inList("customerVehicle", customerVehicles)
        }

        def rows = []

        def perans = [PeranCustomer.findByM115NamaPeranCustomerIlike("Pemilik"), PeranCustomer.findByM115NamaPeranCustomerIlike("Pemilik dan Penanggung Jawab")]

        results.each { mapping ->
            def historyCustomerVehicle = HistoryCustomerVehicle.findByCustomerVehicle(mapping.customerVehicle)
            def historyCustomers = HistoryCustomer.findAllByCustomerAndPeranCustomerInList(mapping.customer, perans)

            historyCustomers.each { customer ->
                rows << [
                        id: mapping.id + customer.id,
                        nopol: ((historyCustomerVehicle?.kodeKotaNoPol?.m116ID) + " " + (historyCustomerVehicle?.t183NoPolTengah) + " " + (historyCustomerVehicle?.t183NoPolBelakang)),
                        customer: ((customer?.t182NamaDepan) + " " + (customer?.t182NamaBelakang)),
                        company: (customer?.company?.namaPerusahaan)?customer?.company?.namaPerusahaan:'-'
                ]
            }


        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = HistoryCustomer.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            createAlias("company", "company", CriteriaSpecification.LEFT_JOIN)
            createAlias("agama", "agama", CriteriaSpecification.LEFT_JOIN)

            eq("staDel", '0')

            if (params."sCriteria_customer") {
                eq("customer", params."sCriteria_customer")
            }

            if (params."sCriteria_t182ID") {
                eq("t182ID", params."sCriteria_t182ID")
            }

            if (params."sCriteria_company") {
                ilike("company.namaPerusahaan", "%" + params."sCriteria_company" + "%")
            }


            if (params."sCriteria_fullNama") {
                ilike("fullNama", "%" + (params."sCriteria_fullNama" as String) + "%")
            }

//            if (params."sCriteria_t182NamaDepan") {
//                ilike("t182NamaDepan", "%" + (params."sCriteria_t182NamaDepan" as String) + "%")
//            }

//            if (params."sCriteria_t182NamaBelakang") {
//                ilike("t182NamaBelakang", "%" + (params."sCriteria_t182NamaBelakang" as String) + "%")
//            }

            if (params."sCriteria_t182NoTelpRumah") {
                ilike("t182NoTelpRumah", "%" + (params."sCriteria_t182NoTelpRumah" as String) + "%")
            }

            if (params."sCriteria_t182NoTelpKantor") {
                ilike("t182NoTelpKantor", "%" + (params."sCriteria_t182NoTelpKantor" as String) + "%")
            }

            if (params."sCriteria_t182NoFax") {
                ilike("t182NoFax", "%" + (params."sCriteria_t182NoFax" as String) + "%")
            }

            if (params."sCriteria_t182NoHp") {
                ilike("t182NoHp", "%" + (params."sCriteria_t182NoHp" as String) + "%")
            }

            if (params."sCriteria_t182Email") {
                ilike("t182Email", "%" + (params."sCriteria_t182Email" as String) + "%")
            }

            if (params."sCriteria_t182Alamat") {
                ilike("t182Alamat", "%" + (params."sCriteria_t182Alamat" as String) + "%")
            }

            if (params."sCriteria_t182AlamatNPWP") {
                ilike("t182AlamatNPWP", "%" + (params."sCriteria_t182AlamatNPWP" as String) + "%")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            def perusahaan = it.company?it.company?.toString()+', '+it.company?.jenisPerusahaan?.namaJenisPerusahaan.toString():''
            rows << [

                    id: it.id,


//                    t182NamaDepan: it.t182NamaDepan?.toString(),
                    fullNama: it.fullNama?.toString(),

//                    t182NamaBelakang: it.t182NamaBelakang?.toString() ? it.t182NamaBelakang?.toString():"-" ,

                    t182Alamat: it.t182Alamat?.toString() ? it.t182Alamat?.toString():"-",

                    t182NoTelpRumah: it.t182NoTelpRumah?.toString() ? it.t182NoTelpRumah?.toString():"-" ,

                    t182NoTelpKantor: it.t182NoTelpKantor?.toString() ? it.t182NoTelpKantor?.toString():"-",

                    t182NoHp: it.t182NoHp?.toString()?it.t182NoHp?.toString():"-",

                    company: perusahaan ? perusahaan:"-"
            ]
        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        def historyCustomer = new HistoryCustomer(params)
        def currentRequest = RequestContextHolder.requestAttributes
        currentRequest.session['detailCustomerCars'] = null
        [historyCustomerInstance: historyCustomer]
    }

    def save() {
        def historyCustomerInstance = new HistoryCustomer(params)
//        if (params?.t182TglLahir_dp && !params?.t182TglLahir_dp?.toString()?.empty) {
//            historyCustomerInstance.t182TglLahir = new SimpleDateFormat("dd/MM/yyyy").parse(params.t182TglLahir_dp)
//        }

        if (session.customerFoto) {
            File fileImage = new File(session?.customerFoto?.toString())
            historyCustomerInstance.t182Foto = fileImage.bytes
            session.customerFoto = null
        }
        historyCustomerInstance.t182ID = generateCodeService.codeGenerateSequence("T182_T102_ID", session.userCompanyDealer)

        def tempCustomer = new Customer()
        tempCustomer.company = historyCustomerInstance.company
        tempCustomer?.dateCreated = datatablesUtilService?.syncTime()
        tempCustomer?.lastUpdated = datatablesUtilService?.syncTime()
        tempCustomer.peranCustomer = historyCustomerInstance.peranCustomer
        tempCustomer.t102ID = historyCustomerInstance.t182ID
        tempCustomer.staDel = '0'
        tempCustomer.lastUpdProcess = "INSERT"
        tempCustomer?.companyDealer = session.userCompanyDealer
        tempCustomer.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        tempCustomer.save()
        historyCustomerInstance.customer = tempCustomer
        historyCustomerInstance.staDel = '0'
     //        Customer tempCustomer = new Customer()
//        tempCustomer.company = company
//        tempCustomer.dateCreated = new Date()
//        tempCustomer.lastUpdated = new Date()
//        tempCustomer.peranCustomer = peranCustomer
//        tempCustomer.t102ID = t182ID
//        tempCustomer.staDel = 0
//        tempCustomer.lastUpdProcess = "INSERT"
//        tempCustomer.createdBy = createdBy
//
//        tempCustomer.save()
//        customer = tempCustomer


        if (!historyCustomerInstance.save(flush: true)) {
            render(view: "create", model: [historyCustomerInstance: historyCustomerInstance])
            return
        }

        def currentRequest = RequestContextHolder.requestAttributes
        def rows = currentRequest.session['detailCustomerCars']
        if(rows){
            rows.each{
                MappingCustVehicle mappingCustVehicle = new MappingCustVehicle()
                mappingCustVehicle.customerVehicle = CustomerVehicle.get(it.id)
                mappingCustVehicle.customer = tempCustomer
                mappingCustVehicle.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                mappingCustVehicle?.dateCreated = datatablesUtilService?.syncTime()
                mappingCustVehicle.lastUpdProcess ='INSERT'
                mappingCustVehicle.lastUpdated = datatablesUtilService?.syncTime()
                mappingCustVehicle.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                mappingCustVehicle.save()
            }
        }
        currentRequest.session['detailCustomerCars']=null
        //menghapus code pada tabel urutbelumterpakai
        //generateCodeService.hapusCodeBelumTerpakai("T182_T102_ID",session.userCompanyDealer,historyCustomerInstance?.t182ID)

        flash.message = message(code: 'default.created.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), historyCustomerInstance.id])
        redirect(action: "show", id: historyCustomerInstance.id)
    }

    def show(Long id) {
        def historyCustomerInstance = HistoryCustomer.get(id)
        if (!historyCustomerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), id])
            redirect(action: "list")
            return
        }

        [historyCustomerInstance: historyCustomerInstance]
    }

    def edit(Long id) {
        def historyCustomerInstance = HistoryCustomer.get(id)
        def currentRequest = RequestContextHolder.requestAttributes
        currentRequest.session['detailCustomerCars'] = null
        if (!historyCustomerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), id])
            redirect(action: "list")
            return
        }

        [historyCustomerInstance: historyCustomerInstance]
    }

    def update(Long id, Long version) {
        def historyCustomerInstance = HistoryCustomer.get(id)
        if (!historyCustomerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), id])
            redirect(action: "list")
            return
        }
      //  historyCustomerInstance.staDel = '1'

        def oldPhoto = historyCustomerInstance.t182Foto

        //def parent = historyCustomerInstance.customer

        if (version != null) {
            if (historyCustomerInstance.version > version) {

                historyCustomerInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'historyCustomer.label', default: 'HistoryCustomer')] as Object[],
                        "Another user has updated this HistoryCustomer while you were editing")
                render(view: "edit", model: [historyCustomerInstance: historyCustomerInstance])
                return
            }
        }

        //update parent (Customer)
//        parent.lastUpdProcess = 'UPDATE'
//        parent.lastUpdated = new Date()
//        try{
//            parent.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
//        }catch(Exception e){}
//
//        historyCustomerInstance.save()
        //parent.save()


//        historyCustomerInstance.properties = params
//        def historyCustomerInstance = new HistoryCustomer()
        params.lastUpdated = datatablesUtilService?.syncTime()
        historyCustomerInstance.properties = params
        historyCustomerInstance.customer = historyCustomerInstance.customer
        historyCustomerInstance.staDel = '0'

//        if (params?.t182TglLahir_dp && !params?.t182TglLahir_dp?.toString()?.empty) {
//            historyCustomerInstance.t182TglLahir = new SimpleDateFormat("MM/dd/yyyy").parse(params.t182TglLahir_dp)
//        }

        if (session.customerFoto) {
            File fileImage = new File(session?.customerFoto?.toString())
//            historyCustomerInstance.t182Foto = fileImage.bytes
            historyCustomerInstance.t182Foto = fileImage.bytes
            session.customerFoto = null
        }else{
            historyCustomerInstance.t182Foto = oldPhoto
        }

        //if (!historyCustomerInstance.save(flush: true)) {
        if (!historyCustomerInstance.save(flush: true)) {
            //render(view: "edit", model: [historyCustomerInstance: historyCustomerInstance])
            render(view: "edit", model: [historyCustomerInstance: historyCustomerInstance])
            return
        }

        def currentRequest = RequestContextHolder.requestAttributes
        def rows = currentRequest.session['detailCustomerCars']
        if(rows){
            rows.each{
                MappingCustVehicle mappingCustVehicle = new MappingCustVehicle()
                mappingCustVehicle.customerVehicle = CustomerVehicle.get(it.id)
                mappingCustVehicle.customer = historyCustomerInstance.customer
                mappingCustVehicle.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                mappingCustVehicle?.dateCreated = datatablesUtilService?.syncTime()
                mappingCustVehicle.lastUpdProcess = 'INSERT'
                mappingCustVehicle?.lastUpdated = datatablesUtilService?.syncTime()
                mappingCustVehicle.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                mappingCustVehicle.save()
            }
        }
        currentRequest.session['detailCustomerCars']=null

        //flash.message = message(code: 'default.updated.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), historyCustomerInstance.id])
        flash.message = message(code: 'default.updated.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), historyCustomerInstance.id])
        //redirect(action: "show", id: historyCustomerInstance.id)
        redirect(action: "show", id: historyCustomerInstance.id)
    }

    def delete(Long id) {
        def historyCustomerInstance = HistoryCustomer.get(id)
        if (!historyCustomerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), id])
            redirect(action: "list")
            return
        }

        try {
            def customer = historyCustomerInstance.customer
            historyCustomerInstance.delete(flush: true)

            def allHistory = HistoryCustomer.findAllByCustomer(customer)
            allHistory.each {
                it.delete(flush: true)
            }

            def mapping = MappingCustVehicle.findAllByCustomer(customer)
            mapping.each {
                it.delete(flush: true)
            }

            customer.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(HistoryCustomer, params)

            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(HistoryCustomer, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def insertJenisID(){
        def jenisID = params.jenisID

        def jenisIdCard = new JenisIdCard()
        def check = JenisIdCard.findByM060JenisIDCardIlikeAndStaDel("%"+jenisID+"%","0")
        def res = [:]

        if(check){
            println "ERROR"
            res.error = "duplicate"
            render res as JSON
        }else{
            jenisIdCard.m060ID = generateCodeService.codeGenerateSequence("M060_ID",null)
            jenisIdCard.m060JenisIDCard = jenisID
            jenisIdCard.dateCreated = datatablesUtilService?.syncTime()
            jenisIdCard.lastUpdated = datatablesUtilService?.syncTime()
            jenisIdCard?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            jenisIdCard?.lastUpdProcess = "INSERT"
            jenisIdCard?.staDel = "0"

            jenisIdCard.save(flush:true)

            def jenisIdData = JenisIdCard.last();
            res.put("id", jenisIdData.id)
            res.put("jenisID", jenisIdData.m060JenisIDCard)
            render res as JSON
        }
    }

    def insertAgama(){
        def agamaAdd = params.agamaAdd

        def agama = new Agama()
        def check = Agama.findByM061NamaAgamaIlikeAndStaDel("%"+agamaAdd+"%","0")
        def res = [:]

        if(check){
            println "ERROR"
            res.error = "duplicate"
            render res as JSON
        }else{
            agama.m061ID = generateCodeService.codeGenerateSequence("M061_ID",null)
            agama.m061NamaAgama = agamaAdd
            agama?.dateCreated = datatablesUtilService?.syncTime()
            agama?.lastUpdated = datatablesUtilService?.syncTime()
            agama?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            agama?.lastUpdProcess = "INSERT"
            agama?.staDel = "0"

            agama.save(flush:true)

            def agamaData = Agama.last();
            res.put("id", agamaData.id)
            res.put("namaAgama", agamaData.m061NamaAgama)
            render res as JSON
        }
    }

    def insertNikah(){
        def nikahAdd = params.nikahAdd

        def nikah = new Nikah()
        def check = Nikah.findByM062StaNikahIlikeAndStaDel("%"+nikahAdd+"%","0")
        def res = [:]

        if(check){
            println "ERROR"
            res.error = "duplicate"
            render res as JSON
        }else{
            nikah.m062ID = generateCodeService.codeGenerateSequence("M062_ID",null)
            nikah.m062StaNikah = nikahAdd
            nikah?.dateCreated = datatablesUtilService?.syncTime()
            nikah?.lastUpdated = datatablesUtilService?.syncTime()
            nikah?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            nikah?.lastUpdProcess = "INSERT"
            nikah?.staDel = "0"

            nikah.save(flush:true)

            def nikahData = Nikah.last();
            res.put("id", nikahData.id)
            res.put("statusNikah", nikahData.m062StaNikah)
            render res as JSON
        }
    }

    def insertHobby(){
        def hobbyAdd = params.hobbyAdd

        def hobby = new Hobby()
        def check = Hobby.findByM063NamaHobbyIlikeAndStaDel("%"+hobbyAdd+"%","0")
        def res = [:]

        if(check){
            println "ERROR"
            res.error = "duplicate"
            render res as JSON
        }else{
            hobby.m063ID = generateCodeService.codeGenerateSequence("M063_ID",null)
            hobby.m063NamaHobby = hobbyAdd
            hobby?.dateCreated = datatablesUtilService?.syncTime()
            hobby?.lastUpdated = datatablesUtilService?.syncTime()
            hobby?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            hobby?.lastUpdProcess = "INSERT"
            hobby?.staDel = "0"

            hobby.save(flush:true)

            def hobbyData = Hobby.last();
            res.put("id", hobbyData.id)
            res.put("hobby", hobbyData.m063NamaHobby)
            render res as JSON
        }
    }

    def mappingVehicle(){
        render (view: 'mappingVehicle', params: params)
    }

    def mappingVehicleDataTables(){
        def dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params

        def customer = HistoryCustomer.get(params.historyCustomerId)?.customer
        def mapping = MappingCustVehicle.findAllByCustomer(customer)
        def rowss = []
        mapping.each {
            def historyCustomerVehicle = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(it.customerVehicle,'0')
            if(historyCustomerVehicle) {
                rowss << [
                        id     : historyCustomerVehicle?.id,
                        pemilik: historyCustomerVehicle?.t183NamaSTNK,
                        nopol  : historyCustomerVehicle?.kodeKotaNoPol?.m116ID + ' ' + historyCustomerVehicle?.t183NoPolTengah + ' ' + historyCustomerVehicle?.t183NoPolBelakang,
                        model  : historyCustomerVehicle?.fullModelCode?.modelName?.m104NamaModelName
                ]
            }
        }

        def currentRequest = RequestContextHolder.requestAttributes
        def sessRows = currentRequest?.session.detailCustomerCars
        sessRows?.each{
            rowss << it
        }

        def inListRow = new ArrayList()
        rowss.each {
            inListRow.add((long)it.id)
        }

        def c = HistoryCustomerVehicle.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(inListRow?.size()>0) {
                not {'in'("id",inListRow)}
            }
            if (params."sCriteria_nopol") {
                or {
                    or {
                        kodeKotaNoPol {
                            ilike("m116ID", "%" + (params."sCriteria_nopol" as String) + "%")
                        }
                        ilike("t183NoPolTengah", "%" + (params."sCriteria_nopol" as String) + "%")
                    }
                    ilike("t183NoPolBelakang", "%" + (params."sCriteria_nopol" as String) + "%")
                }
            }

            if (params."sCriteria_customerVehicle") {
                customerVehicle {
                    ilike("t103VinCode", "%" + (params."sCriteria_customerVehicle" as String) + "%")
                }
            }

            eq("staDel", "0")
        }

        def rows = []

        results.each {
            rows << [

                    id: it.customerVehicle?.id,

                    nopol: it.kodeKotaNoPol?.m116ID + " " + it.t183NoPolTengah + " " + it.t183NoPolBelakang,

                    customerVehicle: it.customerVehicle?.t103VinCode
            ]
        }

        def ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }
}