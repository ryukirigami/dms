package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class BumperPaintingTimeController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]

        session.exportParams = params

        def c = BumperPaintingTime.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m045TglBerlaku") {
                ge("m045TglBerlaku", params."sCriteria_m045TglBerlaku")
                lt("m045TglBerlaku", params."sCriteria_m045TglBerlaku" + 1)
            }

            if (params."sCriteria_masterPanel") {
			masterPanel{
				ilike("m094NamaPanel","%"+(params."sCriteria_masterPanel")+"%")
				}
            }

            if (params."sCriteria_m0452New") {
                eq("m0452New",Double.parseDouble (params."sCriteria_m0452New"))
            }

            if (params."sCriteria_m0453New") {
                eq("m0453New",Double.parseDouble (params."sCriteria_m0453New"))
            }

            if (params."sCriteria_m0452Def") {
                eq("m0452Def",Double.parseDouble (params."sCriteria_m0452Def"))
            }

            if (params."sCriteria_m0453Def") {
                eq("m0453Def",Double.parseDouble (params."sCriteria_m0453Def"))
            }

            if (params."sCriteria_m0452Scr") {
                eq("m0452Scr",Double.parseDouble (params."sCriteria_m0452Scr"))
            }

            if (params."sCriteria_m0453Scr") {
                eq("m0453Scr",Double.parseDouble (params."sCriteria_m0453Scr"))
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_companyDealer") {
                eq("companyDealer", params."sCriteria_companyDealer")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m045TglBerlaku: it.m045TglBerlaku ? it.m045TglBerlaku.format(dateFormat) : "",

                    masterPanel: it.masterPanel?.m094NamaPanel,

                    m0452New: it.m0452New,

                    m0453New: it.m0453New,

                    m0452Def: it.m0452Def,

                    m0453Def: it.m0453Def,

                    m0452Scr: it.m0452Scr,

                    m0453Scr: it.m0453Scr,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

                    companyDealer: it.companyDealer,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [bumperPaintingTimeInstance: new BumperPaintingTime(params)]
    }

    def save() {
        def bumperPaintingTimeInstance = new BumperPaintingTime(params)
        bumperPaintingTimeInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        bumperPaintingTimeInstance?.lastUpdProcess = "INSERT"
        def cek = BumperPaintingTime.createCriteria().list() {
            and{
                eq("m045TglBerlaku",bumperPaintingTimeInstance.m045TglBerlaku)
                eq("masterPanel",bumperPaintingTimeInstance.masterPanel)
                eq("m0452Def",bumperPaintingTimeInstance.m0452Def)
                eq("m0452New",bumperPaintingTimeInstance.m0452New)
                eq("m0452Scr",bumperPaintingTimeInstance.m0452Scr)
                eq("m0453Def",bumperPaintingTimeInstance.m0453Def)
                eq("m0453New",bumperPaintingTimeInstance.m0453New)
                eq("m0453Scr",bumperPaintingTimeInstance.m0453Scr)
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }
        if (bumperPaintingTimeInstance.getM0452New()<=0) {
            flash.message = message(code: 'default.error.created.bumperPaintingTime.message', default: 'Inputan 2 Coat New Panel harus lebih besar dari 0.')
            render(view: "create", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }
        if (bumperPaintingTimeInstance.getM0452Def()<=0) {
            flash.message = message(code: 'default.error.created.bumperPaintingTime.message', default: 'Inputan 2 Coat Deformation Repair harus lebih besar dari 0.')
            render(view: "create", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }
        if (bumperPaintingTimeInstance.getM0452Scr()<=0) {
            flash.message = message(code: 'default.error.created.bumperPaintingTime.message', default: 'Inputan 2 Coat Scratch Repair harus lebih besar dari 0.')
            render(view: "create", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }
        if (bumperPaintingTimeInstance.getM0453New()<=0) {
            flash.message = message(code: 'default.error.created.bumperPaintingTime.message', default: 'Inputan 3 Coat New Panel harus lebih besar dari 0.')
            render(view: "create", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }
        if (bumperPaintingTimeInstance.getM0453Def()<=0) {
            flash.message = message(code: 'default.error.created.bumperPaintingTime.message', default: 'Inputan 3 Coat Deformation Repair harus lebih besar dari 0.')
            render(view: "create", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }
        if (bumperPaintingTimeInstance.getM0453Scr()<=0) {
            flash.message = message(code: 'default.error.created.bumperPaintingTime.message', default: 'Inputan 3 Coat Scratch Repair harus lebih besar dari 0.')
            render(view: "create", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }

        if (!bumperPaintingTimeInstance.save(flush: true)) {
            render(view: "create", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'bumperPaintingTime.label', default: 'Plastic Bumper Painting Time'), bumperPaintingTimeInstance.id])
        redirect(action: "show", id: bumperPaintingTimeInstance.id)
    }

    def show(Long id) {
        def bumperPaintingTimeInstance = BumperPaintingTime.get(id)
        if (!bumperPaintingTimeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bumperPaintingTime.label', default: 'BumperPaintingTime'), id])
            redirect(action: "list")
            return
        }

        [bumperPaintingTimeInstance: bumperPaintingTimeInstance]
    }

    def edit(Long id) {
        def bumperPaintingTimeInstance = BumperPaintingTime.get(id)
        if (!bumperPaintingTimeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bumperPaintingTime.label', default: 'BumperPaintingTime'), id])
            redirect(action: "list")
            return
        }

        [bumperPaintingTimeInstance: bumperPaintingTimeInstance]
    }

    def update(Long id, Long version) {
        params.m0452Def=params.m0452Def.replace(",","")
        params.m0452New=params.m0452New.replace(",","")
        params.m0452Scr=params.m0452Scr.replace(",","")
        params.m0453Def=params.m0453Def.replace(",","")
        params.m0453New=params.m0453New.replace(",","")
        params.m0453Scr=params.m0453Scr.replace(",","")
        def bumperPaintingTimeInstance = BumperPaintingTime.get(id)
        if (!bumperPaintingTimeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bumperPaintingTime.label', default: 'BumperPaintingTime'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (bumperPaintingTimeInstance.version > version) {

                bumperPaintingTimeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'bumperPaintingTime.label', default: 'BumperPaintingTime')] as Object[],
                        "Another user has updated this BumperPaintingTime while you were editing")
                render(view: "edit", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
                return
            }
        }
        def cek = BumperPaintingTime.createCriteria()
        def result = cek.list() {
            and{
                eq("m045TglBerlaku",params.m045TglBerlaku)
                eq("masterPanel",MasterPanel.findById(Long.parseLong(params.masterPanel.id)))
                eq("m0452Def",Double.parseDouble(params.m0452Def))
                eq("m0452New",Double.parseDouble(params.m0452New))
                eq("m0452Scr",Double.parseDouble(params.m0452Scr))
                eq("m0453Def",Double.parseDouble(params.m0453Def))
                eq("m0453New",Double.parseDouble(params.m0453New))
                eq("m0453Scr",Double.parseDouble(params.m0453Scr))
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }

        bumperPaintingTimeInstance.properties = params
        bumperPaintingTimeInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        bumperPaintingTimeInstance?.lastUpdProcess = "UPDATE"
        if (bumperPaintingTimeInstance.getM0452New()<=0) {
            flash.message = message(code: 'default.error.created.bumperPaintingTime.message', default: 'Inputan 2 Coat New Panel harus lebih besar dari 0.')
            render(view: "edit", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }
        if (bumperPaintingTimeInstance.getM0452Def()<=0) {
            flash.message = message(code: 'default.error.created.bumperPaintingTime.message', default: 'Inputan 2 Coat Deformation Repair harus lebih besar dari 0.')
            render(view: "edit", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }
        if (bumperPaintingTimeInstance.getM0452Scr()<=0) {
            flash.message = message(code: 'default.error.created.bumperPaintingTime.message', default: 'Inputan 2 Coat Scratch Repair harus lebih besar dari 0.')
            render(view: "edit", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }
        if (bumperPaintingTimeInstance.getM0453New()<=0) {
            flash.message = message(code: 'default.error.created.bumperPaintingTime.message', default: 'Inputan 3 Coat New Panel harus lebih besar dari 0.')
            render(view: "edit", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }
        if (bumperPaintingTimeInstance.getM0453Def()<=0) {
            flash.message = message(code: 'default.error.created.bumperPaintingTime.message', default: 'Inputan 3 Coat Deformation Repair harus lebih besar dari 0.')
            render(view: "edit", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }
        if (bumperPaintingTimeInstance.getM0453Scr()<=0) {
            flash.message = message(code: 'default.error.created.bumperPaintingTime.message', default: 'Inputan 3 Coat Scratch Repair harus lebih besar dari 0.')
            render(view: "edit", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }
        if (!bumperPaintingTimeInstance.save(flush: true)) {
            render(view: "edit", model: [bumperPaintingTimeInstance: bumperPaintingTimeInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'bumperPaintingTime.label', default: 'Plastic Bumper Painting Time'), bumperPaintingTimeInstance.id])
        redirect(action: "show", id: bumperPaintingTimeInstance.id)
    }

    def delete(Long id) {
        def bumperPaintingTimeInstance = BumperPaintingTime.get(id)
        if (!bumperPaintingTimeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bumperPaintingTime.label', default: 'BumperPaintingTime'), id])
            redirect(action: "list")
            return
        }

        try {
            bumperPaintingTimeInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            bumperPaintingTimeInstance?.lastUpdProcess = "DELETE"
            bumperPaintingTimeInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'bumperPaintingTime.label', default: 'BumperPaintingTime'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'bumperPaintingTime.label', default: 'BumperPaintingTime'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(BumperPaintingTime, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(BumperPaintingTime, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
