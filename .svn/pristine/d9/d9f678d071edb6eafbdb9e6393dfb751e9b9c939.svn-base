package com.kombos.hrd

import com.kombos.administrasi.KegiatanApproval
import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import com.kombos.maintable.ApprovalT770
import com.kombos.parts.StatusApproval
import org.springframework.web.context.request.RequestContextHolder

class HistoryWarningKaryawanService implements AfterApprovalInterface {
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	def datatablesUtilService

	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = HistoryWarningKaryawan.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(!params.companyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                or{
                    eq("companyDealer",params?.companyDealer)
                    karyawan {
                        eq("branch",params?.companyDealer)
                    }
                }
            }
            if(params.sCriteria_companyDealer){
                karyawan {
                    branch{
                        eq("id", params.sCriteria_companyDealer as Long)
                    }
                }
            }

            if(params."sCriteria_tanggalWarning"){
				ge("tanggalWarning",params."sCriteria_tanggalWarning")
				lt("tanggalWarning",params."sCriteria_tanggalWarning" + 1)
			}

			if(params."sCriteria_karyawan"){
                karyawan {
                    ilike ("nama", "%" + params."sCriteria_karyawan" + "%")
                }
				eq("karyawan",params."sCriteria_karyawan")
			}

            if(params."dari"){
                if(params.idKaryawan){
                    karyawan {
                        eq("id", params.idKaryawan.toLong())
                    }
                }else{
                    karyawan {
                        eq("id", -10000.toLong())
                    }
                }
            }

            if(params."sCriteria_keterangan"){
				ilike("keterangan","%" + (params."sCriteria_keterangan" as String) + "%")
			}

			if(params."sCriteria_warningKaryawan"){
                warningKaryawan {
                    ilike("warningKaryawan", "%" + params."sCriteria_warningKaryawan" + "%")
                }
			}


			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}

		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,

			            workshop: it?.karyawan?.branch?.m011NamaWorkshop,

					    tanggalWarning: it.tanggalWarning?it.tanggalWarning.format(dateFormat):"",
			
						karyawan: it.karyawan?.nama,
			
						keterangan: it.keterangan,
			
						warningKaryawan: it.warningKaryawan?.warningKaryawan,

                        approval : it.staApproval==StatusApproval.WAIT_FOR_APPROVAL?"Belum":(it.staApproval==StatusApproval.APPROVED?"Approved":"Un Approved"),
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryWarningKaryawan", params.id] ]
			return result
		}

		result.historyWarningKaryawanInstance = HistoryWarningKaryawan.get(params.id)

		if(!result.historyWarningKaryawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryWarningKaryawan", params.id] ]
			return result
		}

		result.historyWarningKaryawanInstance = HistoryWarningKaryawan.get(params.id)

		if(!result.historyWarningKaryawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryWarningKaryawan", params.id] ]
			return result
		}

		result.historyWarningKaryawanInstance = HistoryWarningKaryawan.get(params.id)

		if(!result.historyWarningKaryawanInstance)
			return fail(code:"default.not.found.message")

		try {
			result.historyWarningKaryawanInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		HistoryWarningKaryawan.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.historyWarningKaryawanInstance && m.field)
					result.historyWarningKaryawanInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["HistoryWarningKaryawan", params.id] ]
				return result
			}

			result.historyWarningKaryawanInstance = HistoryWarningKaryawan.get(params.id)

			if(!result.historyWarningKaryawanInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.historyWarningKaryawanInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.historyWarningKaryawanInstance.properties = params

			if(result.historyWarningKaryawanInstance.hasErrors() || !result.historyWarningKaryawanInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryWarningKaryawan", params.id] ]
			return result
		}

		result.historyWarningKaryawanInstance = new HistoryWarningKaryawan()
		result.historyWarningKaryawanInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.historyWarningKaryawanInstance && m.field)
				result.historyWarningKaryawanInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["HistoryWarningKaryawan", params.id] ]
			return result
		}

        if (params.karyawanId) {
            params.karyawan = Karyawan.findById(new Long(params.karyawanId))
        }
        params.companyDealer = params.cd
        if(params.cd?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
            params.staApproval = StatusApproval.APPROVED
        }
		result.historyWarningKaryawanInstance = new HistoryWarningKaryawan(params)

		if(result.historyWarningKaryawanInstance.hasErrors() || !result.historyWarningKaryawanInstance.save(flush: true))
			return fail(code:"default.not.created.message")

        if(!params.cd?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
            def approval = new ApprovalT770(
                    t770FK:result.historyWarningKaryawanInstance.id as String,
                    kegiatanApproval: KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.WARNING_KARYAWAN),
                    t770NoDokumen: "-",
                    t770TglJamSend: datatablesUtilService?.syncTime(),
                    t770Pesan: "Silahkan Di Approve",
                    dateCreated: datatablesUtilService?.syncTime(),
                    companyDealer: params?.cd,
                    lastUpdated: datatablesUtilService?.syncTime()

            )
            approval.save(flush:true)
            approval.errors.each{ //println it
			}
        }
		// success
		return result
	}
	
	def updateField(def params){
		def historyWarningKaryawan =  HistoryWarningKaryawan.findById(params.id)
		if (historyWarningKaryawan) {
			historyWarningKaryawan."${params.name}" = params.value
			historyWarningKaryawan.save()
			if (historyWarningKaryawan.hasErrors()) {
				throw new Exception("${historyWarningKaryawan.errors}")
			}
		}else{
			throw new Exception("HistoryWarningKaryawan not found")
		}
	}
    def afterApproval(String fks,
                      StatusApproval staApproval,
                      Date tglApproveUnApprove,
                      String alasanUnApprove,
                      String namaUserApproveUnApprove) {
        fks.split(ApprovalT770.FKS_SEPARATOR).each {
            def warning= HistoryWarningKaryawan.get(it as Long)
            warning.staApproval = staApproval
            warning.save(flush: true)
        }
    }
}
