package com.kombos.kriteriaReport

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.sec.shiro.User
import com.kombos.customerprofile.FA
import com.kombos.finance.MonthlyBalanceController
import com.kombos.maintable.JenisFA
import com.kombos.maintable.StatusActual
import com.kombos.maintable.VehicleFA
import com.kombos.production.FinalInspection
import com.kombos.reception.Appointment
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import com.kombos.woinformation.PartsRCP
import org.hibernate.criterion.CriteriaSpecification

class CustomerFAService {

    def datatablesCustomerFADaily(def params, def type){
        Date awal = new Date()?.parse("dd-MM-yyyy",params?.tanggal)
        Date akhir = new Date()?.parse("dd-MM-yyyy",params?.tanggalAkhir)
        CompanyDealer company = params?.companyDealer
        String qBooking = "";
        if(params?.staBooking=="0"){
            qBooking = "and rcp.t401NoAppointment is not null ";
        }else if(params?.staBooking=="1"){
            qBooking = "and rcp.t401NoAppointment is null ";
        }
        String qFA = "";
        if(params?.faStatus=="0"){
            qBooking = "and vfa.t185StaSudahDikerjakan = '1' ";
        }else if(params?.faStatus=="1"){
            qBooking = "and vfa.t185StaSudahDikerjakan is null ";
        }
        String qJenisFA = "";
        if(params?.jenisFA!=""){
            qJenisFA = "and fa.jenisFA.id in ("+params?.jenisFA+") ";
        }
        String qmainDealer = "";
        if(params?.mainDealer!=""){
            qmainDealer = "and vfa.t185MainDealer in ("+params?.mainDealer+") ";
        }
        def result = Reception.executeQuery("select rcp,vfa,fa from Reception rcp, VehicleFA vfa, FA fa where rcp.staDel = '0' and rcp.staSave = '0' and rcp.t401TglJamRencana >=? and rcp.t401TglJamRencana <? "+qBooking+qFA+qJenisFA+qmainDealer+" and rcp.historyCustomerVehicle.customerVehicle=vfa.customerVehicle and vfa.fa.id = fa.id and rcp.companyDealer = ? order by rcp.t401TglJamRencana ",[awal,(akhir+1).clearTime() , company]);

        def rows = []
        result.each {
            Reception rcp = it[0]
            VehicleFA vfa = it[1]
            FA fa = it[2]
            def findApp = Appointment.findByReceptionAndStaDelAndStaSave(rcp,"0","0");
            def partsCek = PartsRCP.createCriteria().list {
                eq("staDel","0")
                eq("reception",rcp)
                or{
                    ilike("t403StaTambahKurang","%0%")
                    and{
                        not{
                            ilike("t403StaTambahKurang","%1%")
                            ilike("t403StaApproveTambahKurang","%1%")
                        }
                    }
                    and{
                        isNull("t403StaTambahKurang")
                        isNull("t403StaApproveTambahKurang")
                    }
                }
            }
            String partName = "",partCode=""
            if(vfa?.t185StaSudahDikerjakan && vfa?.t185StaSudahDikerjakan=="1"){
                int ax = 0
                partsCek.each {
                    ax++;
                    partCode+=it?.goods?.m111ID
                    partName+=it?.goods?.m111Nama
                    if(ax!=partsCek?.size()){
                        partCode+=" , "
                        partName+=" , "
                    }
                }
            }
            def repair = Actual.findByReceptionAndStatusActualAndStaDel(rcp,StatusActual.findByM452StatusActualIlike("%Clock On%"),"0");
            def fi = FinalInspection.findByReception(rcp)
            rows << [
                    column_0: rcp?.customerIn?.t400TglCetakNoAntrian ? rcp?.customerIn?.t400TglCetakNoAntrian?.format("dd-MM-yyyy HH:mm") : rcp?.customerIn?.dateCreated?.format("dd-MM-yyyy HH:mm"),
                    column_1: findApp ? findApp?.dateCreated?.format("dd-MM-yyyy HH:mm") : "-",
                    column_2: findApp ? "Yes" : "No",
                    column_3: rcp?.historyCustomerVehicle?.fullNoPol,
                    column_4: rcp?.historyCustomer?.fullNama,
                    column_5: rcp?.t401NoWO,
                    column_6: rcp?.historyCustomerVehicle?.customerVehicle?.t103VinCode,
                    column_7: rcp?.historyCustomerVehicle?.t183ThnBlnRakit ? rcp?.historyCustomerVehicle?.t183ThnBlnRakit?.substring(0,4) : "",
                    column_8: rcp?.historyCustomerVehicle?.fullModelCode?.t110FullModelCode,
                    column_9: rcp?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                    column_10: rcp?.historyCustomerVehicle?.warna?.m092NamaWarna,
                    column_11: vfa?.t185MainDealer,
                    column_12: vfa?.t185Dealer,
                    column_13: fa?.jenisFA?.m184NamaJenisFA,
                    column_14: fa?.m185NamaFA,
                    column_15: vfa?.t185StaSudahDikerjakan && vfa?.t185StaSudahDikerjakan=="1" ? "Finish" : "Not Yet",
                    column_16: fi ? "Yes" : "No",
                    column_17: repair ? "Yes" : "No",
                    column_18: repair ? partName : "",
                    column_19: repair ? partCode : ""
            ]
        }
        if(result?.size()<1){
            rows << [
                    column_0: "",
                    column_1: "",
                    column_2: "",
                    column_3: "",
                    column_4: "",
                    column_5: "",
                    column_6: "",
                    column_7: "",
                    column_8: "",
                    column_9: "",
                    column_10: "",
                    column_11: "",
                    column_12: "",
                    column_13: "",
                    column_14: "",
                    column_15: "",
                    column_16: "",
                    column_17: "",
                    column_18: "",
                    column_19: ""
            ]
        }
        return rows
    }

    def datatablesCustomerFAMonthly(def params, def type){
        CompanyDealer company = params?.companyDealer
        def rows = []
        def listID = []
        if(params?.jenisFA!=""){
            listID = params?.jenisFA?.toString()?.split(",")
        }else{
            listID = JenisFA?.list()?.id
        }
        def listMD = []
        if(params?.mainDealer!=""){
            listMD = params?.jenisFA?.toString()?.split(",")
        }else{
            listMD = ['Auto 2000','Agung Auto Mall','Hadji Kalla','Hasjrat Abadi','New Ratna Motor']
        }
        for(int a=params?.bulan?.toInteger();a<=params?.bulan2?.toInteger();a++){
            def mbCon = new MonthlyBalanceController()
            def tgglAkhir = mbCon?.tanggalAkhir(a,params?.tahun?.toInteger())
            Date awal = new Date()?.parse("dd-M-yyyy","01-"+a+"-"+params?.tahun)
            Date akhir = new Date()?.parse("dd-M-yyyy",tgglAkhir+"-"+a+"-"+params?.tahun)
            listID.each {
                def idJenisFA = it?.toLong()
                def jenisFA = JenisFA.get(idJenisFA)
                def kegiatanFA = FA.findAllByJenisFA(jenisFA)
                kegiatanFA.each {
                    String namaFA = it?.m185NamaFA;
                    def kegiatan = it
                    listMD.each {
                        def mainDealer = it
                        def result = Reception.executeQuery("select rcp,vfa,fa from Reception rcp, VehicleFA vfa, FA fa where rcp.staDel = '0' and rcp.staSave = '0' and rcp.t401TglJamRencana >=? and rcp.t401TglJamRencana <? and fa.id = ? and fa.jenisFA.id = ? and vfa.t185MainDealer like ? and rcp.historyCustomerVehicle.customerVehicle=vfa.customerVehicle and vfa.fa.id = fa.id and rcp.companyDealer = ? order by rcp.t401TglJamRencana ",[awal,(akhir+1).clearTime(),kegiatan?.id,jenisFA?.id, "%"+mainDealer+"%" , company]);
                        def jumlahTerlibat = 0,inspectC = 0,repairC = 0,inspectRepairC = 0;
                        result.each {
                            Reception rcp = it[0]
                            FA fa = it[2]
                            def repair = Actual.findByReceptionAndStatusActualAndStaDel(rcp,StatusActual.findByM452StatusActualIlike("%Clock On%"),"0");
                            def fi = FinalInspection.findByReception(rcp)
                            if(fi && !repair){
                                inspectC++;
                            }
                            if(!fi && repair){
                                repairC++;
                            }
                            if(fi && repair){
                                inspectRepairC++;
                            }
                        }
                        jumlahTerlibat = result?.size()
                        if(result?.size()>0){
                            rows << [
                                    column_0: awal?.format("MMMM yyyy"),
                                    column_1: jenisFA.m184NamaJenisFA,
                                    column_2: namaFA,
                                    column_3: mainDealer,
                                    column_4: jumlahTerlibat,
                                    column_5: inspectC,
                                    column_6: repairC,
                                    column_7: inspectRepairC
                            ]
                        }
                    }
                }
            }
        }
        if(rows.size()<1){
            rows << [
                    column_0: "-",
                    column_1: "-",
                    column_2: "-",
                    column_3: "-",
                    column_4: 0,
                    column_5: 0,
                    column_6: 0,
                    column_7: 0
            ]

        }
        return rows
    }


}
