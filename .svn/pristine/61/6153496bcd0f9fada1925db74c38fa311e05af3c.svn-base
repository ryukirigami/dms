package com.kombos.baseapp.sec.shiro

class UserAdditionalColumnService {

    def gridUtilService

    def getList(def params){
        def ret = null

        try {
            def res = gridUtilService.getDomainList(UserAdditionalColumnInfo, params)
            def rows = []
            def counter = 0
            def userAdditionalColumn
            def userAdditionalColumnInfo
            def value
            ((List<UserAdditionalColumnInfo>)(res?.rows))?.each{
                userAdditionalColumnInfo = UserAdditionalColumnInfo.findByColumnName(it.columnName)
                userAdditionalColumn = UserAdditionalColumn.findByUserAndUserAdditionalColumnInfo(
                        User.findByUsername(params.username),
                        userAdditionalColumnInfo
                )

                if (!userAdditionalColumn)
                    value = ''
                else
                    value = userAdditionalColumn.value

                rows << [
                        id: counter ++,
                        cell:[
                                it.id,
                                params.username,
                                userAdditionalColumnInfo.columnName,
                                userAdditionalColumnInfo.label,
                                userAdditionalColumn==null?'':userAdditionalColumn.value
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def add(params) throws Exception{
        def userAdditionalColumn = new UserAdditionalColumn()
        userAdditionalColumn.user = User.findByUsername(params.username)
        userAdditionalColumn.userAdditionalColumnInfo = UserAdditionalColumnInfo.findByColumnName(params.columnName)
        userAdditionalColumn.value = params.value
        userAdditionalColumn.save()

        if (userAdditionalColumn.hasErrors()) {
            throw new Exception("${userAdditionalColumn.errors}")
        }
    }

    def edit(params){
        def userAdditionalColumn = UserAdditionalColumn.findByUserAndUserAdditionalColumnInfo(
            User.findByUsername(params.username),
            UserAdditionalColumnInfo.findByColumnName(params.columnName)
        )

        if (userAdditionalColumn) {

            userAdditionalColumn.value = params.value
            userAdditionalColumn.save()

            if (userAdditionalColumn.hasErrors()) {
                throw new Exception("${userAdditionalColumn.errors}")
            }
        }else{
            add(params)
        }
    }

    def delete(params){
        def userAdditionalColumn = UserAdditionalColumn.findByUserAndUserAdditionalColumnInfo(
                User.findByUsername(params.username),
                UserAdditionalColumnInfo.findByColumnName(params.columnName)
        )

        if (userAdditionalColumn) {
            userAdditionalColumn.delete()
        }else{
            throw new Exception("UserAdditionalColumn not found")
        }
    }
}