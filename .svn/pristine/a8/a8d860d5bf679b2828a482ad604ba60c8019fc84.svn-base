package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class VendorCatController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
	
	private static final String EMAIL_PATTERN = /[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})/;
	
	public boolean validasiEmail(String email) {
		email ==~ EMAIL_PATTERN
		}

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = VendorCat.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."sCriteria_m191ID") {
                ilike("m191ID", "%" + (params."sCriteria_m191ID"as String) + "%")
            }

            if (params."sCriteria_m191Nama") {
                ilike("m191Nama", "%" + (params."sCriteria_m191Nama" as String) + "%")
            }

            if (params."sCriteria_m191PIC") {
                ilike("m191PIC", "%" + (params."sCriteria_m191PIC" as String) + "%")
            }

            if (params."sCriteria_m191Alamat") {
                ilike("m191Alamat", "%" + (params."sCriteria_m191Alamat" as String) + "%")
            }

            if (params."sCriteria_m191Telp") {
                ilike("m191Telp", "%" + (params."sCriteria_m191Telp" as String) + "%")
            }

            if (params."sCriteria_m191Email") {
                ilike("m191Email", "%" + (params."sCriteria_m191Email" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m191ID: it.m191ID,

                    m191Nama: it.m191Nama,

                    m191PIC: it.m191PIC,

                    m191Alamat: it.m191Alamat,

                    m191Telp: it.m191Telp,

                    m191Email: it.m191Email,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

                    staDel: it.staDel,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [vendorCatInstance: new VendorCat(params)]
    }

    def save() {
        def vendorCatInstance = new VendorCat(params)

		String email = params.m191Email
		def checkEmail = validasiEmail(email)
		
		if(email != ''){
			if(!checkEmail){
			//    flash.message = message(code: 'default.companyDealer.email', args: [message(code: 'companyDealer.email.error', default: 'Masukan email dengan format xxx@xxx.xxx'),''])
				flash.message = 'Input Email dengan format : xxx@xxx.xxx'
				render(view: "create", model: [vendorCatInstance: vendorCatInstance])
				 return
			}
		}
		vendorCatInstance?.m191ID = generateCodeService.codeGenerateSequence("M191_ID",null)
        vendorCatInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        vendorCatInstance?.lastUpdProcess = "INSERT"
        vendorCatInstance?.setStaDel('0')
        if (!vendorCatInstance.save(flush: true)) {
            render(view: "create", model: [vendorCatInstance: vendorCatInstance])
            return
        }
        flash.message = message(code: 'default.created.message', args: [message(code: 'vendorCat.label', default: 'VendorCat'), vendorCatInstance.id])
        redirect(action: "show", id: vendorCatInstance.id)
    }

    def show(Long id) {
        def vendorCatInstance = VendorCat.get(id)
        if (!vendorCatInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vendorCat.label', default: 'VendorCat'), id])
            redirect(action: "list")
            return
        }

        [vendorCatInstance: vendorCatInstance]
    }

    def edit(Long id) {
        def vendorCatInstance = VendorCat.get(id)
        if (!vendorCatInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vendorCat.label', default: 'VendorCat'), id])
            redirect(action: "list")
            return
        }

        [vendorCatInstance: vendorCatInstance]
    }

    def update(Long id, Long version) {
        def vendorCatInstance = VendorCat.get(id)
        if (!vendorCatInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vendorCat.label', default: 'VendorCat'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (vendorCatInstance.version > version) {

                vendorCatInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'vendorCat.label', default: 'VendorCat')] as Object[],
                        "Another user has updated this VendorCat while you were editing")
                render(view: "edit", model: [vendorCatInstance: vendorCatInstance])
                return
            }
        }


		String email = params.m191Email
		def checkEmail = validasiEmail(email)
		
		if(email != ''){
			if(!checkEmail){
			//    flash.message = message(code: 'default.companyDealer.email', args: [message(code: 'companyDealer.email.error', default: 'Masukan email dengan format xxx@xxx.xxx'),''])
				flash.message = 'Input Email dengan format : xxx@xxx.xxx'
				render(view: "edit", model: [vendorCatInstance: vendorCatInstance])
				return
			}
		}

        vendorCatInstance.properties = params
        vendorCatInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        vendorCatInstance?.lastUpdProcess = "UPDATE"

        if (!vendorCatInstance.save(flush: true)) {
            render(view: "edit", model: [vendorCatInstance: vendorCatInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'vendorCat.label', default: 'VendorCat'), vendorCatInstance.id])
        redirect(action: "show", id: vendorCatInstance.id)
    }

    def delete(Long id) {
        def vendorCatInstance = VendorCat.get(id)
        if (!vendorCatInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vendorCat.label', default: 'VendorCat'), id])
            redirect(action: "list")
            return
        }

        try {
            vendorCatInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            vendorCatInstance?.lastUpdProcess = "DELETE"
            vendorCatInstance?.setStaDel('1')
            vendorCatInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'vendorCat.label', default: 'VendorCat'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'vendorCat.label', default: 'VendorCat'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(VendorCat, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(VendorCat, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
