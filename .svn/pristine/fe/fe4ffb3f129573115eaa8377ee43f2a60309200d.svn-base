package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class SertifikatController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = Sertifikat.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")	
			if(params."sCriteria_m016NamaSertifikat"){
				ilike("m016NamaSertifikat","%" + (params."sCriteria_m016NamaSertifikat" as String) + "%")
			}
	
			if(params."sCriteria_m016Inisial"){
				ilike("m016Inisial","%" + (params."sCriteria_m016Inisial" as String) + "%")
			}
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}
	
			if(params."sCriteria_m016ID"){
				ilike("m016ID","%" + (params."sCriteria_m016ID" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m016NamaSertifikat: it.m016NamaSertifikat.toUpperCase(),
			
						m016Inisial: it.m016Inisial.toUpperCase(),
			
						staDel: it.staDel,
			
						m016ID: it.m016ID,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[sertifikatInstance: new Sertifikat(params)]
	}

	def save() {
		def sertifikatInstance = new Sertifikat(params)
        sertifikatInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        sertifikatInstance?.lastUpdProcess = "INSERT"
		sertifikatInstance?.setStaDel('0')

        def cek = Sertifikat.createCriteria().list() {
            and{
                eq("m016NamaSertifikat",sertifikatInstance.m016NamaSertifikat?.trim(), [ignoreCase: true])
                eq("m016Inisial",sertifikatInstance.m016Inisial?.trim(), [ignoreCase: true])
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [sertifikatInstance: sertifikatInstance])
            return
        }

		if (!sertifikatInstance.save(flush: true)) {
			render(view: "create", model: [sertifikatInstance: sertifikatInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'sertifikat.label', default: 'Sertifikat Man Power'), sertifikatInstance.id])
		redirect(action: "show", id: sertifikatInstance.id)
	}

	def show(Long id) {
		def sertifikatInstance = Sertifikat.get(id)
		if (!sertifikatInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'sertifikat.label', default: 'Sertifikat'), id])
			redirect(action: "list")
			return
		}

		[sertifikatInstance: sertifikatInstance]
	}

	def edit(Long id) {
		def sertifikatInstance = Sertifikat.get(id)
		if (!sertifikatInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'sertifikat.label', default: 'Sertifikat'), id])
			redirect(action: "list")
			return
		}

		[sertifikatInstance: sertifikatInstance]
	}

	def update(Long id, Long version) {
		def sertifikatInstance = Sertifikat.get(id)
        if (!sertifikatInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'sertifikat.label', default: 'Sertifikat'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (sertifikatInstance.version > version) {
				
				sertifikatInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'sertifikat.label', default: 'Sertifikat')] as Object[],
				"Another user has updated this Sertifikat while you were editing")
				render(view: "edit", model: [sertifikatInstance: sertifikatInstance])
				return
			}
		}

//        if(Sertifikat.findByM016NamaSertifikatAndM016InisialAndStaDel(params.m016NamaSertifikat,params.m016Inisial,'0')!=null){
//            if(Sertifikat.findByM016NamaSertifikatAndM016InisialAndStaDel(params.m016NamaSertifikat,params.m016Inisial,'0').id != id){
//
//                flash.message = '* Data Sudah Ada'
//                render(view: "edit", model: [sertifikatInstance: sertifikatInstance])
//                return
//            }
//        }
        def cek = Sertifikat.createCriteria()
        def result = cek.list() {
            eq("m016NamaSertifikat",params.m016NamaSertifikat?.trim(), [ignoreCase: true])
            eq("m016Inisial",params.m016Inisial?.trim(), [ignoreCase: true])
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [sertifikatInstance: sertifikatInstance])
            return
        }
//

		sertifikatInstance.properties = params
        sertifikatInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        sertifikatInstance?.lastUpdProcess = "UPDATE"

		if (!sertifikatInstance.save(flush: true)) {
			render(view: "edit", model: [sertifikatInstance: sertifikatInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'sertifikat.label', default: 'Sertifikat Man Power'), sertifikatInstance.id])
		redirect(action: "show", id: sertifikatInstance.id)
	}

	def delete(Long id) {
		def sertifikatInstance = Sertifikat.get(id)
		if (!sertifikatInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'sertifikat.label', default: 'Sertifikat'), id])
			redirect(action: "list")
			return
		}

		try {
            sertifikatInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            sertifikatInstance?.lastUpdProcess = "DELETE"
			sertifikatInstance?.setStaDel('1')
			sertifikatInstance.save(flush:true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'sertifikat.label', default: 'Sertifikat'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'sertifikat.label', default: 'Sertifikat'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(Sertifikat, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(Sertifikat, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
