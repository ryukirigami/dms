package com.kombos.parts

import com.kombos.administrasi.MappingCompanyRegion
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.finance.Journal
import com.kombos.finance.JournalDetail
import com.kombos.finance.Transaction
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

import java.text.DateFormat
import java.text.SimpleDateFormat

class SalesReturController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    def salesReturService

    def konversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable : new Date().format("ddMMyyyymmss")]
    }

    def addParts() {

    }

    def formAddParts() {

    }
    def sublist() {
        [idTable : new Date().format("ddMMyyyymmss"),salesRetur : params.id]
    }
    def generateNomorRetur(){
        def result = [:]
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def currentDate = new Date().format("dd-MM-yyyy")
        def c = SalesRetur.createCriteria()
        def results = c.list {
            ge("dateCreated",df.parse(currentDate))
            lt("dateCreated",df.parse(currentDate) + 1)
        }

        def m = results.size()?:0
        def reg = MappingCompanyRegion.findByCompanyDealer(session.userCompanyDealer).companyDealer.m011ID
        def data = new Date().format("yyyyMMdd").toString() + "-" + reg +"-" + String.format('RT%05d',m+1)
        result.value = data
        return result
    }
    def datatablesList() {
        session.exportParams = params
        params.companyDealer = session?.userCompanyDealer
        render salesReturService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        session.exportParams = params

        render salesReturService.datatablesSubList(params) as JSON
    }

    def datatablesSOList() {
        session.exportParams = params

        render salesReturService.datatablesSOList(params) as JSON
    }

    def create() {
        def result = salesReturService.create(params)

        if (!result.error)
            return [salesReturInstance: result.salesReturInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        def idSO = params.idSO
        def note = params.note
        def sor = SalesOrder.get(idSO);
        sor.staDel = '1';
        sor.lastUpdProcess="DELETE"
        sor.save(flush: true);

        def dor = DeliveryOrder.findBySorNumber(sor)
        def dorr = DeliveryOrder.get(dor.id);
        dorr.staDel = '1'
        dorr.save(flush: true);

        def salesRetur = new SalesRetur(params)
        salesRetur?.sorNumber = dor?.sorNumber
        salesRetur?.companyDealer = dor?.companyDealer
        salesRetur?.nomorDo = dor
        salesRetur?.note = note
        salesRetur?.nomorRetur = generateNomorRetur().value
        salesRetur?.staDel = "0"
        salesRetur?.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
        salesRetur?.lastUpdProcess="INSERT"
        salesRetur?.dateCreated = datatablesUtilService?.syncTime()
        salesRetur?.lastUpdated = datatablesUtilService?.syncTime()
        salesRetur?.save(flush: true)

        SalesOrderDetail.findAllBySorNumberAndStaDel(sor,'0').each {
            def soDetail = new SalesReturDetail()
            soDetail?.salesRetur = salesRetur
            soDetail?.goods = it.materialCode
            soDetail?.qty = it.quantity
            soDetail?.harga = it.unitPrice
            soDetail?.discount = it.discount
            soDetail?.staDel = "0"
            soDetail.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
            soDetail.lastUpdProcess="INSERT"
            soDetail.dateCreated = datatablesUtilService?.syncTime()
            soDetail.lastUpdated = datatablesUtilService?.syncTime()
            soDetail?.save(flush: true)


            def ub = SalesOrderDetail.get(it.id)
            ub.staDel='1';
            ub.lastUpdProcess="DELETE"
            ub.save(flush: true)

            //
             def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it?.materialCode,'0',it.sorNumber?.companyDealer)
            if(!goodsStok){
                def partsStokService = new PartsStokService()
                partsStokService.newStock(it.materialCode,it?.sorNumber.companyDealer)
                goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it?.materialCode,'0',it.sorNumber?.companyDealer)
            }
            def kurangStok =  PartsStok.get(goodsStok.id)
            kurangStok?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            kurangStok?.lastUpdProcess = 'TUNAI_DELETE'
            kurangStok?.t131Qty1 = kurangStok?.t131Qty1 + it?.quantity
            kurangStok?.t131Qty2 = kurangStok?.t131Qty2 + it?.quantity
            kurangStok?.t131Qty1Free = kurangStok.t131Qty1Free + it?.quantity
            kurangStok?.t131Qty2Free = kurangStok.t131Qty2Free + it?.quantity
            kurangStok.lastUpdated = datatablesUtilService?.syncTime()
            kurangStok?.save(flush: true)


        }

        try {
            def journalId = Journal.findAllByDescriptionIlikeAndCompanyDealerAndStaDel("%"+sor.sorNumber+"%",sor.companyDealer,"0")
            journalId.each { j->
                def journalDel = Journal.get(j.id as Long)
                journalDel.staDel = '1'
                journalDel.lastUpdProcess = 'DELETE'
                journalDel.save(flush: true)

                def journalDetail = JournalDetail.findAllByJournal(journalDel)
                journalDetail.each {
                    def jDel = JournalDetail.get(it.id as Long)
                    jDel.staDel = '1'
                    jDel.lastUpdProcess = 'DELETE'
                    jDel.save(flush: true)

                }
                try {
                    def transa = Transaction.findAllByJournal(journalDel)
                    transa.each {
                        def jDel = Transaction.get(it.id as Long)
                        jDel.staDel = '1'
                        jDel.lastUpdProcess = 'DELETE'
                        jDel.save(flush: true)

                    }
                }catch (Exception e){

                }
            }
        }catch (Exception e){
            println e
        }

        render "OK"
    }

    def show(Long id) {
        def result = salesReturService.show(params)
        if (!result.error)
            return [salesReturInstance: result.salesReturInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = salesReturService.show(params)

        if (!result.error)
            return [salesReturInstance: result.salesReturInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = salesReturService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["SalesRetur", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [salesReturInstance: result.salesReturInstance.attach()])
    }

    def delete() {
        def result = salesReturService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["SalesRetur", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        params.companyDealer = session?.userCompanyDealer
        def hasil = salesReturService.massDelete(params)
        render "ok"
    }

    def listDO(){
        def res = [:]
        def opts = []
        def deliveries = DeliveryOrder.createCriteria().list {
            eq("staDel","0");
            eq("companyDealer",session?.userCompanyDealer);
            ilike("doNumber","%"+params?.query+"%")
            maxResults(10);
        }
        deliveries.each {
            if(!SalesRetur.findByNomorDoAndStaDel(it,'0')){
                opts<<it.doNumber
            }
        }
        res."options" = opts
        render res as JSON
    }

    def detailSO(){
        def result = [:]
        if(params.sorNumber){
            def doe = DeliveryOrder.createCriteria().list {
                eq("staDel","0")
                eq("companyDealer",session?.userCompanyDealer);
                eq("doNumber",params.sorNumber.trim(),[ignoreCase:true])
            }
            def dor = null
            if(doe.size()>0){
                doe.each {
                    dor = it?.sorNumber?.sorNumber
                }
            }

            def so = SalesOrder.createCriteria().list {
                eq("staDel","0")
                eq("sorNumber",dor)
                eq("companyDealer",session?.userCompanyDealer)
            }
            if(so.size()>0){
                def salesOrder = so.last()
                result."hasil" = "ada"
                result."idSO" = salesOrder?.id
                render result as JSON
            }else{
                result."hasil" = "tidak"
                render result as JSON
            }
        }else{
            result."hasil" = "tidak"
            render result as JSON
        }
    }

    def printSalesRetur(){
        def jsonArray = JSON.parse(params.idSalesRetur)
        def returnsList = []

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            returnsList << it
        }

        returnsList.each {

            def reportData = calculateReportData(it)

            def reportDef = new JasperReportDef(name:'salesRetur.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData

            )

            reportDefList.add(reportDef)
        }

        def file = File.createTempFile("RE_",".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")

        response.outputStream << file.newInputStream()

    }
    def calculateReportData(def id){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def sr = SalesRetur.findById(id as Long)
        def c = SalesReturDetail.createCriteria()
        def results = c.list {
            salesRetur{
                eq("id",id as Long)
            }
            eq("staDel","0")
        }

        results.sort {
            it.goods.m111ID
        }

        int count = 0
        def total = 0
        results.each {
            def data = [:]
            count = count + 1
            data.put("noRetur",it?.salesRetur?.nomorRetur)

            data.put("tglRetur",it?.salesRetur?.lastUpdated.format("dd/MM/YYYY"))
            data.put("dealer",session.userCompanyDealer)
            data.put("bulan",it.salesRetur?.lastUpdated.format("MMMM"))
            data.put("pembeli",it.salesRetur?.nomorDo?.sorNumber?.tradingFor.toUpperCase())
            data.put("nama",it.salesRetur?.nomorDo?.sorNumber?.customerName.toUpperCase())
            data.put("alamat",it.salesRetur?.nomorDo?.sorNumber?.deliveryAddress)
            data.put("franc",it.salesRetur?.nomorDo?.sorNumber?.salesType?.m117NamaFranc.toUpperCase())
            data.put("catatan",it.salesRetur?.nomorDo?.sorNumber?.note)
            data.put("note",it?.salesRetur?.note)
            data.put("exGudang",it.salesRetur?.nomorDo?.sorNumber?.deliveryLocation.toUpperCase())
            //
            def hargaTotal = (it.harga as Integer)*(it.qty as Integer)
            def hargaDisc = hargaTotal * ((it.discount/100)==0?0:(it.discount/100))
            def subTotal = hargaTotal-hargaDisc
            total += subTotal
            data.put("no",count)
            data.put("kodeParts",it.goods.m111ID)
            data.put("namaParts",it.goods.m111Nama)
            data.put("qty",it.qty as Integer)
            data.put("disc",it.discount as Integer)
            data.put("harga",konversi.toRupiah(it.harga as Integer))
            data.put("hargaTotal",konversi.toRupiah(hargaTotal as Integer))
            data.put("jumlah",konversi.toRupiah(subTotal as Integer))

            //
            data.put("total",konversi.toRupiah(total as Integer))
            data.put("noDo",it.salesRetur?.nomorDo?.doNumber)
            data.put("sor",it.salesRetur?.nomorDo?.sorNumber?.sorNumber)
            data.put("ppn","PPN (" + (it.salesRetur?.nomorDo?.sorNumber?.ppn as Integer) + "%)")
            def hargaPPn = (total as Integer)* (it.salesRetur?.nomorDo?.sorNumber?.ppn / 100)
            data.put("hargaPPN", konversi.toRupiah(hargaPPn as Integer))
            data.put("grandTotal", konversi.toRupiah((total+hargaPPn) as Integer))

            reportData.add(data)
        }

        return reportData

    }
}
