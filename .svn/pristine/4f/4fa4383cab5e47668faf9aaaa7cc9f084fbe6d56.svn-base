package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON

class JurnalAkhirTahunController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params
        String yerMan = (params.tahun.toInteger()+1).toString()
        def akun = params.akun.toString().contains("|") ? params.akun.toString().substring(0,params.akun.toString().indexOf("|")): params.akun.toString().replace(" ","")
        def akunInstance = AccountNumber.findByAccountNumberAndStaDelAndAccountMutationType(akun.trim(),"0","MUTASI")

        def c = MonthlyBalance.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session.userCompanyDealer)
            eq("awalTahun",yerMan)

            if(params.akun){
                eq("accountNumber",akunInstance)
            }else{
                accountNumber{
                    eq("id",-1000000.toLong())
                }
            }

        }

        def rows = []
        def konversi = new Konversi()
        results.each {
            rows << [

                    id: it.id,

                    accountNumber: it?.accountNumber?.accountNumber,

                    accountName: it?.accountNumber?.accountName,

                    startingBalance: konversi.toRupiah(it?.startingBalance),

                    debetMutation: konversi.toRupiah(it?.debitMutation),

                    creditMutation: konversi.toRupiah(it?.creditMutation),

                    endingBalance: konversi.toRupiah(it?.endingBalance)

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def index() {
        [skrg : new Date().clearTime()]
    }

    def doProses(){
        String staAda = "tidak"
        String periode = "12"+params.tahun
        def allMonthBalance = MonthlyBalance.createCriteria().list {
            eq("companyDealer",session.userCompanyDealer)
            ilike("yearMonth","%"+periode+"%")
            accountNumber{
                order("accountNumber")
            }
        }

        int count = 0;
        def saldoAwal = 0, mutasiDebet = 0 , mutasiKredit = 0, saldoAkhir = 0
        allMonthBalance.each {
            staAda = "ada";
            count++;
            def initJurnal = it.accountNumber.accountNumber
            initJurnal = initJurnal.substring(0,initJurnal.indexOf("."))
            if(initJurnal.toInteger()<=3){
                def nMonthBalance = MonthlyBalance.findByCompanyDealerAndAccountNumberAndYearMonthAndAwalTahun(session.userCompanyDealer,it.accountNumber,it.yearMonth,(params.tahun.toInteger()+1).toString())
                if(!nMonthBalance){
                    nMonthBalance = new MonthlyBalance()
                }
                nMonthBalance.properties = it.properties
                nMonthBalance.awalTahun = (params.tahun.toInteger()+1).toString()
                nMonthBalance.save(flush: true)
            }else{
                saldoAwal = saldoAwal + it.startingBalance
                mutasiDebet = mutasiDebet + it.debitMutation
                mutasiKredit = mutasiKredit + it.creditMutation
                saldoAkhir = saldoAkhir + it.endingBalance
                if(count==allMonthBalance.size()){
                    def accLabaDitahan = AccountNumber.createCriteria().get {
                        eq("staDel","0")
                        or{
                            ilike("accountNumber","%3.2.1.01.001%");
                            ilike("accountName","%RUGI/LABA DITAHAN%");
                        }
                        eq("accountMutationType","MUTASI")
                        maxResults(1);
                    }
                    if(accLabaDitahan){
                        def newMB = MonthlyBalance.findByCompanyDealerAndAccountNumberAndYearMonthAndAwalTahun(session.userCompanyDealer,accLabaDitahan,it.yearMonth,(params.tahun.toInteger()+1).toString())
                        if(!newMB){
                            newMB = new MonthlyBalance()
                        }
                        newMB.companyDealer = session.userCompanyDealer
                        newMB.yearMonth = it.yearMonth
                        newMB.startingBalance = saldoAwal
                        newMB.debitMutation = mutasiDebet
                        newMB.creditMutation = mutasiKredit
                        newMB.endingBalance = saldoAkhir
                        newMB.subLedger = it.subLedger
                        newMB.startingDate = new Date().parse("dd/MM/yyyy","01/01/"+params.tahun)
                        newMB.endingDate = new Date().parse("dd/MM/yyyy","31/12/"+params.tahun)
                        newMB.accountNumber = accLabaDitahan
                        newMB.subType = it.subType
                        newMB.awalTahun = (params.tahun.toInteger()+1).toString()
                        newMB.createdBy =org.apache.shiro.SecurityUtils.subject.principal.toString()
                        newMB.updatedBy =org.apache.shiro.SecurityUtils.subject.principal.toString()
                        newMB.lastUpdProcess = "INSERT"
                        newMB.save(flush: true)
                        newMB.errors.each {println it}
                    }
                }
            }
        }
        render staAda
    }

}
