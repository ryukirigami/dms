package com.kombos.LBUS.Master

enum PartialUploadStatus {
	INPROGRESS("INPROGRESS"), REJECT("REJECT"), PENDING("PENDING"), SUCCESS("SUCCESS"), FAIL("FAIL")

	final String value

	PartialUploadStatus(String value) {
		this.value = value
	}

	String toString() {
		value
	}
	String getKey() {
		name()
	}
}
