package com.kombos.hrd

/**
 * Created by DENDYSWARAN on 12/6/14.
 */
public enum HubunganKeluarga {
    IBU("0"),
    AYAH("1"),
    ISTRI("2"),
    SUAMI("3"),
    ANAK1("4"),
    ANAK2("5"),
    ANAK3("6"),
    ANAK4("7"),
    ANAK5("8"),
    ANAK6("9"),
    ANAK7("10"),
    ANAK8("11"),
    ANAK9("12"),
    ANAK10("13"),

    final String value

    public HubunganKeluarga(String value) {
        this.value = value
    }

    @Override
    public String toString() {
        return value
    }

    String getKey() {
        name()
    }

    static String findByValue(int val) {
        switch (val) {
            case 0: return IBU.key
            case 1: return AYAH.key
            case 2: return ISTRI.key
            case 3: return SUAMI.key
            case 4: return ANAK1.key
            case 5: return ANAK2.key
            case 6: return ANAK3.key
            case 7: return ANAK4.key
            case 8: return ANAK5.key
            case 9: return ANAK6.key
            case 10: return ANAK7.key
            case 11: return ANAK8.key
            case 12: return ANAK9.key
            case 13: return ANAK10.key
        }
    }


}