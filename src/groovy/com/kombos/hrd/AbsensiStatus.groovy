package com.kombos.hrd

/**
 * Created by DENDYSWARAN on 12/7/14.
 */
public enum AbsensiStatus {
    MASUK("MASUK"),
    KELUAR_ISTIRAHAT("KELUAR ISTIRAHAT"),
    MASUK_ISTIRAHAT("MASUK ISTIRAHAT"),
    KERJA("KERJA"),
    PULANG("PULANG"),
    IZIN("IZIN"),
    SAKIT("SAKIT"),
    ALPA("ALPA")

    final String value

    public AbsensiStatus(String value) {
        this.value = value
    }

    @Override
    public String toString() {
        return value
    }

    String getKey() {
        name()
    }
}