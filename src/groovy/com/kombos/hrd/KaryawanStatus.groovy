package com.kombos.hrd

/**
 * Created by DENDYSWARAN on 7/22/14.
 */
public enum KaryawanStatus {
    AVAILABLE("1"),
    UNAVAILABLE("0")

    final String value

    public KaryawanStatus(String value) {
        this.value = value
    }

    @Override
    public String toString() {
        return value
    }

    String getKey() {
        name()
    }

}