package com.kombos.approval

import com.kombos.parts.StatusApproval

/**
 * Created by Maulana Wahid A on 8/18/14.
 */
class ApprovalPOSublet implements AfterApprovalInterface {

    public static STATUS_APPROVED = "1"
    public static STATUS_UNAPPROVED = "2"
    public static STATUS_WAITING = "0"

    @Override
    def afterApproval(String fks, StatusApproval staApproval, Date tglApproveUnApprove, String alasanUnApprove, String namaUserApproveUnApprove) {
        return null
    }


}
