package com.kombos.approvalimport java.util.Date;

import com.kombos.parts.StatusApproval;



interface AfterApprovalInterface {
	def afterApproval(String fks, 		StatusApproval staApproval, 		Date tglApproveUnApprove,			String alasanUnApprove,			String namaUserApproveUnApprove)}
