package com.kombos.board

/**
 * Created by Lenovo on 11/23/2014.
 */
class ASBLine {
    String stallId
    String foremanId
    String foreman
    String groupId
    //String asbId
    String teknisiId
    String group
    String teknisi
    String stall
    String jam7 = ""
    String jam715 = ""
    String jam730 = ""
    String jam745 = ""
    String jam8 = ""
    String jam830 = ""
    String jam815 = ""
    String jam845 = ""
    String jam9 = ""
    String jam930 = ""
    String jam915 = ""
    String jam945 = ""
    String jam10 = ""
    String jam1030 = ""
    String jam1015 = ""
    String jam1045 = ""
    String jam11 = ""
    String jam1130 = ""
    String jam1115 = ""
    String jam1145 = ""
    String jam12 = ""
    String jam1230 = ""
    String jam1215 = ""
    String jam1245 = ""
    String jam13 = ""
    String jam1330 = ""
    String jam1315 = ""
    String jam1345 = ""
    String jam14 = ""
    String jam1430 = ""
    String jam1415 = ""
    String jam1445 = ""
    String jam15 = ""
    String jam1530 = ""
    String jam1515 = ""
    String jam1545 = ""
    String jam16 = ""
    String jam1630 = ""
    String jam1615 = ""
    String jam1645 = ""
    Date date
    String noPolJam7 = ""
    String noPolJam730 = ""
    String noPolJam715 = ""
    String noPolJam745 = ""
    String noPolJam8 = ""
    String noPolJam830 = ""
    String noPolJam815 = ""
    String noPolJam845 = ""
    String noPolJam9 = ""
    String noPolJam930 = ""
    String noPolJam915 = ""
    String noPolJam945 = ""
    String noPolJam10 = ""
    String noPolJam1030 = ""
    String noPolJam1015 = ""
    String noPolJam1045 = ""
    String noPolJam11 = ""
    String noPolJam1130 = ""
    String noPolJam1115 = ""
    String noPolJam1145 = ""
    String noPolJam12 = ""
    String noPolJam1230 = ""
    String noPolJam1215 = ""
    String noPolJam1245 = ""
    String noPolJam13 = ""
    String noPolJam1330 = ""
    String noPolJam1315 = ""
    String noPolJam1345 = ""
    String noPolJam14 = ""
    String noPolJam1430 = ""
    String noPolJam1415 = ""
    String noPolJam1445 = ""
    String noPolJam15 = ""
    String noPolJam1530 = ""
    String noPolJam1515 = ""
    String noPolJam1545 = ""
    String noPolJam16 = ""
    String noPolJam1630 = ""
    String noPolJam1615 = ""
    String noPolJam1645 = ""
    boolean actual = false

    def findFreeSlots(){
        def freeSlots = []
        ASBFreeSlot currentFreeSlot
        boolean prevEmpty = false
        if(prevEmpty){

        } else {
            if(jam7 == ""){
                prevEmpty = true
                currentFreeSlot = new ASBFreeSlot(this)
                currentFreeSlot.start = "jam7"
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 7)
                d.set(Calendar.MINUTE, 0)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam715 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }

        } else {
            if(jam715 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 7)
                d.set(Calendar.MINUTE, 15)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam715"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam730 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }

        } else {
            if(jam730 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 7)
                d.set(Calendar.MINUTE, 30)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam730"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam745 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }

        } else {
            if(jam745 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 7)
                d.set(Calendar.MINUTE, 45)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam745"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam8 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam8 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 8)
                d.set(Calendar.MINUTE, 0)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam8"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam815 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam815 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 8)
                d.set(Calendar.MINUTE, 15)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam815"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam830 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam830 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 8)
                d.set(Calendar.MINUTE, 30)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam830"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam845 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam845 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 8)
                d.set(Calendar.MINUTE, 45)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam845"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam9 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam9 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 9)
                d.set(Calendar.MINUTE, 0)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam9"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam915 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam915 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 9)
                d.set(Calendar.MINUTE, 15)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam915"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam930 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam930 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 9)
                d.set(Calendar.MINUTE, 30)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam930"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam945 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam945 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 9)
                d.set(Calendar.MINUTE, 45)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam945"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam10 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam10 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 10)
                d.set(Calendar.MINUTE, 0)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam10"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1015 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1015 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 10)
                d.set(Calendar.MINUTE, 15)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1015"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1030 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1030 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 10)
                d.set(Calendar.MINUTE, 30)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1030"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1045 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1045 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 10)
                d.set(Calendar.MINUTE, 45)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1045"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam11 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam11 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 11)
                d.set(Calendar.MINUTE, 0)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam11"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1115 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1115 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 11)
                d.set(Calendar.MINUTE, 15)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1115"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1130 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1130 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 11)
                d.set(Calendar.MINUTE, 30)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1130"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1145 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1145 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 11)
                d.set(Calendar.MINUTE, 45)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1145"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam12 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam12 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 12)
                d.set(Calendar.MINUTE, 0)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam12"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1215 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1215 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 12)
                d.set(Calendar.MINUTE, 15)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1215"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }


        if(prevEmpty){
            if(jam1230 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1230 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 12)
                d.set(Calendar.MINUTE, 30)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1230"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1245 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1245 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 12)
                d.set(Calendar.MINUTE, 45)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1245"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam13 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam13 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 13)
                d.set(Calendar.MINUTE, 0)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam13"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1315 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1315 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 13)
                d.set(Calendar.MINUTE, 15)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1315"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1330 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1330 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 13)
                d.set(Calendar.MINUTE, 30)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1330"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1345 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1345 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 13)
                d.set(Calendar.MINUTE, 45)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1345"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam14 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam14 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 14)
                d.set(Calendar.MINUTE, 0)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam14"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1415 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1415 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 14)
                d.set(Calendar.MINUTE, 15)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1415"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1430 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1430 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 14)
                d.set(Calendar.MINUTE, 30)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1430"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1445 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1445 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 14)
                d.set(Calendar.MINUTE, 45)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1445"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam15 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam15 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 15)
                d.set(Calendar.MINUTE, 0)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam15"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1515 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1515 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 15)
                d.set(Calendar.MINUTE, 15)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1515"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1530 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1530 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 15)
                d.set(Calendar.MINUTE, 30)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1530"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1545 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1545 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 15)
                d.set(Calendar.MINUTE, 45)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1545"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam16 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam16 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 16)
                d.set(Calendar.MINUTE, 0)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam16"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1615 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1615 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 16)
                d.set(Calendar.MINUTE, 15)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1615"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1630 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1630 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 16)
                d.set(Calendar.MINUTE, 30)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1630"
                currentFreeSlot.length = currentFreeSlot.length + 15
            } else {
                prevEmpty = false
            }
        }

        if(prevEmpty){
            if(jam1645 == "") {
                if (currentFreeSlot) {
                    currentFreeSlot.length = currentFreeSlot.length + 15
                }
                freeSlots << currentFreeSlot
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        } else {
            if(jam1645 == ""){
                prevEmpty = true
                currentFreeSlot =  new ASBFreeSlot(this)
                def d= new GregorianCalendar()
                d.setTime(date)
                d.set(Calendar.HOUR_OF_DAY, 16)
                d.set(Calendar.MINUTE, 45)
                currentFreeSlot.startDateTime = d.time
                currentFreeSlot.start = "jam1645"
                currentFreeSlot.length = currentFreeSlot.length + 15
                freeSlots << currentFreeSlot
            } else {
                prevEmpty = false
                freeSlots << currentFreeSlot
            }
        }

        freeSlots
    }


}
