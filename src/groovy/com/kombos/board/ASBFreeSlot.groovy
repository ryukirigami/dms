package com.kombos.board

/**
 * Created by Lenovo on 11/24/2014.
 */
class ASBFreeSlot {
    public ASBFreeSlot(ASBLine line){
//        println "Bikin free slot " + line.stall
        this.line = line
    }
    ASBLine line
    String start
    Date startDateTime
    Date breakStart
    int breakLength = 0
    int length = 0

    void setLine(ASBLine line) {
        this.line = line
    }

    void setStart(String start) {
//        println "set start: " + start
        this.start = start
    }

    void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime
    }

    void setLength(int length) {
        this.length = length
    }

    @Override
    public String toString() {
        return "ASBFreeSlot{" +
                "start='" + start + '\'' +
                ", startDateTime=" + startDateTime +
                ", length=" + length +
                '}';
    }
}
