package com.kombos.json

import grails.converters.JSON

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User

class UserMarshaller {
	//private SimpleDateFormat sdf
	private String dateFormat
	
	public UserMarshaller(){
		def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)
		dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		//sdf = new SimpleDateFormat(dateFormat)
	}
	void register() {
		JSON.registerObjectMarshaller(User) { User user ->
			return [
				username: user.username,
				fullname: user.fullname,
				enabled: user.enabled,
				status: user.status,
				lastLoggedIn: user.lastLoggedIn,
				loggedInFrom: user.loggedInFrom,
				lastUnsuccessfullLoggedIn: user.lastUnsuccessfullLoggedIn,
				createdOn: user.createdOn,
				createdBy: user.createdBy,
				updatedOn: user.updatedOn,
				updatedBy: user.updatedBy,
				effectiveStartDate: user.effectiveStartDate,
				effectiveEndDate: user.effectiveEndDate
			]
		}
	}
}
