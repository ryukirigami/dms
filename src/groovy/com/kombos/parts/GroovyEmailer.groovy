package com.kombos.parts

import com.kombos.administrasi.GeneralParameter

import javax.activation.DataHandler
import javax.activation.DataSource
import javax.activation.FileDataSource
import javax.mail.*
import javax.mail.internet.*
import javax.mail.util.ByteArrayDataSource

/**
 * Created by Maulana Wahid A on 3/20/14.
 */
class GroovyEmailer {

    private String username, password, host, subject, bodyMail

    GroovyEmailer() {
        username = ""
        password = ""
        subject = "Email SPLD : "+ new Date().toString()
        bodyMail = "blank"
    }
    GroovyEmailer(String usename, String passcode,String hostname) {
        username = usename
        password = passcode
        host = hostname

    }

    void sendMail(String to, String subject, String message,File file){
        def props = new Properties()
        props.put("mail.smtps.auth", "true")

        def session = Session.getDefaultInstance(props, null)

        def msg = new MimeMessage(session)

      //  println "byte size sendMail : "+bytes.size()

        msg.setSubject(subject)
        msg.setFrom(new InternetAddress(username))
        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to))


        Multipart multipart = new MimeMultipart();
        DataSource source = new FileDataSource(file)

        MimeBodyPart bodyPart = new MimeBodyPart();
        bodyPart.setText(message);

        MimeBodyPart attachPart =  new MimeBodyPart();

        attachPart.setDisposition(Part.ATTACHMENT);
        attachPart.setDataHandler(new DataHandler(source));
        attachPart.setText(message);
        attachPart.attachFile(file)
        attachPart.setHeader("Content-Type", "application/pdf");
        attachPart.setHeader("Content-Transfer-Encoding", "base64");

        multipart.addBodyPart(bodyPart);
        multipart.addBodyPart(attachPart);
        msg.setContent(multipart);

        def transport = session.getTransport "smtps"

        try {
            transport.connect (host, username, password)
            transport.sendMessage(msg,msg.getAllRecipients())
        }
        catch (Exception e) {
            e.printStackTrace()
        }
    }

}