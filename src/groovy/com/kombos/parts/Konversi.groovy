package com.kombos.parts

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

/**
 * Created by craftek on 08/08/14.
 */
class Konversi {
    def tanggalIndonesia(String tgl){
        def arrBulan = ['','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
        String tanggal = tgl.substring(0,2)
        String bulan = arrBulan[tgl.substring(3,5).toInteger()]
        String tahun = tgl.substring(6)
        String hasil = ""
        hasil = tanggal + " " + bulan + " " + tahun
        return hasil
    }
    def toRupiah(def value){
        String output = "0"
        if(value){
            DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
            DecimalFormatSymbols formatRp = new DecimalFormatSymbols()
            formatRp.setCurrencySymbol("");
            char titik = '.';
            char koma = ',';
            formatRp.setMonetaryDecimalSeparator(titik);
            formatRp.setGroupingSeparator(koma);
            kursIndonesia.setDecimalFormatSymbols(formatRp)
            output = kursIndonesia.format(BigDecimal.valueOf(value).setScale(2,BigDecimal.ROUND_HALF_UP));
//            output = kursIndonesia.format(BigDecimal.valueOf(value)); //YG ATAS DI UNCOMMENT INI DICOMMENT
        }
        return output
    }

    def toRupiahInvoice(def value){
        String output = "0"
        if(value){
            DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
            DecimalFormatSymbols formatRp = new DecimalFormatSymbols()
            formatRp.setCurrencySymbol("");
            char titik = '.';
            char koma = ',';
            formatRp.setGroupingSeparator(titik);
            kursIndonesia.setDecimalFormatSymbols(formatRp)
            kursIndonesia.setMaximumFractionDigits(0)
            output = kursIndonesia.format(value);
//            output = value
//            output = kursIndonesia.format(BigDecimal.valueOf(value)); //YG ATAS DI UNCOMMENT INI DICOMMENT
        }
        return output
    }

    def toRupiah2(def value){
        String output = "0"
        if(value){
            DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
            DecimalFormatSymbols formatRp = new DecimalFormatSymbols()
            formatRp.setCurrencySymbol("");
//            char titik = '.';
//            char koma = ',';
//            formatRp.setMonetaryDecimalSeparator(titik);
//            formatRp.setGroupingSeparator(koma);
            kursIndonesia.setDecimalFormatSymbols(formatRp)
//            output = kursIndonesia.format(BigDecimal.valueOf(value).setScale(2,BigDecimal.ROUND_HALF_UP));
            output = kursIndonesia.format(BigDecimal.valueOf(value)); //YG ATAS DI UNCOMMENT INI DICOMMENT
        }
        return output
    }

    def toKM (def kebo){
        String output = "0"
        if(kebo){
            def pattern = "##,###"
            def moneyform = new DecimalFormat(pattern)
            output = moneyform.format(kebo)
        }
        return output
    }

    def secondToHMS(def biggy){
        long longVal = biggy.longValue();
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;
        def jam = hours >= 10 || (hours < 0 && hours <= -10) ? hours:"0"+hours
        def menit = mins >= 10 || (mins < 0 && mins <= -10) ? mins:"0"+mins
        def detik = secs >= 10 || (secs < 0 && secs <= -10) ? secs:"0"+secs

        return jam+":"+menit+":"+detik;
    }

    def getHari(int tgl){
        def hari = ["Minggu","Senin","Selasa","Rabu","Kamis","Jum'at","Sabtu","Minggu"]
        return hari[tgl].toString()
    }
}
