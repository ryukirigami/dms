package com.kombos.parts

enum RequestStatus {
	BELUM_REQUEST_BOOKING_FEE("0"), BOOKING_FEE_BELUM_APPROVAL("1"), BELUM_VALIDASI("2"), VALIDASI_BELUM_APPROVAL("3")

	final String id

	RequestStatus(String id) {
		this.id = id
	}

	String toString() {
		id
	}
	String getKey() {
		name()
	}
	static public RequestStatus getEnumFromId(String id) {
        values().find {it.id == id }
    }   
}
