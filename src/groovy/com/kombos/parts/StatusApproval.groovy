package com.kombos.parts

enum StatusApproval {
	WAIT_FOR_APPROVAL("0"), APPROVED("1"), REJECTED("2"), WAIT_TO_SEND("3")

	final String id

	StatusApproval(String id) {
		this.id = id
	}

	String toString() {
		id
	}
	String getKey() {
		name()
	}
	static public StatusApproval getEnumFromId(String id) {
        values().find {it.id == id }
    }   
}
