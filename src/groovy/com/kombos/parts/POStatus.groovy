package com.kombos.parts

enum POStatus {
	OPEN("0"), CANCEL("1"), CLOSE("2")

	final String id

	POStatus(String id) {
		this.id = id
	}

	String toString() {
		id
	}
	String getKey() {
		name()
	}
	static public POStatus getEnumFromId(String id) {
        values().find {it.id == id }
    }   
}
