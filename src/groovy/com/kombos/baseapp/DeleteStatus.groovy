package com.kombos.baseapp

enum DeleteStatus {
	NOT_DELETED("0"), DELETED("1")

	final String value

	DeleteStatus(String value) {
		this.value = value
	}

	String toString() {
		value
	}
	String getKey() {
		name()
	}
}
