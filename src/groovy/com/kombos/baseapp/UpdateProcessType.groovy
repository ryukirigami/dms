package com.kombos.baseapp

enum UpdateProcessType {
	INSERT("INSERT"), UPDATE("UPDATE"), DELETE("DELETE")

	final String value

	UpdateProcessType(String value) {
		this.value = value
	}

	String toString() {
		value
	}
	String getKey() {
		name()
	}
}
