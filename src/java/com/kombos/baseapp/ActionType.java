package com.kombos.baseapp;

public enum ActionType {
	VIEW("VIEW"),
	ADD("ADD"),
	EDIT("EDIT"),
	DELETE("DELETE");
	
	final String value;

	ActionType(String value) {
		this.value = value;
	}

	public String toString() {
		return value;
	}
	
}
