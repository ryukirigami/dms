package com.kombos.baseapp;

public enum ApprovalStatus {
	DRAFT("DRAFT"),
	PENDING("PENDING"),
	APPROVED("APPROVED"),
	REJECTED("REJECTED");
	
	final String value;

	ApprovalStatus(String value) {
		this.value = value;
	}

	public String toString() {
		return value;
	}
	
}
