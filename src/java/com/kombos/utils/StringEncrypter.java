package com.kombos.utils;

//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * @author Arnold
 */
public class StringEncrypter
{
    Cipher ecipher;
    Cipher dcipher;

    private StringEncrypter(SecretKey paramSecretKey, String paramString)
    {
        try
        {
            this.ecipher = Cipher.getInstance(paramString);
            this.dcipher = Cipher.getInstance(paramString);
            this.ecipher.init(1, paramSecretKey);
            this.dcipher.init(2, paramSecretKey);
        } catch (NoSuchPaddingException localNoSuchPaddingException) {
            System.out.println("EXCEPTION: NoSuchPaddingException");
        } catch (NoSuchAlgorithmException localNoSuchAlgorithmException) {
            System.out.println("EXCEPTION: NoSuchAlgorithmException");
        } catch (InvalidKeyException localInvalidKeyException) {
            System.out.println("EXCEPTION: InvalidKeyException");
        }
    }

    private StringEncrypter(String paramString)
    {
        byte[] arrayOfByte = { -87, -101, -56, 50, 86, 52, -29, 3 };

        int i = 19;
        try
        {
            PBEKeySpec localPBEKeySpec = new PBEKeySpec(paramString.toCharArray(), arrayOfByte, i);
            SecretKey localSecretKey = SecretKeyFactory.getInstance("PBEWithMD5andDES").generateSecret(localPBEKeySpec);

            this.ecipher = Cipher.getInstance(localSecretKey.getAlgorithm());
            this.dcipher = Cipher.getInstance(localSecretKey.getAlgorithm());

            PBEParameterSpec localPBEParameterSpec = new PBEParameterSpec(arrayOfByte, i);

            this.ecipher.init(1, localSecretKey, localPBEParameterSpec);
            this.dcipher.init(2, localSecretKey, localPBEParameterSpec);
        } catch (NoSuchPaddingException localNoSuchPaddingException) {
            System.out.println("EXCEPTION: NoSuchPaddingException");
        } catch (NoSuchAlgorithmException localNoSuchAlgorithmException) {
            System.out.println("EXCEPTION: NoSuchAlgorithmException");
        } catch (InvalidKeyException localInvalidKeyException) {
            System.out.println("EXCEPTION: InvalidKeyException");
        } catch (InvalidAlgorithmParameterException localInvalidAlgorithmParameterException) {
            System.out.println("EXCEPTION : InvalidAlgorithmParameterException");
        } catch (InvalidKeySpecException localInvalidKeySpecException) {
            System.out.println("EXCEPTION: InvalidKeySpecException");
        }
    }

    public String encrypt(String paramString) {
        try {
            byte[] arrayOfByte1 = paramString.getBytes("UTF-8");

            byte[] arrayOfByte2 = this.ecipher.doFinal(arrayOfByte1);

//            return new BASE64Encoder().encode(arrayOfByte2);
            return Base64.encodeBase64String(arrayOfByte2);
        } catch (BadPaddingException localBadPaddingException) {
        } catch (IllegalBlockSizeException localIllegalBlockSizeException) {
        } catch (UnsupportedEncodingException localUnsupportedEncodingException) {
        } 
        return null;
    }

    public String decrypt(String paramString) {
        try {
//            byte[] arrayOfByte1 = new BASE64Decoder().decodeBuffer(paramString);
            byte[] arrayOfByte1 = Base64.decodeBase64(paramString);
            byte[] arrayOfByte2 = this.dcipher.doFinal(arrayOfByte1);
            return new String(arrayOfByte2, "UTF-8");
        } catch (BadPaddingException localBadPaddingException) {
        } catch (IllegalBlockSizeException localIllegalBlockSizeException) {
        } catch (UnsupportedEncodingException localUnsupportedEncodingException) {
//        } catch (IOException localIOException) {
        }
        return null;
    }

    public static void testUsingSecretKey()
    {
        try
        {
            System.out.println();
            System.out.println("+----------------------------------------+");
            System.out.println("|  -- Test Using Secret Key Method --    |");
            System.out.println("+----------------------------------------+");
            System.out.println();

            String str1 = "Attack at dawn!";

            SecretKey localSecretKey1 = KeyGenerator.getInstance("DES").generateKey();
            SecretKey localSecretKey2 = KeyGenerator.getInstance("Blowfish").generateKey();
            SecretKey localSecretKey3 = KeyGenerator.getInstance("DESede").generateKey();

            StringEncrypter localStringEncrypter1 = new StringEncrypter(localSecretKey1, localSecretKey1.getAlgorithm());
            StringEncrypter localStringEncrypter2 = new StringEncrypter(localSecretKey2, localSecretKey2.getAlgorithm());
            StringEncrypter localStringEncrypter3 = new StringEncrypter(localSecretKey3, localSecretKey3.getAlgorithm());

            String str2 = localStringEncrypter1.encrypt(str1);
            String str3 = localStringEncrypter2.encrypt(str1);
            String str4 = localStringEncrypter3.encrypt(str1);

            String str5 = localStringEncrypter1.decrypt(str2);
            String str6 = localStringEncrypter2.decrypt(str3);
            String str7 = localStringEncrypter3.decrypt(str4);

            System.out.println(localSecretKey1.getAlgorithm() + " Encryption algorithm");
            System.out.println("    Original String  : " + str1);
            System.out.println("    Encrypted String : " + str2);
            System.out.println("    Decrypted String : " + str5);
            System.out.println();

            System.out.println(localSecretKey2.getAlgorithm() + " Encryption algorithm");
            System.out.println("    Original String  : " + str1);
            System.out.println("    Encrypted String : " + str3);
            System.out.println("    Decrypted String : " + str6);
            System.out.println();

            System.out.println(localSecretKey3.getAlgorithm() + " Encryption algorithm");
            System.out.println("    Original String  : " + str1);
            System.out.println("    Encrypted String : " + str4);
            System.out.println("    Decrypted String : " + str7);
            System.out.println();
        }
        catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
        {
        }
    }

    public static String testUsingPassPhrase(String paramString)
    {
        System.out.println();
        System.out.println("+----------------------------------------+");
        System.out.println("|  -- Test Using Pass Phrase Method --   |");
        System.out.println("+----------------------------------------+");
        System.out.println();

        String str1 = "OFSA1Password@Phrase2";

        StringEncrypter localStringEncrypter = new StringEncrypter(str1);

        String str2 = localStringEncrypter.encrypt(paramString);

        String str3 = localStringEncrypter.decrypt(str2);

        System.out.println("PBEWithMD5AndDES Encryption algorithm");
        System.out.println("    Original String  : " + paramString);
        System.out.println("    Encrypted String : " + str2);
        System.out.println("    Decrypted String : " + str3);
        System.out.println();

        return str2;
    }

    private static final StringEncrypter instance = new StringEncrypter("g4S93bDux36V9H90wmDNKZT8P4ibwE3v");

    public static StringEncrypter getInstance(){
        return instance;
    }

    public static void main(String[] args)
    {
//        if (args.length > 0) {
//            /*testUsingPassPhrase(args[0]);
//
//            testUsingSecretKey();*/
//
//            StringEncrypter enc = StringEncrypter.getInstance();
//            String encrypted = enc.encrypt(args[0]);
//            String decrypted = enc.decrypt(encrypted);
//
//            System.out.println("Original : " + args[0]);
//            System.out.println("Encrypted : " + encrypted);
//            System.out.println("Decrypted : " + decrypted);
//        } else {
//            System.out.println("Usage: <com.kombos.baseapp> <String>");
//            System.exit(1);
//        }
    	
    	 {
             StringEncrypter enc = StringEncrypter.getInstance();
             String encrypted = enc.encrypt("url=jdbc:oracle:thin:@localhost:1521:xe|username=lbus100|password=lbus100");
             String decrypted = enc.decrypt(encrypted);

             System.out.println("Original : " + args[0]);
             System.out.println("Encrypted : " + encrypted);
             System.out.println("Decrypted : " + decrypted);
         }    			 
    }
}
