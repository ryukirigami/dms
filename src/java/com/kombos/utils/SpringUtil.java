package com.kombos.utils;

import org.codehaus.groovy.grails.web.context.ServletContextHolder;
import org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes;
import org.springframework.context.ApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * User: arnold
 * Date: 26/09/12
 * Time: 14:08
 */
public class SpringUtil {

    public static ApplicationContext getCtx() {
        return getGrailsApplicationContext();
    }

    public static ApplicationContext getGrailsApplicationContext() {
        return (ApplicationContext) ServletContextHolder.getServletContext().getAttribute(GrailsApplicationAttributes.APPLICATION_CONTEXT);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getGrailsBean(String beanName) {
        return (T) getGrailsApplicationContext().getBean(beanName);
    }

}
