package com.kombos.utils;

/**
 *
 * @author Febry Indra Setyawan
 */
public class MoneyUtil {
    private static final String EMPTY = "";
    private static final String SPACE = " ";
    private static final String DOT = ".";

    private static final String RUPIAH = "rupiah";
    private static final String SEN = "sen";

    private static final String NOL = "nol";
    private static final String PREFFIX = "se";
    private static final String PULUHAN = "puluh";
    private static final String BELASAN = "belas";
    private static final String RATUSAN = "ratus";
    private static final String RIBUAN = "ribu";
    private static final String JUTAAN = "juta";
    private static final String MILIARAN = "miliar";
    private static final String TRILIUNAN = "triliun";

    private static final String[] units = {EMPTY, "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan"};
    private static final String[] thousands = {EMPTY, SPACE + RIBUAN, SPACE + JUTAAN, SPACE + MILIARAN, SPACE + TRILIUNAN};

    //private StringBuilder words;

    public String convertMoneyToWords (double money) {
        StringBuilder words = new StringBuilder();
        long number = (long) money;

        if (number == 0L) {
            words.append(NOL + SPACE);
        } else {
            int digits = 0;
            long step = 1L;
            while (step <= number) {
                digits++;
                step *= 1000L;
            }

            for (int index = (digits - 1); index >= 0; index--) {
                long counter = (long) Math.pow(1000, index);
                long temp = number / counter;
                short reminder = (short) (temp % 1000L);

                if (reminder > 0) {
                    addWords(reminder, thousands[index % thousands.length], words);
                    words.append(SPACE);
                }
            }
        }

        words.append(RUPIAH);

        double moneyWithoutDecimal = (double) number;
        double fraction = money - moneyWithoutDecimal;
        if (fraction > 0d) {
            short cent = (short) (fraction * 100d);
            words.append(SPACE);
            addWords(cent, EMPTY, words);
            words.append(SPACE + SEN);
        }

        words.append(DOT);

        return words.toString();
    }

    private void addWords (short number, String suffix, StringBuilder words) {
        int[] digits = new int[3];
        for (int index = 2; index >= 0; index--) {
            digits[index] = number % 10;
            number /= 10;
        }

        boolean isLeadingZero = true;

        if (digits[0] > 0) {
            if (digits[0] == 1) {
                words.append(PREFFIX + RATUSAN);
            } else {
                words.append(units[digits[0]]).append(SPACE + RATUSAN);
            }
            isLeadingZero = false;
        }

        if (digits[1] > 0) {
            if (digits[0] > 0) {
                words.append(SPACE);
            }

            if (digits[1] == 1) {
                switch (digits[2]) {
                    case 0:
                        words.append(PREFFIX + PULUHAN);
                        break;
                    case 1:
                        words.append(PREFFIX + BELASAN);
                        break;
                    default:
                        words.append(units[digits[2]]).append(SPACE + BELASAN);
                        break;
                }

                words.append(suffix);
                return;
            }

            words.append(units[digits[1]]).append(SPACE + PULUHAN);
            isLeadingZero = false;

            if (digits[2] == 0) {
                words.append(suffix);
                return;
            }

            words.append(SPACE);
        }

        if(isLeadingZero && (digits[2] == 1) && (suffix.equals(SPACE + RIBUAN))) {
            words.append(PREFFIX + RIBUAN);
            return;
        }

        words.append(units[digits[2]]).append(suffix);
    }

//    public static void main(String[] args) {
//        double money = 1234567890d;
//        System.out.println("Money : " + money);
//        MoneyUtil converter = new MoneyUtil();
//        String result = converter.convertMoneyToWords(money);
//        System.out.println("Money in words : " + result);
//    }
}
