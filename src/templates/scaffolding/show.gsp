
<% import grails.persistence.Event %>
<%=packageName%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var delete${className};

\$(function(){ 
	delete${className}=function(id){
		\$.ajax({type:'POST', url:'\${request.contextPath}/${domainClass.propertyName}/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reload${className}Table();
   				expandTableLayout('${domainClass.propertyName}');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				\$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-${domainClass.propertyName}" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="\${flash.message}">
			<div class="message" role="status">\${flash.message}</div>
		</g:if>
		<table id="${domainClass.propertyName}"
			class="table table-bordered table-hover">
			<tbody>

				<%  excludedProps = Event.allEvents.toList() << 'id' << 'version'
				allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
				props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) }
				Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
				props.each { p -> %>
				<g:if test="\${${propertyName}?.${p.name}}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="${p.name}-label" class="property-label"><g:message
					code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="${p.name}-label">
						%{--<ba:editableValue
								bean="\${${propertyName}}" field="${p.name}"
								url="\${request.contextPath}/${className}/updatefield" type="text"
								title="Enter ${p.name}" onsuccess="reload${className}Table();" />--}%
							<%  if (p.isEnum()) { %>
								<g:fieldValue bean="\${${propertyName}}" field="${p.name}"/>
							<%  } else if (p.oneToMany || p.manyToMany) { %>
								<g:each in="\${${propertyName}.${p.name}}" var="${p.name[0]}">
								<g:link controller="${p.referencedDomainClass?.propertyName}" action="show" id="\${${p.name[0]}.id}">\${${p.name[0]}?.encodeAsHTML()}</g:link>
								</g:each>
							<%  } else if (p.manyToOne || p.oneToOne) { %>
								<g:link controller="${p.referencedDomainClass?.propertyName}" action="show" id="\${${propertyName}?.${p.name}?.id}">\${${propertyName}?.${p.name}?.encodeAsHTML()}</g:link>
							<%  } else if (p.type == Boolean || p.type == boolean) { %>
								<g:formatBoolean boolean="\${${propertyName}?.${p.name}}" />
							<%  } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
								<g:formatDate date="\${${propertyName}?.${p.name}}" />
							<%  } else if (!p.type.isArray()) { %>
								<g:fieldValue bean="\${${propertyName}}" field="${p.name}"/>
							<%  } %>
						</span></td>
					
				</tr>
				</g:if>
			<%  } %>
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('${domainClass.propertyName}');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="\${${propertyName}?.id}"
					update="[success:'${domainClass.propertyName}-form',failure:'${domainClass.propertyName}-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="\${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="delete${className}('\${${propertyName}?.id}')" label="\${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
