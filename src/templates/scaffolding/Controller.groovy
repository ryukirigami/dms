<%=packageName ? "package ${packageName}\n\n" : ''%>
<% def instanceName = propertyName.replace('Instance','') %>
<% import grails.persistence.Event %>

import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException
import java.text.SimpleDateFormat
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService

class ${className}Controller {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def ${instanceName}Service

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render ${instanceName}Service.datatablesList(params) as JSON
	}

	def create() {
		def result = ${instanceName}Service.create(params)

        if(!result.error)
            return [${instanceName}Instance: result.${instanceName}Instance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
		 def result = ${instanceName}Service.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["${className}", result.${instanceName}Instance.id])
            redirect(action:'show', id: result.${instanceName}Instance.id)
            return
        }

        render(view:'create', model:[${instanceName}Instance: result.${instanceName}Instance])
	}

	def show(Long id) {
		def result = ${instanceName}Service.show(params)

		if(!result.error)
			return [ ${instanceName}Instance: result.${instanceName}Instance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = ${instanceName}Service.show(params)

		if(!result.error)
			return [ ${instanceName}Instance: result.${instanceName}Instance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
		 def result = ${instanceName}Service.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["${className}", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[${instanceName}Instance: result.${instanceName}Instance.attach()])
	}

	def delete() {
		def result = ${instanceName}Service.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["${className}", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(${className}, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
