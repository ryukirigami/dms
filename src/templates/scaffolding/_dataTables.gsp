<% import grails.persistence.Event %>
<%=packageName%>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="${domainClass.propertyName}_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
<%
				excludedProps = Event.allEvents.toList() << 'version' << 'dateCreated' << 'lastUpdated' << 'approval'<< 'createdBy' << 'updatedBy' << 'staDel'
	persistentPropNames = domainClass.persistentProperties*.name
	if (org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsDomainBinder.getMapping(domainClass)?.identity?.generator == 'assigned') {
		persistentPropNames << domainClass.identifier.name
	}
	props = domainClass.properties.findAll { !excludedProps.contains(it.name) }
	Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
	colSize = 0	
	for (p in props) { if(p.name != domainClass.identifier.name) {
%>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}" /></div>
			</th>

<%
	colSize++
	}}
%>		
		</tr>
		<tr>
		
<% 
	for (p in props) { if(p.name != domainClass.identifier.name) {
		if(p.type != Date){
%>	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_${p.name}" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_${p.name}" class="search_init" />
				</div>
			</th>
<%		} else {
%>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_${p.name}" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_${p.name}" value="date.struct">
					<input type="hidden" name="search_${p.name}_day" id="search_${p.name}_day" value="">
					<input type="hidden" name="search_${p.name}_month" id="search_${p.name}_month" value="">
					<input type="hidden" name="search_${p.name}_year" id="search_${p.name}_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_${p.name}_dp" value="" id="search_${p.name}" class="search_init">
				</div>
			</th>
<%		
		}
	}}
%>

		</tr>
	</thead>
</table>

<g:javascript>
var ${domainClass.propertyName}Table;
var reload${className}Table;
\$(function(){
	
	reload${className}Table = function() {
		${domainClass.propertyName}Table.fnDraw();
	}

	<%
	for (p in props) { if(p.name != domainClass.identifier.name) {
		if(p.type == Date){
%>
	\$('#search_${p.name}').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			\$('#search_${p.name}_day').val(newDate.getDate());
			\$('#search_${p.name}_month').val(newDate.getMonth()+1);
			\$('#search_${p.name}_year').val(newDate.getFullYear());
			\$(this).datepicker('hide');
			${domainClass.propertyName}Table.fnDraw();	
	});

	
<%
		}
	}}
%>

\$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	${domainClass.propertyName}Table.fnDraw();
		}
	});
	\$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	${domainClass.propertyName}Table = \$('#${domainClass.propertyName}_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "\${g.createLink(action: "datatablesList")}",
		"aoColumns": [
<% 
idx = 0
for (p in props) { if(p.name != domainClass.identifier.name) {
if(idx == 0){
%>
{
	"sName": "${p.name}",
	"mDataProp": "${p.name}",
	"aTargets": [${idx}],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}
<%
} else {
%>
{
	"sName": "${p.name}",
	"mDataProp": "${p.name}",
	"aTargets": [${idx}],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
<%
}
if(idx < colSize - 1){
%>
,
<%	
}
idx++
}}
%>
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
<% 
	for (p in props) { if(p.name != domainClass.identifier.name) {
		if(p.type != Date){
%>	
						var ${p.name} = \$('#filter_${p.name} input').val();
						if(${p.name}){
							aoData.push(
									{"name": 'sCriteria_${p.name}', "value": ${p.name}}
							);
						}
<%		} else {
%>
						var ${p.name} = \$('#search_${p.name}').val();
						var ${p.name}Day = \$('#search_${p.name}_day').val();
						var ${p.name}Month = \$('#search_${p.name}_month').val();
						var ${p.name}Year = \$('#search_${p.name}_year').val();
						
						if(${p.name}){
							aoData.push(
									{"name": 'sCriteria_${p.name}', "value": "date.struct"},
									{"name": 'sCriteria_${p.name}_dp', "value": ${p.name}},
									{"name": 'sCriteria_${p.name}_day', "value": ${p.name}Day},
									{"name": 'sCriteria_${p.name}_month', "value": ${p.name}Month},
									{"name": 'sCriteria_${p.name}_year', "value": ${p.name}Year}
							);
						}
<%						
					}}}
%>
						\$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
