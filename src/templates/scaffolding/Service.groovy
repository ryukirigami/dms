<%=packageName ? "package ${packageName}" : ''%>
<% import grails.persistence.Event %>
<% def count = 0; def properties = domainClass.getPersistentProperties(); %>
<% def instanceName = propertyName.replace('Instance',''); def idProp_ = org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsDomainBinder.evaluateMapping(domainClass).getIdentity().getName()?:"id"; def idProp = (!idProp_?"":idProp_.getAt(0).toUpperCase() + idProp_.substring(1));%>
import com.kombos.baseapp.AppSettingParam

class ${className}Service {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = ${className}.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			<%
			excludedProps = Event.allEvents.toList() << 'version' << 'dateCreated' << 'lastUpdated' << 'approval' << 'createdBy' << 'updatedBy' << 'staDel'
persistentPropNames = domainClass.persistentProperties*.name
if (org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsDomainBinder.getMapping(domainClass)?.identity?.generator == 'assigned') {
	persistentPropNames << domainClass.identifier.name
}
props = domainClass.properties.findAll { !excludedProps.contains(it.name) }
Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
for (p in props) { if(p.name != domainClass.identifier.name) {
		if(p.type == String){
%>
			if(params."sCriteria_${p.name}"){
				ilike("${p.name}","%" + (params."sCriteria_${p.name}" as String) + "%")
			}
<%		} else if(p.type == Date) {
%>
			if(params."sCriteria_${p.name}"){
				ge("${p.name}",params."sCriteria_${p.name}")
				lt("${p.name}",params."sCriteria_${p.name}" + 1)
			}
<%
		} else {
%>
			if(params."sCriteria_${p.name}"){
				eq("${p.name}",params."sCriteria_${p.name}")
			}
<%
		}
	}}
%>
			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			<%
				for (p in props) {
					if(p.type == Date){
			%>
						${p.name}: it.${p.name}?it.${p.name}.format(dateFormat):"",
			<%		} else {
			%>
						${p.name}: it.${p.name},
			<%
					}
				}
			%>
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["${className}", params.id] ]
			return result
		}

		result.${propertyName} = ${className}.get(params.id)

		if(!result.${propertyName})
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["${className}", params.id] ]
			return result
		}

		result.${propertyName} = ${className}.get(params.id)

		if(!result.${propertyName})
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["${className}", params.id] ]
			return result
		}

		result.${propertyName} = ${className}.get(params.id)

		if(!result.${propertyName})
			return fail(code:"default.not.found.message")

		try {
			result.${propertyName}.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		${className}.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.${propertyName} && m.field)
					result.${propertyName}.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["${className}", params.id] ]
				return result
			}

			result.${propertyName} = ${className}.get(params.id)

			if(!result.${propertyName})
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.${propertyName}.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.${propertyName}.properties = params

			if(result.${propertyName}.hasErrors() || !result.${propertyName}.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["${className}", params.id] ]
			return result
		}

		result.${propertyName} = new ${className}()
		result.${propertyName}.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.${propertyName} && m.field)
				result.${propertyName}.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["${className}", params.id] ]
			return result
		}

		result.${propertyName} = new ${className}(params)

		if(result.${propertyName}.hasErrors() || !result.${propertyName}.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def ${instanceName} =  ${className}.findBy${idProp}(params.${idProp_})
		if (${instanceName}) {
			${instanceName}."\${params.name}" = params.value
			${instanceName}.save()
			if (${instanceName}.hasErrors()) {
				throw new Exception("\${${instanceName}.errors}")
			}
		}else{
			throw new Exception("${className} not found")
		}
	}

}