@artifact.package @class @artifact.name @{

static String name = "@artifact.name@";
static String description = "The @artifact.name@ skin";
static String css = "@artifact.name@.css";

}
